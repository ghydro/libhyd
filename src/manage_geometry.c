/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_geometry.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"

/* Creation of the geometry structure of a face */
s_geometry_hyd *HYD_create_geometry() {
  s_geometry_hyd *pgeom;

  pgeom = new_geometry();
  bzero((char *)pgeom, sizeof(s_geometry_hyd));

  return pgeom;
}

/* Creation of the points describing an AbscZ cross-section geometry */
void HYD_create_ptsAbscZ(s_face_hyd *pface, s_pointAbscZ_hyd *pptAbscZ) {
  /* Point nb */
  int p;
  /* Abscisses of the left and right banks of the cross-section */
  double absc_LB, absc_RB;

  pface->geometry->p_pointsAbscZ = (s_pointAbscZ_hyd **)calloc(pface->geometry->npts, sizeof(s_pointAbscZ_hyd *));

  pptAbscZ = HYD_rewind_ptAbscZ(pptAbscZ, BEGINNING);
  absc_LB = pptAbscZ->absc;
  pptAbscZ = HYD_rewind_ptAbscZ(pptAbscZ, END);
  absc_RB = pptAbscZ->absc;

  for (p = 0; p < pface->geometry->npts; p++) {

    pface->geometry->p_pointsAbscZ[p] = new_pointAbscZ();
    bzero((char *)pface->geometry->p_pointsAbscZ[p], sizeof(s_pointAbscZ_hyd)); // SW 22/03/2021 il faut initialiser
    pface->geometry->p_pointsAbscZ[p]->absc = 0.5 * (absc_LB + absc_RB) - pptAbscZ->absc;
    pface->geometry->p_pointsAbscZ[p]->z = pptAbscZ->z;
    pptAbscZ = pptAbscZ->prev;
  }
  // pface->description = HYD_create_description(); // SW 29/01/2018 il faut intialiser
}

/* creation d'une geometry automatique */
void HYD_create_musk_geometry(s_face_hyd *pface, double pk, double Zbot_am, double Zbot_av, FILE *fp) {
  /* Point nb */
  int p;
  /* Abscisses of the left and right banks of the cross-section */
  double width, z_LB, z_bot;
  double slope, h;
  double slope_zbot;
  pface->geometry->p_pointsAbscZ = (s_pointAbscZ_hyd **)calloc(4, sizeof(s_pointAbscZ_hyd *));
  pface->geometry->npts = 4;
  pface->geometry->type = ABSC_Z;

  width = pface->reach->width;
  slope = fabs(pface->reach->limits[ONE]->Z_init - pface->reach->limits[TWO]->Z_init) / pface->reach->length;
  slope_zbot = (Zbot_am - Zbot_av) / pface->reach->length;
  z_LB = pface->reach->limits[ONE]->Z_init - slope * pk;
  z_bot = Zbot_am - slope_zbot * pk;
  h = z_LB - z_bot;
  if (fabs(pface->description->Zbottom - CODE_HYD) < EPS_HYD) // FB 16/04/2018 If Zbottom is not defined in element_musk file, we calculate them by interpolation of upstream and downstream values.
    pface->description->Zbottom = z_bot;
  else                                   // FB 16/04/2018
    z_bot = pface->description->Zbottom; // FB 16/04/2018
#ifdef DEBUG
  if (slope_zbot < 0)
    LP_warning(fp, "face %s Zbot_Am < Zbot_av, Zbot_am %f Zbot_av %f\n", pface->name, Zbot_am, Zbot_av);
#endif
  /*
  if(pface->description->Zbottom!=0)
    {
      LP_printf(fp,"Z_bot\n");
      z_bot=pface->description->Zbottom;
      h=z_LB-pface->description->Zbottom;
    }
  else
    {
      LP_printf(fp,"h_ini\n");
      z_bot=z_LB-pface->new_dx; //BL on a stocké h_ini ici en temporaire;
      h=pface->new_dx; //BL on a stocké h_ini ici en temporaire;
    }
  */
  if (h < -0.01 || z_bot < -100) {
    LP_printf(fp, "face %s z_bot %f, h %f, width %f, slope %f, pk %f,Z_LB %f,Z_am %f,Z_av %f,Zbot_am %f,Zbot_av %f,slope_bot %f\n", pface->name, z_bot, h, width, slope, pk, z_LB, pface->reach->limits[ONE]->Z_init, pface->reach->limits[TWO]->Z_init, Zbot_am, Zbot_av, slope_zbot); // BL to debug
    LP_error(fp, " negative h while initializing hydraulic geometry\n");
  }
#ifdef DEBUG
  else {

    LP_printf(fp, "face %s z_bot %f, h %f, width %f, slope %f, pf %f,Z_LB %f,Z_am %f,Z_av %f,Zbot_am %f,Zbot_av %f,slope_bot %f\n", pface->name, z_bot, h, width, slope, pk, z_LB, pface->reach->limits[ONE]->Z_init, pface->reach->limits[TWO]->Z_init, Zbot_am, Zbot_av, slope_zbot); // BL to debug
  }
#endif
  for (p = 0; p < pface->geometry->npts; p++) {

    pface->geometry->p_pointsAbscZ[p] = new_pointAbscZ();
    bzero((char *)pface->geometry->p_pointsAbscZ[p], sizeof(s_pointAbscZ_hyd));
  }
  pface->geometry->p_pointsAbscZ[0]->absc = 0;
  pface->geometry->p_pointsAbscZ[0]->z = z_LB;
  pface->geometry->p_pointsAbscZ[1]->absc = h / cos(HYD_deg2rad(45.));
  pface->geometry->p_pointsAbscZ[1]->z = z_bot;
  pface->geometry->p_pointsAbscZ[2]->absc = h / cos(HYD_deg2rad(45.)) + (width - 2 * h * tan(HYD_deg2rad(45.)));
  pface->geometry->p_pointsAbscZ[2]->z = z_bot;
  pface->geometry->p_pointsAbscZ[3]->absc = 2 * h / cos(HYD_deg2rad(45.)) + (width - 2 * h * tan(HYD_deg2rad(45.)));
  pface->geometry->p_pointsAbscZ[3]->z = z_LB;
  // pface->new_dx=0;//BL on a stocké h_ini ici en temporaire on l'annule donc;//FB 12/03/18 This line must be removed if we want to initialize
}

void HYD_create_all_musk_geometry(s_face_hyd *pface_tot, FILE *fp) {
  s_face_hyd *pface;
  double pk;
  int id_reach;
  double Zbot_am, Zbot_av;
  pk = 0;
  id_reach = CODE;
  pface = HYD_rewind_faces(pface_tot);
  //  LP_printf(fp,"/******************************* GEOMETRY ********************************/");
  while (pface != NULL) {
    if (id_reach != pface->reach->id[ABS_HYD]) {
      id_reach = pface->reach->id[ABS_HYD];
      pk = 0;

      Zbot_am = pface->description->Zbottom;
      // LP_printf(fp,"face id %s\n",pface->name);
      Zbot_av = HYD_get_Zbot_av(pface, id_reach, fp);
      // LP_printf(fp,"face id %s\n",pface->name);
      // Zbot_av=pface->reach->limits[DOWNSTREAM]->faces[UPSTREAM][0]->description->Zbottom;
      // modif here faire calcul du Z_bot à partir  du Z_bot amont et Z_bot Aval du reach et calculer à partir de la pente !!!
    }

    if (fabs(Zbot_am - CODE_HYD) > EPS_HYD && fabs(Zbot_av - CODE_HYD) > EPS_HYD) {
      HYD_create_musk_geometry(pface, pk, Zbot_am, Zbot_av, fp);
    } else
      LP_error(fp, "The Zbottom is null check your input files (ele_musk) Z_bot_am %f Z_bot_av %f\n", Zbot_am, Zbot_av);
    pk += pface->geometry->dx;
    pface = pface->next;
  }
  // LP_error(fp,"geom end\n");
}
double HYD_get_Zbot_av(s_face_hyd *pface, int id_reach, FILE *fp) {
  // int id_reach_av;
  s_face_hyd *pface_tmp;
  double Zbot;
  pface_tmp = pface;
  while (pface->next != NULL) {
    if (pface->next->reach->id[ABS_HYD] != id_reach) {
      Zbot = pface->description->Zbottom;
      break;
    }
    pface = pface->next;
  }
  if (pface->next == NULL) {
    Zbot = pface->description->Zbottom;
  }
  while (pface->prev != NULL) {

    if (pface->prev->reach->id[ABS_HYD] != id_reach) {

      break;
    }
    pface = pface->prev;
  }
  return (Zbot);
}
void HYD_print_geometry(s_face_hyd *pface_tot, FILE *fp) {

  s_face_hyd *pface;
  int p;
  pface = HYD_rewind_faces(pface_tot);

  while (pface != NULL) {
    LP_printf(fp, "face %d named %s \n", pface->id[ABS_HYD], pface->name);
    for (p = 0; p < pface->geometry->npts; p++) {
      LP_printf(fp, "Geometry abscisse curviligne : %f Z : %f\n", pface->geometry->p_pointsAbscZ[p]->absc, pface->geometry->p_pointsAbscZ[p]->z);
    }
    pface = pface->next;
  }
}

/* Creation of the points describing an XYZ cross-section geometry */
void HYD_create_ptsXYZ(s_face_hyd *pface, s_pointXYZ_hyd *pptXYZ) {
  /* Point nb */
  int p;

  pface->geometry->p_pointsXYZ = (s_pointXYZ_hyd **)calloc(pface->geometry->npts, sizeof(s_pointXYZ_hyd *));

  pptXYZ = HYD_rewind_ptXYZ(pptXYZ);

  for (p = 0; p < pface->geometry->npts; p++) {

    pface->geometry->p_pointsXYZ[p] = pptXYZ;
    pptXYZ = pptXYZ->next;
  }

  /* Calculatio of the Lambert coordinates of the cross-section's center */
  pface->description->xc = pface->geometry->p_pointsXYZ[0]->x + pface->geometry->p_pointsXYZ[pface->geometry->npts - 1]->x;
  pface->description->xc /= 2.;
  pface->description->yc = pface->geometry->p_pointsXYZ[0]->y + pface->geometry->p_pointsXYZ[pface->geometry->npts - 1]->y;
  pface->description->yc /= 2.;
}

/* Creation of a cross-section's point defined by its abscisse and elevation */
s_pointAbscZ_hyd *HYD_create_ptAbscZ(double absc, double z) {
  s_pointAbscZ_hyd *ppt;

  ppt = new_pointAbscZ();
  bzero((s_pointAbscZ_hyd *)ppt, sizeof(s_pointAbscZ_hyd)); // il faut initialiser SW 26/01/2016
  ppt->absc = absc;
  ppt->z = z;
  ppt->next = NULL;
  ppt->prev = NULL;

  return ppt;
}

/* Creation of a cross-section's point defined by its lambert coordinates  */
s_pointXYZ_hyd *HYD_create_ptXYZ(double x, double y, double z) {
  s_pointXYZ_hyd *ppt;

  ppt = new_pointXYZ();

  ppt->x = x;
  ppt->y = y;
  ppt->z = z;
  ppt->next = NULL;
  ppt->prev = NULL;

  return ppt;
}

/* Function to chain points defined by their abscisses and elevations
 * To define a cross-section
 */
s_pointAbscZ_hyd *HYD_chain_ptsAbscZ(s_pointAbscZ_hyd *ppt1, s_pointAbscZ_hyd *ppt2) {
  if ((ppt1 != NULL) && (ppt2 != NULL)) {

    ppt1->next = ppt2;
    ppt2->prev = ppt1;
  }

  return ppt1;
}

/* Function to chain points defined by their lambert coordinates
 * To define a cross-section
 */
s_pointXYZ_hyd *HYD_chain_ptsXYZ(s_pointXYZ_hyd *ppt1, s_pointXYZ_hyd *ppt2) {
  if ((ppt1 != NULL) && (ppt2 != NULL)) {

    ppt1->next = ppt2;
    ppt2->prev = ppt1;
  }

  return ppt1;
}

/* Function to point towards the first or last point of an AbscZ cross-section */
s_pointAbscZ_hyd *HYD_rewind_ptAbscZ(s_pointAbscZ_hyd *ppt, int dir) {
  s_pointAbscZ_hyd *ppt_temp;

  ppt_temp = new_pointAbscZ();
  ppt_temp = ppt;

  if (dir == BEGINNING) {

    while (ppt_temp->prev != NULL)
      ppt_temp = ppt_temp->prev;
  }

  else if (dir == END) {

    while (ppt_temp->next != NULL)
      ppt_temp = ppt_temp->next;
  }

  return ppt_temp;
}

/* Function to rewind a chain of points defined by their lambert coordinates */
s_pointXYZ_hyd *HYD_rewind_ptXYZ(s_pointXYZ_hyd *ppt) {
  s_pointXYZ_hyd *ppt_temp;

  ppt_temp = new_pointXYZ();
  ppt_temp = ppt;

  while (ppt_temp->prev != NULL)
    ppt_temp = ppt_temp->prev;

  return ppt_temp;
}

/* Calculates the abscisse of the AbscZ points when the geometry is given as XYZ points */
void HYD_calculate_AbscZ_pts(s_geometry_hyd *pgeom) {
  int p;
  double absc;
  double L;

  pgeom->p_pointsAbscZ = (s_pointAbscZ_hyd **)calloc(pgeom->npts, sizeof(s_pointAbscZ_hyd *));

  L = sqrt(pow(pgeom->p_pointsXYZ[pgeom->npts - 1]->x - pgeom->p_pointsXYZ[0]->x, 2.) + pow(pgeom->p_pointsXYZ[pgeom->npts - 1]->y - pgeom->p_pointsXYZ[0]->y, 2.));
  pgeom->p_pointsAbscZ[0] = HYD_create_ptAbscZ(-L / 2., pgeom->p_pointsXYZ[pgeom->npts - 1]->z); // LV 05/04/2012

  for (p = pgeom->npts - 2; p >= 0; p--) {

    absc = -L / 2. + sqrt(pow(pgeom->p_pointsXYZ[p]->x - pgeom->p_pointsXYZ[pgeom->npts - 1]->x, 2) + pow(pgeom->p_pointsXYZ[p]->y - pgeom->p_pointsXYZ[pgeom->npts - 1]->y, 2));
    pgeom->p_pointsAbscZ[pgeom->npts - 1 - p] = HYD_create_ptAbscZ(absc, pgeom->p_pointsXYZ[p]->z);
  }
}

/* Creation of a description structure (elements' centers and faces) */
s_description_hyd *HYD_create_description(s_chyd *pchyd) {
  s_description_hyd *pdesc;

  pdesc = new_description();
  bzero((char *)pdesc, sizeof(s_description_hyd));

  pdesc->curvature = pchyd->settings->general_param[CURVATURE];
  if (pchyd->settings->general_param[CURVATURE] > 0.)
    pdesc->radius = 1. / pchyd->settings->general_param[CURVATURE];
  else
    pdesc->radius = CODE;

  return pdesc;
}

/* Calculation of the descriptive characteristics of an interpolated face */
void HYD_description_interpolated_face(s_face_hyd *pface, s_face_hyd *pface_ndef) {
  pface->description->Abscbottom = pface->prev->description->Abscbottom * pface->new_dx;
  pface->description->Abscbottom += pface_ndef->description->Abscbottom * pface->prev->new_dx;
  pface->description->Abscbottom /= pface->new_dx + pface->prev->new_dx;
  pface->description->Zbottom = pface->prev->description->Zbottom * pface->new_dx;
  pface->description->Zbottom += pface_ndef->description->Zbottom * pface->prev->new_dx;
  pface->description->Zbottom /= pface->new_dx + pface->prev->new_dx;
  pface->description->AbscLB = pface->prev->description->AbscLB * pface->new_dx;
  pface->description->AbscLB += pface_ndef->description->AbscLB * pface->prev->new_dx;
  pface->description->AbscLB /= pface->new_dx + pface->prev->new_dx;
  pface->description->AbscRB = pface->prev->description->AbscRB * pface->new_dx;
  pface->description->AbscRB += pface_ndef->description->AbscRB * pface->prev->new_dx;
  pface->description->AbscRB /= pface->new_dx + pface->prev->new_dx;
  pface->description->xc = pface->prev->description->xc * pface->new_dx;
  pface->description->xc += pface_ndef->description->xc * pface->prev->new_dx;
  pface->description->xc /= pface->new_dx + pface->prev->new_dx;
  pface->description->yc = pface->prev->description->yc * pface->new_dx;
  pface->description->yc += pface_ndef->description->yc * pface->prev->new_dx;
  pface->description->yc /= pface->new_dx + pface->prev->new_dx;
  pface->description->curvature = pface->prev->description->curvature * pface->new_dx;
  pface->description->curvature += pface_ndef->description->curvature * pface->prev->new_dx;
  pface->description->curvature /= pface->new_dx + pface->prev->new_dx;
}

void HYD_calculate_faces_traj(s_face_hyd *pfaces, s_chyd *pchyd) {
  int f, i;
  s_face_hyd *pface;
  double norme;
  double xT, yT;
  double xN, yN;
  double xLB, yLB;
  double xRB, yRB;
  double dx;
  s_description_hyd *pdesc, *pdescp, *pdescn;

  pfaces = HYD_rewind_faces(pfaces);

  pchyd->p_reach[0]->p_faces = ((s_face_hyd **)calloc(pchyd->p_reach[0]->nfaces, sizeof(s_face_hyd *)));
  f = 0;

  while (pfaces != NULL) {

    pface = pchyd->p_reach[0]->p_faces[f] = pfaces;
    pface->geometry->type = X_Y_Z;
    pface->geometry->npts = 4;
    pface->geometry->p_pointsXYZ = ((s_pointXYZ_hyd **)calloc(4, sizeof(s_pointXYZ_hyd *)));

    for (i = 0; i < 4; i++)
      pface->geometry->p_pointsXYZ[i] = new_pointXYZ();

    pdesc = pface->description;

    if (f == 0) {
      pdescn = pface->next->description;
      norme = sqrt(pow(pdescn->yc - pdesc->yc, 2.) + pow(pdescn->xc - pdesc->xc, 2.));
      xT = pdescn->xc - pdesc->xc;
      xT /= norme;
      yT = pdescn->yc - pdesc->yc;
      yT /= norme;
      xN = -yT;
      yN = xT;

      xLB = pdesc->xc + pdesc->l * xN / 2.;
      yLB = pdesc->yc + pdesc->l * yN / 2.;
      xRB = pdesc->xc - pdesc->l * xN / 2.;
      yRB = pdesc->yc - pdesc->l * yN / 2.;
      dx = sqrt(pow(pdescn->xc - pdesc->xc, 2.) + pow(pdescn->yc - pdesc->yc, 2.));
    }

    else if (f == pchyd->p_reach[0]->nfaces - 1) {
      pdescp = pface->prev->description;
      norme = sqrt(pow(pdesc->yc - pdescp->yc, 2.) + pow(pdesc->xc - pdescp->xc, 2.));
      xT = pdesc->xc - pdescp->xc;
      xT /= norme;
      yT = pdesc->yc - pdescp->yc;
      yT /= norme;
      xN = -yT;
      yN = xT;

      xLB = pdesc->xc + pdesc->l * xN / 2.;
      yLB = pdesc->yc + pdesc->l * yN / 2.;
      xRB = pdesc->xc - pdesc->l * xN / 2.;
      yRB = pdesc->yc - pdesc->l * yN / 2.;
      dx = 0.;
    }

    else {
      pdescp = pface->prev->description;
      pdescn = pface->next->description;
      norme = sqrt(pow(pdescn->yc - pdescp->yc, 2.) + pow(pdescn->xc - pdescp->xc, 2.));
      xT = pdescn->xc - pdescp->xc;
      xT /= norme;
      yT = pdescn->yc - pdescp->yc;
      yT /= norme;
      xN = -yT;
      yN = xT;

      xLB = pdesc->xc + pdesc->l * xN / 2.;
      yLB = pdesc->yc + pdesc->l * yN / 2.;
      xRB = pdesc->xc - pdesc->l * xN / 2.;
      yRB = pdesc->yc - pdesc->l * yN / 2.;
      dx = sqrt(pow(pdescn->xc - pdesc->xc, 2.) + pow(pdescn->yc - pdesc->yc, 2.));
    }

    pface->geometry->p_pointsXYZ[0]->x = pface->geometry->p_pointsXYZ[1]->x = xLB;
    pface->geometry->p_pointsXYZ[2]->x = pface->geometry->p_pointsXYZ[3]->x = xRB;
    pface->geometry->p_pointsXYZ[0]->y = pface->geometry->p_pointsXYZ[1]->y = yLB;
    pface->geometry->p_pointsXYZ[2]->y = pface->geometry->p_pointsXYZ[3]->y = yRB;
    pface->geometry->p_pointsXYZ[0]->z = pface->geometry->p_pointsXYZ[3]->z = pdesc->Zbottom + 10.;
    pface->geometry->p_pointsXYZ[1]->z = pface->geometry->p_pointsXYZ[2]->z = pdesc->Zbottom;
    pface->geometry->dx = dx;
    pface->reach->length += dx;

    HYD_calculate_AbscZ_pts(pface->geometry);

    pfaces = pfaces->next;
    f++;
  }
}
