/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_hydro.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"
/* Structure containing the simulation caracteristics */
// extern s_simul *Simul;

/* Initialization of a <<hydro>> structure */
s_hydro_hyd *HYD_create_hydro(int schem_type) {
  s_hydro_hyd *phyd;
  int i;
  int j;
  phyd = new_hydraulics();
  bzero((char *)phyd, sizeof(s_hydro_hyd));

  //  ATTENTION JE NE PENSE PAS QU'IL FAILLE ALOUER LA MEMOIRE GARE AU MEMORY LEAK
  phyd->surf = new_function();
  bzero((char *)phyd->surf, sizeof(s_ft));
  phyd->peri = new_function();
  bzero((char *)phyd->peri, sizeof(s_ft));
  phyd->width = new_function();
  bzero((char *)phyd->width, sizeof(s_ft));
  phyd->hydrad = new_function();
  bzero((char *)phyd->hydrad, sizeof(s_ft));
  /*
    BUG SI FAIT CA
  phyd->surf = NULL;
  phyd->peri = NULL;
  phyd->width = NULL;
  phyd->hydrad = NULL;
  */
  if (schem_type == MUSKINGUM) {
    phyd->pmusk = HYD_create_musk_set();
  }
  for (i = 0; i < NQAPP; i++) {
    for (j = 0; j < NSTEP_HYD; j++) {
      phyd->qapp[i][j] = 0;
    }
  }
  phyd->transp_var = NULL;     // Either concentration and/or temperature
  phyd->param_riverbed = NULL; // Either riverbed lambda or thickness;
  return phyd;
}

/* Calculation of the wet surface and perimeter and of the surface width between
 * two local maxima
 */
void HYD_surf_peri_width(s_face_hyd *pface, double z, int nz, int max1, int max2) {
  /* Point nb */
  int i;
  /* Left and right abscisses of the water surface */
  double xg, xd;
  /* Nb of the left and right points on both sides of the water surface */
  int pl, pr;
  /* Integer defining wether or not pl and pr are set */
  int pl_defined = 0, pr_defined = 0;

  // if (z > 0.) {//LV 4/09/2012 : la cote peut être négative alors que la hauteur d'eau est positive !!!

  /* Search of the first point on the left bank that is not wet */
  i = max1;

  if (pface->geometry->p_pointsAbscZ[max1]->z < z) {
    pl = max1;
    pl_defined = 1;
  }

  while (pl_defined == 0) {
    if ((pface->geometry->p_pointsAbscZ[i]->z >= z) && (pface->geometry->p_pointsAbscZ[i + 1]->z <= z)) {
      pl = i;
      pl_defined = 1;
    } else
      i++;

    if (i == pface->geometry->npts - 1) // LV 06/06/2012
      pl = CODE, pl_defined = YES;
  }

  /* Search of the first point on the right bank that is not wet */
  i = max2;

  if (pl != CODE) { // LV 06/06/2012 (s'il ya de l'eau dans la section entre max1 et max2...

    if (pface->geometry->p_pointsAbscZ[max2]->z < z) {
      pr = max2;
      pr_defined = 1;
    }

    while (pr_defined == 0) {
      if ((pface->geometry->p_pointsAbscZ[i]->z >= z) && (pface->geometry->p_pointsAbscZ[i - 1]->z <= z)) {
        // if (((pface->geometry->p_pointsAbscZ[i]->z >= z) && (pface->geometry->p_pointsAbscZ[i-1]->z <= z)) || (i == 1)) {//LV nov2014
        pr = i;
        pr_defined = 1;
      } else
        i--;

      if (i == 0) // LV nov2014
        pr = CODE, pr_defined = YES;
    }

    /* Calculation of the hydro table */
    for (i = pl + 1; i <= pr; i++) {

      /* First surface interval = triangle */
      if (i == pl + 1) {
        xg = TS_interpol(pface->geometry->p_pointsAbscZ[pl]->absc, pface->geometry->p_pointsAbscZ[pl]->z, pface->geometry->p_pointsAbscZ[pl + 1]->absc, pface->geometry->p_pointsAbscZ[pl + 1]->z, z);
        pface->hydro->width->ft += pface->geometry->p_pointsAbscZ[pl + 1]->absc - xg;
        pface->hydro->surf->ft += (z - pface->geometry->p_pointsAbscZ[pl + 1]->z) * (pface->geometry->p_pointsAbscZ[pl + 1]->absc - xg) / 2.;
        pface->hydro->peri->ft += sqrt(pow((z - pface->geometry->p_pointsAbscZ[pl + 1]->z), 2.) + pow((pface->geometry->p_pointsAbscZ[pl + 1]->absc - xg), 2.));
      }

      /* Last surface interval = triangle */
      else if (i == pr) {
        xd = TS_interpol(pface->geometry->p_pointsAbscZ[pr - 1]->absc, pface->geometry->p_pointsAbscZ[pr - 1]->z, pface->geometry->p_pointsAbscZ[pr]->absc, pface->geometry->p_pointsAbscZ[pr]->z, z);
        pface->hydro->width->ft += xd - pface->geometry->p_pointsAbscZ[pr - 1]->absc;
        pface->hydro->surf->ft += (z - pface->geometry->p_pointsAbscZ[pr - 1]->z) * (xd - pface->geometry->p_pointsAbscZ[pr - 1]->absc) / 2.;
        pface->hydro->peri->ft += sqrt(pow((z - pface->geometry->p_pointsAbscZ[pr - 1]->z), 2.) + pow((xd - pface->geometry->p_pointsAbscZ[pr - 1]->absc), 2.));
      }

      /* Other intervals = trapeze */
      else {
        pface->hydro->width->ft += pface->geometry->p_pointsAbscZ[i]->absc - pface->geometry->p_pointsAbscZ[i - 1]->absc;
        pface->hydro->surf->ft += fabs((pface->geometry->p_pointsAbscZ[i]->absc - pface->geometry->p_pointsAbscZ[i - 1]->absc) * ((z - pface->geometry->p_pointsAbscZ[i]->z) + (z - pface->geometry->p_pointsAbscZ[i - 1]->z)) / 2.);
        pface->hydro->peri->ft += sqrt(pow((pface->geometry->p_pointsAbscZ[i]->z - pface->geometry->p_pointsAbscZ[i - 1]->z), 2.) + pow((pface->geometry->p_pointsAbscZ[i]->absc - pface->geometry->p_pointsAbscZ[i - 1]->absc), 2.));
      }
    }
  }
  //}
}

/* Launch of the calculation of the hydro table
 * for each altitude z, the function finds the local maxima between which the characteristics must be calculated
 */
void HYD_calculate_hydro_table(s_face_hyd *pface, s_chyd *pchyd, s_chronos_CHR *chronos, FILE *fp) {
  /* Loop index : vertical discretization */
  int i;
  /* Altitude of the water */
  double z;
  /* Water height in the section */
  double h;
  /* Nb of the bottom point */
  int loc_bottom;
  /* Current local minimum, left maximum and right maximum */
  int min_temp, max1_temp, max2_temp;

  double dx;

  loc_bottom = pface->geometry->bottom;

  for (i = 0; i < pchyd->settings->ndz; i++) {

    z = pface->geometry->p_pointsAbscZ[loc_bottom]->z + pchyd->settings->general_param[UP_HMIN] + i * pchyd->settings->general_param[DZ];
    h = pchyd->settings->general_param[UP_HMIN] + i * pchyd->settings->general_param[DZ];

    if (i == 0) {
      // pface->hydro->surf=TS_create_function(h,0.);
      // pface->hydro->peri=TS_create_function(h,0.);
      // pface->hydro->hydrad=TS_create_function(h,0.);
      // pface->hydro->width =TS_create_function(h,0.);
      pface->hydro->surf->t = pface->hydro->peri->t = pface->hydro->hydrad->t = pface->hydro->width->t = h;
      pface->hydro->surf->ft = pface->hydro->peri->ft = pface->hydro->hydrad->ft = pface->hydro->width->ft = 0.;
      pface->hydro->surf->prev = pface->hydro->peri->prev = pface->hydro->hydrad->prev = pface->hydro->width->prev = NULL;
    }

    else {
      pface->hydro->surf->next = TS_create_function(h, 0.);
      pface->hydro->surf->next->prev = pface->hydro->surf;
      pface->hydro->surf = pface->hydro->surf->next;
      pface->hydro->peri->next = TS_create_function(h, 0.);
      pface->hydro->peri->next->prev = pface->hydro->peri;
      pface->hydro->peri = pface->hydro->peri->next;
      pface->hydro->hydrad->next = TS_create_function(h, 0.);
      pface->hydro->hydrad->next->prev = pface->hydro->hydrad;
      pface->hydro->hydrad = pface->hydro->hydrad->next;
      pface->hydro->width->next = TS_create_function(h, 0.);
      pface->hydro->width->next->prev = pface->hydro->width;
      pface->hydro->width = pface->hydro->width->next;
    }

    min_temp = 0;
    max1_temp = 0;
    max2_temp = 1;

    if (h > 0.) {
      /* If only two maxima the characteristics are calculated between thee two points */
      if (pface->geometry->nmax == 2) {
        HYD_surf_peri_width(pface, z, i, pface->geometry->max[0], pface->geometry->max[1]);
        if (pface->hydro->peri->ft > 0.)
          pface->hydro->hydrad->ft = pface->hydro->surf->ft / pface->hydro->peri->ft;
      }

      else {

        while (min_temp < pface->geometry->nmin) {

          /* If the altitude of the current minimum is not reached by the water
           * the characteristics are calculated between the next two maxima
           */
          if (pface->geometry->p_pointsAbscZ[pface->geometry->min[min_temp]]->z > z) {
            min_temp++;
            max1_temp++;
            max2_temp++;

          }

          else if ((pface->geometry->p_pointsAbscZ[pface->geometry->max[max2_temp]]->z > z) || (max2_temp == pface->geometry->nmax - 1)) {
            // LP_printf(fp,"min_temp = %d, max1_temp = %d, max2_temp = %d\n",min_temp,max1_temp,max2_temp);
            HYD_surf_peri_width(pface, z, i, pface->geometry->max[max1_temp], pface->geometry->max[max2_temp]);
            min_temp++;
            max1_temp = max2_temp;
            max2_temp++;
          }

          else {
            min_temp++;
            max2_temp++;
          }
        }
        if (pface->hydro->peri->ft > 0.)
          pface->hydro->hydrad->ft = pface->hydro->surf->ft / pface->hydro->peri->ft;
      }
    }
  }

  if (pchyd->settings->schem_type == MUSKINGUM) {

    if (pface->reach->pmusk->param[ALPHA] > 0)
      pface->hydro->pmusk->param[ALPHA] = pface->reach->pmusk->param[ALPHA];
    else
      pface->hydro->pmusk->param[ALPHA] = DEFAULT_ALPHA;

    if (pface->reach->pmusk->calc_type == CST_MUSK) {
      if (pface->prev != NULL && pface->prev->reach->id[INTERN_HYD] == pface->reach->id[INTERN_HYD]) {
        dx = pface->prev->geometry->dx;
        pface->hydro->pmusk->param[K] = (dx / pface->reach->length) * pface->reach->pmusk->param[K];
      }

    } else
      pface->hydro->pmusk->param[K] = HYD_calc_k_face(pface, fp);

  }

  else
    // pface->hydro->ks = TS_function_value_t(chronos->t[BEGINNING],pface->reach->strickler,fp);
    pface->hydro->ks = TS_function_value_t(pface->hydro->Q[T_HYD], pface->reach->strickler, fp); // SW 24/01/2018 strickler is a time_series with Q
  pface->hydro->surf->next = NULL;
  pface->hydro->peri->next = NULL;
  pface->hydro->hydrad->next = NULL;
  pface->hydro->width->next = NULL;
  // pface->hydro->ks = pface->reach->strickler;
  // HYD_print_hydro_table(pface->hydro); // SW 30/01/2018 debug
  // if((pchyd->counter->nele_tot + pface->id[ABS_HYD]) == 8656) // SW 30/01/2018 debug
  // LP_printf(fp,"id = %d name = %s\n",pface->id[ABS_HYD],pface->name);
}

/* Function finding the local maxima and minima in a geometry structure and launching calculate_hydro_table(...) */
void HYD_table_hydro_faces(s_face_hyd *pface, s_chyd *pchyd, s_chronos_CHR *chronos, FILE *fp) {
  /* Point nb */
  int i;
  /* Position of the bottom of the cross-section */
  int bottom_temp = 0;
  /* Counters of the number of local maxima and minima in a cross-section*/
  int nmin_temp = 0, nmax_temp = 0;

  /* Number of vertical steps for the calculation of the hydro tables */
  pchyd->settings->ndz = (int)((pchyd->settings->general_param[DOWN_HMAX] - pchyd->settings->general_param[UP_HMIN]) / pchyd->settings->general_param[DZ]) + 1;

  pface = HYD_rewind_faces(pface);

  nmin_temp = nmax_temp = bottom_temp = 0;

  pface->hydro = HYD_create_hydro(pchyd->settings->schem_type);

  if (pchyd->settings->schem_type == MUSKINGUM) // FB 12/03/18 Hydraulic heads need initialization
    for (i = 0; i < NSTEP_HYD; i++) {
      pface->hydro->Z[i] = pface->new_dx;
      pface->hydro->H[i] = pface->hydro->Z[i] - pface->description->Zbottom;
    }

  /* Prints an error if the first point of the cross-section is lower than the second
   * (may cause some problems if the level of the water is higher than the second point)
   */
  if (pface->geometry->p_pointsAbscZ[0]->z < pface->geometry->p_pointsAbscZ[1]->z) {
    LP_printf(fp, "Warning : First point in cross-section %s branch %s %s %d too low\n", pface->name, pface->reach->limits[ONE]->name, pface->reach->limits[TWO]->name, pface->reach->branch_nb);

  }

  else {
    pface->geometry->nmax = 1;
    pface->geometry->p_pointsAbscZ[0]->pt_type = MAX;
  }

  /* Prints an error if the last point of the cross-section is lower than the previous
   * (may cause some problems if the level of the water is higher than the penultimate point)
   */
  if (pface->geometry->p_pointsAbscZ[pface->geometry->npts - 1]->z < pface->geometry->p_pointsAbscZ[pface->geometry->npts - 2]->z) {
    LP_printf(fp, "Warning : Last point in cross-section %s branch %s %s %d too low\n", pface->name, pface->reach->limits[ONE]->name, pface->reach->limits[TWO]->name, pface->reach->branch_nb);

  }

  else {
    pface->geometry->nmax = 2;
    pface->geometry->p_pointsAbscZ[pface->geometry->npts - 1]->pt_type = MAX;
  }
  pface->geometry->nmin = 0; // SW 26/01/2018 il faut initialiser
  for (i = 1; i < pface->geometry->npts; i++) {
    /* Search of the local minima */
    if (pface->geometry->nmax == pface->geometry->nmin + 2) {
      if (pface->geometry->p_pointsAbscZ[i]->z > pface->geometry->p_pointsAbscZ[i - 1]->z) {
        pface->geometry->nmin++;
        pface->geometry->p_pointsAbscZ[i - 1]->pt_type = MIN;
        // if(strcmp(pface->name,"Profil034.200")==0) // SW debug
        // LP_printf(fp,"pface->name = %s i = %d type \n",pface->name,i);
      }
    }

    /* Search of the local maxima */
    else {
      if (pface->geometry->p_pointsAbscZ[i]->z < pface->geometry->p_pointsAbscZ[i - 1]->z) {
        pface->geometry->nmax++;
        pface->geometry->p_pointsAbscZ[i - 1]->pt_type = MAX;
      }
    }
  }

  pface->geometry->min = (int *)calloc(pface->geometry->nmin, sizeof(int));
  pface->geometry->max = (int *)calloc(pface->geometry->nmax, sizeof(int));

  /* Filling of the min and max tables of the geometry structure
   * (positions of the local extrema in the given points of the cross-section)
   */
  for (i = 0; i < pface->geometry->npts; i++) {
    if (pface->geometry->p_pointsAbscZ[i]->pt_type == MAX) {
      pface->geometry->max[nmax_temp] = i;
      nmax_temp++;
    }
    if (pface->geometry->p_pointsAbscZ[i]->pt_type == MIN) {
      pface->geometry->min[nmin_temp] = i;
      nmin_temp++;
      if (pface->geometry->p_pointsAbscZ[i]->z < pface->geometry->p_pointsAbscZ[bottom_temp]->z)
        bottom_temp = i;
    }
  }

  pface->geometry->bottom = bottom_temp;
  pface->description->Abscbottom = pface->geometry->p_pointsAbscZ[bottom_temp]->absc;
  pface->description->Zbottom = pface->geometry->p_pointsAbscZ[bottom_temp]->z;
  pface->description->AbscLB = pface->geometry->p_pointsAbscZ[pface->geometry->npts - 1]->absc;
  pface->description->AbscRB = pface->geometry->p_pointsAbscZ[0]->absc;

  HYD_calculate_hydro_table(pface, pchyd, chronos, fp);

  while (pface->next != NULL) {
    pface = pface->next;

    nmin_temp = nmax_temp = bottom_temp = 0;

    pface->hydro = HYD_create_hydro(pchyd->settings->schem_type);

    if (pchyd->settings->schem_type == MUSKINGUM) // FB 12/03/18 Hydraulic heads need initialization
      for (i = 0; i < NSTEP_HYD; i++) {
        pface->hydro->Z[i] = pface->new_dx;
        pface->hydro->H[i] = pface->hydro->Z[i] - pface->description->Zbottom;
      }
    /* Prints an error if the first point of the cross-section is lower than the second
     * (may cause some problems if the level of the water is higher than the second point)
     */
    if (pface->geometry->p_pointsAbscZ[0]->z < pface->geometry->p_pointsAbscZ[1]->z) {
      LP_printf(fp, "Warning : First point in cross-section %s branch %s %s %d too low\n", pface->name, pface->reach->limits[ONE]->name, pface->reach->limits[TWO]->name, pface->reach->branch_nb);

    }

    else {
      pface->geometry->nmax = 1;
      pface->geometry->p_pointsAbscZ[0]->pt_type = MAX;
    }

    /* Prints an error if the last point of the cross-section is lower than the previous
     * (may cause some problems if the level of the water is higher than the penultimate point)
     */
    if (pface->geometry->p_pointsAbscZ[pface->geometry->npts - 1]->z < pface->geometry->p_pointsAbscZ[pface->geometry->npts - 2]->z) {
      LP_printf(fp, "Warning : Last point in cross-section %s branch %s %s %d too low\n", pface->name, pface->reach->limits[ONE]->name, pface->reach->limits[TWO]->name, pface->reach->branch_nb);

    }

    else {
      pface->geometry->nmax = 2;
      pface->geometry->p_pointsAbscZ[pface->geometry->npts - 1]->pt_type = MAX;
    }
    pface->geometry->nmin = 0; // SW 26/01/2018 il faut initialiser
    for (i = 1; i < pface->geometry->npts; i++) {
      /* Search of the local minima */
      if (pface->geometry->nmax == pface->geometry->nmin + 2) {
        if (pface->geometry->p_pointsAbscZ[i]->z > pface->geometry->p_pointsAbscZ[i - 1]->z) {
          pface->geometry->nmin++;
          pface->geometry->p_pointsAbscZ[i - 1]->pt_type = MIN;
          // if(strcmp(pface->name,"Profil034.200")==0) // SW debug
          // LP_printf(fp,"pface->name = %s i = %d nmin = %d typei2 = %d\n",pface->name,i,pface->geometry->nmin,pface->geometry->p_pointsAbscZ[2]->pt_type);
        }
      }

      /* Search of the local maxima */
      else {
        if (pface->geometry->p_pointsAbscZ[i]->z < pface->geometry->p_pointsAbscZ[i - 1]->z) {
          pface->geometry->nmax++;
          pface->geometry->p_pointsAbscZ[i - 1]->pt_type = MAX;
        }
      }
    }

    pface->geometry->min = (int *)calloc(pface->geometry->nmin, sizeof(int));
    pface->geometry->max = (int *)calloc(pface->geometry->nmax, sizeof(int));

    /* Filling of the min and max tables of the geometry structure
     * (positions of the local extrema in the given points of the cross-section)
     */
    for (i = 0; i < pface->geometry->npts; i++) {
      if (pface->geometry->p_pointsAbscZ[i]->pt_type == MAX) {
        pface->geometry->max[nmax_temp] = i;
        nmax_temp++;
      }
      if (pface->geometry->p_pointsAbscZ[i]->pt_type == MIN) {
        pface->geometry->min[nmin_temp] = i;
        nmin_temp++;
        if (pface->geometry->p_pointsAbscZ[i]->z < pface->geometry->p_pointsAbscZ[bottom_temp]->z)
          bottom_temp = i;
      }
    }

    pface->geometry->bottom = bottom_temp;
    pface->description->Abscbottom = pface->geometry->p_pointsAbscZ[bottom_temp]->absc;
    pface->description->Zbottom = pface->geometry->p_pointsAbscZ[bottom_temp]->z;
    pface->description->AbscLB = pface->geometry->p_pointsAbscZ[pface->geometry->npts - 1]->absc;
    pface->description->AbscRB = pface->geometry->p_pointsAbscZ[0]->absc;

    HYD_calculate_hydro_table(pface, pchyd, chronos, fp);
  }
}

/* Calculation of the hydro tables of the interpolated faces
 * when user asks for a longitudinal discretization
 */
void HYD_table_hydro_interpolated_face(s_face_hyd *pface, s_face_hyd *pface_pdef, s_face_hyd *pface_ndef, int schem_type, s_chronos_CHR *chronos, FILE *fp) {
  double dx;

  pface->hydro = HYD_create_hydro(schem_type);

  pface->hydro->surf = TS_add_functions(TS_multiply_ft_double(pface->prev->hydro->surf, pface->new_dx), TS_multiply_ft_double(pface_ndef->hydro->surf, pface->prev->new_dx));
  pface->hydro->surf = TS_division_ft_double(pface->hydro->surf, pface->new_dx + pface->prev->new_dx, fp);

  pface->hydro->hydrad = TS_add_functions(TS_multiply_ft_double(pface->prev->hydro->hydrad, pface->new_dx), // SW 10/10/2018 replace pface_pdef by  pface->prev, we use pface->prev->new_dx
                                          TS_multiply_ft_double(pface_ndef->hydro->hydrad, pface->prev->new_dx));
  pface->hydro->hydrad = TS_division_ft_double(pface->hydro->hydrad, pface->new_dx + pface->prev->new_dx, fp);

  pface->hydro->peri = TS_divide_fts(pface->hydro->surf, pface->hydro->hydrad);

  pface->hydro->peri = TS_browse_ft(pface->hydro->peri, BEGINNING_TS); // SW 23/10/2018 for surf = 0, hydrad = 0, so peri = 0.
  if (pface->hydro->peri->ft == TS_INFINITE)
    pface->hydro->peri->ft = 0.;

  pface->hydro->width = TS_add_functions(TS_multiply_ft_double(pface->prev->hydro->width, pface->new_dx), // SW 10/10/2018 replace pface_pdef by  pface->prev, we use pface->prev->new_dx
                                         TS_multiply_ft_double(pface_ndef->hydro->width, pface->prev->new_dx));
  pface->hydro->width = TS_division_ft_double(pface->hydro->width, pface->new_dx + pface->prev->new_dx, fp);

  if (schem_type == MUSKINGUM) {

    if (pface->reach->pmusk->param[ALPHA] > 0)
      pface->hydro->pmusk->param[ALPHA] = pface->reach->pmusk->param[ALPHA];
    else
      pface->hydro->pmusk->param[ALPHA] = DEFAULT_ALPHA;

    if (pface->reach->pmusk->calc_type == CST_MUSK) {
      if (pface->prev != NULL && pface->prev->reach->id[INTERN_HYD] == pface->reach->id[INTERN_HYD]) {
        dx = pface->prev->geometry->dx;
        pface->hydro->pmusk->param[K] = (dx / pface->reach->length) * pface->reach->pmusk->param[K];
      }

    } else
      pface->hydro->pmusk->param[K] = HYD_calc_k_face(pface, fp);

  }

  else
    // pface->hydro->ks = TS_function_value_t(chronos->t[BEGINNING],pface->reach->strickler,fp);
    pface->hydro->ks = TS_function_value_t(pface->hydro->Q[T_HYD], pface->reach->strickler, fp); // SW 29/01/2019 strickler is a function with Q
}

/* Calculation of the hydro tables of the interpolated faces
 * when user asks for a longitudinal discretization
 */
void HYD_table_hydro_interpolated_face_ancien(s_face_hyd *pface, s_face_hyd *pface_pdef, s_face_hyd *pface_ndef, int schem_type, s_chronos_CHR *chronos, FILE *fp) {
  double dx;

  pface->hydro = HYD_create_hydro(schem_type);

  pface->hydro->surf = TS_add_functions(TS_multiply_ft_double(pface->prev->hydro->surf, pface->new_dx), TS_multiply_ft_double(pface_ndef->hydro->surf, pface->prev->new_dx));
  pface->hydro->surf = TS_division_ft_double(pface->hydro->surf, pface->new_dx + pface->prev->new_dx, fp);

  pface->hydro->hydrad = TS_add_functions(TS_multiply_ft_double(pface_pdef->hydro->hydrad, pface->new_dx), // SW 10/10/2018 replace pface_pdef by  pface->prev, we use pface->prev->new_dx
                                          TS_multiply_ft_double(pface_ndef->hydro->hydrad, pface->prev->new_dx));
  pface->hydro->hydrad = TS_division_ft_double(pface->hydro->hydrad, pface->new_dx + pface->prev->new_dx, fp);

  pface->hydro->peri = TS_divide_fts(pface->hydro->surf, pface->hydro->hydrad);

  pface->hydro->peri = TS_browse_ft(pface->hydro->peri, BEGINNING_TS); // SW 23/10/2018 for surf = 0, hydrad = 0, so peri = 0.
  if (pface->hydro->peri->ft == TS_INFINITE)
    pface->hydro->peri->ft = 0.;

  pface->hydro->width = TS_add_functions(TS_multiply_ft_double(pface_pdef->hydro->width, pface->new_dx), // SW 10/10/2018 replace pface_pdef by  pface->prev, we use pface->prev->new_dx
                                         TS_multiply_ft_double(pface_ndef->hydro->width, pface->prev->new_dx));
  pface->hydro->width = TS_division_ft_double(pface->hydro->width, pface->new_dx + pface->prev->new_dx, fp);

  if (schem_type == MUSKINGUM) {

    if (pface->reach->pmusk->param[ALPHA] > 0)
      pface->hydro->pmusk->param[ALPHA] = pface->reach->pmusk->param[ALPHA];
    else
      pface->hydro->pmusk->param[ALPHA] = DEFAULT_ALPHA;

    if (pface->reach->pmusk->calc_type == CST_MUSK) {
      if (pface->prev != NULL && pface->prev->reach->id[INTERN_HYD] == pface->reach->id[INTERN_HYD]) {
        dx = pface->prev->geometry->dx;
        pface->hydro->pmusk->param[K] = (dx / pface->reach->length) * pface->reach->pmusk->param[K];
      }

    } else
      pface->hydro->pmusk->param[K] = HYD_calc_k_face(pface, fp);

  }

  else
    // pface->hydro->ks = TS_function_value_t(chronos->t[BEGINNING],pface->reach->strickler,fp);
    pface->hydro->ks = TS_function_value_t(pface->hydro->Q[T_HYD], pface->reach->strickler, fp); // SW 29/01/2019 strickler is a function with Q
}
/* Calculation of the hydro table of the center of an element
 * The geometry of the center is unknown
 * The hydraulic characteristics are obtained by calculating :
 *   - the mean of the characteristics of the two adjacent faces for the surface, hydraulic radius, width
 *     and center of the cross-section
 *   - surf / hydrad for the perimeter
 */
void HYD_table_hydro_elements(s_chyd *pchyd, s_chronos_CHR *chronos, FILE *fp) {
  /* Reach, element nb */
  int r, e, i;
  double Q_one, Q_two, Q_mean; // SW 29/01/2018 pour strickler
  s_hydro_hyd *phydro;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {
      if (pchyd->p_reach[r]->p_ele[e]->id[ABS_HYD] == 421)
        printf("id = 421");
      pchyd->p_reach[r]->p_ele[e]->center->hydro = HYD_create_hydro(pchyd->settings->schem_type);
      if (pchyd->p_reach[r]->p_ele[e]->center->hydro->surf != NULL)
        pchyd->p_reach[r]->p_ele[e]->center->hydro->surf = TS_free_ts(pchyd->p_reach[r]->p_ele[e]->center->hydro->surf, fp);
      pchyd->p_reach[r]->p_ele[e]->center->hydro->surf = TS_mean_fts(pchyd->p_reach[r]->p_ele[e]->face[X_HYD][ONE]->hydro->surf, pchyd->p_reach[r]->p_ele[e]->face[X_HYD][TWO]->hydro->surf);

      pchyd->p_reach[r]->p_ele[e]->center->hydro->hydrad = TS_free_ts(pchyd->p_reach[r]->p_ele[e]->center->hydro->hydrad, fp);
      pchyd->p_reach[r]->p_ele[e]->center->hydro->hydrad = TS_mean_fts(pchyd->p_reach[r]->p_ele[e]->face[X_HYD][ONE]->hydro->hydrad, pchyd->p_reach[r]->p_ele[e]->face[X_HYD][TWO]->hydro->hydrad);

      pchyd->p_reach[r]->p_ele[e]->center->hydro->width = TS_free_ts(pchyd->p_reach[r]->p_ele[e]->center->hydro->width, fp);
      pchyd->p_reach[r]->p_ele[e]->center->hydro->width = TS_mean_fts(pchyd->p_reach[r]->p_ele[e]->face[X_HYD][ONE]->hydro->width, pchyd->p_reach[r]->p_ele[e]->face[X_HYD][TWO]->hydro->width);
      // Attention voir si ça fait pas tout planter !!!
      if (pchyd->settings->schem_type == ST_VENANT) {
        // pchyd->p_reach[r]->p_ele[e]->center->hydro->ks = TS_function_value_t(chronos->t[BEGINNING],pchyd->p_reach[r]->strickler,fp);
        pchyd->p_reach[r]->p_ele[e]->center->hydro->ks = TS_function_value_t(pchyd->p_reach[r]->p_ele[e]->center->hydro->Q[T_HYD], pchyd->p_reach[r]->strickler, fp); // SW 03/10/2018
        pchyd->p_reach[r]->p_ele[e]->center->hydro->peri = TS_free_ts(pchyd->p_reach[r]->p_ele[e]->center->hydro->peri, fp);
        pchyd->p_reach[r]->p_ele[e]->center->hydro->peri = TS_divide_fts(pchyd->p_reach[r]->p_ele[e]->center->hydro->surf, pchyd->p_reach[r]->p_ele[e]->center->hydro->hydrad);
        pchyd->p_reach[r]->p_ele[e]->center->hydro->peri = TS_browse_ft(pchyd->p_reach[r]->p_ele[e]->center->hydro->peri, BEGINNING_TS); // SW 23/10/2018 for surf = 0, hydrad = 0, so peri = 0.
        if (pchyd->p_reach[r]->p_ele[e]->center->hydro->peri->ft == TS_INFINITE)
          pchyd->p_reach[r]->p_ele[e]->center->hydro->peri->ft = 0.;

      } else {
        phydro = pchyd->p_reach[r]->p_ele[e]->center->hydro;
        pchyd->p_reach[r]->p_ele[e]->center->hydro->pmusk->param[K] = pchyd->p_reach[r]->p_ele[e]->face[X_HYD][TWO]->hydro->pmusk->param[K];
        phydro->pmusk->param[ALPHA] = pchyd->p_reach[r]->p_ele[e]->face[X_HYD][TWO]->hydro->pmusk->param[ALPHA];
        for (i = 0; i < NSTEP_HYD; i++) {
          phydro->Z[i] = pchyd->p_reach[r]->p_ele[e]->face[X_HYD][ONE]->hydro->Z[i] + pchyd->p_reach[r]->p_ele[e]->face[X_HYD][TWO]->hydro->Z[i];
          phydro->Z[i] /= 2.;
          phydro->H[i] = phydro->Z[i] - pchyd->p_reach[r]->p_ele[e]->center->description->Zbottom;
        }
      }
    }
  }
}

/* Printing of a hydro table of a face or an element */
void HYD_print_hydro_table(s_hydro_hyd *phyd) {
  printf("z Surf Peri Width HydRad\n");
  phyd->surf = TS_browse_ft(phyd->surf, BEGINNING);
  phyd->peri = TS_browse_ft(phyd->peri, BEGINNING);
  phyd->width = TS_browse_ft(phyd->width, BEGINNING);
  phyd->hydrad = TS_browse_ft(phyd->hydrad, BEGINNING);
  printf("%f %f %f %f %f\n", phyd->surf->t, phyd->surf->ft, phyd->peri->ft, phyd->width->ft, phyd->hydrad->ft);

  while (phyd->surf->next != NULL) {
    phyd->surf = phyd->surf->next;
    phyd->peri = phyd->peri->next;
    phyd->width = phyd->width->next;
    phyd->hydrad = phyd->hydrad->next;
    printf("%f %f %f %f %f\n", phyd->surf->t, phyd->surf->ft, phyd->peri->ft, phyd->width->ft, phyd->hydrad->ft);
  }

  printf("\n");
}

void HYD_rewind_hydro_tables(s_chyd *pchyd) {
  int r, f, e;
  s_hydro_hyd *phyd;

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    for (f = 0; f < pchyd->p_reach[r]->nfaces; f++) {

      phyd = pchyd->p_reach[r]->p_faces[f]->hydro;

      /*if (pchyd->p_reach[r]->p_faces[f]->id == 3473)
        HYD_print_hydro_table(phyd);
       */// BL to debug ?
      phyd->surf = TS_browse_ft(phyd->surf, BEGINNING);
      phyd->peri = TS_browse_ft(phyd->peri, BEGINNING);
      phyd->width = TS_browse_ft(phyd->width, BEGINNING);
      phyd->hydrad = TS_browse_ft(phyd->hydrad, BEGINNING);
    }

    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {
      phyd = pchyd->p_reach[r]->p_ele[e]->center->hydro;
      phyd->surf = TS_browse_ft(phyd->surf, BEGINNING);
      phyd->peri = TS_browse_ft(phyd->peri, BEGINNING);
      phyd->width = TS_browse_ft(phyd->width, BEGINNING);
      phyd->hydrad = TS_browse_ft(phyd->hydrad, BEGINNING);
    }
  }
}

/* Fills hydro characteristic from hydro tables (h, z are already defined !) */
s_hydro_hyd *HYD_update_hydro_table(s_hydro_hyd *phyd, double hold, double dz, FILE *fp) {
  if (hold > phyd->H[T_HYD]) {
    phyd->surf = TS_browse_tabulations(phyd->H[T_HYD], phyd->surf, BEGINNING, fp);
    phyd->Surf = value_tabulations(phyd->H[T_HYD], phyd->surf, BEGINNING);
    phyd->peri = TS_browse_tabulations(phyd->H[T_HYD], phyd->peri, BEGINNING, fp);
    phyd->Peri = value_tabulations(phyd->H[T_HYD], phyd->peri, BEGINNING);
    phyd->width = TS_browse_tabulations(phyd->H[T_HYD], phyd->width, BEGINNING, fp);
    phyd->Width = value_tabulations(phyd->H[T_HYD], phyd->width, BEGINNING);
    phyd->rhyd = TS_avoid_inf(phyd->Surf, phyd->Peri);
    phyd->rhyd43 = pow(phyd->rhyd, 4. / 3.);
    if (phyd->width->next != NULL)
      phyd->dL_dZ = (phyd->width->next->ft - phyd->width->ft) / dz;
    else if (phyd->width->prev != NULL)
      phyd->dL_dZ = (phyd->width->ft - phyd->width->prev->ft) / dz;
    else
      phyd->dL_dZ = 0.;
  }

  else if (hold < phyd->H[T_HYD]) {
    phyd->surf = TS_browse_tabulations(phyd->H[T_HYD], phyd->surf, END, fp);
    phyd->Surf = value_tabulations(phyd->H[T_HYD], phyd->surf, END);
    phyd->peri = TS_browse_tabulations(phyd->H[T_HYD], phyd->peri, END, fp);
    phyd->Peri = value_tabulations(phyd->H[T_HYD], phyd->peri, END);
    phyd->width = TS_browse_tabulations(phyd->H[T_HYD], phyd->width, END, fp);
    phyd->Width = value_tabulations(phyd->H[T_HYD], phyd->width, END);
    phyd->rhyd = TS_avoid_inf(phyd->Surf, phyd->Peri);
    phyd->rhyd43 = pow(phyd->rhyd, 4. / 3.);
    if (phyd->width->prev != NULL)
      phyd->dL_dZ = (phyd->width->ft - phyd->width->prev->ft) / dz;
    else if (phyd->width->next != NULL)
      phyd->dL_dZ = (phyd->width->next->ft - phyd->width->ft) / dz;
    else
      phyd->dL_dZ = 0.;
  }

  return phyd;
}

/* Calculates hydro characteristics for rectangular cross-sections */
s_hydro_hyd *HYD_calc_hydro_carac_rectangle(s_hydro_hyd *phyd, double larg) {
  phyd->Surf = phyd->H[T_HYD] * larg;
  phyd->Peri = 2 * phyd->H[T_HYD] + larg;
  phyd->Width = larg;
  phyd->rhyd = TS_avoid_inf(phyd->Surf, phyd->Peri);
  phyd->rhyd43 = pow(phyd->rhyd, 4. / 3.);
  phyd->dL_dZ = 0.;

  return phyd;
}

/**\fn void HYD_manage_memory_transport(s_chyd*, int, FILE*)
 *\brief Allocates variable table for all river elements and all species
         in case transport in river is activated
 *\return -
 *
 * NG : 07/06/2023 : Bug fix. Did not account for multi-specie
 *
 */
void HYD_manage_memory_transport(s_element_hyd *pele, int nb_species, FILE *flog) {

  s_hydro_hyd *phydro = pele->center->hydro;

  if (nb_species > 1)
    phydro->transp_var = (double *)realloc(phydro->transp_var, nb_species * sizeof(double));
  else
    phydro->transp_var = (double *)calloc(nb_species, sizeof(double));

  if (phydro->transp_var == NULL) {
    LP_error(flog, "libhyd%4.2f : Error in file %s, function %s at line %d : Bad memory allocation.\n", NVERSION_HYD, __FILE__, __func__, __LINE__);
  }

  phydro->transp_var[nb_species - 1] = 0.;
  // LP_printf(flog,"id_spe %d pele %d valeur %f\n",nb_species-1,pele->id[GIS_HYD],pele->center->hydro->transp_var[nb_species-1]);
}