/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: calc_hyd_gc.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include "spmatrix.h"
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"
#ifdef OMP
#include "omp.h"
#endif
/* Structure containing the simulation caracteristics */
// extern s_simul *Simul;

static char *mat; // matrix for sparse
int HYD_define_ndl(int schem_type, s_chyd *pchyd, FILE *fpout) {
  int ndl;

  if (schem_type == ST_VENANT)
    ndl = pchyd->counter->nele_tot + pchyd->counter->nfaces_tot;
  else if (schem_type == MUSKINGUM) {
    ndl = pchyd->counter->nele_tot;
  } else {
    LP_error(fpout, "schem_type %d is not defined ", schem_type);
  }
  return ndl;
}

// Initialisation de la structure GC : fait dans initialisation de la simulation

// Calcul de ndl, lmat5, mat4 et mat5 de la structure GC (restent les mêmes tout au long de la simu
void HYD_calc_prop_gc(s_chyd *pchyd, FILE *fpout) {

  s_gc *pgc;
  int schem_type;
  // FILE *fpout ;

  pgc = pchyd->calcul_hydro;
  // modif ndl
  schem_type = pchyd->settings->schem_type;
  pgc->ndl = HYD_define_ndl(schem_type, pchyd, fpout);

  pgc->lmat5 = pchyd->counter->ncoefs_tot;

  pgc->mat4 = GC_create_mat_int(pgc->ndl + 1);
  pgc->mat5 = GC_create_mat_int(pchyd->counter->ncoefs_tot);
  pgc->mat6 = GC_create_mat_double(pchyd->counter->ncoefs_tot);
  pgc->b = GC_create_mat_double(pgc->ndl);
  pgc->x = GC_create_mat_double(pgc->ndl);

  pgc->mat4[0] = 0;
  HYD_fill_mat5_mat4(pchyd, fpout);
  // LV nov2014 pour debug
  // printf("id\n");
}

void HYD_fill_mat5_mat4(s_chyd *pchyd, FILE *fpout) {
  int r, e, f;
  s_element_hyd *pele;
  s_face_hyd *pface;
  s_hydro_hyd *phyd;
  int schem_type;
  int i, i_mat4 = 1, i_mat6 = 0;
  s_gc *pgc;

  pgc = pchyd->calcul_hydro;
  schem_type = pchyd->settings->schem_type;
  if (schem_type == ST_VENANT) {
    for (r = 0; r < pchyd->counter->nreaches; r++) {

      for (e = 0; e < pchyd->p_reach[r]->nele; e++) {
        pele = pchyd->p_reach[r]->p_ele[e];
        phyd = pele->center->hydro;

        phyd->id_mat = ((int *)malloc(phyd->ncoefs * sizeof(int)));

        for (i = 0; i < phyd->ncoefs; i++) {

          phyd->id_mat[i] = i_mat6;
          // pgc->mat5[i_mat6] = phyd->id[i];
          pgc->mat5[i_mat6] = phyd->id[i] + 1; // matrice fortran : commence à 1 et non à 0 !
          i_mat6++;
        }

        // pgc->mat4[i_mat4] = phyd->id_mat[phyd->ncoefs - 1];
        pgc->mat4[i_mat4] = phyd->id_mat[phyd->ncoefs - 1] + 1; // matrice fortran : commence à 1 et non à 0 !
        i_mat4++;
      }
    }

    for (r = 0; r < pchyd->counter->nreaches; r++) {
      for (f = 0; f < pchyd->p_reach[r]->nfaces; f++) {
        // if ncoefs>1
        pface = pchyd->p_reach[r]->p_faces[f];
        phyd = pface->hydro;
        phyd->id_mat = ((int *)malloc(phyd->ncoefs * sizeof(int)));

        for (i = 0; i < phyd->ncoefs; i++) {

          phyd->id_mat[i] = i_mat6;
          // pgc->mat5[i_mat6] = phyd->id[i];
          pgc->mat5[i_mat6] = phyd->id[i] + 1; // matrice fortran : commence à 1 et non à 0 !
          i_mat6++;
        }

        // pgc->mat4[i_mat4] = phyd->id_mat[phyd->ncoefs - 1];
        pgc->mat4[i_mat4] = phyd->id_mat[phyd->ncoefs - 1] + 1; // matrice fortran : commence à 1 et non à 0 !
        i_mat4++;
      }
    }

  } else if (schem_type == MUSKINGUM) {

    for (r = 0; r < pchyd->counter->nreaches; r++) {

      for (e = 0; e < pchyd->p_reach[r]->nele; e++) {
        pele = pchyd->p_reach[r]->p_ele[e];
        pface = pele->face[X_HYD][TWO];
        phyd = pface->hydro;
        if (phyd->ncoefs == 0) {
          LP_error(fpout, "the number of coef of ele %d is %d", phyd->ncoefs);
        }
        phyd->id_mat = ((int *)malloc(phyd->ncoefs * sizeof(int)));

        for (i = 0; i < phyd->ncoefs; i++) {

          phyd->id_mat[i] = i_mat6;
          // pgc->mat5[i_mat6] = phyd->id[i];
          pgc->mat5[i_mat6] = phyd->id[i] + 1; // matrice fortran : commence à 1 et non à 0 !
          i_mat6++;
        }

        // pgc->mat4[i_mat4] = phyd->id_mat[phyd->ncoefs - 1];
        pgc->mat4[i_mat4] = phyd->id_mat[phyd->ncoefs - 1] + 1; // matrice fortran : commence à 1 et non à 0 !
        i_mat4++;
      }
    }
  }
}

void HYD_fill_mat6_musk(s_chyd *pchyd) {

  s_hydro_hyd *phyd;
  int r, e, f, c;
  s_gc *pgc;

  pgc = pchyd->calcul_hydro;
  for (r = 0; r < pchyd->counter->nreaches; r++) {
    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {

      phyd = pchyd->p_reach[r]->p_ele[e]->face[X_HYD][TWO]->hydro;

      for (c = 0; c < phyd->ncoefs; c++)
        pgc->mat6[phyd->id_mat[c]] = phyd->coefs[c];
    }
  }
}

void HYD_fill_b_musk(s_chyd *pchyd, FILE *fp) {
  s_hydro_hyd *phyd;
  s_face_hyd *pface;
  s_element_hyd *pele;
  s_gc *pgc;
  int r, e;
  pgc = pchyd->calcul_hydro;
  for (r = 0; r < pchyd->counter->nreaches; r++) {
    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {
      pele = pchyd->p_reach[r]->p_ele[e];
      phyd = pele->face[X_HYD][TWO]->hydro;
      pgc->b[pele->id[ABS_HYD]] = phyd->b;
      // LP_printf(fp,"fill b in river %s ele %d b %f\n",pele->reach->river,pele->id[INTERN_HYD],phyd->b); //BL to debug
    }
  }
}
void HYD_print_b_musk(s_chyd *pchyd, FILE *fp) {
  s_hydro_hyd *phyd;
  s_face_hyd *pface;
  s_element_hyd *pele;
  s_gc *pgc;
  int r, e;
  pgc = pchyd->calcul_hydro;
  LP_printf(fp, "ABS GIS b\n");
  for (r = 0; r < pchyd->counter->nreaches; r++) {
    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {
      pele = pchyd->p_reach[r]->p_ele[e];
      phyd = pele->face[X_HYD][TWO]->hydro;
      if (phyd->b > 0)
        LP_printf(fp, "%d %d %e\n", pele->id[ABS_HYD], pele->id[GIS_HYD], phyd->b);
    }
  }
}
// Remplissage de mat6 et du membre de droite à chaque pas de temps
void HYD_fill_mat6_b(s_chyd *pchyd) {
  s_hydro_hyd *phyd;
  int r, e, f, c;
  s_gc *pgc;
  pgc = pchyd->calcul_hydro;
  for (r = 0; r < pchyd->counter->nreaches; r++) {

    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {

      phyd = pchyd->p_reach[r]->p_ele[e]->center->hydro;
      if (phyd->ncoefs > 0) {
        for (c = 0; c < phyd->ncoefs; c++)
          pgc->mat6[phyd->id_mat[c]] = phyd->coefs[c];

        pgc->b[pchyd->p_reach[r]->p_ele[e]->id[ABS_HYD]] = phyd->b;
      }
    }

    for (f = 0; f < pchyd->p_reach[r]->nfaces; f++) {

      phyd = pchyd->p_reach[r]->p_faces[f]->hydro;
      if (phyd->ncoefs > 0) {
        for (c = 0; c < phyd->ncoefs; c++)
          pgc->mat6[phyd->id_mat[c]] = phyd->coefs[c];

        pgc->b[pchyd->counter->nele_tot + pchyd->p_reach[r]->p_faces[f]->id[ABS_HYD]] = phyd->b;
      }
    }
  }
}

void HYD_print_mats(s_chyd *pchyd, FILE *fpout) {

  int i;
  s_gc *pgc;
  pgc = pchyd->calcul_hydro;
  fprintf(fpout, "\nmat4\n");
  for (i = 0; i < pgc->ndl + 1; i++)
    fprintf(fpout, "%d ", pgc->mat4[i]);
  fprintf(fpout, "\nmat5\n");
  for (i = 0; i < pgc->lmat5; i++)
    fprintf(fpout, "%d ", pgc->mat5[i]);
  fprintf(fpout, "\nmat6\n");
  for (i = 0; i < pgc->lmat5; i++)
    fprintf(fpout, "%f ", pgc->mat6[i]);
  fprintf(fpout, "\nb\n");
  for (i = 0; i < pgc->ndl; i++)
    fprintf(fpout, "%f ", pgc->b[i]);
}

void HYD_print_sol(s_chyd *pchyd, FILE *fpout) {
  int i;
  double X;
  s_gc *pgc;
  pgc = pchyd->calcul_hydro;
  LP_printf(fpout, "\nX\n");
  for (i = 0; i < pgc->ndl; i++) {
    X = pgc->x[i];
    LP_printf(fpout, "%f ", X);
  }

  LP_printf(fpout, "\n");
}

// Calcul de la solution
void HYD_calc_sol_hydro(s_chyd *pchyd, int *ts_count, FILE *fpout) // NG : 11/06/2021 : ts_count added
{
  int schem_type;
  s_gc *pgc;
  pgc = pchyd->calcul_hydro;
  schem_type = pchyd->settings->schem_type;

  if (schem_type == MUSKINGUM)
    HYD_fill_b_musk(pchyd, fpout);
  else
    HYD_fill_mat6_b(pchyd);

  GC_configure_gc(pgc, ts_count, fpout); // NG : 11/06/2021

  // HYD_print_b_musk(pchyd,fpout);
  gc_init_sys_(pgc->mat4, pgc->mat5, &pgc->ndl);
  // HYD_print_mats(pchyd,fpout); // SW 25/01/2018 degug
  gc_solve_(pgc->mat6, &pgc->lmat5, pgc->b, &pgc->ndl, pgc->x, pgc->x, pgc->sw_int, &pgc->epsgc);
}

// Calcul de la solution par sparse SW 11/07/2018
void HYD_calc_sol_hydro_sparse(s_chyd *pchyd, int n, FILE *fpout) {
  int schem_type, error;
  int i;
  s_gc *pgc;
  double *rhs_b; // right hand side Ax = b

  double *sol_x; // solution x
  double *pelement;
  pgc = pchyd->calcul_hydro;
  schem_type = pchyd->settings->schem_type;

  if (schem_type == MUSKINGUM)
    HYD_fill_b_musk(pchyd, fpout);
  else
    HYD_fill_mat6_b(pchyd);
  // HYD_print_mats(pchyd,fpout);
  sol_x = (spREAL *)calloc(pgc->ndl + 1, sizeof(double));
  bzero((char *)sol_x, (pgc->ndl + 1) * sizeof(double));
  rhs_b = (spREAL *)calloc(pgc->ndl + 1, sizeof(double));
  bzero((char *)rhs_b, (pgc->ndl + 1) * sizeof(double));
  if (n == 0)
    mat = spCreate(pgc->ndl, 0, &error);
  else
    spClear(mat);
  GC_fill_sparse(pgc, rhs_b, mat);
  // spPrint(mat,0,1,1);
  // pelement = spGetElement(mat,1,4272);
  // printf("pelement = %f yes = %d\n",*pelement,YES);
  if (n == 0) // SW 07/11/2018  if the matrix struct change, there will be a bug
    error = spOrderAndFactor(mat, rhs_b, 0.01, 0, 0);
  else
    error = spFactor(mat);
  /* SW 08/11/2018 if error == 2, spZERO_DIAG, A zero was encountered on the diagonal the matrix.  This
      does not necessarily imply that the matrix is singular.  When this
      error occurs, the matrix should be reconstructed and factored using
     spOrderAndFactor().*/
  if (error == 2) {
    spClear(mat);
    GC_fill_sparse(pgc, rhs_b, mat);
    error = spOrderAndFactor(mat, rhs_b, 0.01, 0, 0);
  }
  spSolve(mat, rhs_b, sol_x);

  for (i = 0; i < pgc->ndl; i++)
    pgc->x[i] = sol_x[i + 1];
  free(sol_x);
  sol_x = NULL;
  free(rhs_b);
  rhs_b = NULL;
  // spDestroy(mat);
  // mat = NULL;
  // HYD_print_b_musk(pchyd,fpout);
  // gc_init_sys_(pgc->mat4,pgc->mat5,&pgc->ndl);
  // HYD_print_mats(pchyd,fpout); // SW 25/01/2018 degug
  // gc_solve_(pgc->mat6,&pgc->lmat5,pgc->b,&pgc->ndl,pgc->x,pgc->x,pgc->sw_int,&pgc->epsgc);
}

void HYD_extract_sol_Musk_reach(s_chyd *pchyd, s_reach_hyd *preach, s_gc *pgc, FILE *fp) {
  int e;
  // s_gc *pgc;
  s_hydro_hyd *phyd;
  s_element_hyd *pele;
  int id;
#ifdef OMP
  s_smp *psmp;
  psmp = pchyd->settings->psmp;
#endif
  pele = preach->p_ele[0];
  HYD_extract_Q_first_ele(pele, pgc, fp);

#ifdef OMP
  int taille;
  int nthreads;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);

  psmp->chunk = PC_set_chunk_size_silent(fp, preach->nele - 1, nthreads);
  taille = psmp->chunk;
#pragma omp parallel shared(preach, pgc, nthreads, taille) private(e, pele, phyd, id)
  {
#pragma omp for schedule(dynamic, taille)
#endif
    for (e = 1; e < preach->nele; e++) {
      pele = preach->p_ele[e];
      phyd = pele->face[X_HYD][TWO]->hydro;
      phyd->Q[ITER_HYD] = phyd->Q[T_HYD];
      phyd->Q[ITER_INTERPOL_HYD] = phyd->Q[INTERPOL_HYD];
      phyd->dq = phyd->Q[T_HYD]; // LV nov2014 pour calcul convergence
      phyd->Q[INTERPOL_HYD] = phyd->Q[T_HYD];
      id = pele->id[ABS_HYD];

      phyd->Q[T_HYD] = pgc->x[id];

      phyd->dq -= phyd->Q[T_HYD]; // LV nov2014 pour calcul convergence
      phyd->Q[INTERPOL_HYD] = phyd->Q[T_HYD];
    }
#ifdef OMP
  } // end of parallel section
#endif
}
// Lecture de la solution : Z aux centres des éléments et Q aux faces
void HYD_extract_solgc(s_chyd *pchyd, FILE *fp) {
  int r, e, f;
  // s_gc *pgc;
  s_hydro_hyd *phyd;
  s_element_hyd *pele;
  s_face_hyd *pface;
  int id;
  int schem_type, nreaches;
  s_gc *pgc;
  s_reach_hyd **p_reach;
  s_reach_hyd *preach;

#ifdef OMP // SW 06/09/2018 ajout block openmp
  s_smp *psmp;
  psmp = pchyd->settings->psmp;
#endif
  schem_type = pchyd->settings->schem_type;
  pgc = pchyd->calcul_hydro;

  if (schem_type == ST_VENANT) {
#ifdef OMP // SW 06/09/2018 ajout block openmp
    int taille;
    int nthreads;
    nthreads = psmp->nthreads;
    omp_set_num_threads(psmp->nthreads);

    psmp->chunk = PC_set_chunk_size_silent(fp, pchyd->counter->nreaches - 1, nthreads);
    taille = psmp->chunk;
#pragma omp parallel shared(pchyd, pgc, nthreads, taille) private(preach, e, pele, phyd, id, f, pface)
    {
#pragma omp for schedule(dynamic, taille)
#endif
      for (r = 0; r < pchyd->counter->nreaches; r++) {
        preach = pchyd->p_reach[r];
        for (e = 0; e < pchyd->p_reach[r]->nele; e++) {
          pele = preach->p_ele[e]; // SW 06/09/2018 ajout block openmp
          phyd = pele->center->hydro;
          phyd->Z[ITER_HYD] = phyd->Z[T_HYD];
          phyd->Z[ITER_INTERPOL_HYD] = phyd->Z[INTERPOL_HYD];
          phyd->dz = phyd->Z[T_HYD]; // LV nov2014 pour calcul convergence
          id = pele->id[ABS_HYD];
          phyd->Z[T_HYD] = pgc->x[id];
          phyd->Z[INTERPOL_HYD] = phyd->Z[T_HYD];
          phyd->dz -= phyd->Z[T_HYD]; // LV nov2014 pour calcul convergence
        }
      }
#ifdef OMP
    } // end of parallel section
#endif

#ifdef OMP
#pragma omp parallel shared(pchyd, pgc, nthreads, taille) private(preach, e, pele, phyd, id, f, pface)
    {
#pragma omp for schedule(dynamic, taille)
#endif
      for (r = 0; r < pchyd->counter->nreaches; r++) {
        preach = pchyd->p_reach[r];
        for (f = 0; f < preach->nfaces; f++) {
          pface = preach->p_faces[f];
          phyd = pface->hydro;
          phyd->Q[ITER_HYD] = phyd->Q[T_HYD];
          phyd->Q[ITER_INTERPOL_HYD] = phyd->Q[INTERPOL_HYD];
          phyd->dq = phyd->Q[T_HYD]; // LV nov2014 pour calcul convergence
          id = pface->id[ABS_HYD] + pchyd->counter->nele_tot;
          phyd->Q[T_HYD] = pgc->x[id];
          phyd->Q[INTERPOL_HYD] = phyd->Q[T_HYD];
          phyd->dq -= phyd->Q[T_HYD]; // LV nov2014 pour calcul convergence
        }
      }
#ifdef OMP
    } // end of parallel section
#endif
  } else if (schem_type == MUSKINGUM) {
    p_reach = pchyd->p_reach;
    nreaches = pchyd->counter->nreaches;

    for (r = 0; r < pchyd->counter->nreaches; r++) {

      HYD_extract_sol_Musk_reach(pchyd, p_reach[r], pgc, fp);
    }
  }
}
/* doit traiter le premier element à part !! en effet, le schema de muskingum permet de calculer uniquement le débit à la sortie soit à la face [X_HYD][TWO]. Pour le premier element pour que le débit à la face [X_HYD][ONE] ne soit pas nul, doit être calculer à part peut sans doute s'intégrer dans le calcul matriciel en ajoutant un element san apport latéraux à voir ... BL*/
void HYD_extract_Q_first_ele(s_element_hyd *pele, s_gc *pgc, FILE *fp) {
  int id, id_upstr, i, nupstr;
  s_hydro_hyd *phyd, *phyd_upstr;
  s_face_hyd *pface_reach_upstr;
  id = pele->id[ABS_HYD];
  phyd = pele->face[X_HYD][TWO]->hydro;
  phyd->Q[ITER_HYD] = phyd->Q[T_HYD];
  phyd->Q[ITER_INTERPOL_HYD] = phyd->Q[INTERPOL_HYD];

  phyd->dq = phyd->Q[T_HYD]; // LV nov2014 pour calcul convergence
  phyd_upstr = pele->face[X_HYD][ONE]->hydro;
  phyd_upstr->dq = phyd_upstr->Q[T_HYD]; // LV nov2014 pour calcul convergence

  phyd->Q[T_HYD] = pgc->x[id];

  phyd->Q[INTERPOL_HYD] = phyd->Q[T_HYD];
  phyd->dq -= phyd->Q[T_HYD]; // LV nov2014 pour calcul convergence
  if (pele->reach->limits[UPSTREAM]->nupstr_reaches != 0) {
    phyd_upstr->Q[ITER_HYD] = phyd_upstr->Q[T_HYD];
    phyd_upstr->Q[ITER_INTERPOL_HYD] = phyd_upstr->Q[INTERPOL_HYD];
    nupstr = pele->reach->limits[UPSTREAM]->nupstr_reaches;
    phyd_upstr->Q[T_HYD] = 0;
    for (i = 0; i < nupstr; i++) {
      pface_reach_upstr = pele->reach->limits[UPSTREAM]->faces[UPSTREAM][i];
      id_upstr = pface_reach_upstr->element[UPSTREAM]->id[ABS_HYD];
      phyd_upstr->Q[T_HYD] += pgc->x[id_upstr];
    }
    phyd_upstr->Q[INTERPOL_HYD] = phyd_upstr->Q[T_HYD];
  } else {
    phyd_upstr->Q[ITER_HYD] = phyd_upstr->Q[T_HYD];
    phyd_upstr->Q[ITER_INTERPOL_HYD] = phyd_upstr->Q[INTERPOL_HYD];
    phyd_upstr->Q[T_HYD] = pele->reach->q_upstr;
    phyd_upstr->Q[INTERPOL_HYD] = phyd_upstr->Q[T_HYD];
  }
  phyd_upstr->dq -= phyd_upstr->Q[T_HYD]; // LV nov2014 pour calcul convergence
}

void HYD_extract_sol_pic_Musk_reach(s_chyd *pchyd, s_reach_hyd *preach, s_gc *pgc, FILE *fp) {
  int e;
  // s_gc *pgc;
  s_hydro_hyd *phyd;
  s_element_hyd *pele;
  int id;
#ifdef OMP
  s_smp *psmp;
  psmp = pchyd->settings->psmp;
#endif
  pele = preach->p_ele[0];
  HYD_extract_Q_first_ele_pic(pele, pgc, fp);
#ifdef OMP
  int taille;
  int nthreads;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);

  psmp->chunk = PC_set_chunk_size_silent(fp, preach->nele - 1, nthreads);
  taille = psmp->chunk;
#pragma omp parallel shared(pgc, nthreads, taille) private(e, pele, phyd, id)
  {
#pragma omp for schedule(dynamic, taille)
#endif
    for (e = 1; e < preach->nele; e++) {
      pele = preach->p_ele[e];
      phyd = pele->face[X_HYD][TWO]->hydro;
      id = pele->id[ABS_HYD];

      phyd->Q[PIC_HYD] = pgc->x[id];
    }

#ifdef OMP
  } // end of parallel section
#endif
}
// Lecture de la solution : Z aux centres des éléments et Q aux faces
void HYD_extract_solgc_pic(s_chyd *pchyd, FILE *fp) {
  int r, e, f, nreaches;
  // s_gc *pgc;
  s_hydro_hyd *phyd;
  s_element_hyd *pele;
  int id;
  int schem_type;
  s_gc *pgc;
  s_reach_hyd **p_reach;
  s_reach_hyd *preach;

  schem_type = pchyd->settings->schem_type;
  pgc = pchyd->calcul_hydro;
  p_reach = pchyd->p_reach;
  nreaches = pchyd->counter->nreaches;
  if (schem_type == ST_VENANT) {
    LP_error(fp, "picard loop haven't been implemented for ST_VENANT scheme\n");
  } else if (schem_type == MUSKINGUM) {

    for (r = 0; r < pchyd->counter->nreaches; r++) {
      // preach=p_reach[r];
      HYD_extract_sol_pic_Musk_reach(pchyd, p_reach[r], pgc, fp);
    }
  }
}
/* doit traiter le premier element à part !! en effet, le schema de muskingum permet de calculer uniquement le débit à la sortie soit à la face [X_HYD][TWO]. Pour le premier element pour que le débit à la face [X_HYD][ONE] ne soit pas nul, doit être calculer à part peut sans doute s'intégrer dans le calcul matriciel en ajoutant un element san apport latéraux à voir ... BL*/
void HYD_extract_Q_first_ele_pic(s_element_hyd *pele, s_gc *pgc, FILE *fp) {
  int id, id_upstr, i, nupstr;
  s_hydro_hyd *phyd, *phyd_upstr;
  s_face_hyd *pface_reach_upstr;
  id = pele->id[ABS_HYD];
  phyd = pele->face[X_HYD][TWO]->hydro;
  phyd_upstr = pele->face[X_HYD][ONE]->hydro;

  phyd->Q[PIC_HYD] = pgc->x[id];

  if (pele->reach->limits[ONE]->nupstr_reaches != 0) {
    nupstr = pele->reach->limits[UPSTREAM]->nupstr_reaches;
    phyd_upstr->Q[PIC_HYD] = 0;
    for (i = 0; i < nupstr; i++) {
      pface_reach_upstr = pele->reach->limits[UPSTREAM]->faces[UPSTREAM][i];
      id_upstr = pface_reach_upstr->element[UPSTREAM]->id[ABS_HYD];
      phyd_upstr->Q[PIC_HYD] += pgc->x[id_upstr];
    }

  } else
    phyd_upstr->Q[PIC_HYD] = pele->reach->q_upstr;
}

void HYD_extract_sol_t(s_chyd *pchyd, s_clock_CHR *clock, s_chronos_CHR *chronos, s_output_hyd ***p_outputs, s_inout_set_io *pinout, s_out_io **pout_all, FILE *fp) {
  FILE *fpmb;
  double t, dt, t_out = -9999;
  s_mb_hyd *pmb;
  s_out_io *pout;
  pout = pout_all[MBHYD_IO];
  if (pout == NULL) {
    pout = pout_all[HYD_H_IO];
    if (pout == NULL)
      pout = pout_all[HYD_Q_IO];
  }
  dt = chronos->dt;
  t = chronos->t[CUR_CHR];
  if (pout != NULL) {
    t_out = pout->t_out[CUR_IO];
  } else {
    fpmb = pinout->fmb;
    pmb = HYD_init_mass_balance();                         // LV 22/05/2012
    pmb->time = t;                                         // NF 27/3/2012
    CHR_begin_timer();                                     // LV 3/09/2012
    pmb = HYD_set_state_for_mass_balance(pmb, INI, pchyd); // NF 27/3/2012
    clock->time_spent[OUTPUTS_CHR] += CHR_end_timer();     // LV 3/09/2012
  }

  CHR_begin_timer(); // LV 3/09/2012
  HYD_extract_solgc(pchyd, fp);
  HYD_interpolate_Q_Z(t, pchyd, T_HYD, fp);
  if (fabs(t_out - CODE) > EPS_HYD) {
    if (t_out < t && t_out > t - dt) {
      HYD_interpol_var(pchyd, t, t_out, dt, fp);
    }
    HYD_interpolate_Q_Z(t_out, pchyd, INTERPOL_HYD, fp);
  }
  clock->time_spent[MATRIX_GET_CHR] += CHR_end_timer(); // LV 3/09/2012

  if (pout != NULL) {
    CHR_begin_timer(); // LV 3/09/2012
    HYD_calc_mb_All(pchyd, chronos, pout_all, pchyd->settings->general_param[THETA], fp);
    clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/20127

  }

  else {

    CHR_begin_timer(); // LV 3/09/2012
    pmb = HYD_set_state_for_mass_balance(pmb, END, pchyd);
    clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012

    CHR_begin_timer();                                       // LV 3/09/2012
    pmb = HYD_print_mass_balance(pmb, t, fpmb, chronos, fp); // NF 5/15/06

    if (pinout->calc[MB_ELE] == YES) // LV 15/06/2012
      HYD_print_mb_at_elements(t, p_outputs, pchyd, chronos, fp);

    clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012

    if (pmb != NULL)
      pmb = (s_mb_hyd *)HYD_free_p(pmb);
  }
}

// NG : 23/02/2020 Extraction de la solution pour l'hyperdermique
void HYD_extract_sol_t_hyperdermic(s_chyd *pchyd, s_clock_CHR *clock, s_chronos_CHR *chronos, s_output_hyd ***p_outputs, s_inout_set_io *pinout, s_out_io **pout_all, FILE *fp) {
  FILE *fpmb;
  double t, dt, t_out = -9999;
  s_mb_hyd *pmb;
  s_out_io *pout;
  pout = pout_all[HDERM_MB_IO];
  if (pout == NULL)
    pout = pout_all[HDERM_Q_IO];

  dt = chronos->dt;
  t = chronos->t[CUR_CHR];
  if (pout != NULL) {
    t_out = pout->t_out[CUR_IO];
  } else {
    fpmb = pinout->fmb;
    pmb = HYD_init_mass_balance();                         // LV 22/05/2012
    pmb->time = t;                                         // NF 27/3/2012
    CHR_begin_timer();                                     // LV 3/09/2012
    pmb = HYD_set_state_for_mass_balance(pmb, INI, pchyd); // NF 27/3/2012
    clock->time_spent[OUTPUTS_CHR] += CHR_end_timer();     // LV 3/09/2012
  }

  CHR_begin_timer(); // LV 3/09/2012
  HYD_extract_solgc(pchyd, fp);
  HYD_interpolate_Q_Z(t, pchyd, T_HYD, fp);
  if (fabs(t_out - CODE) > EPS_HYD) {
    if (t_out < t && t_out > t - dt) {
      HYD_interpol_var(pchyd, t, t_out, dt, fp);
    }
    HYD_interpolate_Q_Z(t_out, pchyd, INTERPOL_HYD, fp);
  }
  clock->time_spent[MATRIX_GET_CHR] += CHR_end_timer(); // LV 3/09/2012

  if (pout != NULL) {
    CHR_begin_timer();                                                                                // LV 3/09/2012
    HYD_calc_mb_All_hyperdermic(pchyd, chronos, pout_all, pchyd->settings->general_param[THETA], fp); // NG : 23/02/2020 fonction à merger avec HYD_calc_mb_All
    clock->time_spent[OUTPUTS_CHR] += CHR_end_timer();                                                // LV 3/09/20127

  }

  else {

    CHR_begin_timer(); // LV 3/09/2012
    pmb = HYD_set_state_for_mass_balance(pmb, END, pchyd);
    clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012

    CHR_begin_timer();                                       // LV 3/09/2012
    pmb = HYD_print_mass_balance(pmb, t, fpmb, chronos, fp); // NF 5/15/06

    if (pinout->calc[MB_ELE] == YES) // LV 15/06/2012
      HYD_print_mb_at_elements(t, p_outputs, pchyd, chronos, fp);

    clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012

    if (pmb != NULL)
      pmb = (s_mb_hyd *)HYD_free_p(pmb);
  }
}

void HYD_interpol_var_reach(s_reach_hyd *preach, double t, double t_out, double dt, FILE *fp) {
  s_element_hyd *pele;
  s_face_hyd *pface;
  int nele, j;

  nele = preach->nele;
  for (j = 0; j < nele; j++) {
    pele = preach->p_ele[j];
    pface = pele->face[X_HYD][TWO];
    pface->hydro->Q[INTERPOL_HYD] = TS_interpol(pface->hydro->Q[ITER_HYD], t - dt, pface->hydro->Q[T_HYD], t, t_out);
  }
}

void HYD_interpol_var(s_chyd *pchyd, double t, double t_out, double dt, FILE *fp) {
  int nreach, k;
  s_reach_hyd **p_reach;

#ifdef OMP
  s_smp *psmp;
  psmp = pchyd->settings->psmp;
#endif
  nreach = pchyd->counter->nreaches;
  p_reach = pchyd->p_reach;
#ifdef OMP
  int taille;
  int nthreads;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);

  psmp->chunk = PC_set_chunk_size_silent(fp, pchyd->counter->nreaches, nthreads);
  taille = psmp->chunk;
#pragma omp parallel shared(p_reach, nthreads, taille) private(k)
  {
#pragma omp for schedule(dynamic, taille)
#endif
    for (k = 0; k < pchyd->counter->nreaches; k++) {
      HYD_interpol_var_reach(p_reach[k], t, t_out, dt, fp);
    }
#ifdef OMP
  } // end of parallel section
#endif

  HYD_interpolate_Q_Z(t_out, pchyd, INTERPOL_HYD, fp);
}

double HYD_calc_qout(s_element_hyd *pele, double dt, double theta, double qinr, FILE *fp) {
  s_hydro_hyd *phyd, *phyd_face;
  double qout = 0;
  double qstock;
  phyd = pele->center->hydro;
  phyd_face = pele->face[X_HYD][ONE]->hydro;
  // qout=theta*(phyd_face->Q[PIC_HYD]+qinr+phyd->qapp[RUNOFF_HYD][T_HYD]*pele->length)+(1-theta)*((phyd->qapp[INR_HYD][ITER_HYD]+phyd->qapp[RUNOFF_HYD][ITER_HYD])*pele->length+phyd_face->Q[T_HYD]);
  qout = qinr + phyd->qapp[RUNOFF_HYD][T_HYD] * pele->length + phyd_face->Q[PIC_HYD];
  qstock = HYD_calc_stock_musk_pic(pele, theta, qinr, fp);
  qstock /= dt;
  qout -= qstock;
  return qout;
}
