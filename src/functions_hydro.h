/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: functions_hydro.h
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/* in calc_hyd_gc.c */
int HYD_define_ndl(int, s_chyd *, FILE *);
void HYD_calc_prop_gc(s_chyd *, FILE *);
void HYD_fill_mat5_mat4(s_chyd *, FILE *);
void HYD_fill_mat6_musk(s_chyd *);
void HYD_fill_b_musk(s_chyd *, FILE *);
void HYD_fill_mat6_b(s_chyd *);
void HYD_print_mats(s_chyd *, FILE *);
void HYD_print_sol(s_chyd *, FILE *);
void HYD_calc_sol_hydro(s_chyd *, int *, FILE *);
void HYD_extract_sol_Musk_reach(s_chyd *, s_reach_hyd *, s_gc *, FILE *);
void HYD_extract_solgc(s_chyd *, FILE *);
void HYD_extract_Q_first_ele(s_element_hyd *, s_gc *, FILE *);
void HYD_extract_sol_pic_Musk_reach(s_chyd *, s_reach_hyd *, s_gc *, FILE *);
void HYD_extract_solgc_pic(s_chyd *, FILE *);
void HYD_extract_Q_first_ele_pic(s_element_hyd *, s_gc *, FILE *);
void HYD_extract_sol_t(s_chyd *, s_clock_CHR *, s_chronos_CHR *, s_output_hyd ***, s_inout_set_io *, s_out_io **, FILE *);
void HYD_extract_sol_t_hyperdermic(s_chyd *, s_clock_CHR *, s_chronos_CHR *, s_output_hyd ***, s_inout_set_io *, s_out_io **, FILE *);
void HYD_interpol_var_reach(s_reach_hyd *, double, double, double, FILE *);
void HYD_interpol_var(s_chyd *, double, double, double, FILE *);
void HYD_print_b_musk(s_chyd *, FILE *);
double HYD_calc_qout(s_element_hyd *, double, double, double, FILE *);
void HYD_calc_sol_hydro_sparse(s_chyd *, int, FILE *); // SW 11/07/2018

/*in curvature.c*/
void HYD_calculate_curvature_radius(s_chyd *);
void HYD_zero_curvature(s_chyd *, FILE *);

/*in hydraulics.c*/
void HYD_calc_cv_criteria(double *, double *, int *, int *, s_chyd *);
void HYD_steady_state(double, s_mb_hyd *, s_mb_hyd *, FILE *, s_inout_set_io *, s_output_hyd ***, s_chyd *, s_clock_CHR *, s_chronos_CHR *, FILE *);
void HYD_transient_hydraulics(double *, double, s_mb_hyd *, s_mb_hyd *, FILE *, s_chyd *, s_clock_CHR *, s_chronos_CHR *, s_output_hyd ***, s_inout_set_io *, FILE *);
s_mb_hyd *HYD_solve_hydro(double, FILE *, s_mb_hyd *, s_chyd *, s_clock_CHR *, s_chronos_CHR *, s_output_hyd ***, s_inout_set_io *, int, FILE *);
void HYD_calc_Qapp(double, s_chyd *, FILE *);
void HYD_find_id_coefs_ele(s_chyd *);
void HYD_find_id_coefs_face(s_chyd *);
void HYD_find_id_coefs_muskingum(s_chyd *, FILE *);
void HYD_calc_LHS_Musk(s_chyd *, s_chronos_CHR *, FILE *);
void HYD_calc_LHS_Musk_reach(s_reach_hyd *, double, double, FILE *);
void HYD_calc_LHS_coef_Musk(s_face_hyd *, double, double, double, FILE *);
void HYD_calc_RHS_Musk_reach(s_reach_hyd *, double, double, double, FILE *);
void HYD_calc_RHS_Musk(double, double, s_chyd *, FILE *);
void HYD_calc_RHS_coef_upstream_Musk(s_face_hyd *, s_face_hyd *, double, double, double, double, FILE *);
void HYD_calc_RHS_coef_common_Musk(s_face_hyd *, double, double, double, FILE *);
void HYD_calc_RHS_coef_central_Musk(s_face_hyd *, double, FILE *);
void HYD_calc_coefs_ele(double, double, s_chyd *, FILE *);
void HYD_calc_coefs_ele_centraux(s_element_hyd *, double, double);
void HYD_calc_coefs_ele_qup(s_element_hyd *, double, double, double);
void HYD_calc_coefs_ele_wl(s_element_hyd *, double);
void HYD_calc_coefs_eq_Z(s_element_hyd *);
void HYD_calc_coefs_ele_weir_gate(s_element_hyd *, s_BC_char_hyd **, int, double, double);
void HYD_calc_coefs_faces(double, double, s_chyd *, FILE *);
void HYD_calc_coefs_faces_centrales(s_face_hyd *, double, double);
void HYD_calc_coefs_face_qup(s_face_hyd *, double);
void HYD_calc_coefs_face_mass_conservation_down(s_face_hyd *, double, double);
void HYD_calc_coefs_face_mass_conservation_up(s_face_hyd *, double, double);
void HYD_calc_coefs_eq_Q(s_face_hyd *);
void HYD_calc_coefs_eq_Q_conf_diff(s_face_hyd *);
void HYD_calc_coefs_eq_Q_confdiff(s_face_hyd *);
void HYD_calc_coefs_face_openBC(s_face_hyd *, double, double);
int HYD_test_sing_face(s_face_hyd *, int);
void HYD_interpolate_Q_Z(double, s_chyd *, int, FILE *);
void HYD_initialize_GC(s_chyd *, s_chronos_CHR *, FILE *);
void HYD_calc_Q2Z_ele(s_element_hyd *, int, FILE *);
void HYD_calc_Q2ZMusk_ele(s_element_hyd *, int, FILE *);
void HYD_allocate_coefshydro(s_hydro_hyd *, int);

/* in init_hyd.c */
void HYD_initialize_hydro(s_chyd *, s_inout_set_io *, s_out_io *, s_chronos_CHR *, FILE *);
void HYD_initialize_Z_from_file(s_chyd *, FILE *);
void HYD_initialize_Q_from_file(s_chyd *, FILE *);
void HYD_calculation_Z_init(s_chyd *, FILE *);
void HYD_calculate_Z_sing(s_chyd *, s_chronos_CHR *, FILE *);
void HYD_calculate_Q_reach(s_chyd *, FILE *);
void HYD_calculation_Q_init(s_chyd *, FILE *);
void HYD_initialize_Q_from_file_caw(FILE *, s_chyd *, FILE *);

/*in itos.c */
char *HYD_name_inflow(int);
char *HYD_name_output(int);
char *HYD_riverbank_inflow(int);     // NG : 01/10/2020
char *HYD_inflow_variable_name(int); // NG : 01/10/2020
char *HYD_name_calc_state(int);
char *HYD_name_general_param(int);
char *HYD_name_extremum(int);
char *HYD_name_answer(int);
char *HYD_name_direction(int);
char *HYD_name_sing_type(int);
char *HYD_name_hyd_var(int);
char *HYD_unit_general_param(int);
char *HYD_name_scheme(int);
char *HYD_id_type(int);               // NG : 18/08/2020
char *HYD_coordinates_hyd_type(int);  // NG : 02/10/2020
char *HYD_K_method_name(int, FILE *); // NG : 05/08/2022
char *HYD_river_height_method_name(int, FILE *);

/* in manage element */
void HYD_create_elements(s_chyd *, FILE *);
s_element_hyd *HYD_create_element(s_chyd *);
int HYD_check_is_element_outlet(s_chyd *, int, FILE *);               // NG : 06/03/2020
double HYD_extract_q_element(s_chyd *, int, FILE *);                  // NG : 06/03/2020
s_element_hyd *HYD_check_is_ele_defined(s_chyd *, int, int, FILE *);  // NG : 18/08/2020
void HYD_print_ele_transport_variable(s_chyd *, int, FILE *);         // NG : 09/10/2021
s_element_hyd *HYD_find_element_by_id_io(int, int, s_chyd *, FILE *); // NG : 27/06/2023

/*in manage_faces.c */
void HYD_add_faces(s_chyd *, s_chronos_CHR *, FILE *);
void HYD_check_nb_faces(s_chyd *, s_chronos_CHR *, FILE *);
void HYD_create_faces(s_face_hyd *, s_chyd *, s_chronos_CHR *, FILE *);
s_face_hyd *HYD_create_face(int, s_chyd *);
s_face_hyd *HYD_chain_faces(s_face_hyd *, s_face_hyd *);
s_face_hyd *HYD_chain_faces_fwd(s_face_hyd *, s_face_hyd *);
s_face_hyd *HYD_rewind_faces(s_face_hyd *);
s_face_hyd *HYD_browse_faces(s_face_hyd *, int);
void HYD_calc_reach_mean_slope_bed(s_reach_hyd *, FILE *); // SW 15/10/2018

/*in manage geometry */
s_geometry_hyd *HYD_create_geometry();
void HYD_create_ptsAbscZ(s_face_hyd *, s_pointAbscZ_hyd *);
void HYD_create_musk_geometry(s_face_hyd *, double, double, double, FILE *);
void HYD_create_all_musk_geometry(s_face_hyd *, FILE *);
void HYD_print_geometry(s_face_hyd *, FILE *);
void HYD_create_ptsXYZ(s_face_hyd *, s_pointXYZ_hyd *);
s_pointAbscZ_hyd *HYD_create_ptAbscZ(double, double);
s_pointXYZ_hyd *HYD_create_ptXYZ(double, double, double);
s_pointAbscZ_hyd *HYD_chain_ptsAbscZ(s_pointAbscZ_hyd *, s_pointAbscZ_hyd *);
s_pointXYZ_hyd *HYD_chain_ptsXYZ(s_pointXYZ_hyd *, s_pointXYZ_hyd *);
s_pointAbscZ_hyd *HYD_rewind_ptAbscZ(s_pointAbscZ_hyd *, int);
s_pointXYZ_hyd *HYD_rewind_ptXYZ(s_pointXYZ_hyd *);
void HYD_calculate_AbscZ_pts(s_geometry_hyd *);
s_description_hyd *HYD_create_description(s_chyd *);
void HYD_description_interpolated_face(s_face_hyd *, s_face_hyd *);
void HYD_calculate_faces_traj(s_face_hyd *, s_chyd *);
double HYD_get_Zbot_av(s_face_hyd *, int, FILE *);

/* in manage_hydro.c */
s_hydro_hyd *HYD_create_hydro(int);
void HYD_surf_peri_width(s_face_hyd *, double, int, int, int);
void HYD_calculate_hydro_table(s_face_hyd *, s_chyd *, s_chronos_CHR *, FILE *);
void HYD_table_hydro_faces(s_face_hyd *, s_chyd *, s_chronos_CHR *, FILE *);
void HYD_table_hydro_interpolated_face(s_face_hyd *, s_face_hyd *, s_face_hyd *, int, s_chronos_CHR *, FILE *);
void HYD_table_hydro_elements(s_chyd *, s_chronos_CHR *, FILE *);
void HYD_print_hydro_table(s_hydro_hyd *);
void HYD_rewind_hydro_tables(s_chyd *);
s_hydro_hyd *HYD_update_hydro_table(s_hydro_hyd *, double, double, FILE *);
s_hydro_hyd *HYD_calc_hydro_carac_rectangle(s_hydro_hyd *, double);
void HYD_manage_memory_transport(s_element_hyd *, int, FILE *); // NG : 07/06/2023

/*in manage_inflows.c*/
void HYD_create_inflows(s_inflow_hyd *, s_chyd *, int, FILE *);                                // NG : 05/10/2020
s_inflow_hyd *HYD_create_inflow(char *, s_chyd *);                                             // NG : 09/10/2020
s_pt_inflow_hyd *HYD_create_point_inflow(char *name);                                          // NG : 02/10/2020
s_inflow_hyd *HYD_chain_inflows(s_inflow_hyd *, s_inflow_hyd *);                               // NG : 01/10/2020
s_inflow_hyd *HYD_browse_inflows(s_inflow_hyd *, int, FILE *);                                 // NG : 01/10/2020
int HYD_riverside_inflow(double);                                                              // NG : 01/10/2020
void HYD_display_inflows_properties(s_chyd *, int, FILE *);                                    // NG : 01/10/2020
void HYD_inflows_settings(s_chyd *, int, int, s_inflow_hyd *, FILE *);                         // NG : 01/10/2020
void HYD_display_inflow_at_element_faces(s_chyd *, int, FILE *);                               // NG : 02/10/2020
s_diffuse_inflow_hyd *HYD_create_new_diffuse_inflow(void);                                     // NG : 02/10/2020
void HYD_attribute_diffuse_inflows_time_series(s_inflow_hyd *, int, int, double, int, FILE *); // NG : 05/10/2020
void HYD_display_inflows_time_series(s_inflow_hyd *, int, FILE *);                             // NG : 05/10/2020

/*in manage_mb.c */
s_inout_set_io *HYD_init_inout_set();
s_output_hyd ***HYD_init_output_hyd();
s_mb_hyd *HYD_init_mass_balance(void);
s_mb_hyd *HYD_chain_mb(s_mb_hyd *, s_mb_hyd *);
void HYD_initialize_mass_balance_t(s_mb_hyd *);
s_mb_hyd *HYD_set_state_for_mass_balance(s_mb_hyd *, int, s_chyd *);
s_mb_hyd *HYD_calculate_single_mb(s_element_hyd *, int, s_mb_hyd *, s_chyd *);
s_mb_hyd *HYD_print_mass_balance(s_mb_hyd *, double, FILE *, s_chronos_CHR *, FILE *);
void HYD_calc_mb_face(s_chyd *, s_element_hyd *, double, FILE *);
void HYD_calc_mb(s_element_hyd *, double, double, FILE *);
double HYD_calc_stock_musk(s_element_hyd *, double, double, FILE *);
void HYD_calc_flux(s_element_hyd *, double, FILE *);
void HYD_calc_output(s_chronos_CHR *, s_chyd *, s_out_io *, int, FILE *);
void HYD_calc_output_hyperdermic(s_chronos_CHR *, s_chyd *, s_out_io *, int, FILE *); // NG : 23/02/2020
void HYD_reinit_mb_All(s_chyd *, FILE *);
void HYD_reinit_mb(s_mb_hyd *, FILE *);
void HYD_calc_mb_All(s_chyd *, s_chronos_CHR *, s_out_io **, double, FILE *);
void HYD_calc_mb_All_hyperdermic(s_chyd *, s_chronos_CHR *, s_out_io **, double, FILE *); // NG : 23/02/2020
FILE *HYD_create_header_mass_balance_file(FILE *);
double HYD_calc_stock_musk_pic(s_element_hyd *, double, double, FILE *);

/*in manage_musk_set */
s_musk_set_hyd *HYD_create_musk_set();
double HYD_calc_k_face(s_face_hyd *, FILE *);
double HYD_calc_TC(double, double, double, FILE *);
void HYD_calc_TC_reach(s_chyd *, FILE *);
void HYD_calc_SDA(s_chyd *, double **, FILE *);

/* NG : 29/02/2020 : BL functions below dealing with concentration time management are now obsolete. Tc is now managed by libfp
 as it is an attribute of the libfp object "catchment". These functions have been updated to integrate Tc-related calculations (Itr_max, Ki_musk, etc.)
 for multiples catchments in a same model. Both methods (TTRA and FORMULA) remain valid. New functions have been put in HYD_hydraulics_coupled.c as
 they use both libhyd and libfp objects and attributes. */

// double HYD_get_TC_Bassin(s_chyd *,FILE *);
// void HYD_set_TC_reach(s_chyd *,FILE *);
// double HYD_calc_Itr_i(s_chyd *, double ** ,FILE *);
// double HYD_set_K_TTRA_Bassin(s_chyd *,double ,FILE *);
// void HYD_set_K_basin(s_chyd *,FILE *);

/* in manage_outputs.c */
s_output_hyd *HYD_initialize_output(s_chronos_CHR *);
void HYD_create_output_files(s_inout_set_io *, s_output_hyd ***, s_chyd *, FILE *);
s_output_hyd *HYD_chain_outputs(s_output_hyd *, s_output_hyd *);
s_lp_pk_hyd *HYD_init_lp_pk(char *, double, int, double, int, s_chyd *);
s_ts_pk_hyd *HYD_chain_ts_pk(s_ts_pk_hyd *, s_ts_pk_hyd *);
s_lp_pk_hyd *HYD_chain_lp_pk(s_lp_pk_hyd *, s_lp_pk_hyd *);
void HYD_create_time_series(s_output_hyd *, s_output_hyd ***, int);
void HYD_create_long_profiles(s_output_hyd *, s_output_hyd ***, int);
void HYD_create_mass_balances(s_output_hyd *, s_output_hyd ***, int);
int HYD_find_river(char *, int, s_chyd *, FILE *);
void HYD_create_output_extents(s_output_hyd *, s_lp_pk_hyd *);
s_lp_pk_hyd *HYD_browse_lp_pk(s_lp_pk_hyd *, int);
void HYD_create_output_points(s_output_hyd *, s_ts_pk_hyd *);
s_ts_pk_hyd *HYD_browse_ts_pk(s_ts_pk_hyd *, int);
void HYD_calculate_output_domain(s_lp_pk_hyd *, int, s_chyd *, FILE *);
void HYD_calculate_ts_domain(s_ts_pk_hyd *, s_chyd *, FILE *);
void HYD_propage_output(s_reach_hyd *, int *, double, int, double, int, int *, int *, s_chyd *);
void HYD_test_path(int, double, s_reach_hyd *, int *);

/*in manage_reaches*/
void HYD_create_reaches(s_reach_hyd *, s_chyd *);
s_reach_hyd *HYD_create_reach(char *, char *, int, s_chyd *, FILE *);
s_reach_hyd *HYD_chain_reaches(s_reach_hyd *, s_reach_hyd *);
s_reach_hyd *HYD_chain_reaches_fwd(s_reach_hyd *, s_reach_hyd *);
s_reach_hyd *HYD_find_reach(char *, int, s_chyd *);
s_reach_hyd *HYD_find_reach_by_id(int, int, s_chyd *, FILE *);
s_reach_hyd *HYD_rewind_reach(s_reach_hyd *);
s_reach_hyd *HYD_browse_reach_chain(s_reach_hyd *, int, FILE *);
s_reach_hyd *HYD_secured_chain_reaches_fwd(s_reach_hyd *, s_reach_hyd *);
void HYD_assign_river(s_chyd *);
void HYD_set_reach_param(s_reach_hyd *, int, double, double, double, int, double, double, double, double, double, double, double, FILE *);
s_reach_hyd *HYD_create_reaches_musk(s_lec_tmp_hyd **, s_chyd *, int, char **, FILE *);

/* in manage_singularities.c */
void HYD_create_singularities(s_singularity_hyd *, s_chyd *, FILE *);
s_singularity_hyd *HYD_create_singularity(char *, s_chyd *);
s_singularity_hyd *HYD_chain_singularities(s_singularity_hyd *, s_singularity_hyd *);
s_singularity_hyd *HYD_chain_singularities_fwd(s_singularity_hyd *, s_singularity_hyd *);
s_singularity_hyd *HYD_find_singularity(char *, s_chyd *);
s_singularity_hyd *HYD_rewind_sing(s_singularity_hyd *);
s_singularity_hyd *HYD_browse_sing(s_singularity_hyd *, int);
void HYD_BC_faces(s_chyd *, FILE *);
s_BC_char_hyd *HYD_create_hydwork(int);
s_BC_char_hyd *HYD_chain_hydworks(s_BC_char_hyd *, s_BC_char_hyd *);
void HYD_verif_singularities(s_chyd *, FILE *);
char **HYD_finalyze_sing(s_singularity_hyd *, s_chyd *, s_chronos_CHR *, int, FILE *);
void HYD_print_sing_name(s_singularity_hyd *, FILE *);
s_singularity_hyd *HYD_create_default_singularity(char *, s_chyd *, s_chronos_CHR *, FILE *);
void HYD_print_sing(s_chyd *, FILE *);
s_singularity_hyd *HYD_create_musk_sing(s_lec_tmp_hyd **, s_chyd *, int);

/* in pk.c */
void HYD_calculate_pk_sing(s_chyd *, FILE *);
void HYD_propage_pk(s_singularity_hyd *, double, int, FILE *);
void HYD_module_calculation_pk(s_singularity_hyd *, double, int, FILE *);
void HYD_calculate_pk_faces_centers(s_chyd *, FILE *);
s_singularity_hyd *HYD_get_undefined_pk(s_chyd *, FILE *);

/* in print_output.c */
FILE *HYD_open_iterations_file(s_output_hyd ***, s_inout_set_io *, FILE *);
void HYD_print_hydraulic_state(FILE *, s_chyd *);
void HYD_print_final_state(s_output_hyd ***, s_inout_set_io *, s_chyd *, FILE *);
void HYD_print_pk(s_output_hyd ***, s_inout_set_io *, s_chyd *, FILE *);
void HYD_print_outputs_formats(s_output_hyd ***, s_inout_set_io *, s_chyd *, FILE *);
void HYD_long_profile_format(s_chyd *, s_output_hyd ***, s_inout_set_io *, FILE *);
void HYD_mass_balance_format(s_chyd *, s_output_hyd ***, s_inout_set_io *, FILE *);
void HYD_create_GNUPLOT_lp(int, s_output_hyd ***, s_inout_set_io *, FILE *);
void HYD_create_GNUPLOT_mb(int, s_output_hyd ***, s_inout_set_io *, FILE *);
void HYD_transv_profile_format(s_chyd *, s_output_hyd ***, s_inout_set_io *, FILE *);
void HYD_create_GNUPLOT_ts(int, s_output_hyd ***, s_inout_set_io *, FILE *);
void HYD_print_variables(FILE *, s_output_hyd *);
void HYD_print_outputs_old(double, s_output_hyd ***, s_inout_set_io *, s_chyd *, s_chronos_CHR *, FILE *);
void HYD_long_profile(double, s_output_hyd *, s_inout_set_io *, s_chyd *, FILE *);
void HYD_mass_balance(double, s_output_hyd *, s_chyd *, s_chronos_CHR *, FILE *);
void HYD_print_GNUPLOT_lp(s_output_hyd *, s_inout_set_io *, s_lp_pk_hyd *, int, double, FILE *);
void HYD_print_average_result(s_element_hyd *, double, s_output_hyd *, FILE *);
void HYD_transv_profile(double, s_output_hyd *, FILE *);
void HYD_create_mb_at_elements_file(s_output_hyd ***, s_inout_set_io *, FILE *);
void HYD_print_mb_at_elements(double, s_output_hyd ***, s_chyd *, s_chronos_CHR *, FILE *);
void HYD_print_times(s_clock_CHR *, s_inout_set_io *, FILE *);
// void HYD_print_outputs_formated(s_chronos_CHR *,s_chyd *,double,s_out_io *,int ,FILE *);
void HYD_print_outputs_all(s_chronos_CHR *, double, s_chyd *, s_out_io **, int, FILE *);
void HYD_print_header(FILE *, int);
void HYD_print_mb_formated(double, s_out_io *, s_chyd *, double **, FILE *);
void HYD_print_mb_for(double, s_element_hyd *, double **, FILE *, FILE *);
void HYD_print_H_formated(double, s_out_io *, s_chyd *, double **, FILE *);
void HYD_print_H_for(double, s_element_hyd *, double **, FILE *, FILE *);
void HYD_print_Q_formated(double, s_out_io *, s_chyd *, double **, FILE *);
void HYD_print_Q_for(double, s_element_hyd *, double **, FILE *, FILE *);
void HYD_print_corresp(s_chyd *, int, FILE *);
// void HYD_print_H_bin(s_out_io *,s_chyd *,FILE *);
// void HYD_print_Q_bin(s_out_io *,s_chyd *,FILE *);
// void HYD_print_mb_bin(s_out_io *,s_chyd *,FILE *);
void HYD_print_H(double, s_out_io *, s_chyd *, FILE *);
void HYD_print_Q(double, s_out_io *, s_chyd *, FILE *);
void HYD_print_mb(s_chronos_CHR *, double, s_out_io *, s_chyd *, FILE *);
void HYD_print_outputs(s_chronos_CHR *, s_chyd *, double, s_out_io *, int, FILE *);
double **HYD_get_MASS_val_bin(s_chronos_CHR *, s_chyd *, FILE *);
void HYD_print_abstract(s_chyd *, FILE *);
void HYD_print_ficvid(s_chyd *, char *, FILE *); // NG : 21/02/2020
int **HYD_create_ficvid(s_chyd *, FILE *);
void HYD_get_upstream(s_reach_hyd *, int **, FILE *);
int HYD_get_downstream(s_reach_hyd *, int **, FILE *);
void HYD_print_Q_fin_state(s_out_io *, s_chyd *, FILE *);
void HYD_print_Zbot(s_chyd *, FILE *);

/* in usual_functions.c */
void HYD_test(int);
void *HYD_free_p(void *);
double HYD_deg2rad(double);
int HYD_get_order(double);

/* in manage_carac_hydro.c */
s_chyd *HYD_create_chyd();
void HYD_init_chyd(s_chyd *);
void HYD_print_network(s_chyd *, FILE *);
s_settings_hyd *HYD_create_sets();

/* in manage_musk_face.c*/
s_face_hyd *HYD_free_chained_faces(s_face_hyd *, FILE *);
s_face_hyd *HYD_free_a_face(s_face_hyd *, FILE *);
s_face_hyd **HYD_free_tab_faces(s_face_hyd **, int, FILE *);
s_face_hyd **HYD_create_tab_faces(s_face_hyd *, int, s_chyd *, FILE *);
s_face_hyd *HYD_reorder_musk_faces(s_face_hyd *, s_chyd *, FILE *);
s_face_hyd *HYD_order_musk_face_in_tab(s_face_hyd *, s_reach_hyd *, s_chyd *, FILE *);
int HYD_count_ele_musk(s_face_hyd *);
s_face_hyd *HYD_concatene_faces(s_face_hyd **, s_chyd *, FILE *);
s_face_hyd *HYD_create_musk_face(char *, int, int, int, int, int, double, double, double, s_reach_hyd *, double *, s_chyd *, FILE *);
s_face_hyd *HYD_find_first_musk_face(s_face_hyd *, int, FILE *);
s_face_hyd *HYD_find_dwonstr_musk_face(s_face_hyd *, int, int, FILE *);
void HYD_print_faces(s_face_hyd *, FILE *);
s_face_hyd *HYD_order_in_reach(s_face_hyd *, s_face_hyd *, int, s_chyd *, FILE *);
s_face_hyd *HYD_copy_musk_faces(s_face_hyd *, s_chyd *, FILE *);

/*in manage_tmp_lec*/
s_lec_tmp_hyd *HYD_create_tmp_lec(char *, int, int, int, double, double, double, int, double, double, double, double, double, double, double, FILE *);
s_lec_tmp_hyd **HYD_resize_tmp_lec(s_lec_tmp_hyd **, int, int, FILE *);
s_lec_tmp_hyd **HYD_free_plec(s_lec_tmp_hyd **, int, FILE *);
int HYD_get_max_node(s_lec_tmp_hyd **, int, FILE *);

/*in manage_input.c*/
double **HYD_get_init_Q(FILE *, s_chyd *, FILE *);
