/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_carac_hydro.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"

s_chyd *HYD_create_chyd() {
  s_chyd *pchyd;

  pchyd = new_charac_hydro();
  bzero((char *)pchyd, sizeof(s_chyd));

  return pchyd;
}

void HYD_init_chyd(s_chyd *pchyd) {

  pchyd->counter = new_counter();
  bzero((char *)pchyd->counter, sizeof(s_counter_hyd));
  pchyd->p_sing = new_p_sing();
  pchyd->p_reach = new_p_reach();
  pchyd->mass_balance = new_mb();                       // NF 27/3/2012
  bzero((char *)pchyd->mass_balance, sizeof(s_mb_hyd)); // NF 27/3/2012

  HYD_initialize_mass_balance_t(pchyd->mass_balance); // NF 27/3/2012

  pchyd->settings = HYD_create_sets(); // FB 29/03/2018
  pchyd->settings->normal[ONE] = 1;
  pchyd->settings->normal[TWO] = -1;
  pchyd->settings->solver_type = GC_SOL;      // SW 11/07/2018
                                              //  psimul->calcul_hydro = new_petsc_calc();
  pchyd->calcul_hydro = GC_create_gc(HYD_GC); // LV nov2014
#ifdef OMP
  pchyd->settings->psmp = new_smp();
  pchyd->settings->psmp->nthreads = 1;
#endif
}

s_settings_hyd *HYD_create_sets() {

  s_settings_hyd *settings;

  settings = new_settings();
  bzero((char *)settings, sizeof(s_settings_hyd));
  settings->ndim = 1;
  settings->calc_state = STEADY;
  settings->calc_curvature = NO;
  settings->cs_shape = RECTANGULAR;
  settings->pk_defined = -1;
  settings->general_param[THETA] = 0.9;
  settings->general_param[DX] = CODE;
  settings->general_param[DY] = BASIC_DY;
  settings->general_param[DZ] = BASIC_DZ;
  settings->general_param[UP_HMIN] = 0.;
  settings->general_param[DOWN_HMAX] = 10.;
  settings->general_param[CURVATURE] = 0.;
  settings->general_param[STRICKLER] = 40.;
  settings->general_param[EPS_Q] = EPS_CONV;
  settings->general_param[EPS_Z] = EPS_CONV;
  settings->Kdef = TTRA;                  // NG : 05/08/2022 : Adding default init as TTRA for K_def method
  settings->hCalcWay = MANNING_STRICKLER; // NF 7/3/2023
  return settings;
}

void HYD_print_network(s_chyd *pchyd, FILE *fp) {
  int r, f, e, p;
  s_reach_hyd *preach;
  s_face_hyd *pface;
  s_element_hyd *pele;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];
    LP_printf(fp, " reach %d length %f, sing amont %s pk %f, sing aval %s pk %f\n", preach->id, preach->length, preach->limits[ONE]->name, preach->limits[ONE]->pk, preach->limits[TWO]->name, preach->limits[TWO]->pk);
    LP_printf(fp, " nb faces in reach : %d nb ele in reach : %d\n\n", preach->nfaces, preach->nele);
    for (f = 0; f < preach->nfaces; f++) {
      pface = preach->p_faces[f];
      LP_printf(fp, "face %d named %s fnode %d tnode %d length %f new_dx %f\n", pface->id, pface->name, pface->fnode, pface->tnode, pface->geometry->dx, pface->new_dx);
      for (p = 0; p < pface->geometry->npts; p++) {
        LP_printf(fp, "Geometry abscisse curviligne : %f Z : %f\n", pface->geometry->p_pointsAbscZ[p]->absc, pface->geometry->p_pointsAbscZ[p]->z);
      }
      LP_printf(fp, "Geometry via description : \n");
      LP_printf(fp, "Z_bottom %f,AbscLB : %f,AbscRB : %f,Absc_bot : %f\n", pface->description->Zbottom, pface->description->AbscLB, pface->description->AbscRB, pface->description->Abscbottom);
    }
    for (e = 0; f < preach->nele; e++) {
      pele = preach->p_ele[e];
      LP_printf(fp, "ele %d length %f face amont %s face aval %s\n", pele->id, pele->length, pele->face[X_HYD][ONE]->name, pele->face[X_HYD][TWO]->name);
    }
  }
}