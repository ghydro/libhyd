/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: curvature.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"

/* Structure containing the simulation characteristics */
// extern s_simul *Simul;

/* Calculation of the radius of curvature at the elements' centers ans faces */
void HYD_calculate_curvature_radius(s_chyd *pchyd) {
  /* Loop indexes */
  int r, e, f, i;
  /* Lambert coordinates of the center of curvature*/
  double xr, yr;
  /* Denominator to calculate xr and yr */
  double denom;
  /* Calculated curvature radius */
  double radius1, radius2;
  /* Number of interpolated faces between two raw sections */
  int ninterpol;
  /* Indexes of the raw sections considered */
  int p1, p2;
  /* Geometry of faces nb p1 and p2 */
  s_geometry_hyd *geometry1, *geometry2;
  s_pointXYZ_hyd *p1l, *p1r, *p2l, *p2r;
  /* Coordinates of the center of 2 cross-sections */
  double xc1, yc1;
  double xc2, yc2;
  /* Dot products */
  double scalar1, scalar2;
  /* Parameters of the lines passing through the two banks of 2 cross-sections */
  double a1, a2, b1, b2;
  /* Length from the last raw section */
  double tot_length;
  /* Current element and face */
  s_element_hyd *pele;
  s_face_hyd *pface;

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    p1 = 0, p2 = 1;

    while (p2 < pchyd->p_reach[r]->nfaces) {

      ninterpol = 0;

      /* Search for the next raw section defined by the user */
      while (pchyd->p_reach[r]->p_faces[p2]->def == INTERPOLATED_FACE) {
        ninterpol++;
        p2++;
      }

      if ((pchyd->p_reach[r]->p_faces[p1]->geometry->type == X_Y_Z) & (pchyd->p_reach[r]->p_faces[p2]->geometry->type == X_Y_Z)) {

        geometry1 = pchyd->p_reach[r]->p_faces[p1]->geometry;
        p1l = geometry1->p_pointsXYZ[0];
        p1r = geometry1->p_pointsXYZ[geometry1->npts - 1];
        geometry2 = pchyd->p_reach[r]->p_faces[p2]->geometry;
        p2l = geometry2->p_pointsXYZ[0];
        p2r = geometry2->p_pointsXYZ[geometry2->npts - 1];

        xc1 = (p1l->x + p1r->x) / 2.;
        yc1 = (p1l->y + p1r->y) / 2.;
        xc2 = (p2l->x + p2r->x) / 2.;
        yc2 = (p2l->y + p2r->y) / 2.;

        /* Calculation of the coordinates of the intersection point
         * of the two cross-sections at faces nb p1 and p2
         */
        denom = (p2l->y - p2r->y) * (p1l->x - p1r->x) - (p1l->y - p1r->y) * (p2l->x - p2r->x);

        if (denom == 0.) {
          /* In this case, the curvature radius is infinite
           * and the curvature is zero (straight reach)...
           */
          pchyd->p_reach[r]->p_faces[p1]->description->radius = CODE;
          pchyd->p_reach[r]->p_faces[p2]->description->radius = CODE;
          pchyd->p_reach[r]->p_faces[p1]->description->curvature = 0.;
          pchyd->p_reach[r]->p_faces[p2]->description->curvature = 0.;

          /* ... for all the interpolated faces between the raw sections
           * p1 and p2,too
           */
          for (i = 0; i < ninterpol; i++)
            pchyd->p_reach[r]->p_faces[p1 + i + 1]->description->radius = CODE;
          pchyd->p_reach[r]->p_faces[p1 + i + 1]->description->curvature = 0.;
        }

        /* Calculation of the coordinates of the intersection point */
        else {
          if (p1l->x == p1r->x) {
            xr = p1l->x;
            a2 = (p2l->y - p2r->y) / (p2l->x - p2r->x);
            b2 = (p2r->y * p2l->x - p2l->y * p2r->x) / (p2l->x - p2r->x);
            yr = a2 * xr + b2;
          } else if (p2l->x == p2r->x) {
            xr = p2l->x;
            a1 = (p1l->y - p1r->y) / (p1l->x - p1r->x);
            b1 = (p1r->y * p1l->x - p1l->y * p1r->x) / (p1l->x - p1r->x);
            yr = a1 * xr + b1;
          } else {
            a1 = (p1l->y - p1r->y) / (p1l->x - p1r->x);
            b1 = (p1r->y * p1l->x - p1l->y * p1r->x) / (p1l->x - p1r->x);

            a2 = (p2l->y - p2r->y) / (p2l->x - p2r->x);
            b2 = (p2r->y * p2l->x - p2l->y * p2r->x) / (p2l->x - p2r->x);

            xr = (b2 - b1) / (a1 - a2);
            yr = a1 * xr + b1;
          }

          /* The curvature radius is calculated as the distance between
           * the lowest point of the section and the center of curvature
           * The dot products enables to determine if it is positive or negative
           */
          scalar1 = (p1l->x - p1r->x) * (xr - p1r->x) + (p1l->y - p1r->y) * (yr - p1r->y);
          scalar2 = (p2l->x - p2r->x) * (xr - p2r->x) + (p2l->y - p2r->y) * (yr - p2r->y);
          if (scalar1 > 0)
            radius1 = sqrt(pow((xc1 - xr), 2) + pow((yc1 - yr), 2));
          else
            radius1 = -sqrt(pow((xc1 - xr), 2) + pow((yc1 - yr), 2));
          if (scalar2 > 0)
            radius2 = sqrt(pow((xc2 - xr), 2) + pow((yc2 - yr), 2));
          else
            radius2 = -sqrt(pow((xc2 - xr), 2) + pow((yc2 - yr), 2));

          if (p1 == 0) {
            pchyd->p_reach[r]->p_faces[p1]->description->radius = radius1;
            pchyd->p_reach[r]->p_faces[p2]->description->radius = radius2;
            pchyd->p_reach[r]->p_faces[p1]->description->curvature = 1. / radius1;
            pchyd->p_reach[r]->p_faces[p2]->description->curvature = 1. / radius2;
          } else {
            /* When the section isn't the 1st nor the last of the reach
             * the curvature is the mean of the curvatures calculated with the two adjacent faces
             */
            pchyd->p_reach[r]->p_faces[p1]->description->curvature += 1. / radius1;
            pchyd->p_reach[r]->p_faces[p1]->description->curvature /= 2.;
            pchyd->p_reach[r]->p_faces[p1]->description->radius = 1. / pchyd->p_reach[r]->p_faces[p1]->description->curvature;
            pchyd->p_reach[r]->p_faces[p2]->description->radius = radius2;
            pchyd->p_reach[r]->p_faces[p2]->description->curvature = 1. / radius2;
          }

          tot_length = 0.;

          /* Calculation of the curvature and radius at the interpolated faces
           * Interpolation of the curvature values and calculation of the radius
           */
          for (i = 0; i < ninterpol; i++) {
            pchyd->p_reach[r]->p_faces[p1 + i + 1]->description->curvature = pchyd->p_reach[r]->p_faces[p1]->description->curvature * (tot_length + pchyd->p_reach[r]->p_faces[p1 + i]->new_dx) + pchyd->p_reach[r]->p_faces[p2]->description->curvature * (geometry1->dx - (tot_length + pchyd->p_reach[r]->p_faces[p1 + i]->new_dx));
            pchyd->p_reach[r]->p_faces[p1 + i + 1]->description->curvature /= geometry1->dx;
            pchyd->p_reach[r]->p_faces[p1 + i + 1]->description->radius = 1. / pchyd->p_reach[r]->p_faces[p1 + i + 1]->description->curvature;

            tot_length += pchyd->p_reach[r]->p_faces[p1 + i]->new_dx;
          }
        }
      }

      p1 = p2, p2++;
    }

    /* Calculation of the curvature at the element centers
     * = mean of the curvatures at the adjacent faces
     */
    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {

      pele = pchyd->p_reach[r]->p_ele[e];

      pele->center->description->curvature = pele->face[X_HYD][ONE]->description->curvature + pele->face[X_HYD][TWO]->description->curvature;
      pele->center->description->curvature /= 2.;

      if (pchyd->p_reach[r]->p_ele[e]->center->description->curvature == 0.)
        pele->center->description->radius = CODE;
      else
        pele->center->description->radius = 1. / pele->center->description->curvature;

      pele->center->description->dK_ds = pele->face[X_HYD][TWO]->description->curvature - pele->face[X_HYD][ONE]->description->curvature;
      pele->center->description->dK_ds /= pele->length;
    }

    for (f = 0; f < pchyd->p_reach[r]->nfaces; f++) {

      pface = pchyd->p_reach[r]->p_faces[f];

      if (f == 0)
        pface->description->dK_ds = (pface->element[TWO]->center->description->curvature - pface->description->curvature) / (pface->element[TWO]->length / 2.);
      else if (f == pchyd->p_reach[r]->nfaces - 1)
        pface->description->dK_ds = (pface->description->curvature - pface->element[ONE]->center->description->curvature) / (pface->element[ONE]->length / 2.);
      else
        pface->description->dK_ds = (pface->element[TWO]->center->description->curvature - pface->element[ONE]->center->description->curvature) / (pface->element[TWO]->center->pk - pface->element[ONE]->center->pk);
    }
  }
}

void HYD_zero_curvature(s_chyd *pchyd, FILE *fp) {
  int r, f, e;
  // LP_printf(fp,"Enter HYD_zero_curvature\n"); // BL to debug
  for (r = 0; r < pchyd->counter->nreaches; r++) {

    for (f = 0; f < pchyd->p_reach[r]->nfaces; f++) {
      pchyd->p_reach[r]->p_faces[f]->description->curvature = 0.;
    }

    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {
      pchyd->p_reach[r]->p_ele[e]->center->description->curvature = 0.;
    }
  }
  // LP_printf(fp,"Out of HYD_zero_curvature\n"); // BL to debug
}
