/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_singularities.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"

/* Structure containing the simulation caracteristics */
// extern s_simul *Simul;

/* Initialisation of the singularities of the simulated network */
void HYD_create_singularities(s_singularity_hyd *psingtot, s_chyd *pchyd, FILE *fp) {
  int s;

  pchyd->p_sing = (s_singularity_hyd **)calloc(pchyd->counter->nsing, sizeof(s_singularity_hyd *));
  psingtot = HYD_rewind_sing(psingtot);

  for (s = 0; s < pchyd->counter->nsing; s++) {
    pchyd->p_sing[s] = psingtot;
    if (psingtot->pk != CODE) {
      if (pchyd->settings->pk_defined != -1) {

        LP_warning(fp, "Warning! pk defined at more than one singularity!\n");
        LP_warning(fp, "The pk at singularity nb %d is taken into account\n", s);
      }
      pchyd->settings->pk_defined = s;
    }
    psingtot = psingtot->next;
  }
  if (psingtot != NULL)
    LP_error(fp, "verif nsing !! il y a encore des singularités !!\n");
}

/* Creation of a new singularity structure */
s_singularity_hyd *HYD_create_singularity(char *name, s_chyd *pchyd) {
  s_singularity_hyd *psing;

  psing = new_singularity();
  bzero((char *)psing, sizeof(s_singularity_hyd));

  LP_lowercase(name);
  psing->name = strdup(name);
  psing->id[INTERN_HYD] = pchyd->counter->nsing;
  psing->id[ABS_HYD] = pchyd->counter->nsing;
  psing->pk = CODE;
  psing->Z_init = CODE;
  psing->type = CODE;
  psing->passage = NO;
  psing->nworks = 0;

  pchyd->counter->nsing++;

  return psing;
}

/* Chains two singularities */
s_singularity_hyd *HYD_chain_singularities(s_singularity_hyd *psing1, s_singularity_hyd *psing2) {
  if ((psing1 != NULL) && (psing2 != NULL)) {
    psing1->next = psing2;
    psing2->prev = psing1;
  }

  return psing1;
}

/* Chains two singularities */
s_singularity_hyd *HYD_chain_singularities_fwd(s_singularity_hyd *psing1, s_singularity_hyd *psing2) {
  if ((psing1 != NULL)) {
    psing1->next = psing2;
    psing2->prev = psing1;
  }

  return psing2;
}

/* Finds a singularity in Simul->p_sing with its name */
s_singularity_hyd *HYD_find_singularity(char *name, s_chyd *pchyd) {
  /* Integer defining wether or not the singularity was found */
  int found = NO;
  /* Loop index */
  int i = 0;

  LP_lowercase(name);

  while ((found == NO) && (i < pchyd->counter->nsing)) {
    if (strcmp(pchyd->p_sing[i]->name, name) == 0)
      found = YES;
    else
      i++;
  }

  return pchyd->p_sing[i];
}

/* Rewinds a chain of singularities */
s_singularity_hyd *HYD_rewind_sing(s_singularity_hyd *psing) {
  s_singularity_hyd *psing_temp;

  psing_temp = psing;

  if (psing != NULL) {
    while (psing_temp->prev != NULL)
      psing_temp = psing_temp->prev;
  }

  return psing_temp;
}

/* Rewinds a chain of singularities */
s_singularity_hyd *HYD_browse_sing(s_singularity_hyd *psing, int beg_end) {
  s_singularity_hyd *psing_temp;

  psing_temp = psing;

  if (psing != NULL) {
    if (beg_end == BEGINNING) {
      while (psing_temp->prev != NULL)
        psing_temp = psing_temp->prev;
    } else {
      while (psing_temp->next != NULL)
        psing_temp = psing_temp->next;
    }
  }

  return psing_temp;
}

void HYD_BC_faces(s_chyd *pchyd, FILE *fp) {
  int s, i;
  int nworkup1, nworkup2, nworkdown1, nworkdown2;
  /* Current singularity */
  s_singularity_hyd *psing;

  for (s = 0; s < pchyd->counter->nsing; s++) {

    psing = pchyd->p_sing[s];

    if (psing->ndownstr_reaches == 1) {

      // psing->faces[DOWNSTREAM][0]->BC_char = ((s_BC_char_hyd **) calloc(psing->nworks,sizeof(s_BC_char_hyd *)));
      psing->faces[DOWNSTREAM][0]->nworks = psing->nworks;
      psing->faces[DOWNSTREAM][0]->BC_char = psing->BC_char;

      if (psing->nupstr_reaches == 0) {
        if (psing->type == CODE)
          psing->type = DISCHARGE; // definition sing discharge si pas d'upstream au reach
        if (psing->faces[DOWNSTREAM][0]->pt_inflows == NULL) {
          psing->faces[DOWNSTREAM][0]->pt_inflows = (s_pt_inflow_hyd **)malloc(sizeof(s_pt_inflow_hyd *));
          psing->faces[DOWNSTREAM][0]->pt_inflows[0] = new_pt_inflow();
          psing->faces[DOWNSTREAM][0]->pt_inflows[0]->name = psing->name;
          psing->faces[DOWNSTREAM][0]->pt_inflows[0]->discharge = TS_create_function(0., 0.);
        }

      }

      else if (psing->nupstr_reaches == 1) {

        // psing->faces[UPSTREAM][0]->BC_char = ((s_BC_char_hyd **) calloc(psing->nworks,sizeof(s_BC_char_hyd *)));
        psing->faces[UPSTREAM][0]->nworks = psing->nworks;
        psing->faces[UPSTREAM][0]->BC_char = psing->BC_char;

        if (psing->type == CODE)
          psing->type = CONTINU; // si 1 upstream et pas de hydrowork
      }
    }

    else if (psing->nupstr_reaches == 1) {

      psing->faces[UPSTREAM][0]->BC_char = ((s_BC_char_hyd **)calloc(psing->nworks, sizeof(s_BC_char_hyd *)));
      psing->faces[UPSTREAM][0]->nworks = psing->nworks;
      psing->faces[UPSTREAM][0]->BC_char = psing->BC_char;
    }

    /* If several downstream reaches : DIFFLUENCE */
    // if (psing->ndownstr_reaches > 1) {
    if (psing->ndownstr_reaches > 1 && psing->ndownstr_reaches < 3) { // SW 20/11/2019 add TRIDIFFLUENCE

      if (psing->type == CODE)
        psing->type = DIFFLUENCE; // si 2 downstream

      else if ((psing->type == RATING_CURVE) || (psing->type == WATER_LEVEL)) {

        if (psing->BC_char[0]->position[0] == UPSTREAM) {

          psing->type = WORK_DIFF;
          psing->faces[UPSTREAM][psing->BC_char[0]->position[1]]->nworks = 1;
          psing->faces[UPSTREAM][psing->BC_char[0]->position[1]]->BC_char = ((s_BC_char_hyd **)calloc(1, sizeof(s_BC_char_hyd *)));
          psing->faces[UPSTREAM][psing->BC_char[0]->position[1]]->BC_char[0] = psing->BC_char[0];
        }

        else if (psing->BC_char[0]->position[0] == DOWNSTREAM) {

          psing->type = DIFF_WORK;
          psing->faces[DOWNSTREAM][psing->BC_char[0]->position[1]]->nworks = 1;
          psing->faces[DOWNSTREAM][psing->BC_char[0]->position[1]]->BC_char = ((s_BC_char_hyd **)calloc(1, sizeof(s_BC_char_hyd *)));
          psing->faces[DOWNSTREAM][psing->BC_char[0]->position[1]]->BC_char[0] = psing->BC_char[0];
        }
      }

      else if (psing->type == HYDWORK) {

        nworkup1 = 0, nworkdown1 = 0, nworkdown2 = 0;

        for (i = 0; i < psing->nworks; i++) {

          if (psing->BC_char[i]->position[0] == UPSTREAM)
            nworkup1++;

          if ((psing->BC_char[i]->position[0] == DOWNSTREAM) && (psing->BC_char[i]->position[1] == 0))
            nworkdown1++;

          if ((psing->BC_char[i]->position[0] == DOWNSTREAM) && (psing->BC_char[i]->position[1] == 1))
            nworkdown2++;
        }

        if ((nworkup1 > 0) && ((nworkdown1 > 0) || (nworkdown2 > 0)))
          LP_error(fp, "There cannot be 2 dams upstream and downstream a diffluence : problem at singularity %s\n", psing->name);

        else if (nworkup1 > 0)
          psing->type = WORK_DIFF;

        else if ((nworkdown1 > 0) && (nworkdown2 > 0))
          psing->type = DIFF_WORKS;

        else
          psing->type = DIFF_WORK;

        psing->faces[UPSTREAM][0]->nworks = nworkup1;
        psing->faces[DOWNSTREAM][0]->nworks = nworkdown1;
        psing->faces[DOWNSTREAM][1]->nworks = nworkdown2;
        psing->faces[UPSTREAM][0]->BC_char = ((s_BC_char_hyd **)calloc(nworkup1, sizeof(s_BC_char_hyd *)));
        psing->faces[DOWNSTREAM][0]->BC_char = ((s_BC_char_hyd **)calloc(nworkdown1, sizeof(s_BC_char_hyd *)));
        psing->faces[DOWNSTREAM][1]->BC_char = ((s_BC_char_hyd **)calloc(nworkdown2, sizeof(s_BC_char_hyd *)));

        nworkup1 = 0, nworkdown1 = 0, nworkdown2 = 0;

        for (i = 0; i < psing->nworks; i++) {

          if (psing->BC_char[i]->position[0] == UPSTREAM) {
            psing->faces[UPSTREAM][0]->BC_char[nworkup1] = psing->BC_char[i];
            nworkup1++;
          }

          if ((psing->BC_char[i]->position[0] == DOWNSTREAM) && (psing->BC_char[i]->position[1] == 0)) {
            psing->faces[DOWNSTREAM][0]->BC_char[nworkdown1] = psing->BC_char[i];
            nworkdown1++;
          }

          if ((psing->BC_char[i]->position[0] == DOWNSTREAM) && (psing->BC_char[i]->position[1] == 1)) {
            psing->faces[DOWNSTREAM][1]->BC_char[nworkdown2] = psing->BC_char[i];
            nworkdown2++;
          }
        }
      }
    }
    if (psing->ndownstr_reaches == 3) { // SW AR NG TEST pour 3 branches

      if (psing->type == CODE)
        psing->type = TRIDIFFLUENCE;
      else
        LP_error(fp, "Three downstream reaches, but limit type defined as %s. Need modify the code or the input data\n", HYD_name_sing_type(psing->type));
    }
    /* If several upstream reaches : CONFLUENCE */
    // if (psing->nupstr_reaches > 1) {
    if (psing->nupstr_reaches > 1 && psing->nupstr_reaches < 3) {

      if (psing->type == CODE)
        psing->type = CONFLUENCE;

      else if (psing->type == DIFFLUENCE)
        psing->type = CONF_DIFF;

      else if ((psing->type == RATING_CURVE) || (psing->type == WATER_LEVEL)) {

        if (psing->BC_char[0]->position[0] == DOWNSTREAM) {

          psing->type = CONF_WORK;
          psing->faces[DOWNSTREAM][psing->BC_char[0]->position[1]]->nworks = 1;
          psing->faces[DOWNSTREAM][psing->BC_char[0]->position[1]]->BC_char = ((s_BC_char_hyd **)calloc(1, sizeof(s_BC_char_hyd *)));
          psing->faces[DOWNSTREAM][psing->BC_char[0]->position[1]]->BC_char[0] = psing->BC_char[0];
        }

        else if (psing->BC_char[0]->position[0] == UPSTREAM) {

          psing->type = WORK_CONF;
          psing->faces[UPSTREAM][psing->BC_char[0]->position[1]]->nworks = 1;
          psing->faces[DOWNSTREAM][psing->BC_char[0]->position[1]]->BC_char = ((s_BC_char_hyd **)calloc(1, sizeof(s_BC_char_hyd *)));
          psing->faces[DOWNSTREAM][psing->BC_char[0]->position[1]]->BC_char[0] = psing->BC_char[0];
        }
      }

      else if (psing->type == HYDWORK) {

        nworkup1 = 0, nworkup2 = 0, nworkdown1 = 0;

        for (i = 0; i < psing->nworks; i++) {

          if (psing->BC_char[i]->position[0] == DOWNSTREAM)
            nworkdown1++;

          if ((psing->BC_char[i]->position[0] == UPSTREAM) && (psing->BC_char[i]->position[1] == 0))
            nworkup1++;

          if ((psing->BC_char[i]->position[0] == UPSTREAM) && (psing->BC_char[i]->position[1] == 1))
            nworkup2++;
        }

        if ((nworkdown1 > 0) && ((nworkup1 > 0) || (nworkup2 > 0)))
          LP_error(fp, "There cannot be 2 dams upstream and downstream a confluence : problem at singularity %s\n", psing->name);

        else if (nworkdown1 > 0)
          psing->type = CONF_WORK;

        else if ((nworkup1 > 0) && (nworkup2 > 0))
          psing->type = WORKS_CONF;

        else
          psing->type = WORK_CONF;

        psing->faces[UPSTREAM][0]->nworks = nworkup1;
        psing->faces[UPSTREAM][1]->nworks = nworkup2;
        psing->faces[DOWNSTREAM][0]->nworks = nworkdown1;
        psing->faces[UPSTREAM][0]->BC_char = ((s_BC_char_hyd **)calloc(nworkup1, sizeof(s_BC_char_hyd *)));
        psing->faces[UPSTREAM][1]->BC_char = ((s_BC_char_hyd **)calloc(nworkup2, sizeof(s_BC_char_hyd *)));
        psing->faces[DOWNSTREAM][0]->BC_char = ((s_BC_char_hyd **)calloc(nworkdown1, sizeof(s_BC_char_hyd *)));

        nworkdown1 = 0, nworkup1 = 0, nworkup2 = 0;

        for (i = 0; i < psing->nworks; i++) {

          if (psing->BC_char[i]->position[0] == DOWNSTREAM) {
            psing->faces[DOWNSTREAM][0]->BC_char[nworkdown1] = psing->BC_char[i];
            nworkdown1++;
          }

          if ((psing->BC_char[i]->position[0] == UPSTREAM) && (psing->BC_char[i]->position[1] == 0)) {
            psing->faces[UPSTREAM][0]->BC_char[nworkup1] = psing->BC_char[i];
            nworkup1++;
          }

          if ((psing->BC_char[i]->position[0] == UPSTREAM) && (psing->BC_char[i]->position[1] == 1)) {
            psing->faces[UPSTREAM][1]->BC_char[nworkup2] = psing->BC_char[i];
            nworkup2++;
          }
        }
      }
    }
    if (psing->nupstr_reaches == 3) { // SW 20/11/2019 add TRIDIFFLUENCE

      if (psing->type == CODE)
        psing->type = TRICONFLUENCE;
      else
        LP_error(fp, "Three upstream reaches, but limit type defined as %s. Need modify the code or the input data\n", HYD_name_sing_type(psing->type));
    }
  }
}

s_BC_char_hyd *HYD_create_hydwork(int worktype) {
  s_BC_char_hyd *pwork;

  pwork = new_BC_char();
  bzero((char *)pwork, sizeof(s_BC_char_hyd));

  pwork->hydwork_type = worktype;

  pwork->hydwork_param[MU] = 0.4;
  pwork->hydwork_param[WIDTH] = CODE;

  return pwork;
}

s_BC_char_hyd *HYD_chain_hydworks(s_BC_char_hyd *pw1, s_BC_char_hyd *pw2) {
  pw1->next = pw2;
  pw2->prev = pw1;

  return pw1;
}

/* Function to check if the user has correctly defined the hydwork singularities */
void HYD_verif_singularities(s_chyd *pchyd, FILE *fp) {
  int s, i;

  s_singularity_hyd *limit;

  for (s = 0; s < pchyd->counter->nsing; s++) {

    limit = pchyd->p_sing[s];

    if (limit->nupstr_reaches > 2)
      // LP_error(fp,"Singularity %s has more than 2 upstream reaches\n",limit->name);
      LP_warning(fp, "Singularity %s has more than 2 upstream reaches : Number of upstream reaches : %d\n", limit->name, limit->nupstr_reaches);
    if (limit->ndownstr_reaches > 2)
      // LP_error(fp,"Singularity %s has more than 2 downstream reaches\n",limit->name);
      LP_warning(fp, "Singularity %s has more than 2 downstream reaches : Number of downstream reaches : %d\n", limit->name, limit->ndownstr_reaches);

    if (limit->type == HYDWORK) {

      if ((limit->ndownstr_reaches == 0) && ((limit->nworks > 1) || (limit->BC_char[0]->fion_type != ZT))) {

        LP_error(fp, "A downstream hydraulic work can only be represented with one weir described by a Z(t) function\n");
      }

      for (i = 0; i < limit->nworks; i++) {

        if (limit->BC_char[i]->hydwork_param[WIDTH] == CODE)
          LP_error(fp, "No given width for hydwork constituing %s singularity\n", limit->name);

        if ((limit->nworks > 1) && (limit->BC_char[i]->fion_type == ZT))
          LP_error(fp, "A hydraulic work described by a Z(t) function cannot have several weirs or gates\n");

        // else if ((limit->nworks > 1) && (limit->BC_char[i]->fion_type == ZQ))
        // LP_error(fp,"A hydraulic work described by a Z(Q) function cannot have several weirs or gates\n");
      }
    }
  }
}

char **HYD_finalyze_sing(s_singularity_hyd *psingtot, s_chyd *pchyd, s_chronos_CHR *chronos, int max_FnodeTnode, FILE *fp) {

  char **FnodeTnode;
  s_singularity_hyd *psing_tmp, *psing_tmp2, *psing, *psing_outlet;
  s_id_io *id_list;
  int nb_Tnode, str_len, i;
  int Fnode, Tnode, dim_char;
  int nb_outlet = 0;

  psing_tmp = HYD_rewind_sing(psingtot);
  FnodeTnode = (char **)malloc((2 * max_FnodeTnode) * sizeof(char *));
  for (i = 0; i < 2 * max_FnodeTnode; i++) {
    FnodeTnode[i] = NULL;
  }
  psing_outlet = NULL;
  while (psing_tmp != NULL) {
    Fnode = psing_tmp->Fnode;
    Tnode = psing_tmp->Tnode;
    str_len = strlen(psing_tmp->name) + 1;
    FnodeTnode[psing_tmp->Fnode] = (char *)malloc(str_len * sizeof(char));
    sprintf(FnodeTnode[psing_tmp->Fnode], "%s", psing_tmp->name);
    // LP_printf(fp,"Dealing with river %s, Fnode : %d Tnode : %d\n",FnodeTnode[psing_tmp->Fnode],Fnode,Tnode); //BL to debug
    psing_tmp2 = HYD_rewind_sing(psingtot);
    nb_Tnode = 0;
    while (psing_tmp2 != NULL) {

      if (Tnode == psing_tmp2->Fnode) {
        nb_Tnode++;
        break;
      }
      psing_tmp2 = psing_tmp2->next;
    }
    if (nb_Tnode == 0) {
      // LP_printf(fp,"Outlet reached for Fnode : %d Tnode : %d\nEnd of river %s \n",Fnode,Tnode,FnodeTnode[psing_tmp->Fnode]); //BL to debug

      if (FnodeTnode[psing_tmp->Tnode] == NULL) {
        FnodeTnode[psing_tmp->Tnode] = (char *)calloc(20, sizeof(char));
        sprintf(FnodeTnode[psing_tmp->Tnode], "%s_%d", "outlet", nb_outlet);
        //  LP_printf(fp,"Sing_outlet = %s\n",FnodeTnode[psing_tmp->Tnode]); // BL to debug
        psing = HYD_create_default_singularity(FnodeTnode[psing_tmp->Tnode], pchyd, chronos, fp);
        psing_outlet = HYD_chain_singularities_fwd(psing_outlet, psing);
        nb_outlet = nb_outlet + 1;
      } else {
        LP_error(fp, "A Bassin outlet cannot be a confluence, Tnode %d already stored, check your input files \n", psing_tmp->Tnode);
      }
    }
    psing_tmp = psing_tmp->next;
  }

  LP_printf(fp, " %d Outlet(s) have been found \n", nb_outlet);
  psingtot = HYD_browse_sing(psingtot, END);
  psing_outlet = HYD_browse_sing(psing_outlet, BEGINNING);
  psingtot = HYD_chain_singularities_fwd(psingtot, psing_outlet);
  //  HYD_print_sing_name(psingtot,fp); //BL to debug
  return FnodeTnode;
}

void HYD_print_sing_name(s_singularity_hyd *psingtot, FILE *fp) {

  s_singularity_hyd *psing;
  psing = HYD_rewind_sing(psingtot);
  while (psing != NULL) {
    LP_printf(fp, "Singularity %d named %s \n", psing->id[ABS_HYD], psing->name);
    psing = psing->next;
  }
}

s_singularity_hyd *HYD_create_default_singularity(char *name, s_chyd *pchyd, s_chronos_CHR *chronos, FILE *fp) {
  s_singularity_hyd *psing;

  psing = HYD_create_singularity(name, pchyd);
  psing->BC_char = ((s_BC_char_hyd **)calloc(1, sizeof(s_BC_char_hyd *)));
  psing->BC_char[0] = HYD_create_hydwork(NONE);
  psing->nworks = 1;
  psing->BC_char[0]->fion_type = ZT;
  psing->BC_char[0]->fion = TS_create_function(0., 1.);
  psing->BC_char[0]->fion_t = TS_function_value_t(chronos->t[INI], psing->BC_char[0]->fion, fp);
  psing->type = WATER_LEVEL;
  return psing;
}

void HYD_print_sing(s_chyd *pchyd, FILE *fp) {
  int s;
  s_singularity_hyd *psing;
  for (s = 0; s < pchyd->counter->nsing; s++) {

    psing = pchyd->p_sing[s];
    LP_printf(fp, "singularirty %d named %s type %s downstream_nb %d upstream_nb %d\n", psing->id[ABS_HYD], psing->name, HYD_name_sing_type(psing->type), psing->ndownstr_reaches, psing->nupstr_reaches);
    // LP_printf(fp,"linked with upstream face %s,downstream face %s \n",psing->faces[UPSTREAM][0]->name,psing->faces[DOWNSTREAM][0]->name);
  }
}

s_singularity_hyd *HYD_create_musk_sing(s_lec_tmp_hyd **plec, s_chyd *pchyd, int dim_tmp_lec) {

  int i, j;
  s_singularity_hyd *psing, *psing_tmp;
  psing = NULL;
  for (i = 0; i < dim_tmp_lec; i++) {
    if (plec[i] != NULL) {
      psing_tmp = HYD_create_singularity(plec[i]->name, pchyd);
      psing_tmp->Fnode = plec[i]->Fnode;
      psing_tmp->Tnode = plec[i]->Tnode;
      psing = HYD_chain_singularities_fwd(psing, psing_tmp);
    }
  }
  psing = HYD_rewind_sing(psing);
  return psing;
}
