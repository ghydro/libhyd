/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: main_test_HyCu_Flumy.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include "param_HYDRO.h"
#include "struct_HYDRO.h"
// #include "global_var.h"
#include "functions.h"
#include "HyCu_Flumy.h"
#include "petscksp.h"
// #include "../../lib_time_series/manage_functions.h"

/* Structure containing the simulation characteristics */
extern s_simul *Simul;

int main(int argc, char **argv) {
  int nsections = 1750;
  double **carac_sections;
  double Q_up = 1000.;
  double Ks = 50.;
  double **outputs;
  int f;

  carac_sections = (double **)calloc(nsections, sizeof(double));
  for (f = 0; f < nsections; f++)
    carac_sections[f] = (double *)calloc(6, sizeof(double));
  outputs = (double **)calloc(nsections, sizeof(double));
  for (f = 0; f < nsections; f++)
    outputs[f] = (double *)calloc(2, sizeof(double));

  // section 0
  carac_sections[0][0] = 0.00;
  carac_sections[0][1] = 0.00;
  carac_sections[0][2] = 30.00;
  carac_sections[0][3] = 100.00;
  outputs[0][0] = 3.17;
  outputs[0][1] = 5.16;

  // section 1
  carac_sections[1][0] = 65.12;
  carac_sections[1][1] = 0.00;
  carac_sections[1][2] = 29.94;
  carac_sections[1][3] = 100.00;
  outputs[1][0] = 3.17;
  outputs[1][1] = 5.16;

  // section 2
  carac_sections[2][0] = 106.33;
  carac_sections[2][1] = 0.00;
  carac_sections[2][2] = 29.90;
  carac_sections[2][3] = 100.00;
  outputs[2][0] = 4.69;
  outputs[2][1] = 3.48;

  // section 3
  carac_sections[3][0] = 151.36;
  carac_sections[3][1] = 0.00;
  carac_sections[3][2] = 29.85;
  carac_sections[3][3] = 100.00;
  outputs[3][0] = 4.71;
  outputs[3][1] = 3.47;

  // section 4
  carac_sections[4][0] = 214.65;
  carac_sections[4][1] = 0.00;
  carac_sections[4][2] = 29.79;
  carac_sections[4][3] = 100.00;
  outputs[4][0] = 4.73;
  outputs[4][1] = 3.45;

  // section 5
  carac_sections[5][0] = 258.19;
  carac_sections[5][1] = 0.00;
  carac_sections[5][2] = 29.75;
  carac_sections[5][3] = 100.00;
  outputs[5][0] = 4.75;
  outputs[5][1] = 3.44;

  // section 6
  carac_sections[6][0] = 302.71;
  carac_sections[6][1] = 0.00;
  carac_sections[6][2] = 29.71;
  carac_sections[6][3] = 100.00;
  outputs[6][0] = 4.77;
  outputs[6][1] = 3.43;

  // section 7
  carac_sections[7][0] = 371.27;
  carac_sections[7][1] = 0.00;
  carac_sections[7][2] = 29.64;
  carac_sections[7][3] = 100.00;
  outputs[7][0] = 4.80;
  outputs[7][1] = 3.40;

  // section 8
  carac_sections[8][0] = 422.17;
  carac_sections[8][1] = 0.00;
  carac_sections[8][2] = 29.59;
  carac_sections[8][3] = 100.00;
  outputs[8][0] = 4.83;
  outputs[8][1] = 3.38;

  // section 9
  carac_sections[9][0] = 474.07;
  carac_sections[9][1] = 0.00;
  carac_sections[9][2] = 29.54;
  carac_sections[9][3] = 100.00;
  outputs[9][0] = 4.85;
  outputs[9][1] = 3.36;

  // section 10
  carac_sections[10][0] = 512.94;
  carac_sections[10][1] = 0.00;
  carac_sections[10][2] = 29.50;
  carac_sections[10][3] = 100.00;
  outputs[10][0] = 4.87;
  outputs[10][1] = 3.35;

  // section 11
  carac_sections[11][0] = 552.18;
  carac_sections[11][1] = 0.00;
  carac_sections[11][2] = 29.46;
  carac_sections[11][3] = 100.00;
  outputs[11][0] = 4.89;
  outputs[11][1] = 3.34;

  // section 12
  carac_sections[12][0] = 625.04;
  carac_sections[12][1] = 0.00;
  carac_sections[12][2] = 29.39;
  carac_sections[12][3] = 100.00;
  outputs[12][0] = 4.93;
  outputs[12][1] = 3.31;

  // section 13
  carac_sections[13][0] = 694.69;
  carac_sections[13][1] = 0.00;
  carac_sections[13][2] = 29.32;
  carac_sections[13][3] = 100.00;
  outputs[13][0] = 4.96;
  outputs[13][1] = 3.29;

  // section 14
  carac_sections[14][0] = 756.51;
  carac_sections[14][1] = 0.00;
  carac_sections[14][2] = 29.27;
  carac_sections[14][3] = 100.00;
  outputs[14][0] = 4.99;
  outputs[14][1] = 3.27;

  // section 15
  carac_sections[15][0] = 798.91;
  carac_sections[15][1] = 0.00;
  carac_sections[15][2] = 29.23;
  carac_sections[15][3] = 100.00;
  outputs[15][0] = 5.00;
  outputs[15][1] = 3.26;

  // section 16
  carac_sections[16][0] = 841.08;
  carac_sections[16][1] = 0.00;
  carac_sections[16][2] = 29.20;
  carac_sections[16][3] = 100.00;
  outputs[16][0] = 5.02;
  outputs[16][1] = 3.25;

  // section 17
  carac_sections[17][0] = 898.36;
  carac_sections[17][1] = 0.00;
  carac_sections[17][2] = 29.15;
  carac_sections[17][3] = 100.00;
  outputs[17][0] = 5.04;
  outputs[17][1] = 3.24;

  // section 18
  carac_sections[18][0] = 954.34;
  carac_sections[18][1] = 0.00;
  carac_sections[18][2] = 29.10;
  carac_sections[18][3] = 100.00;
  outputs[18][0] = 5.05;
  outputs[18][1] = 3.23;

  // section 19
  carac_sections[19][0] = 1012.29;
  carac_sections[19][1] = 0.00;
  carac_sections[19][2] = 29.06;
  carac_sections[19][3] = 100.00;
  outputs[19][0] = 5.07;
  outputs[19][1] = 3.22;

  // section 20
  carac_sections[20][0] = 1072.47;
  carac_sections[20][1] = 0.00;
  carac_sections[20][2] = 29.02;
  carac_sections[20][3] = 100.00;
  outputs[20][0] = 5.08;
  outputs[20][1] = 3.21;

  // section 21
  carac_sections[21][0] = 1113.74;
  carac_sections[21][1] = 0.00;
  carac_sections[21][2] = 28.99;
  carac_sections[21][3] = 100.00;
  outputs[21][0] = 5.09;
  outputs[21][1] = 3.21;

  // section 22
  carac_sections[22][0] = 1155.09;
  carac_sections[22][1] = 0.00;
  carac_sections[22][2] = 28.97;
  carac_sections[22][3] = 100.00;
  outputs[22][0] = 5.09;
  outputs[22][1] = 3.21;

  // section 23
  carac_sections[23][0] = 1221.92;
  carac_sections[23][1] = 0.00;
  carac_sections[23][2] = 28.93;
  carac_sections[23][3] = 100.00;
  outputs[23][0] = 5.10;
  outputs[23][1] = 3.20;

  // section 24
  carac_sections[24][0] = 1294.83;
  carac_sections[24][1] = 0.00;
  carac_sections[24][2] = 28.89;
  carac_sections[24][3] = 100.00;
  outputs[24][0] = 5.10;
  outputs[24][1] = 3.20;

  // section 25
  carac_sections[25][0] = 1352.27;
  carac_sections[25][1] = 0.00;
  carac_sections[25][2] = 28.86;
  carac_sections[25][3] = 100.00;
  outputs[25][0] = 5.10;
  outputs[25][1] = 3.20;

  // section 26
  carac_sections[26][0] = 1390.03;
  carac_sections[26][1] = 0.00;
  carac_sections[26][2] = 28.84;
  carac_sections[26][3] = 100.00;
  outputs[26][0] = 5.09;
  outputs[26][1] = 3.21;

  // section 27
  carac_sections[27][0] = 1426.24;
  carac_sections[27][1] = 0.00;
  carac_sections[27][2] = 28.83;
  carac_sections[27][3] = 100.00;
  outputs[27][0] = 5.09;
  outputs[27][1] = 3.21;

  // section 28
  carac_sections[28][0] = 1482.65;
  carac_sections[28][1] = 0.00;
  carac_sections[28][2] = 28.81;
  carac_sections[28][3] = 100.00;
  outputs[28][0] = 5.08;
  outputs[28][1] = 3.21;

  // section 29
  carac_sections[29][0] = 1537.30;
  carac_sections[29][1] = 0.00;
  carac_sections[29][2] = 28.79;
  carac_sections[29][3] = 100.00;
  outputs[29][0] = 5.07;
  outputs[29][1] = 3.22;

  // section 30
  carac_sections[30][0] = 1597.76;
  carac_sections[30][1] = 0.00;
  carac_sections[30][2] = 28.77;
  carac_sections[30][3] = 100.00;
  outputs[30][0] = 5.05;
  outputs[30][1] = 3.23;

  // section 31
  carac_sections[31][0] = 1656.35;
  carac_sections[31][1] = 0.00;
  carac_sections[31][2] = 28.75;
  carac_sections[31][3] = 100.00;
  outputs[31][0] = 5.04;
  outputs[31][1] = 3.24;

  // section 32
  carac_sections[32][0] = 1717.34;
  carac_sections[32][1] = 0.00;
  carac_sections[32][2] = 28.73;
  carac_sections[32][3] = 100.00;
  outputs[32][0] = 5.02;
  outputs[32][1] = 3.25;

  // section 33
  carac_sections[33][0] = 1779.40;
  carac_sections[33][1] = 0.00;
  carac_sections[33][2] = 28.71;
  carac_sections[33][3] = 100.00;
  outputs[33][0] = 4.99;
  outputs[33][1] = 3.27;

  // section 34
  carac_sections[34][0] = 1844.52;
  carac_sections[34][1] = 0.00;
  carac_sections[34][2] = 28.69;
  carac_sections[34][3] = 100.00;
  outputs[34][0] = 4.97;
  outputs[34][1] = 3.28;

  // section 35
  carac_sections[35][0] = 1911.73;
  carac_sections[35][1] = 0.00;
  carac_sections[35][2] = 28.68;
  carac_sections[35][3] = 100.00;
  outputs[35][0] = 4.95;
  outputs[35][1] = 3.30;

  // section 36
  carac_sections[36][0] = 1944.51;
  carac_sections[36][1] = 0.00;
  carac_sections[36][2] = 28.67;
  carac_sections[36][3] = 100.00;
  outputs[36][0] = 4.93;
  outputs[36][1] = 3.31;

  // section 37
  carac_sections[37][0] = 1979.06;
  carac_sections[37][1] = 0.00;
  carac_sections[37][2] = 28.66;
  carac_sections[37][3] = 100.00;
  outputs[37][0] = 4.92;
  outputs[37][1] = 3.32;

  // section 38
  carac_sections[38][0] = 2015.86;
  carac_sections[38][1] = 0.00;
  carac_sections[38][2] = 28.65;
  carac_sections[38][3] = 100.00;
  outputs[38][0] = 4.91;
  outputs[38][1] = 3.33;

  // section 39
  carac_sections[39][0] = 2052.91;
  carac_sections[39][1] = 0.00;
  carac_sections[39][2] = 28.63;
  carac_sections[39][3] = 100.00;
  outputs[39][0] = 4.89;
  outputs[39][1] = 3.34;

  // section 40
  carac_sections[40][0] = 2093.57;
  carac_sections[40][1] = 0.00;
  carac_sections[40][2] = 28.62;
  carac_sections[40][3] = 100.00;
  outputs[40][0] = 4.88;
  outputs[40][1] = 3.35;

  // section 41
  carac_sections[41][0] = 2135.94;
  carac_sections[41][1] = 0.00;
  carac_sections[41][2] = 28.61;
  carac_sections[41][3] = 100.00;
  outputs[41][0] = 4.86;
  outputs[41][1] = 3.36;

  // section 42
  carac_sections[42][0] = 2176.34;
  carac_sections[42][1] = 0.00;
  carac_sections[42][2] = 28.59;
  carac_sections[42][3] = 100.00;
  outputs[42][0] = 4.85;
  outputs[42][1] = 3.37;

  // section 43
  carac_sections[43][0] = 2218.28;
  carac_sections[43][1] = 0.00;
  carac_sections[43][2] = 28.57;
  carac_sections[43][3] = 100.00;
  outputs[43][0] = 4.84;
  outputs[43][1] = 3.38;

  // section 44
  carac_sections[44][0] = 2263.95;
  carac_sections[44][1] = 0.00;
  carac_sections[44][2] = 28.55;
  carac_sections[44][3] = 100.00;
  outputs[44][0] = 4.83;
  outputs[44][1] = 3.38;

  // section 45
  carac_sections[45][0] = 2311.43;
  carac_sections[45][1] = 0.00;
  carac_sections[45][2] = 28.53;
  carac_sections[45][3] = 100.00;
  outputs[45][0] = 4.82;
  outputs[45][1] = 3.39;

  // section 46
  carac_sections[46][0] = 2361.53;
  carac_sections[46][1] = 0.00;
  carac_sections[46][2] = 28.51;
  carac_sections[46][3] = 100.00;
  outputs[46][0] = 4.81;
  outputs[46][1] = 3.40;

  // section 47
  carac_sections[47][0] = 2414.69;
  carac_sections[47][1] = 0.00;
  carac_sections[47][2] = 28.48;
  carac_sections[47][3] = 100.00;
  outputs[47][0] = 4.80;
  outputs[47][1] = 3.40;

  // section 48
  carac_sections[48][0] = 2472.18;
  carac_sections[48][1] = 0.00;
  carac_sections[48][2] = 28.44;
  carac_sections[48][3] = 100.00;
  outputs[48][0] = 4.80;
  outputs[48][1] = 3.40;

  // section 49
  carac_sections[49][0] = 2533.38;
  carac_sections[49][1] = 0.00;
  carac_sections[49][2] = 28.40;
  carac_sections[49][3] = 100.00;
  outputs[49][0] = 4.80;
  outputs[49][1] = 3.40;

  // section 50
  carac_sections[50][0] = 2595.03;
  carac_sections[50][1] = 0.00;
  carac_sections[50][2] = 28.36;
  carac_sections[50][3] = 100.00;
  outputs[50][0] = 4.81;
  outputs[50][1] = 3.39;

  // section 51
  carac_sections[51][0] = 2659.34;
  carac_sections[51][1] = 0.00;
  carac_sections[51][2] = 28.31;
  carac_sections[51][3] = 100.00;
  outputs[51][0] = 4.82;
  outputs[51][1] = 3.39;

  // section 52
  carac_sections[52][0] = 2727.16;
  carac_sections[52][1] = 0.00;
  carac_sections[52][2] = 28.25;
  carac_sections[52][3] = 100.00;
  outputs[52][0] = 4.84;
  outputs[52][1] = 3.37;

  // section 53
  carac_sections[53][0] = 2797.65;
  carac_sections[53][1] = 0.00;
  carac_sections[53][2] = 28.19;
  carac_sections[53][3] = 100.00;
  outputs[53][0] = 4.86;
  outputs[53][1] = 3.36;

  // section 54
  carac_sections[54][0] = 2869.32;
  carac_sections[54][1] = 0.00;
  carac_sections[54][2] = 28.12;
  carac_sections[54][3] = 100.00;
  outputs[54][0] = 4.89;
  outputs[54][1] = 3.34;

  // section 55
  carac_sections[55][0] = 2942.10;
  carac_sections[55][1] = 0.00;
  carac_sections[55][2] = 28.05;
  carac_sections[55][3] = 100.00;
  outputs[55][0] = 4.93;
  outputs[55][1] = 3.31;

  // section 56
  carac_sections[56][0] = 3015.63;
  carac_sections[56][1] = 0.00;
  carac_sections[56][2] = 27.98;
  carac_sections[56][3] = 100.00;
  outputs[56][0] = 4.96;
  outputs[56][1] = 3.29;

  // section 57
  carac_sections[57][0] = 3088.77;
  carac_sections[57][1] = 0.00;
  carac_sections[57][2] = 27.91;
  carac_sections[57][3] = 100.00;
  outputs[57][0] = 5.01;
  outputs[57][1] = 3.26;

  // section 58
  carac_sections[58][0] = 3151.35;
  carac_sections[58][1] = 0.00;
  carac_sections[58][2] = 27.85;
  carac_sections[58][3] = 100.00;
  outputs[58][0] = 5.04;
  outputs[58][1] = 3.24;

  // section 59
  carac_sections[59][0] = 3209.50;
  carac_sections[59][1] = 0.00;
  carac_sections[59][2] = 27.79;
  carac_sections[59][3] = 100.00;
  outputs[59][0] = 5.07;
  outputs[59][1] = 3.22;

  // section 60
  carac_sections[60][0] = 3265.32;
  carac_sections[60][1] = 0.00;
  carac_sections[60][2] = 27.73;
  carac_sections[60][3] = 100.00;
  outputs[60][0] = 5.11;
  outputs[60][1] = 3.20;

  // section 61
  carac_sections[61][0] = 3319.08;
  carac_sections[61][1] = 0.00;
  carac_sections[61][2] = 27.68;
  carac_sections[61][3] = 100.00;
  outputs[61][0] = 5.14;
  outputs[61][1] = 3.18;

  // section 62
  carac_sections[62][0] = 3363.01;
  carac_sections[62][1] = 0.00;
  carac_sections[62][2] = 27.64;
  carac_sections[62][3] = 100.00;
  outputs[62][0] = 5.17;
  outputs[62][1] = 3.16;

  // section 63
  carac_sections[63][0] = 3407.55;
  carac_sections[63][1] = 0.00;
  carac_sections[63][2] = 27.59;
  carac_sections[63][3] = 100.00;
  outputs[63][0] = 5.19;
  outputs[63][1] = 3.15;

  // section 64
  carac_sections[64][0] = 3476.36;
  carac_sections[64][1] = 0.00;
  carac_sections[64][2] = 27.53;
  carac_sections[64][3] = 100.00;
  outputs[64][0] = 5.23;
  outputs[64][1] = 3.12;

  // section 65
  carac_sections[65][0] = 3526.55;
  carac_sections[65][1] = 0.00;
  carac_sections[65][2] = 27.48;
  carac_sections[65][3] = 100.00;
  outputs[65][0] = 5.26;
  outputs[65][1] = 3.10;

  // section 66
  carac_sections[66][0] = 3562.32;
  carac_sections[66][1] = 0.00;
  carac_sections[66][2] = 27.44;
  carac_sections[66][3] = 100.00;
  outputs[66][0] = 5.28;
  outputs[66][1] = 3.09;

  // section 67
  carac_sections[67][0] = 3595.87;
  carac_sections[67][1] = 0.00;
  carac_sections[67][2] = 27.41;
  carac_sections[67][3] = 100.00;
  outputs[67][0] = 5.30;
  outputs[67][1] = 3.08;

  // section 68
  carac_sections[68][0] = 3626.66;
  carac_sections[68][1] = 0.00;
  carac_sections[68][2] = 27.38;
  carac_sections[68][3] = 100.00;
  outputs[68][0] = 5.32;
  outputs[68][1] = 3.07;

  // section 69
  carac_sections[69][0] = 3668.17;
  carac_sections[69][1] = 0.00;
  carac_sections[69][2] = 27.35;
  carac_sections[69][3] = 100.00;
  outputs[69][0] = 5.35;
  outputs[69][1] = 3.05;

  // section 70
  carac_sections[70][0] = 3717.41;
  carac_sections[70][1] = 0.00;
  carac_sections[70][2] = 27.30;
  carac_sections[70][3] = 100.00;
  outputs[70][0] = 5.37;
  outputs[70][1] = 3.04;

  // section 71
  carac_sections[71][0] = 3776.08;
  carac_sections[71][1] = 0.00;
  carac_sections[71][2] = 27.25;
  carac_sections[71][3] = 100.00;
  outputs[71][0] = 5.41;
  outputs[71][1] = 3.02;

  // section 72
  carac_sections[72][0] = 3828.77;
  carac_sections[72][1] = 0.00;
  carac_sections[72][2] = 27.20;
  carac_sections[72][3] = 100.00;
  outputs[72][0] = 5.44;
  outputs[72][1] = 3.00;

  // section 73
  carac_sections[73][0] = 3854.87;
  carac_sections[73][1] = 0.00;
  carac_sections[73][2] = 27.17;
  carac_sections[73][3] = 100.00;
  outputs[73][0] = 5.45;
  outputs[73][1] = 2.99;

  // section 74
  carac_sections[74][0] = 3883.74;
  carac_sections[74][1] = 0.00;
  carac_sections[74][2] = 27.15;
  carac_sections[74][3] = 100.00;
  outputs[74][0] = 5.47;
  outputs[74][1] = 2.98;

  // section 75
  carac_sections[75][0] = 3911.52;
  carac_sections[75][1] = 0.00;
  carac_sections[75][2] = 27.12;
  carac_sections[75][3] = 100.00;
  outputs[75][0] = 5.49;
  outputs[75][1] = 2.98;

  // section 76
  carac_sections[76][0] = 3941.00;
  carac_sections[76][1] = 0.00;
  carac_sections[76][2] = 27.10;
  carac_sections[76][3] = 100.00;
  outputs[76][0] = 5.51;
  outputs[76][1] = 2.97;

  // section 77
  carac_sections[77][0] = 3968.94;
  carac_sections[77][1] = 0.00;
  carac_sections[77][2] = 27.07;
  carac_sections[77][3] = 100.00;
  outputs[77][0] = 5.52;
  outputs[77][1] = 2.96;

  // section 78
  carac_sections[78][0] = 3998.36;
  carac_sections[78][1] = 0.00;
  carac_sections[78][2] = 27.04;
  carac_sections[78][3] = 100.00;
  outputs[78][0] = 5.54;
  outputs[78][1] = 2.95;

  // section 79
  carac_sections[79][0] = 4027.13;
  carac_sections[79][1] = 0.00;
  carac_sections[79][2] = 27.02;
  carac_sections[79][3] = 100.00;
  outputs[79][0] = 5.56;
  outputs[79][1] = 2.94;

  // section 80
  carac_sections[80][0] = 4052.35;
  carac_sections[80][1] = 0.00;
  carac_sections[80][2] = 26.99;
  carac_sections[80][3] = 100.00;
  outputs[80][0] = 5.57;
  outputs[80][1] = 2.93;

  // section 81
  carac_sections[81][0] = 4115.54;
  carac_sections[81][1] = 0.00;
  carac_sections[81][2] = 26.93;
  carac_sections[81][3] = 100.00;
  outputs[81][0] = 5.62;
  outputs[81][1] = 2.91;

  // section 82
  carac_sections[82][0] = 4175.55;
  carac_sections[82][1] = 0.00;
  carac_sections[82][2] = 26.88;
  carac_sections[82][3] = 100.00;
  outputs[82][0] = 5.66;
  outputs[82][1] = 2.89;

  // section 83
  carac_sections[83][0] = 4237.00;
  carac_sections[83][1] = 0.00;
  carac_sections[83][2] = 26.82;
  carac_sections[83][3] = 100.00;
  outputs[83][0] = 5.70;
  outputs[83][1] = 2.87;

  // section 84
  carac_sections[84][0] = 4298.79;
  carac_sections[84][1] = 0.00;
  carac_sections[84][2] = 26.76;
  carac_sections[84][3] = 100.00;
  outputs[84][0] = 5.74;
  outputs[84][1] = 2.84;

  // section 85
  carac_sections[85][0] = 4361.68;
  carac_sections[85][1] = 0.00;
  carac_sections[85][2] = 26.69;
  carac_sections[85][3] = 100.00;
  outputs[85][0] = 5.79;
  outputs[85][1] = 2.82;

  // section 86
  carac_sections[86][0] = 4425.44;
  carac_sections[86][1] = 0.00;
  carac_sections[86][2] = 26.63;
  carac_sections[86][3] = 100.00;
  outputs[86][0] = 5.84;
  outputs[86][1] = 2.80;

  // section 87
  carac_sections[87][0] = 4490.08;
  carac_sections[87][1] = 0.00;
  carac_sections[87][2] = 26.57;
  carac_sections[87][3] = 100.00;
  outputs[87][0] = 5.89;
  outputs[87][1] = 2.77;

  // section 88
  carac_sections[88][0] = 4554.62;
  carac_sections[88][1] = 0.00;
  carac_sections[88][2] = 26.50;
  carac_sections[88][3] = 100.00;
  outputs[88][0] = 5.94;
  outputs[88][1] = 2.75;

  // section 89
  carac_sections[89][0] = 4619.13;
  carac_sections[89][1] = 0.00;
  carac_sections[89][2] = 26.44;
  carac_sections[89][3] = 100.00;
  outputs[89][0] = 5.99;
  outputs[89][1] = 2.73;

  // section 90
  carac_sections[90][0] = 4682.95;
  carac_sections[90][1] = 0.00;
  carac_sections[90][2] = 26.38;
  carac_sections[90][3] = 100.00;
  outputs[90][0] = 6.03;
  outputs[90][1] = 2.71;

  // section 91
  carac_sections[91][0] = 4746.43;
  carac_sections[91][1] = 0.00;
  carac_sections[91][2] = 26.32;
  carac_sections[91][3] = 100.00;
  outputs[91][0] = 6.08;
  outputs[91][1] = 2.69;

  // section 92
  carac_sections[92][0] = 4803.40;
  carac_sections[92][1] = 0.00;
  carac_sections[92][2] = 26.27;
  carac_sections[92][3] = 100.00;
  outputs[92][0] = 6.12;
  outputs[92][1] = 2.67;

  // section 93
  carac_sections[93][0] = 4858.41;
  carac_sections[93][1] = 0.00;
  carac_sections[93][2] = 26.22;
  carac_sections[93][3] = 100.00;
  outputs[93][0] = 6.15;
  outputs[93][1] = 2.65;

  // section 94
  carac_sections[94][0] = 4911.54;
  carac_sections[94][1] = 0.00;
  carac_sections[94][2] = 26.18;
  carac_sections[94][3] = 100.00;
  outputs[94][0] = 6.18;
  outputs[94][1] = 2.64;

  // section 95
  carac_sections[95][0] = 4963.98;
  carac_sections[95][1] = 0.00;
  carac_sections[95][2] = 26.14;
  carac_sections[95][3] = 100.00;
  outputs[95][0] = 6.21;
  outputs[95][1] = 2.63;

  // section 96
  carac_sections[96][0] = 5014.10;
  carac_sections[96][1] = 0.00;
  carac_sections[96][2] = 26.10;
  carac_sections[96][3] = 100.00;
  outputs[96][0] = 6.24;
  outputs[96][1] = 2.62;

  // section 97
  carac_sections[97][0] = 5063.70;
  carac_sections[97][1] = 0.00;
  carac_sections[97][2] = 26.06;
  carac_sections[97][3] = 100.00;
  outputs[97][0] = 6.27;
  outputs[97][1] = 2.61;

  // section 98
  carac_sections[98][0] = 5112.32;
  carac_sections[98][1] = 0.00;
  carac_sections[98][2] = 26.03;
  carac_sections[98][3] = 100.00;
  outputs[98][0] = 6.29;
  outputs[98][1] = 2.60;

  // section 99
  carac_sections[99][0] = 5161.62;
  carac_sections[99][1] = 0.00;
  carac_sections[99][2] = 25.99;
  carac_sections[99][3] = 100.00;
  outputs[99][0] = 6.32;
  outputs[99][1] = 2.59;

  // section 100
  carac_sections[100][0] = 5208.23;
  carac_sections[100][1] = 0.00;
  carac_sections[100][2] = 25.96;
  carac_sections[100][3] = 100.00;
  outputs[100][0] = 6.34;
  outputs[100][1] = 2.58;

  // section 101
  carac_sections[101][0] = 5255.51;
  carac_sections[101][1] = 0.00;
  carac_sections[101][2] = 25.93;
  carac_sections[101][3] = 100.00;
  outputs[101][0] = 6.36;
  outputs[101][1] = 2.57;

  // section 102
  carac_sections[102][0] = 5303.03;
  carac_sections[102][1] = 0.00;
  carac_sections[102][2] = 25.89;
  carac_sections[102][3] = 100.00;
  outputs[102][0] = 6.39;
  outputs[102][1] = 2.56;

  // section 103
  carac_sections[103][0] = 5352.39;
  carac_sections[103][1] = 0.00;
  carac_sections[103][2] = 25.86;
  carac_sections[103][3] = 100.00;
  outputs[103][0] = 6.41;
  outputs[103][1] = 2.55;

  // section 104
  carac_sections[104][0] = 5402.05;
  carac_sections[104][1] = 0.00;
  carac_sections[104][2] = 25.82;
  carac_sections[104][3] = 100.00;
  outputs[104][0] = 6.44;
  outputs[104][1] = 2.54;

  // section 105
  carac_sections[105][0] = 5454.10;
  carac_sections[105][1] = 0.00;
  carac_sections[105][2] = 25.78;
  carac_sections[105][3] = 100.00;
  outputs[105][0] = 6.47;
  outputs[105][1] = 2.52;

  // section 106
  carac_sections[106][0] = 5508.32;
  carac_sections[106][1] = 0.00;
  carac_sections[106][2] = 25.74;
  carac_sections[106][3] = 100.00;
  outputs[106][0] = 6.50;
  outputs[106][1] = 2.51;

  // section 107
  carac_sections[107][0] = 5566.69;
  carac_sections[107][1] = 0.00;
  carac_sections[107][2] = 25.69;
  carac_sections[107][3] = 100.00;
  outputs[107][0] = 6.54;
  outputs[107][1] = 2.50;

  // section 108
  carac_sections[108][0] = 5620.94;
  carac_sections[108][1] = 0.00;
  carac_sections[108][2] = 25.64;
  carac_sections[108][3] = 100.00;
  outputs[108][0] = 6.58;
  outputs[108][1] = 2.48;

  // section 109
  carac_sections[109][0] = 5677.42;
  carac_sections[109][1] = 0.00;
  carac_sections[109][2] = 25.59;
  carac_sections[109][3] = 100.00;
  outputs[109][0] = 6.62;
  outputs[109][1] = 2.47;

  // section 110
  carac_sections[110][0] = 5736.00;
  carac_sections[110][1] = 0.00;
  carac_sections[110][2] = 25.54;
  carac_sections[110][3] = 100.00;
  outputs[110][0] = 6.66;
  outputs[110][1] = 2.45;

  // section 111
  carac_sections[111][0] = 5797.68;
  carac_sections[111][1] = 0.00;
  carac_sections[111][2] = 25.48;
  carac_sections[111][3] = 100.00;
  outputs[111][0] = 6.71;
  outputs[111][1] = 2.43;

  // section 112
  carac_sections[112][0] = 5859.82;
  carac_sections[112][1] = 0.00;
  carac_sections[112][2] = 25.42;
  carac_sections[112][3] = 100.00;
  outputs[112][0] = 6.76;
  outputs[112][1] = 2.41;

  // section 113
  carac_sections[113][0] = 5923.77;
  carac_sections[113][1] = 0.00;
  carac_sections[113][2] = 25.36;
  carac_sections[113][3] = 100.00;
  outputs[113][0] = 6.82;
  outputs[113][1] = 2.39;

  // section 114
  carac_sections[114][0] = 5988.74;
  carac_sections[114][1] = 0.00;
  carac_sections[114][2] = 25.29;
  carac_sections[114][3] = 100.00;
  outputs[114][0] = 6.88;
  outputs[114][1] = 2.37;

  // section 115
  carac_sections[115][0] = 6055.05;
  carac_sections[115][1] = 0.00;
  carac_sections[115][2] = 25.22;
  carac_sections[115][3] = 100.00;
  outputs[115][0] = 6.93;
  outputs[115][1] = 2.35;

  // section 116
  carac_sections[116][0] = 6118.13;
  carac_sections[116][1] = 0.00;
  carac_sections[116][2] = 25.16;
  carac_sections[116][3] = 100.00;
  outputs[116][0] = 6.99;
  outputs[116][1] = 2.34;

  // section 117
  carac_sections[117][0] = 6180.60;
  carac_sections[117][1] = 0.00;
  carac_sections[117][2] = 25.10;
  carac_sections[117][3] = 100.00;
  outputs[117][0] = 7.04;
  outputs[117][1] = 2.32;

  // section 118
  carac_sections[118][0] = 6242.12;
  carac_sections[118][1] = 0.00;
  carac_sections[118][2] = 25.05;
  carac_sections[118][3] = 100.00;
  outputs[118][0] = 7.09;
  outputs[118][1] = 2.30;

  // section 119
  carac_sections[119][0] = 6303.36;
  carac_sections[119][1] = 0.00;
  carac_sections[119][2] = 24.99;
  carac_sections[119][3] = 100.00;
  outputs[119][0] = 7.13;
  outputs[119][1] = 2.29;

  // section 120
  carac_sections[120][0] = 6362.17;
  carac_sections[120][1] = 0.00;
  carac_sections[120][2] = 24.95;
  carac_sections[120][3] = 100.00;
  outputs[120][0] = 7.17;
  outputs[120][1] = 2.28;

  // section 121
  carac_sections[121][0] = 6420.16;
  carac_sections[121][1] = 0.00;
  carac_sections[121][2] = 24.90;
  carac_sections[121][3] = 100.00;
  outputs[121][0] = 7.21;
  outputs[121][1] = 2.26;

  // section 122
  carac_sections[122][0] = 6476.87;
  carac_sections[122][1] = 0.00;
  carac_sections[122][2] = 24.86;
  carac_sections[122][3] = 100.00;
  outputs[122][0] = 7.25;
  outputs[122][1] = 2.25;

  // section 123
  carac_sections[123][0] = 6534.00;
  carac_sections[123][1] = 0.00;
  carac_sections[123][2] = 24.81;
  carac_sections[123][3] = 100.00;
  outputs[123][0] = 7.28;
  outputs[123][1] = 2.24;

  // section 124
  carac_sections[124][0] = 6582.28;
  carac_sections[124][1] = 0.00;
  carac_sections[124][2] = 24.78;
  carac_sections[124][3] = 100.00;
  outputs[124][0] = 7.31;
  outputs[124][1] = 2.23;

  // section 125
  carac_sections[125][0] = 6629.48;
  carac_sections[125][1] = 0.00;
  carac_sections[125][2] = 24.75;
  carac_sections[125][3] = 100.00;
  outputs[125][0] = 7.33;
  outputs[125][1] = 2.23;

  // section 126
  carac_sections[126][0] = 6675.17;
  carac_sections[126][1] = 0.00;
  carac_sections[126][2] = 24.72;
  carac_sections[126][3] = 100.00;
  outputs[126][0] = 7.36;
  outputs[126][1] = 2.22;

  // section 127
  carac_sections[127][0] = 6721.08;
  carac_sections[127][1] = 0.00;
  carac_sections[127][2] = 24.70;
  carac_sections[127][3] = 100.00;
  outputs[127][0] = 7.38;
  outputs[127][1] = 2.21;

  // section 128
  carac_sections[128][0] = 6764.10;
  carac_sections[128][1] = 0.00;
  carac_sections[128][2] = 24.67;
  carac_sections[128][3] = 100.00;
  outputs[128][0] = 7.40;
  outputs[128][1] = 2.21;

  // section 129
  carac_sections[129][0] = 6807.02;
  carac_sections[129][1] = 0.00;
  carac_sections[129][2] = 24.65;
  carac_sections[129][3] = 100.00;
  outputs[129][0] = 7.41;
  outputs[129][1] = 2.20;

  // section 130
  carac_sections[130][0] = 6848.69;
  carac_sections[130][1] = 0.00;
  carac_sections[130][2] = 24.63;
  carac_sections[130][3] = 100.00;
  outputs[130][0] = 7.43;
  outputs[130][1] = 2.20;

  // section 131
  carac_sections[131][0] = 6891.53;
  carac_sections[131][1] = 0.00;
  carac_sections[131][2] = 24.61;
  carac_sections[131][3] = 100.00;
  outputs[131][0] = 7.44;
  outputs[131][1] = 2.19;

  // section 132
  carac_sections[132][0] = 6929.41;
  carac_sections[132][1] = 0.00;
  carac_sections[132][2] = 24.59;
  carac_sections[132][3] = 100.00;
  outputs[132][0] = 7.45;
  outputs[132][1] = 2.19;

  // section 133
  carac_sections[133][0] = 6967.41;
  carac_sections[133][1] = 0.00;
  carac_sections[133][2] = 24.58;
  carac_sections[133][3] = 100.00;
  outputs[133][0] = 7.46;
  outputs[133][1] = 2.19;

  // section 134
  carac_sections[134][0] = 7041.70;
  carac_sections[134][1] = 0.00;
  carac_sections[134][2] = 24.55;
  carac_sections[134][3] = 100.00;
  outputs[134][0] = 7.48;
  outputs[134][1] = 2.18;

  // section 135
  carac_sections[135][0] = 7113.44;
  carac_sections[135][1] = 0.00;
  carac_sections[135][2] = 24.52;
  carac_sections[135][3] = 100.00;
  outputs[135][0] = 7.50;
  outputs[135][1] = 2.18;

  // section 136
  carac_sections[136][0] = 7183.81;
  carac_sections[136][1] = 0.00;
  carac_sections[136][2] = 24.50;
  carac_sections[136][3] = 100.00;
  outputs[136][0] = 7.51;
  outputs[136][1] = 2.17;

  // section 137
  carac_sections[137][0] = 7245.07;
  carac_sections[137][1] = 0.00;
  carac_sections[137][2] = 24.48;
  carac_sections[137][3] = 100.00;
  outputs[137][0] = 7.52;
  outputs[137][1] = 2.17;

  // section 138
  carac_sections[138][0] = 7303.95;
  carac_sections[138][1] = 0.00;
  carac_sections[138][2] = 24.47;
  carac_sections[138][3] = 100.00;
  outputs[138][0] = 7.53;
  outputs[138][1] = 2.17;

  // section 139
  carac_sections[139][0] = 7360.67;
  carac_sections[139][1] = 0.00;
  carac_sections[139][2] = 24.45;
  carac_sections[139][3] = 100.00;
  outputs[139][0] = 7.53;
  outputs[139][1] = 2.17;

  // section 140
  carac_sections[140][0] = 7417.05;
  carac_sections[140][1] = 0.00;
  carac_sections[140][2] = 24.44;
  carac_sections[140][3] = 100.00;
  outputs[140][0] = 7.54;
  outputs[140][1] = 2.17;

  // section 141
  carac_sections[141][0] = 7469.38;
  carac_sections[141][1] = 0.00;
  carac_sections[141][2] = 24.43;
  carac_sections[141][3] = 100.00;
  outputs[141][0] = 7.54;
  outputs[141][1] = 2.17;

  // section 142
  carac_sections[142][0] = 7520.99;
  carac_sections[142][1] = 0.00;
  carac_sections[142][2] = 24.42;
  carac_sections[142][3] = 100.00;
  outputs[142][0] = 7.54;
  outputs[142][1] = 2.17;

  // section 143
  carac_sections[143][0] = 7571.19;
  carac_sections[143][1] = 0.00;
  carac_sections[143][2] = 24.42;
  carac_sections[143][3] = 100.00;
  outputs[143][0] = 7.54;
  outputs[143][1] = 2.17;

  // section 144
  carac_sections[144][0] = 7622.55;
  carac_sections[144][1] = 0.00;
  carac_sections[144][2] = 24.41;
  carac_sections[144][3] = 100.00;
  outputs[144][0] = 7.54;
  outputs[144][1] = 2.17;

  // section 145
  carac_sections[145][0] = 7665.52;
  carac_sections[145][1] = 0.00;
  carac_sections[145][2] = 24.40;
  carac_sections[145][3] = 100.00;
  outputs[145][0] = 7.53;
  outputs[145][1] = 2.17;

  // section 146
  carac_sections[146][0] = 7708.15;
  carac_sections[146][1] = 0.00;
  carac_sections[146][2] = 24.40;
  carac_sections[146][3] = 100.00;
  outputs[146][0] = 7.53;
  outputs[146][1] = 2.17;

  // section 147
  carac_sections[147][0] = 7749.51;
  carac_sections[147][1] = 0.00;
  carac_sections[147][2] = 24.40;
  carac_sections[147][3] = 100.00;
  outputs[147][0] = 7.53;
  outputs[147][1] = 2.17;

  // section 148
  carac_sections[148][0] = 7791.75;
  carac_sections[148][1] = 0.00;
  carac_sections[148][2] = 24.39;
  carac_sections[148][3] = 100.00;
  outputs[148][0] = 7.52;
  outputs[148][1] = 2.17;

  // section 149
  carac_sections[149][0] = 7830.34;
  carac_sections[149][1] = 0.00;
  carac_sections[149][2] = 24.39;
  carac_sections[149][3] = 100.00;
  outputs[149][0] = 7.52;
  outputs[149][1] = 2.17;

  // section 150
  carac_sections[150][0] = 7869.21;
  carac_sections[150][1] = 0.00;
  carac_sections[150][2] = 24.39;
  carac_sections[150][3] = 100.00;
  outputs[150][0] = 7.52;
  outputs[150][1] = 2.17;

  // section 151
  carac_sections[151][0] = 7906.93;
  carac_sections[151][1] = 0.00;
  carac_sections[151][2] = 24.39;
  carac_sections[151][3] = 100.00;
  outputs[151][0] = 7.51;
  outputs[151][1] = 2.17;

  // section 152
  carac_sections[152][0] = 7944.72;
  carac_sections[152][1] = 0.00;
  carac_sections[152][2] = 24.39;
  carac_sections[152][3] = 100.00;
  outputs[152][0] = 7.50;
  outputs[152][1] = 2.18;

  // section 153
  carac_sections[153][0] = 8011.65;
  carac_sections[153][1] = 0.00;
  carac_sections[153][2] = 24.39;
  carac_sections[153][3] = 100.00;
  outputs[153][0] = 7.49;
  outputs[153][1] = 2.18;

  // section 154
  carac_sections[154][0] = 8075.95;
  carac_sections[154][1] = 0.00;
  carac_sections[154][2] = 24.39;
  carac_sections[154][3] = 100.00;
  outputs[154][0] = 7.48;
  outputs[154][1] = 2.18;

  // section 155
  carac_sections[155][0] = 8137.00;
  carac_sections[155][1] = 0.00;
  carac_sections[155][2] = 24.40;
  carac_sections[155][3] = 100.00;
  outputs[155][0] = 7.46;
  outputs[155][1] = 2.19;

  // section 156
  carac_sections[156][0] = 8197.66;
  carac_sections[156][1] = 0.00;
  carac_sections[156][2] = 24.40;
  carac_sections[156][3] = 100.00;
  outputs[156][0] = 7.45;
  outputs[156][1] = 2.19;

  // section 157
  carac_sections[157][0] = 8250.32;
  carac_sections[157][1] = 0.00;
  carac_sections[157][2] = 24.41;
  carac_sections[157][3] = 100.00;
  outputs[157][0] = 7.44;
  outputs[157][1] = 2.20;

  // section 158
  carac_sections[158][0] = 8301.89;
  carac_sections[158][1] = 0.00;
  carac_sections[158][2] = 24.41;
  carac_sections[158][3] = 100.00;
  outputs[158][0] = 7.42;
  outputs[158][1] = 2.20;

  // section 159
  carac_sections[159][0] = 8350.97;
  carac_sections[159][1] = 0.00;
  carac_sections[159][2] = 24.42;
  carac_sections[159][3] = 100.00;
  outputs[159][0] = 7.41;
  outputs[159][1] = 2.20;

  // section 160
  carac_sections[160][0] = 8401.16;
  carac_sections[160][1] = 0.00;
  carac_sections[160][2] = 24.43;
  carac_sections[160][3] = 100.00;
  outputs[160][0] = 7.39;
  outputs[160][1] = 2.21;

  // section 161
  carac_sections[161][0] = 8443.48;
  carac_sections[161][1] = 0.00;
  carac_sections[161][2] = 24.43;
  carac_sections[161][3] = 100.00;
  outputs[161][0] = 7.37;
  outputs[161][1] = 2.21;

  // section 162
  carac_sections[162][0] = 8485.85;
  carac_sections[162][1] = 0.00;
  carac_sections[162][2] = 24.44;
  carac_sections[162][3] = 100.00;
  outputs[162][0] = 7.36;
  outputs[162][1] = 2.22;

  // section 163
  carac_sections[163][0] = 8526.18;
  carac_sections[163][1] = 0.00;
  carac_sections[163][2] = 24.45;
  carac_sections[163][3] = 100.00;
  outputs[163][0] = 7.34;
  outputs[163][1] = 2.22;

  // section 164
  carac_sections[164][0] = 8567.42;
  carac_sections[164][1] = 0.00;
  carac_sections[164][2] = 24.46;
  carac_sections[164][3] = 100.00;
  outputs[164][0] = 7.33;
  outputs[164][1] = 2.23;

  // section 165
  carac_sections[165][0] = 8638.46;
  carac_sections[165][1] = 0.00;
  carac_sections[165][2] = 24.47;
  carac_sections[165][3] = 100.00;
  outputs[165][0] = 7.30;
  outputs[165][1] = 2.24;

  // section 166
  carac_sections[166][0] = 8707.34;
  carac_sections[166][1] = 0.00;
  carac_sections[166][2] = 24.49;
  carac_sections[166][3] = 100.00;
  outputs[166][0] = 7.27;
  outputs[166][1] = 2.25;

  // section 167
  carac_sections[167][0] = 8768.57;
  carac_sections[167][1] = 0.00;
  carac_sections[167][2] = 24.50;
  carac_sections[167][3] = 100.00;
  outputs[167][0] = 7.24;
  outputs[167][1] = 2.26;

  // section 168
  carac_sections[168][0] = 8828.11;
  carac_sections[168][1] = 0.00;
  carac_sections[168][2] = 24.52;
  carac_sections[168][3] = 100.00;
  outputs[168][0] = 7.21;
  outputs[168][1] = 2.26;

  // section 169
  carac_sections[169][0] = 8880.66;
  carac_sections[169][1] = 0.00;
  carac_sections[169][2] = 24.54;
  carac_sections[169][3] = 100.00;
  outputs[169][0] = 7.18;
  outputs[169][1] = 2.27;

  // section 170
  carac_sections[170][0] = 8932.91;
  carac_sections[170][1] = 0.00;
  carac_sections[170][2] = 24.55;
  carac_sections[170][3] = 100.00;
  outputs[170][0] = 7.16;
  outputs[170][1] = 2.28;

  // section 171
  carac_sections[171][0] = 8978.55;
  carac_sections[171][1] = 0.00;
  carac_sections[171][2] = 24.57;
  carac_sections[171][3] = 100.00;
  outputs[171][0] = 7.13;
  outputs[171][1] = 2.29;

  // section 172
  carac_sections[172][0] = 9024.96;
  carac_sections[172][1] = 0.00;
  carac_sections[172][2] = 24.58;
  carac_sections[172][3] = 100.00;
  outputs[172][0] = 7.11;
  outputs[172][1] = 2.30;

  // section 173
  carac_sections[173][0] = 9065.16;
  carac_sections[173][1] = 0.00;
  carac_sections[173][2] = 24.60;
  carac_sections[173][3] = 100.00;
  outputs[173][0] = 7.08;
  outputs[173][1] = 2.31;

  // section 174
  carac_sections[174][0] = 9106.15;
  carac_sections[174][1] = 0.00;
  carac_sections[174][2] = 24.61;
  carac_sections[174][3] = 100.00;
  outputs[174][0] = 7.06;
  outputs[174][1] = 2.31;

  // section 175
  carac_sections[175][0] = 9179.15;
  carac_sections[175][1] = 0.00;
  carac_sections[175][2] = 24.64;
  carac_sections[175][3] = 100.00;
  outputs[175][0] = 7.01;
  outputs[175][1] = 2.33;

  // section 176
  carac_sections[176][0] = 9246.85;
  carac_sections[176][1] = 0.00;
  carac_sections[176][2] = 24.67;
  carac_sections[176][3] = 100.00;
  outputs[176][0] = 6.97;
  outputs[176][1] = 2.34;

  // section 177
  carac_sections[177][0] = 9309.28;
  carac_sections[177][1] = 0.00;
  carac_sections[177][2] = 24.69;
  carac_sections[177][3] = 100.00;
  outputs[177][0] = 6.93;
  outputs[177][1] = 2.36;

  // section 178
  carac_sections[178][0] = 9367.42;
  carac_sections[178][1] = 0.00;
  carac_sections[178][2] = 24.72;
  carac_sections[178][3] = 100.00;
  outputs[178][0] = 6.89;
  outputs[178][1] = 2.37;

  // section 179
  carac_sections[179][0] = 9422.06;
  carac_sections[179][1] = 0.00;
  carac_sections[179][2] = 24.74;
  carac_sections[179][3] = 100.00;
  outputs[179][0] = 6.85;
  outputs[179][1] = 2.38;

  // section 180
  carac_sections[180][0] = 9473.96;
  carac_sections[180][1] = 0.00;
  carac_sections[180][2] = 24.77;
  carac_sections[180][3] = 100.00;
  outputs[180][0] = 6.81;
  outputs[180][1] = 2.40;

  // section 181
  carac_sections[181][0] = 9523.73;
  carac_sections[181][1] = 0.00;
  carac_sections[181][2] = 24.79;
  carac_sections[181][3] = 100.00;
  outputs[181][0] = 6.78;
  outputs[181][1] = 2.41;

  // section 182
  carac_sections[182][0] = 9571.91;
  carac_sections[182][1] = 0.00;
  carac_sections[182][2] = 24.81;
  carac_sections[182][3] = 100.00;
  outputs[182][0] = 6.74;
  outputs[182][1] = 2.42;

  // section 183
  carac_sections[183][0] = 9618.94;
  carac_sections[183][1] = 0.00;
  carac_sections[183][2] = 24.84;
  carac_sections[183][3] = 100.00;
  outputs[183][0] = 6.70;
  outputs[183][1] = 2.44;

  // section 184
  carac_sections[184][0] = 9665.42;
  carac_sections[184][1] = 0.00;
  carac_sections[184][2] = 24.86;
  carac_sections[184][3] = 100.00;
  outputs[184][0] = 6.66;
  outputs[184][1] = 2.45;

  // section 185
  carac_sections[185][0] = 9711.74;
  carac_sections[185][1] = 0.00;
  carac_sections[185][2] = 24.88;
  carac_sections[185][3] = 100.00;
  outputs[185][0] = 6.63;
  outputs[185][1] = 2.46;

  // section 186
  carac_sections[186][0] = 9758.41;
  carac_sections[186][1] = 0.00;
  carac_sections[186][2] = 24.91;
  carac_sections[186][3] = 100.00;
  outputs[186][0] = 6.59;
  outputs[186][1] = 2.48;

  // section 187
  carac_sections[187][0] = 9805.86;
  carac_sections[187][1] = 0.00;
  carac_sections[187][2] = 24.93;
  carac_sections[187][3] = 100.00;
  outputs[187][0] = 6.55;
  outputs[187][1] = 2.49;

  // section 188
  carac_sections[188][0] = 9854.77;
  carac_sections[188][1] = 0.00;
  carac_sections[188][2] = 24.96;
  carac_sections[188][3] = 100.00;
  outputs[188][0] = 6.51;
  outputs[188][1] = 2.51;

  // section 189
  carac_sections[189][0] = 9905.69;
  carac_sections[189][1] = 0.00;
  carac_sections[189][2] = 24.99;
  carac_sections[189][3] = 100.00;
  outputs[189][0] = 6.46;
  outputs[189][1] = 2.53;

  // section 190
  carac_sections[190][0] = 9959.36;
  carac_sections[190][1] = 0.00;
  carac_sections[190][2] = 25.02;
  carac_sections[190][3] = 100.00;
  outputs[190][0] = 6.41;
  outputs[190][1] = 2.55;

  // section 191
  carac_sections[191][0] = 10016.78;
  carac_sections[191][1] = 0.00;
  carac_sections[191][2] = 25.05;
  carac_sections[191][3] = 100.00;
  outputs[191][0] = 6.36;
  outputs[191][1] = 2.57;

  // section 192
  carac_sections[192][0] = 10079.25;
  carac_sections[192][1] = 0.00;
  carac_sections[192][2] = 25.08;
  carac_sections[192][3] = 100.00;
  outputs[192][0] = 6.30;
  outputs[192][1] = 2.59;

  // section 193
  carac_sections[193][0] = 10148.93;
  carac_sections[193][1] = 0.00;
  carac_sections[193][2] = 25.12;
  carac_sections[193][3] = 100.00;
  outputs[193][0] = 6.24;
  outputs[193][1] = 2.62;

  // section 194
  carac_sections[194][0] = 10186.56;
  carac_sections[194][1] = 0.00;
  carac_sections[194][2] = 25.14;
  carac_sections[194][3] = 100.00;
  outputs[194][0] = 6.20;
  outputs[194][1] = 2.63;

  // section 195
  carac_sections[195][0] = 10226.21;
  carac_sections[195][1] = 0.00;
  carac_sections[195][2] = 25.17;
  carac_sections[195][3] = 100.00;
  outputs[195][0] = 6.16;
  outputs[195][1] = 2.65;

  // section 196
  carac_sections[196][0] = 10270.24;
  carac_sections[196][1] = 0.00;
  carac_sections[196][2] = 25.19;
  carac_sections[196][3] = 100.00;
  outputs[196][0] = 6.12;
  outputs[196][1] = 2.67;

  // section 197
  carac_sections[197][0] = 10317.01;
  carac_sections[197][1] = 0.00;
  carac_sections[197][2] = 25.22;
  carac_sections[197][3] = 100.00;
  outputs[197][0] = 6.08;
  outputs[197][1] = 2.69;

  // section 198
  carac_sections[198][0] = 10368.75;
  carac_sections[198][1] = 0.00;
  carac_sections[198][2] = 25.25;
  carac_sections[198][3] = 100.00;
  outputs[198][0] = 6.03;
  outputs[198][1] = 2.71;

  // section 199
  carac_sections[199][0] = 10425.58;
  carac_sections[199][1] = 0.00;
  carac_sections[199][2] = 25.28;
  carac_sections[199][3] = 100.00;
  outputs[199][0] = 5.97;
  outputs[199][1] = 2.74;

  // section 200
  carac_sections[200][0] = 10490.72;
  carac_sections[200][1] = 0.00;
  carac_sections[200][2] = 25.31;
  carac_sections[200][3] = 100.00;
  outputs[200][0] = 5.91;
  outputs[200][1] = 2.76;

  // section 201
  carac_sections[201][0] = 10524.78;
  carac_sections[201][1] = 0.00;
  carac_sections[201][2] = 25.33;
  carac_sections[201][3] = 100.00;
  outputs[201][0] = 5.87;
  outputs[201][1] = 2.78;

  // section 202
  carac_sections[202][0] = 10559.99;
  carac_sections[202][1] = 0.00;
  carac_sections[202][2] = 25.35;
  carac_sections[202][3] = 100.00;
  outputs[202][0] = 5.83;
  outputs[202][1] = 2.80;

  // section 203
  carac_sections[203][0] = 10601.67;
  carac_sections[203][1] = 0.00;
  carac_sections[203][2] = 25.37;
  carac_sections[203][3] = 100.00;
  outputs[203][0] = 5.79;
  outputs[203][1] = 2.82;

  // section 204
  carac_sections[204][0] = 10643.43;
  carac_sections[204][1] = 0.00;
  carac_sections[204][2] = 25.39;
  carac_sections[204][3] = 100.00;
  outputs[204][0] = 5.75;
  outputs[204][1] = 2.84;

  // section 205
  carac_sections[205][0] = 10691.35;
  carac_sections[205][1] = 0.00;
  carac_sections[205][2] = 25.42;
  carac_sections[205][3] = 100.00;
  outputs[205][0] = 5.70;
  outputs[205][1] = 2.86;

  // section 206
  carac_sections[206][0] = 10741.62;
  carac_sections[206][1] = 0.00;
  carac_sections[206][2] = 25.44;
  carac_sections[206][3] = 100.00;
  outputs[206][0] = 5.65;
  outputs[206][1] = 2.89;

  // section 207
  carac_sections[207][0] = 10797.63;
  carac_sections[207][1] = 0.00;
  carac_sections[207][2] = 25.47;
  carac_sections[207][3] = 100.00;
  outputs[207][0] = 5.60;
  outputs[207][1] = 2.92;

  // section 208
  carac_sections[208][0] = 10858.57;
  carac_sections[208][1] = 0.00;
  carac_sections[208][2] = 25.49;
  carac_sections[208][3] = 100.00;
  outputs[208][0] = 5.54;
  outputs[208][1] = 2.95;

  // section 209
  carac_sections[209][0] = 10927.37;
  carac_sections[209][1] = 0.00;
  carac_sections[209][2] = 25.52;
  carac_sections[209][3] = 100.00;
  outputs[209][0] = 5.47;
  outputs[209][1] = 2.98;

  // section 210
  carac_sections[210][0] = 11001.75;
  carac_sections[210][1] = 0.00;
  carac_sections[210][2] = 25.54;
  carac_sections[210][3] = 100.00;
  outputs[210][0] = 5.41;
  outputs[210][1] = 3.02;

  // section 211
  carac_sections[211][0] = 11044.55;
  carac_sections[211][1] = 0.00;
  carac_sections[211][2] = 25.56;
  carac_sections[211][3] = 100.00;
  outputs[211][0] = 5.37;
  outputs[211][1] = 3.04;

  // section 212
  carac_sections[212][0] = 11087.14;
  carac_sections[212][1] = 0.00;
  carac_sections[212][2] = 25.57;
  carac_sections[212][3] = 100.00;
  outputs[212][0] = 5.33;
  outputs[212][1] = 3.06;

  // section 213
  carac_sections[213][0] = 11134.40;
  carac_sections[213][1] = 0.00;
  carac_sections[213][2] = 25.58;
  carac_sections[213][3] = 100.00;
  outputs[213][0] = 5.29;
  outputs[213][1] = 3.09;

  // section 214
  carac_sections[214][0] = 11182.89;
  carac_sections[214][1] = 0.00;
  carac_sections[214][2] = 25.59;
  carac_sections[214][3] = 100.00;
  outputs[214][0] = 5.25;
  outputs[214][1] = 3.11;

  // section 215
  carac_sections[215][0] = 11236.36;
  carac_sections[215][1] = 0.00;
  carac_sections[215][2] = 25.60;
  carac_sections[215][3] = 100.00;
  outputs[215][0] = 5.21;
  outputs[215][1] = 3.14;

  // section 216
  carac_sections[216][0] = 11292.01;
  carac_sections[216][1] = 0.00;
  carac_sections[216][2] = 25.60;
  carac_sections[216][3] = 100.00;
  outputs[216][0] = 5.17;
  outputs[216][1] = 3.16;

  // section 217
  carac_sections[217][0] = 11352.26;
  carac_sections[217][1] = 0.00;
  carac_sections[217][2] = 25.60;
  carac_sections[217][3] = 100.00;
  outputs[217][0] = 5.12;
  outputs[217][1] = 3.19;

  // section 218
  carac_sections[218][0] = 11415.22;
  carac_sections[218][1] = 0.00;
  carac_sections[218][2] = 25.60;
  carac_sections[218][3] = 100.00;
  outputs[218][0] = 5.08;
  outputs[218][1] = 3.21;

  // section 219
  carac_sections[219][0] = 11481.59;
  carac_sections[219][1] = 0.00;
  carac_sections[219][2] = 25.60;
  carac_sections[219][3] = 100.00;
  outputs[219][0] = 5.05;
  outputs[219][1] = 3.24;

  // section 220
  carac_sections[220][0] = 11551.20;
  carac_sections[220][1] = 0.00;
  carac_sections[220][2] = 25.59;
  carac_sections[220][3] = 100.00;
  outputs[220][0] = 5.01;
  outputs[220][1] = 3.26;

  // section 221
  carac_sections[221][0] = 11624.48;
  carac_sections[221][1] = 0.00;
  carac_sections[221][2] = 25.57;
  carac_sections[221][3] = 100.00;
  outputs[221][0] = 4.99;
  outputs[221][1] = 3.27;

  // section 222
  carac_sections[222][0] = 11662.52;
  carac_sections[222][1] = 0.00;
  carac_sections[222][2] = 25.56;
  carac_sections[222][3] = 100.00;
  outputs[222][0] = 4.97;
  outputs[222][1] = 3.29;

  // section 223
  carac_sections[223][0] = 11700.42;
  carac_sections[223][1] = 0.00;
  carac_sections[223][2] = 25.55;
  carac_sections[223][3] = 100.00;
  outputs[223][0] = 4.95;
  outputs[223][1] = 3.30;

  // section 224
  carac_sections[224][0] = 11740.92;
  carac_sections[224][1] = 0.00;
  carac_sections[224][2] = 25.53;
  carac_sections[224][3] = 100.00;
  outputs[224][0] = 4.94;
  outputs[224][1] = 3.30;

  // section 225
  carac_sections[225][0] = 11780.78;
  carac_sections[225][1] = 0.00;
  carac_sections[225][2] = 25.52;
  carac_sections[225][3] = 100.00;
  outputs[225][0] = 4.93;
  outputs[225][1] = 3.31;

  // section 226
  carac_sections[226][0] = 11822.31;
  carac_sections[226][1] = 0.00;
  carac_sections[226][2] = 25.50;
  carac_sections[226][3] = 100.00;
  outputs[226][0] = 4.93;
  outputs[226][1] = 3.31;

  // section 227
  carac_sections[227][0] = 11863.58;
  carac_sections[227][1] = 0.00;
  carac_sections[227][2] = 25.48;
  carac_sections[227][3] = 100.00;
  outputs[227][0] = 4.92;
  outputs[227][1] = 3.32;

  // section 228
  carac_sections[228][0] = 11906.61;
  carac_sections[228][1] = 0.00;
  carac_sections[228][2] = 25.46;
  carac_sections[228][3] = 100.00;
  outputs[228][0] = 4.92;
  outputs[228][1] = 3.32;

  // section 229
  carac_sections[229][0] = 11949.39;
  carac_sections[229][1] = 0.00;
  carac_sections[229][2] = 25.43;
  carac_sections[229][3] = 100.00;
  outputs[229][0] = 4.92;
  outputs[229][1] = 3.32;

  // section 230
  carac_sections[230][0] = 11993.46;
  carac_sections[230][1] = 0.00;
  carac_sections[230][2] = 25.41;
  carac_sections[230][3] = 100.00;
  outputs[230][0] = 4.92;
  outputs[230][1] = 3.32;

  // section 231
  carac_sections[231][0] = 12037.59;
  carac_sections[231][1] = 0.00;
  carac_sections[231][2] = 25.38;
  carac_sections[231][3] = 100.00;
  outputs[231][0] = 4.92;
  outputs[231][1] = 3.32;

  // section 232
  carac_sections[232][0] = 12080.86;
  carac_sections[232][1] = 0.00;
  carac_sections[232][2] = 25.35;
  carac_sections[232][3] = 100.00;
  outputs[232][0] = 4.92;
  outputs[232][1] = 3.32;

  // section 233
  carac_sections[233][0] = 12124.15;
  carac_sections[233][1] = 0.00;
  carac_sections[233][2] = 25.32;
  carac_sections[233][3] = 100.00;
  outputs[233][0] = 4.93;
  outputs[233][1] = 3.31;

  // section 234
  carac_sections[234][0] = 12167.88;
  carac_sections[234][1] = 0.00;
  carac_sections[234][2] = 25.29;
  carac_sections[234][3] = 100.00;
  outputs[234][0] = 4.94;
  outputs[234][1] = 3.31;

  // section 235
  carac_sections[235][0] = 12211.59;
  carac_sections[235][1] = 0.00;
  carac_sections[235][2] = 25.25;
  carac_sections[235][3] = 100.00;
  outputs[235][0] = 4.95;
  outputs[235][1] = 3.30;

  // section 236
  carac_sections[236][0] = 12255.53;
  carac_sections[236][1] = 0.00;
  carac_sections[236][2] = 25.22;
  carac_sections[236][3] = 100.00;
  outputs[236][0] = 4.96;
  outputs[236][1] = 3.29;

  // section 237
  carac_sections[237][0] = 12299.46;
  carac_sections[237][1] = 0.00;
  carac_sections[237][2] = 25.18;
  carac_sections[237][3] = 100.00;
  outputs[237][0] = 4.97;
  outputs[237][1] = 3.28;

  // section 238
  carac_sections[238][0] = 12343.65;
  carac_sections[238][1] = 0.00;
  carac_sections[238][2] = 25.15;
  carac_sections[238][3] = 100.00;
  outputs[238][0] = 4.99;
  outputs[238][1] = 3.27;

  // section 239
  carac_sections[239][0] = 12387.96;
  carac_sections[239][1] = 0.00;
  carac_sections[239][2] = 25.11;
  carac_sections[239][3] = 100.00;
  outputs[239][0] = 5.00;
  outputs[239][1] = 3.26;

  // section 240
  carac_sections[240][0] = 12431.58;
  carac_sections[240][1] = 0.00;
  carac_sections[240][2] = 25.07;
  carac_sections[240][3] = 100.00;
  outputs[240][0] = 5.02;
  outputs[240][1] = 3.25;

  // section 241
  carac_sections[241][0] = 12475.16;
  carac_sections[241][1] = 0.00;
  carac_sections[241][2] = 25.03;
  carac_sections[241][3] = 100.00;
  outputs[241][0] = 5.04;
  outputs[241][1] = 3.24;

  // section 242
  carac_sections[242][0] = 12518.66;
  carac_sections[242][1] = 0.00;
  carac_sections[242][2] = 24.99;
  carac_sections[242][3] = 100.00;
  outputs[242][0] = 5.06;
  outputs[242][1] = 3.23;

  // section 243
  carac_sections[243][0] = 12562.23;
  carac_sections[243][1] = 0.00;
  carac_sections[243][2] = 24.95;
  carac_sections[243][3] = 100.00;
  outputs[243][0] = 5.08;
  outputs[243][1] = 3.21;

  // section 244
  carac_sections[244][0] = 12605.39;
  carac_sections[244][1] = 0.00;
  carac_sections[244][2] = 24.91;
  carac_sections[244][3] = 100.00;
  outputs[244][0] = 5.11;
  outputs[244][1] = 3.20;

  // section 245
  carac_sections[245][0] = 12648.55;
  carac_sections[245][1] = 0.00;
  carac_sections[245][2] = 24.87;
  carac_sections[245][3] = 100.00;
  outputs[245][0] = 5.13;
  outputs[245][1] = 3.18;

  // section 246
  carac_sections[246][0] = 12691.49;
  carac_sections[246][1] = 0.00;
  carac_sections[246][2] = 24.83;
  carac_sections[246][3] = 100.00;
  outputs[246][0] = 5.15;
  outputs[246][1] = 3.17;

  // section 247
  carac_sections[247][0] = 12735.34;
  carac_sections[247][1] = 0.00;
  carac_sections[247][2] = 24.79;
  carac_sections[247][3] = 100.00;
  outputs[247][0] = 5.18;
  outputs[247][1] = 3.15;

  // section 248
  carac_sections[248][0] = 12775.59;
  carac_sections[248][1] = 0.00;
  carac_sections[248][2] = 24.75;
  carac_sections[248][3] = 100.00;
  outputs[248][0] = 5.20;
  outputs[248][1] = 3.14;

  // section 249
  carac_sections[249][0] = 12815.81;
  carac_sections[249][1] = 0.00;
  carac_sections[249][2] = 24.71;
  carac_sections[249][3] = 100.00;
  outputs[249][0] = 5.23;
  outputs[249][1] = 3.12;

  // section 250
  carac_sections[250][0] = 12855.61;
  carac_sections[250][1] = 0.00;
  carac_sections[250][2] = 24.67;
  carac_sections[250][3] = 100.00;
  outputs[250][0] = 5.25;
  outputs[250][1] = 3.11;

  // section 251
  carac_sections[251][0] = 12895.70;
  carac_sections[251][1] = 0.00;
  carac_sections[251][2] = 24.63;
  carac_sections[251][3] = 100.00;
  outputs[251][0] = 5.28;
  outputs[251][1] = 3.09;

  // section 252
  carac_sections[252][0] = 12934.67;
  carac_sections[252][1] = 0.00;
  carac_sections[252][2] = 24.59;
  carac_sections[252][3] = 100.00;
  outputs[252][0] = 5.30;
  outputs[252][1] = 3.08;

  // section 253
  carac_sections[253][0] = 12973.77;
  carac_sections[253][1] = 0.00;
  carac_sections[253][2] = 24.55;
  carac_sections[253][3] = 100.00;
  outputs[253][0] = 5.33;
  outputs[253][1] = 3.06;

  // section 254
  carac_sections[254][0] = 13012.36;
  carac_sections[254][1] = 0.00;
  carac_sections[254][2] = 24.51;
  carac_sections[254][3] = 100.00;
  outputs[254][0] = 5.35;
  outputs[254][1] = 3.05;

  // section 255
  carac_sections[255][0] = 13051.46;
  carac_sections[255][1] = 0.00;
  carac_sections[255][2] = 24.47;
  carac_sections[255][3] = 100.00;
  outputs[255][0] = 5.38;
  outputs[255][1] = 3.04;

  // section 256
  carac_sections[256][0] = 13125.80;
  carac_sections[256][1] = 0.00;
  carac_sections[256][2] = 24.40;
  carac_sections[256][3] = 100.00;
  outputs[256][0] = 5.43;
  outputs[256][1] = 3.01;

  // section 257
  carac_sections[257][0] = 13199.45;
  carac_sections[257][1] = 0.00;
  carac_sections[257][2] = 24.33;
  carac_sections[257][3] = 100.00;
  outputs[257][0] = 5.48;
  outputs[257][1] = 2.98;

  // section 258
  carac_sections[258][0] = 13271.76;
  carac_sections[258][1] = 0.00;
  carac_sections[258][2] = 24.26;
  carac_sections[258][3] = 100.00;
  outputs[258][0] = 5.53;
  outputs[258][1] = 2.95;

  // section 259
  carac_sections[259][0] = 13343.22;
  carac_sections[259][1] = 0.00;
  carac_sections[259][2] = 24.19;
  carac_sections[259][3] = 100.00;
  outputs[259][0] = 5.58;
  outputs[259][1] = 2.93;

  // section 260
  carac_sections[260][0] = 13410.11;
  carac_sections[260][1] = 0.00;
  carac_sections[260][2] = 24.12;
  carac_sections[260][3] = 100.00;
  outputs[260][0] = 5.63;
  outputs[260][1] = 2.90;

  // section 261
  carac_sections[261][0] = 13475.35;
  carac_sections[261][1] = 0.00;
  carac_sections[261][2] = 24.06;
  carac_sections[261][3] = 100.00;
  outputs[261][0] = 5.67;
  outputs[261][1] = 2.88;

  // section 262
  carac_sections[262][0] = 13539.09;
  carac_sections[262][1] = 0.00;
  carac_sections[262][2] = 24.00;
  carac_sections[262][3] = 100.00;
  outputs[262][0] = 5.72;
  outputs[262][1] = 2.86;

  // section 263
  carac_sections[263][0] = 13602.21;
  carac_sections[263][1] = 0.00;
  carac_sections[263][2] = 23.94;
  carac_sections[263][3] = 100.00;
  outputs[263][0] = 5.76;
  outputs[263][1] = 2.84;

  // section 264
  carac_sections[264][0] = 13662.72;
  carac_sections[264][1] = 0.00;
  carac_sections[264][2] = 23.88;
  carac_sections[264][3] = 100.00;
  outputs[264][0] = 5.80;
  outputs[264][1] = 2.82;

  // section 265
  carac_sections[265][0] = 13722.35;
  carac_sections[265][1] = 0.00;
  carac_sections[265][2] = 23.83;
  carac_sections[265][3] = 100.00;
  outputs[265][0] = 5.84;
  outputs[265][1] = 2.80;

  // section 266
  carac_sections[266][0] = 13780.74;
  carac_sections[266][1] = 0.00;
  carac_sections[266][2] = 23.78;
  carac_sections[266][3] = 100.00;
  outputs[266][0] = 5.87;
  outputs[266][1] = 2.78;

  // section 267
  carac_sections[267][0] = 13839.11;
  carac_sections[267][1] = 0.00;
  carac_sections[267][2] = 23.72;
  carac_sections[267][3] = 100.00;
  outputs[267][0] = 5.91;
  outputs[267][1] = 2.76;

  // section 268
  carac_sections[268][0] = 13892.46;
  carac_sections[268][1] = 0.00;
  carac_sections[268][2] = 23.68;
  carac_sections[268][3] = 100.00;
  outputs[268][0] = 5.94;
  outputs[268][1] = 2.75;

  // section 269
  carac_sections[269][0] = 13944.91;
  carac_sections[269][1] = 0.00;
  carac_sections[269][2] = 23.64;
  carac_sections[269][3] = 100.00;
  outputs[269][0] = 5.98;
  outputs[269][1] = 2.73;

  // section 270
  carac_sections[270][0] = 13996.13;
  carac_sections[270][1] = 0.00;
  carac_sections[270][2] = 23.59;
  carac_sections[270][3] = 100.00;
  outputs[270][0] = 6.01;
  outputs[270][1] = 2.72;

  // section 271
  carac_sections[271][0] = 14047.45;
  carac_sections[271][1] = 0.00;
  carac_sections[271][2] = 23.55;
  carac_sections[271][3] = 100.00;
  outputs[271][0] = 6.03;
  outputs[271][1] = 2.71;

  // section 272
  carac_sections[272][0] = 14095.92;
  carac_sections[272][1] = 0.00;
  carac_sections[272][2] = 23.51;
  carac_sections[272][3] = 100.00;
  outputs[272][0] = 6.06;
  outputs[272][1] = 2.69;

  // section 273
  carac_sections[273][0] = 14144.08;
  carac_sections[273][1] = 0.00;
  carac_sections[273][2] = 23.48;
  carac_sections[273][3] = 100.00;
  outputs[273][0] = 6.09;
  outputs[273][1] = 2.68;

  // section 274
  carac_sections[274][0] = 14191.16;
  carac_sections[274][1] = 0.00;
  carac_sections[274][2] = 23.44;
  carac_sections[274][3] = 100.00;
  outputs[274][0] = 6.11;
  outputs[274][1] = 2.67;

  // section 275
  carac_sections[275][0] = 14239.12;
  carac_sections[275][1] = 0.00;
  carac_sections[275][2] = 23.40;
  carac_sections[275][3] = 100.00;
  outputs[275][0] = 6.14;
  outputs[275][1] = 2.66;

  // section 276
  carac_sections[276][0] = 14281.90;
  carac_sections[276][1] = 0.00;
  carac_sections[276][2] = 23.37;
  carac_sections[276][3] = 100.00;
  outputs[276][0] = 6.16;
  outputs[276][1] = 2.65;

  // section 277
  carac_sections[277][0] = 14324.58;
  carac_sections[277][1] = 0.00;
  carac_sections[277][2] = 23.34;
  carac_sections[277][3] = 100.00;
  outputs[277][0] = 6.18;
  outputs[277][1] = 2.64;

  // section 278
  carac_sections[278][0] = 14366.15;
  carac_sections[278][1] = 0.00;
  carac_sections[278][2] = 23.32;
  carac_sections[278][3] = 100.00;
  outputs[278][0] = 6.20;
  outputs[278][1] = 2.63;

  // section 279
  carac_sections[279][0] = 14408.42;
  carac_sections[279][1] = 0.00;
  carac_sections[279][2] = 23.29;
  carac_sections[279][3] = 100.00;
  outputs[279][0] = 6.22;
  outputs[279][1] = 2.63;

  // section 280
  carac_sections[280][0] = 14447.78;
  carac_sections[280][1] = 0.00;
  carac_sections[280][2] = 23.26;
  carac_sections[280][3] = 100.00;
  outputs[280][0] = 6.23;
  outputs[280][1] = 2.62;

  // section 281
  carac_sections[281][0] = 14487.37;
  carac_sections[281][1] = 0.00;
  carac_sections[281][2] = 23.24;
  carac_sections[281][3] = 100.00;
  outputs[281][0] = 6.25;
  outputs[281][1] = 2.61;

  // section 282
  carac_sections[282][0] = 14525.88;
  carac_sections[282][1] = 0.00;
  carac_sections[282][2] = 23.21;
  carac_sections[282][3] = 100.00;
  outputs[282][0] = 6.26;
  outputs[282][1] = 2.61;

  // section 283
  carac_sections[283][0] = 14564.75;
  carac_sections[283][1] = 0.00;
  carac_sections[283][2] = 23.19;
  carac_sections[283][3] = 100.00;
  outputs[283][0] = 6.28;
  outputs[283][1] = 2.60;

  // section 284
  carac_sections[284][0] = 14635.39;
  carac_sections[284][1] = 0.00;
  carac_sections[284][2] = 23.15;
  carac_sections[284][3] = 100.00;
  outputs[284][0] = 6.30;
  outputs[284][1] = 2.59;

  // section 285
  carac_sections[285][0] = 14704.05;
  carac_sections[285][1] = 0.00;
  carac_sections[285][2] = 23.11;
  carac_sections[285][3] = 100.00;
  outputs[285][0] = 6.33;
  outputs[285][1] = 2.58;

  // section 286
  carac_sections[286][0] = 14769.80;
  carac_sections[286][1] = 0.00;
  carac_sections[286][2] = 23.07;
  carac_sections[286][3] = 100.00;
  outputs[286][0] = 6.35;
  outputs[286][1] = 2.57;

  // section 287
  carac_sections[287][0] = 14834.52;
  carac_sections[287][1] = 0.00;
  carac_sections[287][2] = 23.04;
  carac_sections[287][3] = 100.00;
  outputs[287][0] = 6.36;
  outputs[287][1] = 2.57;

  // section 288
  carac_sections[288][0] = 14893.73;
  carac_sections[288][1] = 0.00;
  carac_sections[288][2] = 23.01;
  carac_sections[288][3] = 100.00;
  outputs[288][0] = 6.38;
  outputs[288][1] = 2.56;

  // section 289
  carac_sections[289][0] = 14951.58;
  carac_sections[289][1] = 0.00;
  carac_sections[289][2] = 22.99;
  carac_sections[289][3] = 100.00;
  outputs[289][0] = 6.39;
  outputs[289][1] = 2.56;

  // section 290
  carac_sections[290][0] = 15006.96;
  carac_sections[290][1] = 0.00;
  carac_sections[290][2] = 22.96;
  carac_sections[290][3] = 100.00;
  outputs[290][0] = 6.40;
  outputs[290][1] = 2.55;

  // section 291
  carac_sections[291][0] = 15062.31;
  carac_sections[291][1] = 0.00;
  carac_sections[291][2] = 22.94;
  carac_sections[291][3] = 100.00;
  outputs[291][0] = 6.41;
  outputs[291][1] = 2.55;

  // section 292
  carac_sections[292][0] = 15112.17;
  carac_sections[292][1] = 0.00;
  carac_sections[292][2] = 22.92;
  carac_sections[292][3] = 100.00;
  outputs[292][0] = 6.42;
  outputs[292][1] = 2.54;

  // section 293
  carac_sections[293][0] = 15161.47;
  carac_sections[293][1] = 0.00;
  carac_sections[293][2] = 22.90;
  carac_sections[293][3] = 100.00;
  outputs[293][0] = 6.42;
  outputs[293][1] = 2.54;

  // section 294
  carac_sections[294][0] = 15208.58;
  carac_sections[294][1] = 0.00;
  carac_sections[294][2] = 22.89;
  carac_sections[294][3] = 100.00;
  outputs[294][0] = 6.43;
  outputs[294][1] = 2.54;

  // section 295
  carac_sections[295][0] = 15256.53;
  carac_sections[295][1] = 0.00;
  carac_sections[295][2] = 22.87;
  carac_sections[295][3] = 100.00;
  outputs[295][0] = 6.43;
  outputs[295][1] = 2.54;

  // section 296
  carac_sections[296][0] = 15298.85;
  carac_sections[296][1] = 0.00;
  carac_sections[296][2] = 22.86;
  carac_sections[296][3] = 100.00;
  outputs[296][0] = 6.43;
  outputs[296][1] = 2.54;

  // section 297
  carac_sections[297][0] = 15341.33;
  carac_sections[297][1] = 0.00;
  carac_sections[297][2] = 22.85;
  carac_sections[297][3] = 100.00;
  outputs[297][0] = 6.43;
  outputs[297][1] = 2.54;

  // section 298
  carac_sections[298][0] = 15381.75;
  carac_sections[298][1] = 0.00;
  carac_sections[298][2] = 22.84;
  carac_sections[298][3] = 100.00;
  outputs[298][0] = 6.43;
  outputs[298][1] = 2.54;

  // section 299
  carac_sections[299][0] = 15423.17;
  carac_sections[299][1] = 0.00;
  carac_sections[299][2] = 22.83;
  carac_sections[299][3] = 100.00;
  outputs[299][0] = 6.43;
  outputs[299][1] = 2.54;

  // section 300
  carac_sections[300][0] = 15496.58;
  carac_sections[300][1] = 0.00;
  carac_sections[300][2] = 22.81;
  carac_sections[300][3] = 100.00;
  outputs[300][0] = 6.43;
  outputs[300][1] = 2.54;

  // section 301
  carac_sections[301][0] = 15567.99;
  carac_sections[301][1] = 0.00;
  carac_sections[301][2] = 22.80;
  carac_sections[301][3] = 100.00;
  outputs[301][0] = 6.43;
  outputs[301][1] = 2.54;

  // section 302
  carac_sections[302][0] = 15633.60;
  carac_sections[302][1] = 0.00;
  carac_sections[302][2] = 22.79;
  carac_sections[302][3] = 100.00;
  outputs[302][0] = 6.42;
  outputs[302][1] = 2.54;

  // section 303
  carac_sections[303][0] = 15696.99;
  carac_sections[303][1] = 0.00;
  carac_sections[303][2] = 22.78;
  carac_sections[303][3] = 100.00;
  outputs[303][0] = 6.41;
  outputs[303][1] = 2.55;

  // section 304
  carac_sections[304][0] = 15755.00;
  carac_sections[304][1] = 0.00;
  carac_sections[304][2] = 22.78;
  carac_sections[304][3] = 100.00;
  outputs[304][0] = 6.40;
  outputs[304][1] = 2.55;

  // section 305
  carac_sections[305][0] = 15811.75;
  carac_sections[305][1] = 0.00;
  carac_sections[305][2] = 22.77;
  carac_sections[305][3] = 100.00;
  outputs[305][0] = 6.39;
  outputs[305][1] = 2.56;

  // section 306
  carac_sections[306][0] = 15863.45;
  carac_sections[306][1] = 0.00;
  carac_sections[306][2] = 22.77;
  carac_sections[306][3] = 100.00;
  outputs[306][0] = 6.38;
  outputs[306][1] = 2.56;

  // section 307
  carac_sections[307][0] = 15914.71;
  carac_sections[307][1] = 0.00;
  carac_sections[307][2] = 22.77;
  carac_sections[307][3] = 100.00;
  outputs[307][0] = 6.37;
  outputs[307][1] = 2.56;

  // section 308
  carac_sections[308][0] = 15961.16;
  carac_sections[308][1] = 0.00;
  carac_sections[308][2] = 22.76;
  carac_sections[308][3] = 100.00;
  outputs[308][0] = 6.35;
  outputs[308][1] = 2.57;

  // section 309
  carac_sections[309][0] = 16007.87;
  carac_sections[309][1] = 0.00;
  carac_sections[309][2] = 22.76;
  carac_sections[309][3] = 100.00;
  outputs[309][0] = 6.34;
  outputs[309][1] = 2.57;

  // section 310
  carac_sections[310][0] = 16049.94;
  carac_sections[310][1] = 0.00;
  carac_sections[310][2] = 22.77;
  carac_sections[310][3] = 100.00;
  outputs[310][0] = 6.33;
  outputs[310][1] = 2.58;

  // section 311
  carac_sections[311][0] = 16092.87;
  carac_sections[311][1] = 0.00;
  carac_sections[311][2] = 22.77;
  carac_sections[311][3] = 100.00;
  outputs[311][0] = 6.31;
  outputs[311][1] = 2.59;

  // section 312
  carac_sections[312][0] = 16131.24;
  carac_sections[312][1] = 0.00;
  carac_sections[312][2] = 22.77;
  carac_sections[312][3] = 100.00;
  outputs[312][0] = 6.30;
  outputs[312][1] = 2.59;

  // section 313
  carac_sections[313][0] = 16169.86;
  carac_sections[313][1] = 0.00;
  carac_sections[313][2] = 22.77;
  carac_sections[313][3] = 100.00;
  outputs[313][0] = 6.28;
  outputs[313][1] = 2.60;

  // section 314
  carac_sections[314][0] = 16241.99;
  carac_sections[314][1] = 0.00;
  carac_sections[314][2] = 22.78;
  carac_sections[314][3] = 100.00;
  outputs[314][0] = 6.26;
  outputs[314][1] = 2.61;

  // section 315
  carac_sections[315][0] = 16309.76;
  carac_sections[315][1] = 0.00;
  carac_sections[315][2] = 22.78;
  carac_sections[315][3] = 100.00;
  outputs[315][0] = 6.23;
  outputs[315][1] = 2.62;

  // section 316
  carac_sections[316][0] = 16373.48;
  carac_sections[316][1] = 0.00;
  carac_sections[316][2] = 22.79;
  carac_sections[316][3] = 100.00;
  outputs[316][0] = 6.20;
  outputs[316][1] = 2.63;

  // section 317
  carac_sections[317][0] = 16433.81;
  carac_sections[317][1] = 0.00;
  carac_sections[317][2] = 22.80;
  carac_sections[317][3] = 100.00;
  outputs[317][0] = 6.17;
  outputs[317][1] = 2.65;

  // section 318
  carac_sections[318][0] = 16491.36;
  carac_sections[318][1] = 0.00;
  carac_sections[318][2] = 22.81;
  carac_sections[318][3] = 100.00;
  outputs[318][0] = 6.14;
  outputs[318][1] = 2.66;

  // section 319
  carac_sections[319][0] = 16546.68;
  carac_sections[319][1] = 0.00;
  carac_sections[319][2] = 22.82;
  carac_sections[319][3] = 100.00;
  outputs[319][0] = 6.12;
  outputs[319][1] = 2.67;

  // section 320
  carac_sections[320][0] = 16600.26;
  carac_sections[320][1] = 0.00;
  carac_sections[320][2] = 22.83;
  carac_sections[320][3] = 100.00;
  outputs[320][0] = 6.09;
  outputs[320][1] = 2.68;

  // section 321
  carac_sections[321][0] = 16652.51;
  carac_sections[321][1] = 0.00;
  carac_sections[321][2] = 22.84;
  carac_sections[321][3] = 100.00;
  outputs[321][0] = 6.06;
  outputs[321][1] = 2.70;

  // section 322
  carac_sections[322][0] = 16704.02;
  carac_sections[322][1] = 0.00;
  carac_sections[322][2] = 22.85;
  carac_sections[322][3] = 100.00;
  outputs[322][0] = 6.03;
  outputs[322][1] = 2.71;

  // section 323
  carac_sections[323][0] = 16755.03;
  carac_sections[323][1] = 0.00;
  carac_sections[323][2] = 22.86;
  carac_sections[323][3] = 100.00;
  outputs[323][0] = 6.00;
  outputs[323][1] = 2.72;

  // section 324
  carac_sections[324][0] = 16806.02;
  carac_sections[324][1] = 0.00;
  carac_sections[324][2] = 22.87;
  carac_sections[324][3] = 100.00;
  outputs[324][0] = 5.97;
  outputs[324][1] = 2.74;

  // section 325
  carac_sections[325][0] = 16857.48;
  carac_sections[325][1] = 0.00;
  carac_sections[325][2] = 22.88;
  carac_sections[325][3] = 100.00;
  outputs[325][0] = 5.93;
  outputs[325][1] = 2.75;

  // section 326
  carac_sections[326][0] = 16910.01;
  carac_sections[326][1] = 0.00;
  carac_sections[326][2] = 22.89;
  carac_sections[326][3] = 100.00;
  outputs[326][0] = 5.90;
  outputs[326][1] = 2.77;

  // section 327
  carac_sections[327][0] = 16964.04;
  carac_sections[327][1] = 0.00;
  carac_sections[327][2] = 22.90;
  carac_sections[327][3] = 100.00;
  outputs[327][0] = 5.87;
  outputs[327][1] = 2.78;

  // section 328
  carac_sections[328][0] = 17020.47;
  carac_sections[328][1] = 0.00;
  carac_sections[328][2] = 22.92;
  carac_sections[328][3] = 100.00;
  outputs[328][0] = 5.83;
  outputs[328][1] = 2.80;

  // section 329
  carac_sections[329][0] = 17079.93;
  carac_sections[329][1] = 0.00;
  carac_sections[329][2] = 22.93;
  carac_sections[329][3] = 100.00;
  outputs[329][0] = 5.79;
  outputs[329][1] = 2.82;

  // section 330
  carac_sections[330][0] = 17143.57;
  carac_sections[330][1] = 0.00;
  carac_sections[330][2] = 22.95;
  carac_sections[330][3] = 100.00;
  outputs[330][0] = 5.74;
  outputs[330][1] = 2.84;

  // section 331
  carac_sections[331][0] = 17213.29;
  carac_sections[331][1] = 0.00;
  carac_sections[331][2] = 22.96;
  carac_sections[331][3] = 100.00;
  outputs[331][0] = 5.70;
  outputs[331][1] = 2.86;

  // section 332
  carac_sections[332][0] = 17250.18;
  carac_sections[332][1] = 0.00;
  carac_sections[332][2] = 22.97;
  carac_sections[332][3] = 100.00;
  outputs[332][0] = 5.67;
  outputs[332][1] = 2.88;

  // section 333
  carac_sections[333][0] = 17288.63;
  carac_sections[333][1] = 0.00;
  carac_sections[333][2] = 22.98;
  carac_sections[333][3] = 100.00;
  outputs[333][0] = 5.65;
  outputs[333][1] = 2.89;

  // section 334
  carac_sections[334][0] = 17331.08;
  carac_sections[334][1] = 0.00;
  carac_sections[334][2] = 22.99;
  carac_sections[334][3] = 100.00;
  outputs[334][0] = 5.62;
  outputs[334][1] = 2.91;

  // section 335
  carac_sections[335][0] = 17375.14;
  carac_sections[335][1] = 0.00;
  carac_sections[335][2] = 22.99;
  carac_sections[335][3] = 100.00;
  outputs[335][0] = 5.59;
  outputs[335][1] = 2.92;

  // section 336
  carac_sections[336][0] = 17423.40;
  carac_sections[336][1] = 0.00;
  carac_sections[336][2] = 23.00;
  carac_sections[336][3] = 100.00;
  outputs[336][0] = 5.55;
  outputs[336][1] = 2.94;

  // section 337
  carac_sections[337][0] = 17474.92;
  carac_sections[337][1] = 0.00;
  carac_sections[337][2] = 23.01;
  carac_sections[337][3] = 100.00;
  outputs[337][0] = 5.52;
  outputs[337][1] = 2.96;

  // section 338
  carac_sections[338][0] = 17531.56;
  carac_sections[338][1] = 0.00;
  carac_sections[338][2] = 23.02;
  carac_sections[338][3] = 100.00;
  outputs[338][0] = 5.48;
  outputs[338][1] = 2.98;

  // section 339
  carac_sections[339][0] = 17593.44;
  carac_sections[339][1] = 0.00;
  carac_sections[339][2] = 23.02;
  carac_sections[339][3] = 100.00;
  outputs[339][0] = 5.44;
  outputs[339][1] = 3.00;

  // section 340
  carac_sections[340][0] = 17662.11;
  carac_sections[340][1] = 0.00;
  carac_sections[340][2] = 23.03;
  carac_sections[340][3] = 100.00;
  outputs[340][0] = 5.40;
  outputs[340][1] = 3.02;

  // section 341
  carac_sections[341][0] = 17736.92;
  carac_sections[341][1] = 0.00;
  carac_sections[341][2] = 23.03;
  carac_sections[341][3] = 100.00;
  outputs[341][0] = 5.36;
  outputs[341][1] = 3.04;

  // section 342
  carac_sections[342][0] = 17780.12;
  carac_sections[342][1] = 0.00;
  carac_sections[342][2] = 23.03;
  carac_sections[342][3] = 100.00;
  outputs[342][0] = 5.34;
  outputs[342][1] = 3.06;

  // section 343
  carac_sections[343][0] = 17823.29;
  carac_sections[343][1] = 0.00;
  carac_sections[343][2] = 23.03;
  carac_sections[343][3] = 100.00;
  outputs[343][0] = 5.31;
  outputs[343][1] = 3.07;

  // section 344
  carac_sections[344][0] = 17871.49;
  carac_sections[344][1] = 0.00;
  carac_sections[344][2] = 23.03;
  carac_sections[344][3] = 100.00;
  outputs[344][0] = 5.29;
  outputs[344][1] = 3.09;

  // section 345
  carac_sections[345][0] = 17921.22;
  carac_sections[345][1] = 0.00;
  carac_sections[345][2] = 23.02;
  carac_sections[345][3] = 100.00;
  outputs[345][0] = 5.27;
  outputs[345][1] = 3.10;

  // section 346
  carac_sections[346][0] = 17975.17;
  carac_sections[346][1] = 0.00;
  carac_sections[346][2] = 23.01;
  carac_sections[346][3] = 100.00;
  outputs[346][0] = 5.25;
  outputs[346][1] = 3.11;

  // section 347
  carac_sections[347][0] = 18031.91;
  carac_sections[347][1] = 0.00;
  carac_sections[347][2] = 23.00;
  carac_sections[347][3] = 100.00;
  outputs[347][0] = 5.23;
  outputs[347][1] = 3.12;

  // section 348
  carac_sections[348][0] = 18093.82;
  carac_sections[348][1] = 0.00;
  carac_sections[348][2] = 22.98;
  carac_sections[348][3] = 100.00;
  outputs[348][0] = 5.21;
  outputs[348][1] = 3.13;

  // section 349
  carac_sections[349][0] = 18159.03;
  carac_sections[349][1] = 0.00;
  carac_sections[349][2] = 22.96;
  carac_sections[349][3] = 100.00;
  outputs[349][0] = 5.20;
  outputs[349][1] = 3.14;

  // section 350
  carac_sections[350][0] = 18228.58;
  carac_sections[350][1] = 0.00;
  carac_sections[350][2] = 22.94;
  carac_sections[350][3] = 100.00;
  outputs[350][0] = 5.19;
  outputs[350][1] = 3.15;

  // section 351
  carac_sections[351][0] = 18301.55;
  carac_sections[351][1] = 0.00;
  carac_sections[351][2] = 22.91;
  carac_sections[351][3] = 100.00;
  outputs[351][0] = 5.19;
  outputs[351][1] = 3.15;

  // section 352
  carac_sections[352][0] = 18341.30;
  carac_sections[352][1] = 0.00;
  carac_sections[352][2] = 22.89;
  carac_sections[352][3] = 100.00;
  outputs[352][0] = 5.18;
  outputs[352][1] = 3.15;

  // section 353
  carac_sections[353][0] = 18380.57;
  carac_sections[353][1] = 0.00;
  carac_sections[353][2] = 22.87;
  carac_sections[353][3] = 100.00;
  outputs[353][0] = 5.18;
  outputs[353][1] = 3.15;

  // section 354
  carac_sections[354][0] = 18422.30;
  carac_sections[354][1] = 0.00;
  carac_sections[354][2] = 22.84;
  carac_sections[354][3] = 100.00;
  outputs[354][0] = 5.19;
  outputs[354][1] = 3.15;

  // section 355
  carac_sections[355][0] = 18463.65;
  carac_sections[355][1] = 0.00;
  carac_sections[355][2] = 22.82;
  carac_sections[355][3] = 100.00;
  outputs[355][0] = 5.19;
  outputs[355][1] = 3.15;

  // section 356
  carac_sections[356][0] = 18507.07;
  carac_sections[356][1] = 0.00;
  carac_sections[356][2] = 22.79;
  carac_sections[356][3] = 100.00;
  outputs[356][0] = 5.20;
  outputs[356][1] = 3.14;

  // section 357
  carac_sections[357][0] = 18550.37;
  carac_sections[357][1] = 0.00;
  carac_sections[357][2] = 22.76;
  carac_sections[357][3] = 100.00;
  outputs[357][0] = 5.21;
  outputs[357][1] = 3.14;

  // section 358
  carac_sections[358][0] = 18595.44;
  carac_sections[358][1] = 0.00;
  carac_sections[358][2] = 22.73;
  carac_sections[358][3] = 100.00;
  outputs[358][0] = 5.22;
  outputs[358][1] = 3.13;

  // section 359
  carac_sections[359][0] = 18640.60;
  carac_sections[359][1] = 0.00;
  carac_sections[359][2] = 22.70;
  carac_sections[359][3] = 100.00;
  outputs[359][0] = 5.23;
  outputs[359][1] = 3.12;

  // section 360
  carac_sections[360][0] = 18687.93;
  carac_sections[360][1] = 0.00;
  carac_sections[360][2] = 22.66;
  carac_sections[360][3] = 100.00;
  outputs[360][0] = 5.25;
  outputs[360][1] = 3.11;

  // section 361
  carac_sections[361][0] = 18735.24;
  carac_sections[361][1] = 0.00;
  carac_sections[361][2] = 22.62;
  carac_sections[361][3] = 100.00;
  outputs[361][0] = 5.27;
  outputs[361][1] = 3.10;

  // section 362
  carac_sections[362][0] = 18783.92;
  carac_sections[362][1] = 0.00;
  carac_sections[362][2] = 22.58;
  carac_sections[362][3] = 100.00;
  outputs[362][0] = 5.29;
  outputs[362][1] = 3.09;

  // section 363
  carac_sections[363][0] = 18832.63;
  carac_sections[363][1] = 0.00;
  carac_sections[363][2] = 22.54;
  carac_sections[363][3] = 100.00;
  outputs[363][0] = 5.31;
  outputs[363][1] = 3.07;

  // section 364
  carac_sections[364][0] = 18881.55;
  carac_sections[364][1] = 0.00;
  carac_sections[364][2] = 22.50;
  carac_sections[364][3] = 100.00;
  outputs[364][0] = 5.33;
  outputs[364][1] = 3.06;

  // section 365
  carac_sections[365][0] = 18930.71;
  carac_sections[365][1] = 0.00;
  carac_sections[365][2] = 22.46;
  carac_sections[365][3] = 100.00;
  outputs[365][0] = 5.36;
  outputs[365][1] = 3.05;

  // section 366
  carac_sections[366][0] = 18980.32;
  carac_sections[366][1] = 0.00;
  carac_sections[366][2] = 22.41;
  carac_sections[366][3] = 100.00;
  outputs[366][0] = 5.39;
  outputs[366][1] = 3.03;

  // section 367
  carac_sections[367][0] = 19030.12;
  carac_sections[367][1] = 0.00;
  carac_sections[367][2] = 22.37;
  carac_sections[367][3] = 100.00;
  outputs[367][0] = 5.42;
  outputs[367][1] = 3.01;

  // section 368
  carac_sections[368][0] = 19080.72;
  carac_sections[368][1] = 0.00;
  carac_sections[368][2] = 22.32;
  carac_sections[368][3] = 100.00;
  outputs[368][0] = 5.45;
  outputs[368][1] = 3.00;

  // section 369
  carac_sections[369][0] = 19131.31;
  carac_sections[369][1] = 0.00;
  carac_sections[369][2] = 22.27;
  carac_sections[369][3] = 100.00;
  outputs[369][0] = 5.48;
  outputs[369][1] = 2.98;

  // section 370
  carac_sections[370][0] = 19182.22;
  carac_sections[370][1] = 0.00;
  carac_sections[370][2] = 22.22;
  carac_sections[370][3] = 100.00;
  outputs[370][0] = 5.52;
  outputs[370][1] = 2.96;

  // section 371
  carac_sections[371][0] = 19233.36;
  carac_sections[371][1] = 0.00;
  carac_sections[371][2] = 22.17;
  carac_sections[371][3] = 100.00;
  outputs[371][0] = 5.55;
  outputs[371][1] = 2.94;

  // section 372
  carac_sections[372][0] = 19283.21;
  carac_sections[372][1] = 0.00;
  carac_sections[372][2] = 22.12;
  carac_sections[372][3] = 100.00;
  outputs[372][0] = 5.59;
  outputs[372][1] = 2.92;

  // section 373
  carac_sections[373][0] = 19332.89;
  carac_sections[373][1] = 0.00;
  carac_sections[373][2] = 22.07;
  carac_sections[373][3] = 100.00;
  outputs[373][0] = 5.62;
  outputs[373][1] = 2.90;

  // section 374
  carac_sections[374][0] = 19382.42;
  carac_sections[374][1] = 0.00;
  carac_sections[374][2] = 22.02;
  carac_sections[374][3] = 100.00;
  outputs[374][0] = 5.66;
  outputs[374][1] = 2.89;

  // section 375
  carac_sections[375][0] = 19431.93;
  carac_sections[375][1] = 0.00;
  carac_sections[375][2] = 21.97;
  carac_sections[375][3] = 100.00;
  outputs[375][0] = 5.70;
  outputs[375][1] = 2.87;

  // section 376
  carac_sections[376][0] = 19481.22;
  carac_sections[376][1] = 0.00;
  carac_sections[376][2] = 21.92;
  carac_sections[376][3] = 100.00;
  outputs[376][0] = 5.73;
  outputs[376][1] = 2.85;

  // section 377
  carac_sections[377][0] = 19530.47;
  carac_sections[377][1] = 0.00;
  carac_sections[377][2] = 21.87;
  carac_sections[377][3] = 100.00;
  outputs[377][0] = 5.77;
  outputs[377][1] = 2.83;

  // section 378
  carac_sections[378][0] = 19579.45;
  carac_sections[378][1] = 0.00;
  carac_sections[378][2] = 21.82;
  carac_sections[378][3] = 100.00;
  outputs[378][0] = 5.81;
  outputs[378][1] = 2.81;

  // section 379
  carac_sections[379][0] = 19628.79;
  carac_sections[379][1] = 0.00;
  carac_sections[379][2] = 21.78;
  carac_sections[379][3] = 100.00;
  outputs[379][0] = 5.84;
  outputs[379][1] = 2.79;

  // section 380
  carac_sections[380][0] = 19675.62;
  carac_sections[380][1] = 0.00;
  carac_sections[380][2] = 21.73;
  carac_sections[380][3] = 100.00;
  outputs[380][0] = 5.88;
  outputs[380][1] = 2.78;

  // section 381
  carac_sections[381][0] = 19722.19;
  carac_sections[381][1] = 0.00;
  carac_sections[381][2] = 21.68;
  carac_sections[381][3] = 100.00;
  outputs[381][0] = 5.91;
  outputs[381][1] = 2.76;

  // section 382
  carac_sections[382][0] = 19768.24;
  carac_sections[382][1] = 0.00;
  carac_sections[382][2] = 21.64;
  carac_sections[382][3] = 100.00;
  outputs[382][0] = 5.95;
  outputs[382][1] = 2.75;

  // section 383
  carac_sections[383][0] = 19814.36;
  carac_sections[383][1] = 0.00;
  carac_sections[383][2] = 21.60;
  carac_sections[383][3] = 100.00;
  outputs[383][0] = 5.98;
  outputs[383][1] = 2.73;

  // section 384
  carac_sections[384][0] = 19859.64;
  carac_sections[384][1] = 0.00;
  carac_sections[384][2] = 21.55;
  carac_sections[384][3] = 100.00;
  outputs[384][0] = 6.01;
  outputs[384][1] = 2.71;

  // section 385
  carac_sections[385][0] = 19904.86;
  carac_sections[385][1] = 0.00;
  carac_sections[385][2] = 21.51;
  carac_sections[385][3] = 100.00;
  outputs[385][0] = 6.05;
  outputs[385][1] = 2.70;

  // section 386
  carac_sections[386][0] = 19949.53;
  carac_sections[386][1] = 0.00;
  carac_sections[386][2] = 21.47;
  carac_sections[386][3] = 100.00;
  outputs[386][0] = 6.08;
  outputs[386][1] = 2.69;

  // section 387
  carac_sections[387][0] = 19994.84;
  carac_sections[387][1] = 0.00;
  carac_sections[387][2] = 21.43;
  carac_sections[387][3] = 100.00;
  outputs[387][0] = 6.11;
  outputs[387][1] = 2.67;

  // section 388
  carac_sections[388][0] = 20037.00;
  carac_sections[388][1] = 0.00;
  carac_sections[388][2] = 21.39;
  carac_sections[388][3] = 100.00;
  outputs[388][0] = 6.14;
  outputs[388][1] = 2.66;

  // section 389
  carac_sections[389][0] = 20079.14;
  carac_sections[389][1] = 0.00;
  carac_sections[389][2] = 21.35;
  carac_sections[389][3] = 100.00;
  outputs[389][0] = 6.17;
  outputs[389][1] = 2.65;

  // section 390
  carac_sections[390][0] = 20120.52;
  carac_sections[390][1] = 0.00;
  carac_sections[390][2] = 21.31;
  carac_sections[390][3] = 100.00;
  outputs[390][0] = 6.20;
  outputs[390][1] = 2.63;

  // section 391
  carac_sections[391][0] = 20162.18;
  carac_sections[391][1] = 0.00;
  carac_sections[391][2] = 21.28;
  carac_sections[391][3] = 100.00;
  outputs[391][0] = 6.22;
  outputs[391][1] = 2.62;

  // section 392
  carac_sections[392][0] = 20202.56;
  carac_sections[392][1] = 0.00;
  carac_sections[392][2] = 21.24;
  carac_sections[392][3] = 100.00;
  outputs[392][0] = 6.25;
  outputs[392][1] = 2.61;

  // section 393
  carac_sections[393][0] = 20243.10;
  carac_sections[393][1] = 0.00;
  carac_sections[393][2] = 21.21;
  carac_sections[393][3] = 100.00;
  outputs[393][0] = 6.28;
  outputs[393][1] = 2.60;

  // section 394
  carac_sections[394][0] = 20282.84;
  carac_sections[394][1] = 0.00;
  carac_sections[394][2] = 21.18;
  carac_sections[394][3] = 100.00;
  outputs[394][0] = 6.30;
  outputs[394][1] = 2.59;

  // section 395
  carac_sections[395][0] = 20323.48;
  carac_sections[395][1] = 0.00;
  carac_sections[395][2] = 21.14;
  carac_sections[395][3] = 100.00;
  outputs[395][0] = 6.32;
  outputs[395][1] = 2.58;

  // section 396
  carac_sections[396][0] = 20397.85;
  carac_sections[396][1] = 0.00;
  carac_sections[396][2] = 21.09;
  carac_sections[396][3] = 100.00;
  outputs[396][0] = 6.37;
  outputs[396][1] = 2.56;

  // section 397
  carac_sections[397][0] = 20471.27;
  carac_sections[397][1] = 0.00;
  carac_sections[397][2] = 21.03;
  carac_sections[397][3] = 100.00;
  outputs[397][0] = 6.41;
  outputs[397][1] = 2.55;

  // section 398
  carac_sections[398][0] = 20542.90;
  carac_sections[398][1] = 0.00;
  carac_sections[398][2] = 20.98;
  carac_sections[398][3] = 100.00;
  outputs[398][0] = 6.45;
  outputs[398][1] = 2.53;

  // section 399
  carac_sections[399][0] = 20613.46;
  carac_sections[399][1] = 0.00;
  carac_sections[399][2] = 20.93;
  carac_sections[399][3] = 100.00;
  outputs[399][0] = 6.48;
  outputs[399][1] = 2.52;

  // section 400
  carac_sections[400][0] = 20679.81;
  carac_sections[400][1] = 0.00;
  carac_sections[400][2] = 20.89;
  carac_sections[400][3] = 100.00;
  outputs[400][0] = 6.52;
  outputs[400][1] = 2.51;

  // section 401
  carac_sections[401][0] = 20744.54;
  carac_sections[401][1] = 0.00;
  carac_sections[401][2] = 20.84;
  carac_sections[401][3] = 100.00;
  outputs[401][0] = 6.54;
  outputs[401][1] = 2.50;

  // section 402
  carac_sections[402][0] = 20807.54;
  carac_sections[402][1] = 0.00;
  carac_sections[402][2] = 20.81;
  carac_sections[402][3] = 100.00;
  outputs[402][0] = 6.57;
  outputs[402][1] = 2.49;

  // section 403
  carac_sections[403][0] = 20869.97;
  carac_sections[403][1] = 0.00;
  carac_sections[403][2] = 20.77;
  carac_sections[403][3] = 100.00;
  outputs[403][0] = 6.59;
  outputs[403][1] = 2.48;

  // section 404
  carac_sections[404][0] = 20928.10;
  carac_sections[404][1] = 0.00;
  carac_sections[404][2] = 20.74;
  carac_sections[404][3] = 100.00;
  outputs[404][0] = 6.61;
  outputs[404][1] = 2.47;

  // section 405
  carac_sections[405][0] = 20985.07;
  carac_sections[405][1] = 0.00;
  carac_sections[405][2] = 20.71;
  carac_sections[405][3] = 100.00;
  outputs[405][0] = 6.63;
  outputs[405][1] = 2.46;

  // section 406
  carac_sections[406][0] = 21040.41;
  carac_sections[406][1] = 0.00;
  carac_sections[406][2] = 20.68;
  carac_sections[406][3] = 100.00;
  outputs[406][0] = 6.65;
  outputs[406][1] = 2.46;

  // section 407
  carac_sections[407][0] = 21095.70;
  carac_sections[407][1] = 0.00;
  carac_sections[407][2] = 20.65;
  carac_sections[407][3] = 100.00;
  outputs[407][0] = 6.67;
  outputs[407][1] = 2.45;

  // section 408
  carac_sections[408][0] = 21146.58;
  carac_sections[408][1] = 0.00;
  carac_sections[408][2] = 20.63;
  carac_sections[408][3] = 100.00;
  outputs[408][0] = 6.68;
  outputs[408][1] = 2.45;

  // section 409
  carac_sections[409][0] = 21196.82;
  carac_sections[409][1] = 0.00;
  carac_sections[409][2] = 20.61;
  carac_sections[409][3] = 100.00;
  outputs[409][0] = 6.69;
  outputs[409][1] = 2.44;

  // section 410
  carac_sections[410][0] = 21245.41;
  carac_sections[410][1] = 0.00;
  carac_sections[410][2] = 20.59;
  carac_sections[410][3] = 100.00;
  outputs[410][0] = 6.70;
  outputs[410][1] = 2.44;

  // section 411
  carac_sections[411][0] = 21294.47;
  carac_sections[411][1] = 0.00;
  carac_sections[411][2] = 20.57;
  carac_sections[411][3] = 100.00;
  outputs[411][0] = 6.71;
  outputs[411][1] = 2.43;

  // section 412
  carac_sections[412][0] = 21339.19;
  carac_sections[412][1] = 0.00;
  carac_sections[412][2] = 20.56;
  carac_sections[412][3] = 100.00;
  outputs[412][0] = 6.71;
  outputs[412][1] = 2.43;

  // section 413
  carac_sections[413][0] = 21383.77;
  carac_sections[413][1] = 0.00;
  carac_sections[413][2] = 20.54;
  carac_sections[413][3] = 100.00;
  outputs[413][0] = 6.72;
  outputs[413][1] = 2.43;

  // section 414
  carac_sections[414][0] = 21426.75;
  carac_sections[414][1] = 0.00;
  carac_sections[414][2] = 20.53;
  carac_sections[414][3] = 100.00;
  outputs[414][0] = 6.72;
  outputs[414][1] = 2.43;

  // section 415
  carac_sections[415][0] = 21470.72;
  carac_sections[415][1] = 0.00;
  carac_sections[415][2] = 20.52;
  carac_sections[415][3] = 100.00;
  outputs[415][0] = 6.73;
  outputs[415][1] = 2.43;

  // section 416
  carac_sections[416][0] = 21510.12;
  carac_sections[416][1] = 0.00;
  carac_sections[416][2] = 20.50;
  carac_sections[416][3] = 100.00;
  outputs[416][0] = 6.73;
  outputs[416][1] = 2.43;

  // section 417
  carac_sections[417][0] = 21549.86;
  carac_sections[417][1] = 0.00;
  carac_sections[417][2] = 20.49;
  carac_sections[417][3] = 100.00;
  outputs[417][0] = 6.73;
  outputs[417][1] = 2.43;

  // section 418
  carac_sections[418][0] = 21587.97;
  carac_sections[418][1] = 0.00;
  carac_sections[418][2] = 20.49;
  carac_sections[418][3] = 100.00;
  outputs[418][0] = 6.73;
  outputs[418][1] = 2.43;

  // section 419
  carac_sections[419][0] = 21626.30;
  carac_sections[419][1] = 0.00;
  carac_sections[419][2] = 20.48;
  carac_sections[419][3] = 100.00;
  outputs[419][0] = 6.73;
  outputs[419][1] = 2.43;

  // section 420
  carac_sections[420][0] = 21697.79;
  carac_sections[420][1] = 0.00;
  carac_sections[420][2] = 20.46;
  carac_sections[420][3] = 100.00;
  outputs[420][0] = 6.73;
  outputs[420][1] = 2.43;

  // section 421
  carac_sections[421][0] = 21767.00;
  carac_sections[421][1] = 0.00;
  carac_sections[421][2] = 20.45;
  carac_sections[421][3] = 100.00;
  outputs[421][0] = 6.73;
  outputs[421][1] = 2.43;

  // section 422
  carac_sections[422][0] = 21831.62;
  carac_sections[422][1] = 0.00;
  carac_sections[422][2] = 20.44;
  carac_sections[422][3] = 100.00;
  outputs[422][0] = 6.72;
  outputs[422][1] = 2.43;

  // section 423
  carac_sections[423][0] = 21894.30;
  carac_sections[423][1] = 0.00;
  carac_sections[423][2] = 20.44;
  carac_sections[423][3] = 100.00;
  outputs[423][0] = 6.71;
  outputs[423][1] = 2.43;

  // section 424
  carac_sections[424][0] = 21952.83;
  carac_sections[424][1] = 0.00;
  carac_sections[424][2] = 20.43;
  carac_sections[424][3] = 100.00;
  outputs[424][0] = 6.70;
  outputs[424][1] = 2.44;

  // section 425
  carac_sections[425][0] = 22009.97;
  carac_sections[425][1] = 0.00;
  carac_sections[425][2] = 20.43;
  carac_sections[425][3] = 100.00;
  outputs[425][0] = 6.69;
  outputs[425][1] = 2.44;

  // section 426
  carac_sections[426][0] = 22063.64;
  carac_sections[426][1] = 0.00;
  carac_sections[426][2] = 20.43;
  carac_sections[426][3] = 100.00;
  outputs[426][0] = 6.68;
  outputs[426][1] = 2.44;

  // section 427
  carac_sections[427][0] = 22116.42;
  carac_sections[427][1] = 0.00;
  carac_sections[427][2] = 20.43;
  carac_sections[427][3] = 100.00;
  outputs[427][0] = 6.67;
  outputs[427][1] = 2.45;

  // section 428
  carac_sections[428][0] = 22166.14;
  carac_sections[428][1] = 0.00;
  carac_sections[428][2] = 20.43;
  carac_sections[428][3] = 100.00;
  outputs[428][0] = 6.66;
  outputs[428][1] = 2.45;

  // section 429
  carac_sections[429][0] = 22215.24;
  carac_sections[429][1] = 0.00;
  carac_sections[429][2] = 20.43;
  carac_sections[429][3] = 100.00;
  outputs[429][0] = 6.64;
  outputs[429][1] = 2.46;

  // section 430
  carac_sections[430][0] = 22261.83;
  carac_sections[430][1] = 0.00;
  carac_sections[430][2] = 20.43;
  carac_sections[430][3] = 100.00;
  outputs[430][0] = 6.63;
  outputs[430][1] = 2.46;

  // section 431
  carac_sections[431][0] = 22307.60;
  carac_sections[431][1] = 0.00;
  carac_sections[431][2] = 20.43;
  carac_sections[431][3] = 100.00;
  outputs[431][0] = 6.62;
  outputs[431][1] = 2.47;

  // section 432
  carac_sections[432][0] = 22352.28;
  carac_sections[432][1] = 0.00;
  carac_sections[432][2] = 20.44;
  carac_sections[432][3] = 100.00;
  outputs[432][0] = 6.60;
  outputs[432][1] = 2.47;

  // section 433
  carac_sections[433][0] = 22395.46;
  carac_sections[433][1] = 0.00;
  carac_sections[433][2] = 20.44;
  carac_sections[433][3] = 100.00;
  outputs[433][0] = 6.59;
  outputs[433][1] = 2.48;

  // section 434
  carac_sections[434][0] = 22438.23;
  carac_sections[434][1] = 0.00;
  carac_sections[434][2] = 20.44;
  carac_sections[434][3] = 100.00;
  outputs[434][0] = 6.57;
  outputs[434][1] = 2.49;

  // section 435
  carac_sections[435][0] = 22479.96;
  carac_sections[435][1] = 0.00;
  carac_sections[435][2] = 20.45;
  carac_sections[435][3] = 100.00;
  outputs[435][0] = 6.55;
  outputs[435][1] = 2.49;

  // section 436
  carac_sections[436][0] = 22521.10;
  carac_sections[436][1] = 0.00;
  carac_sections[436][2] = 20.46;
  carac_sections[436][3] = 100.00;
  outputs[436][0] = 6.54;
  outputs[436][1] = 2.50;

  // section 437
  carac_sections[437][0] = 22562.40;
  carac_sections[437][1] = 0.00;
  carac_sections[437][2] = 20.46;
  carac_sections[437][3] = 100.00;
  outputs[437][0] = 6.52;
  outputs[437][1] = 2.50;

  // section 438
  carac_sections[438][0] = 22602.94;
  carac_sections[438][1] = 0.00;
  carac_sections[438][2] = 20.47;
  carac_sections[438][3] = 100.00;
  outputs[438][0] = 6.50;
  outputs[438][1] = 2.51;

  // section 439
  carac_sections[439][0] = 22644.00;
  carac_sections[439][1] = 0.00;
  carac_sections[439][2] = 20.47;
  carac_sections[439][3] = 100.00;
  outputs[439][0] = 6.48;
  outputs[439][1] = 2.52;

  // section 440
  carac_sections[440][0] = 22685.45;
  carac_sections[440][1] = 0.00;
  carac_sections[440][2] = 20.48;
  carac_sections[440][3] = 100.00;
  outputs[440][0] = 6.46;
  outputs[440][1] = 2.53;

  // section 441
  carac_sections[441][0] = 22728.39;
  carac_sections[441][1] = 0.00;
  carac_sections[441][2] = 20.49;
  carac_sections[441][3] = 100.00;
  outputs[441][0] = 6.44;
  outputs[441][1] = 2.53;

  // section 442
  carac_sections[442][0] = 22771.50;
  carac_sections[442][1] = 0.00;
  carac_sections[442][2] = 20.50;
  carac_sections[442][3] = 100.00;
  outputs[442][0] = 6.42;
  outputs[442][1] = 2.54;

  // section 443
  carac_sections[443][0] = 22816.85;
  carac_sections[443][1] = 0.00;
  carac_sections[443][2] = 20.51;
  carac_sections[443][3] = 100.00;
  outputs[443][0] = 6.40;
  outputs[443][1] = 2.55;

  // section 444
  carac_sections[444][0] = 22863.52;
  carac_sections[444][1] = 0.00;
  carac_sections[444][2] = 20.52;
  carac_sections[444][3] = 100.00;
  outputs[444][0] = 6.37;
  outputs[444][1] = 2.56;

  // section 445
  carac_sections[445][0] = 22913.96;
  carac_sections[445][1] = 0.00;
  carac_sections[445][2] = 20.53;
  carac_sections[445][3] = 100.00;
  outputs[445][0] = 6.35;
  outputs[445][1] = 2.57;

  // section 446
  carac_sections[446][0] = 22966.72;
  carac_sections[446][1] = 0.00;
  carac_sections[446][2] = 20.54;
  carac_sections[446][3] = 100.00;
  outputs[446][0] = 6.32;
  outputs[446][1] = 2.58;

  // section 447
  carac_sections[447][0] = 23029.31;
  carac_sections[447][1] = 0.00;
  carac_sections[447][2] = 20.55;
  carac_sections[447][3] = 100.00;
  outputs[447][0] = 6.29;
  outputs[447][1] = 2.60;

  // section 448
  carac_sections[448][0] = 23057.25;
  carac_sections[448][1] = 0.00;
  carac_sections[448][2] = 20.56;
  carac_sections[448][3] = 100.00;
  outputs[448][0] = 6.27;
  outputs[448][1] = 2.60;

  // section 449
  carac_sections[449][0] = 23089.30;
  carac_sections[449][1] = 0.00;
  carac_sections[449][2] = 20.56;
  carac_sections[449][3] = 100.00;
  outputs[449][0] = 6.25;
  outputs[449][1] = 2.61;

  // section 450
  carac_sections[450][0] = 23126.31;
  carac_sections[450][1] = 0.00;
  carac_sections[450][2] = 20.57;
  carac_sections[450][3] = 100.00;
  outputs[450][0] = 6.23;
  outputs[450][1] = 2.62;

  // section 451
  carac_sections[451][0] = 23164.40;
  carac_sections[451][1] = 0.00;
  carac_sections[451][2] = 20.58;
  carac_sections[451][3] = 100.00;
  outputs[451][0] = 6.21;
  outputs[451][1] = 2.63;

  // section 452
  carac_sections[452][0] = 23205.40;
  carac_sections[452][1] = 0.00;
  carac_sections[452][2] = 20.59;
  carac_sections[452][3] = 100.00;
  outputs[452][0] = 6.19;
  outputs[452][1] = 2.64;

  // section 453
  carac_sections[453][0] = 23250.26;
  carac_sections[453][1] = 0.00;
  carac_sections[453][2] = 20.60;
  carac_sections[453][3] = 100.00;
  outputs[453][0] = 6.17;
  outputs[453][1] = 2.65;

  // section 454
  carac_sections[454][0] = 23300.69;
  carac_sections[454][1] = 0.00;
  carac_sections[454][2] = 20.61;
  carac_sections[454][3] = 100.00;
  outputs[454][0] = 6.14;
  outputs[454][1] = 2.66;

  // section 455
  carac_sections[455][0] = 23357.60;
  carac_sections[455][1] = 0.00;
  carac_sections[455][2] = 20.61;
  carac_sections[455][3] = 100.00;
  outputs[455][0] = 6.11;
  outputs[455][1] = 2.67;

  // section 456
  carac_sections[456][0] = 23422.46;
  carac_sections[456][1] = 0.00;
  carac_sections[456][2] = 20.62;
  carac_sections[456][3] = 100.00;
  outputs[456][0] = 6.08;
  outputs[456][1] = 2.68;

  // section 457
  carac_sections[457][0] = 23456.24;
  carac_sections[457][1] = 0.00;
  carac_sections[457][2] = 20.63;
  carac_sections[457][3] = 100.00;
  outputs[457][0] = 6.06;
  outputs[457][1] = 2.69;

  // section 458
  carac_sections[458][0] = 23492.69;
  carac_sections[458][1] = 0.00;
  carac_sections[458][2] = 20.63;
  carac_sections[458][3] = 100.00;
  outputs[458][0] = 6.05;
  outputs[458][1] = 2.70;

  // section 459
  carac_sections[459][0] = 23535.23;
  carac_sections[459][1] = 0.00;
  carac_sections[459][2] = 20.63;
  carac_sections[459][3] = 100.00;
  outputs[459][0] = 6.03;
  outputs[459][1] = 2.71;

  // section 460
  carac_sections[460][0] = 23579.68;
  carac_sections[460][1] = 0.00;
  carac_sections[460][2] = 20.64;
  carac_sections[460][3] = 100.00;
  outputs[460][0] = 6.01;
  outputs[460][1] = 2.72;

  // section 461
  carac_sections[461][0] = 23631.26;
  carac_sections[461][1] = 0.00;
  carac_sections[461][2] = 20.64;
  carac_sections[461][3] = 100.00;
  outputs[461][0] = 5.99;
  outputs[461][1] = 2.72;

  // section 462
  carac_sections[462][0] = 23687.04;
  carac_sections[462][1] = 0.00;
  carac_sections[462][2] = 20.63;
  carac_sections[462][3] = 100.00;
  outputs[462][0] = 5.97;
  outputs[462][1] = 2.73;

  // section 463
  carac_sections[463][0] = 23747.07;
  carac_sections[463][1] = 0.00;
  carac_sections[463][2] = 20.63;
  carac_sections[463][3] = 100.00;
  outputs[463][0] = 5.96;
  outputs[463][1] = 2.74;

  // section 464
  carac_sections[464][0] = 23813.54;
  carac_sections[464][1] = 0.00;
  carac_sections[464][2] = 20.62;
  carac_sections[464][3] = 100.00;
  outputs[464][0] = 5.94;
  outputs[464][1] = 2.75;

  // section 465
  carac_sections[465][0] = 23887.22;
  carac_sections[465][1] = 0.00;
  carac_sections[465][2] = 20.61;
  carac_sections[465][3] = 100.00;
  outputs[465][0] = 5.93;
  outputs[465][1] = 2.75;

  // section 466
  carac_sections[466][0] = 23929.36;
  carac_sections[466][1] = 0.00;
  carac_sections[466][2] = 20.60;
  carac_sections[466][3] = 100.00;
  outputs[466][0] = 5.93;
  outputs[466][1] = 2.75;

  // section 467
  carac_sections[467][0] = 23971.31;
  carac_sections[467][1] = 0.00;
  carac_sections[467][2] = 20.58;
  carac_sections[467][3] = 100.00;
  outputs[467][0] = 5.93;
  outputs[467][1] = 2.75;

  // section 468
  carac_sections[468][0] = 24018.46;
  carac_sections[468][1] = 0.00;
  carac_sections[468][2] = 20.57;
  carac_sections[468][3] = 100.00;
  outputs[468][0] = 5.93;
  outputs[468][1] = 2.75;

  // section 469
  carac_sections[469][0] = 24066.44;
  carac_sections[469][1] = 0.00;
  carac_sections[469][2] = 20.55;
  carac_sections[469][3] = 100.00;
  outputs[469][0] = 5.93;
  outputs[469][1] = 2.75;

  // section 470
  carac_sections[470][0] = 24118.85;
  carac_sections[470][1] = 0.00;
  carac_sections[470][2] = 20.53;
  carac_sections[470][3] = 100.00;
  outputs[470][0] = 5.94;
  outputs[470][1] = 2.75;

  // section 471
  carac_sections[471][0] = 24173.21;
  carac_sections[471][1] = 0.00;
  carac_sections[471][2] = 20.50;
  carac_sections[471][3] = 100.00;
  outputs[471][0] = 5.95;
  outputs[471][1] = 2.74;

  // section 472
  carac_sections[472][0] = 24232.18;
  carac_sections[472][1] = 0.00;
  carac_sections[472][2] = 20.47;
  carac_sections[472][3] = 100.00;
  outputs[472][0] = 5.97;
  outputs[472][1] = 2.74;

  // section 473
  carac_sections[473][0] = 24293.60;
  carac_sections[473][1] = 0.00;
  carac_sections[473][2] = 20.43;
  carac_sections[473][3] = 100.00;
  outputs[473][0] = 5.99;
  outputs[473][1] = 2.73;

  // section 474
  carac_sections[474][0] = 24359.30;
  carac_sections[474][1] = 0.00;
  carac_sections[474][2] = 20.38;
  carac_sections[474][3] = 100.00;
  outputs[474][0] = 6.01;
  outputs[474][1] = 2.72;

  // section 475
  carac_sections[475][0] = 24427.30;
  carac_sections[475][1] = 0.00;
  carac_sections[475][2] = 20.33;
  carac_sections[475][3] = 100.00;
  outputs[475][0] = 6.05;
  outputs[475][1] = 2.70;

  // section 476
  carac_sections[476][0] = 24494.54;
  carac_sections[476][1] = 0.00;
  carac_sections[476][2] = 20.28;
  carac_sections[476][3] = 100.00;
  outputs[476][0] = 6.08;
  outputs[476][1] = 2.68;

  // section 477
  carac_sections[477][0] = 24562.93;
  carac_sections[477][1] = 0.00;
  carac_sections[477][2] = 20.23;
  carac_sections[477][3] = 100.00;
  outputs[477][0] = 6.12;
  outputs[477][1] = 2.67;

  // section 478
  carac_sections[478][0] = 24633.41;
  carac_sections[478][1] = 0.00;
  carac_sections[478][2] = 20.16;
  carac_sections[478][3] = 100.00;
  outputs[478][0] = 6.17;
  outputs[478][1] = 2.65;

  // section 479
  carac_sections[479][0] = 24705.34;
  carac_sections[479][1] = 0.00;
  carac_sections[479][2] = 20.10;
  carac_sections[479][3] = 100.00;
  outputs[479][0] = 6.22;
  outputs[479][1] = 2.62;

  // section 480
  carac_sections[480][0] = 24778.65;
  carac_sections[480][1] = 0.00;
  carac_sections[480][2] = 20.03;
  carac_sections[480][3] = 100.00;
  outputs[480][0] = 6.28;
  outputs[480][1] = 2.60;

  // section 481
  carac_sections[481][0] = 24852.97;
  carac_sections[481][1] = 0.00;
  carac_sections[481][2] = 19.96;
  carac_sections[481][3] = 100.00;
  outputs[481][0] = 6.34;
  outputs[481][1] = 2.58;

  // section 482
  carac_sections[482][0] = 24890.74;
  carac_sections[482][1] = 0.00;
  carac_sections[482][2] = 19.92;
  carac_sections[482][3] = 100.00;
  outputs[482][0] = 6.36;
  outputs[482][1] = 2.57;

  // section 483
  carac_sections[483][0] = 24928.41;
  carac_sections[483][1] = 0.00;
  carac_sections[483][2] = 19.89;
  carac_sections[483][3] = 100.00;
  outputs[483][0] = 6.39;
  outputs[483][1] = 2.55;

  // section 484
  carac_sections[484][0] = 24966.62;
  carac_sections[484][1] = 0.00;
  carac_sections[484][2] = 19.85;
  carac_sections[484][3] = 100.00;
  outputs[484][0] = 6.43;
  outputs[484][1] = 2.54;

  // section 485
  carac_sections[485][0] = 25004.92;
  carac_sections[485][1] = 0.00;
  carac_sections[485][2] = 19.81;
  carac_sections[485][3] = 100.00;
  outputs[485][0] = 6.46;
  outputs[485][1] = 2.53;

  // section 486
  carac_sections[486][0] = 25079.22;
  carac_sections[486][1] = 0.00;
  carac_sections[486][2] = 19.74;
  carac_sections[486][3] = 100.00;
  outputs[486][0] = 6.52;
  outputs[486][1] = 2.50;

  // section 487
  carac_sections[487][0] = 25153.30;
  carac_sections[487][1] = 0.00;
  carac_sections[487][2] = 19.66;
  carac_sections[487][3] = 100.00;
  outputs[487][0] = 6.58;
  outputs[487][1] = 2.48;

  // section 488
  carac_sections[488][0] = 25227.16;
  carac_sections[488][1] = 0.00;
  carac_sections[488][2] = 19.59;
  carac_sections[488][3] = 100.00;
  outputs[488][0] = 6.65;
  outputs[488][1] = 2.46;

  // section 489
  carac_sections[489][0] = 25300.79;
  carac_sections[489][1] = 0.00;
  carac_sections[489][2] = 19.52;
  carac_sections[489][3] = 100.00;
  outputs[489][0] = 6.71;
  outputs[489][1] = 2.43;

  // section 490
  carac_sections[490][0] = 25373.37;
  carac_sections[490][1] = 0.00;
  carac_sections[490][2] = 19.44;
  carac_sections[490][3] = 100.00;
  outputs[490][0] = 6.77;
  outputs[490][1] = 2.41;

  // section 491
  carac_sections[491][0] = 25445.39;
  carac_sections[491][1] = 0.00;
  carac_sections[491][2] = 19.38;
  carac_sections[491][3] = 100.00;
  outputs[491][0] = 6.83;
  outputs[491][1] = 2.39;

  // section 492
  carac_sections[492][0] = 25516.76;
  carac_sections[492][1] = 0.00;
  carac_sections[492][2] = 19.31;
  carac_sections[492][3] = 100.00;
  outputs[492][0] = 6.88;
  outputs[492][1] = 2.37;

  // section 493
  carac_sections[493][0] = 25587.68;
  carac_sections[493][1] = 0.00;
  carac_sections[493][2] = 19.25;
  carac_sections[493][3] = 100.00;
  outputs[493][0] = 6.94;
  outputs[493][1] = 2.35;

  // section 494
  carac_sections[494][0] = 25652.19;
  carac_sections[494][1] = 0.00;
  carac_sections[494][2] = 19.19;
  carac_sections[494][3] = 100.00;
  outputs[494][0] = 6.99;
  outputs[494][1] = 2.34;

  // section 495
  carac_sections[495][0] = 25714.78;
  carac_sections[495][1] = 0.00;
  carac_sections[495][2] = 19.13;
  carac_sections[495][3] = 100.00;
  outputs[495][0] = 7.03;
  outputs[495][1] = 2.32;

  // section 496
  carac_sections[496][0] = 25776.09;
  carac_sections[496][1] = 0.00;
  carac_sections[496][2] = 19.08;
  carac_sections[496][3] = 100.00;
  outputs[496][0] = 7.07;
  outputs[496][1] = 2.31;

  // section 497
  carac_sections[497][0] = 25836.94;
  carac_sections[497][1] = 0.00;
  carac_sections[497][2] = 19.04;
  carac_sections[497][3] = 100.00;
  outputs[497][0] = 7.11;
  outputs[497][1] = 2.29;

  // section 498
  carac_sections[498][0] = 25895.64;
  carac_sections[498][1] = 0.00;
  carac_sections[498][2] = 18.99;
  carac_sections[498][3] = 100.00;
  outputs[498][0] = 7.15;
  outputs[498][1] = 2.28;

  // section 499
  carac_sections[499][0] = 25953.65;
  carac_sections[499][1] = 0.00;
  carac_sections[499][2] = 18.95;
  carac_sections[499][3] = 100.00;
  outputs[499][0] = 7.19;
  outputs[499][1] = 2.27;

  // section 500
  carac_sections[500][0] = 26010.61;
  carac_sections[500][1] = 0.00;
  carac_sections[500][2] = 18.91;
  carac_sections[500][3] = 100.00;
  outputs[500][0] = 7.22;
  outputs[500][1] = 2.26;

  // section 501
  carac_sections[501][0] = 26067.53;
  carac_sections[501][1] = 0.00;
  carac_sections[501][2] = 18.87;
  carac_sections[501][3] = 100.00;
  outputs[501][0] = 7.25;
  outputs[501][1] = 2.25;

  // section 502
  carac_sections[502][0] = 26120.39;
  carac_sections[502][1] = 0.00;
  carac_sections[502][2] = 18.83;
  carac_sections[502][3] = 100.00;
  outputs[502][0] = 7.28;
  outputs[502][1] = 2.24;

  // section 503
  carac_sections[503][0] = 26172.47;
  carac_sections[503][1] = 0.00;
  carac_sections[503][2] = 18.80;
  carac_sections[503][3] = 100.00;
  outputs[503][0] = 7.31;
  outputs[503][1] = 2.23;

  // section 504
  carac_sections[504][0] = 26223.37;
  carac_sections[504][1] = 0.00;
  carac_sections[504][2] = 18.77;
  carac_sections[504][3] = 100.00;
  outputs[504][0] = 7.33;
  outputs[504][1] = 2.23;

  // section 505
  carac_sections[505][0] = 26274.19;
  carac_sections[505][1] = 0.00;
  carac_sections[505][2] = 18.74;
  carac_sections[505][3] = 100.00;
  outputs[505][0] = 7.35;
  outputs[505][1] = 2.22;

  // section 506
  carac_sections[506][0] = 26322.95;
  carac_sections[506][1] = 0.00;
  carac_sections[506][2] = 18.71;
  carac_sections[506][3] = 100.00;
  outputs[506][0] = 7.37;
  outputs[506][1] = 2.22;

  // section 507
  carac_sections[507][0] = 26371.46;
  carac_sections[507][1] = 0.00;
  carac_sections[507][2] = 18.69;
  carac_sections[507][3] = 100.00;
  outputs[507][0] = 7.39;
  outputs[507][1] = 2.21;

  // section 508
  carac_sections[508][0] = 26418.93;
  carac_sections[508][1] = 0.00;
  carac_sections[508][2] = 18.67;
  carac_sections[508][3] = 100.00;
  outputs[508][0] = 7.41;
  outputs[508][1] = 2.20;

  // section 509
  carac_sections[509][0] = 26467.87;
  carac_sections[509][1] = 0.00;
  carac_sections[509][2] = 18.64;
  carac_sections[509][3] = 100.00;
  outputs[509][0] = 7.42;
  outputs[509][1] = 2.20;

  // section 510
  carac_sections[510][0] = 26508.92;
  carac_sections[510][1] = 0.00;
  carac_sections[510][2] = 18.63;
  carac_sections[510][3] = 100.00;
  outputs[510][0] = 7.43;
  outputs[510][1] = 2.20;

  // section 511
  carac_sections[511][0] = 26549.93;
  carac_sections[511][1] = 0.00;
  carac_sections[511][2] = 18.61;
  carac_sections[511][3] = 100.00;
  outputs[511][0] = 7.44;
  outputs[511][1] = 2.19;

  // section 512
  carac_sections[512][0] = 26589.70;
  carac_sections[512][1] = 0.00;
  carac_sections[512][2] = 18.59;
  carac_sections[512][3] = 100.00;
  outputs[512][0] = 7.45;
  outputs[512][1] = 2.19;

  // section 513
  carac_sections[513][0] = 26630.18;
  carac_sections[513][1] = 0.00;
  carac_sections[513][2] = 18.58;
  carac_sections[513][3] = 100.00;
  outputs[513][0] = 7.46;
  outputs[513][1] = 2.19;

  // section 514
  carac_sections[514][0] = 26668.07;
  carac_sections[514][1] = 0.00;
  carac_sections[514][2] = 18.57;
  carac_sections[514][3] = 100.00;
  outputs[514][0] = 7.47;
  outputs[514][1] = 2.19;

  // section 515
  carac_sections[515][0] = 26706.11;
  carac_sections[515][1] = 0.00;
  carac_sections[515][2] = 18.55;
  carac_sections[515][3] = 100.00;
  outputs[515][0] = 7.48;
  outputs[515][1] = 2.18;

  // section 516
  carac_sections[516][0] = 26780.58;
  carac_sections[516][1] = 0.00;
  carac_sections[516][2] = 18.53;
  carac_sections[516][3] = 100.00;
  outputs[516][0] = 7.49;
  outputs[516][1] = 2.18;

  // section 517
  carac_sections[517][0] = 26850.42;
  carac_sections[517][1] = 0.00;
  carac_sections[517][2] = 18.52;
  carac_sections[517][3] = 100.00;
  outputs[517][0] = 7.49;
  outputs[517][1] = 2.18;

  // section 518
  carac_sections[518][0] = 26918.15;
  carac_sections[518][1] = 0.00;
  carac_sections[518][2] = 18.50;
  carac_sections[518][3] = 100.00;
  outputs[518][0] = 7.50;
  outputs[518][1] = 2.18;

  // section 519
  carac_sections[519][0] = 26983.54;
  carac_sections[519][1] = 0.00;
  carac_sections[519][2] = 18.49;
  carac_sections[519][3] = 100.00;
  outputs[519][0] = 7.50;
  outputs[519][1] = 2.18;

  // section 520
  carac_sections[520][0] = 27048.16;
  carac_sections[520][1] = 0.00;
  carac_sections[520][2] = 18.48;
  carac_sections[520][3] = 100.00;
  outputs[520][0] = 7.50;
  outputs[520][1] = 2.18;

  // section 521
  carac_sections[521][0] = 27104.79;
  carac_sections[521][1] = 0.00;
  carac_sections[521][2] = 18.48;
  carac_sections[521][3] = 100.00;
  outputs[521][0] = 7.49;
  outputs[521][1] = 2.18;

  // section 522
  carac_sections[522][0] = 27159.67;
  carac_sections[522][1] = 0.00;
  carac_sections[522][2] = 18.48;
  carac_sections[522][3] = 100.00;
  outputs[522][0] = 7.49;
  outputs[522][1] = 2.18;

  // section 523
  carac_sections[523][0] = 27212.48;
  carac_sections[523][1] = 0.00;
  carac_sections[523][2] = 18.47;
  carac_sections[523][3] = 100.00;
  outputs[523][0] = 7.48;
  outputs[523][1] = 2.18;

  // section 524
  carac_sections[524][0] = 27265.30;
  carac_sections[524][1] = 0.00;
  carac_sections[524][2] = 18.47;
  carac_sections[524][3] = 100.00;
  outputs[524][0] = 7.47;
  outputs[524][1] = 2.19;

  // section 525
  carac_sections[525][0] = 27313.69;
  carac_sections[525][1] = 0.00;
  carac_sections[525][2] = 18.48;
  carac_sections[525][3] = 100.00;
  outputs[525][0] = 7.46;
  outputs[525][1] = 2.19;

  // section 526
  carac_sections[526][0] = 27361.61;
  carac_sections[526][1] = 0.00;
  carac_sections[526][2] = 18.48;
  carac_sections[526][3] = 100.00;
  outputs[526][0] = 7.45;
  outputs[526][1] = 2.19;

  // section 527
  carac_sections[527][0] = 27407.81;
  carac_sections[527][1] = 0.00;
  carac_sections[527][2] = 18.48;
  carac_sections[527][3] = 100.00;
  outputs[527][0] = 7.44;
  outputs[527][1] = 2.19;

  // section 528
  carac_sections[528][0] = 27455.51;
  carac_sections[528][1] = 0.00;
  carac_sections[528][2] = 18.49;
  carac_sections[528][3] = 100.00;
  outputs[528][0] = 7.43;
  outputs[528][1] = 2.20;

  // section 529
  carac_sections[529][0] = 27495.24;
  carac_sections[529][1] = 0.00;
  carac_sections[529][2] = 18.49;
  carac_sections[529][3] = 100.00;
  outputs[529][0] = 7.41;
  outputs[529][1] = 2.20;

  // section 530
  carac_sections[530][0] = 27535.25;
  carac_sections[530][1] = 0.00;
  carac_sections[530][2] = 18.50;
  carac_sections[530][3] = 100.00;
  outputs[530][0] = 7.40;
  outputs[530][1] = 2.21;

  // section 531
  carac_sections[531][0] = 27573.48;
  carac_sections[531][1] = 0.00;
  carac_sections[531][2] = 18.50;
  carac_sections[531][3] = 100.00;
  outputs[531][0] = 7.39;
  outputs[531][1] = 2.21;

  // section 532
  carac_sections[532][0] = 27611.99;
  carac_sections[532][1] = 0.00;
  carac_sections[532][2] = 18.51;
  carac_sections[532][3] = 100.00;
  outputs[532][0] = 7.37;
  outputs[532][1] = 2.21;

  // section 533
  carac_sections[533][0] = 27683.57;
  carac_sections[533][1] = 0.00;
  carac_sections[533][2] = 18.53;
  carac_sections[533][3] = 100.00;
  outputs[533][0] = 7.35;
  outputs[533][1] = 2.22;

  // section 534
  carac_sections[534][0] = 27753.06;
  carac_sections[534][1] = 0.00;
  carac_sections[534][2] = 18.54;
  carac_sections[534][3] = 100.00;
  outputs[534][0] = 7.32;
  outputs[534][1] = 2.23;

  // section 535
  carac_sections[535][0] = 27814.99;
  carac_sections[535][1] = 0.00;
  carac_sections[535][2] = 18.56;
  carac_sections[535][3] = 100.00;
  outputs[535][0] = 7.29;
  outputs[535][1] = 2.24;

  // section 536
  carac_sections[536][0] = 27874.71;
  carac_sections[536][1] = 0.00;
  carac_sections[536][2] = 18.58;
  carac_sections[536][3] = 100.00;
  outputs[536][0] = 7.26;
  outputs[536][1] = 2.25;

  // section 537
  carac_sections[537][0] = 27930.29;
  carac_sections[537][1] = 0.00;
  carac_sections[537][2] = 18.59;
  carac_sections[537][3] = 100.00;
  outputs[537][0] = 7.23;
  outputs[537][1] = 2.26;

  // section 538
  carac_sections[538][0] = 27985.50;
  carac_sections[538][1] = 0.00;
  carac_sections[538][2] = 18.61;
  carac_sections[538][3] = 100.00;
  outputs[538][0] = 7.20;
  outputs[538][1] = 2.27;

  // section 539
  carac_sections[539][0] = 28033.69;
  carac_sections[539][1] = 0.00;
  carac_sections[539][2] = 18.63;
  carac_sections[539][3] = 100.00;
  outputs[539][0] = 7.17;
  outputs[539][1] = 2.28;

  // section 540
  carac_sections[540][0] = 28081.64;
  carac_sections[540][1] = 0.00;
  carac_sections[540][2] = 18.65;
  carac_sections[540][3] = 100.00;
  outputs[540][0] = 7.14;
  outputs[540][1] = 2.29;

  // section 541
  carac_sections[541][0] = 28125.16;
  carac_sections[541][1] = 0.00;
  carac_sections[541][2] = 18.67;
  carac_sections[541][3] = 100.00;
  outputs[541][0] = 7.11;
  outputs[541][1] = 2.30;

  // section 542
  carac_sections[542][0] = 28169.70;
  carac_sections[542][1] = 0.00;
  carac_sections[542][2] = 18.69;
  carac_sections[542][3] = 100.00;
  outputs[542][0] = 7.08;
  outputs[542][1] = 2.31;

  // section 543
  carac_sections[543][0] = 28208.48;
  carac_sections[543][1] = 0.00;
  carac_sections[543][2] = 18.70;
  carac_sections[543][3] = 100.00;
  outputs[543][0] = 7.05;
  outputs[543][1] = 2.31;

  // section 544
  carac_sections[544][0] = 28247.59;
  carac_sections[544][1] = 0.00;
  carac_sections[544][2] = 18.72;
  carac_sections[544][3] = 100.00;
  outputs[544][0] = 7.02;
  outputs[544][1] = 2.32;

  // section 545
  carac_sections[545][0] = 28319.18;
  carac_sections[545][1] = 0.00;
  carac_sections[545][2] = 18.76;
  carac_sections[545][3] = 100.00;
  outputs[545][0] = 6.98;
  outputs[545][1] = 2.34;

  // section 546
  carac_sections[546][0] = 28385.51;
  carac_sections[546][1] = 0.00;
  carac_sections[546][2] = 18.79;
  carac_sections[546][3] = 100.00;
  outputs[546][0] = 6.93;
  outputs[546][1] = 2.36;

  // section 547
  carac_sections[547][0] = 28446.70;
  carac_sections[547][1] = 0.00;
  carac_sections[547][2] = 18.82;
  carac_sections[547][3] = 100.00;
  outputs[547][0] = 6.88;
  outputs[547][1] = 2.37;

  // section 548
  carac_sections[548][0] = 28503.28;
  carac_sections[548][1] = 0.00;
  carac_sections[548][2] = 18.85;
  carac_sections[548][3] = 100.00;
  outputs[548][0] = 6.83;
  outputs[548][1] = 2.39;

  // section 549
  carac_sections[549][0] = 28556.71;
  carac_sections[549][1] = 0.00;
  carac_sections[549][2] = 18.88;
  carac_sections[549][3] = 100.00;
  outputs[549][0] = 6.79;
  outputs[549][1] = 2.41;

  // section 550
  carac_sections[550][0] = 28606.44;
  carac_sections[550][1] = 0.00;
  carac_sections[550][2] = 18.91;
  carac_sections[550][3] = 100.00;
  outputs[550][0] = 6.75;
  outputs[550][1] = 2.42;

  // section 551
  carac_sections[551][0] = 28654.58;
  carac_sections[551][1] = 0.00;
  carac_sections[551][2] = 18.93;
  carac_sections[551][3] = 100.00;
  outputs[551][0] = 6.71;
  outputs[551][1] = 2.44;

  // section 552
  carac_sections[552][0] = 28699.72;
  carac_sections[552][1] = 0.00;
  carac_sections[552][2] = 18.96;
  carac_sections[552][3] = 100.00;
  outputs[552][0] = 6.67;
  outputs[552][1] = 2.45;

  // section 553
  carac_sections[553][0] = 28744.40;
  carac_sections[553][1] = 0.00;
  carac_sections[553][2] = 18.99;
  carac_sections[553][3] = 100.00;
  outputs[553][0] = 6.62;
  outputs[553][1] = 2.46;

  // section 554
  carac_sections[554][0] = 28787.02;
  carac_sections[554][1] = 0.00;
  carac_sections[554][2] = 19.01;
  carac_sections[554][3] = 100.00;
  outputs[554][0] = 6.59;
  outputs[554][1] = 2.48;

  // section 555
  carac_sections[555][0] = 28830.36;
  carac_sections[555][1] = 0.00;
  carac_sections[555][2] = 19.04;
  carac_sections[555][3] = 100.00;
  outputs[555][0] = 6.55;
  outputs[555][1] = 2.49;

  // section 556
  carac_sections[556][0] = 28872.63;
  carac_sections[556][1] = 0.00;
  carac_sections[556][2] = 19.06;
  carac_sections[556][3] = 100.00;
  outputs[556][0] = 6.51;
  outputs[556][1] = 2.51;

  // section 557
  carac_sections[557][0] = 28917.00;
  carac_sections[557][1] = 0.00;
  carac_sections[557][2] = 19.09;
  carac_sections[557][3] = 100.00;
  outputs[557][0] = 6.46;
  outputs[557][1] = 2.53;

  // section 558
  carac_sections[558][0] = 28960.89;
  carac_sections[558][1] = 0.00;
  carac_sections[558][2] = 19.12;
  carac_sections[558][3] = 100.00;
  outputs[558][0] = 6.42;
  outputs[558][1] = 2.54;

  // section 559
  carac_sections[559][0] = 29014.73;
  carac_sections[559][1] = 0.00;
  carac_sections[559][2] = 19.15;
  carac_sections[559][3] = 100.00;
  outputs[559][0] = 6.37;
  outputs[559][1] = 2.56;

  // section 560
  carac_sections[560][0] = 29063.06;
  carac_sections[560][1] = 0.00;
  carac_sections[560][2] = 19.18;
  carac_sections[560][3] = 100.00;
  outputs[560][0] = 6.32;
  outputs[560][1] = 2.58;

  // section 561
  carac_sections[561][0] = 29090.34;
  carac_sections[561][1] = 0.00;
  carac_sections[561][2] = 19.20;
  carac_sections[561][3] = 100.00;
  outputs[561][0] = 6.29;
  outputs[561][1] = 2.60;

  // section 562
  carac_sections[562][0] = 29120.56;
  carac_sections[562][1] = 0.00;
  carac_sections[562][2] = 19.22;
  carac_sections[562][3] = 100.00;
  outputs[562][0] = 6.26;
  outputs[562][1] = 2.61;

  // section 563
  carac_sections[563][0] = 29152.21;
  carac_sections[563][1] = 0.00;
  carac_sections[563][2] = 19.24;
  carac_sections[563][3] = 100.00;
  outputs[563][0] = 6.23;
  outputs[563][1] = 2.62;

  // section 564
  carac_sections[564][0] = 29186.97;
  carac_sections[564][1] = 0.00;
  carac_sections[564][2] = 19.26;
  carac_sections[564][3] = 100.00;
  outputs[564][0] = 6.19;
  outputs[564][1] = 2.64;

  // section 565
  carac_sections[565][0] = 29226.47;
  carac_sections[565][1] = 0.00;
  carac_sections[565][2] = 19.29;
  carac_sections[565][3] = 100.00;
  outputs[565][0] = 6.15;
  outputs[565][1] = 2.65;

  // section 566
  carac_sections[566][0] = 29271.74;
  carac_sections[566][1] = 0.00;
  carac_sections[566][2] = 19.32;
  carac_sections[566][3] = 100.00;
  outputs[566][0] = 6.10;
  outputs[566][1] = 2.68;

  // section 567
  carac_sections[567][0] = 29327.95;
  carac_sections[567][1] = 0.00;
  carac_sections[567][2] = 19.35;
  carac_sections[567][3] = 100.00;
  outputs[567][0] = 6.05;
  outputs[567][1] = 2.70;

  // section 568
  carac_sections[568][0] = 29383.29;
  carac_sections[568][1] = 0.00;
  carac_sections[568][2] = 19.38;
  carac_sections[568][3] = 100.00;
  outputs[568][0] = 5.99;
  outputs[568][1] = 2.73;

  // section 569
  carac_sections[569][0] = 29420.76;
  carac_sections[569][1] = 0.00;
  carac_sections[569][2] = 19.41;
  carac_sections[569][3] = 100.00;
  outputs[569][0] = 5.95;
  outputs[569][1] = 2.75;

  // section 570
  carac_sections[570][0] = 29460.40;
  carac_sections[570][1] = 0.00;
  carac_sections[570][2] = 19.43;
  carac_sections[570][3] = 100.00;
  outputs[570][0] = 5.91;
  outputs[570][1] = 2.76;

  // section 571
  carac_sections[571][0] = 29508.87;
  carac_sections[571][1] = 0.00;
  carac_sections[571][2] = 19.46;
  carac_sections[571][3] = 100.00;
  outputs[571][0] = 5.85;
  outputs[571][1] = 2.79;

  // section 572
  carac_sections[572][0] = 29564.34;
  carac_sections[572][1] = 0.00;
  carac_sections[572][2] = 19.49;
  carac_sections[572][3] = 100.00;
  outputs[572][0] = 5.80;
  outputs[572][1] = 2.82;

  // section 573
  carac_sections[573][0] = 29628.73;
  carac_sections[573][1] = 0.00;
  carac_sections[573][2] = 19.52;
  carac_sections[573][3] = 100.00;
  outputs[573][0] = 5.73;
  outputs[573][1] = 2.85;

  // section 574
  carac_sections[574][0] = 29663.96;
  carac_sections[574][1] = 0.00;
  carac_sections[574][2] = 19.54;
  carac_sections[574][3] = 100.00;
  outputs[574][0] = 5.69;
  outputs[574][1] = 2.87;

  // section 575
  carac_sections[575][0] = 29701.32;
  carac_sections[575][1] = 0.00;
  carac_sections[575][2] = 19.56;
  carac_sections[575][3] = 100.00;
  outputs[575][0] = 5.66;
  outputs[575][1] = 2.89;

  // section 576
  carac_sections[576][0] = 29747.98;
  carac_sections[576][1] = 0.00;
  carac_sections[576][2] = 19.58;
  carac_sections[576][3] = 100.00;
  outputs[576][0] = 5.61;
  outputs[576][1] = 2.91;

  // section 577
  carac_sections[577][0] = 29797.27;
  carac_sections[577][1] = 0.00;
  carac_sections[577][2] = 19.60;
  carac_sections[577][3] = 100.00;
  outputs[577][0] = 5.56;
  outputs[577][1] = 2.94;

  // section 578
  carac_sections[578][0] = 29854.98;
  carac_sections[578][1] = 0.00;
  carac_sections[578][2] = 19.62;
  carac_sections[578][3] = 100.00;
  outputs[578][0] = 5.51;
  outputs[578][1] = 2.96;

  // section 579
  carac_sections[579][0] = 29919.58;
  carac_sections[579][1] = 0.00;
  carac_sections[579][2] = 19.65;
  carac_sections[579][3] = 100.00;
  outputs[579][0] = 5.45;
  outputs[579][1] = 3.00;

  // section 580
  carac_sections[580][0] = 29993.13;
  carac_sections[580][1] = 0.00;
  carac_sections[580][2] = 19.67;
  carac_sections[580][3] = 100.00;
  outputs[580][0] = 5.39;
  outputs[580][1] = 3.03;

  // section 581
  carac_sections[581][0] = 30036.14;
  carac_sections[581][1] = 0.00;
  carac_sections[581][2] = 19.68;
  carac_sections[581][3] = 100.00;
  outputs[581][0] = 5.35;
  outputs[581][1] = 3.05;

  // section 582
  carac_sections[582][0] = 30079.10;
  carac_sections[582][1] = 0.00;
  carac_sections[582][2] = 19.68;
  carac_sections[582][3] = 100.00;
  outputs[582][0] = 5.32;
  outputs[582][1] = 3.07;

  // section 583
  carac_sections[583][0] = 30129.42;
  carac_sections[583][1] = 0.00;
  carac_sections[583][2] = 19.69;
  carac_sections[583][3] = 100.00;
  outputs[583][0] = 5.29;
  outputs[583][1] = 3.09;

  // section 584
  carac_sections[584][0] = 30180.97;
  carac_sections[584][1] = 0.00;
  carac_sections[584][2] = 19.69;
  carac_sections[584][3] = 100.00;
  outputs[584][0] = 5.25;
  outputs[584][1] = 3.11;

  // section 585
  carac_sections[585][0] = 30237.76;
  carac_sections[585][1] = 0.00;
  carac_sections[585][2] = 19.69;
  carac_sections[585][3] = 100.00;
  outputs[585][0] = 5.22;
  outputs[585][1] = 3.13;

  // section 586
  carac_sections[586][0] = 30297.56;
  carac_sections[586][1] = 0.00;
  carac_sections[586][2] = 19.69;
  carac_sections[586][3] = 100.00;
  outputs[586][0] = 5.19;
  outputs[586][1] = 3.15;

  // section 587
  carac_sections[587][0] = 30363.63;
  carac_sections[587][1] = 0.00;
  carac_sections[587][2] = 19.68;
  carac_sections[587][3] = 100.00;
  outputs[587][0] = 5.16;
  outputs[587][1] = 3.16;

  // section 588
  carac_sections[588][0] = 30433.04;
  carac_sections[588][1] = 0.00;
  carac_sections[588][2] = 19.66;
  carac_sections[588][3] = 100.00;
  outputs[588][0] = 5.14;
  outputs[588][1] = 3.18;

  // section 589
  carac_sections[589][0] = 30506.73;
  carac_sections[589][1] = 0.00;
  carac_sections[589][2] = 19.64;
  carac_sections[589][3] = 100.00;
  outputs[589][0] = 5.12;
  outputs[589][1] = 3.19;

  // section 590
  carac_sections[590][0] = 30545.11;
  carac_sections[590][1] = 0.00;
  carac_sections[590][2] = 19.62;
  carac_sections[590][3] = 100.00;
  outputs[590][0] = 5.11;
  outputs[590][1] = 3.19;

  // section 591
  carac_sections[591][0] = 30583.27;
  carac_sections[591][1] = 0.00;
  carac_sections[591][2] = 19.61;
  carac_sections[591][3] = 100.00;
  outputs[591][0] = 5.11;
  outputs[591][1] = 3.20;

  // section 592
  carac_sections[592][0] = 30625.21;
  carac_sections[592][1] = 0.00;
  carac_sections[592][2] = 19.59;
  carac_sections[592][3] = 100.00;
  outputs[592][0] = 5.10;
  outputs[592][1] = 3.20;

  // section 593
  carac_sections[593][0] = 30666.22;
  carac_sections[593][1] = 0.00;
  carac_sections[593][2] = 19.56;
  carac_sections[593][3] = 100.00;
  outputs[593][0] = 5.10;
  outputs[593][1] = 3.20;

  // section 594
  carac_sections[594][0] = 30708.55;
  carac_sections[594][1] = 0.00;
  carac_sections[594][2] = 19.54;
  carac_sections[594][3] = 100.00;
  outputs[594][0] = 5.11;
  outputs[594][1] = 3.20;

  // section 595
  carac_sections[595][0] = 30750.76;
  carac_sections[595][1] = 0.00;
  carac_sections[595][2] = 19.52;
  carac_sections[595][3] = 100.00;
  outputs[595][0] = 5.11;
  outputs[595][1] = 3.20;

  // section 596
  carac_sections[596][0] = 30794.76;
  carac_sections[596][1] = 0.00;
  carac_sections[596][2] = 19.49;
  carac_sections[596][3] = 100.00;
  outputs[596][0] = 5.12;
  outputs[596][1] = 3.19;

  // section 597
  carac_sections[597][0] = 30838.46;
  carac_sections[597][1] = 0.00;
  carac_sections[597][2] = 19.46;
  carac_sections[597][3] = 100.00;
  outputs[597][0] = 5.12;
  outputs[597][1] = 3.19;

  // section 598
  carac_sections[598][0] = 30882.74;
  carac_sections[598][1] = 0.00;
  carac_sections[598][2] = 19.43;
  carac_sections[598][3] = 100.00;
  outputs[598][0] = 5.14;
  outputs[598][1] = 3.18;

  // section 599
  carac_sections[599][0] = 30928.19;
  carac_sections[599][1] = 0.00;
  carac_sections[599][2] = 19.39;
  carac_sections[599][3] = 100.00;
  outputs[599][0] = 5.15;
  outputs[599][1] = 3.17;

  // section 600
  carac_sections[600][0] = 30968.91;
  carac_sections[600][1] = 0.00;
  carac_sections[600][2] = 19.36;
  carac_sections[600][3] = 100.00;
  outputs[600][0] = 5.16;
  outputs[600][1] = 3.16;

  // section 601
  carac_sections[601][0] = 31009.58;
  carac_sections[601][1] = 0.00;
  carac_sections[601][2] = 19.33;
  carac_sections[601][3] = 100.00;
  outputs[601][0] = 5.18;
  outputs[601][1] = 3.15;

  // section 602
  carac_sections[602][0] = 31049.84;
  carac_sections[602][1] = 0.00;
  carac_sections[602][2] = 19.30;
  carac_sections[602][3] = 100.00;
  outputs[602][0] = 5.19;
  outputs[602][1] = 3.14;

  // section 603
  carac_sections[603][0] = 31090.58;
  carac_sections[603][1] = 0.00;
  carac_sections[603][2] = 19.26;
  carac_sections[603][3] = 100.00;
  outputs[603][0] = 5.21;
  outputs[603][1] = 3.13;

  // section 604
  carac_sections[604][0] = 31129.68;
  carac_sections[604][1] = 0.00;
  carac_sections[604][2] = 19.23;
  carac_sections[604][3] = 100.00;
  outputs[604][0] = 5.23;
  outputs[604][1] = 3.12;

  // section 605
  carac_sections[605][0] = 31169.01;
  carac_sections[605][1] = 0.00;
  carac_sections[605][2] = 19.19;
  carac_sections[605][3] = 100.00;
  outputs[605][0] = 5.25;
  outputs[605][1] = 3.11;

  // section 606
  carac_sections[606][0] = 31207.42;
  carac_sections[606][1] = 0.00;
  carac_sections[606][2] = 19.16;
  carac_sections[606][3] = 100.00;
  outputs[606][0] = 5.27;
  outputs[606][1] = 3.10;

  // section 607
  carac_sections[607][0] = 31246.19;
  carac_sections[607][1] = 0.00;
  carac_sections[607][2] = 19.12;
  carac_sections[607][3] = 100.00;
  outputs[607][0] = 5.29;
  outputs[607][1] = 3.09;

  // section 608
  carac_sections[608][0] = 31319.26;
  carac_sections[608][1] = 0.00;
  carac_sections[608][2] = 19.05;
  carac_sections[608][3] = 100.00;
  outputs[608][0] = 5.34;
  outputs[608][1] = 3.06;

  // section 609
  carac_sections[609][0] = 31390.44;
  carac_sections[609][1] = 0.00;
  carac_sections[609][2] = 18.98;
  carac_sections[609][3] = 100.00;
  outputs[609][0] = 5.38;
  outputs[609][1] = 3.03;

  // section 610
  carac_sections[610][0] = 31456.50;
  carac_sections[610][1] = 0.00;
  carac_sections[610][2] = 18.92;
  carac_sections[610][3] = 100.00;
  outputs[610][0] = 5.42;
  outputs[610][1] = 3.01;

  // section 611
  carac_sections[611][0] = 31520.55;
  carac_sections[611][1] = 0.00;
  carac_sections[611][2] = 18.86;
  carac_sections[611][3] = 100.00;
  outputs[611][0] = 5.47;
  outputs[611][1] = 2.99;

  // section 612
  carac_sections[612][0] = 31575.82;
  carac_sections[612][1] = 0.00;
  carac_sections[612][2] = 18.80;
  carac_sections[612][3] = 100.00;
  outputs[612][0] = 5.50;
  outputs[612][1] = 2.97;

  // section 613
  carac_sections[613][0] = 31630.55;
  carac_sections[613][1] = 0.00;
  carac_sections[613][2] = 18.75;
  carac_sections[613][3] = 100.00;
  outputs[613][0] = 5.54;
  outputs[613][1] = 2.95;

  // section 614
  carac_sections[614][0] = 31674.38;
  carac_sections[614][1] = 0.00;
  carac_sections[614][2] = 18.70;
  carac_sections[614][3] = 100.00;
  outputs[614][0] = 5.57;
  outputs[614][1] = 2.93;

  // section 615
  carac_sections[615][0] = 31718.72;
  carac_sections[615][1] = 0.00;
  carac_sections[615][2] = 18.66;
  carac_sections[615][3] = 100.00;
  outputs[615][0] = 5.60;
  outputs[615][1] = 2.91;

  // section 616
  carac_sections[616][0] = 31787.50;
  carac_sections[616][1] = 0.00;
  carac_sections[616][2] = 18.59;
  carac_sections[616][3] = 100.00;
  outputs[616][0] = 5.66;
  outputs[616][1] = 2.89;

  // section 617
  carac_sections[617][0] = 31844.88;
  carac_sections[617][1] = 0.00;
  carac_sections[617][2] = 18.53;
  carac_sections[617][3] = 100.00;
  outputs[617][0] = 5.70;
  outputs[617][1] = 2.86;

  // section 618
  carac_sections[618][0] = 31888.15;
  carac_sections[618][1] = 0.00;
  carac_sections[618][2] = 18.49;
  carac_sections[618][3] = 100.00;
  outputs[618][0] = 5.73;
  outputs[618][1] = 2.85;

  // section 619
  carac_sections[619][0] = 31929.53;
  carac_sections[619][1] = 0.00;
  carac_sections[619][2] = 18.45;
  carac_sections[619][3] = 100.00;
  outputs[619][0] = 5.76;
  outputs[619][1] = 2.83;

  // section 620
  carac_sections[620][0] = 31960.71;
  carac_sections[620][1] = 0.00;
  carac_sections[620][2] = 18.42;
  carac_sections[620][3] = 100.00;
  outputs[620][0] = 5.78;
  outputs[620][1] = 2.82;

  // section 621
  carac_sections[621][0] = 31989.54;
  carac_sections[621][1] = 0.00;
  carac_sections[621][2] = 18.39;
  carac_sections[621][3] = 100.00;
  outputs[621][0] = 5.81;
  outputs[621][1] = 2.81;

  // section 622
  carac_sections[622][0] = 32021.50;
  carac_sections[622][1] = 0.00;
  carac_sections[622][2] = 18.36;
  carac_sections[622][3] = 100.00;
  outputs[622][0] = 5.83;
  outputs[622][1] = 2.80;

  // section 623
  carac_sections[623][0] = 32060.41;
  carac_sections[623][1] = 0.00;
  carac_sections[623][2] = 18.32;
  carac_sections[623][3] = 100.00;
  outputs[623][0] = 5.86;
  outputs[623][1] = 2.79;

  // section 624
  carac_sections[624][0] = 32108.93;
  carac_sections[624][1] = 0.00;
  carac_sections[624][2] = 18.27;
  carac_sections[624][3] = 100.00;
  outputs[624][0] = 5.90;
  outputs[624][1] = 2.77;

  // section 625
  carac_sections[625][0] = 32154.49;
  carac_sections[625][1] = 0.00;
  carac_sections[625][2] = 18.23;
  carac_sections[625][3] = 100.00;
  outputs[625][0] = 5.93;
  outputs[625][1] = 2.75;

  // section 626
  carac_sections[626][0] = 32203.61;
  carac_sections[626][1] = 0.00;
  carac_sections[626][2] = 18.18;
  carac_sections[626][3] = 100.00;
  outputs[626][0] = 5.97;
  outputs[626][1] = 2.74;

  // section 627
  carac_sections[627][0] = 32229.98;
  carac_sections[627][1] = 0.00;
  carac_sections[627][2] = 18.15;
  carac_sections[627][3] = 100.00;
  outputs[627][0] = 5.99;
  outputs[627][1] = 2.73;

  // section 628
  carac_sections[628][0] = 32256.16;
  carac_sections[628][1] = 0.00;
  carac_sections[628][2] = 18.13;
  carac_sections[628][3] = 100.00;
  outputs[628][0] = 6.01;
  outputs[628][1] = 2.72;

  // section 629
  carac_sections[629][0] = 32284.02;
  carac_sections[629][1] = 0.00;
  carac_sections[629][2] = 18.10;
  carac_sections[629][3] = 100.00;
  outputs[629][0] = 6.03;
  outputs[629][1] = 2.71;

  // section 630
  carac_sections[630][0] = 32311.16;
  carac_sections[630][1] = 0.00;
  carac_sections[630][2] = 18.07;
  carac_sections[630][3] = 100.00;
  outputs[630][0] = 6.05;
  outputs[630][1] = 2.70;

  // section 631
  carac_sections[631][0] = 32341.04;
  carac_sections[631][1] = 0.00;
  carac_sections[631][2] = 18.05;
  carac_sections[631][3] = 100.00;
  outputs[631][0] = 6.07;
  outputs[631][1] = 2.69;

  // section 632
  carac_sections[632][0] = 32370.14;
  carac_sections[632][1] = 0.00;
  carac_sections[632][2] = 18.02;
  carac_sections[632][3] = 100.00;
  outputs[632][0] = 6.09;
  outputs[632][1] = 2.68;

  // section 633
  carac_sections[633][0] = 32400.46;
  carac_sections[633][1] = 0.00;
  carac_sections[633][2] = 17.99;
  carac_sections[633][3] = 100.00;
  outputs[633][0] = 6.12;
  outputs[633][1] = 2.67;

  // section 634
  carac_sections[634][0] = 32430.87;
  carac_sections[634][1] = 0.00;
  carac_sections[634][2] = 17.96;
  carac_sections[634][3] = 100.00;
  outputs[634][0] = 6.14;
  outputs[634][1] = 2.66;

  // section 635
  carac_sections[635][0] = 32462.88;
  carac_sections[635][1] = 0.00;
  carac_sections[635][2] = 17.93;
  carac_sections[635][3] = 100.00;
  outputs[635][0] = 6.17;
  outputs[635][1] = 2.65;

  // section 636
  carac_sections[636][0] = 32495.12;
  carac_sections[636][1] = 0.00;
  carac_sections[636][2] = 17.89;
  carac_sections[636][3] = 100.00;
  outputs[636][0] = 6.19;
  outputs[636][1] = 2.64;

  // section 637
  carac_sections[637][0] = 32528.21;
  carac_sections[637][1] = 0.00;
  carac_sections[637][2] = 17.86;
  carac_sections[637][3] = 100.00;
  outputs[637][0] = 6.22;
  outputs[637][1] = 2.63;

  // section 638
  carac_sections[638][0] = 32563.60;
  carac_sections[638][1] = 0.00;
  carac_sections[638][2] = 17.83;
  carac_sections[638][3] = 100.00;
  outputs[638][0] = 6.25;
  outputs[638][1] = 2.61;

  // section 639
  carac_sections[639][0] = 32595.73;
  carac_sections[639][1] = 0.00;
  carac_sections[639][2] = 17.80;
  carac_sections[639][3] = 100.00;
  outputs[639][0] = 6.27;
  outputs[639][1] = 2.60;

  // section 640
  carac_sections[640][0] = 32629.16;
  carac_sections[640][1] = 0.00;
  carac_sections[640][2] = 17.76;
  carac_sections[640][3] = 100.00;
  outputs[640][0] = 6.30;
  outputs[640][1] = 2.59;

  // section 641
  carac_sections[641][0] = 32663.12;
  carac_sections[641][1] = 0.00;
  carac_sections[641][2] = 17.73;
  carac_sections[641][3] = 100.00;
  outputs[641][0] = 6.33;
  outputs[641][1] = 2.58;

  // section 642
  carac_sections[642][0] = 32698.33;
  carac_sections[642][1] = 0.00;
  carac_sections[642][2] = 17.69;
  carac_sections[642][3] = 100.00;
  outputs[642][0] = 6.36;
  outputs[642][1] = 2.57;

  // section 643
  carac_sections[643][0] = 32733.76;
  carac_sections[643][1] = 0.00;
  carac_sections[643][2] = 17.66;
  carac_sections[643][3] = 100.00;
  outputs[643][0] = 6.39;
  outputs[643][1] = 2.56;

  // section 644
  carac_sections[644][0] = 32770.39;
  carac_sections[644][1] = 0.00;
  carac_sections[644][2] = 17.62;
  carac_sections[644][3] = 100.00;
  outputs[644][0] = 6.42;
  outputs[644][1] = 2.54;

  // section 645
  carac_sections[645][0] = 32807.62;
  carac_sections[645][1] = 0.00;
  carac_sections[645][2] = 17.58;
  carac_sections[645][3] = 100.00;
  outputs[645][0] = 6.45;
  outputs[645][1] = 2.53;

  // section 646
  carac_sections[646][0] = 32846.24;
  carac_sections[646][1] = 0.00;
  carac_sections[646][2] = 17.55;
  carac_sections[646][3] = 100.00;
  outputs[646][0] = 6.48;
  outputs[646][1] = 2.52;

  // section 647
  carac_sections[647][0] = 32884.43;
  carac_sections[647][1] = 0.00;
  carac_sections[647][2] = 17.51;
  carac_sections[647][3] = 100.00;
  outputs[647][0] = 6.51;
  outputs[647][1] = 2.51;

  // section 648
  carac_sections[648][0] = 32923.71;
  carac_sections[648][1] = 0.00;
  carac_sections[648][2] = 17.47;
  carac_sections[648][3] = 100.00;
  outputs[648][0] = 6.54;
  outputs[648][1] = 2.50;

  // section 649
  carac_sections[649][0] = 32963.36;
  carac_sections[649][1] = 0.00;
  carac_sections[649][2] = 17.43;
  carac_sections[649][3] = 100.00;
  outputs[649][0] = 6.58;
  outputs[649][1] = 2.48;

  // section 650
  carac_sections[650][0] = 33004.08;
  carac_sections[650][1] = 0.00;
  carac_sections[650][2] = 17.39;
  carac_sections[650][3] = 100.00;
  outputs[650][0] = 6.61;
  outputs[650][1] = 2.47;

  // section 651
  carac_sections[651][0] = 33044.68;
  carac_sections[651][1] = 0.00;
  carac_sections[651][2] = 17.35;
  carac_sections[651][3] = 100.00;
  outputs[651][0] = 6.65;
  outputs[651][1] = 2.46;

  // section 652
  carac_sections[652][0] = 33086.24;
  carac_sections[652][1] = 0.00;
  carac_sections[652][2] = 17.31;
  carac_sections[652][3] = 100.00;
  outputs[652][0] = 6.68;
  outputs[652][1] = 2.44;

  // section 653
  carac_sections[653][0] = 33127.72;
  carac_sections[653][1] = 0.00;
  carac_sections[653][2] = 17.27;
  carac_sections[653][3] = 100.00;
  outputs[653][0] = 6.71;
  outputs[653][1] = 2.43;

  // section 654
  carac_sections[654][0] = 33170.62;
  carac_sections[654][1] = 0.00;
  carac_sections[654][2] = 17.23;
  carac_sections[654][3] = 100.00;
  outputs[654][0] = 6.75;
  outputs[654][1] = 2.42;

  // section 655
  carac_sections[655][0] = 33211.30;
  carac_sections[655][1] = 0.00;
  carac_sections[655][2] = 17.19;
  carac_sections[655][3] = 100.00;
  outputs[655][0] = 6.78;
  outputs[655][1] = 2.41;

  // section 656
  carac_sections[656][0] = 33252.63;
  carac_sections[656][1] = 0.00;
  carac_sections[656][2] = 17.15;
  carac_sections[656][3] = 100.00;
  outputs[656][0] = 6.82;
  outputs[656][1] = 2.40;

  // section 657
  carac_sections[657][0] = 33293.76;
  carac_sections[657][1] = 0.00;
  carac_sections[657][2] = 17.11;
  carac_sections[657][3] = 100.00;
  outputs[657][0] = 6.85;
  outputs[657][1] = 2.38;

  // section 658
  carac_sections[658][0] = 33335.77;
  carac_sections[658][1] = 0.00;
  carac_sections[658][2] = 17.07;
  carac_sections[658][3] = 100.00;
  outputs[658][0] = 6.88;
  outputs[658][1] = 2.37;

  // section 659
  carac_sections[659][0] = 33377.05;
  carac_sections[659][1] = 0.00;
  carac_sections[659][2] = 17.03;
  carac_sections[659][3] = 100.00;
  outputs[659][0] = 6.92;
  outputs[659][1] = 2.36;

  // section 660
  carac_sections[660][0] = 33419.01;
  carac_sections[660][1] = 0.00;
  carac_sections[660][2] = 16.99;
  carac_sections[660][3] = 100.00;
  outputs[660][0] = 6.95;
  outputs[660][1] = 2.35;

  // section 661
  carac_sections[661][0] = 33460.93;
  carac_sections[661][1] = 0.00;
  carac_sections[661][2] = 16.95;
  carac_sections[661][3] = 100.00;
  outputs[661][0] = 6.98;
  outputs[661][1] = 2.34;

  // section 662
  carac_sections[662][0] = 33504.06;
  carac_sections[662][1] = 0.00;
  carac_sections[662][2] = 16.91;
  carac_sections[662][3] = 100.00;
  outputs[662][0] = 7.02;
  outputs[662][1] = 2.33;

  // section 663
  carac_sections[663][0] = 33545.72;
  carac_sections[663][1] = 0.00;
  carac_sections[663][2] = 16.87;
  carac_sections[663][3] = 100.00;
  outputs[663][0] = 7.05;
  outputs[663][1] = 2.32;

  // section 664
  carac_sections[664][0] = 33588.16;
  carac_sections[664][1] = 0.00;
  carac_sections[664][2] = 16.83;
  carac_sections[664][3] = 100.00;
  outputs[664][0] = 7.09;
  outputs[664][1] = 2.30;

  // section 665
  carac_sections[665][0] = 33630.84;
  carac_sections[665][1] = 0.00;
  carac_sections[665][2] = 16.79;
  carac_sections[665][3] = 100.00;
  outputs[665][0] = 7.12;
  outputs[665][1] = 2.29;

  // section 666
  carac_sections[666][0] = 33674.60;
  carac_sections[666][1] = 0.00;
  carac_sections[666][2] = 16.75;
  carac_sections[666][3] = 100.00;
  outputs[666][0] = 7.16;
  outputs[666][1] = 2.28;

  // section 667
  carac_sections[667][0] = 33718.01;
  carac_sections[667][1] = 0.00;
  carac_sections[667][2] = 16.71;
  carac_sections[667][3] = 100.00;
  outputs[667][0] = 7.19;
  outputs[667][1] = 2.27;

  // section 668
  carac_sections[668][0] = 33762.28;
  carac_sections[668][1] = 0.00;
  carac_sections[668][2] = 16.67;
  carac_sections[668][3] = 100.00;
  outputs[668][0] = 7.23;
  outputs[668][1] = 2.26;

  // section 669
  carac_sections[669][0] = 33807.00;
  carac_sections[669][1] = 0.00;
  carac_sections[669][2] = 16.62;
  carac_sections[669][3] = 100.00;
  outputs[669][0] = 7.27;
  outputs[669][1] = 2.25;

  // section 670
  carac_sections[670][0] = 33854.41;
  carac_sections[670][1] = 0.00;
  carac_sections[670][2] = 16.58;
  carac_sections[670][3] = 100.00;
  outputs[670][0] = 7.31;
  outputs[670][1] = 2.23;

  // section 671
  carac_sections[671][0] = 33895.07;
  carac_sections[671][1] = 0.00;
  carac_sections[671][2] = 16.54;
  carac_sections[671][3] = 100.00;
  outputs[671][0] = 7.35;
  outputs[671][1] = 2.22;

  // section 672
  carac_sections[672][0] = 33936.31;
  carac_sections[672][1] = 0.00;
  carac_sections[672][2] = 16.50;
  carac_sections[672][3] = 100.00;
  outputs[672][0] = 7.38;
  outputs[672][1] = 2.21;

  // section 673
  carac_sections[673][0] = 33977.75;
  carac_sections[673][1] = 0.00;
  carac_sections[673][2] = 16.46;
  carac_sections[673][3] = 100.00;
  outputs[673][0] = 7.42;
  outputs[673][1] = 2.20;

  // section 674
  carac_sections[674][0] = 34020.30;
  carac_sections[674][1] = 0.00;
  carac_sections[674][2] = 16.41;
  carac_sections[674][3] = 100.00;
  outputs[674][0] = 7.46;
  outputs[674][1] = 2.19;

  // section 675
  carac_sections[675][0] = 34061.74;
  carac_sections[675][1] = 0.00;
  carac_sections[675][2] = 16.37;
  carac_sections[675][3] = 100.00;
  outputs[675][0] = 7.50;
  outputs[675][1] = 2.18;

  // section 676
  carac_sections[676][0] = 34103.76;
  carac_sections[676][1] = 0.00;
  carac_sections[676][2] = 16.33;
  carac_sections[676][3] = 100.00;
  outputs[676][0] = 7.54;
  outputs[676][1] = 2.17;

  // section 677
  carac_sections[677][0] = 34145.93;
  carac_sections[677][1] = 0.00;
  carac_sections[677][2] = 16.29;
  carac_sections[677][3] = 100.00;
  outputs[677][0] = 7.57;
  outputs[677][1] = 2.16;

  // section 678
  carac_sections[678][0] = 34189.26;
  carac_sections[678][1] = 0.00;
  carac_sections[678][2] = 16.25;
  carac_sections[678][3] = 100.00;
  outputs[678][0] = 7.61;
  outputs[678][1] = 2.14;

  // section 679
  carac_sections[679][0] = 34230.28;
  carac_sections[679][1] = 0.00;
  carac_sections[679][2] = 16.20;
  carac_sections[679][3] = 100.00;
  outputs[679][0] = 7.65;
  outputs[679][1] = 2.13;

  // section 680
  carac_sections[680][0] = 34271.60;
  carac_sections[680][1] = 0.00;
  carac_sections[680][2] = 16.16;
  carac_sections[680][3] = 100.00;
  outputs[680][0] = 7.69;
  outputs[680][1] = 2.12;

  // section 681
  carac_sections[681][0] = 34312.80;
  carac_sections[681][1] = 0.00;
  carac_sections[681][2] = 16.13;
  carac_sections[681][3] = 100.00;
  outputs[681][0] = 7.72;
  outputs[681][1] = 2.11;

  // section 682
  carac_sections[682][0] = 34354.64;
  carac_sections[682][1] = 0.00;
  carac_sections[682][2] = 16.09;
  carac_sections[682][3] = 100.00;
  outputs[682][0] = 7.76;
  outputs[682][1] = 2.10;

  // section 683
  carac_sections[683][0] = 34395.20;
  carac_sections[683][1] = 0.00;
  carac_sections[683][2] = 16.05;
  carac_sections[683][3] = 100.00;
  outputs[683][0] = 7.79;
  outputs[683][1] = 2.10;

  // section 684
  carac_sections[684][0] = 34436.00;
  carac_sections[684][1] = 0.00;
  carac_sections[684][2] = 16.01;
  carac_sections[684][3] = 100.00;
  outputs[684][0] = 7.83;
  outputs[684][1] = 2.09;

  // section 685
  carac_sections[685][0] = 34476.41;
  carac_sections[685][1] = 0.00;
  carac_sections[685][2] = 15.97;
  carac_sections[685][3] = 100.00;
  outputs[685][0] = 7.86;
  outputs[685][1] = 2.08;

  // section 686
  carac_sections[686][0] = 34518.16;
  carac_sections[686][1] = 0.00;
  carac_sections[686][2] = 15.94;
  carac_sections[686][3] = 100.00;
  outputs[686][0] = 7.89;
  outputs[686][1] = 2.07;

  // section 687
  carac_sections[687][0] = 34593.03;
  carac_sections[687][1] = 0.00;
  carac_sections[687][2] = 15.87;
  carac_sections[687][3] = 100.00;
  outputs[687][0] = 7.95;
  outputs[687][1] = 2.05;

  // section 688
  carac_sections[688][0] = 34667.45;
  carac_sections[688][1] = 0.00;
  carac_sections[688][2] = 15.81;
  carac_sections[688][3] = 100.00;
  outputs[688][0] = 8.00;
  outputs[688][1] = 2.04;

  // section 689
  carac_sections[689][0] = 34740.10;
  carac_sections[689][1] = 0.00;
  carac_sections[689][2] = 15.76;
  carac_sections[689][3] = 100.00;
  outputs[689][0] = 8.05;
  outputs[689][1] = 2.03;

  // section 690
  carac_sections[690][0] = 34811.73;
  carac_sections[690][1] = 0.00;
  carac_sections[690][2] = 15.71;
  carac_sections[690][3] = 100.00;
  outputs[690][0] = 8.10;
  outputs[690][1] = 2.02;

  // section 691
  carac_sections[691][0] = 34880.51;
  carac_sections[691][1] = 0.00;
  carac_sections[691][2] = 15.66;
  carac_sections[691][3] = 100.00;
  outputs[691][0] = 8.14;
  outputs[691][1] = 2.01;

  // section 692
  carac_sections[692][0] = 34948.05;
  carac_sections[692][1] = 0.00;
  carac_sections[692][2] = 15.61;
  carac_sections[692][3] = 100.00;
  outputs[692][0] = 8.18;
  outputs[692][1] = 2.00;

  // section 693
  carac_sections[693][0] = 35014.11;
  carac_sections[693][1] = 0.00;
  carac_sections[693][2] = 15.57;
  carac_sections[693][3] = 100.00;
  outputs[693][0] = 8.21;
  outputs[693][1] = 1.99;

  // section 694
  carac_sections[694][0] = 35079.88;
  carac_sections[694][1] = 0.00;
  carac_sections[694][2] = 15.53;
  carac_sections[694][3] = 100.00;
  outputs[694][0] = 8.24;
  outputs[694][1] = 1.98;

  // section 695
  carac_sections[695][0] = 35137.03;
  carac_sections[695][1] = 0.00;
  carac_sections[695][2] = 15.50;
  carac_sections[695][3] = 100.00;
  outputs[695][0] = 8.27;
  outputs[695][1] = 1.97;

  // section 696
  carac_sections[696][0] = 35192.37;
  carac_sections[696][1] = 0.00;
  carac_sections[696][2] = 15.47;
  carac_sections[696][3] = 100.00;
  outputs[696][0] = 8.29;
  outputs[696][1] = 1.97;

  // section 697
  carac_sections[697][0] = 35246.13;
  carac_sections[697][1] = 0.00;
  carac_sections[697][2] = 15.45;
  carac_sections[697][3] = 100.00;
  outputs[697][0] = 8.32;
  outputs[697][1] = 1.96;

  // section 698
  carac_sections[698][0] = 35299.78;
  carac_sections[698][1] = 0.00;
  carac_sections[698][2] = 15.42;
  carac_sections[698][3] = 100.00;
  outputs[698][0] = 8.34;
  outputs[698][1] = 1.96;

  // section 699
  carac_sections[699][0] = 35350.64;
  carac_sections[699][1] = 0.00;
  carac_sections[699][2] = 15.40;
  carac_sections[699][3] = 100.00;
  outputs[699][0] = 8.35;
  outputs[699][1] = 1.95;

  // section 700
  carac_sections[700][0] = 35401.05;
  carac_sections[700][1] = 0.00;
  carac_sections[700][2] = 15.38;
  carac_sections[700][3] = 100.00;
  outputs[700][0] = 8.37;
  outputs[700][1] = 1.95;

  // section 701
  carac_sections[701][0] = 35450.35;
  carac_sections[701][1] = 0.00;
  carac_sections[701][2] = 15.36;
  carac_sections[701][3] = 100.00;
  outputs[701][0] = 8.38;
  outputs[701][1] = 1.95;

  // section 702
  carac_sections[702][0] = 35500.17;
  carac_sections[702][1] = 0.00;
  carac_sections[702][2] = 15.34;
  carac_sections[702][3] = 100.00;
  outputs[702][0] = 8.39;
  outputs[702][1] = 1.94;

  // section 703
  carac_sections[703][0] = 35545.51;
  carac_sections[703][1] = 0.00;
  carac_sections[703][2] = 15.33;
  carac_sections[703][3] = 100.00;
  outputs[703][0] = 8.41;
  outputs[703][1] = 1.94;

  // section 704
  carac_sections[704][0] = 35590.54;
  carac_sections[704][1] = 0.00;
  carac_sections[704][2] = 15.32;
  carac_sections[704][3] = 100.00;
  outputs[704][0] = 8.42;
  outputs[704][1] = 1.94;

  // section 705
  carac_sections[705][0] = 35634.52;
  carac_sections[705][1] = 0.00;
  carac_sections[705][2] = 15.30;
  carac_sections[705][3] = 100.00;
  outputs[705][0] = 8.42;
  outputs[705][1] = 1.94;

  // section 706
  carac_sections[706][0] = 35678.83;
  carac_sections[706][1] = 0.00;
  carac_sections[706][2] = 15.29;
  carac_sections[706][3] = 100.00;
  outputs[706][0] = 8.43;
  outputs[706][1] = 1.94;

  // section 707
  carac_sections[707][0] = 35720.91;
  carac_sections[707][1] = 0.00;
  carac_sections[707][2] = 15.28;
  carac_sections[707][3] = 100.00;
  outputs[707][0] = 8.44;
  outputs[707][1] = 1.94;

  // section 708
  carac_sections[708][0] = 35763.03;
  carac_sections[708][1] = 0.00;
  carac_sections[708][2] = 15.27;
  carac_sections[708][3] = 100.00;
  outputs[708][0] = 8.44;
  outputs[708][1] = 1.93;

  // section 709
  carac_sections[709][0] = 35804.19;
  carac_sections[709][1] = 0.00;
  carac_sections[709][2] = 15.26;
  carac_sections[709][3] = 100.00;
  outputs[709][0] = 8.45;
  outputs[709][1] = 1.93;

  // section 710
  carac_sections[710][0] = 35846.51;
  carac_sections[710][1] = 0.00;
  carac_sections[710][2] = 15.25;
  carac_sections[710][3] = 100.00;
  outputs[710][0] = 8.45;
  outputs[710][1] = 1.93;

  // section 711
  carac_sections[711][0] = 35917.35;
  carac_sections[711][1] = 0.00;
  carac_sections[711][2] = 15.24;
  carac_sections[711][3] = 100.00;
  outputs[711][0] = 8.45;
  outputs[711][1] = 1.93;

  // section 712
  carac_sections[712][0] = 35986.43;
  carac_sections[712][1] = 0.00;
  carac_sections[712][2] = 15.23;
  carac_sections[712][3] = 100.00;
  outputs[712][0] = 8.46;
  outputs[712][1] = 1.93;

  // section 713
  carac_sections[713][0] = 36052.84;
  carac_sections[713][1] = 0.00;
  carac_sections[713][2] = 15.23;
  carac_sections[713][3] = 100.00;
  outputs[713][0] = 8.46;
  outputs[713][1] = 1.93;

  // section 714
  carac_sections[714][0] = 36118.27;
  carac_sections[714][1] = 0.00;
  carac_sections[714][2] = 15.22;
  carac_sections[714][3] = 100.00;
  outputs[714][0] = 8.46;
  outputs[714][1] = 1.93;

  // section 715
  carac_sections[715][0] = 36179.29;
  carac_sections[715][1] = 0.00;
  carac_sections[715][2] = 15.22;
  carac_sections[715][3] = 100.00;
  outputs[715][0] = 8.45;
  outputs[715][1] = 1.93;

  // section 716
  carac_sections[716][0] = 36239.05;
  carac_sections[716][1] = 0.00;
  carac_sections[716][2] = 15.22;
  carac_sections[716][3] = 100.00;
  outputs[716][0] = 8.45;
  outputs[716][1] = 1.93;

  // section 717
  carac_sections[717][0] = 36297.06;
  carac_sections[717][1] = 0.00;
  carac_sections[717][2] = 15.22;
  carac_sections[717][3] = 100.00;
  outputs[717][0] = 8.44;
  outputs[717][1] = 1.93;

  // section 718
  carac_sections[718][0] = 36355.46;
  carac_sections[718][1] = 0.00;
  carac_sections[718][2] = 15.22;
  carac_sections[718][3] = 100.00;
  outputs[718][0] = 8.43;
  outputs[718][1] = 1.94;

  // section 719
  carac_sections[719][0] = 36405.20;
  carac_sections[719][1] = 0.00;
  carac_sections[719][2] = 15.22;
  carac_sections[719][3] = 100.00;
  outputs[719][0] = 8.43;
  outputs[719][1] = 1.94;

  // section 720
  carac_sections[720][0] = 36453.94;
  carac_sections[720][1] = 0.00;
  carac_sections[720][2] = 15.22;
  carac_sections[720][3] = 100.00;
  outputs[720][0] = 8.42;
  outputs[720][1] = 1.94;

  // section 721
  carac_sections[721][0] = 36501.09;
  carac_sections[721][1] = 0.00;
  carac_sections[721][2] = 15.23;
  carac_sections[721][3] = 100.00;
  outputs[721][0] = 8.41;
  outputs[721][1] = 1.94;

  // section 722
  carac_sections[722][0] = 36548.77;
  carac_sections[722][1] = 0.00;
  carac_sections[722][2] = 15.23;
  carac_sections[722][3] = 100.00;
  outputs[722][0] = 8.40;
  outputs[722][1] = 1.94;

  // section 723
  carac_sections[723][0] = 36592.58;
  carac_sections[723][1] = 0.00;
  carac_sections[723][2] = 15.24;
  carac_sections[723][3] = 100.00;
  outputs[723][0] = 8.39;
  outputs[723][1] = 1.95;

  // section 724
  carac_sections[724][0] = 36636.33;
  carac_sections[724][1] = 0.00;
  carac_sections[724][2] = 15.24;
  carac_sections[724][3] = 100.00;
  outputs[724][0] = 8.38;
  outputs[724][1] = 1.95;

  // section 725
  carac_sections[725][0] = 36678.79;
  carac_sections[725][1] = 0.00;
  carac_sections[725][2] = 15.25;
  carac_sections[725][3] = 100.00;
  outputs[725][0] = 8.36;
  outputs[725][1] = 1.95;

  // section 726
  carac_sections[726][0] = 36722.77;
  carac_sections[726][1] = 0.00;
  carac_sections[726][2] = 15.26;
  carac_sections[726][3] = 100.00;
  outputs[726][0] = 8.35;
  outputs[726][1] = 1.96;

  // section 727
  carac_sections[727][0] = 36795.41;
  carac_sections[727][1] = 0.00;
  carac_sections[727][2] = 15.27;
  carac_sections[727][3] = 100.00;
  outputs[727][0] = 8.33;
  outputs[727][1] = 1.96;

  // section 728
  carac_sections[728][0] = 36866.50;
  carac_sections[728][1] = 0.00;
  carac_sections[728][2] = 15.28;
  carac_sections[728][3] = 100.00;
  outputs[728][0] = 8.30;
  outputs[728][1] = 1.97;

  // section 729
  carac_sections[729][0] = 36933.75;
  carac_sections[729][1] = 0.00;
  carac_sections[729][2] = 15.30;
  carac_sections[729][3] = 100.00;
  outputs[729][0] = 8.28;
  outputs[729][1] = 1.97;

  // section 730
  carac_sections[730][0] = 36999.79;
  carac_sections[730][1] = 0.00;
  carac_sections[730][2] = 15.32;
  carac_sections[730][3] = 100.00;
  outputs[730][0] = 8.25;
  outputs[730][1] = 1.98;

  // section 731
  carac_sections[731][0] = 37057.48;
  carac_sections[731][1] = 0.00;
  carac_sections[731][2] = 15.33;
  carac_sections[731][3] = 100.00;
  outputs[731][0] = 8.23;
  outputs[731][1] = 1.98;

  // section 732
  carac_sections[732][0] = 37113.56;
  carac_sections[732][1] = 0.00;
  carac_sections[732][2] = 15.35;
  carac_sections[732][3] = 100.00;
  outputs[732][0] = 8.21;
  outputs[732][1] = 1.99;

  // section 733
  carac_sections[733][0] = 37166.67;
  carac_sections[733][1] = 0.00;
  carac_sections[733][2] = 15.37;
  carac_sections[733][3] = 100.00;
  outputs[733][0] = 8.18;
  outputs[733][1] = 2.00;

  // section 734
  carac_sections[734][0] = 37220.34;
  carac_sections[734][1] = 0.00;
  carac_sections[734][2] = 15.39;
  carac_sections[734][3] = 100.00;
  outputs[734][0] = 8.16;
  outputs[734][1] = 2.00;

  // section 735
  carac_sections[735][0] = 37265.91;
  carac_sections[735][1] = 0.00;
  carac_sections[735][2] = 15.40;
  carac_sections[735][3] = 100.00;
  outputs[735][0] = 8.13;
  outputs[735][1] = 2.01;

  // section 736
  carac_sections[736][0] = 37311.26;
  carac_sections[736][1] = 0.00;
  carac_sections[736][2] = 15.42;
  carac_sections[736][3] = 100.00;
  outputs[736][0] = 8.11;
  outputs[736][1] = 2.01;

  // section 737
  carac_sections[737][0] = 37354.06;
  carac_sections[737][1] = 0.00;
  carac_sections[737][2] = 15.44;
  carac_sections[737][3] = 100.00;
  outputs[737][0] = 8.09;
  outputs[737][1] = 2.02;

  // section 738
  carac_sections[738][0] = 37398.64;
  carac_sections[738][1] = 0.00;
  carac_sections[738][2] = 15.45;
  carac_sections[738][3] = 100.00;
  outputs[738][0] = 8.06;
  outputs[738][1] = 2.03;

  // section 739
  carac_sections[739][0] = 37472.43;
  carac_sections[739][1] = 0.00;
  carac_sections[739][2] = 15.48;
  carac_sections[739][3] = 100.00;
  outputs[739][0] = 8.02;
  outputs[739][1] = 2.04;

  // section 740
  carac_sections[740][0] = 37544.42;
  carac_sections[740][1] = 0.00;
  carac_sections[740][2] = 15.52;
  carac_sections[740][3] = 100.00;
  outputs[740][0] = 7.98;
  outputs[740][1] = 2.05;

  // section 741
  carac_sections[741][0] = 37608.14;
  carac_sections[741][1] = 0.00;
  carac_sections[741][2] = 15.54;
  carac_sections[741][3] = 100.00;
  outputs[741][0] = 7.94;
  outputs[741][1] = 2.06;

  // section 742
  carac_sections[742][0] = 37669.62;
  carac_sections[742][1] = 0.00;
  carac_sections[742][2] = 15.57;
  carac_sections[742][3] = 100.00;
  outputs[742][0] = 7.90;
  outputs[742][1] = 2.07;

  // section 743
  carac_sections[743][0] = 37723.59;
  carac_sections[743][1] = 0.00;
  carac_sections[743][2] = 15.60;
  carac_sections[743][3] = 100.00;
  outputs[743][0] = 7.86;
  outputs[743][1] = 2.08;

  // section 744
  carac_sections[744][0] = 37776.97;
  carac_sections[744][1] = 0.00;
  carac_sections[744][2] = 15.63;
  carac_sections[744][3] = 100.00;
  outputs[744][0] = 7.83;
  outputs[744][1] = 2.09;

  // section 745
  carac_sections[745][0] = 37823.71;
  carac_sections[745][1] = 0.00;
  carac_sections[745][2] = 15.65;
  carac_sections[745][3] = 100.00;
  outputs[745][0] = 7.79;
  outputs[745][1] = 2.09;

  // section 746
  carac_sections[746][0] = 37870.80;
  carac_sections[746][1] = 0.00;
  carac_sections[746][2] = 15.68;
  carac_sections[746][3] = 100.00;
  outputs[746][0] = 7.76;
  outputs[746][1] = 2.10;

  // section 747
  carac_sections[747][0] = 37911.58;
  carac_sections[747][1] = 0.00;
  carac_sections[747][2] = 15.70;
  carac_sections[747][3] = 100.00;
  outputs[747][0] = 7.73;
  outputs[747][1] = 2.11;

  // section 748
  carac_sections[748][0] = 37953.09;
  carac_sections[748][1] = 0.00;
  carac_sections[748][2] = 15.72;
  carac_sections[748][3] = 100.00;
  outputs[748][0] = 7.70;
  outputs[748][1] = 2.12;

  // section 749
  carac_sections[749][0] = 38026.62;
  carac_sections[749][1] = 0.00;
  carac_sections[749][2] = 15.77;
  carac_sections[749][3] = 100.00;
  outputs[749][0] = 7.64;
  outputs[749][1] = 2.14;

  // section 750
  carac_sections[750][0] = 38094.95;
  carac_sections[750][1] = 0.00;
  carac_sections[750][2] = 15.81;
  carac_sections[750][3] = 100.00;
  outputs[750][0] = 7.59;
  outputs[750][1] = 2.15;

  // section 751
  carac_sections[751][0] = 38158.24;
  carac_sections[751][1] = 0.00;
  carac_sections[751][2] = 15.84;
  carac_sections[751][3] = 100.00;
  outputs[751][0] = 7.54;
  outputs[751][1] = 2.17;

  // section 752
  carac_sections[752][0] = 38217.76;
  carac_sections[752][1] = 0.00;
  carac_sections[752][2] = 15.88;
  carac_sections[752][3] = 100.00;
  outputs[752][0] = 7.49;
  outputs[752][1] = 2.18;

  // section 753
  carac_sections[753][0] = 38274.46;
  carac_sections[753][1] = 0.00;
  carac_sections[753][2] = 15.92;
  carac_sections[753][3] = 100.00;
  outputs[753][0] = 7.44;
  outputs[753][1] = 2.19;

  // section 754
  carac_sections[754][0] = 38329.35;
  carac_sections[754][1] = 0.00;
  carac_sections[754][2] = 15.95;
  carac_sections[754][3] = 100.00;
  outputs[754][0] = 7.40;
  outputs[754][1] = 2.21;

  // section 755
  carac_sections[755][0] = 38383.43;
  carac_sections[755][1] = 0.00;
  carac_sections[755][2] = 15.99;
  carac_sections[755][3] = 100.00;
  outputs[755][0] = 7.35;
  outputs[755][1] = 2.22;

  // section 756
  carac_sections[756][0] = 38437.75;
  carac_sections[756][1] = 0.00;
  carac_sections[756][2] = 16.02;
  carac_sections[756][3] = 100.00;
  outputs[756][0] = 7.30;
  outputs[756][1] = 2.24;

  // section 757
  carac_sections[757][0] = 38493.09;
  carac_sections[757][1] = 0.00;
  carac_sections[757][2] = 16.06;
  carac_sections[757][3] = 100.00;
  outputs[757][0] = 7.25;
  outputs[757][1] = 2.25;

  // section 758
  carac_sections[758][0] = 38555.42;
  carac_sections[758][1] = 0.00;
  carac_sections[758][2] = 16.10;
  carac_sections[758][3] = 100.00;
  outputs[758][0] = 7.20;
  outputs[758][1] = 2.27;

  // section 759
  carac_sections[759][0] = 38582.73;
  carac_sections[759][1] = 0.00;
  carac_sections[759][2] = 16.12;
  carac_sections[759][3] = 100.00;
  outputs[759][0] = 7.17;
  outputs[759][1] = 2.28;

  // section 760
  carac_sections[760][0] = 38613.84;
  carac_sections[760][1] = 0.00;
  carac_sections[760][2] = 16.14;
  carac_sections[760][3] = 100.00;
  outputs[760][0] = 7.14;
  outputs[760][1] = 2.29;

  // section 761
  carac_sections[761][0] = 38648.04;
  carac_sections[761][1] = 0.00;
  carac_sections[761][2] = 16.16;
  carac_sections[761][3] = 100.00;
  outputs[761][0] = 7.11;
  outputs[761][1] = 2.30;

  // section 762
  carac_sections[762][0] = 38683.02;
  carac_sections[762][1] = 0.00;
  carac_sections[762][2] = 16.19;
  carac_sections[762][3] = 100.00;
  outputs[762][0] = 7.08;
  outputs[762][1] = 2.31;

  // section 763
  carac_sections[763][0] = 38721.23;
  carac_sections[763][1] = 0.00;
  carac_sections[763][2] = 16.21;
  carac_sections[763][3] = 100.00;
  outputs[763][0] = 7.04;
  outputs[763][1] = 2.32;

  // section 764
  carac_sections[764][0] = 38762.02;
  carac_sections[764][1] = 0.00;
  carac_sections[764][2] = 16.24;
  carac_sections[764][3] = 100.00;
  outputs[764][0] = 7.00;
  outputs[764][1] = 2.33;

  // section 765
  carac_sections[765][0] = 38807.21;
  carac_sections[765][1] = 0.00;
  carac_sections[765][2] = 16.27;
  carac_sections[765][3] = 100.00;
  outputs[765][0] = 6.96;
  outputs[765][1] = 2.35;

  // section 766
  carac_sections[766][0] = 38857.09;
  carac_sections[766][1] = 0.00;
  carac_sections[766][2] = 16.31;
  carac_sections[766][3] = 100.00;
  outputs[766][0] = 6.91;
  outputs[766][1] = 2.36;

  // section 767
  carac_sections[767][0] = 38913.45;
  carac_sections[767][1] = 0.00;
  carac_sections[767][2] = 16.35;
  carac_sections[767][3] = 100.00;
  outputs[767][0] = 6.85;
  outputs[767][1] = 2.38;

  // section 768
  carac_sections[768][0] = 38979.55;
  carac_sections[768][1] = 0.00;
  carac_sections[768][2] = 16.39;
  carac_sections[768][3] = 100.00;
  outputs[768][0] = 6.79;
  outputs[768][1] = 2.41;

  // section 769
  carac_sections[769][0] = 39015.36;
  carac_sections[769][1] = 0.00;
  carac_sections[769][2] = 16.42;
  carac_sections[769][3] = 100.00;
  outputs[769][0] = 6.75;
  outputs[769][1] = 2.42;

  // section 770
  carac_sections[770][0] = 39053.60;
  carac_sections[770][1] = 0.00;
  carac_sections[770][2] = 16.45;
  carac_sections[770][3] = 100.00;
  outputs[770][0] = 6.71;
  outputs[770][1] = 2.43;

  // section 771
  carac_sections[771][0] = 39098.90;
  carac_sections[771][1] = 0.00;
  carac_sections[771][2] = 16.48;
  carac_sections[771][3] = 100.00;
  outputs[771][0] = 6.67;
  outputs[771][1] = 2.45;

  // section 772
  carac_sections[772][0] = 39147.20;
  carac_sections[772][1] = 0.00;
  carac_sections[772][2] = 16.51;
  carac_sections[772][3] = 100.00;
  outputs[772][0] = 6.62;
  outputs[772][1] = 2.47;

  // section 773
  carac_sections[773][0] = 39202.61;
  carac_sections[773][1] = 0.00;
  carac_sections[773][2] = 16.55;
  carac_sections[773][3] = 100.00;
  outputs[773][0] = 6.56;
  outputs[773][1] = 2.49;

  // section 774
  carac_sections[774][0] = 39266.34;
  carac_sections[774][1] = 0.00;
  carac_sections[774][2] = 16.59;
  carac_sections[774][3] = 100.00;
  outputs[774][0] = 6.50;
  outputs[774][1] = 2.51;

  // section 775
  carac_sections[775][0] = 39300.82;
  carac_sections[775][1] = 0.00;
  carac_sections[775][2] = 16.62;
  carac_sections[775][3] = 100.00;
  outputs[775][0] = 6.46;
  outputs[775][1] = 2.53;

  // section 776
  carac_sections[776][0] = 39336.59;
  carac_sections[776][1] = 0.00;
  carac_sections[776][2] = 16.64;
  carac_sections[776][3] = 100.00;
  outputs[776][0] = 6.43;
  outputs[776][1] = 2.54;

  // section 777
  carac_sections[777][0] = 39379.45;
  carac_sections[777][1] = 0.00;
  carac_sections[777][2] = 16.67;
  carac_sections[777][3] = 100.00;
  outputs[777][0] = 6.38;
  outputs[777][1] = 2.56;

  // section 778
  carac_sections[778][0] = 39422.80;
  carac_sections[778][1] = 0.00;
  carac_sections[778][2] = 16.69;
  carac_sections[778][3] = 100.00;
  outputs[778][0] = 6.34;
  outputs[778][1] = 2.58;

  // section 779
  carac_sections[779][0] = 39472.52;
  carac_sections[779][1] = 0.00;
  carac_sections[779][2] = 16.72;
  carac_sections[779][3] = 100.00;
  outputs[779][0] = 6.29;
  outputs[779][1] = 2.60;

  // section 780
  carac_sections[780][0] = 39525.40;
  carac_sections[780][1] = 0.00;
  carac_sections[780][2] = 16.76;
  carac_sections[780][3] = 100.00;
  outputs[780][0] = 6.24;
  outputs[780][1] = 2.62;

  // section 781
  carac_sections[781][0] = 39585.62;
  carac_sections[781][1] = 0.00;
  carac_sections[781][2] = 16.79;
  carac_sections[781][3] = 100.00;
  outputs[781][0] = 6.18;
  outputs[781][1] = 2.64;

  // section 782
  carac_sections[782][0] = 39651.81;
  carac_sections[782][1] = 0.00;
  carac_sections[782][2] = 16.83;
  carac_sections[782][3] = 100.00;
  outputs[782][0] = 6.12;
  outputs[782][1] = 2.67;

  // section 783
  carac_sections[783][0] = 39725.73;
  carac_sections[783][1] = 0.00;
  carac_sections[783][2] = 16.86;
  carac_sections[783][3] = 100.00;
  outputs[783][0] = 6.05;
  outputs[783][1] = 2.70;

  // section 784
  carac_sections[784][0] = 39767.88;
  carac_sections[784][1] = 0.00;
  carac_sections[784][2] = 16.88;
  carac_sections[784][3] = 100.00;
  outputs[784][0] = 6.01;
  outputs[784][1] = 2.72;

  // section 785
  carac_sections[785][0] = 39810.13;
  carac_sections[785][1] = 0.00;
  carac_sections[785][2] = 16.90;
  carac_sections[785][3] = 100.00;
  outputs[785][0] = 5.98;
  outputs[785][1] = 2.73;

  // section 786
  carac_sections[786][0] = 39859.00;
  carac_sections[786][1] = 0.00;
  carac_sections[786][2] = 16.92;
  carac_sections[786][3] = 100.00;
  outputs[786][0] = 5.94;
  outputs[786][1] = 2.75;

  // section 787
  carac_sections[787][0] = 39908.96;
  carac_sections[787][1] = 0.00;
  carac_sections[787][2] = 16.94;
  carac_sections[787][3] = 100.00;
  outputs[787][0] = 5.90;
  outputs[787][1] = 2.77;

  // section 788
  carac_sections[788][0] = 39963.33;
  carac_sections[788][1] = 0.00;
  carac_sections[788][2] = 16.96;
  carac_sections[788][3] = 100.00;
  outputs[788][0] = 5.85;
  outputs[788][1] = 2.79;

  // section 789
  carac_sections[789][0] = 40020.29;
  carac_sections[789][1] = 0.00;
  carac_sections[789][2] = 16.98;
  carac_sections[789][3] = 100.00;
  outputs[789][0] = 5.81;
  outputs[789][1] = 2.81;

  // section 790
  carac_sections[790][0] = 40081.78;
  carac_sections[790][1] = 0.00;
  carac_sections[790][2] = 16.99;
  carac_sections[790][3] = 100.00;
  outputs[790][0] = 5.77;
  outputs[790][1] = 2.83;

  // section 791
  carac_sections[791][0] = 40146.63;
  carac_sections[791][1] = 0.00;
  carac_sections[791][2] = 17.00;
  carac_sections[791][3] = 100.00;
  outputs[791][0] = 5.73;
  outputs[791][1] = 2.85;

  // section 792
  carac_sections[792][0] = 40216.26;
  carac_sections[792][1] = 0.00;
  carac_sections[792][2] = 17.01;
  carac_sections[792][3] = 100.00;
  outputs[792][0] = 5.69;
  outputs[792][1] = 2.87;

  // section 793
  carac_sections[793][0] = 40289.18;
  carac_sections[793][1] = 0.00;
  carac_sections[793][2] = 17.01;
  carac_sections[793][3] = 100.00;
  outputs[793][0] = 5.66;
  outputs[793][1] = 2.88;

  // section 794
  carac_sections[794][0] = 40328.69;
  carac_sections[794][1] = 0.00;
  carac_sections[794][2] = 17.01;
  carac_sections[794][3] = 100.00;
  outputs[794][0] = 5.64;
  outputs[794][1] = 2.90;

  // section 795
  carac_sections[795][0] = 40367.75;
  carac_sections[795][1] = 0.00;
  carac_sections[795][2] = 17.01;
  carac_sections[795][3] = 100.00;
  outputs[795][0] = 5.62;
  outputs[795][1] = 2.90;

  // section 796
  carac_sections[796][0] = 40409.23;
  carac_sections[796][1] = 0.00;
  carac_sections[796][2] = 17.01;
  carac_sections[796][3] = 100.00;
  outputs[796][0] = 5.61;
  outputs[796][1] = 2.91;

  // section 797
  carac_sections[797][0] = 40450.33;
  carac_sections[797][1] = 0.00;
  carac_sections[797][2] = 17.00;
  carac_sections[797][3] = 100.00;
  outputs[797][0] = 5.60;
  outputs[797][1] = 2.92;

  // section 798
  carac_sections[798][0] = 40494.18;
  carac_sections[798][1] = 0.00;
  carac_sections[798][2] = 16.99;
  carac_sections[798][3] = 100.00;
  outputs[798][0] = 5.59;
  outputs[798][1] = 2.92;

  // section 799
  carac_sections[799][0] = 40537.71;
  carac_sections[799][1] = 0.00;
  carac_sections[799][2] = 16.98;
  carac_sections[799][3] = 100.00;
  outputs[799][0] = 5.58;
  outputs[799][1] = 2.93;

  // section 800
  carac_sections[800][0] = 40582.80;
  carac_sections[800][1] = 0.00;
  carac_sections[800][2] = 16.97;
  carac_sections[800][3] = 100.00;
  outputs[800][0] = 5.57;
  outputs[800][1] = 2.93;

  // section 801
  carac_sections[801][0] = 40627.96;
  carac_sections[801][1] = 0.00;
  carac_sections[801][2] = 16.96;
  carac_sections[801][3] = 100.00;
  outputs[801][0] = 5.57;
  outputs[801][1] = 2.93;

  // section 802
  carac_sections[802][0] = 40674.74;
  carac_sections[802][1] = 0.00;
  carac_sections[802][2] = 16.94;
  carac_sections[802][3] = 100.00;
  outputs[802][0] = 5.57;
  outputs[802][1] = 2.93;

  // section 803
  carac_sections[803][0] = 40721.55;
  carac_sections[803][1] = 0.00;
  carac_sections[803][2] = 16.92;
  carac_sections[803][3] = 100.00;
  outputs[803][0] = 5.57;
  outputs[803][1] = 2.93;

  // section 804
  carac_sections[804][0] = 40769.41;
  carac_sections[804][1] = 0.00;
  carac_sections[804][2] = 16.90;
  carac_sections[804][3] = 100.00;
  outputs[804][0] = 5.57;
  outputs[804][1] = 2.93;

  // section 805
  carac_sections[805][0] = 40817.41;
  carac_sections[805][1] = 0.00;
  carac_sections[805][2] = 16.88;
  carac_sections[805][3] = 100.00;
  outputs[805][0] = 5.58;
  outputs[805][1] = 2.93;

  // section 806
  carac_sections[806][0] = 40866.46;
  carac_sections[806][1] = 0.00;
  carac_sections[806][2] = 16.85;
  carac_sections[806][3] = 100.00;
  outputs[806][0] = 5.58;
  outputs[806][1] = 2.92;

  // section 807
  carac_sections[807][0] = 40915.55;
  carac_sections[807][1] = 0.00;
  carac_sections[807][2] = 16.82;
  carac_sections[807][3] = 100.00;
  outputs[807][0] = 5.59;
  outputs[807][1] = 2.92;

  // section 808
  carac_sections[808][0] = 40965.22;
  carac_sections[808][1] = 0.00;
  carac_sections[808][2] = 16.79;
  carac_sections[808][3] = 100.00;
  outputs[808][0] = 5.61;
  outputs[808][1] = 2.91;

  // section 809
  carac_sections[809][0] = 41014.98;
  carac_sections[809][1] = 0.00;
  carac_sections[809][2] = 16.76;
  carac_sections[809][3] = 100.00;
  outputs[809][0] = 5.62;
  outputs[809][1] = 2.90;

  // section 810
  carac_sections[810][0] = 41065.27;
  carac_sections[810][1] = 0.00;
  carac_sections[810][2] = 16.73;
  carac_sections[810][3] = 100.00;
  outputs[810][0] = 5.64;
  outputs[810][1] = 2.90;

  // section 811
  carac_sections[811][0] = 41115.57;
  carac_sections[811][1] = 0.00;
  carac_sections[811][2] = 16.69;
  carac_sections[811][3] = 100.00;
  outputs[811][0] = 5.66;
  outputs[811][1] = 2.89;

  // section 812
  carac_sections[812][0] = 41166.03;
  carac_sections[812][1] = 0.00;
  carac_sections[812][2] = 16.65;
  carac_sections[812][3] = 100.00;
  outputs[812][0] = 5.68;
  outputs[812][1] = 2.87;

  // section 813
  carac_sections[813][0] = 41216.49;
  carac_sections[813][1] = 0.00;
  carac_sections[813][2] = 16.61;
  carac_sections[813][3] = 100.00;
  outputs[813][0] = 5.70;
  outputs[813][1] = 2.86;

  // section 814
  carac_sections[814][0] = 41267.01;
  carac_sections[814][1] = 0.00;
  carac_sections[814][2] = 16.57;
  carac_sections[814][3] = 100.00;
  outputs[814][0] = 5.73;
  outputs[814][1] = 2.85;

  // section 815
  carac_sections[815][0] = 41317.53;
  carac_sections[815][1] = 0.00;
  carac_sections[815][2] = 16.53;
  carac_sections[815][3] = 100.00;
  outputs[815][0] = 5.76;
  outputs[815][1] = 2.84;

  // section 816
  carac_sections[816][0] = 41367.92;
  carac_sections[816][1] = 0.00;
  carac_sections[816][2] = 16.49;
  carac_sections[816][3] = 100.00;
  outputs[816][0] = 5.79;
  outputs[816][1] = 2.82;

  // section 817
  carac_sections[817][0] = 41418.86;
  carac_sections[817][1] = 0.00;
  carac_sections[817][2] = 16.45;
  carac_sections[817][3] = 100.00;
  outputs[817][0] = 5.82;
  outputs[817][1] = 2.81;

  // section 818
  carac_sections[818][0] = 41466.77;
  carac_sections[818][1] = 0.00;
  carac_sections[818][2] = 16.40;
  carac_sections[818][3] = 100.00;
  outputs[818][0] = 5.85;
  outputs[818][1] = 2.79;

  // section 819
  carac_sections[819][0] = 41514.34;
  carac_sections[819][1] = 0.00;
  carac_sections[819][2] = 16.36;
  carac_sections[819][3] = 100.00;
  outputs[819][0] = 5.88;
  outputs[819][1] = 2.78;

  // section 820
  carac_sections[820][0] = 41561.51;
  carac_sections[820][1] = 0.00;
  carac_sections[820][2] = 16.32;
  carac_sections[820][3] = 100.00;
  outputs[820][0] = 5.91;
  outputs[820][1] = 2.76;

  // section 821
  carac_sections[821][0] = 41608.71;
  carac_sections[821][1] = 0.00;
  carac_sections[821][2] = 16.27;
  carac_sections[821][3] = 100.00;
  outputs[821][0] = 5.94;
  outputs[821][1] = 2.75;

  // section 822
  carac_sections[822][0] = 41655.08;
  carac_sections[822][1] = 0.00;
  carac_sections[822][2] = 16.23;
  carac_sections[822][3] = 100.00;
  outputs[822][0] = 5.98;
  outputs[822][1] = 2.73;

  // section 823
  carac_sections[823][0] = 41701.39;
  carac_sections[823][1] = 0.00;
  carac_sections[823][2] = 16.19;
  carac_sections[823][3] = 100.00;
  outputs[823][0] = 6.01;
  outputs[823][1] = 2.72;

  // section 824
  carac_sections[824][0] = 41747.22;
  carac_sections[824][1] = 0.00;
  carac_sections[824][2] = 16.14;
  carac_sections[824][3] = 100.00;
  outputs[824][0] = 6.04;
  outputs[824][1] = 2.70;

  // section 825
  carac_sections[825][0] = 41793.20;
  carac_sections[825][1] = 0.00;
  carac_sections[825][2] = 16.10;
  carac_sections[825][3] = 100.00;
  outputs[825][0] = 6.08;
  outputs[825][1] = 2.69;

  // section 826
  carac_sections[826][0] = 41837.80;
  carac_sections[826][1] = 0.00;
  carac_sections[826][2] = 16.05;
  carac_sections[826][3] = 100.00;
  outputs[826][0] = 6.11;
  outputs[826][1] = 2.67;

  // section 827
  carac_sections[827][0] = 41882.46;
  carac_sections[827][1] = 0.00;
  carac_sections[827][2] = 16.01;
  carac_sections[827][3] = 100.00;
  outputs[827][0] = 6.15;
  outputs[827][1] = 2.66;

  // section 828
  carac_sections[828][0] = 41926.51;
  carac_sections[828][1] = 0.00;
  carac_sections[828][2] = 15.97;
  carac_sections[828][3] = 100.00;
  outputs[828][0] = 6.18;
  outputs[828][1] = 2.64;

  // section 829
  carac_sections[829][0] = 41970.53;
  carac_sections[829][1] = 0.00;
  carac_sections[829][2] = 15.92;
  carac_sections[829][3] = 100.00;
  outputs[829][0] = 6.22;
  outputs[829][1] = 2.63;

  // section 830
  carac_sections[830][0] = 42013.68;
  carac_sections[830][1] = 0.00;
  carac_sections[830][2] = 15.88;
  carac_sections[830][3] = 100.00;
  outputs[830][0] = 6.25;
  outputs[830][1] = 2.61;

  // section 831
  carac_sections[831][0] = 42056.97;
  carac_sections[831][1] = 0.00;
  carac_sections[831][2] = 15.84;
  carac_sections[831][3] = 100.00;
  outputs[831][0] = 6.29;
  outputs[831][1] = 2.60;

  // section 832
  carac_sections[832][0] = 42099.53;
  carac_sections[832][1] = 0.00;
  carac_sections[832][2] = 15.79;
  carac_sections[832][3] = 100.00;
  outputs[832][0] = 6.32;
  outputs[832][1] = 2.58;

  // section 833
  carac_sections[833][0] = 42143.01;
  carac_sections[833][1] = 0.00;
  carac_sections[833][2] = 15.75;
  carac_sections[833][3] = 100.00;
  outputs[833][0] = 6.36;
  outputs[833][1] = 2.57;

  // section 834
  carac_sections[834][0] = 42183.09;
  carac_sections[834][1] = 0.00;
  carac_sections[834][2] = 15.71;
  carac_sections[834][3] = 100.00;
  outputs[834][0] = 6.39;
  outputs[834][1] = 2.55;

  // section 835
  carac_sections[835][0] = 42223.43;
  carac_sections[835][1] = 0.00;
  carac_sections[835][2] = 15.67;
  carac_sections[835][3] = 100.00;
  outputs[835][0] = 6.43;
  outputs[835][1] = 2.54;

  // section 836
  carac_sections[836][0] = 42263.15;
  carac_sections[836][1] = 0.00;
  carac_sections[836][2] = 15.63;
  carac_sections[836][3] = 100.00;
  outputs[836][0] = 6.46;
  outputs[836][1] = 2.53;

  // section 837
  carac_sections[837][0] = 42303.13;
  carac_sections[837][1] = 0.00;
  carac_sections[837][2] = 15.59;
  carac_sections[837][3] = 100.00;
  outputs[837][0] = 6.49;
  outputs[837][1] = 2.51;

  // section 838
  carac_sections[838][0] = 42341.99;
  carac_sections[838][1] = 0.00;
  carac_sections[838][2] = 15.55;
  carac_sections[838][3] = 100.00;
  outputs[838][0] = 6.53;
  outputs[838][1] = 2.50;

  // section 839
  carac_sections[839][0] = 42380.99;
  carac_sections[839][1] = 0.00;
  carac_sections[839][2] = 15.51;
  carac_sections[839][3] = 100.00;
  outputs[839][0] = 6.56;
  outputs[839][1] = 2.49;

  // section 840
  carac_sections[840][0] = 42419.26;
  carac_sections[840][1] = 0.00;
  carac_sections[840][2] = 15.47;
  carac_sections[840][3] = 100.00;
  outputs[840][0] = 6.59;
  outputs[840][1] = 2.48;

  // section 841
  carac_sections[841][0] = 42457.88;
  carac_sections[841][1] = 0.00;
  carac_sections[841][2] = 15.44;
  carac_sections[841][3] = 100.00;
  outputs[841][0] = 6.62;
  outputs[841][1] = 2.47;

  // section 842
  carac_sections[842][0] = 42532.07;
  carac_sections[842][1] = 0.00;
  carac_sections[842][2] = 15.36;
  carac_sections[842][3] = 100.00;
  outputs[842][0] = 6.68;
  outputs[842][1] = 2.44;

  // section 843
  carac_sections[843][0] = 42605.36;
  carac_sections[843][1] = 0.00;
  carac_sections[843][2] = 15.29;
  carac_sections[843][3] = 100.00;
  outputs[843][0] = 6.74;
  outputs[843][1] = 2.42;

  // section 844
  carac_sections[844][0] = 42677.29;
  carac_sections[844][1] = 0.00;
  carac_sections[844][2] = 15.23;
  carac_sections[844][3] = 100.00;
  outputs[844][0] = 6.80;
  outputs[844][1] = 2.40;

  // section 845
  carac_sections[845][0] = 42748.40;
  carac_sections[845][1] = 0.00;
  carac_sections[845][2] = 15.16;
  carac_sections[845][3] = 100.00;
  outputs[845][0] = 6.86;
  outputs[845][1] = 2.38;

  // section 846
  carac_sections[846][0] = 42814.28;
  carac_sections[846][1] = 0.00;
  carac_sections[846][2] = 15.10;
  carac_sections[846][3] = 100.00;
  outputs[846][0] = 6.91;
  outputs[846][1] = 2.36;

  // section 847
  carac_sections[847][0] = 42878.43;
  carac_sections[847][1] = 0.00;
  carac_sections[847][2] = 15.04;
  carac_sections[847][3] = 100.00;
  outputs[847][0] = 6.96;
  outputs[847][1] = 2.35;

  // section 848
  carac_sections[848][0] = 42941.06;
  carac_sections[848][1] = 0.00;
  carac_sections[848][2] = 14.98;
  carac_sections[848][3] = 100.00;
  outputs[848][0] = 7.01;
  outputs[848][1] = 2.33;

  // section 849
  carac_sections[849][0] = 43003.09;
  carac_sections[849][1] = 0.00;
  carac_sections[849][2] = 14.93;
  carac_sections[849][3] = 100.00;
  outputs[849][0] = 7.06;
  outputs[849][1] = 2.31;

  // section 850
  carac_sections[850][0] = 43062.91;
  carac_sections[850][1] = 0.00;
  carac_sections[850][2] = 14.87;
  carac_sections[850][3] = 100.00;
  outputs[850][0] = 7.10;
  outputs[850][1] = 2.30;

  // section 851
  carac_sections[851][0] = 43122.00;
  carac_sections[851][1] = 0.00;
  carac_sections[851][2] = 14.82;
  carac_sections[851][3] = 100.00;
  outputs[851][0] = 7.15;
  outputs[851][1] = 2.29;

  // section 852
  carac_sections[852][0] = 43179.88;
  carac_sections[852][1] = 0.00;
  carac_sections[852][2] = 14.78;
  carac_sections[852][3] = 100.00;
  outputs[852][0] = 7.19;
  outputs[852][1] = 2.27;

  // section 853
  carac_sections[853][0] = 43237.60;
  carac_sections[853][1] = 0.00;
  carac_sections[853][2] = 14.73;
  carac_sections[853][3] = 100.00;
  outputs[853][0] = 7.23;
  outputs[853][1] = 2.26;

  // section 854
  carac_sections[854][0] = 43291.84;
  carac_sections[854][1] = 0.00;
  carac_sections[854][2] = 14.68;
  carac_sections[854][3] = 100.00;
  outputs[854][0] = 7.26;
  outputs[854][1] = 2.25;

  // section 855
  carac_sections[855][0] = 43345.36;
  carac_sections[855][1] = 0.00;
  carac_sections[855][2] = 14.64;
  carac_sections[855][3] = 100.00;
  outputs[855][0] = 7.30;
  outputs[855][1] = 2.24;

  // section 856
  carac_sections[856][0] = 43397.72;
  carac_sections[856][1] = 0.00;
  carac_sections[856][2] = 14.60;
  carac_sections[856][3] = 100.00;
  outputs[856][0] = 7.33;
  outputs[856][1] = 2.23;

  // section 857
  carac_sections[857][0] = 43450.01;
  carac_sections[857][1] = 0.00;
  carac_sections[857][2] = 14.56;
  carac_sections[857][3] = 100.00;
  outputs[857][0] = 7.37;
  outputs[857][1] = 2.22;

  // section 858
  carac_sections[858][0] = 43500.29;
  carac_sections[858][1] = 0.00;
  carac_sections[858][2] = 14.52;
  carac_sections[858][3] = 100.00;
  outputs[858][0] = 7.40;
  outputs[858][1] = 2.21;

  // section 859
  carac_sections[859][0] = 43550.30;
  carac_sections[859][1] = 0.00;
  carac_sections[859][2] = 14.49;
  carac_sections[859][3] = 100.00;
  outputs[859][0] = 7.43;
  outputs[859][1] = 2.20;

  // section 860
  carac_sections[860][0] = 43599.32;
  carac_sections[860][1] = 0.00;
  carac_sections[860][2] = 14.45;
  carac_sections[860][3] = 100.00;
  outputs[860][0] = 7.46;
  outputs[860][1] = 2.19;

  // section 861
  carac_sections[861][0] = 43649.30;
  carac_sections[861][1] = 0.00;
  carac_sections[861][2] = 14.42;
  carac_sections[861][3] = 100.00;
  outputs[861][0] = 7.49;
  outputs[861][1] = 2.18;

  // section 862
  carac_sections[862][0] = 43693.33;
  carac_sections[862][1] = 0.00;
  carac_sections[862][2] = 14.39;
  carac_sections[862][3] = 100.00;
  outputs[862][0] = 7.51;
  outputs[862][1] = 2.17;

  // section 863
  carac_sections[863][0] = 43737.09;
  carac_sections[863][1] = 0.00;
  carac_sections[863][2] = 14.36;
  carac_sections[863][3] = 100.00;
  outputs[863][0] = 7.54;
  outputs[863][1] = 2.17;

  // section 864
  carac_sections[864][0] = 43779.85;
  carac_sections[864][1] = 0.00;
  carac_sections[864][2] = 14.33;
  carac_sections[864][3] = 100.00;
  outputs[864][0] = 7.56;
  outputs[864][1] = 2.16;

  // section 865
  carac_sections[865][0] = 43823.07;
  carac_sections[865][1] = 0.00;
  carac_sections[865][2] = 14.30;
  carac_sections[865][3] = 100.00;
  outputs[865][0] = 7.58;
  outputs[865][1] = 2.15;

  // section 866
  carac_sections[866][0] = 43863.99;
  carac_sections[866][1] = 0.00;
  carac_sections[866][2] = 14.28;
  carac_sections[866][3] = 100.00;
  outputs[866][0] = 7.60;
  outputs[866][1] = 2.15;

  // section 867
  carac_sections[867][0] = 43905.04;
  carac_sections[867][1] = 0.00;
  carac_sections[867][2] = 14.25;
  carac_sections[867][3] = 100.00;
  outputs[867][0] = 7.62;
  outputs[867][1] = 2.14;

  // section 868
  carac_sections[868][0] = 43945.18;
  carac_sections[868][1] = 0.00;
  carac_sections[868][2] = 14.23;
  carac_sections[868][3] = 100.00;
  outputs[868][0] = 7.64;
  outputs[868][1] = 2.14;

  // section 869
  carac_sections[869][0] = 43986.39;
  carac_sections[869][1] = 0.00;
  carac_sections[869][2] = 14.21;
  carac_sections[869][3] = 100.00;
  outputs[869][0] = 7.66;
  outputs[869][1] = 2.13;

  // section 870
  carac_sections[870][0] = 44061.20;
  carac_sections[870][1] = 0.00;
  carac_sections[870][2] = 14.17;
  carac_sections[870][3] = 100.00;
  outputs[870][0] = 7.69;
  outputs[870][1] = 2.12;

  // section 871
  carac_sections[871][0] = 44135.12;
  carac_sections[871][1] = 0.00;
  carac_sections[871][2] = 14.13;
  carac_sections[871][3] = 100.00;
  outputs[871][0] = 7.72;
  outputs[871][1] = 2.11;

  // section 872
  carac_sections[872][0] = 44206.80;
  carac_sections[872][1] = 0.00;
  carac_sections[872][2] = 14.09;
  carac_sections[872][3] = 100.00;
  outputs[872][0] = 7.75;
  outputs[872][1] = 2.11;

  // section 873
  carac_sections[873][0] = 44277.33;
  carac_sections[873][1] = 0.00;
  carac_sections[873][2] = 14.06;
  carac_sections[873][3] = 100.00;
  outputs[873][0] = 7.77;
  outputs[873][1] = 2.10;

  // section 874
  carac_sections[874][0] = 44341.75;
  carac_sections[874][1] = 0.00;
  carac_sections[874][2] = 14.03;
  carac_sections[874][3] = 100.00;
  outputs[874][0] = 7.79;
  outputs[874][1] = 2.09;

  // section 875
  carac_sections[875][0] = 44404.29;
  carac_sections[875][1] = 0.00;
  carac_sections[875][2] = 14.00;
  carac_sections[875][3] = 100.00;
  outputs[875][0] = 7.81;
  outputs[875][1] = 2.09;

  // section 876
  carac_sections[876][0] = 44464.84;
  carac_sections[876][1] = 0.00;
  carac_sections[876][2] = 13.98;
  carac_sections[876][3] = 100.00;
  outputs[876][0] = 7.83;
  outputs[876][1] = 2.09;

  // section 877
  carac_sections[877][0] = 44524.92;
  carac_sections[877][1] = 0.00;
  carac_sections[877][2] = 13.96;
  carac_sections[877][3] = 100.00;
  outputs[877][0] = 7.85;
  outputs[877][1] = 2.08;

  // section 878
  carac_sections[878][0] = 44581.11;
  carac_sections[878][1] = 0.00;
  carac_sections[878][2] = 13.94;
  carac_sections[878][3] = 100.00;
  outputs[878][0] = 7.86;
  outputs[878][1] = 2.08;

  // section 879
  carac_sections[879][0] = 44636.45;
  carac_sections[879][1] = 0.00;
  carac_sections[879][2] = 13.92;
  carac_sections[879][3] = 100.00;
  outputs[879][0] = 7.87;
  outputs[879][1] = 2.07;

  // section 880
  carac_sections[880][0] = 44690.23;
  carac_sections[880][1] = 0.00;
  carac_sections[880][2] = 13.90;
  carac_sections[880][3] = 100.00;
  outputs[880][0] = 7.88;
  outputs[880][1] = 2.07;

  // section 881
  carac_sections[881][0] = 44744.39;
  carac_sections[881][1] = 0.00;
  carac_sections[881][2] = 13.89;
  carac_sections[881][3] = 100.00;
  outputs[881][0] = 7.89;
  outputs[881][1] = 2.07;

  // section 882
  carac_sections[882][0] = 44792.79;
  carac_sections[882][1] = 0.00;
  carac_sections[882][2] = 13.88;
  carac_sections[882][3] = 100.00;
  outputs[882][0] = 7.90;
  outputs[882][1] = 2.07;

  // section 883
  carac_sections[883][0] = 44840.66;
  carac_sections[883][1] = 0.00;
  carac_sections[883][2] = 13.86;
  carac_sections[883][3] = 100.00;
  outputs[883][0] = 7.90;
  outputs[883][1] = 2.07;

  // section 884
  carac_sections[884][0] = 44887.02;
  carac_sections[884][1] = 0.00;
  carac_sections[884][2] = 13.85;
  carac_sections[884][3] = 100.00;
  outputs[884][0] = 7.91;
  outputs[884][1] = 2.07;

  // section 885
  carac_sections[885][0] = 44933.92;
  carac_sections[885][1] = 0.00;
  carac_sections[885][2] = 13.84;
  carac_sections[885][3] = 100.00;
  outputs[885][0] = 7.91;
  outputs[885][1] = 2.06;

  // section 886
  carac_sections[886][0] = 44977.20;
  carac_sections[886][1] = 0.00;
  carac_sections[886][2] = 13.84;
  carac_sections[886][3] = 100.00;
  outputs[886][0] = 7.91;
  outputs[886][1] = 2.06;

  // section 887
  carac_sections[887][0] = 45020.48;
  carac_sections[887][1] = 0.00;
  carac_sections[887][2] = 13.83;
  carac_sections[887][3] = 100.00;
  outputs[887][0] = 7.91;
  outputs[887][1] = 2.06;

  // section 888
  carac_sections[888][0] = 45062.45;
  carac_sections[888][1] = 0.00;
  carac_sections[888][2] = 13.82;
  carac_sections[888][3] = 100.00;
  outputs[888][0] = 7.92;
  outputs[888][1] = 2.06;

  // section 889
  carac_sections[889][0] = 45105.98;
  carac_sections[889][1] = 0.00;
  carac_sections[889][2] = 13.81;
  carac_sections[889][3] = 100.00;
  outputs[889][0] = 7.92;
  outputs[889][1] = 2.06;

  // section 890
  carac_sections[890][0] = 45180.84;
  carac_sections[890][1] = 0.00;
  carac_sections[890][2] = 13.81;
  carac_sections[890][3] = 100.00;
  outputs[890][0] = 7.92;
  outputs[890][1] = 2.06;

  // section 891
  carac_sections[891][0] = 45254.48;
  carac_sections[891][1] = 0.00;
  carac_sections[891][2] = 13.80;
  carac_sections[891][3] = 100.00;
  outputs[891][0] = 7.91;
  outputs[891][1] = 2.06;

  // section 892
  carac_sections[892][0] = 45324.34;
  carac_sections[892][1] = 0.00;
  carac_sections[892][2] = 13.80;
  carac_sections[892][3] = 100.00;
  outputs[892][0] = 7.91;
  outputs[892][1] = 2.06;

  // section 893
  carac_sections[893][0] = 45392.55;
  carac_sections[893][1] = 0.00;
  carac_sections[893][2] = 13.79;
  carac_sections[893][3] = 100.00;
  outputs[893][0] = 7.90;
  outputs[893][1] = 2.07;

  // section 894
  carac_sections[894][0] = 45454.63;
  carac_sections[894][1] = 0.00;
  carac_sections[894][2] = 13.79;
  carac_sections[894][3] = 100.00;
  outputs[894][0] = 7.89;
  outputs[894][1] = 2.07;

  // section 895
  carac_sections[895][0] = 45514.98;
  carac_sections[895][1] = 0.00;
  carac_sections[895][2] = 13.79;
  carac_sections[895][3] = 100.00;
  outputs[895][0] = 7.88;
  outputs[895][1] = 2.07;

  // section 896
  carac_sections[896][0] = 45572.16;
  carac_sections[896][1] = 0.00;
  carac_sections[896][2] = 13.79;
  carac_sections[896][3] = 100.00;
  outputs[896][0] = 7.87;
  outputs[896][1] = 2.07;

  // section 897
  carac_sections[897][0] = 45629.04;
  carac_sections[897][1] = 0.00;
  carac_sections[897][2] = 13.80;
  carac_sections[897][3] = 100.00;
  outputs[897][0] = 7.86;
  outputs[897][1] = 2.08;

  // section 898
  carac_sections[898][0] = 45680.05;
  carac_sections[898][1] = 0.00;
  carac_sections[898][2] = 13.80;
  carac_sections[898][3] = 100.00;
  outputs[898][0] = 7.85;
  outputs[898][1] = 2.08;

  // section 899
  carac_sections[899][0] = 45730.46;
  carac_sections[899][1] = 0.00;
  carac_sections[899][2] = 13.81;
  carac_sections[899][3] = 100.00;
  outputs[899][0] = 7.84;
  outputs[899][1] = 2.08;

  // section 900
  carac_sections[900][0] = 45778.03;
  carac_sections[900][1] = 0.00;
  carac_sections[900][2] = 13.81;
  carac_sections[900][3] = 100.00;
  outputs[900][0] = 7.83;
  outputs[900][1] = 2.09;

  // section 901
  carac_sections[901][0] = 45826.30;
  carac_sections[901][1] = 0.00;
  carac_sections[901][2] = 13.82;
  carac_sections[901][3] = 100.00;
  outputs[901][0] = 7.81;
  outputs[901][1] = 2.09;

  // section 902
  carac_sections[902][0] = 45869.03;
  carac_sections[902][1] = 0.00;
  carac_sections[902][2] = 13.82;
  carac_sections[902][3] = 100.00;
  outputs[902][0] = 7.80;
  outputs[902][1] = 2.09;

  // section 903
  carac_sections[903][0] = 45912.00;
  carac_sections[903][1] = 0.00;
  carac_sections[903][2] = 13.83;
  carac_sections[903][3] = 100.00;
  outputs[903][0] = 7.79;
  outputs[903][1] = 2.10;

  // section 904
  carac_sections[904][0] = 45952.42;
  carac_sections[904][1] = 0.00;
  carac_sections[904][2] = 13.84;
  carac_sections[904][3] = 100.00;
  outputs[904][0] = 7.77;
  outputs[904][1] = 2.10;

  // section 905
  carac_sections[905][0] = 45993.84;
  carac_sections[905][1] = 0.00;
  carac_sections[905][2] = 13.85;
  carac_sections[905][3] = 100.00;
  outputs[905][0] = 7.76;
  outputs[905][1] = 2.10;

  // section 906
  carac_sections[906][0] = 46066.98;
  carac_sections[906][1] = 0.00;
  carac_sections[906][2] = 13.86;
  carac_sections[906][3] = 100.00;
  outputs[906][0] = 7.73;
  outputs[906][1] = 2.11;

  // section 907
  carac_sections[907][0] = 46137.64;
  carac_sections[907][1] = 0.00;
  carac_sections[907][2] = 13.88;
  carac_sections[907][3] = 100.00;
  outputs[907][0] = 7.71;
  outputs[907][1] = 2.12;

  // section 908
  carac_sections[908][0] = 46202.60;
  carac_sections[908][1] = 0.00;
  carac_sections[908][2] = 13.89;
  carac_sections[908][3] = 100.00;
  outputs[908][0] = 7.68;
  outputs[908][1] = 2.13;

  // section 909
  carac_sections[909][0] = 46265.12;
  carac_sections[909][1] = 0.00;
  carac_sections[909][2] = 13.91;
  carac_sections[909][3] = 100.00;
  outputs[909][0] = 7.65;
  outputs[909][1] = 2.13;

  // section 910
  carac_sections[910][0] = 46322.34;
  carac_sections[910][1] = 0.00;
  carac_sections[910][2] = 13.93;
  carac_sections[910][3] = 100.00;
  outputs[910][0] = 7.62;
  outputs[910][1] = 2.14;

  // section 911
  carac_sections[911][0] = 46378.04;
  carac_sections[911][1] = 0.00;
  carac_sections[911][2] = 13.94;
  carac_sections[911][3] = 100.00;
  outputs[911][0] = 7.60;
  outputs[911][1] = 2.15;

  // section 912
  carac_sections[912][0] = 46429.09;
  carac_sections[912][1] = 0.00;
  carac_sections[912][2] = 13.96;
  carac_sections[912][3] = 100.00;
  outputs[912][0] = 7.57;
  outputs[912][1] = 2.16;

  // section 913
  carac_sections[913][0] = 46479.61;
  carac_sections[913][1] = 0.00;
  carac_sections[913][2] = 13.98;
  carac_sections[913][3] = 100.00;
  outputs[913][0] = 7.55;
  outputs[913][1] = 2.16;

  // section 914
  carac_sections[914][0] = 46525.75;
  carac_sections[914][1] = 0.00;
  carac_sections[914][2] = 13.99;
  carac_sections[914][3] = 100.00;
  outputs[914][0] = 7.52;
  outputs[914][1] = 2.17;

  // section 915
  carac_sections[915][0] = 46571.85;
  carac_sections[915][1] = 0.00;
  carac_sections[915][2] = 14.01;
  carac_sections[915][3] = 100.00;
  outputs[915][0] = 7.50;
  outputs[915][1] = 2.18;

  // section 916
  carac_sections[916][0] = 46614.16;
  carac_sections[916][1] = 0.00;
  carac_sections[916][2] = 14.03;
  carac_sections[916][3] = 100.00;
  outputs[916][0] = 7.47;
  outputs[916][1] = 2.19;

  // section 917
  carac_sections[917][0] = 46656.98;
  carac_sections[917][1] = 0.00;
  carac_sections[917][2] = 14.04;
  carac_sections[917][3] = 100.00;
  outputs[917][0] = 7.45;
  outputs[917][1] = 2.19;

  // section 918
  carac_sections[918][0] = 46696.46;
  carac_sections[918][1] = 0.00;
  carac_sections[918][2] = 14.06;
  carac_sections[918][3] = 100.00;
  outputs[918][0] = 7.42;
  outputs[918][1] = 2.20;

  // section 919
  carac_sections[919][0] = 46736.54;
  carac_sections[919][1] = 0.00;
  carac_sections[919][2] = 14.07;
  carac_sections[919][3] = 100.00;
  outputs[919][0] = 7.40;
  outputs[919][1] = 2.21;

  // section 920
  carac_sections[920][0] = 46774.18;
  carac_sections[920][1] = 0.00;
  carac_sections[920][2] = 14.09;
  carac_sections[920][3] = 100.00;
  outputs[920][0] = 7.38;
  outputs[920][1] = 2.21;

  // section 921
  carac_sections[921][0] = 46811.87;
  carac_sections[921][1] = 0.00;
  carac_sections[921][2] = 14.11;
  carac_sections[921][3] = 100.00;
  outputs[921][0] = 7.35;
  outputs[921][1] = 2.22;

  // section 922
  carac_sections[922][0] = 46885.91;
  carac_sections[922][1] = 0.00;
  carac_sections[922][2] = 14.14;
  carac_sections[922][3] = 100.00;
  outputs[922][0] = 7.31;
  outputs[922][1] = 2.23;

  // section 923
  carac_sections[923][0] = 46921.93;
  carac_sections[923][1] = 0.00;
  carac_sections[923][2] = 14.15;
  carac_sections[923][3] = 100.00;
  outputs[923][0] = 7.28;
  outputs[923][1] = 2.24;

  // section 924
  carac_sections[924][0] = 46957.87;
  carac_sections[924][1] = 0.00;
  carac_sections[924][2] = 14.17;
  carac_sections[924][3] = 100.00;
  outputs[924][0] = 7.26;
  outputs[924][1] = 2.25;

  // section 925
  carac_sections[925][0] = 46993.99;
  carac_sections[925][1] = 0.00;
  carac_sections[925][2] = 14.19;
  carac_sections[925][3] = 100.00;
  outputs[925][0] = 7.23;
  outputs[925][1] = 2.26;

  // section 926
  carac_sections[926][0] = 47030.55;
  carac_sections[926][1] = 0.00;
  carac_sections[926][2] = 14.20;
  carac_sections[926][3] = 100.00;
  outputs[926][0] = 7.21;
  outputs[926][1] = 2.27;

  // section 927
  carac_sections[927][0] = 47067.07;
  carac_sections[927][1] = 0.00;
  carac_sections[927][2] = 14.22;
  carac_sections[927][3] = 100.00;
  outputs[927][0] = 7.18;
  outputs[927][1] = 2.27;

  // section 928
  carac_sections[928][0] = 47105.00;
  carac_sections[928][1] = 0.00;
  carac_sections[928][2] = 14.24;
  carac_sections[928][3] = 100.00;
  outputs[928][0] = 7.15;
  outputs[928][1] = 2.28;

  // section 929
  carac_sections[929][0] = 47143.28;
  carac_sections[929][1] = 0.00;
  carac_sections[929][2] = 14.26;
  carac_sections[929][3] = 100.00;
  outputs[929][0] = 7.13;
  outputs[929][1] = 2.29;

  // section 930
  carac_sections[930][0] = 47183.88;
  carac_sections[930][1] = 0.00;
  carac_sections[930][2] = 14.28;
  carac_sections[930][3] = 100.00;
  outputs[930][0] = 7.10;
  outputs[930][1] = 2.30;

  // section 931
  carac_sections[931][0] = 47225.47;
  carac_sections[931][1] = 0.00;
  carac_sections[931][2] = 14.30;
  carac_sections[931][3] = 100.00;
  outputs[931][0] = 7.07;
  outputs[931][1] = 2.31;

  // section 932
  carac_sections[932][0] = 47270.69;
  carac_sections[932][1] = 0.00;
  carac_sections[932][2] = 14.32;
  carac_sections[932][3] = 100.00;
  outputs[932][0] = 7.03;
  outputs[932][1] = 2.32;

  // section 933
  carac_sections[933][0] = 47318.17;
  carac_sections[933][1] = 0.00;
  carac_sections[933][2] = 14.34;
  carac_sections[933][3] = 100.00;
  outputs[933][0] = 7.00;
  outputs[933][1] = 2.33;

  // section 934
  carac_sections[934][0] = 47370.89;
  carac_sections[934][1] = 0.00;
  carac_sections[934][2] = 14.37;
  carac_sections[934][3] = 100.00;
  outputs[934][0] = 6.96;
  outputs[934][1] = 2.35;

  // section 935
  carac_sections[935][0] = 47431.75;
  carac_sections[935][1] = 0.00;
  carac_sections[935][2] = 14.40;
  carac_sections[935][3] = 100.00;
  outputs[935][0] = 6.92;
  outputs[935][1] = 2.36;

  // section 936
  carac_sections[936][0] = 47461.52;
  carac_sections[936][1] = 0.00;
  carac_sections[936][2] = 14.42;
  carac_sections[936][3] = 100.00;
  outputs[936][0] = 6.89;
  outputs[936][1] = 2.37;

  // section 937
  carac_sections[937][0] = 47494.55;
  carac_sections[937][1] = 0.00;
  carac_sections[937][2] = 14.43;
  carac_sections[937][3] = 100.00;
  outputs[937][0] = 6.86;
  outputs[937][1] = 2.38;

  // section 938
  carac_sections[938][0] = 47531.89;
  carac_sections[938][1] = 0.00;
  carac_sections[938][2] = 14.45;
  carac_sections[938][3] = 100.00;
  outputs[938][0] = 6.84;
  outputs[938][1] = 2.39;

  // section 939
  carac_sections[939][0] = 47570.47;
  carac_sections[939][1] = 0.00;
  carac_sections[939][2] = 14.47;
  carac_sections[939][3] = 100.00;
  outputs[939][0] = 6.81;
  outputs[939][1] = 2.40;

  // section 940
  carac_sections[940][0] = 47615.71;
  carac_sections[940][1] = 0.00;
  carac_sections[940][2] = 14.49;
  carac_sections[940][3] = 100.00;
  outputs[940][0] = 6.77;
  outputs[940][1] = 2.41;

  // section 941
  carac_sections[941][0] = 47664.33;
  carac_sections[941][1] = 0.00;
  carac_sections[941][2] = 14.52;
  carac_sections[941][3] = 100.00;
  outputs[941][0] = 6.73;
  outputs[941][1] = 2.42;

  // section 942
  carac_sections[942][0] = 47718.16;
  carac_sections[942][1] = 0.00;
  carac_sections[942][2] = 14.54;
  carac_sections[942][3] = 100.00;
  outputs[942][0] = 6.69;
  outputs[942][1] = 2.44;

  // section 943
  carac_sections[943][0] = 47781.01;
  carac_sections[943][1] = 0.00;
  carac_sections[943][2] = 14.57;
  carac_sections[943][3] = 100.00;
  outputs[943][0] = 6.65;
  outputs[943][1] = 2.46;

  // section 944
  carac_sections[944][0] = 47814.79;
  carac_sections[944][1] = 0.00;
  carac_sections[944][2] = 14.59;
  carac_sections[944][3] = 100.00;
  outputs[944][0] = 6.62;
  outputs[944][1] = 2.47;

  // section 945
  carac_sections[945][0] = 47849.64;
  carac_sections[945][1] = 0.00;
  carac_sections[945][2] = 14.60;
  carac_sections[945][3] = 100.00;
  outputs[945][0] = 6.59;
  outputs[945][1] = 2.48;

  // section 946
  carac_sections[946][0] = 47892.89;
  carac_sections[946][1] = 0.00;
  carac_sections[946][2] = 14.62;
  carac_sections[946][3] = 100.00;
  outputs[946][0] = 6.56;
  outputs[946][1] = 2.49;

  // section 947
  carac_sections[947][0] = 47936.22;
  carac_sections[947][1] = 0.00;
  carac_sections[947][2] = 14.64;
  carac_sections[947][3] = 100.00;
  outputs[947][0] = 6.53;
  outputs[947][1] = 2.50;

  // section 948
  carac_sections[948][0] = 47984.84;
  carac_sections[948][1] = 0.00;
  carac_sections[948][2] = 14.66;
  carac_sections[948][3] = 100.00;
  outputs[948][0] = 6.49;
  outputs[948][1] = 2.52;

  // section 949
  carac_sections[949][0] = 48036.86;
  carac_sections[949][1] = 0.00;
  carac_sections[949][2] = 14.68;
  carac_sections[949][3] = 100.00;
  outputs[949][0] = 6.45;
  outputs[949][1] = 2.53;

  // section 950
  carac_sections[950][0] = 48096.26;
  carac_sections[950][1] = 0.00;
  carac_sections[950][2] = 14.71;
  carac_sections[950][3] = 100.00;
  outputs[950][0] = 6.41;
  outputs[950][1] = 2.55;

  // section 951
  carac_sections[951][0] = 48162.02;
  carac_sections[951][1] = 0.00;
  carac_sections[951][2] = 14.73;
  carac_sections[951][3] = 100.00;
  outputs[951][0] = 6.37;
  outputs[951][1] = 2.56;

  // section 952
  carac_sections[952][0] = 48236.54;
  carac_sections[952][1] = 0.00;
  carac_sections[952][2] = 14.75;
  carac_sections[952][3] = 100.00;
  outputs[952][0] = 6.32;
  outputs[952][1] = 2.58;

  // section 953
  carac_sections[953][0] = 48279.80;
  carac_sections[953][1] = 0.00;
  carac_sections[953][2] = 14.77;
  carac_sections[953][3] = 100.00;
  outputs[953][0] = 6.29;
  outputs[953][1] = 2.59;

  // section 954
  carac_sections[954][0] = 48323.28;
  carac_sections[954][1] = 0.00;
  carac_sections[954][2] = 14.78;
  carac_sections[954][3] = 100.00;
  outputs[954][0] = 6.27;
  outputs[954][1] = 2.61;

  // section 955
  carac_sections[955][0] = 48374.12;
  carac_sections[955][1] = 0.00;
  carac_sections[955][2] = 14.79;
  carac_sections[955][3] = 100.00;
  outputs[955][0] = 6.24;
  outputs[955][1] = 2.62;

  // section 956
  carac_sections[956][0] = 48426.68;
  carac_sections[956][1] = 0.00;
  carac_sections[956][2] = 14.80;
  carac_sections[956][3] = 100.00;
  outputs[956][0] = 6.21;
  outputs[956][1] = 2.63;

  // section 957
  carac_sections[957][0] = 48484.91;
  carac_sections[957][1] = 0.00;
  carac_sections[957][2] = 14.81;
  carac_sections[957][3] = 100.00;
  outputs[957][0] = 6.18;
  outputs[957][1] = 2.64;

  // section 958
  carac_sections[958][0] = 48546.12;
  carac_sections[958][1] = 0.00;
  carac_sections[958][2] = 14.81;
  carac_sections[958][3] = 100.00;
  outputs[958][0] = 6.16;
  outputs[958][1] = 2.65;

  // section 959
  carac_sections[959][0] = 48611.47;
  carac_sections[959][1] = 0.00;
  carac_sections[959][2] = 14.82;
  carac_sections[959][3] = 100.00;
  outputs[959][0] = 6.13;
  outputs[959][1] = 2.66;

  // section 960
  carac_sections[960][0] = 48681.08;
  carac_sections[960][1] = 0.00;
  carac_sections[960][2] = 14.81;
  carac_sections[960][3] = 100.00;
  outputs[960][0] = 6.11;
  outputs[960][1] = 2.67;

  // section 961
  carac_sections[961][0] = 48755.42;
  carac_sections[961][1] = 0.00;
  carac_sections[961][2] = 14.81;
  carac_sections[961][3] = 100.00;
  outputs[961][0] = 6.09;
  outputs[961][1] = 2.68;

  // section 962
  carac_sections[962][0] = 48795.68;
  carac_sections[962][1] = 0.00;
  carac_sections[962][2] = 14.80;
  carac_sections[962][3] = 100.00;
  outputs[962][0] = 6.08;
  outputs[962][1] = 2.68;

  // section 963
  carac_sections[963][0] = 48835.42;
  carac_sections[963][1] = 0.00;
  carac_sections[963][2] = 14.80;
  carac_sections[963][3] = 100.00;
  outputs[963][0] = 6.08;
  outputs[963][1] = 2.69;

  // section 964
  carac_sections[964][0] = 48880.09;
  carac_sections[964][1] = 0.00;
  carac_sections[964][2] = 14.79;
  carac_sections[964][3] = 100.00;
  outputs[964][0] = 6.07;
  outputs[964][1] = 2.69;

  // section 965
  carac_sections[965][0] = 48924.14;
  carac_sections[965][1] = 0.00;
  carac_sections[965][2] = 14.78;
  carac_sections[965][3] = 100.00;
  outputs[965][0] = 6.07;
  outputs[965][1] = 2.69;

  // section 966
  carac_sections[966][0] = 48970.60;
  carac_sections[966][1] = 0.00;
  carac_sections[966][2] = 14.76;
  carac_sections[966][3] = 100.00;
  outputs[966][0] = 6.07;
  outputs[966][1] = 2.69;

  // section 967
  carac_sections[967][0] = 49017.43;
  carac_sections[967][1] = 0.00;
  carac_sections[967][2] = 14.75;
  carac_sections[967][3] = 100.00;
  outputs[967][0] = 6.07;
  outputs[967][1] = 2.69;

  // section 968
  carac_sections[968][0] = 49067.59;
  carac_sections[968][1] = 0.00;
  carac_sections[968][2] = 14.73;
  carac_sections[968][3] = 100.00;
  outputs[968][0] = 6.07;
  outputs[968][1] = 2.69;

  // section 969
  carac_sections[969][0] = 49118.04;
  carac_sections[969][1] = 0.00;
  carac_sections[969][2] = 14.71;
  carac_sections[969][3] = 100.00;
  outputs[969][0] = 6.08;
  outputs[969][1] = 2.69;

  // section 970
  carac_sections[970][0] = 49170.35;
  carac_sections[970][1] = 0.00;
  carac_sections[970][2] = 14.69;
  carac_sections[970][3] = 100.00;
  outputs[970][0] = 6.09;
  outputs[970][1] = 2.68;

  // section 971
  carac_sections[971][0] = 49223.48;
  carac_sections[971][1] = 0.00;
  carac_sections[971][2] = 14.66;
  carac_sections[971][3] = 100.00;
  outputs[971][0] = 6.10;
  outputs[971][1] = 2.68;

  // section 972
  carac_sections[972][0] = 49279.54;
  carac_sections[972][1] = 0.00;
  carac_sections[972][2] = 14.63;
  carac_sections[972][3] = 100.00;
  outputs[972][0] = 6.12;
  outputs[972][1] = 2.67;

  // section 973
  carac_sections[973][0] = 49336.07;
  carac_sections[973][1] = 0.00;
  carac_sections[973][2] = 14.60;
  carac_sections[973][3] = 100.00;
  outputs[973][0] = 6.14;
  outputs[973][1] = 2.66;

  // section 974
  carac_sections[974][0] = 49393.93;
  carac_sections[974][1] = 0.00;
  carac_sections[974][2] = 14.56;
  carac_sections[974][3] = 100.00;
  outputs[974][0] = 6.16;
  outputs[974][1] = 2.65;

  // section 975
  carac_sections[975][0] = 49452.52;
  carac_sections[975][1] = 0.00;
  carac_sections[975][2] = 14.52;
  carac_sections[975][3] = 100.00;
  outputs[975][0] = 6.18;
  outputs[975][1] = 2.64;

  // section 976
  carac_sections[976][0] = 49512.80;
  carac_sections[976][1] = 0.00;
  carac_sections[976][2] = 14.48;
  carac_sections[976][3] = 100.00;
  outputs[976][0] = 6.21;
  outputs[976][1] = 2.63;

  // section 977
  carac_sections[977][0] = 49573.53;
  carac_sections[977][1] = 0.00;
  carac_sections[977][2] = 14.43;
  carac_sections[977][3] = 100.00;
  outputs[977][0] = 6.24;
  outputs[977][1] = 2.61;

  // section 978
  carac_sections[978][0] = 49635.08;
  carac_sections[978][1] = 0.00;
  carac_sections[978][2] = 14.38;
  carac_sections[978][3] = 100.00;
  outputs[978][0] = 6.28;
  outputs[978][1] = 2.60;

  // section 979
  carac_sections[979][0] = 49697.23;
  carac_sections[979][1] = 0.00;
  carac_sections[979][2] = 14.33;
  carac_sections[979][3] = 100.00;
  outputs[979][0] = 6.32;
  outputs[979][1] = 2.58;

  // section 980
  carac_sections[980][0] = 49760.89;
  carac_sections[980][1] = 0.00;
  carac_sections[980][2] = 14.28;
  carac_sections[980][3] = 100.00;
  outputs[980][0] = 6.36;
  outputs[980][1] = 2.57;

  // section 981
  carac_sections[981][0] = 49824.68;
  carac_sections[981][1] = 0.00;
  carac_sections[981][2] = 14.22;
  carac_sections[981][3] = 100.00;
  outputs[981][0] = 6.40;
  outputs[981][1] = 2.55;

  // section 982
  carac_sections[982][0] = 49888.73;
  carac_sections[982][1] = 0.00;
  carac_sections[982][2] = 14.17;
  carac_sections[982][3] = 100.00;
  outputs[982][0] = 6.45;
  outputs[982][1] = 2.53;

  // section 983
  carac_sections[983][0] = 49952.94;
  carac_sections[983][1] = 0.00;
  carac_sections[983][2] = 14.11;
  carac_sections[983][3] = 100.00;
  outputs[983][0] = 6.50;
  outputs[983][1] = 2.51;

  // section 984
  carac_sections[984][0] = 50017.71;
  carac_sections[984][1] = 0.00;
  carac_sections[984][2] = 14.05;
  carac_sections[984][3] = 100.00;
  outputs[984][0] = 6.55;
  outputs[984][1] = 2.49;

  // section 985
  carac_sections[985][0] = 50082.45;
  carac_sections[985][1] = 0.00;
  carac_sections[985][2] = 13.99;
  carac_sections[985][3] = 100.00;
  outputs[985][0] = 6.60;
  outputs[985][1] = 2.47;

  // section 986
  carac_sections[986][0] = 50147.27;
  carac_sections[986][1] = 0.00;
  carac_sections[986][2] = 13.92;
  carac_sections[986][3] = 100.00;
  outputs[986][0] = 6.65;
  outputs[986][1] = 2.45;

  // section 987
  carac_sections[987][0] = 50212.29;
  carac_sections[987][1] = 0.00;
  carac_sections[987][2] = 13.86;
  carac_sections[987][3] = 100.00;
  outputs[987][0] = 6.71;
  outputs[987][1] = 2.43;

  // section 988
  carac_sections[988][0] = 50272.97;
  carac_sections[988][1] = 0.00;
  carac_sections[988][2] = 13.80;
  carac_sections[988][3] = 100.00;
  outputs[988][0] = 6.76;
  outputs[988][1] = 2.42;

  // section 989
  carac_sections[989][0] = 50332.51;
  carac_sections[989][1] = 0.00;
  carac_sections[989][2] = 13.74;
  carac_sections[989][3] = 100.00;
  outputs[989][0] = 6.81;
  outputs[989][1] = 2.40;

  // section 990
  carac_sections[990][0] = 50391.57;
  carac_sections[990][1] = 0.00;
  carac_sections[990][2] = 13.68;
  carac_sections[990][3] = 100.00;
  outputs[990][0] = 6.86;
  outputs[990][1] = 2.38;

  // section 991
  carac_sections[991][0] = 50450.46;
  carac_sections[991][1] = 0.00;
  carac_sections[991][2] = 13.62;
  carac_sections[991][3] = 100.00;
  outputs[991][0] = 6.91;
  outputs[991][1] = 2.36;

  // section 992
  carac_sections[992][0] = 50508.50;
  carac_sections[992][1] = 0.00;
  carac_sections[992][2] = 13.56;
  carac_sections[992][3] = 100.00;
  outputs[992][0] = 6.96;
  outputs[992][1] = 2.35;

  // section 993
  carac_sections[993][0] = 50566.24;
  carac_sections[993][1] = 0.00;
  carac_sections[993][2] = 13.51;
  carac_sections[993][3] = 100.00;
  outputs[993][0] = 7.01;
  outputs[993][1] = 2.33;

  // section 994
  carac_sections[994][0] = 50623.31;
  carac_sections[994][1] = 0.00;
  carac_sections[994][2] = 13.45;
  carac_sections[994][3] = 100.00;
  outputs[994][0] = 7.06;
  outputs[994][1] = 2.31;

  // section 995
  carac_sections[995][0] = 50680.26;
  carac_sections[995][1] = 0.00;
  carac_sections[995][2] = 13.39;
  carac_sections[995][3] = 100.00;
  outputs[995][0] = 7.11;
  outputs[995][1] = 2.30;

  // section 996
  carac_sections[996][0] = 50736.07;
  carac_sections[996][1] = 0.00;
  carac_sections[996][2] = 13.34;
  carac_sections[996][3] = 100.00;
  outputs[996][0] = 7.16;
  outputs[996][1] = 2.28;

  // section 997
  carac_sections[997][0] = 50791.61;
  carac_sections[997][1] = 0.00;
  carac_sections[997][2] = 13.28;
  carac_sections[997][3] = 100.00;
  outputs[997][0] = 7.21;
  outputs[997][1] = 2.26;

  // section 998
  carac_sections[998][0] = 50846.59;
  carac_sections[998][1] = 0.00;
  carac_sections[998][2] = 13.23;
  carac_sections[998][3] = 100.00;
  outputs[998][0] = 7.26;
  outputs[998][1] = 2.25;

  // section 999
  carac_sections[999][0] = 50901.32;
  carac_sections[999][1] = 0.00;
  carac_sections[999][2] = 13.18;
  carac_sections[999][3] = 100.00;
  outputs[999][0] = 7.30;
  outputs[999][1] = 2.24;

  // section 1000
  carac_sections[1000][0] = 50955.18;
  carac_sections[1000][1] = 0.00;
  carac_sections[1000][2] = 13.13;
  carac_sections[1000][3] = 100.00;
  outputs[1000][0] = 7.35;
  outputs[1000][1] = 2.22;

  // section 1001
  carac_sections[1001][0] = 51008.75;
  carac_sections[1001][1] = 0.00;
  carac_sections[1001][2] = 13.07;
  carac_sections[1001][3] = 100.00;
  outputs[1001][0] = 7.39;
  outputs[1001][1] = 2.21;

  // section 1002
  carac_sections[1002][0] = 51061.50;
  carac_sections[1002][1] = 0.00;
  carac_sections[1002][2] = 13.03;
  carac_sections[1002][3] = 100.00;
  outputs[1002][0] = 7.44;
  outputs[1002][1] = 2.19;

  // section 1003
  carac_sections[1003][0] = 51114.34;
  carac_sections[1003][1] = 0.00;
  carac_sections[1003][2] = 12.98;
  carac_sections[1003][3] = 100.00;
  outputs[1003][0] = 7.48;
  outputs[1003][1] = 2.18;

  // section 1004
  carac_sections[1004][0] = 51165.04;
  carac_sections[1004][1] = 0.00;
  carac_sections[1004][2] = 12.93;
  carac_sections[1004][3] = 100.00;
  outputs[1004][0] = 7.52;
  outputs[1004][1] = 2.17;

  // section 1005
  carac_sections[1005][0] = 51215.34;
  carac_sections[1005][1] = 0.00;
  carac_sections[1005][2] = 12.88;
  carac_sections[1005][3] = 100.00;
  outputs[1005][0] = 7.56;
  outputs[1005][1] = 2.16;

  // section 1006
  carac_sections[1006][0] = 51264.79;
  carac_sections[1006][1] = 0.00;
  carac_sections[1006][2] = 12.84;
  carac_sections[1006][3] = 100.00;
  outputs[1006][0] = 7.60;
  outputs[1006][1] = 2.15;

  // section 1007
  carac_sections[1007][0] = 51314.19;
  carac_sections[1007][1] = 0.00;
  carac_sections[1007][2] = 12.80;
  carac_sections[1007][3] = 100.00;
  outputs[1007][0] = 7.64;
  outputs[1007][1] = 2.14;

  // section 1008
  carac_sections[1008][0] = 51362.48;
  carac_sections[1008][1] = 0.00;
  carac_sections[1008][2] = 12.76;
  carac_sections[1008][3] = 100.00;
  outputs[1008][0] = 7.68;
  outputs[1008][1] = 2.13;

  // section 1009
  carac_sections[1009][0] = 51410.49;
  carac_sections[1009][1] = 0.00;
  carac_sections[1009][2] = 12.72;
  carac_sections[1009][3] = 100.00;
  outputs[1009][0] = 7.71;
  outputs[1009][1] = 2.12;

  // section 1010
  carac_sections[1010][0] = 51457.93;
  carac_sections[1010][1] = 0.00;
  carac_sections[1010][2] = 12.68;
  carac_sections[1010][3] = 100.00;
  outputs[1010][0] = 7.75;
  outputs[1010][1] = 2.11;

  // section 1011
  carac_sections[1011][0] = 51505.51;
  carac_sections[1011][1] = 0.00;
  carac_sections[1011][2] = 12.64;
  carac_sections[1011][3] = 100.00;
  outputs[1011][0] = 7.78;
  outputs[1011][1] = 2.10;

  // section 1012
  carac_sections[1012][0] = 51551.45;
  carac_sections[1012][1] = 0.00;
  carac_sections[1012][2] = 12.60;
  carac_sections[1012][3] = 100.00;
  outputs[1012][0] = 7.82;
  outputs[1012][1] = 2.09;

  // section 1013
  carac_sections[1013][0] = 51597.23;
  carac_sections[1013][1] = 0.00;
  carac_sections[1013][2] = 12.56;
  carac_sections[1013][3] = 100.00;
  outputs[1013][0] = 7.85;
  outputs[1013][1] = 2.08;

  // section 1014
  carac_sections[1014][0] = 51642.33;
  carac_sections[1014][1] = 0.00;
  carac_sections[1014][2] = 12.53;
  carac_sections[1014][3] = 100.00;
  outputs[1014][0] = 7.88;
  outputs[1014][1] = 2.07;

  // section 1015
  carac_sections[1015][0] = 51687.54;
  carac_sections[1015][1] = 0.00;
  carac_sections[1015][2] = 12.49;
  carac_sections[1015][3] = 100.00;
  outputs[1015][0] = 7.91;
  outputs[1015][1] = 2.06;

  // section 1016
  carac_sections[1016][0] = 51731.53;
  carac_sections[1016][1] = 0.00;
  carac_sections[1016][2] = 12.46;
  carac_sections[1016][3] = 100.00;
  outputs[1016][0] = 7.94;
  outputs[1016][1] = 2.06;

  // section 1017
  carac_sections[1017][0] = 51775.54;
  carac_sections[1017][1] = 0.00;
  carac_sections[1017][2] = 12.43;
  carac_sections[1017][3] = 100.00;
  outputs[1017][0] = 7.97;
  outputs[1017][1] = 2.05;

  // section 1018
  carac_sections[1018][0] = 51818.83;
  carac_sections[1018][1] = 0.00;
  carac_sections[1018][2] = 12.40;
  carac_sections[1018][3] = 100.00;
  outputs[1018][0] = 7.99;
  outputs[1018][1] = 2.04;

  // section 1019
  carac_sections[1019][0] = 51863.94;
  carac_sections[1019][1] = 0.00;
  carac_sections[1019][2] = 12.37;
  carac_sections[1019][3] = 100.00;
  outputs[1019][0] = 8.02;
  outputs[1019][1] = 2.04;

  // section 1020
  carac_sections[1020][0] = 51901.82;
  carac_sections[1020][1] = 0.00;
  carac_sections[1020][2] = 12.34;
  carac_sections[1020][3] = 100.00;
  outputs[1020][0] = 8.04;
  outputs[1020][1] = 2.03;

  // section 1021
  carac_sections[1021][0] = 51939.83;
  carac_sections[1021][1] = 0.00;
  carac_sections[1021][2] = 12.32;
  carac_sections[1021][3] = 100.00;
  outputs[1021][0] = 8.06;
  outputs[1021][1] = 2.02;

  // section 1022
  carac_sections[1022][0] = 52014.58;
  carac_sections[1022][1] = 0.00;
  carac_sections[1022][2] = 12.27;
  carac_sections[1022][3] = 100.00;
  outputs[1022][0] = 8.11;
  outputs[1022][1] = 2.01;

  // section 1023
  carac_sections[1023][0] = 52087.52;
  carac_sections[1023][1] = 0.00;
  carac_sections[1023][2] = 12.22;
  carac_sections[1023][3] = 100.00;
  outputs[1023][0] = 8.14;
  outputs[1023][1] = 2.00;

  // section 1024
  carac_sections[1024][0] = 52159.36;
  carac_sections[1024][1] = 0.00;
  carac_sections[1024][2] = 12.18;
  carac_sections[1024][3] = 100.00;
  outputs[1024][0] = 8.18;
  outputs[1024][1] = 2.00;

  // section 1025
  carac_sections[1025][0] = 52228.62;
  carac_sections[1025][1] = 0.00;
  carac_sections[1025][2] = 12.14;
  carac_sections[1025][3] = 100.00;
  outputs[1025][0] = 8.21;
  outputs[1025][1] = 1.99;

  // section 1026
  carac_sections[1026][0] = 52296.66;
  carac_sections[1026][1] = 0.00;
  carac_sections[1026][2] = 12.10;
  carac_sections[1026][3] = 100.00;
  outputs[1026][0] = 8.24;
  outputs[1026][1] = 1.98;

  // section 1027
  carac_sections[1027][0] = 52363.27;
  carac_sections[1027][1] = 0.00;
  carac_sections[1027][2] = 12.07;
  carac_sections[1027][3] = 100.00;
  outputs[1027][0] = 8.27;
  outputs[1027][1] = 1.97;

  // section 1028
  carac_sections[1028][0] = 52429.18;
  carac_sections[1028][1] = 0.00;
  carac_sections[1028][2] = 12.04;
  carac_sections[1028][3] = 100.00;
  outputs[1028][0] = 8.30;
  outputs[1028][1] = 1.97;

  // section 1029
  carac_sections[1029][0] = 52491.37;
  carac_sections[1029][1] = 0.00;
  carac_sections[1029][2] = 12.01;
  carac_sections[1029][3] = 100.00;
  outputs[1029][0] = 8.32;
  outputs[1029][1] = 1.96;

  // section 1030
  carac_sections[1030][0] = 52552.38;
  carac_sections[1030][1] = 0.00;
  carac_sections[1030][2] = 11.98;
  carac_sections[1030][3] = 100.00;
  outputs[1030][0] = 8.34;
  outputs[1030][1] = 1.96;

  // section 1031
  carac_sections[1031][0] = 52612.01;
  carac_sections[1031][1] = 0.00;
  carac_sections[1031][2] = 11.96;
  carac_sections[1031][3] = 100.00;
  outputs[1031][0] = 8.36;
  outputs[1031][1] = 1.95;

  // section 1032
  carac_sections[1032][0] = 52671.18;
  carac_sections[1032][1] = 0.00;
  carac_sections[1032][2] = 11.94;
  carac_sections[1032][3] = 100.00;
  outputs[1032][0] = 8.37;
  outputs[1032][1] = 1.95;

  // section 1033
  carac_sections[1033][0] = 52728.38;
  carac_sections[1033][1] = 0.00;
  carac_sections[1033][2] = 11.92;
  carac_sections[1033][3] = 100.00;
  outputs[1033][0] = 8.39;
  outputs[1033][1] = 1.95;

  // section 1034
  carac_sections[1034][0] = 52785.02;
  carac_sections[1034][1] = 0.00;
  carac_sections[1034][2] = 11.90;
  carac_sections[1034][3] = 100.00;
  outputs[1034][0] = 8.40;
  outputs[1034][1] = 1.94;

  // section 1035
  carac_sections[1035][0] = 52840.64;
  carac_sections[1035][1] = 0.00;
  carac_sections[1035][2] = 11.88;
  carac_sections[1035][3] = 100.00;
  outputs[1035][0] = 8.41;
  outputs[1035][1] = 1.94;

  // section 1036
  carac_sections[1036][0] = 52896.94;
  carac_sections[1036][1] = 0.00;
  carac_sections[1036][2] = 11.86;
  carac_sections[1036][3] = 100.00;
  outputs[1036][0] = 8.43;
  outputs[1036][1] = 1.94;

  // section 1037
  carac_sections[1037][0] = 52945.39;
  carac_sections[1037][1] = 0.00;
  carac_sections[1037][2] = 11.85;
  carac_sections[1037][3] = 100.00;
  outputs[1037][0] = 8.43;
  outputs[1037][1] = 1.94;

  // section 1038
  carac_sections[1038][0] = 52992.96;
  carac_sections[1038][1] = 0.00;
  carac_sections[1038][2] = 11.84;
  carac_sections[1038][3] = 100.00;
  outputs[1038][0] = 8.44;
  outputs[1038][1] = 1.93;

  // section 1039
  carac_sections[1039][0] = 53039.42;
  carac_sections[1039][1] = 0.00;
  carac_sections[1039][2] = 11.83;
  carac_sections[1039][3] = 100.00;
  outputs[1039][0] = 8.45;
  outputs[1039][1] = 1.93;

  // section 1040
  carac_sections[1040][0] = 53086.12;
  carac_sections[1040][1] = 0.00;
  carac_sections[1040][2] = 11.82;
  carac_sections[1040][3] = 100.00;
  outputs[1040][0] = 8.45;
  outputs[1040][1] = 1.93;

  // section 1041
  carac_sections[1041][0] = 53130.54;
  carac_sections[1041][1] = 0.00;
  carac_sections[1041][2] = 11.81;
  carac_sections[1041][3] = 100.00;
  outputs[1041][0] = 8.45;
  outputs[1041][1] = 1.93;

  // section 1042
  carac_sections[1042][0] = 53174.90;
  carac_sections[1042][1] = 0.00;
  carac_sections[1042][2] = 11.81;
  carac_sections[1042][3] = 100.00;
  outputs[1042][0] = 8.46;
  outputs[1042][1] = 1.93;

  // section 1043
  carac_sections[1043][0] = 53218.29;
  carac_sections[1043][1] = 0.00;
  carac_sections[1043][2] = 11.80;
  carac_sections[1043][3] = 100.00;
  outputs[1043][0] = 8.46;
  outputs[1043][1] = 1.93;

  // section 1044
  carac_sections[1044][0] = 53262.55;
  carac_sections[1044][1] = 0.00;
  carac_sections[1044][2] = 11.79;
  carac_sections[1044][3] = 100.00;
  outputs[1044][0] = 8.46;
  outputs[1044][1] = 1.93;

  // section 1045
  carac_sections[1045][0] = 53302.98;
  carac_sections[1045][1] = 0.00;
  carac_sections[1045][2] = 11.79;
  carac_sections[1045][3] = 100.00;
  outputs[1045][0] = 8.46;
  outputs[1045][1] = 1.93;

  // section 1046
  carac_sections[1046][0] = 53343.52;
  carac_sections[1046][1] = 0.00;
  carac_sections[1046][2] = 11.79;
  carac_sections[1046][3] = 100.00;
  outputs[1046][0] = 8.46;
  outputs[1046][1] = 1.93;

  // section 1047
  carac_sections[1047][0] = 53383.11;
  carac_sections[1047][1] = 0.00;
  carac_sections[1047][2] = 11.78;
  carac_sections[1047][3] = 100.00;
  outputs[1047][0] = 8.46;
  outputs[1047][1] = 1.93;

  // section 1048
  carac_sections[1048][0] = 53423.21;
  carac_sections[1048][1] = 0.00;
  carac_sections[1048][2] = 11.78;
  carac_sections[1048][3] = 100.00;
  outputs[1048][0] = 8.46;
  outputs[1048][1] = 1.93;

  // section 1049
  carac_sections[1049][0] = 53461.43;
  carac_sections[1049][1] = 0.00;
  carac_sections[1049][2] = 11.78;
  carac_sections[1049][3] = 100.00;
  outputs[1049][0] = 8.45;
  outputs[1049][1] = 1.93;

  // section 1050
  carac_sections[1050][0] = 53499.91;
  carac_sections[1050][1] = 0.00;
  carac_sections[1050][2] = 11.78;
  carac_sections[1050][3] = 100.00;
  outputs[1050][0] = 8.45;
  outputs[1050][1] = 1.93;

  // section 1051
  carac_sections[1051][0] = 53537.48;
  carac_sections[1051][1] = 0.00;
  carac_sections[1051][2] = 11.78;
  carac_sections[1051][3] = 100.00;
  outputs[1051][0] = 8.45;
  outputs[1051][1] = 1.93;

  // section 1052
  carac_sections[1052][0] = 53575.07;
  carac_sections[1052][1] = 0.00;
  carac_sections[1052][2] = 11.78;
  carac_sections[1052][3] = 100.00;
  outputs[1052][0] = 8.44;
  outputs[1052][1] = 1.93;

  // section 1053
  carac_sections[1053][0] = 53642.08;
  carac_sections[1053][1] = 0.00;
  carac_sections[1053][2] = 11.78;
  carac_sections[1053][3] = 100.00;
  outputs[1053][0] = 8.43;
  outputs[1053][1] = 1.94;

  // section 1054
  carac_sections[1054][0] = 53706.44;
  carac_sections[1054][1] = 0.00;
  carac_sections[1054][2] = 11.78;
  carac_sections[1054][3] = 100.00;
  outputs[1054][0] = 8.42;
  outputs[1054][1] = 1.94;

  // section 1055
  carac_sections[1055][0] = 53768.62;
  carac_sections[1055][1] = 0.00;
  carac_sections[1055][2] = 11.79;
  carac_sections[1055][3] = 100.00;
  outputs[1055][0] = 8.41;
  outputs[1055][1] = 1.94;

  // section 1056
  carac_sections[1056][0] = 53830.16;
  carac_sections[1056][1] = 0.00;
  carac_sections[1056][2] = 11.79;
  carac_sections[1056][3] = 100.00;
  outputs[1056][0] = 8.40;
  outputs[1056][1] = 1.94;

  // section 1057
  carac_sections[1057][0] = 53888.03;
  carac_sections[1057][1] = 0.00;
  carac_sections[1057][2] = 11.80;
  carac_sections[1057][3] = 100.00;
  outputs[1057][0] = 8.38;
  outputs[1057][1] = 1.95;

  // section 1058
  carac_sections[1058][0] = 53944.96;
  carac_sections[1058][1] = 0.00;
  carac_sections[1058][2] = 11.81;
  carac_sections[1058][3] = 100.00;
  outputs[1058][0] = 8.37;
  outputs[1058][1] = 1.95;

  // section 1059
  carac_sections[1059][0] = 54000.35;
  carac_sections[1059][1] = 0.00;
  carac_sections[1059][2] = 11.82;
  carac_sections[1059][3] = 100.00;
  outputs[1059][0] = 8.35;
  outputs[1059][1] = 1.96;

  // section 1060
  carac_sections[1060][0] = 54056.26;
  carac_sections[1060][1] = 0.00;
  carac_sections[1060][2] = 11.83;
  carac_sections[1060][3] = 100.00;
  outputs[1060][0] = 8.33;
  outputs[1060][1] = 1.96;

  // section 1061
  carac_sections[1061][0] = 54104.43;
  carac_sections[1061][1] = 0.00;
  carac_sections[1061][2] = 11.84;
  carac_sections[1061][3] = 100.00;
  outputs[1061][0] = 8.32;
  outputs[1061][1] = 1.96;

  // section 1062
  carac_sections[1062][0] = 54151.80;
  carac_sections[1062][1] = 0.00;
  carac_sections[1062][2] = 11.85;
  carac_sections[1062][3] = 100.00;
  outputs[1062][0] = 8.30;
  outputs[1062][1] = 1.97;

  // section 1063
  carac_sections[1063][0] = 54197.73;
  carac_sections[1063][1] = 0.00;
  carac_sections[1063][2] = 11.86;
  carac_sections[1063][3] = 100.00;
  outputs[1063][0] = 8.28;
  outputs[1063][1] = 1.97;

  // section 1064
  carac_sections[1064][0] = 54244.11;
  carac_sections[1064][1] = 0.00;
  carac_sections[1064][2] = 11.87;
  carac_sections[1064][3] = 100.00;
  outputs[1064][0] = 8.26;
  outputs[1064][1] = 1.98;

  // section 1065
  carac_sections[1065][0] = 54287.09;
  carac_sections[1065][1] = 0.00;
  carac_sections[1065][2] = 11.89;
  carac_sections[1065][3] = 100.00;
  outputs[1065][0] = 8.25;
  outputs[1065][1] = 1.98;

  // section 1066
  carac_sections[1066][0] = 54330.03;
  carac_sections[1066][1] = 0.00;
  carac_sections[1066][2] = 11.90;
  carac_sections[1066][3] = 100.00;
  outputs[1066][0] = 8.23;
  outputs[1066][1] = 1.98;

  // section 1067
  carac_sections[1067][0] = 54371.70;
  carac_sections[1067][1] = 0.00;
  carac_sections[1067][2] = 11.91;
  carac_sections[1067][3] = 100.00;
  outputs[1067][0] = 8.21;
  outputs[1067][1] = 1.99;

  // section 1068
  carac_sections[1068][0] = 54414.70;
  carac_sections[1068][1] = 0.00;
  carac_sections[1068][2] = 11.93;
  carac_sections[1068][3] = 100.00;
  outputs[1068][0] = 8.19;
  outputs[1068][1] = 1.99;

  // section 1069
  carac_sections[1069][0] = 54487.24;
  carac_sections[1069][1] = 0.00;
  carac_sections[1069][2] = 11.95;
  carac_sections[1069][3] = 100.00;
  outputs[1069][0] = 8.15;
  outputs[1069][1] = 2.00;

  // section 1070
  carac_sections[1070][0] = 54557.92;
  carac_sections[1070][1] = 0.00;
  carac_sections[1070][2] = 11.98;
  carac_sections[1070][3] = 100.00;
  outputs[1070][0] = 8.12;
  outputs[1070][1] = 2.01;

  // section 1071
  carac_sections[1071][0] = 54625.28;
  carac_sections[1071][1] = 0.00;
  carac_sections[1071][2] = 12.00;
  carac_sections[1071][3] = 100.00;
  outputs[1071][0] = 8.08;
  outputs[1071][1] = 2.02;

  // section 1072
  carac_sections[1072][0] = 54691.26;
  carac_sections[1072][1] = 0.00;
  carac_sections[1072][2] = 12.03;
  carac_sections[1072][3] = 100.00;
  outputs[1072][0] = 8.04;
  outputs[1072][1] = 2.03;

  // section 1073
  carac_sections[1073][0] = 54749.48;
  carac_sections[1073][1] = 0.00;
  carac_sections[1073][2] = 12.06;
  carac_sections[1073][3] = 100.00;
  outputs[1073][0] = 8.01;
  outputs[1073][1] = 2.04;

  // section 1074
  carac_sections[1074][0] = 54806.01;
  carac_sections[1074][1] = 0.00;
  carac_sections[1074][2] = 12.08;
  carac_sections[1074][3] = 100.00;
  outputs[1074][0] = 7.97;
  outputs[1074][1] = 2.05;

  // section 1075
  carac_sections[1075][0] = 54859.49;
  carac_sections[1075][1] = 0.00;
  carac_sections[1075][2] = 12.11;
  carac_sections[1075][3] = 100.00;
  outputs[1075][0] = 7.94;
  outputs[1075][1] = 2.06;

  // section 1076
  carac_sections[1076][0] = 54913.43;
  carac_sections[1076][1] = 0.00;
  carac_sections[1076][2] = 12.13;
  carac_sections[1076][3] = 100.00;
  outputs[1076][0] = 7.91;
  outputs[1076][1] = 2.07;

  // section 1077
  carac_sections[1077][0] = 54959.80;
  carac_sections[1077][1] = 0.00;
  carac_sections[1077][2] = 12.16;
  carac_sections[1077][3] = 100.00;
  outputs[1077][0] = 7.87;
  outputs[1077][1] = 2.07;

  // section 1078
  carac_sections[1078][0] = 55006.02;
  carac_sections[1078][1] = 0.00;
  carac_sections[1078][2] = 12.18;
  carac_sections[1078][3] = 100.00;
  outputs[1078][0] = 7.84;
  outputs[1078][1] = 2.08;

  // section 1079
  carac_sections[1079][0] = 55049.93;
  carac_sections[1079][1] = 0.00;
  carac_sections[1079][2] = 12.21;
  carac_sections[1079][3] = 100.00;
  outputs[1079][0] = 7.81;
  outputs[1079][1] = 2.09;

  // section 1080
  carac_sections[1080][0] = 55094.91;
  carac_sections[1080][1] = 0.00;
  carac_sections[1080][2] = 12.23;
  carac_sections[1080][3] = 100.00;
  outputs[1080][0] = 7.78;
  outputs[1080][1] = 2.10;

  // section 1081
  carac_sections[1081][0] = 55132.94;
  carac_sections[1081][1] = 0.00;
  carac_sections[1081][2] = 12.25;
  carac_sections[1081][3] = 100.00;
  outputs[1081][0] = 7.75;
  outputs[1081][1] = 2.11;

  // section 1082
  carac_sections[1082][0] = 55171.07;
  carac_sections[1082][1] = 0.00;
  carac_sections[1082][2] = 12.27;
  carac_sections[1082][3] = 100.00;
  outputs[1082][0] = 7.72;
  outputs[1082][1] = 2.12;

  // section 1083
  carac_sections[1083][0] = 55244.79;
  carac_sections[1083][1] = 0.00;
  carac_sections[1083][2] = 12.32;
  carac_sections[1083][3] = 100.00;
  outputs[1083][0] = 7.66;
  outputs[1083][1] = 2.13;

  // section 1084
  carac_sections[1084][0] = 55311.85;
  carac_sections[1084][1] = 0.00;
  carac_sections[1084][2] = 12.36;
  carac_sections[1084][3] = 100.00;
  outputs[1084][0] = 7.61;
  outputs[1084][1] = 2.15;

  // section 1085
  carac_sections[1085][0] = 55376.13;
  carac_sections[1085][1] = 0.00;
  carac_sections[1085][2] = 12.40;
  carac_sections[1085][3] = 100.00;
  outputs[1085][0] = 7.56;
  outputs[1085][1] = 2.16;

  // section 1086
  carac_sections[1086][0] = 55435.21;
  carac_sections[1086][1] = 0.00;
  carac_sections[1086][2] = 12.43;
  carac_sections[1086][3] = 100.00;
  outputs[1086][0] = 7.51;
  outputs[1086][1] = 2.17;

  // section 1087
  carac_sections[1087][0] = 55491.53;
  carac_sections[1087][1] = 0.00;
  carac_sections[1087][2] = 12.47;
  carac_sections[1087][3] = 100.00;
  outputs[1087][0] = 7.46;
  outputs[1087][1] = 2.19;

  // section 1088
  carac_sections[1088][0] = 55544.46;
  carac_sections[1088][1] = 0.00;
  carac_sections[1088][2] = 12.51;
  carac_sections[1088][3] = 100.00;
  outputs[1088][0] = 7.41;
  outputs[1088][1] = 2.20;

  // section 1089
  carac_sections[1089][0] = 55594.94;
  carac_sections[1089][1] = 0.00;
  carac_sections[1089][2] = 12.54;
  carac_sections[1089][3] = 100.00;
  outputs[1089][0] = 7.37;
  outputs[1089][1] = 2.22;

  // section 1090
  carac_sections[1090][0] = 55642.80;
  carac_sections[1090][1] = 0.00;
  carac_sections[1090][2] = 12.57;
  carac_sections[1090][3] = 100.00;
  outputs[1090][0] = 7.32;
  outputs[1090][1] = 2.23;

  // section 1091
  carac_sections[1091][0] = 55689.15;
  carac_sections[1091][1] = 0.00;
  carac_sections[1091][2] = 12.60;
  carac_sections[1091][3] = 100.00;
  outputs[1091][0] = 7.28;
  outputs[1091][1] = 2.24;

  // section 1092
  carac_sections[1092][0] = 55734.02;
  carac_sections[1092][1] = 0.00;
  carac_sections[1092][2] = 12.64;
  carac_sections[1092][3] = 100.00;
  outputs[1092][0] = 7.24;
  outputs[1092][1] = 2.26;

  // section 1093
  carac_sections[1093][0] = 55778.71;
  carac_sections[1093][1] = 0.00;
  carac_sections[1093][2] = 12.67;
  carac_sections[1093][3] = 100.00;
  outputs[1093][0] = 7.20;
  outputs[1093][1] = 2.27;

  // section 1094
  carac_sections[1094][0] = 55822.11;
  carac_sections[1094][1] = 0.00;
  carac_sections[1094][2] = 12.70;
  carac_sections[1094][3] = 100.00;
  outputs[1094][0] = 7.15;
  outputs[1094][1] = 2.28;

  // section 1095
  carac_sections[1095][0] = 55866.18;
  carac_sections[1095][1] = 0.00;
  carac_sections[1095][2] = 12.73;
  carac_sections[1095][3] = 100.00;
  outputs[1095][0] = 7.11;
  outputs[1095][1] = 2.30;

  // section 1096
  carac_sections[1096][0] = 55909.76;
  carac_sections[1096][1] = 0.00;
  carac_sections[1096][2] = 12.76;
  carac_sections[1096][3] = 100.00;
  outputs[1096][0] = 7.07;
  outputs[1096][1] = 2.31;

  // section 1097
  carac_sections[1097][0] = 55956.19;
  carac_sections[1097][1] = 0.00;
  carac_sections[1097][2] = 12.80;
  carac_sections[1097][3] = 100.00;
  outputs[1097][0] = 7.02;
  outputs[1097][1] = 2.33;

  // section 1098
  carac_sections[1098][0] = 56003.36;
  carac_sections[1098][1] = 0.00;
  carac_sections[1098][2] = 12.83;
  carac_sections[1098][3] = 100.00;
  outputs[1098][0] = 6.97;
  outputs[1098][1] = 2.34;

  // section 1099
  carac_sections[1099][0] = 56054.07;
  carac_sections[1099][1] = 0.00;
  carac_sections[1099][2] = 12.87;
  carac_sections[1099][3] = 100.00;
  outputs[1099][0] = 6.92;
  outputs[1099][1] = 2.36;

  // section 1100
  carac_sections[1100][0] = 56113.50;
  carac_sections[1100][1] = 0.00;
  carac_sections[1100][2] = 12.92;
  carac_sections[1100][3] = 100.00;
  outputs[1100][0] = 6.86;
  outputs[1100][1] = 2.38;

  // section 1101
  carac_sections[1101][0] = 56140.70;
  carac_sections[1101][1] = 0.00;
  carac_sections[1101][2] = 12.94;
  carac_sections[1101][3] = 100.00;
  outputs[1101][0] = 6.83;
  outputs[1101][1] = 2.39;

  // section 1102
  carac_sections[1102][0] = 56173.00;
  carac_sections[1102][1] = 0.00;
  carac_sections[1102][2] = 12.96;
  carac_sections[1102][3] = 100.00;
  outputs[1102][0] = 6.79;
  outputs[1102][1] = 2.40;

  // section 1103
  carac_sections[1103][0] = 56207.49;
  carac_sections[1103][1] = 0.00;
  carac_sections[1103][2] = 12.99;
  carac_sections[1103][3] = 100.00;
  outputs[1103][0] = 6.76;
  outputs[1103][1] = 2.42;

  // section 1104
  carac_sections[1104][0] = 56244.69;
  carac_sections[1104][1] = 0.00;
  carac_sections[1104][2] = 13.02;
  carac_sections[1104][3] = 100.00;
  outputs[1104][0] = 6.72;
  outputs[1104][1] = 2.43;

  // section 1105
  carac_sections[1105][0] = 56287.68;
  carac_sections[1105][1] = 0.00;
  carac_sections[1105][2] = 13.05;
  carac_sections[1105][3] = 100.00;
  outputs[1105][0] = 6.67;
  outputs[1105][1] = 2.45;

  // section 1106
  carac_sections[1106][0] = 56337.46;
  carac_sections[1106][1] = 0.00;
  carac_sections[1106][2] = 13.09;
  carac_sections[1106][3] = 100.00;
  outputs[1106][0] = 6.61;
  outputs[1106][1] = 2.47;

  // section 1107
  carac_sections[1107][0] = 56396.26;
  carac_sections[1107][1] = 0.00;
  carac_sections[1107][2] = 13.14;
  carac_sections[1107][3] = 100.00;
  outputs[1107][0] = 6.55;
  outputs[1107][1] = 2.49;

  // section 1108
  carac_sections[1108][0] = 56425.15;
  carac_sections[1108][1] = 0.00;
  carac_sections[1108][2] = 13.16;
  carac_sections[1108][3] = 100.00;
  outputs[1108][0] = 6.51;
  outputs[1108][1] = 2.51;

  // section 1109
  carac_sections[1109][0] = 56457.77;
  carac_sections[1109][1] = 0.00;
  carac_sections[1109][2] = 13.19;
  carac_sections[1109][3] = 100.00;
  outputs[1109][0] = 6.48;
  outputs[1109][1] = 2.52;

  // section 1110
  carac_sections[1110][0] = 56497.86;
  carac_sections[1110][1] = 0.00;
  carac_sections[1110][2] = 13.22;
  carac_sections[1110][3] = 100.00;
  outputs[1110][0] = 6.43;
  outputs[1110][1] = 2.54;

  // section 1111
  carac_sections[1111][0] = 56541.02;
  carac_sections[1111][1] = 0.00;
  carac_sections[1111][2] = 13.25;
  carac_sections[1111][3] = 100.00;
  outputs[1111][0] = 6.38;
  outputs[1111][1] = 2.56;

  // section 1112
  carac_sections[1112][0] = 56593.60;
  carac_sections[1112][1] = 0.00;
  carac_sections[1112][2] = 13.29;
  carac_sections[1112][3] = 100.00;
  outputs[1112][0] = 6.32;
  outputs[1112][1] = 2.59;

  // section 1113
  carac_sections[1113][0] = 56654.35;
  carac_sections[1113][1] = 0.00;
  carac_sections[1113][2] = 13.34;
  carac_sections[1113][3] = 100.00;
  outputs[1113][0] = 6.24;
  outputs[1113][1] = 2.62;

  // section 1114
  carac_sections[1114][0] = 56723.17;
  carac_sections[1114][1] = 0.00;
  carac_sections[1114][2] = 13.40;
  carac_sections[1114][3] = 100.00;
  outputs[1114][0] = 6.16;
  outputs[1114][1] = 2.65;

  // section 1115
  carac_sections[1115][0] = 56763.15;
  carac_sections[1115][1] = 0.00;
  carac_sections[1115][2] = 13.43;
  carac_sections[1115][3] = 100.00;
  outputs[1115][0] = 6.11;
  outputs[1115][1] = 2.67;

  // section 1116
  carac_sections[1116][0] = 56805.80;
  carac_sections[1116][1] = 0.00;
  carac_sections[1116][2] = 13.46;
  carac_sections[1116][3] = 100.00;
  outputs[1116][0] = 6.06;
  outputs[1116][1] = 2.69;

  // section 1117
  carac_sections[1117][0] = 56857.32;
  carac_sections[1117][1] = 0.00;
  carac_sections[1117][2] = 13.50;
  carac_sections[1117][3] = 100.00;
  outputs[1117][0] = 6.00;
  outputs[1117][1] = 2.72;

  // section 1118
  carac_sections[1118][0] = 56914.04;
  carac_sections[1118][1] = 0.00;
  carac_sections[1118][2] = 13.54;
  carac_sections[1118][3] = 100.00;
  outputs[1118][0] = 5.93;
  outputs[1118][1] = 2.75;

  // section 1119
  carac_sections[1119][0] = 56981.53;
  carac_sections[1119][1] = 0.00;
  carac_sections[1119][2] = 13.59;
  carac_sections[1119][3] = 100.00;
  outputs[1119][0] = 5.85;
  outputs[1119][1] = 2.79;

  // section 1120
  carac_sections[1120][0] = 57018.28;
  carac_sections[1120][1] = 0.00;
  carac_sections[1120][2] = 13.62;
  carac_sections[1120][3] = 100.00;
  outputs[1120][0] = 5.80;
  outputs[1120][1] = 2.82;

  // section 1121
  carac_sections[1121][0] = 57056.19;
  carac_sections[1121][1] = 0.00;
  carac_sections[1121][2] = 13.65;
  carac_sections[1121][3] = 100.00;
  outputs[1121][0] = 5.75;
  outputs[1121][1] = 2.84;

  // section 1122
  carac_sections[1122][0] = 57102.96;
  carac_sections[1122][1] = 0.00;
  carac_sections[1122][2] = 13.68;
  carac_sections[1122][3] = 100.00;
  outputs[1122][0] = 5.69;
  outputs[1122][1] = 2.87;

  // section 1123
  carac_sections[1123][0] = 57150.77;
  carac_sections[1123][1] = 0.00;
  carac_sections[1123][2] = 13.71;
  carac_sections[1123][3] = 100.00;
  outputs[1123][0] = 5.63;
  outputs[1123][1] = 2.90;

  // section 1124
  carac_sections[1124][0] = 57205.40;
  carac_sections[1124][1] = 0.00;
  carac_sections[1124][2] = 13.75;
  carac_sections[1124][3] = 100.00;
  outputs[1124][0] = 5.57;
  outputs[1124][1] = 2.93;

  // section 1125
  carac_sections[1125][0] = 57264.61;
  carac_sections[1125][1] = 0.00;
  carac_sections[1125][2] = 13.79;
  carac_sections[1125][3] = 100.00;
  outputs[1125][0] = 5.49;
  outputs[1125][1] = 2.97;

  // section 1126
  carac_sections[1126][0] = 57333.20;
  carac_sections[1126][1] = 0.00;
  carac_sections[1126][2] = 13.83;
  carac_sections[1126][3] = 100.00;
  outputs[1126][0] = 5.41;
  outputs[1126][1] = 3.02;

  // section 1127
  carac_sections[1127][0] = 57407.09;
  carac_sections[1127][1] = 0.00;
  carac_sections[1127][2] = 13.87;
  carac_sections[1127][3] = 100.00;
  outputs[1127][0] = 5.33;
  outputs[1127][1] = 3.06;

  // section 1128
  carac_sections[1128][0] = 57450.83;
  carac_sections[1128][1] = 0.00;
  carac_sections[1128][2] = 13.89;
  carac_sections[1128][3] = 100.00;
  outputs[1128][0] = 5.27;
  outputs[1128][1] = 3.10;

  // section 1129
  carac_sections[1129][0] = 57494.12;
  carac_sections[1129][1] = 0.00;
  carac_sections[1129][2] = 13.91;
  carac_sections[1129][3] = 100.00;
  outputs[1129][0] = 5.22;
  outputs[1129][1] = 3.13;

  // section 1130
  carac_sections[1130][0] = 57542.00;
  carac_sections[1130][1] = 0.00;
  carac_sections[1130][2] = 13.93;
  carac_sections[1130][3] = 100.00;
  outputs[1130][0] = 5.16;
  outputs[1130][1] = 3.16;

  // section 1131
  carac_sections[1131][0] = 57591.21;
  carac_sections[1131][1] = 0.00;
  carac_sections[1131][2] = 13.95;
  carac_sections[1131][3] = 100.00;
  outputs[1131][0] = 5.11;
  outputs[1131][1] = 3.20;

  // section 1132
  carac_sections[1132][0] = 57647.12;
  carac_sections[1132][1] = 0.00;
  carac_sections[1132][2] = 13.97;
  carac_sections[1132][3] = 100.00;
  outputs[1132][0] = 5.05;
  outputs[1132][1] = 3.23;

  // section 1133
  carac_sections[1133][0] = 57704.90;
  carac_sections[1133][1] = 0.00;
  carac_sections[1133][2] = 13.98;
  carac_sections[1133][3] = 100.00;
  outputs[1133][0] = 4.99;
  outputs[1133][1] = 3.27;

  // section 1134
  carac_sections[1134][0] = 57766.72;
  carac_sections[1134][1] = 0.00;
  carac_sections[1134][2] = 14.00;
  carac_sections[1134][3] = 100.00;
  outputs[1134][0] = 4.92;
  outputs[1134][1] = 3.32;

  // section 1135
  carac_sections[1135][0] = 57831.54;
  carac_sections[1135][1] = 0.00;
  carac_sections[1135][2] = 14.01;
  carac_sections[1135][3] = 100.00;
  outputs[1135][0] = 4.86;
  outputs[1135][1] = 3.36;

  // section 1136
  carac_sections[1136][0] = 57901.18;
  carac_sections[1136][1] = 0.00;
  carac_sections[1136][2] = 14.01;
  carac_sections[1136][3] = 100.00;
  outputs[1136][0] = 4.80;
  outputs[1136][1] = 3.40;

  // section 1137
  carac_sections[1137][0] = 57973.65;
  carac_sections[1137][1] = 0.00;
  carac_sections[1137][2] = 14.01;
  carac_sections[1137][3] = 100.00;
  outputs[1137][0] = 4.74;
  outputs[1137][1] = 3.44;

  // section 1138
  carac_sections[1138][0] = 58011.50;
  carac_sections[1138][1] = 0.00;
  carac_sections[1138][2] = 14.01;
  carac_sections[1138][3] = 100.00;
  outputs[1138][0] = 4.70;
  outputs[1138][1] = 3.47;

  // section 1139
  carac_sections[1139][0] = 58049.26;
  carac_sections[1139][1] = 0.00;
  carac_sections[1139][2] = 14.00;
  carac_sections[1139][3] = 100.00;
  outputs[1139][0] = 4.67;
  outputs[1139][1] = 3.49;

  // section 1140
  carac_sections[1140][0] = 58088.88;
  carac_sections[1140][1] = 0.00;
  carac_sections[1140][2] = 14.00;
  carac_sections[1140][3] = 100.00;
  outputs[1140][0] = 4.65;
  outputs[1140][1] = 3.51;

  // section 1141
  carac_sections[1141][0] = 58128.05;
  carac_sections[1141][1] = 0.00;
  carac_sections[1141][2] = 13.99;
  carac_sections[1141][3] = 100.00;
  outputs[1141][0] = 4.62;
  outputs[1141][1] = 3.54;

  // section 1142
  carac_sections[1142][0] = 58170.59;
  carac_sections[1142][1] = 0.00;
  carac_sections[1142][2] = 13.98;
  carac_sections[1142][3] = 100.00;
  outputs[1142][0] = 4.59;
  outputs[1142][1] = 3.56;

  // section 1143
  carac_sections[1143][0] = 58212.35;
  carac_sections[1143][1] = 0.00;
  carac_sections[1143][2] = 13.97;
  carac_sections[1143][3] = 100.00;
  outputs[1143][0] = 4.56;
  outputs[1143][1] = 3.58;

  // section 1144
  carac_sections[1144][0] = 58255.16;
  carac_sections[1144][1] = 0.00;
  carac_sections[1144][2] = 13.95;
  carac_sections[1144][3] = 100.00;
  outputs[1144][0] = 4.54;
  outputs[1144][1] = 3.60;

  // section 1145
  carac_sections[1145][0] = 58297.83;
  carac_sections[1145][1] = 0.00;
  carac_sections[1145][2] = 13.94;
  carac_sections[1145][3] = 100.00;
  outputs[1145][0] = 4.51;
  outputs[1145][1] = 3.62;

  // section 1146
  carac_sections[1146][0] = 58341.46;
  carac_sections[1146][1] = 0.00;
  carac_sections[1146][2] = 13.92;
  carac_sections[1146][3] = 100.00;
  outputs[1146][0] = 4.49;
  outputs[1146][1] = 3.64;

  // section 1147
  carac_sections[1147][0] = 58384.98;
  carac_sections[1147][1] = 0.00;
  carac_sections[1147][2] = 13.90;
  carac_sections[1147][3] = 100.00;
  outputs[1147][0] = 4.47;
  outputs[1147][1] = 3.65;

  // section 1148
  carac_sections[1148][0] = 58429.00;
  carac_sections[1148][1] = 0.00;
  carac_sections[1148][2] = 13.88;
  carac_sections[1148][3] = 100.00;
  outputs[1148][0] = 4.45;
  outputs[1148][1] = 3.67;

  // section 1149
  carac_sections[1149][0] = 58473.05;
  carac_sections[1149][1] = 0.00;
  carac_sections[1149][2] = 13.85;
  carac_sections[1149][3] = 100.00;
  outputs[1149][0] = 4.43;
  outputs[1149][1] = 3.68;

  // section 1150
  carac_sections[1150][0] = 58518.21;
  carac_sections[1150][1] = 0.00;
  carac_sections[1150][2] = 13.83;
  carac_sections[1150][3] = 100.00;
  outputs[1150][0] = 4.42;
  outputs[1150][1] = 3.69;

  // section 1151
  carac_sections[1151][0] = 58563.12;
  carac_sections[1151][1] = 0.00;
  carac_sections[1151][2] = 13.80;
  carac_sections[1151][3] = 100.00;
  outputs[1151][0] = 4.40;
  outputs[1151][1] = 3.71;

  // section 1152
  carac_sections[1152][0] = 58608.00;
  carac_sections[1152][1] = 0.00;
  carac_sections[1152][2] = 13.77;
  carac_sections[1152][3] = 100.00;
  outputs[1152][0] = 4.39;
  outputs[1152][1] = 3.72;

  // section 1153
  carac_sections[1153][0] = 58652.86;
  carac_sections[1153][1] = 0.00;
  carac_sections[1153][2] = 13.74;
  carac_sections[1153][3] = 100.00;
  outputs[1153][0] = 4.38;
  outputs[1153][1] = 3.73;

  // section 1154
  carac_sections[1154][0] = 58697.75;
  carac_sections[1154][1] = 0.00;
  carac_sections[1154][2] = 13.70;
  carac_sections[1154][3] = 100.00;
  outputs[1154][0] = 4.37;
  outputs[1154][1] = 3.73;

  // section 1155
  carac_sections[1155][0] = 58742.58;
  carac_sections[1155][1] = 0.00;
  carac_sections[1155][2] = 13.67;
  carac_sections[1155][3] = 100.00;
  outputs[1155][0] = 4.37;
  outputs[1155][1] = 3.74;

  // section 1156
  carac_sections[1156][0] = 58787.35;
  carac_sections[1156][1] = 0.00;
  carac_sections[1156][2] = 13.64;
  carac_sections[1156][3] = 100.00;
  outputs[1156][0] = 4.36;
  outputs[1156][1] = 3.75;

  // section 1157
  carac_sections[1157][0] = 58833.70;
  carac_sections[1157][1] = 0.00;
  carac_sections[1157][2] = 13.60;
  carac_sections[1157][3] = 100.00;
  outputs[1157][0] = 4.36;
  outputs[1157][1] = 3.75;

  // section 1158
  carac_sections[1158][0] = 58873.34;
  carac_sections[1158][1] = 0.00;
  carac_sections[1158][2] = 13.57;
  carac_sections[1158][3] = 100.00;
  outputs[1158][0] = 4.35;
  outputs[1158][1] = 3.75;

  // section 1159
  carac_sections[1159][0] = 58912.98;
  carac_sections[1159][1] = 0.00;
  carac_sections[1159][2] = 13.53;
  carac_sections[1159][3] = 100.00;
  outputs[1159][0] = 4.35;
  outputs[1159][1] = 3.75;

  // section 1160
  carac_sections[1160][0] = 58951.97;
  carac_sections[1160][1] = 0.00;
  carac_sections[1160][2] = 13.50;
  carac_sections[1160][3] = 100.00;
  outputs[1160][0] = 4.35;
  outputs[1160][1] = 3.75;

  // section 1161
  carac_sections[1161][0] = 58991.37;
  carac_sections[1161][1] = 0.00;
  carac_sections[1161][2] = 13.46;
  carac_sections[1161][3] = 100.00;
  outputs[1161][0] = 4.35;
  outputs[1161][1] = 3.75;

  // section 1162
  carac_sections[1162][0] = 59029.42;
  carac_sections[1162][1] = 0.00;
  carac_sections[1162][2] = 13.43;
  carac_sections[1162][3] = 100.00;
  outputs[1162][0] = 4.35;
  outputs[1162][1] = 3.75;

  // section 1163
  carac_sections[1163][0] = 59067.66;
  carac_sections[1163][1] = 0.00;
  carac_sections[1163][2] = 13.40;
  carac_sections[1163][3] = 100.00;
  outputs[1163][0] = 4.35;
  outputs[1163][1] = 3.75;

  // section 1164
  carac_sections[1164][0] = 59105.26;
  carac_sections[1164][1] = 0.00;
  carac_sections[1164][2] = 13.36;
  carac_sections[1164][3] = 100.00;
  outputs[1164][0] = 4.36;
  outputs[1164][1] = 3.75;

  // section 1165
  carac_sections[1165][0] = 59142.91;
  carac_sections[1165][1] = 0.00;
  carac_sections[1165][2] = 13.33;
  carac_sections[1165][3] = 100.00;
  outputs[1165][0] = 4.35;
  outputs[1165][1] = 3.75;

  // section 1166
  carac_sections[1166][0] = 59216.36;
  carac_sections[1166][1] = 0.00;
  carac_sections[1166][2] = 13.26;
  carac_sections[1166][3] = 100.00;
  outputs[1166][0] = 4.37;
  outputs[1166][1] = 3.74;

  // section 1167
  carac_sections[1167][0] = 59288.68;
  carac_sections[1167][1] = 0.00;
  carac_sections[1167][2] = 13.19;
  carac_sections[1167][3] = 100.00;
  outputs[1167][0] = 4.38;
  outputs[1167][1] = 3.73;

  // section 1168
  carac_sections[1168][0] = 59359.47;
  carac_sections[1168][1] = 0.00;
  carac_sections[1168][2] = 13.12;
  carac_sections[1168][3] = 100.00;
  outputs[1168][0] = 4.39;
  outputs[1168][1] = 3.72;

  // section 1169
  carac_sections[1169][0] = 59429.24;
  carac_sections[1169][1] = 0.00;
  carac_sections[1169][2] = 13.05;
  carac_sections[1169][3] = 100.00;
  outputs[1169][0] = 4.41;
  outputs[1169][1] = 3.71;

  // section 1170
  carac_sections[1170][0] = 59496.40;
  carac_sections[1170][1] = 0.00;
  carac_sections[1170][2] = 12.98;
  carac_sections[1170][3] = 100.00;
  outputs[1170][0] = 4.42;
  outputs[1170][1] = 3.69;

  // section 1171
  carac_sections[1171][0] = 59562.16;
  carac_sections[1171][1] = 0.00;
  carac_sections[1171][2] = 12.92;
  carac_sections[1171][3] = 100.00;
  outputs[1171][0] = 4.44;
  outputs[1171][1] = 3.68;

  // section 1172
  carac_sections[1172][0] = 59626.84;
  carac_sections[1172][1] = 0.00;
  carac_sections[1172][2] = 12.85;
  carac_sections[1172][3] = 100.00;
  outputs[1172][0] = 4.45;
  outputs[1172][1] = 3.67;

  // section 1173
  carac_sections[1173][0] = 59690.91;
  carac_sections[1173][1] = 0.00;
  carac_sections[1173][2] = 12.79;
  carac_sections[1173][3] = 100.00;
  outputs[1173][0] = 4.47;
  outputs[1173][1] = 3.65;

  // section 1174
  carac_sections[1174][0] = 59753.03;
  carac_sections[1174][1] = 0.00;
  carac_sections[1174][2] = 12.73;
  carac_sections[1174][3] = 100.00;
  outputs[1174][0] = 4.49;
  outputs[1174][1] = 3.64;

  // section 1175
  carac_sections[1175][0] = 59814.41;
  carac_sections[1175][1] = 0.00;
  carac_sections[1175][2] = 12.67;
  carac_sections[1175][3] = 100.00;
  outputs[1175][0] = 4.51;
  outputs[1175][1] = 3.62;

  // section 1176
  carac_sections[1176][0] = 59873.78;
  carac_sections[1176][1] = 0.00;
  carac_sections[1176][2] = 12.61;
  carac_sections[1176][3] = 100.00;
  outputs[1176][0] = 4.53;
  outputs[1176][1] = 3.61;

  // section 1177
  carac_sections[1177][0] = 59932.57;
  carac_sections[1177][1] = 0.00;
  carac_sections[1177][2] = 12.55;
  carac_sections[1177][3] = 100.00;
  outputs[1177][0] = 4.55;
  outputs[1177][1] = 3.59;

  // section 1178
  carac_sections[1178][0] = 59988.71;
  carac_sections[1178][1] = 0.00;
  carac_sections[1178][2] = 12.49;
  carac_sections[1178][3] = 100.00;
  outputs[1178][0] = 4.56;
  outputs[1178][1] = 3.58;

  // section 1179
  carac_sections[1179][0] = 60044.11;
  carac_sections[1179][1] = 0.00;
  carac_sections[1179][2] = 12.44;
  carac_sections[1179][3] = 100.00;
  outputs[1179][0] = 4.58;
  outputs[1179][1] = 3.56;

  // section 1180
  carac_sections[1180][0] = 60097.84;
  carac_sections[1180][1] = 0.00;
  carac_sections[1180][2] = 12.39;
  carac_sections[1180][3] = 100.00;
  outputs[1180][0] = 4.60;
  outputs[1180][1] = 3.55;

  // section 1181
  carac_sections[1181][0] = 60151.14;
  carac_sections[1181][1] = 0.00;
  carac_sections[1181][2] = 12.34;
  carac_sections[1181][3] = 100.00;
  outputs[1181][0] = 4.62;
  outputs[1181][1] = 3.54;

  // section 1182
  carac_sections[1182][0] = 60203.24;
  carac_sections[1182][1] = 0.00;
  carac_sections[1182][2] = 12.29;
  carac_sections[1182][3] = 100.00;
  outputs[1182][0] = 4.63;
  outputs[1182][1] = 3.52;

  // section 1183
  carac_sections[1183][0] = 60254.50;
  carac_sections[1183][1] = 0.00;
  carac_sections[1183][2] = 12.24;
  carac_sections[1183][3] = 100.00;
  outputs[1183][0] = 4.65;
  outputs[1183][1] = 3.51;

  // section 1184
  carac_sections[1184][0] = 60304.80;
  carac_sections[1184][1] = 0.00;
  carac_sections[1184][2] = 12.19;
  carac_sections[1184][3] = 100.00;
  outputs[1184][0] = 4.66;
  outputs[1184][1] = 3.50;

  // section 1185
  carac_sections[1185][0] = 60354.74;
  carac_sections[1185][1] = 0.00;
  carac_sections[1185][2] = 12.15;
  carac_sections[1185][3] = 100.00;
  outputs[1185][0] = 4.68;
  outputs[1185][1] = 3.49;

  // section 1186
  carac_sections[1186][0] = 60402.21;
  carac_sections[1186][1] = 0.00;
  carac_sections[1186][2] = 12.10;
  carac_sections[1186][3] = 100.00;
  outputs[1186][0] = 4.69;
  outputs[1186][1] = 3.48;

  // section 1187
  carac_sections[1187][0] = 60449.40;
  carac_sections[1187][1] = 0.00;
  carac_sections[1187][2] = 12.06;
  carac_sections[1187][3] = 100.00;
  outputs[1187][0] = 4.71;
  outputs[1187][1] = 3.47;

  // section 1188
  carac_sections[1188][0] = 60495.77;
  carac_sections[1188][1] = 0.00;
  carac_sections[1188][2] = 12.02;
  carac_sections[1188][3] = 100.00;
  outputs[1188][0] = 4.72;
  outputs[1188][1] = 3.46;

  // section 1189
  carac_sections[1189][0] = 60541.92;
  carac_sections[1189][1] = 0.00;
  carac_sections[1189][2] = 11.98;
  carac_sections[1189][3] = 100.00;
  outputs[1189][0] = 4.73;
  outputs[1189][1] = 3.45;

  // section 1190
  carac_sections[1190][0] = 60586.44;
  carac_sections[1190][1] = 0.00;
  carac_sections[1190][2] = 11.94;
  carac_sections[1190][3] = 100.00;
  outputs[1190][0] = 4.75;
  outputs[1190][1] = 3.44;

  // section 1191
  carac_sections[1191][0] = 60630.92;
  carac_sections[1191][1] = 0.00;
  carac_sections[1191][2] = 11.90;
  carac_sections[1191][3] = 100.00;
  outputs[1191][0] = 4.76;
  outputs[1191][1] = 3.43;

  // section 1192
  carac_sections[1192][0] = 60674.22;
  carac_sections[1192][1] = 0.00;
  carac_sections[1192][2] = 11.87;
  carac_sections[1192][3] = 100.00;
  outputs[1192][0] = 4.77;
  outputs[1192][1] = 3.42;

  // section 1193
  carac_sections[1193][0] = 60718.76;
  carac_sections[1193][1] = 0.00;
  carac_sections[1193][2] = 11.83;
  carac_sections[1193][3] = 100.00;
  outputs[1193][0] = 4.78;
  outputs[1193][1] = 3.42;

  // section 1194
  carac_sections[1194][0] = 60757.87;
  carac_sections[1194][1] = 0.00;
  carac_sections[1194][2] = 11.80;
  carac_sections[1194][3] = 100.00;
  outputs[1194][0] = 4.79;
  outputs[1194][1] = 3.41;

  // section 1195
  carac_sections[1195][0] = 60797.13;
  carac_sections[1195][1] = 0.00;
  carac_sections[1195][2] = 11.77;
  carac_sections[1195][3] = 100.00;
  outputs[1195][0] = 4.80;
  outputs[1195][1] = 3.40;

  // section 1196
  carac_sections[1196][0] = 60835.47;
  carac_sections[1196][1] = 0.00;
  carac_sections[1196][2] = 11.74;
  carac_sections[1196][3] = 100.00;
  outputs[1196][0] = 4.80;
  outputs[1196][1] = 3.40;

  // section 1197
  carac_sections[1197][0] = 60874.21;
  carac_sections[1197][1] = 0.00;
  carac_sections[1197][2] = 11.71;
  carac_sections[1197][3] = 100.00;
  outputs[1197][0] = 4.81;
  outputs[1197][1] = 3.40;

  // section 1198
  carac_sections[1198][0] = 60947.97;
  carac_sections[1198][1] = 0.00;
  carac_sections[1198][2] = 11.65;
  carac_sections[1198][3] = 100.00;
  outputs[1198][0] = 4.82;
  outputs[1198][1] = 3.38;

  // section 1199
  carac_sections[1199][0] = 61020.04;
  carac_sections[1199][1] = 0.00;
  carac_sections[1199][2] = 11.59;
  carac_sections[1199][3] = 100.00;
  outputs[1199][0] = 4.84;
  outputs[1199][1] = 3.38;

  // section 1200
  carac_sections[1200][0] = 61088.42;
  carac_sections[1200][1] = 0.00;
  carac_sections[1200][2] = 11.55;
  carac_sections[1200][3] = 100.00;
  outputs[1200][0] = 4.85;
  outputs[1200][1] = 3.37;

  // section 1201
  carac_sections[1201][0] = 61154.69;
  carac_sections[1201][1] = 0.00;
  carac_sections[1201][2] = 11.50;
  carac_sections[1201][3] = 100.00;
  outputs[1201][0] = 4.85;
  outputs[1201][1] = 3.36;

  // section 1202
  carac_sections[1202][0] = 61218.34;
  carac_sections[1202][1] = 0.00;
  carac_sections[1202][2] = 11.46;
  carac_sections[1202][3] = 100.00;
  outputs[1202][0] = 4.86;
  outputs[1202][1] = 3.36;

  // section 1203
  carac_sections[1203][0] = 61279.79;
  carac_sections[1203][1] = 0.00;
  carac_sections[1203][2] = 11.41;
  carac_sections[1203][3] = 100.00;
  outputs[1203][0] = 4.86;
  outputs[1203][1] = 3.36;

  // section 1204
  carac_sections[1204][0] = 61336.69;
  carac_sections[1204][1] = 0.00;
  carac_sections[1204][2] = 11.38;
  carac_sections[1204][3] = 100.00;
  outputs[1204][0] = 4.87;
  outputs[1204][1] = 3.36;

  // section 1205
  carac_sections[1205][0] = 61391.30;
  carac_sections[1205][1] = 0.00;
  carac_sections[1205][2] = 11.34;
  carac_sections[1205][3] = 100.00;
  outputs[1205][0] = 4.87;
  outputs[1205][1] = 3.36;

  // section 1206
  carac_sections[1206][0] = 61442.24;
  carac_sections[1206][1] = 0.00;
  carac_sections[1206][2] = 11.31;
  carac_sections[1206][3] = 100.00;
  outputs[1206][0] = 4.87;
  outputs[1206][1] = 3.36;

  // section 1207
  carac_sections[1207][0] = 61492.66;
  carac_sections[1207][1] = 0.00;
  carac_sections[1207][2] = 11.28;
  carac_sections[1207][3] = 100.00;
  outputs[1207][0] = 4.87;
  outputs[1207][1] = 3.36;

  // section 1208
  carac_sections[1208][0] = 61536.36;
  carac_sections[1208][1] = 0.00;
  carac_sections[1208][2] = 11.26;
  carac_sections[1208][3] = 100.00;
  outputs[1208][0] = 4.86;
  outputs[1208][1] = 3.36;

  // section 1209
  carac_sections[1209][0] = 61580.73;
  carac_sections[1209][1] = 0.00;
  carac_sections[1209][2] = 11.23;
  carac_sections[1209][3] = 100.00;
  outputs[1209][0] = 4.86;
  outputs[1209][1] = 3.36;

  // section 1210
  carac_sections[1210][0] = 61654.29;
  carac_sections[1210][1] = 0.00;
  carac_sections[1210][2] = 11.19;
  carac_sections[1210][3] = 100.00;
  outputs[1210][0] = 4.86;
  outputs[1210][1] = 3.36;

  // section 1211
  carac_sections[1211][0] = 61710.62;
  carac_sections[1211][1] = 0.00;
  carac_sections[1211][2] = 11.16;
  carac_sections[1211][3] = 100.00;
  outputs[1211][0] = 4.85;
  outputs[1211][1] = 3.37;

  // section 1212
  carac_sections[1212][0] = 61756.00;
  carac_sections[1212][1] = 0.00;
  carac_sections[1212][2] = 11.14;
  carac_sections[1212][3] = 100.00;
  outputs[1212][0] = 4.85;
  outputs[1212][1] = 3.37;

  // section 1213
  carac_sections[1213][0] = 61790.27;
  carac_sections[1213][1] = 0.00;
  carac_sections[1213][2] = 11.12;
  carac_sections[1213][3] = 100.00;
  outputs[1213][0] = 4.84;
  outputs[1213][1] = 3.38;

  // section 1214
  carac_sections[1214][0] = 61835.05;
  carac_sections[1214][1] = 0.00;
  carac_sections[1214][2] = 11.10;
  carac_sections[1214][3] = 100.00;
  outputs[1214][0] = 4.84;
  outputs[1214][1] = 3.38;

  // section 1215
  carac_sections[1215][0] = 61862.78;
  carac_sections[1215][1] = 0.00;
  carac_sections[1215][2] = 11.08;
  carac_sections[1215][3] = 100.00;
  outputs[1215][0] = 4.83;
  outputs[1215][1] = 3.38;

  // section 1216
  carac_sections[1216][0] = 61896.47;
  carac_sections[1216][1] = 0.00;
  carac_sections[1216][2] = 11.07;
  carac_sections[1216][3] = 100.00;
  outputs[1216][0] = 4.82;
  outputs[1216][1] = 3.39;

  // section 1217
  carac_sections[1217][0] = 61925.93;
  carac_sections[1217][1] = 0.00;
  carac_sections[1217][2] = 11.05;
  carac_sections[1217][3] = 100.00;
  outputs[1217][0] = 4.81;
  outputs[1217][1] = 3.39;

  // section 1218
  carac_sections[1218][0] = 61972.04;
  carac_sections[1218][1] = 0.00;
  carac_sections[1218][2] = 11.03;
  carac_sections[1218][3] = 100.00;
  outputs[1218][0] = 4.81;
  outputs[1218][1] = 3.39;

  // section 1219
  carac_sections[1219][0] = 61997.61;
  carac_sections[1219][1] = 0.00;
  carac_sections[1219][2] = 11.02;
  carac_sections[1219][3] = 100.00;
  outputs[1219][0] = 4.80;
  outputs[1219][1] = 3.40;

  // section 1220
  carac_sections[1220][0] = 62026.16;
  carac_sections[1220][1] = 0.00;
  carac_sections[1220][2] = 11.00;
  carac_sections[1220][3] = 100.00;
  outputs[1220][0] = 4.80;
  outputs[1220][1] = 3.40;

  // section 1221
  carac_sections[1221][0] = 62058.42;
  carac_sections[1221][1] = 0.00;
  carac_sections[1221][2] = 10.99;
  carac_sections[1221][3] = 100.00;
  outputs[1221][0] = 4.79;
  outputs[1221][1] = 3.41;

  // section 1222
  carac_sections[1222][0] = 62090.59;
  carac_sections[1222][1] = 0.00;
  carac_sections[1222][2] = 10.97;
  carac_sections[1222][3] = 100.00;
  outputs[1222][0] = 4.78;
  outputs[1222][1] = 3.41;

  // section 1223
  carac_sections[1223][0] = 62126.10;
  carac_sections[1223][1] = 0.00;
  carac_sections[1223][2] = 10.95;
  carac_sections[1223][3] = 100.00;
  outputs[1223][0] = 4.78;
  outputs[1223][1] = 3.42;

  // section 1224
  carac_sections[1224][0] = 62161.89;
  carac_sections[1224][1] = 0.00;
  carac_sections[1224][2] = 10.94;
  carac_sections[1224][3] = 100.00;
  outputs[1224][0] = 4.77;
  outputs[1224][1] = 3.42;

  // section 1225
  carac_sections[1225][0] = 62200.89;
  carac_sections[1225][1] = 0.00;
  carac_sections[1225][2] = 10.92;
  carac_sections[1225][3] = 100.00;
  outputs[1225][0] = 4.76;
  outputs[1225][1] = 3.43;

  // section 1226
  carac_sections[1226][0] = 62240.52;
  carac_sections[1226][1] = 0.00;
  carac_sections[1226][2] = 10.90;
  carac_sections[1226][3] = 100.00;
  outputs[1226][0] = 4.75;
  outputs[1226][1] = 3.43;

  // section 1227
  carac_sections[1227][0] = 62282.67;
  carac_sections[1227][1] = 0.00;
  carac_sections[1227][2] = 10.87;
  carac_sections[1227][3] = 100.00;
  outputs[1227][0] = 4.75;
  outputs[1227][1] = 3.44;

  // section 1228
  carac_sections[1228][0] = 62326.09;
  carac_sections[1228][1] = 0.00;
  carac_sections[1228][2] = 10.85;
  carac_sections[1228][3] = 100.00;
  outputs[1228][0] = 4.74;
  outputs[1228][1] = 3.45;

  // section 1229
  carac_sections[1229][0] = 62372.27;
  carac_sections[1229][1] = 0.00;
  carac_sections[1229][2] = 10.83;
  carac_sections[1229][3] = 100.00;
  outputs[1229][0] = 4.73;
  outputs[1229][1] = 3.45;

  // section 1230
  carac_sections[1230][0] = 62420.31;
  carac_sections[1230][1] = 0.00;
  carac_sections[1230][2] = 10.80;
  carac_sections[1230][3] = 100.00;
  outputs[1230][0] = 4.72;
  outputs[1230][1] = 3.46;

  // section 1231
  carac_sections[1231][0] = 62470.44;
  carac_sections[1231][1] = 0.00;
  carac_sections[1231][2] = 10.77;
  carac_sections[1231][3] = 100.00;
  outputs[1231][0] = 4.72;
  outputs[1231][1] = 3.46;

  // section 1232
  carac_sections[1232][0] = 62522.80;
  carac_sections[1232][1] = 0.00;
  carac_sections[1232][2] = 10.74;
  carac_sections[1232][3] = 100.00;
  outputs[1232][0] = 4.72;
  outputs[1232][1] = 3.46;

  // section 1233
  carac_sections[1233][0] = 62577.73;
  carac_sections[1233][1] = 0.00;
  carac_sections[1233][2] = 10.70;
  carac_sections[1233][3] = 100.00;
  outputs[1233][0] = 4.71;
  outputs[1233][1] = 3.46;

  // section 1234
  carac_sections[1234][0] = 62635.07;
  carac_sections[1234][1] = 0.00;
  carac_sections[1234][2] = 10.66;
  carac_sections[1234][3] = 100.00;
  outputs[1234][0] = 4.71;
  outputs[1234][1] = 3.46;

  // section 1235
  carac_sections[1235][0] = 62694.68;
  carac_sections[1235][1] = 0.00;
  carac_sections[1235][2] = 10.62;
  carac_sections[1235][3] = 100.00;
  outputs[1235][0] = 4.72;
  outputs[1235][1] = 3.46;

  // section 1236
  carac_sections[1236][0] = 62756.49;
  carac_sections[1236][1] = 0.00;
  carac_sections[1236][2] = 10.57;
  carac_sections[1236][3] = 100.00;
  outputs[1236][0] = 4.72;
  outputs[1236][1] = 3.46;

  // section 1237
  carac_sections[1237][0] = 62820.53;
  carac_sections[1237][1] = 0.00;
  carac_sections[1237][2] = 10.52;
  carac_sections[1237][3] = 100.00;
  outputs[1237][0] = 4.74;
  outputs[1237][1] = 3.45;

  // section 1238
  carac_sections[1238][0] = 62886.59;
  carac_sections[1238][1] = 0.00;
  carac_sections[1238][2] = 10.47;
  carac_sections[1238][3] = 100.00;
  outputs[1238][0] = 4.75;
  outputs[1238][1] = 3.44;

  // section 1239
  carac_sections[1239][0] = 62954.34;
  carac_sections[1239][1] = 0.00;
  carac_sections[1239][2] = 10.41;
  carac_sections[1239][3] = 100.00;
  outputs[1239][0] = 4.77;
  outputs[1239][1] = 3.42;

  // section 1240
  carac_sections[1240][0] = 63023.24;
  carac_sections[1240][1] = 0.00;
  carac_sections[1240][2] = 10.34;
  carac_sections[1240][3] = 100.00;
  outputs[1240][0] = 4.80;
  outputs[1240][1] = 3.40;

  // section 1241
  carac_sections[1241][0] = 63087.13;
  carac_sections[1241][1] = 0.00;
  carac_sections[1241][2] = 10.28;
  carac_sections[1241][3] = 100.00;
  outputs[1241][0] = 4.83;
  outputs[1241][1] = 3.38;

  // section 1242
  carac_sections[1242][0] = 63150.71;
  carac_sections[1242][1] = 0.00;
  carac_sections[1242][2] = 10.22;
  carac_sections[1242][3] = 100.00;
  outputs[1242][0] = 4.86;
  outputs[1242][1] = 3.36;

  // section 1243
  carac_sections[1243][0] = 63216.92;
  carac_sections[1243][1] = 0.00;
  carac_sections[1243][2] = 10.15;
  carac_sections[1243][3] = 100.00;
  outputs[1243][0] = 4.89;
  outputs[1243][1] = 3.34;

  // section 1244
  carac_sections[1244][0] = 63259.62;
  carac_sections[1244][1] = 0.00;
  carac_sections[1244][2] = 10.11;
  carac_sections[1244][3] = 100.00;
  outputs[1244][0] = 4.91;
  outputs[1244][1] = 3.32;

  // section 1245
  carac_sections[1245][0] = 63301.46;
  carac_sections[1245][1] = 0.00;
  carac_sections[1245][2] = 10.07;
  carac_sections[1245][3] = 100.00;
  outputs[1245][0] = 4.94;
  outputs[1245][1] = 3.31;

  // section 1246
  carac_sections[1246][0] = 63343.12;
  carac_sections[1246][1] = 0.00;
  carac_sections[1246][2] = 10.03;
  carac_sections[1246][3] = 100.00;
  outputs[1246][0] = 4.96;
  outputs[1246][1] = 3.29;

  // section 1247
  carac_sections[1247][0] = 63385.57;
  carac_sections[1247][1] = 0.00;
  carac_sections[1247][2] = 9.98;
  carac_sections[1247][3] = 100.00;
  outputs[1247][0] = 4.98;
  outputs[1247][1] = 3.28;

  // section 1248
  carac_sections[1248][0] = 63439.71;
  carac_sections[1248][1] = 0.00;
  carac_sections[1248][2] = 9.93;
  carac_sections[1248][3] = 100.00;
  outputs[1248][0] = 5.01;
  outputs[1248][1] = 3.26;

  // section 1249
  carac_sections[1249][0] = 63492.71;
  carac_sections[1249][1] = 0.00;
  carac_sections[1249][2] = 9.88;
  carac_sections[1249][3] = 100.00;
  outputs[1249][0] = 5.04;
  outputs[1249][1] = 3.24;

  // section 1250
  carac_sections[1250][0] = 63541.18;
  carac_sections[1250][1] = 0.00;
  carac_sections[1250][2] = 9.83;
  carac_sections[1250][3] = 100.00;
  outputs[1250][0] = 5.06;
  outputs[1250][1] = 3.22;

  // section 1251
  carac_sections[1251][0] = 63590.04;
  carac_sections[1251][1] = 0.00;
  carac_sections[1251][2] = 9.79;
  carac_sections[1251][3] = 100.00;
  outputs[1251][0] = 5.09;
  outputs[1251][1] = 3.21;

  // section 1252
  carac_sections[1252][0] = 63634.37;
  carac_sections[1252][1] = 0.00;
  carac_sections[1252][2] = 9.75;
  carac_sections[1252][3] = 100.00;
  outputs[1252][0] = 5.11;
  outputs[1252][1] = 3.20;

  // section 1253
  carac_sections[1253][0] = 63679.52;
  carac_sections[1253][1] = 0.00;
  carac_sections[1253][2] = 9.71;
  carac_sections[1253][3] = 100.00;
  outputs[1253][0] = 5.13;
  outputs[1253][1] = 3.18;

  // section 1254
  carac_sections[1254][0] = 63719.73;
  carac_sections[1254][1] = 0.00;
  carac_sections[1254][2] = 9.67;
  carac_sections[1254][3] = 100.00;
  outputs[1254][0] = 5.15;
  outputs[1254][1] = 3.17;

  // section 1255
  carac_sections[1255][0] = 63760.76;
  carac_sections[1255][1] = 0.00;
  carac_sections[1255][2] = 9.64;
  carac_sections[1255][3] = 100.00;
  outputs[1255][0] = 5.16;
  outputs[1255][1] = 3.16;

  // section 1256
  carac_sections[1256][0] = 63834.92;
  carac_sections[1256][1] = 0.00;
  carac_sections[1256][2] = 9.58;
  carac_sections[1256][3] = 100.00;
  outputs[1256][0] = 5.19;
  outputs[1256][1] = 3.14;

  // section 1257
  carac_sections[1257][0] = 63902.67;
  carac_sections[1257][1] = 0.00;
  carac_sections[1257][2] = 9.53;
  carac_sections[1257][3] = 100.00;
  outputs[1257][0] = 5.22;
  outputs[1257][1] = 3.13;

  // section 1258
  carac_sections[1258][0] = 63965.29;
  carac_sections[1258][1] = 0.00;
  carac_sections[1258][2] = 9.48;
  carac_sections[1258][3] = 100.00;
  outputs[1258][0] = 5.23;
  outputs[1258][1] = 3.12;

  // section 1259
  carac_sections[1259][0] = 64023.68;
  carac_sections[1259][1] = 0.00;
  carac_sections[1259][2] = 9.44;
  carac_sections[1259][3] = 100.00;
  outputs[1259][0] = 5.25;
  outputs[1259][1] = 3.11;

  // section 1260
  carac_sections[1260][0] = 64079.44;
  carac_sections[1260][1] = 0.00;
  carac_sections[1260][2] = 9.41;
  carac_sections[1260][3] = 100.00;
  outputs[1260][0] = 5.26;
  outputs[1260][1] = 3.10;

  // section 1261
  carac_sections[1261][0] = 64131.91;
  carac_sections[1261][1] = 0.00;
  carac_sections[1261][2] = 9.37;
  carac_sections[1261][3] = 100.00;
  outputs[1261][0] = 5.27;
  outputs[1261][1] = 3.10;

  // section 1262
  carac_sections[1262][0] = 64183.09;
  carac_sections[1262][1] = 0.00;
  carac_sections[1262][2] = 9.34;
  carac_sections[1262][3] = 100.00;
  outputs[1262][0] = 5.28;
  outputs[1262][1] = 3.09;

  // section 1263
  carac_sections[1263][0] = 64233.37;
  carac_sections[1263][1] = 0.00;
  carac_sections[1263][2] = 9.31;
  carac_sections[1263][3] = 100.00;
  outputs[1263][0] = 5.29;
  outputs[1263][1] = 3.09;

  // section 1264
  carac_sections[1264][0] = 64285.53;
  carac_sections[1264][1] = 0.00;
  carac_sections[1264][2] = 9.28;
  carac_sections[1264][3] = 100.00;
  outputs[1264][0] = 5.30;
  outputs[1264][1] = 3.08;

  // section 1265
  carac_sections[1265][0] = 64330.68;
  carac_sections[1265][1] = 0.00;
  carac_sections[1265][2] = 9.26;
  carac_sections[1265][3] = 100.00;
  outputs[1265][0] = 5.30;
  outputs[1265][1] = 3.08;

  // section 1266
  carac_sections[1266][0] = 64377.29;
  carac_sections[1266][1] = 0.00;
  carac_sections[1266][2] = 9.23;
  carac_sections[1266][3] = 100.00;
  outputs[1266][0] = 5.31;
  outputs[1266][1] = 3.08;

  // section 1267
  carac_sections[1267][0] = 64425.45;
  carac_sections[1267][1] = 0.00;
  carac_sections[1267][2] = 9.20;
  carac_sections[1267][3] = 100.00;
  outputs[1267][0] = 5.32;
  outputs[1267][1] = 3.07;

  // section 1268
  carac_sections[1268][0] = 64477.29;
  carac_sections[1268][1] = 0.00;
  carac_sections[1268][2] = 9.17;
  carac_sections[1268][3] = 100.00;
  outputs[1268][0] = 5.32;
  outputs[1268][1] = 3.07;

  // section 1269
  carac_sections[1269][0] = 64530.77;
  carac_sections[1269][1] = 0.00;
  carac_sections[1269][2] = 9.14;
  carac_sections[1269][3] = 100.00;
  outputs[1269][0] = 5.33;
  outputs[1269][1] = 3.06;

  // section 1270
  carac_sections[1270][0] = 64587.88;
  carac_sections[1270][1] = 0.00;
  carac_sections[1270][2] = 9.10;
  carac_sections[1270][3] = 100.00;
  outputs[1270][0] = 5.35;
  outputs[1270][1] = 3.05;

  // section 1271
  carac_sections[1271][0] = 64646.70;
  carac_sections[1271][1] = 0.00;
  carac_sections[1271][2] = 9.06;
  carac_sections[1271][3] = 100.00;
  outputs[1271][0] = 5.36;
  outputs[1271][1] = 3.04;

  // section 1272
  carac_sections[1272][0] = 64706.04;
  carac_sections[1272][1] = 0.00;
  carac_sections[1272][2] = 9.02;
  carac_sections[1272][3] = 100.00;
  outputs[1272][0] = 5.38;
  outputs[1272][1] = 3.03;

  // section 1273
  carac_sections[1273][0] = 64758.25;
  carac_sections[1273][1] = 0.00;
  carac_sections[1273][2] = 8.98;
  carac_sections[1273][3] = 100.00;
  outputs[1273][0] = 5.40;
  outputs[1273][1] = 3.02;

  // section 1274
  carac_sections[1274][0] = 64799.34;
  carac_sections[1274][1] = 0.00;
  carac_sections[1274][2] = 8.95;
  carac_sections[1274][3] = 100.00;
  outputs[1274][0] = 5.42;
  outputs[1274][1] = 3.01;

  // section 1275
  carac_sections[1275][0] = 64847.12;
  carac_sections[1275][1] = 0.00;
  carac_sections[1275][2] = 8.91;
  carac_sections[1275][3] = 100.00;
  outputs[1275][0] = 5.44;
  outputs[1275][1] = 3.00;

  // section 1276
  carac_sections[1276][0] = 64890.38;
  carac_sections[1276][1] = 0.00;
  carac_sections[1276][2] = 8.87;
  carac_sections[1276][3] = 100.00;
  outputs[1276][0] = 5.47;
  outputs[1276][1] = 2.99;

  // section 1277
  carac_sections[1277][0] = 64918.20;
  carac_sections[1277][1] = 0.00;
  carac_sections[1277][2] = 8.85;
  carac_sections[1277][3] = 100.00;
  outputs[1277][0] = 5.48;
  outputs[1277][1] = 2.98;

  // section 1278
  carac_sections[1278][0] = 64947.38;
  carac_sections[1278][1] = 0.00;
  carac_sections[1278][2] = 8.82;
  carac_sections[1278][3] = 100.00;
  outputs[1278][0] = 5.49;
  outputs[1278][1] = 2.97;

  // section 1279
  carac_sections[1279][0] = 65002.30;
  carac_sections[1279][1] = 0.00;
  carac_sections[1279][2] = 8.77;
  carac_sections[1279][3] = 100.00;
  outputs[1279][0] = 5.53;
  outputs[1279][1] = 2.95;

  // section 1280
  carac_sections[1280][0] = 65035.01;
  carac_sections[1280][1] = 0.00;
  carac_sections[1280][2] = 8.74;
  carac_sections[1280][3] = 100.00;
  outputs[1280][0] = 5.55;
  outputs[1280][1] = 2.94;

  // section 1281
  carac_sections[1281][0] = 65070.76;
  carac_sections[1281][1] = 0.00;
  carac_sections[1281][2] = 8.71;
  carac_sections[1281][3] = 100.00;
  outputs[1281][0] = 5.57;
  outputs[1281][1] = 2.93;

  // section 1282
  carac_sections[1282][0] = 65109.25;
  carac_sections[1282][1] = 0.00;
  carac_sections[1282][2] = 8.67;
  carac_sections[1282][3] = 100.00;
  outputs[1282][0] = 5.60;
  outputs[1282][1] = 2.92;

  // section 1283
  carac_sections[1283][0] = 65147.84;
  carac_sections[1283][1] = 0.00;
  carac_sections[1283][2] = 8.63;
  carac_sections[1283][3] = 100.00;
  outputs[1283][0] = 5.63;
  outputs[1283][1] = 2.90;

  // section 1284
  carac_sections[1284][0] = 65187.87;
  carac_sections[1284][1] = 0.00;
  carac_sections[1284][2] = 8.59;
  carac_sections[1284][3] = 100.00;
  outputs[1284][0] = 5.65;
  outputs[1284][1] = 2.89;

  // section 1285
  carac_sections[1285][0] = 65227.61;
  carac_sections[1285][1] = 0.00;
  carac_sections[1285][2] = 8.55;
  carac_sections[1285][3] = 100.00;
  outputs[1285][0] = 5.68;
  outputs[1285][1] = 2.87;

  // section 1286
  carac_sections[1286][0] = 65268.63;
  carac_sections[1286][1] = 0.00;
  carac_sections[1286][2] = 8.51;
  carac_sections[1286][3] = 100.00;
  outputs[1286][0] = 5.71;
  outputs[1286][1] = 2.86;

  // section 1287
  carac_sections[1287][0] = 65308.86;
  carac_sections[1287][1] = 0.00;
  carac_sections[1287][2] = 8.47;
  carac_sections[1287][3] = 100.00;
  outputs[1287][0] = 5.74;
  outputs[1287][1] = 2.84;

  // section 1288
  carac_sections[1288][0] = 65349.08;
  carac_sections[1288][1] = 0.00;
  carac_sections[1288][2] = 8.43;
  carac_sections[1288][3] = 100.00;
  outputs[1288][0] = 5.77;
  outputs[1288][1] = 2.83;

  // section 1289
  carac_sections[1289][0] = 65388.86;
  carac_sections[1289][1] = 0.00;
  carac_sections[1289][2] = 8.39;
  carac_sections[1289][3] = 100.00;
  outputs[1289][0] = 5.80;
  outputs[1289][1] = 2.81;

  // section 1290
  carac_sections[1290][0] = 65428.69;
  carac_sections[1290][1] = 0.00;
  carac_sections[1290][2] = 8.35;
  carac_sections[1290][3] = 100.00;
  outputs[1290][0] = 5.83;
  outputs[1290][1] = 2.80;

  // section 1291
  carac_sections[1291][0] = 65467.92;
  carac_sections[1291][1] = 0.00;
  carac_sections[1291][2] = 8.31;
  carac_sections[1291][3] = 100.00;
  outputs[1291][0] = 5.86;
  outputs[1291][1] = 2.78;

  // section 1292
  carac_sections[1292][0] = 65506.87;
  carac_sections[1292][1] = 0.00;
  carac_sections[1292][2] = 8.28;
  carac_sections[1292][3] = 100.00;
  outputs[1292][0] = 5.89;
  outputs[1292][1] = 2.77;

  // section 1293
  carac_sections[1293][0] = 65545.76;
  carac_sections[1293][1] = 0.00;
  carac_sections[1293][2] = 8.24;
  carac_sections[1293][3] = 100.00;
  outputs[1293][0] = 5.92;
  outputs[1293][1] = 2.76;

  // section 1294
  carac_sections[1294][0] = 65583.62;
  carac_sections[1294][1] = 0.00;
  carac_sections[1294][2] = 8.20;
  carac_sections[1294][3] = 100.00;
  outputs[1294][0] = 5.95;
  outputs[1294][1] = 2.74;

  // section 1295
  carac_sections[1295][0] = 65621.50;
  carac_sections[1295][1] = 0.00;
  carac_sections[1295][2] = 8.16;
  carac_sections[1295][3] = 100.00;
  outputs[1295][0] = 5.98;
  outputs[1295][1] = 2.73;

  // section 1296
  carac_sections[1296][0] = 65659.19;
  carac_sections[1296][1] = 0.00;
  carac_sections[1296][2] = 8.12;
  carac_sections[1296][3] = 100.00;
  outputs[1296][0] = 6.01;
  outputs[1296][1] = 2.72;

  // section 1297
  carac_sections[1297][0] = 65697.02;
  carac_sections[1297][1] = 0.00;
  carac_sections[1297][2] = 8.09;
  carac_sections[1297][3] = 100.00;
  outputs[1297][0] = 6.04;
  outputs[1297][1] = 2.70;

  // section 1298
  carac_sections[1298][0] = 65734.49;
  carac_sections[1298][1] = 0.00;
  carac_sections[1298][2] = 8.05;
  carac_sections[1298][3] = 100.00;
  outputs[1298][0] = 6.07;
  outputs[1298][1] = 2.69;

  // section 1299
  carac_sections[1299][0] = 65772.15;
  carac_sections[1299][1] = 0.00;
  carac_sections[1299][2] = 8.01;
  carac_sections[1299][3] = 100.00;
  outputs[1299][0] = 6.10;
  outputs[1299][1] = 2.68;

  // section 1300
  carac_sections[1300][0] = 65809.68;
  carac_sections[1300][1] = 0.00;
  carac_sections[1300][2] = 7.97;
  carac_sections[1300][3] = 100.00;
  outputs[1300][0] = 6.13;
  outputs[1300][1] = 2.66;

  // section 1301
  carac_sections[1301][0] = 65847.57;
  carac_sections[1301][1] = 0.00;
  carac_sections[1301][2] = 7.93;
  carac_sections[1301][3] = 100.00;
  outputs[1301][0] = 6.16;
  outputs[1301][1] = 2.65;

  // section 1302
  carac_sections[1302][0] = 65885.09;
  carac_sections[1302][1] = 0.00;
  carac_sections[1302][2] = 7.90;
  carac_sections[1302][3] = 100.00;
  outputs[1302][0] = 6.19;
  outputs[1302][1] = 2.64;

  // section 1303
  carac_sections[1303][0] = 65923.04;
  carac_sections[1303][1] = 0.00;
  carac_sections[1303][2] = 7.86;
  carac_sections[1303][3] = 100.00;
  outputs[1303][0] = 6.22;
  outputs[1303][1] = 2.62;

  // section 1304
  carac_sections[1304][0] = 65960.94;
  carac_sections[1304][1] = 0.00;
  carac_sections[1304][2] = 7.82;
  carac_sections[1304][3] = 100.00;
  outputs[1304][0] = 6.25;
  outputs[1304][1] = 2.61;

  // section 1305
  carac_sections[1305][0] = 65999.27;
  carac_sections[1305][1] = 0.00;
  carac_sections[1305][2] = 7.78;
  carac_sections[1305][3] = 100.00;
  outputs[1305][0] = 6.29;
  outputs[1305][1] = 2.60;

  // section 1306
  carac_sections[1306][0] = 66037.11;
  carac_sections[1306][1] = 0.00;
  carac_sections[1306][2] = 7.75;
  carac_sections[1306][3] = 100.00;
  outputs[1306][0] = 6.32;
  outputs[1306][1] = 2.59;

  // section 1307
  carac_sections[1307][0] = 66075.61;
  carac_sections[1307][1] = 0.00;
  carac_sections[1307][2] = 7.71;
  carac_sections[1307][3] = 100.00;
  outputs[1307][0] = 6.35;
  outputs[1307][1] = 2.57;

  // section 1308
  carac_sections[1308][0] = 66113.69;
  carac_sections[1308][1] = 0.00;
  carac_sections[1308][2] = 7.67;
  carac_sections[1308][3] = 100.00;
  outputs[1308][0] = 6.38;
  outputs[1308][1] = 2.56;

  // section 1309
  carac_sections[1309][0] = 66154.25;
  carac_sections[1309][1] = 0.00;
  carac_sections[1309][2] = 7.63;
  carac_sections[1309][3] = 100.00;
  outputs[1309][0] = 6.41;
  outputs[1309][1] = 2.55;

  // section 1310
  carac_sections[1310][0] = 66188.77;
  carac_sections[1310][1] = 0.00;
  carac_sections[1310][2] = 7.60;
  carac_sections[1310][3] = 100.00;
  outputs[1310][0] = 6.44;
  outputs[1310][1] = 2.54;

  // section 1311
  carac_sections[1311][0] = 66223.76;
  carac_sections[1311][1] = 0.00;
  carac_sections[1311][2] = 7.57;
  carac_sections[1311][3] = 100.00;
  outputs[1311][0] = 6.46;
  outputs[1311][1] = 2.53;

  // section 1312
  carac_sections[1312][0] = 66259.09;
  carac_sections[1312][1] = 0.00;
  carac_sections[1312][2] = 7.53;
  carac_sections[1312][3] = 100.00;
  outputs[1312][0] = 6.49;
  outputs[1312][1] = 2.51;

  // section 1313
  carac_sections[1313][0] = 66293.50;
  carac_sections[1313][1] = 0.00;
  carac_sections[1313][2] = 7.50;
  carac_sections[1313][3] = 100.00;
  outputs[1313][0] = 6.52;
  outputs[1313][1] = 2.50;

  // section 1314
  carac_sections[1314][0] = 66365.30;
  carac_sections[1314][1] = 0.00;
  carac_sections[1314][2] = 7.43;
  carac_sections[1314][3] = 100.00;
  outputs[1314][0] = 6.58;
  outputs[1314][1] = 2.48;

  // section 1315
  carac_sections[1315][0] = 66436.98;
  carac_sections[1315][1] = 0.00;
  carac_sections[1315][2] = 7.36;
  carac_sections[1315][3] = 100.00;
  outputs[1315][0] = 6.64;
  outputs[1315][1] = 2.46;

  // section 1316
  carac_sections[1316][0] = 66508.64;
  carac_sections[1316][1] = 0.00;
  carac_sections[1316][2] = 7.29;
  carac_sections[1316][3] = 100.00;
  outputs[1316][0] = 6.69;
  outputs[1316][1] = 2.44;

  // section 1317
  carac_sections[1317][0] = 66581.41;
  carac_sections[1317][1] = 0.00;
  carac_sections[1317][2] = 7.22;
  carac_sections[1317][3] = 100.00;
  outputs[1317][0] = 6.75;
  outputs[1317][1] = 2.42;

  // section 1318
  carac_sections[1318][0] = 66655.33;
  carac_sections[1318][1] = 0.00;
  carac_sections[1318][2] = 7.15;
  carac_sections[1318][3] = 100.00;
  outputs[1318][0] = 6.82;
  outputs[1318][1] = 2.39;

  // section 1319
  carac_sections[1319][0] = 66692.78;
  carac_sections[1319][1] = 0.00;
  carac_sections[1319][2] = 7.11;
  carac_sections[1319][3] = 100.00;
  outputs[1319][0] = 6.85;
  outputs[1319][1] = 2.38;

  // section 1320
  carac_sections[1320][0] = 66730.81;
  carac_sections[1320][1] = 0.00;
  carac_sections[1320][2] = 7.07;
  carac_sections[1320][3] = 100.00;
  outputs[1320][0] = 6.88;
  outputs[1320][1] = 2.37;

  // section 1321
  carac_sections[1321][0] = 66804.53;
  carac_sections[1321][1] = 0.00;
  carac_sections[1321][2] = 7.00;
  carac_sections[1321][3] = 100.00;
  outputs[1321][0] = 6.95;
  outputs[1321][1] = 2.35;

  // section 1322
  carac_sections[1322][0] = 66878.76;
  carac_sections[1322][1] = 0.00;
  carac_sections[1322][2] = 6.93;
  carac_sections[1322][3] = 100.00;
  outputs[1322][0] = 7.01;
  outputs[1322][1] = 2.33;

  // section 1323
  carac_sections[1323][0] = 66953.26;
  carac_sections[1323][1] = 0.00;
  carac_sections[1323][2] = 6.85;
  carac_sections[1323][3] = 100.00;
  outputs[1323][0] = 7.08;
  outputs[1323][1] = 2.31;

  // section 1324
  carac_sections[1324][0] = 66990.97;
  carac_sections[1324][1] = 0.00;
  carac_sections[1324][2] = 6.81;
  carac_sections[1324][3] = 100.00;
  outputs[1324][0] = 7.11;
  outputs[1324][1] = 2.30;

  // section 1325
  carac_sections[1325][0] = 67028.75;
  carac_sections[1325][1] = 0.00;
  carac_sections[1325][2] = 6.78;
  carac_sections[1325][3] = 100.00;
  outputs[1325][0] = 7.14;
  outputs[1325][1] = 2.29;

  // section 1326
  carac_sections[1326][0] = 67103.06;
  carac_sections[1326][1] = 0.00;
  carac_sections[1326][2] = 6.71;
  carac_sections[1326][3] = 100.00;
  outputs[1326][0] = 7.21;
  outputs[1326][1] = 2.27;

  // section 1327
  carac_sections[1327][0] = 67177.44;
  carac_sections[1327][1] = 0.00;
  carac_sections[1327][2] = 6.63;
  carac_sections[1327][3] = 100.00;
  outputs[1327][0] = 7.27;
  outputs[1327][1] = 2.25;

  // section 1328
  carac_sections[1328][0] = 67251.34;
  carac_sections[1328][1] = 0.00;
  carac_sections[1328][2] = 6.57;
  carac_sections[1328][3] = 100.00;
  outputs[1328][0] = 7.33;
  outputs[1328][1] = 2.23;

  // section 1329
  carac_sections[1329][0] = 67325.02;
  carac_sections[1329][1] = 0.00;
  carac_sections[1329][2] = 6.50;
  carac_sections[1329][3] = 100.00;
  outputs[1329][0] = 7.39;
  outputs[1329][1] = 2.21;

  // section 1330
  carac_sections[1330][0] = 67390.45;
  carac_sections[1330][1] = 0.00;
  carac_sections[1330][2] = 6.45;
  carac_sections[1330][3] = 100.00;
  outputs[1330][0] = 7.43;
  outputs[1330][1] = 2.20;

  // section 1331
  carac_sections[1331][0] = 67453.73;
  carac_sections[1331][1] = 0.00;
  carac_sections[1331][2] = 6.39;
  carac_sections[1331][3] = 100.00;
  outputs[1331][0] = 7.48;
  outputs[1331][1] = 2.18;

  // section 1332
  carac_sections[1332][0] = 67515.50;
  carac_sections[1332][1] = 0.00;
  carac_sections[1332][2] = 6.34;
  carac_sections[1332][3] = 100.00;
  outputs[1332][0] = 7.52;
  outputs[1332][1] = 2.17;

  // section 1333
  carac_sections[1333][0] = 67577.02;
  carac_sections[1333][1] = 0.00;
  carac_sections[1333][2] = 6.30;
  carac_sections[1333][3] = 100.00;
  outputs[1333][0] = 7.56;
  outputs[1333][1] = 2.16;

  // section 1334
  carac_sections[1334][0] = 67635.81;
  carac_sections[1334][1] = 0.00;
  carac_sections[1334][2] = 6.26;
  carac_sections[1334][3] = 100.00;
  outputs[1334][0] = 7.60;
  outputs[1334][1] = 2.15;

  // section 1335
  carac_sections[1335][0] = 67693.90;
  carac_sections[1335][1] = 0.00;
  carac_sections[1335][2] = 6.22;
  carac_sections[1335][3] = 100.00;
  outputs[1335][0] = 7.63;
  outputs[1335][1] = 2.14;

  // section 1336
  carac_sections[1336][0] = 67750.82;
  carac_sections[1336][1] = 0.00;
  carac_sections[1336][2] = 6.18;
  carac_sections[1336][3] = 100.00;
  outputs[1336][0] = 7.66;
  outputs[1336][1] = 2.13;

  // section 1337
  carac_sections[1337][0] = 67807.77;
  carac_sections[1337][1] = 0.00;
  carac_sections[1337][2] = 6.14;
  carac_sections[1337][3] = 100.00;
  outputs[1337][0] = 7.69;
  outputs[1337][1] = 2.12;

  // section 1338
  carac_sections[1338][0] = 67860.46;
  carac_sections[1338][1] = 0.00;
  carac_sections[1338][2] = 6.11;
  carac_sections[1338][3] = 100.00;
  outputs[1338][0] = 7.72;
  outputs[1338][1] = 2.12;

  // section 1339
  carac_sections[1339][0] = 67912.61;
  carac_sections[1339][1] = 0.00;
  carac_sections[1339][2] = 6.08;
  carac_sections[1339][3] = 100.00;
  outputs[1339][0] = 7.74;
  outputs[1339][1] = 2.11;

  // section 1340
  carac_sections[1340][0] = 67963.52;
  carac_sections[1340][1] = 0.00;
  carac_sections[1340][2] = 6.05;
  carac_sections[1340][3] = 100.00;
  outputs[1340][0] = 7.76;
  outputs[1340][1] = 2.10;

  // section 1341
  carac_sections[1341][0] = 68014.35;
  carac_sections[1341][1] = 0.00;
  carac_sections[1341][2] = 6.03;
  carac_sections[1341][3] = 100.00;
  outputs[1341][0] = 7.78;
  outputs[1341][1] = 2.10;

  // section 1342
  carac_sections[1342][0] = 68063.06;
  carac_sections[1342][1] = 0.00;
  carac_sections[1342][2] = 6.00;
  carac_sections[1342][3] = 100.00;
  outputs[1342][0] = 7.80;
  outputs[1342][1] = 2.09;

  // section 1343
  carac_sections[1343][0] = 68111.48;
  carac_sections[1343][1] = 0.00;
  carac_sections[1343][2] = 5.98;
  carac_sections[1343][3] = 100.00;
  outputs[1343][0] = 7.82;
  outputs[1343][1] = 2.09;

  // section 1344
  carac_sections[1344][0] = 68159.00;
  carac_sections[1344][1] = 0.00;
  carac_sections[1344][2] = 5.96;
  carac_sections[1344][3] = 100.00;
  outputs[1344][0] = 7.83;
  outputs[1344][1] = 2.08;

  // section 1345
  carac_sections[1345][0] = 68208.26;
  carac_sections[1345][1] = 0.00;
  carac_sections[1345][2] = 5.94;
  carac_sections[1345][3] = 100.00;
  outputs[1345][0] = 7.85;
  outputs[1345][1] = 2.08;

  // section 1346
  carac_sections[1346][0] = 68248.03;
  carac_sections[1346][1] = 0.00;
  carac_sections[1346][2] = 5.92;
  carac_sections[1346][3] = 100.00;
  outputs[1346][0] = 7.86;
  outputs[1346][1] = 2.08;

  // section 1347
  carac_sections[1347][0] = 68287.83;
  carac_sections[1347][1] = 0.00;
  carac_sections[1347][2] = 5.91;
  carac_sections[1347][3] = 100.00;
  outputs[1347][0] = 7.87;
  outputs[1347][1] = 2.07;

  // section 1348
  carac_sections[1348][0] = 68326.54;
  carac_sections[1348][1] = 0.00;
  carac_sections[1348][2] = 5.90;
  carac_sections[1348][3] = 100.00;
  outputs[1348][0] = 7.88;
  outputs[1348][1] = 2.07;

  // section 1349
  carac_sections[1349][0] = 68365.77;
  carac_sections[1349][1] = 0.00;
  carac_sections[1349][2] = 5.88;
  carac_sections[1349][3] = 100.00;
  outputs[1349][0] = 7.89;
  outputs[1349][1] = 2.07;

  // section 1350
  carac_sections[1350][0] = 68440.05;
  carac_sections[1350][1] = 0.00;
  carac_sections[1350][2] = 5.86;
  carac_sections[1350][3] = 100.00;
  outputs[1350][0] = 7.90;
  outputs[1350][1] = 2.07;

  // section 1351
  carac_sections[1351][0] = 68513.30;
  carac_sections[1351][1] = 0.00;
  carac_sections[1351][2] = 5.84;
  carac_sections[1351][3] = 100.00;
  outputs[1351][0] = 7.91;
  outputs[1351][1] = 2.06;

  // section 1352
  carac_sections[1352][0] = 68582.11;
  carac_sections[1352][1] = 0.00;
  carac_sections[1352][2] = 5.83;
  carac_sections[1352][3] = 100.00;
  outputs[1352][0] = 7.92;
  outputs[1352][1] = 2.06;

  // section 1353
  carac_sections[1353][0] = 68649.23;
  carac_sections[1353][1] = 0.00;
  carac_sections[1353][2] = 5.81;
  carac_sections[1353][3] = 100.00;
  outputs[1353][0] = 7.92;
  outputs[1353][1] = 2.06;

  // section 1354
  carac_sections[1354][0] = 68714.40;
  carac_sections[1354][1] = 0.00;
  carac_sections[1354][2] = 5.80;
  carac_sections[1354][3] = 100.00;
  outputs[1354][0] = 7.93;
  outputs[1354][1] = 2.06;

  // section 1355
  carac_sections[1355][0] = 68779.26;
  carac_sections[1355][1] = 0.00;
  carac_sections[1355][2] = 5.79;
  carac_sections[1355][3] = 100.00;
  outputs[1355][0] = 7.93;
  outputs[1355][1] = 2.06;

  // section 1356
  carac_sections[1356][0] = 68835.00;
  carac_sections[1356][1] = 0.00;
  carac_sections[1356][2] = 5.78;
  carac_sections[1356][3] = 100.00;
  outputs[1356][0] = 7.93;
  outputs[1356][1] = 2.06;

  // section 1357
  carac_sections[1357][0] = 68889.10;
  carac_sections[1357][1] = 0.00;
  carac_sections[1357][2] = 5.78;
  carac_sections[1357][3] = 100.00;
  outputs[1357][0] = 7.92;
  outputs[1357][1] = 2.06;

  // section 1358
  carac_sections[1358][0] = 68941.37;
  carac_sections[1358][1] = 0.00;
  carac_sections[1358][2] = 5.78;
  carac_sections[1358][3] = 100.00;
  outputs[1358][0] = 7.92;
  outputs[1358][1] = 2.06;

  // section 1359
  carac_sections[1359][0] = 68993.79;
  carac_sections[1359][1] = 0.00;
  carac_sections[1359][2] = 5.77;
  carac_sections[1359][3] = 100.00;
  outputs[1359][0] = 7.92;
  outputs[1359][1] = 2.06;

  // section 1360
  carac_sections[1360][0] = 69042.32;
  carac_sections[1360][1] = 0.00;
  carac_sections[1360][2] = 5.77;
  carac_sections[1360][3] = 100.00;
  outputs[1360][0] = 7.91;
  outputs[1360][1] = 2.06;

  // section 1361
  carac_sections[1361][0] = 69090.50;
  carac_sections[1361][1] = 0.00;
  carac_sections[1361][2] = 5.77;
  carac_sections[1361][3] = 100.00;
  outputs[1361][0] = 7.91;
  outputs[1361][1] = 2.07;

  // section 1362
  carac_sections[1362][0] = 69137.32;
  carac_sections[1362][1] = 0.00;
  carac_sections[1362][2] = 5.77;
  carac_sections[1362][3] = 100.00;
  outputs[1362][0] = 7.90;
  outputs[1362][1] = 2.07;

  // section 1363
  carac_sections[1363][0] = 69185.86;
  carac_sections[1363][1] = 0.00;
  carac_sections[1363][2] = 5.77;
  carac_sections[1363][3] = 100.00;
  outputs[1363][0] = 7.89;
  outputs[1363][1] = 2.07;

  // section 1364
  carac_sections[1364][0] = 69225.72;
  carac_sections[1364][1] = 0.00;
  carac_sections[1364][2] = 5.78;
  carac_sections[1364][3] = 100.00;
  outputs[1364][0] = 7.88;
  outputs[1364][1] = 2.07;

  // section 1365
  carac_sections[1365][0] = 69265.68;
  carac_sections[1365][1] = 0.00;
  carac_sections[1365][2] = 5.78;
  carac_sections[1365][3] = 100.00;
  outputs[1365][0] = 7.87;
  outputs[1365][1] = 2.07;

  // section 1366
  carac_sections[1366][0] = 69304.38;
  carac_sections[1366][1] = 0.00;
  carac_sections[1366][2] = 5.78;
  carac_sections[1366][3] = 100.00;
  outputs[1366][0] = 7.86;
  outputs[1366][1] = 2.08;

  // section 1367
  carac_sections[1367][0] = 69343.56;
  carac_sections[1367][1] = 0.00;
  carac_sections[1367][2] = 5.79;
  carac_sections[1367][3] = 100.00;
  outputs[1367][0] = 7.85;
  outputs[1367][1] = 2.08;

  // section 1368
  carac_sections[1368][0] = 69416.66;
  carac_sections[1368][1] = 0.00;
  carac_sections[1368][2] = 5.80;
  carac_sections[1368][3] = 100.00;
  outputs[1368][0] = 7.83;
  outputs[1368][1] = 2.08;

  // section 1369
  carac_sections[1369][0] = 69488.35;
  carac_sections[1369][1] = 0.00;
  carac_sections[1369][2] = 5.81;
  carac_sections[1369][3] = 100.00;
  outputs[1369][0] = 7.81;
  outputs[1369][1] = 2.09;

  // section 1370
  carac_sections[1370][0] = 69550.52;
  carac_sections[1370][1] = 0.00;
  carac_sections[1370][2] = 5.82;
  carac_sections[1370][3] = 100.00;
  outputs[1370][0] = 7.79;
  outputs[1370][1] = 2.10;

  // section 1371
  carac_sections[1371][0] = 69610.41;
  carac_sections[1371][1] = 0.00;
  carac_sections[1371][2] = 5.83;
  carac_sections[1371][3] = 100.00;
  outputs[1371][0] = 7.77;
  outputs[1371][1] = 2.10;

  // section 1372
  carac_sections[1372][0] = 69667.18;
  carac_sections[1372][1] = 0.00;
  carac_sections[1372][2] = 5.84;
  carac_sections[1372][3] = 100.00;
  outputs[1372][0] = 7.75;
  outputs[1372][1] = 2.11;

  // section 1373
  carac_sections[1373][0] = 69724.16;
  carac_sections[1373][1] = 0.00;
  carac_sections[1373][2] = 5.86;
  carac_sections[1373][3] = 100.00;
  outputs[1373][0] = 7.73;
  outputs[1373][1] = 2.11;

  // section 1374
  carac_sections[1374][0] = 69772.37;
  carac_sections[1374][1] = 0.00;
  carac_sections[1374][2] = 5.87;
  carac_sections[1374][3] = 100.00;
  outputs[1374][0] = 7.70;
  outputs[1374][1] = 2.12;

  // section 1375
  carac_sections[1375][0] = 69819.77;
  carac_sections[1375][1] = 0.00;
  carac_sections[1375][2] = 5.88;
  carac_sections[1375][3] = 100.00;
  outputs[1375][0] = 7.68;
  outputs[1375][1] = 2.13;

  // section 1376
  carac_sections[1376][0] = 69864.75;
  carac_sections[1376][1] = 0.00;
  carac_sections[1376][2] = 5.90;
  carac_sections[1376][3] = 100.00;
  outputs[1376][0] = 7.66;
  outputs[1376][1] = 2.13;

  // section 1377
  carac_sections[1377][0] = 69911.56;
  carac_sections[1377][1] = 0.00;
  carac_sections[1377][2] = 5.91;
  carac_sections[1377][3] = 100.00;
  outputs[1377][0] = 7.64;
  outputs[1377][1] = 2.14;

  // section 1378
  carac_sections[1378][0] = 69949.42;
  carac_sections[1378][1] = 0.00;
  carac_sections[1378][2] = 5.92;
  carac_sections[1378][3] = 100.00;
  outputs[1378][0] = 7.62;
  outputs[1378][1] = 2.14;

  // section 1379
  carac_sections[1379][0] = 69987.41;
  carac_sections[1379][1] = 0.00;
  carac_sections[1379][2] = 5.94;
  carac_sections[1379][3] = 100.00;
  outputs[1379][0] = 7.60;
  outputs[1379][1] = 2.15;

  // section 1380
  carac_sections[1380][0] = 70061.12;
  carac_sections[1380][1] = 0.00;
  carac_sections[1380][2] = 5.96;
  carac_sections[1380][3] = 100.00;
  outputs[1380][0] = 7.56;
  outputs[1380][1] = 2.16;

  // section 1381
  carac_sections[1381][0] = 70126.38;
  carac_sections[1381][1] = 0.00;
  carac_sections[1381][2] = 5.99;
  carac_sections[1381][3] = 100.00;
  outputs[1381][0] = 7.52;
  outputs[1381][1] = 2.17;

  // section 1382
  carac_sections[1382][0] = 70189.11;
  carac_sections[1382][1] = 0.00;
  carac_sections[1382][2] = 6.01;
  carac_sections[1382][3] = 100.00;
  outputs[1382][0] = 7.49;
  outputs[1382][1] = 2.18;

  // section 1383
  carac_sections[1383][0] = 70243.71;
  carac_sections[1383][1] = 0.00;
  carac_sections[1383][2] = 6.04;
  carac_sections[1383][3] = 100.00;
  outputs[1383][0] = 7.45;
  outputs[1383][1] = 2.19;

  // section 1384
  carac_sections[1384][0] = 70297.70;
  carac_sections[1384][1] = 0.00;
  carac_sections[1384][2] = 6.06;
  carac_sections[1384][3] = 100.00;
  outputs[1384][0] = 7.42;
  outputs[1384][1] = 2.20;

  // section 1385
  carac_sections[1385][0] = 70344.62;
  carac_sections[1385][1] = 0.00;
  carac_sections[1385][2] = 6.08;
  carac_sections[1385][3] = 100.00;
  outputs[1385][0] = 7.38;
  outputs[1385][1] = 2.21;

  // section 1386
  carac_sections[1386][0] = 70391.66;
  carac_sections[1386][1] = 0.00;
  carac_sections[1386][2] = 6.11;
  carac_sections[1386][3] = 100.00;
  outputs[1386][0] = 7.35;
  outputs[1386][1] = 2.22;

  // section 1387
  carac_sections[1387][0] = 70432.20;
  carac_sections[1387][1] = 0.00;
  carac_sections[1387][2] = 6.12;
  carac_sections[1387][3] = 100.00;
  outputs[1387][0] = 7.32;
  outputs[1387][1] = 2.23;

  // section 1388
  carac_sections[1388][0] = 70473.66;
  carac_sections[1388][1] = 0.00;
  carac_sections[1388][2] = 6.15;
  carac_sections[1388][3] = 100.00;
  outputs[1388][0] = 7.29;
  outputs[1388][1] = 2.24;

  // section 1389
  carac_sections[1389][0] = 70546.14;
  carac_sections[1389][1] = 0.00;
  carac_sections[1389][2] = 6.18;
  carac_sections[1389][3] = 100.00;
  outputs[1389][0] = 7.24;
  outputs[1389][1] = 2.25;

  // section 1390
  carac_sections[1390][0] = 70614.33;
  carac_sections[1390][1] = 0.00;
  carac_sections[1390][2] = 6.22;
  carac_sections[1390][3] = 100.00;
  outputs[1390][0] = 7.19;
  outputs[1390][1] = 2.27;

  // section 1391
  carac_sections[1391][0] = 70676.71;
  carac_sections[1391][1] = 0.00;
  carac_sections[1391][2] = 6.25;
  carac_sections[1391][3] = 100.00;
  outputs[1391][0] = 7.14;
  outputs[1391][1] = 2.29;

  // section 1392
  carac_sections[1392][0] = 70736.12;
  carac_sections[1392][1] = 0.00;
  carac_sections[1392][2] = 6.29;
  carac_sections[1392][3] = 100.00;
  outputs[1392][0] = 7.09;
  outputs[1392][1] = 2.30;

  // section 1393
  carac_sections[1393][0] = 70792.68;
  carac_sections[1393][1] = 0.00;
  carac_sections[1393][2] = 6.32;
  carac_sections[1393][3] = 100.00;
  outputs[1393][0] = 7.05;
  outputs[1393][1] = 2.32;

  // section 1394
  carac_sections[1394][0] = 70848.50;
  carac_sections[1394][1] = 0.00;
  carac_sections[1394][2] = 6.35;
  carac_sections[1394][3] = 100.00;
  outputs[1394][0] = 7.00;
  outputs[1394][1] = 2.33;

  // section 1395
  carac_sections[1395][0] = 70904.62;
  carac_sections[1395][1] = 0.00;
  carac_sections[1395][2] = 6.39;
  carac_sections[1395][3] = 100.00;
  outputs[1395][0] = 6.95;
  outputs[1395][1] = 2.35;

  // section 1396
  carac_sections[1396][0] = 70961.31;
  carac_sections[1396][1] = 0.00;
  carac_sections[1396][2] = 6.42;
  carac_sections[1396][3] = 100.00;
  outputs[1396][0] = 6.90;
  outputs[1396][1] = 2.37;

  // section 1397
  carac_sections[1397][0] = 71025.08;
  carac_sections[1397][1] = 0.00;
  carac_sections[1397][2] = 6.46;
  carac_sections[1397][3] = 100.00;
  outputs[1397][0] = 6.84;
  outputs[1397][1] = 2.39;

  // section 1398
  carac_sections[1398][0] = 71053.62;
  carac_sections[1398][1] = 0.00;
  carac_sections[1398][2] = 6.48;
  carac_sections[1398][3] = 100.00;
  outputs[1398][0] = 6.82;
  outputs[1398][1] = 2.40;

  // section 1399
  carac_sections[1399][0] = 71086.39;
  carac_sections[1399][1] = 0.00;
  carac_sections[1399][2] = 6.50;
  carac_sections[1399][3] = 100.00;
  outputs[1399][0] = 6.79;
  outputs[1399][1] = 2.41;

  // section 1400
  carac_sections[1400][0] = 71121.40;
  carac_sections[1400][1] = 0.00;
  carac_sections[1400][2] = 6.52;
  carac_sections[1400][3] = 100.00;
  outputs[1400][0] = 6.75;
  outputs[1400][1] = 2.42;

  // section 1401
  carac_sections[1401][0] = 71158.45;
  carac_sections[1401][1] = 0.00;
  carac_sections[1401][2] = 6.55;
  carac_sections[1401][3] = 100.00;
  outputs[1401][0] = 6.72;
  outputs[1401][1] = 2.43;

  // section 1402
  carac_sections[1402][0] = 71197.92;
  carac_sections[1402][1] = 0.00;
  carac_sections[1402][2] = 6.57;
  carac_sections[1402][3] = 100.00;
  outputs[1402][0] = 6.68;
  outputs[1402][1] = 2.44;

  // section 1403
  carac_sections[1403][0] = 71241.37;
  carac_sections[1403][1] = 0.00;
  carac_sections[1403][2] = 6.60;
  carac_sections[1403][3] = 100.00;
  outputs[1403][0] = 6.64;
  outputs[1403][1] = 2.46;

  // section 1404
  carac_sections[1404][0] = 71289.84;
  carac_sections[1404][1] = 0.00;
  carac_sections[1404][2] = 6.63;
  carac_sections[1404][3] = 100.00;
  outputs[1404][0] = 6.59;
  outputs[1404][1] = 2.48;

  // section 1405
  carac_sections[1405][0] = 71344.07;
  carac_sections[1405][1] = 0.00;
  carac_sections[1405][2] = 6.67;
  carac_sections[1405][3] = 100.00;
  outputs[1405][0] = 6.54;
  outputs[1405][1] = 2.50;

  // section 1406
  carac_sections[1406][0] = 71407.90;
  carac_sections[1406][1] = 0.00;
  carac_sections[1406][2] = 6.71;
  carac_sections[1406][3] = 100.00;
  outputs[1406][0] = 6.48;
  outputs[1406][1] = 2.52;

  // section 1407
  carac_sections[1407][0] = 71442.58;
  carac_sections[1407][1] = 0.00;
  carac_sections[1407][2] = 6.73;
  carac_sections[1407][3] = 100.00;
  outputs[1407][0] = 6.44;
  outputs[1407][1] = 2.54;

  // section 1408
  carac_sections[1408][0] = 71479.83;
  carac_sections[1408][1] = 0.00;
  carac_sections[1408][2] = 6.76;
  carac_sections[1408][3] = 100.00;
  outputs[1408][0] = 6.40;
  outputs[1408][1] = 2.55;

  // section 1409
  carac_sections[1409][0] = 71523.04;
  carac_sections[1409][1] = 0.00;
  carac_sections[1409][2] = 6.78;
  carac_sections[1409][3] = 100.00;
  outputs[1409][0] = 6.36;
  outputs[1409][1] = 2.57;

  // section 1410
  carac_sections[1410][0] = 71569.48;
  carac_sections[1410][1] = 0.00;
  carac_sections[1410][2] = 6.82;
  carac_sections[1410][3] = 100.00;
  outputs[1410][0] = 6.31;
  outputs[1410][1] = 2.59;

  // section 1411
  carac_sections[1411][0] = 71623.74;
  carac_sections[1411][1] = 0.00;
  carac_sections[1411][2] = 6.85;
  carac_sections[1411][3] = 100.00;
  outputs[1411][0] = 6.25;
  outputs[1411][1] = 2.61;

  // section 1412
  carac_sections[1412][0] = 71684.90;
  carac_sections[1412][1] = 0.00;
  carac_sections[1412][2] = 6.89;
  carac_sections[1412][3] = 100.00;
  outputs[1412][0] = 6.19;
  outputs[1412][1] = 2.64;

  // section 1413
  carac_sections[1413][0] = 71755.10;
  carac_sections[1413][1] = 0.00;
  carac_sections[1413][2] = 6.93;
  carac_sections[1413][3] = 100.00;
  outputs[1413][0] = 6.12;
  outputs[1413][1] = 2.67;

  // section 1414
  carac_sections[1414][0] = 71795.88;
  carac_sections[1414][1] = 0.00;
  carac_sections[1414][2] = 6.96;
  carac_sections[1414][3] = 100.00;
  outputs[1414][0] = 6.08;
  outputs[1414][1] = 2.69;

  // section 1415
  carac_sections[1415][0] = 71838.17;
  carac_sections[1415][1] = 0.00;
  carac_sections[1415][2] = 6.98;
  carac_sections[1415][3] = 100.00;
  outputs[1415][0] = 6.03;
  outputs[1415][1] = 2.71;

  // section 1416
  carac_sections[1416][0] = 71887.90;
  carac_sections[1416][1] = 0.00;
  carac_sections[1416][2] = 7.01;
  carac_sections[1416][3] = 100.00;
  outputs[1416][0] = 5.98;
  outputs[1416][1] = 2.73;

  // section 1417
  carac_sections[1417][0] = 71941.23;
  carac_sections[1417][1] = 0.00;
  carac_sections[1417][2] = 7.04;
  carac_sections[1417][3] = 100.00;
  outputs[1417][0] = 5.93;
  outputs[1417][1] = 2.76;

  // section 1418
  carac_sections[1418][0] = 72003.20;
  carac_sections[1418][1] = 0.00;
  carac_sections[1418][2] = 7.08;
  carac_sections[1418][3] = 100.00;
  outputs[1418][0] = 5.86;
  outputs[1418][1] = 2.78;

  // section 1419
  carac_sections[1419][0] = 72071.90;
  carac_sections[1419][1] = 0.00;
  carac_sections[1419][2] = 7.11;
  carac_sections[1419][3] = 100.00;
  outputs[1419][0] = 5.80;
  outputs[1419][1] = 2.82;

  // section 1420
  carac_sections[1420][0] = 72109.70;
  carac_sections[1420][1] = 0.00;
  carac_sections[1420][2] = 7.13;
  carac_sections[1420][3] = 100.00;
  outputs[1420][0] = 5.76;
  outputs[1420][1] = 2.84;

  // section 1421
  carac_sections[1421][0] = 72147.92;
  carac_sections[1421][1] = 0.00;
  carac_sections[1421][2] = 7.15;
  carac_sections[1421][3] = 100.00;
  outputs[1421][0] = 5.72;
  outputs[1421][1] = 2.85;

  // section 1422
  carac_sections[1422][0] = 72192.23;
  carac_sections[1422][1] = 0.00;
  carac_sections[1422][2] = 7.17;
  carac_sections[1422][3] = 100.00;
  outputs[1422][0] = 5.68;
  outputs[1422][1] = 2.87;

  // section 1423
  carac_sections[1423][0] = 72237.07;
  carac_sections[1423][1] = 0.00;
  carac_sections[1423][2] = 7.19;
  carac_sections[1423][3] = 100.00;
  outputs[1423][0] = 5.64;
  outputs[1423][1] = 2.90;

  // section 1424
  carac_sections[1424][0] = 72288.25;
  carac_sections[1424][1] = 0.00;
  carac_sections[1424][2] = 7.21;
  carac_sections[1424][3] = 100.00;
  outputs[1424][0] = 5.59;
  outputs[1424][1] = 2.92;

  // section 1425
  carac_sections[1425][0] = 72341.70;
  carac_sections[1425][1] = 0.00;
  carac_sections[1425][2] = 7.22;
  carac_sections[1425][3] = 100.00;
  outputs[1425][0] = 5.55;
  outputs[1425][1] = 2.94;

  // section 1426
  carac_sections[1426][0] = 72400.48;
  carac_sections[1426][1] = 0.00;
  carac_sections[1426][2] = 7.24;
  carac_sections[1426][3] = 100.00;
  outputs[1426][0] = 5.50;
  outputs[1426][1] = 2.97;

  // section 1427
  carac_sections[1427][0] = 72462.98;
  carac_sections[1427][1] = 0.00;
  carac_sections[1427][2] = 7.26;
  carac_sections[1427][3] = 100.00;
  outputs[1427][0] = 5.45;
  outputs[1427][1] = 3.00;

  // section 1428
  carac_sections[1428][0] = 72531.62;
  carac_sections[1428][1] = 0.00;
  carac_sections[1428][2] = 7.27;
  carac_sections[1428][3] = 100.00;
  outputs[1428][0] = 5.40;
  outputs[1428][1] = 3.02;

  // section 1429
  carac_sections[1429][0] = 72604.23;
  carac_sections[1429][1] = 0.00;
  carac_sections[1429][2] = 7.27;
  carac_sections[1429][3] = 100.00;
  outputs[1429][0] = 5.36;
  outputs[1429][1] = 3.05;

  // section 1430
  carac_sections[1430][0] = 72644.14;
  carac_sections[1430][1] = 0.00;
  carac_sections[1430][2] = 7.28;
  carac_sections[1430][3] = 100.00;
  outputs[1430][0] = 5.33;
  outputs[1430][1] = 3.06;

  // section 1431
  carac_sections[1431][0] = 72683.60;
  carac_sections[1431][1] = 0.00;
  carac_sections[1431][2] = 7.28;
  carac_sections[1431][3] = 100.00;
  outputs[1431][0] = 5.31;
  outputs[1431][1] = 3.08;

  // section 1432
  carac_sections[1432][0] = 72726.12;
  carac_sections[1432][1] = 0.00;
  carac_sections[1432][2] = 7.27;
  carac_sections[1432][3] = 100.00;
  outputs[1432][0] = 5.28;
  outputs[1432][1] = 3.09;

  // section 1433
  carac_sections[1433][0] = 72768.45;
  carac_sections[1433][1] = 0.00;
  carac_sections[1433][2] = 7.27;
  carac_sections[1433][3] = 100.00;
  outputs[1433][0] = 5.26;
  outputs[1433][1] = 3.10;

  // section 1434
  carac_sections[1434][0] = 72814.91;
  carac_sections[1434][1] = 0.00;
  carac_sections[1434][2] = 7.26;
  carac_sections[1434][3] = 100.00;
  outputs[1434][0] = 5.25;
  outputs[1434][1] = 3.11;

  // section 1435
  carac_sections[1435][0] = 72861.12;
  carac_sections[1435][1] = 0.00;
  carac_sections[1435][2] = 7.25;
  carac_sections[1435][3] = 100.00;
  outputs[1435][0] = 5.23;
  outputs[1435][1] = 3.12;

  // section 1436
  carac_sections[1436][0] = 72909.51;
  carac_sections[1436][1] = 0.00;
  carac_sections[1436][2] = 7.24;
  carac_sections[1436][3] = 100.00;
  outputs[1436][0] = 5.21;
  outputs[1436][1] = 3.13;

  // section 1437
  carac_sections[1437][0] = 72958.31;
  carac_sections[1437][1] = 0.00;
  carac_sections[1437][2] = 7.23;
  carac_sections[1437][3] = 100.00;
  outputs[1437][0] = 5.20;
  outputs[1437][1] = 3.14;

  // section 1438
  carac_sections[1438][0] = 73009.77;
  carac_sections[1438][1] = 0.00;
  carac_sections[1438][2] = 7.21;
  carac_sections[1438][3] = 100.00;
  outputs[1438][0] = 5.19;
  outputs[1438][1] = 3.14;

  // section 1439
  carac_sections[1439][0] = 73061.52;
  carac_sections[1439][1] = 0.00;
  carac_sections[1439][2] = 7.19;
  carac_sections[1439][3] = 100.00;
  outputs[1439][0] = 5.19;
  outputs[1439][1] = 3.15;

  // section 1440
  carac_sections[1440][0] = 73114.99;
  carac_sections[1440][1] = 0.00;
  carac_sections[1440][2] = 7.17;
  carac_sections[1440][3] = 100.00;
  outputs[1440][0] = 5.18;
  outputs[1440][1] = 3.15;

  // section 1441
  carac_sections[1441][0] = 73169.00;
  carac_sections[1441][1] = 0.00;
  carac_sections[1441][2] = 7.14;
  carac_sections[1441][3] = 100.00;
  outputs[1441][0] = 5.18;
  outputs[1441][1] = 3.15;

  // section 1442
  carac_sections[1442][0] = 73221.68;
  carac_sections[1442][1] = 0.00;
  carac_sections[1442][2] = 7.11;
  carac_sections[1442][3] = 100.00;
  outputs[1442][0] = 5.19;
  outputs[1442][1] = 3.15;

  // section 1443
  carac_sections[1443][0] = 73274.45;
  carac_sections[1443][1] = 0.00;
  carac_sections[1443][2] = 7.08;
  carac_sections[1443][3] = 100.00;
  outputs[1443][0] = 5.19;
  outputs[1443][1] = 3.14;

  // section 1444
  carac_sections[1444][0] = 73327.78;
  carac_sections[1444][1] = 0.00;
  carac_sections[1444][2] = 7.05;
  carac_sections[1444][3] = 100.00;
  outputs[1444][0] = 5.20;
  outputs[1444][1] = 3.14;

  // section 1445
  carac_sections[1445][0] = 73381.31;
  carac_sections[1445][1] = 0.00;
  carac_sections[1445][2] = 7.01;
  carac_sections[1445][3] = 100.00;
  outputs[1445][0] = 5.21;
  outputs[1445][1] = 3.13;

  // section 1446
  carac_sections[1446][0] = 73435.17;
  carac_sections[1446][1] = 0.00;
  carac_sections[1446][2] = 6.97;
  carac_sections[1446][3] = 100.00;
  outputs[1446][0] = 5.23;
  outputs[1446][1] = 3.12;

  // section 1447
  carac_sections[1447][0] = 73489.18;
  carac_sections[1447][1] = 0.00;
  carac_sections[1447][2] = 6.93;
  carac_sections[1447][3] = 100.00;
  outputs[1447][0] = 5.24;
  outputs[1447][1] = 3.11;

  // section 1448
  carac_sections[1448][0] = 73543.50;
  carac_sections[1448][1] = 0.00;
  carac_sections[1448][2] = 6.89;
  carac_sections[1448][3] = 100.00;
  outputs[1448][0] = 5.26;
  outputs[1448][1] = 3.10;

  // section 1449
  carac_sections[1449][0] = 73597.76;
  carac_sections[1449][1] = 0.00;
  carac_sections[1449][2] = 6.85;
  carac_sections[1449][3] = 100.00;
  outputs[1449][0] = 5.29;
  outputs[1449][1] = 3.09;

  // section 1450
  carac_sections[1450][0] = 73651.95;
  carac_sections[1450][1] = 0.00;
  carac_sections[1450][2] = 6.80;
  carac_sections[1450][3] = 100.00;
  outputs[1450][0] = 5.31;
  outputs[1450][1] = 3.07;

  // section 1451
  carac_sections[1451][0] = 73705.87;
  carac_sections[1451][1] = 0.00;
  carac_sections[1451][2] = 6.76;
  carac_sections[1451][3] = 100.00;
  outputs[1451][0] = 5.34;
  outputs[1451][1] = 3.06;

  // section 1452
  carac_sections[1452][0] = 73759.70;
  carac_sections[1452][1] = 0.00;
  carac_sections[1452][2] = 6.71;
  carac_sections[1452][3] = 100.00;
  outputs[1452][0] = 5.37;
  outputs[1452][1] = 3.04;

  // section 1453
  carac_sections[1453][0] = 73813.48;
  carac_sections[1453][1] = 0.00;
  carac_sections[1453][2] = 6.66;
  carac_sections[1453][3] = 100.00;
  outputs[1453][0] = 5.40;
  outputs[1453][1] = 3.03;

  // section 1454
  carac_sections[1454][0] = 73866.95;
  carac_sections[1454][1] = 0.00;
  carac_sections[1454][2] = 6.61;
  carac_sections[1454][3] = 100.00;
  outputs[1454][0] = 5.43;
  outputs[1454][1] = 3.01;

  // section 1455
  carac_sections[1455][0] = 73920.25;
  carac_sections[1455][1] = 0.00;
  carac_sections[1455][2] = 6.56;
  carac_sections[1455][3] = 100.00;
  outputs[1455][0] = 5.46;
  outputs[1455][1] = 2.99;

  // section 1456
  carac_sections[1456][0] = 73973.23;
  carac_sections[1456][1] = 0.00;
  carac_sections[1456][2] = 6.51;
  carac_sections[1456][3] = 100.00;
  outputs[1456][0] = 5.50;
  outputs[1456][1] = 2.97;

  // section 1457
  carac_sections[1457][0] = 74026.34;
  carac_sections[1457][1] = 0.00;
  carac_sections[1457][2] = 6.46;
  carac_sections[1457][3] = 100.00;
  outputs[1457][0] = 5.53;
  outputs[1457][1] = 2.95;

  // section 1458
  carac_sections[1458][0] = 74076.80;
  carac_sections[1458][1] = 0.00;
  carac_sections[1458][2] = 6.41;
  carac_sections[1458][3] = 100.00;
  outputs[1458][0] = 5.56;
  outputs[1458][1] = 2.93;

  // section 1459
  carac_sections[1459][0] = 74127.05;
  carac_sections[1459][1] = 0.00;
  carac_sections[1459][2] = 6.36;
  carac_sections[1459][3] = 100.00;
  outputs[1459][0] = 5.60;
  outputs[1459][1] = 2.92;

  // section 1460
  carac_sections[1460][0] = 74176.75;
  carac_sections[1460][1] = 0.00;
  carac_sections[1460][2] = 6.31;
  carac_sections[1460][3] = 100.00;
  outputs[1460][0] = 5.64;
  outputs[1460][1] = 2.90;

  // section 1461
  carac_sections[1461][0] = 74226.59;
  carac_sections[1461][1] = 0.00;
  carac_sections[1461][2] = 6.26;
  carac_sections[1461][3] = 100.00;
  outputs[1461][0] = 5.67;
  outputs[1461][1] = 2.88;

  // section 1462
  carac_sections[1462][0] = 74275.14;
  carac_sections[1462][1] = 0.00;
  carac_sections[1462][2] = 6.21;
  carac_sections[1462][3] = 100.00;
  outputs[1462][0] = 5.71;
  outputs[1462][1] = 2.86;

  // section 1463
  carac_sections[1463][0] = 74323.08;
  carac_sections[1463][1] = 0.00;
  carac_sections[1463][2] = 6.17;
  carac_sections[1463][3] = 100.00;
  outputs[1463][0] = 5.74;
  outputs[1463][1] = 2.84;

  // section 1464
  carac_sections[1464][0] = 74371.20;
  carac_sections[1464][1] = 0.00;
  carac_sections[1464][2] = 6.12;
  carac_sections[1464][3] = 100.00;
  outputs[1464][0] = 5.78;
  outputs[1464][1] = 2.83;

  // section 1465
  carac_sections[1465][0] = 74418.80;
  carac_sections[1465][1] = 0.00;
  carac_sections[1465][2] = 6.07;
  carac_sections[1465][3] = 100.00;
  outputs[1465][0] = 5.82;
  outputs[1465][1] = 2.81;

  // section 1466
  carac_sections[1466][0] = 74465.20;
  carac_sections[1466][1] = 0.00;
  carac_sections[1466][2] = 6.02;
  carac_sections[1466][3] = 100.00;
  outputs[1466][0] = 5.85;
  outputs[1466][1] = 2.79;

  // section 1467
  carac_sections[1467][0] = 74511.43;
  carac_sections[1467][1] = 0.00;
  carac_sections[1467][2] = 5.98;
  carac_sections[1467][3] = 100.00;
  outputs[1467][0] = 5.89;
  outputs[1467][1] = 2.77;

  // section 1468
  carac_sections[1468][0] = 74556.80;
  carac_sections[1468][1] = 0.00;
  carac_sections[1468][2] = 5.93;
  carac_sections[1468][3] = 100.00;
  outputs[1468][0] = 5.92;
  outputs[1468][1] = 2.76;

  // section 1469
  carac_sections[1469][0] = 74602.16;
  carac_sections[1469][1] = 0.00;
  carac_sections[1469][2] = 5.89;
  carac_sections[1469][3] = 100.00;
  outputs[1469][0] = 5.96;
  outputs[1469][1] = 2.74;

  // section 1470
  carac_sections[1470][0] = 74646.64;
  carac_sections[1470][1] = 0.00;
  carac_sections[1470][2] = 5.84;
  carac_sections[1470][3] = 100.00;
  outputs[1470][0] = 5.99;
  outputs[1470][1] = 2.73;

  // section 1471
  carac_sections[1471][0] = 74691.02;
  carac_sections[1471][1] = 0.00;
  carac_sections[1471][2] = 5.80;
  carac_sections[1471][3] = 100.00;
  outputs[1471][0] = 6.02;
  outputs[1471][1] = 2.71;

  // section 1472
  carac_sections[1472][0] = 74734.86;
  carac_sections[1472][1] = 0.00;
  carac_sections[1472][2] = 5.76;
  carac_sections[1472][3] = 100.00;
  outputs[1472][0] = 6.06;
  outputs[1472][1] = 2.70;

  // section 1473
  carac_sections[1473][0] = 74780.22;
  carac_sections[1473][1] = 0.00;
  carac_sections[1473][2] = 5.71;
  carac_sections[1473][3] = 100.00;
  outputs[1473][0] = 6.09;
  outputs[1473][1] = 2.68;

  // section 1474
  carac_sections[1474][0] = 74819.19;
  carac_sections[1474][1] = 0.00;
  carac_sections[1474][2] = 5.68;
  carac_sections[1474][3] = 100.00;
  outputs[1474][0] = 6.12;
  outputs[1474][1] = 2.67;

  // section 1475
  carac_sections[1475][0] = 74858.25;
  carac_sections[1475][1] = 0.00;
  carac_sections[1475][2] = 5.64;
  carac_sections[1475][3] = 100.00;
  outputs[1475][0] = 6.15;
  outputs[1475][1] = 2.65;

  // section 1476
  carac_sections[1476][0] = 74896.57;
  carac_sections[1476][1] = 0.00;
  carac_sections[1476][2] = 5.60;
  carac_sections[1476][3] = 100.00;
  outputs[1476][0] = 6.18;
  outputs[1476][1] = 2.64;

  // section 1477
  carac_sections[1477][0] = 74935.30;
  carac_sections[1477][1] = 0.00;
  carac_sections[1477][2] = 5.57;
  carac_sections[1477][3] = 100.00;
  outputs[1477][0] = 6.21;
  outputs[1477][1] = 2.63;

  // section 1478
  carac_sections[1478][0] = 75009.64;
  carac_sections[1478][1] = 0.00;
  carac_sections[1478][2] = 5.50;
  carac_sections[1478][3] = 100.00;
  outputs[1478][0] = 6.26;
  outputs[1478][1] = 2.61;

  // section 1479
  carac_sections[1479][0] = 75083.30;
  carac_sections[1479][1] = 0.00;
  carac_sections[1479][2] = 5.43;
  carac_sections[1479][3] = 100.00;
  outputs[1479][0] = 6.31;
  outputs[1479][1] = 2.59;

  // section 1480
  carac_sections[1480][0] = 75154.21;
  carac_sections[1480][1] = 0.00;
  carac_sections[1480][2] = 5.37;
  carac_sections[1480][3] = 100.00;
  outputs[1480][0] = 6.36;
  outputs[1480][1] = 2.57;

  // section 1481
  carac_sections[1481][0] = 75223.95;
  carac_sections[1481][1] = 0.00;
  carac_sections[1481][2] = 5.31;
  carac_sections[1481][3] = 100.00;
  outputs[1481][0] = 6.41;
  outputs[1481][1] = 2.55;

  // section 1482
  carac_sections[1482][0] = 75292.20;
  carac_sections[1482][1] = 0.00;
  carac_sections[1482][2] = 5.25;
  carac_sections[1482][3] = 100.00;
  outputs[1482][0] = 6.46;
  outputs[1482][1] = 2.53;

  // section 1483
  carac_sections[1483][0] = 75359.68;
  carac_sections[1483][1] = 0.00;
  carac_sections[1483][2] = 5.19;
  carac_sections[1483][3] = 100.00;
  outputs[1483][0] = 6.50;
  outputs[1483][1] = 2.51;

  // section 1484
  carac_sections[1484][0] = 75422.77;
  carac_sections[1484][1] = 0.00;
  carac_sections[1484][2] = 5.14;
  carac_sections[1484][3] = 100.00;
  outputs[1484][0] = 6.54;
  outputs[1484][1] = 2.50;

  // section 1485
  carac_sections[1485][0] = 75484.41;
  carac_sections[1485][1] = 0.00;
  carac_sections[1485][2] = 5.09;
  carac_sections[1485][3] = 100.00;
  outputs[1485][0] = 6.58;
  outputs[1485][1] = 2.48;

  // section 1486
  carac_sections[1486][0] = 75544.54;
  carac_sections[1486][1] = 0.00;
  carac_sections[1486][2] = 5.05;
  carac_sections[1486][3] = 100.00;
  outputs[1486][0] = 6.61;
  outputs[1486][1] = 2.47;

  // section 1487
  carac_sections[1487][0] = 75604.20;
  carac_sections[1487][1] = 0.00;
  carac_sections[1487][2] = 5.00;
  carac_sections[1487][3] = 100.00;
  outputs[1487][0] = 6.65;
  outputs[1487][1] = 2.46;

  // section 1488
  carac_sections[1488][0] = 75661.56;
  carac_sections[1488][1] = 0.00;
  carac_sections[1488][2] = 4.96;
  carac_sections[1488][3] = 100.00;
  outputs[1488][0] = 6.68;
  outputs[1488][1] = 2.44;

  // section 1489
  carac_sections[1489][0] = 75718.27;
  carac_sections[1489][1] = 0.00;
  carac_sections[1489][2] = 4.92;
  carac_sections[1489][3] = 100.00;
  outputs[1489][0] = 6.71;
  outputs[1489][1] = 2.43;

  // section 1490
  carac_sections[1490][0] = 75773.87;
  carac_sections[1490][1] = 0.00;
  carac_sections[1490][2] = 4.88;
  carac_sections[1490][3] = 100.00;
  outputs[1490][0] = 6.74;
  outputs[1490][1] = 2.42;

  // section 1491
  carac_sections[1491][0] = 75829.95;
  carac_sections[1491][1] = 0.00;
  carac_sections[1491][2] = 4.85;
  carac_sections[1491][3] = 100.00;
  outputs[1491][0] = 6.77;
  outputs[1491][1] = 2.41;

  // section 1492
  carac_sections[1492][0] = 75878.96;
  carac_sections[1492][1] = 0.00;
  carac_sections[1492][2] = 4.81;
  carac_sections[1492][3] = 100.00;
  outputs[1492][0] = 6.79;
  outputs[1492][1] = 2.40;

  // section 1493
  carac_sections[1493][0] = 75927.04;
  carac_sections[1493][1] = 0.00;
  carac_sections[1493][2] = 4.78;
  carac_sections[1493][3] = 100.00;
  outputs[1493][0] = 6.81;
  outputs[1493][1] = 2.40;

  // section 1494
  carac_sections[1494][0] = 75974.07;
  carac_sections[1494][1] = 0.00;
  carac_sections[1494][2] = 4.76;
  carac_sections[1494][3] = 100.00;
  outputs[1494][0] = 6.83;
  outputs[1494][1] = 2.39;

  // section 1495
  carac_sections[1495][0] = 76021.38;
  carac_sections[1495][1] = 0.00;
  carac_sections[1495][2] = 4.73;
  carac_sections[1495][3] = 100.00;
  outputs[1495][0] = 6.85;
  outputs[1495][1] = 2.38;

  // section 1496
  carac_sections[1496][0] = 76066.09;
  carac_sections[1496][1] = 0.00;
  carac_sections[1496][2] = 4.70;
  carac_sections[1496][3] = 100.00;
  outputs[1496][0] = 6.87;
  outputs[1496][1] = 2.38;

  // section 1497
  carac_sections[1497][0] = 76110.73;
  carac_sections[1497][1] = 0.00;
  carac_sections[1497][2] = 4.68;
  carac_sections[1497][3] = 100.00;
  outputs[1497][0] = 6.89;
  outputs[1497][1] = 2.37;

  // section 1498
  carac_sections[1498][0] = 76154.41;
  carac_sections[1498][1] = 0.00;
  carac_sections[1498][2] = 4.66;
  carac_sections[1498][3] = 100.00;
  outputs[1498][0] = 6.90;
  outputs[1498][1] = 2.37;

  // section 1499
  carac_sections[1499][0] = 76199.08;
  carac_sections[1499][1] = 0.00;
  carac_sections[1499][2] = 4.63;
  carac_sections[1499][3] = 100.00;
  outputs[1499][0] = 6.92;
  outputs[1499][1] = 2.36;

  // section 1500
  carac_sections[1500][0] = 76239.03;
  carac_sections[1500][1] = 0.00;
  carac_sections[1500][2] = 4.61;
  carac_sections[1500][3] = 100.00;
  outputs[1500][0] = 6.93;
  outputs[1500][1] = 2.36;

  // section 1501
  carac_sections[1501][0] = 76279.12;
  carac_sections[1501][1] = 0.00;
  carac_sections[1501][2] = 4.59;
  carac_sections[1501][3] = 100.00;
  outputs[1501][0] = 6.94;
  outputs[1501][1] = 2.35;

  // section 1502
  carac_sections[1502][0] = 76318.06;
  carac_sections[1502][1] = 0.00;
  carac_sections[1502][2] = 4.58;
  carac_sections[1502][3] = 100.00;
  outputs[1502][0] = 6.95;
  outputs[1502][1] = 2.35;

  // section 1503
  carac_sections[1503][0] = 76357.67;
  carac_sections[1503][1] = 0.00;
  carac_sections[1503][2] = 4.56;
  carac_sections[1503][3] = 100.00;
  outputs[1503][0] = 6.96;
  outputs[1503][1] = 2.35;

  // section 1504
  carac_sections[1504][0] = 76432.51;
  carac_sections[1504][1] = 0.00;
  carac_sections[1504][2] = 4.53;
  carac_sections[1504][3] = 100.00;
  outputs[1504][0] = 6.98;
  outputs[1504][1] = 2.34;

  // section 1505
  carac_sections[1505][0] = 76506.40;
  carac_sections[1505][1] = 0.00;
  carac_sections[1505][2] = 4.50;
  carac_sections[1505][3] = 100.00;
  outputs[1505][0] = 7.00;
  outputs[1505][1] = 2.33;

  // section 1506
  carac_sections[1506][0] = 76572.88;
  carac_sections[1506][1] = 0.00;
  carac_sections[1506][2] = 4.48;
  carac_sections[1506][3] = 100.00;
  outputs[1506][0] = 7.01;
  outputs[1506][1] = 2.33;

  // section 1507
  carac_sections[1507][0] = 76636.95;
  carac_sections[1507][1] = 0.00;
  carac_sections[1507][2] = 4.45;
  carac_sections[1507][3] = 100.00;
  outputs[1507][0] = 7.02;
  outputs[1507][1] = 2.33;

  // section 1508
  carac_sections[1508][0] = 76698.69;
  carac_sections[1508][1] = 0.00;
  carac_sections[1508][2] = 4.44;
  carac_sections[1508][3] = 100.00;
  outputs[1508][0] = 7.03;
  outputs[1508][1] = 2.32;

  // section 1509
  carac_sections[1509][0] = 76759.80;
  carac_sections[1509][1] = 0.00;
  carac_sections[1509][2] = 4.42;
  carac_sections[1509][3] = 100.00;
  outputs[1509][0] = 7.03;
  outputs[1509][1] = 2.32;

  // section 1510
  carac_sections[1510][0] = 76816.48;
  carac_sections[1510][1] = 0.00;
  carac_sections[1510][2] = 4.41;
  carac_sections[1510][3] = 100.00;
  outputs[1510][0] = 7.04;
  outputs[1510][1] = 2.32;

  // section 1511
  carac_sections[1511][0] = 76872.09;
  carac_sections[1511][1] = 0.00;
  carac_sections[1511][2] = 4.39;
  carac_sections[1511][3] = 100.00;
  outputs[1511][0] = 7.04;
  outputs[1511][1] = 2.32;

  // section 1512
  carac_sections[1512][0] = 76926.01;
  carac_sections[1512][1] = 0.00;
  carac_sections[1512][2] = 4.38;
  carac_sections[1512][3] = 100.00;
  outputs[1512][0] = 7.04;
  outputs[1512][1] = 2.32;

  // section 1513
  carac_sections[1513][0] = 76980.71;
  carac_sections[1513][1] = 0.00;
  carac_sections[1513][2] = 4.37;
  carac_sections[1513][3] = 100.00;
  outputs[1513][0] = 7.04;
  outputs[1513][1] = 2.32;

  // section 1514
  carac_sections[1514][0] = 77027.52;
  carac_sections[1514][1] = 0.00;
  carac_sections[1514][2] = 4.36;
  carac_sections[1514][3] = 100.00;
  outputs[1514][0] = 7.04;
  outputs[1514][1] = 2.32;

  // section 1515
  carac_sections[1515][0] = 77073.81;
  carac_sections[1515][1] = 0.00;
  carac_sections[1515][2] = 4.36;
  carac_sections[1515][3] = 100.00;
  outputs[1515][0] = 7.03;
  outputs[1515][1] = 2.32;

  // section 1516
  carac_sections[1516][0] = 77118.46;
  carac_sections[1516][1] = 0.00;
  carac_sections[1516][2] = 4.35;
  carac_sections[1516][3] = 100.00;
  outputs[1516][0] = 7.03;
  outputs[1516][1] = 2.32;

  // section 1517
  carac_sections[1517][0] = 77163.99;
  carac_sections[1517][1] = 0.00;
  carac_sections[1517][2] = 4.35;
  carac_sections[1517][3] = 100.00;
  outputs[1517][0] = 7.03;
  outputs[1517][1] = 2.32;

  // section 1518
  carac_sections[1518][0] = 77205.03;
  carac_sections[1518][1] = 0.00;
  carac_sections[1518][2] = 4.35;
  carac_sections[1518][3] = 100.00;
  outputs[1518][0] = 7.02;
  outputs[1518][1] = 2.33;

  // section 1519
  carac_sections[1519][0] = 77246.25;
  carac_sections[1519][1] = 0.00;
  carac_sections[1519][2] = 4.34;
  carac_sections[1519][3] = 100.00;
  outputs[1519][0] = 7.02;
  outputs[1519][1] = 2.33;

  // section 1520
  carac_sections[1520][0] = 77286.03;
  carac_sections[1520][1] = 0.00;
  carac_sections[1520][2] = 4.34;
  carac_sections[1520][3] = 100.00;
  outputs[1520][0] = 7.01;
  outputs[1520][1] = 2.33;

  // section 1521
  carac_sections[1521][0] = 77326.58;
  carac_sections[1521][1] = 0.00;
  carac_sections[1521][2] = 4.34;
  carac_sections[1521][3] = 100.00;
  outputs[1521][0] = 7.00;
  outputs[1521][1] = 2.33;

  // section 1522
  carac_sections[1522][0] = 77396.86;
  carac_sections[1522][1] = 0.00;
  carac_sections[1522][2] = 4.34;
  carac_sections[1522][3] = 100.00;
  outputs[1522][0] = 6.99;
  outputs[1522][1] = 2.34;

  // section 1523
  carac_sections[1523][0] = 77465.01;
  carac_sections[1523][1] = 0.00;
  carac_sections[1523][2] = 4.34;
  carac_sections[1523][3] = 100.00;
  outputs[1523][0] = 6.97;
  outputs[1523][1] = 2.34;

  // section 1524
  carac_sections[1524][0] = 77528.89;
  carac_sections[1524][1] = 0.00;
  carac_sections[1524][2] = 4.35;
  carac_sections[1524][3] = 100.00;
  outputs[1524][0] = 6.95;
  outputs[1524][1] = 2.35;

  // section 1525
  carac_sections[1525][0] = 77591.66;
  carac_sections[1525][1] = 0.00;
  carac_sections[1525][2] = 4.35;
  carac_sections[1525][3] = 100.00;
  outputs[1525][0] = 6.94;
  outputs[1525][1] = 2.35;

  // section 1526
  carac_sections[1526][0] = 77647.04;
  carac_sections[1526][1] = 0.00;
  carac_sections[1526][2] = 4.36;
  carac_sections[1526][3] = 100.00;
  outputs[1526][0] = 6.92;
  outputs[1526][1] = 2.36;

  // section 1527
  carac_sections[1527][0] = 77701.17;
  carac_sections[1527][1] = 0.00;
  carac_sections[1527][2] = 4.36;
  carac_sections[1527][3] = 100.00;
  outputs[1527][0] = 6.90;
  outputs[1527][1] = 2.37;

  // section 1528
  carac_sections[1528][0] = 77751.68;
  carac_sections[1528][1] = 0.00;
  carac_sections[1528][2] = 4.37;
  carac_sections[1528][3] = 100.00;
  outputs[1528][0] = 6.88;
  outputs[1528][1] = 2.37;

  // section 1529
  carac_sections[1529][0] = 77802.66;
  carac_sections[1529][1] = 0.00;
  carac_sections[1529][2] = 4.38;
  carac_sections[1529][3] = 100.00;
  outputs[1529][0] = 6.86;
  outputs[1529][1] = 2.38;

  // section 1530
  carac_sections[1530][0] = 77846.91;
  carac_sections[1530][1] = 0.00;
  carac_sections[1530][2] = 4.39;
  carac_sections[1530][3] = 100.00;
  outputs[1530][0] = 6.84;
  outputs[1530][1] = 2.39;

  // section 1531
  carac_sections[1531][0] = 77891.34;
  carac_sections[1531][1] = 0.00;
  carac_sections[1531][2] = 4.40;
  carac_sections[1531][3] = 100.00;
  outputs[1531][0] = 6.82;
  outputs[1531][1] = 2.39;

  // section 1532
  carac_sections[1532][0] = 77932.46;
  carac_sections[1532][1] = 0.00;
  carac_sections[1532][2] = 4.41;
  carac_sections[1532][3] = 100.00;
  outputs[1532][0] = 6.80;
  outputs[1532][1] = 2.40;

  // section 1533
  carac_sections[1533][0] = 77974.71;
  carac_sections[1533][1] = 0.00;
  carac_sections[1533][2] = 4.42;
  carac_sections[1533][3] = 100.00;
  outputs[1533][0] = 6.78;
  outputs[1533][1] = 2.41;

  // section 1534
  carac_sections[1534][0] = 78047.14;
  carac_sections[1534][1] = 0.00;
  carac_sections[1534][2] = 4.43;
  carac_sections[1534][3] = 100.00;
  outputs[1534][0] = 6.74;
  outputs[1534][1] = 2.42;

  // section 1535
  carac_sections[1535][0] = 78116.55;
  carac_sections[1535][1] = 0.00;
  carac_sections[1535][2] = 4.45;
  carac_sections[1535][3] = 100.00;
  outputs[1535][0] = 6.71;
  outputs[1535][1] = 2.44;

  // section 1536
  carac_sections[1536][0] = 78179.16;
  carac_sections[1536][1] = 0.00;
  carac_sections[1536][2] = 4.47;
  carac_sections[1536][3] = 100.00;
  outputs[1536][0] = 6.67;
  outputs[1536][1] = 2.45;

  // section 1537
  carac_sections[1537][0] = 78239.13;
  carac_sections[1537][1] = 0.00;
  carac_sections[1537][2] = 4.49;
  carac_sections[1537][3] = 100.00;
  outputs[1537][0] = 6.63;
  outputs[1537][1] = 2.46;

  // section 1538
  carac_sections[1538][0] = 78292.97;
  carac_sections[1538][1] = 0.00;
  carac_sections[1538][2] = 4.51;
  carac_sections[1538][3] = 100.00;
  outputs[1538][0] = 6.60;
  outputs[1538][1] = 2.47;

  // section 1539
  carac_sections[1539][0] = 78345.58;
  carac_sections[1539][1] = 0.00;
  carac_sections[1539][2] = 4.53;
  carac_sections[1539][3] = 100.00;
  outputs[1539][0] = 6.57;
  outputs[1539][1] = 2.49;

  // section 1540
  carac_sections[1540][0] = 78393.20;
  carac_sections[1540][1] = 0.00;
  carac_sections[1540][2] = 4.55;
  carac_sections[1540][3] = 100.00;
  outputs[1540][0] = 6.53;
  outputs[1540][1] = 2.50;

  // section 1541
  carac_sections[1541][0] = 78439.30;
  carac_sections[1541][1] = 0.00;
  carac_sections[1541][2] = 4.56;
  carac_sections[1541][3] = 100.00;
  outputs[1541][0] = 6.50;
  outputs[1541][1] = 2.51;

  // section 1542
  carac_sections[1542][0] = 78482.89;
  carac_sections[1542][1] = 0.00;
  carac_sections[1542][2] = 4.58;
  carac_sections[1542][3] = 100.00;
  outputs[1542][0] = 6.47;
  outputs[1542][1] = 2.52;

  // section 1543
  carac_sections[1543][0] = 78524.37;
  carac_sections[1543][1] = 0.00;
  carac_sections[1543][2] = 4.60;
  carac_sections[1543][3] = 100.00;
  outputs[1543][0] = 6.44;
  outputs[1543][1] = 2.54;

  // section 1544
  carac_sections[1544][0] = 78564.43;
  carac_sections[1544][1] = 0.00;
  carac_sections[1544][2] = 4.62;
  carac_sections[1544][3] = 100.00;
  outputs[1544][0] = 6.41;
  outputs[1544][1] = 2.55;

  // section 1545
  carac_sections[1545][0] = 78602.94;
  carac_sections[1545][1] = 0.00;
  carac_sections[1545][2] = 4.63;
  carac_sections[1545][3] = 100.00;
  outputs[1545][0] = 6.38;
  outputs[1545][1] = 2.56;

  // section 1546
  carac_sections[1546][0] = 78640.36;
  carac_sections[1546][1] = 0.00;
  carac_sections[1546][2] = 4.65;
  carac_sections[1546][3] = 100.00;
  outputs[1546][0] = 6.35;
  outputs[1546][1] = 2.57;

  // section 1547
  carac_sections[1547][0] = 78677.20;
  carac_sections[1547][1] = 0.00;
  carac_sections[1547][2] = 4.66;
  carac_sections[1547][3] = 100.00;
  outputs[1547][0] = 6.32;
  outputs[1547][1] = 2.58;

  // section 1548
  carac_sections[1548][0] = 78713.00;
  carac_sections[1548][1] = 0.00;
  carac_sections[1548][2] = 4.68;
  carac_sections[1548][3] = 100.00;
  outputs[1548][0] = 6.29;
  outputs[1548][1] = 2.59;

  // section 1549
  carac_sections[1549][0] = 78749.14;
  carac_sections[1549][1] = 0.00;
  carac_sections[1549][2] = 4.70;
  carac_sections[1549][3] = 100.00;
  outputs[1549][0] = 6.26;
  outputs[1549][1] = 2.61;

  // section 1550
  carac_sections[1550][0] = 78784.80;
  carac_sections[1550][1] = 0.00;
  carac_sections[1550][2] = 4.72;
  carac_sections[1550][3] = 100.00;
  outputs[1550][0] = 6.23;
  outputs[1550][1] = 2.62;

  // section 1551
  carac_sections[1551][0] = 78821.45;
  carac_sections[1551][1] = 0.00;
  carac_sections[1551][2] = 4.73;
  carac_sections[1551][3] = 100.00;
  outputs[1551][0] = 6.20;
  outputs[1551][1] = 2.63;

  // section 1552
  carac_sections[1552][0] = 78857.87;
  carac_sections[1552][1] = 0.00;
  carac_sections[1552][2] = 4.75;
  carac_sections[1552][3] = 100.00;
  outputs[1552][0] = 6.17;
  outputs[1552][1] = 2.65;

  // section 1553
  carac_sections[1553][0] = 78896.37;
  carac_sections[1553][1] = 0.00;
  carac_sections[1553][2] = 4.77;
  carac_sections[1553][3] = 100.00;
  outputs[1553][0] = 6.14;
  outputs[1553][1] = 2.66;

  // section 1554
  carac_sections[1554][0] = 78935.19;
  carac_sections[1554][1] = 0.00;
  carac_sections[1554][2] = 4.79;
  carac_sections[1554][3] = 100.00;
  outputs[1554][0] = 6.10;
  outputs[1554][1] = 2.68;

  // section 1555
  carac_sections[1555][0] = 78977.10;
  carac_sections[1555][1] = 0.00;
  carac_sections[1555][2] = 4.81;
  carac_sections[1555][3] = 100.00;
  outputs[1555][0] = 6.06;
  outputs[1555][1] = 2.69;

  // section 1556
  carac_sections[1556][0] = 79020.34;
  carac_sections[1556][1] = 0.00;
  carac_sections[1556][2] = 4.83;
  carac_sections[1556][3] = 100.00;
  outputs[1556][0] = 6.02;
  outputs[1556][1] = 2.71;

  // section 1557
  carac_sections[1557][0] = 79068.80;
  carac_sections[1557][1] = 0.00;
  carac_sections[1557][2] = 4.86;
  carac_sections[1557][3] = 100.00;
  outputs[1557][0] = 5.98;
  outputs[1557][1] = 2.73;

  // section 1558
  carac_sections[1558][0] = 79120.91;
  carac_sections[1558][1] = 0.00;
  carac_sections[1558][2] = 4.88;
  carac_sections[1558][3] = 100.00;
  outputs[1558][0] = 5.93;
  outputs[1558][1] = 2.76;

  // section 1559
  carac_sections[1559][0] = 79183.44;
  carac_sections[1559][1] = 0.00;
  carac_sections[1559][2] = 4.92;
  carac_sections[1559][3] = 100.00;
  outputs[1559][0] = 5.87;
  outputs[1559][1] = 2.78;

  // section 1560
  carac_sections[1560][0] = 79214.15;
  carac_sections[1560][1] = 0.00;
  carac_sections[1560][2] = 4.93;
  carac_sections[1560][3] = 100.00;
  outputs[1560][0] = 5.83;
  outputs[1560][1] = 2.80;

  // section 1561
  carac_sections[1561][0] = 79248.40;
  carac_sections[1561][1] = 0.00;
  carac_sections[1561][2] = 4.95;
  carac_sections[1561][3] = 100.00;
  outputs[1561][0] = 5.80;
  outputs[1561][1] = 2.82;

  // section 1562
  carac_sections[1562][0] = 79289.50;
  carac_sections[1562][1] = 0.00;
  carac_sections[1562][2] = 4.97;
  carac_sections[1562][3] = 100.00;
  outputs[1562][0] = 5.76;
  outputs[1562][1] = 2.84;

  // section 1563
  carac_sections[1563][0] = 79333.00;
  carac_sections[1563][1] = 0.00;
  carac_sections[1563][2] = 4.99;
  carac_sections[1563][3] = 100.00;
  outputs[1563][0] = 5.71;
  outputs[1563][1] = 2.86;

  // section 1564
  carac_sections[1564][0] = 79381.61;
  carac_sections[1564][1] = 0.00;
  carac_sections[1564][2] = 5.02;
  carac_sections[1564][3] = 100.00;
  outputs[1564][0] = 5.66;
  outputs[1564][1] = 2.88;

  // section 1565
  carac_sections[1565][0] = 79436.77;
  carac_sections[1565][1] = 0.00;
  carac_sections[1565][2] = 5.05;
  carac_sections[1565][3] = 100.00;
  outputs[1565][0] = 5.61;
  outputs[1565][1] = 2.91;

  // section 1566
  carac_sections[1566][0] = 79502.37;
  carac_sections[1566][1] = 0.00;
  carac_sections[1566][2] = 5.08;
  carac_sections[1566][3] = 100.00;
  outputs[1566][0] = 5.54;
  outputs[1566][1] = 2.95;

  // section 1567
  carac_sections[1567][0] = 79537.91;
  carac_sections[1567][1] = 0.00;
  carac_sections[1567][2] = 5.10;
  carac_sections[1567][3] = 100.00;
  outputs[1567][0] = 5.50;
  outputs[1567][1] = 2.97;

  // section 1568
  carac_sections[1568][0] = 79575.70;
  carac_sections[1568][1] = 0.00;
  carac_sections[1568][2] = 5.11;
  carac_sections[1568][3] = 100.00;
  outputs[1568][0] = 5.46;
  outputs[1568][1] = 2.99;

  // section 1569
  carac_sections[1569][0] = 79620.37;
  carac_sections[1569][1] = 0.00;
  carac_sections[1569][2] = 5.13;
  carac_sections[1569][3] = 100.00;
  outputs[1569][0] = 5.41;
  outputs[1569][1] = 3.02;

  // section 1570
  carac_sections[1570][0] = 79667.36;
  carac_sections[1570][1] = 0.00;
  carac_sections[1570][2] = 5.15;
  carac_sections[1570][3] = 100.00;
  outputs[1570][0] = 5.36;
  outputs[1570][1] = 3.05;

  // section 1571
  carac_sections[1571][0] = 79722.31;
  carac_sections[1571][1] = 0.00;
  carac_sections[1571][2] = 5.18;
  carac_sections[1571][3] = 100.00;
  outputs[1571][0] = 5.30;
  outputs[1571][1] = 3.08;

  // section 1572
  carac_sections[1572][0] = 79782.59;
  carac_sections[1572][1] = 0.00;
  carac_sections[1572][2] = 5.20;
  carac_sections[1572][3] = 100.00;
  outputs[1572][0] = 5.24;
  outputs[1572][1] = 3.12;

  // section 1573
  carac_sections[1573][0] = 79851.52;
  carac_sections[1573][1] = 0.00;
  carac_sections[1573][2] = 5.23;
  carac_sections[1573][3] = 100.00;
  outputs[1573][0] = 5.17;
  outputs[1573][1] = 3.16;

  // section 1574
  carac_sections[1574][0] = 79889.62;
  carac_sections[1574][1] = 0.00;
  carac_sections[1574][2] = 5.24;
  carac_sections[1574][3] = 100.00;
  outputs[1574][0] = 5.13;
  outputs[1574][1] = 3.18;

  // section 1575
  carac_sections[1575][0] = 79928.08;
  carac_sections[1575][1] = 0.00;
  carac_sections[1575][2] = 5.25;
  carac_sections[1575][3] = 100.00;
  outputs[1575][0] = 5.09;
  outputs[1575][1] = 3.21;

  // section 1576
  carac_sections[1576][0] = 79974.95;
  carac_sections[1576][1] = 0.00;
  carac_sections[1576][2] = 5.26;
  carac_sections[1576][3] = 100.00;
  outputs[1576][0] = 5.04;
  outputs[1576][1] = 3.24;

  // section 1577
  carac_sections[1577][0] = 80022.33;
  carac_sections[1577][1] = 0.00;
  carac_sections[1577][2] = 5.27;
  carac_sections[1577][3] = 100.00;
  outputs[1577][0] = 4.99;
  outputs[1577][1] = 3.27;

  // section 1578
  carac_sections[1578][0] = 80075.33;
  carac_sections[1578][1] = 0.00;
  carac_sections[1578][2] = 5.28;
  carac_sections[1578][3] = 100.00;
  outputs[1578][0] = 4.94;
  outputs[1578][1] = 3.31;

  // section 1579
  carac_sections[1579][0] = 80131.27;
  carac_sections[1579][1] = 0.00;
  carac_sections[1579][2] = 5.29;
  carac_sections[1579][3] = 100.00;
  outputs[1579][0] = 4.89;
  outputs[1579][1] = 3.34;

  // section 1580
  carac_sections[1580][0] = 80192.84;
  carac_sections[1580][1] = 0.00;
  carac_sections[1580][2] = 5.30;
  carac_sections[1580][3] = 100.00;
  outputs[1580][0] = 4.83;
  outputs[1580][1] = 3.38;

  // section 1581
  carac_sections[1581][0] = 80258.73;
  carac_sections[1581][1] = 0.00;
  carac_sections[1581][2] = 5.30;
  carac_sections[1581][3] = 100.00;
  outputs[1581][0] = 4.77;
  outputs[1581][1] = 3.42;

  // section 1582
  carac_sections[1582][0] = 80330.80;
  carac_sections[1582][1] = 0.00;
  carac_sections[1582][2] = 5.30;
  carac_sections[1582][3] = 100.00;
  outputs[1582][0] = 4.72;
  outputs[1582][1] = 3.46;

  // section 1583
  carac_sections[1583][0] = 80369.06;
  carac_sections[1583][1] = 0.00;
  carac_sections[1583][2] = 5.29;
  carac_sections[1583][3] = 100.00;
  outputs[1583][0] = 4.68;
  outputs[1583][1] = 3.49;

  // section 1584
  carac_sections[1584][0] = 80407.20;
  carac_sections[1584][1] = 0.00;
  carac_sections[1584][2] = 5.29;
  carac_sections[1584][3] = 100.00;
  outputs[1584][0] = 4.65;
  outputs[1584][1] = 3.51;

  // section 1585
  carac_sections[1585][0] = 80450.87;
  carac_sections[1585][1] = 0.00;
  carac_sections[1585][2] = 5.28;
  carac_sections[1585][3] = 100.00;
  outputs[1585][0] = 4.62;
  outputs[1585][1] = 3.53;

  // section 1586
  carac_sections[1586][0] = 80493.59;
  carac_sections[1586][1] = 0.00;
  carac_sections[1586][2] = 5.27;
  carac_sections[1586][3] = 100.00;
  outputs[1586][0] = 4.59;
  outputs[1586][1] = 3.55;

  // section 1587
  carac_sections[1587][0] = 80539.25;
  carac_sections[1587][1] = 0.00;
  carac_sections[1587][2] = 5.25;
  carac_sections[1587][3] = 100.00;
  outputs[1587][0] = 4.57;
  outputs[1587][1] = 3.58;

  // section 1588
  carac_sections[1588][0] = 80585.13;
  carac_sections[1588][1] = 0.00;
  carac_sections[1588][2] = 5.24;
  carac_sections[1588][3] = 100.00;
  outputs[1588][0] = 4.54;
  outputs[1588][1] = 3.60;

  // section 1589
  carac_sections[1589][0] = 80634.65;
  carac_sections[1589][1] = 0.00;
  carac_sections[1589][2] = 5.22;
  carac_sections[1589][3] = 100.00;
  outputs[1589][0] = 4.51;
  outputs[1589][1] = 3.62;

  // section 1590
  carac_sections[1590][0] = 80684.35;
  carac_sections[1590][1] = 0.00;
  carac_sections[1590][2] = 5.20;
  carac_sections[1590][3] = 100.00;
  outputs[1590][0] = 4.49;
  outputs[1590][1] = 3.64;

  // section 1591
  carac_sections[1591][0] = 80736.05;
  carac_sections[1591][1] = 0.00;
  carac_sections[1591][2] = 5.17;
  carac_sections[1591][3] = 100.00;
  outputs[1591][0] = 4.47;
  outputs[1591][1] = 3.66;

  // section 1592
  carac_sections[1592][0] = 80788.27;
  carac_sections[1592][1] = 0.00;
  carac_sections[1592][2] = 5.14;
  carac_sections[1592][3] = 100.00;
  outputs[1592][0] = 4.44;
  outputs[1592][1] = 3.67;

  // section 1593
  carac_sections[1593][0] = 80841.94;
  carac_sections[1593][1] = 0.00;
  carac_sections[1593][2] = 5.11;
  carac_sections[1593][3] = 100.00;
  outputs[1593][0] = 4.43;
  outputs[1593][1] = 3.69;

  // section 1594
  carac_sections[1594][0] = 80896.09;
  carac_sections[1594][1] = 0.00;
  carac_sections[1594][2] = 5.08;
  carac_sections[1594][3] = 100.00;
  outputs[1594][0] = 4.41;
  outputs[1594][1] = 3.70;

  // section 1595
  carac_sections[1595][0] = 80951.49;
  carac_sections[1595][1] = 0.00;
  carac_sections[1595][2] = 5.04;
  carac_sections[1595][3] = 100.00;
  outputs[1595][0] = 4.40;
  outputs[1595][1] = 3.71;

  // section 1596
  carac_sections[1596][0] = 81007.27;
  carac_sections[1596][1] = 0.00;
  carac_sections[1596][2] = 5.00;
  carac_sections[1596][3] = 100.00;
  outputs[1596][0] = 4.39;
  outputs[1596][1] = 3.72;

  // section 1597
  carac_sections[1597][0] = 81064.17;
  carac_sections[1597][1] = 0.00;
  carac_sections[1597][2] = 4.96;
  carac_sections[1597][3] = 100.00;
  outputs[1597][0] = 4.38;
  outputs[1597][1] = 3.73;

  // section 1598
  carac_sections[1598][0] = 81121.27;
  carac_sections[1598][1] = 0.00;
  carac_sections[1598][2] = 4.91;
  carac_sections[1598][3] = 100.00;
  outputs[1598][0] = 4.38;
  outputs[1598][1] = 3.73;

  // section 1599
  carac_sections[1599][0] = 81178.82;
  carac_sections[1599][1] = 0.00;
  carac_sections[1599][2] = 4.86;
  carac_sections[1599][3] = 100.00;
  outputs[1599][0] = 4.38;
  outputs[1599][1] = 3.73;

  // section 1600
  carac_sections[1600][0] = 81236.46;
  carac_sections[1600][1] = 0.00;
  carac_sections[1600][2] = 4.81;
  carac_sections[1600][3] = 100.00;
  outputs[1600][0] = 4.38;
  outputs[1600][1] = 3.73;

  // section 1601
  carac_sections[1601][0] = 81294.50;
  carac_sections[1601][1] = 0.00;
  carac_sections[1601][2] = 4.76;
  carac_sections[1601][3] = 100.00;
  outputs[1601][0] = 4.38;
  outputs[1601][1] = 3.73;

  // section 1602
  carac_sections[1602][0] = 81352.28;
  carac_sections[1602][1] = 0.00;
  carac_sections[1602][2] = 4.71;
  carac_sections[1602][3] = 100.00;
  outputs[1602][0] = 4.38;
  outputs[1602][1] = 3.72;

  // section 1603
  carac_sections[1603][0] = 81409.99;
  carac_sections[1603][1] = 0.00;
  carac_sections[1603][2] = 4.65;
  carac_sections[1603][3] = 100.00;
  outputs[1603][0] = 4.39;
  outputs[1603][1] = 3.72;

  // section 1604
  carac_sections[1604][0] = 81467.45;
  carac_sections[1604][1] = 0.00;
  carac_sections[1604][2] = 4.60;
  carac_sections[1604][3] = 100.00;
  outputs[1604][0] = 4.40;
  outputs[1604][1] = 3.71;

  // section 1605
  carac_sections[1605][0] = 81524.48;
  carac_sections[1605][1] = 0.00;
  carac_sections[1605][2] = 4.54;
  carac_sections[1605][3] = 100.00;
  outputs[1605][0] = 4.41;
  outputs[1605][1] = 3.70;

  // section 1606
  carac_sections[1606][0] = 81581.14;
  carac_sections[1606][1] = 0.00;
  carac_sections[1606][2] = 4.49;
  carac_sections[1606][3] = 100.00;
  outputs[1606][0] = 4.42;
  outputs[1606][1] = 3.69;

  // section 1607
  carac_sections[1607][0] = 81636.98;
  carac_sections[1607][1] = 0.00;
  carac_sections[1607][2] = 4.43;
  carac_sections[1607][3] = 100.00;
  outputs[1607][0] = 4.44;
  outputs[1607][1] = 3.68;

  // section 1608
  carac_sections[1608][0] = 81692.84;
  carac_sections[1608][1] = 0.00;
  carac_sections[1608][2] = 4.38;
  carac_sections[1608][3] = 100.00;
  outputs[1608][0] = 4.45;
  outputs[1608][1] = 3.67;

  // section 1609
  carac_sections[1609][0] = 81744.07;
  carac_sections[1609][1] = 0.00;
  carac_sections[1609][2] = 4.33;
  carac_sections[1609][3] = 100.00;
  outputs[1609][0] = 4.47;
  outputs[1609][1] = 3.66;

  // section 1610
  carac_sections[1610][0] = 81794.63;
  carac_sections[1610][1] = 0.00;
  carac_sections[1610][2] = 4.27;
  carac_sections[1610][3] = 100.00;
  outputs[1610][0] = 4.48;
  outputs[1610][1] = 3.64;

  // section 1611
  carac_sections[1611][0] = 81843.06;
  carac_sections[1611][1] = 0.00;
  carac_sections[1611][2] = 4.23;
  carac_sections[1611][3] = 100.00;
  outputs[1611][0] = 4.50;
  outputs[1611][1] = 3.63;

  // section 1612
  carac_sections[1612][0] = 81891.28;
  carac_sections[1612][1] = 0.00;
  carac_sections[1612][2] = 4.18;
  carac_sections[1612][3] = 100.00;
  outputs[1612][0] = 4.51;
  outputs[1612][1] = 3.62;

  // section 1613
  carac_sections[1613][0] = 81936.70;
  carac_sections[1613][1] = 0.00;
  carac_sections[1613][2] = 4.13;
  carac_sections[1613][3] = 100.00;
  outputs[1613][0] = 4.52;
  outputs[1613][1] = 3.61;

  // section 1614
  carac_sections[1614][0] = 81982.23;
  carac_sections[1614][1] = 0.00;
  carac_sections[1614][2] = 4.09;
  carac_sections[1614][3] = 100.00;
  outputs[1614][0] = 4.54;
  outputs[1614][1] = 3.60;

  // section 1615
  carac_sections[1615][0] = 82023.99;
  carac_sections[1615][1] = 0.00;
  carac_sections[1615][2] = 4.05;
  carac_sections[1615][3] = 100.00;
  outputs[1615][0] = 4.55;
  outputs[1615][1] = 3.59;

  // section 1616
  carac_sections[1616][0] = 82066.66;
  carac_sections[1616][1] = 0.00;
  carac_sections[1616][2] = 4.00;
  carac_sections[1616][3] = 100.00;
  outputs[1616][0] = 4.56;
  outputs[1616][1] = 3.58;

  // section 1617
  carac_sections[1617][0] = 82140.14;
  carac_sections[1617][1] = 0.00;
  carac_sections[1617][2] = 3.93;
  carac_sections[1617][3] = 100.00;
  outputs[1617][0] = 4.59;
  outputs[1617][1] = 3.56;

  // section 1618
  carac_sections[1618][0] = 82206.92;
  carac_sections[1618][1] = 0.00;
  carac_sections[1618][2] = 3.87;
  carac_sections[1618][3] = 100.00;
  outputs[1618][0] = 4.62;
  outputs[1618][1] = 3.54;

  // section 1619
  carac_sections[1619][0] = 82264.34;
  carac_sections[1619][1] = 0.00;
  carac_sections[1619][2] = 3.81;
  carac_sections[1619][3] = 100.00;
  outputs[1619][0] = 4.63;
  outputs[1619][1] = 3.52;

  // section 1620
  carac_sections[1620][0] = 82313.05;
  carac_sections[1620][1] = 0.00;
  carac_sections[1620][2] = 3.77;
  carac_sections[1620][3] = 100.00;
  outputs[1620][0] = 4.65;
  outputs[1620][1] = 3.51;

  // section 1621
  carac_sections[1621][0] = 82349.43;
  carac_sections[1621][1] = 0.00;
  carac_sections[1621][2] = 3.73;
  carac_sections[1621][3] = 100.00;
  outputs[1621][0] = 4.66;
  outputs[1621][1] = 3.50;

  // section 1622
  carac_sections[1622][0] = 82389.56;
  carac_sections[1622][1] = 0.00;
  carac_sections[1622][2] = 3.69;
  carac_sections[1622][3] = 100.00;
  outputs[1622][0] = 4.68;
  outputs[1622][1] = 3.49;

  // section 1623
  carac_sections[1623][0] = 82427.22;
  carac_sections[1623][1] = 0.00;
  carac_sections[1623][2] = 3.66;
  carac_sections[1623][3] = 100.00;
  outputs[1623][0] = 4.69;
  outputs[1623][1] = 3.48;

  // section 1624
  carac_sections[1624][0] = 82470.10;
  carac_sections[1624][1] = 0.00;
  carac_sections[1624][2] = 3.62;
  carac_sections[1624][3] = 100.00;
  outputs[1624][0] = 4.71;
  outputs[1624][1] = 3.47;

  // section 1625
  carac_sections[1625][0] = 82498.13;
  carac_sections[1625][1] = 0.00;
  carac_sections[1625][2] = 3.59;
  carac_sections[1625][3] = 100.00;
  outputs[1625][0] = 4.72;
  outputs[1625][1] = 3.46;

  // section 1626
  carac_sections[1626][0] = 82526.52;
  carac_sections[1626][1] = 0.00;
  carac_sections[1626][2] = 3.56;
  carac_sections[1626][3] = 100.00;
  outputs[1626][0] = 4.72;
  outputs[1626][1] = 3.46;

  // section 1627
  carac_sections[1627][0] = 82578.87;
  carac_sections[1627][1] = 0.00;
  carac_sections[1627][2] = 3.51;
  carac_sections[1627][3] = 100.00;
  outputs[1627][0] = 4.75;
  outputs[1627][1] = 3.44;

  // section 1628
  carac_sections[1628][0] = 82610.01;
  carac_sections[1628][1] = 0.00;
  carac_sections[1628][2] = 3.48;
  carac_sections[1628][3] = 100.00;
  outputs[1628][0] = 4.76;
  outputs[1628][1] = 3.43;

  // section 1629
  carac_sections[1629][0] = 82643.09;
  carac_sections[1629][1] = 0.00;
  carac_sections[1629][2] = 3.45;
  carac_sections[1629][3] = 100.00;
  outputs[1629][0] = 4.77;
  outputs[1629][1] = 3.42;

  // section 1630
  carac_sections[1630][0] = 82682.23;
  carac_sections[1630][1] = 0.00;
  carac_sections[1630][2] = 3.41;
  carac_sections[1630][3] = 100.00;
  outputs[1630][0] = 4.79;
  outputs[1630][1] = 3.41;

  // section 1631
  carac_sections[1631][0] = 82723.18;
  carac_sections[1631][1] = 0.00;
  carac_sections[1631][2] = 3.37;
  carac_sections[1631][3] = 100.00;
  outputs[1631][0] = 4.81;
  outputs[1631][1] = 3.39;

  // section 1632
  carac_sections[1632][0] = 82768.56;
  carac_sections[1632][1] = 0.00;
  carac_sections[1632][2] = 3.33;
  carac_sections[1632][3] = 100.00;
  outputs[1632][0] = 4.83;
  outputs[1632][1] = 3.38;

  // section 1633
  carac_sections[1633][0] = 82817.34;
  carac_sections[1633][1] = 0.00;
  carac_sections[1633][2] = 3.28;
  carac_sections[1633][3] = 100.00;
  outputs[1633][0] = 4.86;
  outputs[1633][1] = 3.36;

  // section 1634
  carac_sections[1634][0] = 82871.84;
  carac_sections[1634][1] = 0.00;
  carac_sections[1634][2] = 3.22;
  carac_sections[1634][3] = 100.00;
  outputs[1634][0] = 4.88;
  outputs[1634][1] = 3.34;

  // section 1635
  carac_sections[1635][0] = 82930.12;
  carac_sections[1635][1] = 0.00;
  carac_sections[1635][2] = 3.17;
  carac_sections[1635][3] = 100.00;
  outputs[1635][0] = 4.91;
  outputs[1635][1] = 3.32;

  // section 1636
  carac_sections[1636][0] = 82992.88;
  carac_sections[1636][1] = 0.00;
  carac_sections[1636][2] = 3.10;
  carac_sections[1636][3] = 100.00;
  outputs[1636][0] = 4.95;
  outputs[1636][1] = 3.30;

  // section 1637
  carac_sections[1637][0] = 83059.53;
  carac_sections[1637][1] = 0.00;
  carac_sections[1637][2] = 3.04;
  carac_sections[1637][3] = 100.00;
  outputs[1637][0] = 4.98;
  outputs[1637][1] = 3.28;

  // section 1638
  carac_sections[1638][0] = 83129.72;
  carac_sections[1638][1] = 0.00;
  carac_sections[1638][2] = 2.97;
  carac_sections[1638][3] = 100.00;
  outputs[1638][0] = 5.02;
  outputs[1638][1] = 3.25;

  // section 1639
  carac_sections[1639][0] = 83201.69;
  carac_sections[1639][1] = 0.00;
  carac_sections[1639][2] = 2.91;
  carac_sections[1639][3] = 100.00;
  outputs[1639][0] = 5.05;
  outputs[1639][1] = 3.23;

  // section 1640
  carac_sections[1640][0] = 83273.95;
  carac_sections[1640][1] = 0.00;
  carac_sections[1640][2] = 2.85;
  carac_sections[1640][3] = 100.00;
  outputs[1640][0] = 5.08;
  outputs[1640][1] = 3.22;

  // section 1641
  carac_sections[1641][0] = 83345.13;
  carac_sections[1641][1] = 0.00;
  carac_sections[1641][2] = 2.79;
  carac_sections[1641][3] = 100.00;
  outputs[1641][0] = 5.09;
  outputs[1641][1] = 3.20;

  // section 1642
  carac_sections[1642][0] = 83413.95;
  carac_sections[1642][1] = 0.00;
  carac_sections[1642][2] = 2.75;
  carac_sections[1642][3] = 100.00;
  outputs[1642][0] = 5.11;
  outputs[1642][1] = 3.20;

  // section 1643
  carac_sections[1643][0] = 83479.74;
  carac_sections[1643][1] = 0.00;
  carac_sections[1643][2] = 2.71;
  carac_sections[1643][3] = 100.00;
  outputs[1643][0] = 5.11;
  outputs[1643][1] = 3.20;

  // section 1644
  carac_sections[1644][0] = 83541.52;
  carac_sections[1644][1] = 0.00;
  carac_sections[1644][2] = 2.69;
  carac_sections[1644][3] = 100.00;
  outputs[1644][0] = 5.10;
  outputs[1644][1] = 3.20;

  // section 1645
  carac_sections[1645][0] = 83599.91;
  carac_sections[1645][1] = 0.00;
  carac_sections[1645][2] = 2.66;
  carac_sections[1645][3] = 100.00;
  outputs[1645][0] = 5.09;
  outputs[1645][1] = 3.21;

  // section 1646
  carac_sections[1646][0] = 83654.39;
  carac_sections[1646][1] = 0.00;
  carac_sections[1646][2] = 2.65;
  carac_sections[1646][3] = 100.00;
  outputs[1646][0] = 5.08;
  outputs[1646][1] = 3.22;

  // section 1647
  carac_sections[1647][0] = 83706.52;
  carac_sections[1647][1] = 0.00;
  carac_sections[1647][2] = 2.63;
  carac_sections[1647][3] = 100.00;
  outputs[1647][0] = 5.06;
  outputs[1647][1] = 3.23;

  // section 1648
  carac_sections[1648][0] = 83755.60;
  carac_sections[1648][1] = 0.00;
  carac_sections[1648][2] = 2.62;
  carac_sections[1648][3] = 100.00;
  outputs[1648][0] = 5.04;
  outputs[1648][1] = 3.24;

  // section 1649
  carac_sections[1649][0] = 83803.90;
  carac_sections[1649][1] = 0.00;
  carac_sections[1649][2] = 2.62;
  carac_sections[1649][3] = 100.00;
  outputs[1649][0] = 5.01;
  outputs[1649][1] = 3.26;

  // section 1650
  carac_sections[1650][0] = 83849.73;
  carac_sections[1650][1] = 0.00;
  carac_sections[1650][2] = 2.61;
  carac_sections[1650][3] = 100.00;
  outputs[1650][0] = 4.99;
  outputs[1650][1] = 3.27;

  // section 1651
  carac_sections[1651][0] = 83895.85;
  carac_sections[1651][1] = 0.00;
  carac_sections[1651][2] = 2.60;
  carac_sections[1651][3] = 100.00;
  outputs[1651][0] = 4.96;
  outputs[1651][1] = 3.29;

  // section 1652
  carac_sections[1652][0] = 83942.00;
  carac_sections[1652][1] = 0.00;
  carac_sections[1652][2] = 2.60;
  carac_sections[1652][3] = 100.00;
  outputs[1652][0] = 4.93;
  outputs[1652][1] = 3.31;

  // section 1653
  carac_sections[1653][0] = 83990.71;
  carac_sections[1653][1] = 0.00;
  carac_sections[1653][2] = 2.59;
  carac_sections[1653][3] = 100.00;
  outputs[1653][0] = 4.91;
  outputs[1653][1] = 3.33;

  // section 1654
  carac_sections[1654][0] = 84040.91;
  carac_sections[1654][1] = 0.00;
  carac_sections[1654][2] = 2.58;
  carac_sections[1654][3] = 100.00;
  outputs[1654][0] = 4.88;
  outputs[1654][1] = 3.35;

  // section 1655
  carac_sections[1655][0] = 84101.31;
  carac_sections[1655][1] = 0.00;
  carac_sections[1655][2] = 2.57;
  carac_sections[1655][3] = 100.00;
  outputs[1655][0] = 4.86;
  outputs[1655][1] = 3.36;

  // section 1656
  carac_sections[1656][0] = 84126.41;
  carac_sections[1656][1] = 0.00;
  carac_sections[1656][2] = 2.56;
  carac_sections[1656][3] = 100.00;
  outputs[1656][0] = 4.84;
  outputs[1656][1] = 3.38;

  // section 1657
  carac_sections[1657][0] = 84157.72;
  carac_sections[1657][1] = 0.00;
  carac_sections[1657][2] = 2.55;
  carac_sections[1657][3] = 100.00;
  outputs[1657][0] = 4.82;
  outputs[1657][1] = 3.38;

  // section 1658
  carac_sections[1658][0] = 84191.12;
  carac_sections[1658][1] = 0.00;
  carac_sections[1658][2] = 2.54;
  carac_sections[1658][3] = 100.00;
  outputs[1658][0] = 4.81;
  outputs[1658][1] = 3.39;

  // section 1659
  carac_sections[1659][0] = 84227.70;
  carac_sections[1659][1] = 0.00;
  carac_sections[1659][2] = 2.52;
  carac_sections[1659][3] = 100.00;
  outputs[1659][0] = 4.80;
  outputs[1659][1] = 3.40;

  // section 1660
  carac_sections[1660][0] = 84264.12;
  carac_sections[1660][1] = 0.00;
  carac_sections[1660][2] = 2.51;
  carac_sections[1660][3] = 100.00;
  outputs[1660][0] = 4.80;
  outputs[1660][1] = 3.40;

  // section 1661
  carac_sections[1661][0] = 84303.40;
  carac_sections[1661][1] = 0.00;
  carac_sections[1661][2] = 2.48;
  carac_sections[1661][3] = 100.00;
  outputs[1661][0] = 4.79;
  outputs[1661][1] = 3.41;

  // section 1662
  carac_sections[1662][0] = 84344.92;
  carac_sections[1662][1] = 0.00;
  carac_sections[1662][2] = 2.46;
  carac_sections[1662][3] = 100.00;
  outputs[1662][0] = 4.79;
  outputs[1662][1] = 3.41;

  // section 1663
  carac_sections[1663][0] = 84389.87;
  carac_sections[1663][1] = 0.00;
  carac_sections[1663][2] = 2.42;
  carac_sections[1663][3] = 100.00;
  outputs[1663][0] = 4.80;
  outputs[1663][1] = 3.40;

  // section 1664
  carac_sections[1664][0] = 84434.97;
  carac_sections[1664][1] = 0.00;
  carac_sections[1664][2] = 2.39;
  carac_sections[1664][3] = 100.00;
  outputs[1664][0] = 4.81;
  outputs[1664][1] = 3.40;

  // section 1665
  carac_sections[1665][0] = 84482.35;
  carac_sections[1665][1] = 0.00;
  carac_sections[1665][2] = 2.35;
  carac_sections[1665][3] = 100.00;
  outputs[1665][0] = 4.82;
  outputs[1665][1] = 3.39;

  // section 1666
  carac_sections[1666][0] = 84530.94;
  carac_sections[1666][1] = 0.00;
  carac_sections[1666][2] = 2.31;
  carac_sections[1666][3] = 100.00;
  outputs[1666][0] = 4.84;
  outputs[1666][1] = 3.37;

  // section 1667
  carac_sections[1667][0] = 84581.69;
  carac_sections[1667][1] = 0.00;
  carac_sections[1667][2] = 2.26;
  carac_sections[1667][3] = 100.00;
  outputs[1667][0] = 4.86;
  outputs[1667][1] = 3.36;

  // section 1668
  carac_sections[1668][0] = 84630.21;
  carac_sections[1668][1] = 0.00;
  carac_sections[1668][2] = 2.21;
  carac_sections[1668][3] = 100.00;
  outputs[1668][0] = 4.88;
  outputs[1668][1] = 3.34;

  // section 1669
  carac_sections[1669][0] = 84679.43;
  carac_sections[1669][1] = 0.00;
  carac_sections[1669][2] = 2.16;
  carac_sections[1669][3] = 100.00;
  outputs[1669][0] = 4.91;
  outputs[1669][1] = 3.33;

  // section 1670
  carac_sections[1670][0] = 84727.48;
  carac_sections[1670][1] = 0.00;
  carac_sections[1670][2] = 2.11;
  carac_sections[1670][3] = 100.00;
  outputs[1670][0] = 4.93;
  outputs[1670][1] = 3.31;

  // section 1671
  carac_sections[1671][0] = 84776.12;
  carac_sections[1671][1] = 0.00;
  carac_sections[1671][2] = 2.06;
  carac_sections[1671][3] = 100.00;
  outputs[1671][0] = 4.96;
  outputs[1671][1] = 3.29;

  // section 1672
  carac_sections[1672][0] = 84822.15;
  carac_sections[1672][1] = 0.00;
  carac_sections[1672][2] = 2.02;
  carac_sections[1672][3] = 100.00;
  outputs[1672][0] = 4.99;
  outputs[1672][1] = 3.28;

  // section 1673
  carac_sections[1673][0] = 84868.30;
  carac_sections[1673][1] = 0.00;
  carac_sections[1673][2] = 1.97;
  carac_sections[1673][3] = 100.00;
  outputs[1673][0] = 5.01;
  outputs[1673][1] = 3.26;

  // section 1674
  carac_sections[1674][0] = 84912.52;
  carac_sections[1674][1] = 0.00;
  carac_sections[1674][2] = 1.93;
  carac_sections[1674][3] = 100.00;
  outputs[1674][0] = 5.03;
  outputs[1674][1] = 3.24;

  // section 1675
  carac_sections[1675][0] = 84958.09;
  carac_sections[1675][1] = 0.00;
  carac_sections[1675][2] = 1.89;
  carac_sections[1675][3] = 100.00;
  outputs[1675][0] = 5.05;
  outputs[1675][1] = 3.23;

  // section 1676
  carac_sections[1676][0] = 85025.16;
  carac_sections[1676][1] = 0.00;
  carac_sections[1676][2] = 1.83;
  carac_sections[1676][3] = 100.00;
  outputs[1676][0] = 5.09;
  outputs[1676][1] = 3.21;

  // section 1677
  carac_sections[1677][0] = 85089.19;
  carac_sections[1677][1] = 0.00;
  carac_sections[1677][2] = 1.77;
  carac_sections[1677][3] = 100.00;
  outputs[1677][0] = 5.11;
  outputs[1677][1] = 3.19;

  // section 1678
  carac_sections[1678][0] = 85149.68;
  carac_sections[1678][1] = 0.00;
  carac_sections[1678][2] = 1.73;
  carac_sections[1678][3] = 100.00;
  outputs[1678][0] = 5.13;
  outputs[1678][1] = 3.18;

  // section 1679
  carac_sections[1679][0] = 85208.52;
  carac_sections[1679][1] = 0.00;
  carac_sections[1679][2] = 1.68;
  carac_sections[1679][3] = 100.00;
  outputs[1679][0] = 5.15;
  outputs[1679][1] = 3.17;

  // section 1680
  carac_sections[1680][0] = 85261.58;
  carac_sections[1680][1] = 0.00;
  carac_sections[1680][2] = 1.64;
  carac_sections[1680][3] = 100.00;
  outputs[1680][0] = 5.17;
  outputs[1680][1] = 3.16;

  // section 1681
  carac_sections[1681][0] = 85312.91;
  carac_sections[1681][1] = 0.00;
  carac_sections[1681][2] = 1.61;
  carac_sections[1681][3] = 100.00;
  outputs[1681][0] = 5.18;
  outputs[1681][1] = 3.15;

  // section 1682
  carac_sections[1682][0] = 85361.90;
  carac_sections[1682][1] = 0.00;
  carac_sections[1682][2] = 1.57;
  carac_sections[1682][3] = 100.00;
  outputs[1682][0] = 5.19;
  outputs[1682][1] = 3.15;

  // section 1683
  carac_sections[1683][0] = 85410.84;
  carac_sections[1683][1] = 0.00;
  carac_sections[1683][2] = 1.54;
  carac_sections[1683][3] = 100.00;
  outputs[1683][0] = 5.20;
  outputs[1683][1] = 3.14;

  // section 1684
  carac_sections[1684][0] = 85454.01;
  carac_sections[1684][1] = 0.00;
  carac_sections[1684][2] = 1.52;
  carac_sections[1684][3] = 100.00;
  outputs[1684][0] = 5.21;
  outputs[1684][1] = 3.14;

  // section 1685
  carac_sections[1685][0] = 85496.91;
  carac_sections[1685][1] = 0.00;
  carac_sections[1685][2] = 1.49;
  carac_sections[1685][3] = 100.00;
  outputs[1685][0] = 5.21;
  outputs[1685][1] = 3.13;

  // section 1686
  carac_sections[1686][0] = 85537.45;
  carac_sections[1686][1] = 0.00;
  carac_sections[1686][2] = 1.47;
  carac_sections[1686][3] = 100.00;
  outputs[1686][0] = 5.21;
  outputs[1686][1] = 3.13;

  // section 1687
  carac_sections[1687][0] = 85578.70;
  carac_sections[1687][1] = 0.00;
  carac_sections[1687][2] = 1.45;
  carac_sections[1687][3] = 100.00;
  outputs[1687][0] = 5.21;
  outputs[1687][1] = 3.13;

  // section 1688
  carac_sections[1688][0] = 85651.79;
  carac_sections[1688][1] = 0.00;
  carac_sections[1688][2] = 1.41;
  carac_sections[1688][3] = 100.00;
  outputs[1688][0] = 5.22;
  outputs[1688][1] = 3.13;

  // section 1689
  carac_sections[1689][0] = 85722.38;
  carac_sections[1689][1] = 0.00;
  carac_sections[1689][2] = 1.38;
  carac_sections[1689][3] = 100.00;
  outputs[1689][0] = 5.22;
  outputs[1689][1] = 3.13;

  // section 1690
  carac_sections[1690][0] = 85784.88;
  carac_sections[1690][1] = 0.00;
  carac_sections[1690][2] = 1.35;
  carac_sections[1690][3] = 100.00;
  outputs[1690][0] = 5.21;
  outputs[1690][1] = 3.13;

  // section 1691
  carac_sections[1691][0] = 85844.84;
  carac_sections[1691][1] = 0.00;
  carac_sections[1691][2] = 1.33;
  carac_sections[1691][3] = 100.00;
  outputs[1691][0] = 5.21;
  outputs[1691][1] = 3.14;

  // section 1692
  carac_sections[1692][0] = 85899.72;
  carac_sections[1692][1] = 0.00;
  carac_sections[1692][2] = 1.30;
  carac_sections[1692][3] = 100.00;
  outputs[1692][0] = 5.20;
  outputs[1692][1] = 3.14;

  // section 1693
  carac_sections[1693][0] = 85953.98;
  carac_sections[1693][1] = 0.00;
  carac_sections[1693][2] = 1.28;
  carac_sections[1693][3] = 100.00;
  outputs[1693][0] = 5.19;
  outputs[1693][1] = 3.14;

  // section 1694
  carac_sections[1694][0] = 86001.38;
  carac_sections[1694][1] = 0.00;
  carac_sections[1694][2] = 1.27;
  carac_sections[1694][3] = 100.00;
  outputs[1694][0] = 5.18;
  outputs[1694][1] = 3.15;

  // section 1695
  carac_sections[1695][0] = 86048.97;
  carac_sections[1695][1] = 0.00;
  carac_sections[1695][2] = 1.25;
  carac_sections[1695][3] = 100.00;
  outputs[1695][0] = 5.18;
  outputs[1695][1] = 3.15;

  // section 1696
  carac_sections[1696][0] = 86090.57;
  carac_sections[1696][1] = 0.00;
  carac_sections[1696][2] = 1.23;
  carac_sections[1696][3] = 100.00;
  outputs[1696][0] = 5.17;
  outputs[1696][1] = 3.16;

  // section 1697
  carac_sections[1697][0] = 86132.70;
  carac_sections[1697][1] = 0.00;
  carac_sections[1697][2] = 1.22;
  carac_sections[1697][3] = 100.00;
  outputs[1697][0] = 5.15;
  outputs[1697][1] = 3.17;

  // section 1698
  carac_sections[1698][0] = 86198.37;
  carac_sections[1698][1] = 0.00;
  carac_sections[1698][2] = 1.20;
  carac_sections[1698][3] = 100.00;
  outputs[1698][0] = 5.14;
  outputs[1698][1] = 3.18;

  // section 1699
  carac_sections[1699][0] = 86261.16;
  carac_sections[1699][1] = 0.00;
  carac_sections[1699][2] = 1.18;
  carac_sections[1699][3] = 100.00;
  outputs[1699][0] = 5.13;
  outputs[1699][1] = 3.18;

  // section 1700
  carac_sections[1700][0] = 86305.63;
  carac_sections[1700][1] = 0.00;
  carac_sections[1700][2] = 1.17;
  carac_sections[1700][3] = 100.00;
  outputs[1700][0] = 5.11;
  outputs[1700][1] = 3.19;

  // section 1701
  carac_sections[1701][0] = 86349.90;
  carac_sections[1701][1] = 0.00;
  carac_sections[1701][2] = 1.16;
  carac_sections[1701][3] = 100.00;
  outputs[1701][0] = 5.10;
  outputs[1701][1] = 3.20;

  // section 1702
  carac_sections[1702][0] = 86415.80;
  carac_sections[1702][1] = 0.00;
  carac_sections[1702][2] = 1.14;
  carac_sections[1702][3] = 100.00;
  outputs[1702][0] = 5.08;
  outputs[1702][1] = 3.22;

  // section 1703
  carac_sections[1703][0] = 86489.89;
  carac_sections[1703][1] = 0.00;
  carac_sections[1703][2] = 1.12;
  carac_sections[1703][3] = 100.00;
  outputs[1703][0] = 5.05;
  outputs[1703][1] = 3.23;

  // section 1704
  carac_sections[1704][0] = 86555.45;
  carac_sections[1704][1] = 0.00;
  carac_sections[1704][2] = 1.10;
  carac_sections[1704][3] = 100.00;
  outputs[1704][0] = 5.03;
  outputs[1704][1] = 3.24;

  // section 1705
  carac_sections[1705][0] = 86589.57;
  carac_sections[1705][1] = 0.00;
  carac_sections[1705][2] = 1.09;
  carac_sections[1705][3] = 100.00;
  outputs[1705][0] = 5.01;
  outputs[1705][1] = 3.26;

  // section 1706
  carac_sections[1706][0] = 86646.84;
  carac_sections[1706][1] = 0.00;
  carac_sections[1706][2] = 1.08;
  carac_sections[1706][3] = 100.00;
  outputs[1706][0] = 4.99;
  outputs[1706][1] = 3.27;

  // section 1707
  carac_sections[1707][0] = 86679.42;
  carac_sections[1707][1] = 0.00;
  carac_sections[1707][2] = 1.07;
  carac_sections[1707][3] = 100.00;
  outputs[1707][0] = 4.98;
  outputs[1707][1] = 3.28;

  // section 1708
  carac_sections[1708][0] = 86713.24;
  carac_sections[1708][1] = 0.00;
  carac_sections[1708][2] = 1.07;
  carac_sections[1708][3] = 100.00;
  outputs[1708][0] = 4.96;
  outputs[1708][1] = 3.29;

  // section 1709
  carac_sections[1709][0] = 86753.73;
  carac_sections[1709][1] = 0.00;
  carac_sections[1709][2] = 1.06;
  carac_sections[1709][3] = 100.00;
  outputs[1709][0] = 4.94;
  outputs[1709][1] = 3.30;

  // section 1710
  carac_sections[1710][0] = 86794.23;
  carac_sections[1710][1] = 0.00;
  carac_sections[1710][2] = 1.05;
  carac_sections[1710][3] = 100.00;
  outputs[1710][0] = 4.92;
  outputs[1710][1] = 3.32;

  // section 1711
  carac_sections[1711][0] = 86838.54;
  carac_sections[1711][1] = 0.00;
  carac_sections[1711][2] = 1.04;
  carac_sections[1711][3] = 100.00;
  outputs[1711][0] = 4.90;
  outputs[1711][1] = 3.33;

  // section 1712
  carac_sections[1712][0] = 86885.97;
  carac_sections[1712][1] = 0.00;
  carac_sections[1712][2] = 1.03;
  carac_sections[1712][3] = 100.00;
  outputs[1712][0] = 4.88;
  outputs[1712][1] = 3.35;

  // section 1713
  carac_sections[1713][0] = 86930.64;
  carac_sections[1713][1] = 0.00;
  carac_sections[1713][2] = 1.02;
  carac_sections[1713][3] = 100.00;
  outputs[1713][0] = 4.85;
  outputs[1713][1] = 3.36;

  // section 1714
  carac_sections[1714][0] = 86977.51;
  carac_sections[1714][1] = 0.00;
  carac_sections[1714][2] = 1.01;
  carac_sections[1714][3] = 100.00;
  outputs[1714][0] = 4.83;
  outputs[1714][1] = 3.38;

  // section 1715
  carac_sections[1715][0] = 87027.72;
  carac_sections[1715][1] = 0.00;
  carac_sections[1715][2] = 1.00;
  carac_sections[1715][3] = 100.00;
  outputs[1715][0] = 4.80;
  outputs[1715][1] = 3.40;

  // section 1716
  carac_sections[1716][0] = 87081.69;
  carac_sections[1716][1] = 0.00;
  carac_sections[1716][2] = 0.99;
  carac_sections[1716][3] = 100.00;
  outputs[1716][0] = 4.77;
  outputs[1716][1] = 3.42;

  // section 1717
  carac_sections[1717][0] = 87144.04;
  carac_sections[1717][1] = 0.00;
  carac_sections[1717][2] = 0.97;
  carac_sections[1717][3] = 100.00;
  outputs[1717][0] = 4.74;
  outputs[1717][1] = 3.45;

  // section 1718
  carac_sections[1718][0] = 87173.44;
  carac_sections[1718][1] = 0.00;
  carac_sections[1718][2] = 0.97;
  carac_sections[1718][3] = 100.00;
  outputs[1718][0] = 4.71;
  outputs[1718][1] = 3.46;

  // section 1719
  carac_sections[1719][0] = 87206.06;
  carac_sections[1719][1] = 0.00;
  carac_sections[1719][2] = 0.96;
  carac_sections[1719][3] = 100.00;
  outputs[1719][0] = 4.69;
  outputs[1719][1] = 3.48;

  // section 1720
  carac_sections[1720][0] = 87242.55;
  carac_sections[1720][1] = 0.00;
  carac_sections[1720][2] = 0.95;
  carac_sections[1720][3] = 100.00;
  outputs[1720][0] = 4.67;
  outputs[1720][1] = 3.49;

  // section 1721
  carac_sections[1721][0] = 87280.27;
  carac_sections[1721][1] = 0.00;
  carac_sections[1721][2] = 0.94;
  carac_sections[1721][3] = 100.00;
  outputs[1721][0] = 4.65;
  outputs[1721][1] = 3.51;

  // section 1722
  carac_sections[1722][0] = 87320.90;
  carac_sections[1722][1] = 0.00;
  carac_sections[1722][2] = 0.93;
  carac_sections[1722][3] = 100.00;
  outputs[1722][0] = 4.62;
  outputs[1722][1] = 3.53;

  // section 1723
  carac_sections[1723][0] = 87365.70;
  carac_sections[1723][1] = 0.00;
  carac_sections[1723][2] = 0.92;
  carac_sections[1723][3] = 100.00;
  outputs[1723][0] = 4.59;
  outputs[1723][1] = 3.55;

  // section 1724
  carac_sections[1724][0] = 87412.05;
  carac_sections[1724][1] = 0.00;
  carac_sections[1724][2] = 0.91;
  carac_sections[1724][3] = 100.00;
  outputs[1724][0] = 4.57;
  outputs[1724][1] = 3.58;

  // section 1725
  carac_sections[1725][0] = 87463.45;
  carac_sections[1725][1] = 0.00;
  carac_sections[1725][2] = 0.89;
  carac_sections[1725][3] = 100.00;
  outputs[1725][0] = 4.53;
  outputs[1725][1] = 3.60;

  // section 1726
  carac_sections[1726][0] = 87519.12;
  carac_sections[1726][1] = 0.00;
  carac_sections[1726][2] = 0.87;
  carac_sections[1726][3] = 100.00;
  outputs[1726][0] = 4.50;
  outputs[1726][1] = 3.63;

  // section 1727
  carac_sections[1727][0] = 87582.54;
  carac_sections[1727][1] = 0.00;
  carac_sections[1727][2] = 0.85;
  carac_sections[1727][3] = 100.00;
  outputs[1727][0] = 4.46;
  outputs[1727][1] = 3.66;

  // section 1728
  carac_sections[1728][0] = 87650.88;
  carac_sections[1728][1] = 0.00;
  carac_sections[1728][2] = 0.82;
  carac_sections[1728][3] = 100.00;
  outputs[1728][0] = 4.43;
  outputs[1728][1] = 3.68;

  // section 1729
  carac_sections[1729][0] = 87689.41;
  carac_sections[1729][1] = 0.00;
  carac_sections[1729][2] = 0.80;
  carac_sections[1729][3] = 100.00;
  outputs[1729][0] = 4.40;
  outputs[1729][1] = 3.71;

  // section 1730
  carac_sections[1730][0] = 87728.35;
  carac_sections[1730][1] = 0.00;
  carac_sections[1730][2] = 0.78;
  carac_sections[1730][3] = 100.00;
  outputs[1730][0] = 4.38;
  outputs[1730][1] = 3.73;

  // section 1731
  carac_sections[1731][0] = 87771.86;
  carac_sections[1731][1] = 0.00;
  carac_sections[1731][2] = 0.76;
  carac_sections[1731][3] = 100.00;
  outputs[1731][0] = 4.36;
  outputs[1731][1] = 3.74;

  // section 1732
  carac_sections[1732][0] = 87816.40;
  carac_sections[1732][1] = 0.00;
  carac_sections[1732][2] = 0.73;
  carac_sections[1732][3] = 100.00;
  outputs[1732][0] = 4.34;
  outputs[1732][1] = 3.76;

  // section 1733
  carac_sections[1733][0] = 87867.77;
  carac_sections[1733][1] = 0.00;
  carac_sections[1733][2] = 0.70;
  carac_sections[1733][3] = 100.00;
  outputs[1733][0] = 4.32;
  outputs[1733][1] = 3.78;

  // section 1734
  carac_sections[1734][0] = 87921.44;
  carac_sections[1734][1] = 0.00;
  carac_sections[1734][2] = 0.66;
  carac_sections[1734][3] = 100.00;
  outputs[1734][0] = 4.30;
  outputs[1734][1] = 3.80;

  // section 1735
  carac_sections[1735][0] = 87975.94;
  carac_sections[1735][1] = 0.00;
  carac_sections[1735][2] = 0.63;
  carac_sections[1735][3] = 100.00;
  outputs[1735][0] = 4.28;
  outputs[1735][1] = 3.81;

  // section 1736
  carac_sections[1736][0] = 88033.80;
  carac_sections[1736][1] = 0.00;
  carac_sections[1736][2] = 0.58;
  carac_sections[1736][3] = 100.00;
  outputs[1736][0] = 4.27;
  outputs[1736][1] = 3.82;

  // section 1737
  carac_sections[1737][0] = 88097.85;
  carac_sections[1737][1] = 0.00;
  carac_sections[1737][2] = 0.53;
  carac_sections[1737][3] = 100.00;
  outputs[1737][0] = 4.26;
  outputs[1737][1] = 3.83;

  // section 1738
  carac_sections[1738][0] = 88166.34;
  carac_sections[1738][1] = 0.00;
  carac_sections[1738][2] = 0.47;
  carac_sections[1738][3] = 100.00;
  outputs[1738][0] = 4.25;
  outputs[1738][1] = 3.84;

  // section 1739
  carac_sections[1739][0] = 88238.46;
  carac_sections[1739][1] = 0.00;
  carac_sections[1739][2] = 0.40;
  carac_sections[1739][3] = 100.00;
  outputs[1739][0] = 4.26;
  outputs[1739][1] = 3.83;

  // section 1740
  carac_sections[1740][0] = 88277.31;
  carac_sections[1740][1] = 0.00;
  carac_sections[1740][2] = 0.37;
  carac_sections[1740][3] = 100.00;
  outputs[1740][0] = 4.25;
  outputs[1740][1] = 3.84;

  // section 1741
  carac_sections[1741][0] = 88315.95;
  carac_sections[1741][1] = 0.00;
  carac_sections[1741][2] = 0.33;
  carac_sections[1741][3] = 100.00;
  outputs[1741][0] = 4.25;
  outputs[1741][1] = 3.84;

  // section 1742
  carac_sections[1742][0] = 88360.74;
  carac_sections[1742][1] = 0.00;
  carac_sections[1742][2] = 0.28;
  carac_sections[1742][3] = 100.00;
  outputs[1742][0] = 4.26;
  outputs[1742][1] = 3.84;

  // section 1743
  carac_sections[1743][0] = 88405.26;
  carac_sections[1743][1] = 0.00;
  carac_sections[1743][2] = 0.24;
  carac_sections[1743][3] = 100.00;
  outputs[1743][0] = 4.26;
  outputs[1743][1] = 3.83;

  // section 1744
  carac_sections[1744][0] = 88456.26;
  carac_sections[1744][1] = 0.00;
  carac_sections[1744][2] = 0.19;
  carac_sections[1744][3] = 100.00;
  outputs[1744][0] = 4.27;
  outputs[1744][1] = 3.83;

  // section 1745
  carac_sections[1745][0] = 88509.17;
  carac_sections[1745][1] = 0.00;
  carac_sections[1745][2] = 0.14;
  carac_sections[1745][3] = 100.00;
  outputs[1745][0] = 4.27;
  outputs[1745][1] = 3.83;

  // section 1746
  carac_sections[1746][0] = 88567.88;
  carac_sections[1746][1] = 0.00;
  carac_sections[1746][2] = 0.08;
  carac_sections[1746][3] = 100.00;
  outputs[1746][0] = 4.27;
  outputs[1746][1] = 3.83;

  // section 1747
  carac_sections[1747][0] = 88631.13;
  carac_sections[1747][1] = 0.00;
  carac_sections[1747][2] = 0.03;
  carac_sections[1747][3] = 100.00;
  outputs[1747][0] = 4.25;
  outputs[1747][1] = 3.84;

  // section 1748
  carac_sections[1748][0] = 88698.81;
  carac_sections[1748][1] = 0.00;
  carac_sections[1748][2] = 0.00;
  carac_sections[1748][3] = 100.00;
  outputs[1748][0] = 4.22;
  outputs[1748][1] = 3.87;

  // section 1749
  carac_sections[1749][0] = 88733.04;
  carac_sections[1749][1] = 0.00;
  carac_sections[1749][2] = -0.03;
  carac_sections[1749][3] = 100.00;
  outputs[1749][0] = 4.17;
  outputs[1749][1] = 3.92;

  start_HyCu_Flumy(nsections, carac_sections, Q_up, Ks, outputs);

  printf("section \th \tV \tQ\n");
  for (f = 0; f < 10; f++) {
    printf("%d\t%f\t%f\t%f\n", f, outputs[f][0], outputs[f][1], outputs[f][0] * outputs[f][1] * carac_sections[f][3]);
  }
  for (f = 10; f > 0; f--) {
    printf("%d\t%f\t%f\t%f\n", nsections - f, outputs[nsections - f][0], outputs[nsections - f][1], outputs[nsections - f][0] * outputs[nsections - f][1] * carac_sections[nsections - f][3]);
  }

  return 0;
}
