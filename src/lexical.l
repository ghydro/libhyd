/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libhyd
* FILE NAME: lexical.l
* 
* CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
* 
* LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with 
* Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using 
* a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as 
* long as geometries are defined, 1D approach handles islands, tributaries, 
* and hydraulics works such as dams. 
*
* Library developed at the Geosciences Center, joint research center 
* of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
*
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/


/********************************************************/
/*             Lexical words used in HyCu               */
/********************************************************/


%x incl str incl_str variable incl_variable unit

%{

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include <libprint.h>
#include <time_series.h>
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#include "reservoir.h"
#include "FP.h"
#endif
#include "HYD_full.h"
#include "global_HYD.h"
#include "ext_HYD.h"
#include "input.h"




/* Local functions declaration */
void include_function(char *);
/*void yyerror(char *);*/
int yylex();

char *pname;



%}

%a 10000
%p 10000
%o 15000
%e 5000
%n 2000

number	[0-9]
to_power [DdEe][-+]?{number}+
alnum [0-9A-Za-z]

%%
#[^\n]*\n 	{ line_nb++;}	/* To not read the comments */
\n	{ line_nb++;}
[ \t]+ ;			/* To ignore spaces and tabulations */

\$ {BEGIN(variable);} 
\[ {BEGIN(unit); return LEX_OPENING_BRACKET;}
[Ii][Nn][Cc][Ll][Uu][DdRr][Ee] {BEGIN(incl);}
[Nn][oO] {yylval.integer = NO; return LEX_ANSWER;}
[Yy][Ee][Ss] {yylval.integer = YES; return LEX_ANSWER;}
[Gg][Ii][Vv][Ee][Nn] {yylval.integer = GIVEN; return LEX_ANSWER;}
"=" return LEX_EQUAL; 
\{ return LEX_OPENING_BRACE;
\} return LEX_CLOSING_BRACE;
":" return LEX_COLON;
";" return LEX_SEMI_COLON;
"->" return LEX_ARROW;
">" return LEX_ARROW; 
"<-" return LEX_REVERSE_ARROW; 
"<" return LEX_REVERSE_ARROW; 

[Ii][Nn][Pp][Uu][Tt]"_"[Ff][Oo][Ll][Dd][Ee][Rr][Ss] return LEX_INPUT_FOLDER;
[Oo][Uu][Tt][Pp][Uu][Tt]"_"[Ff][Oo][Ll][Dd][Ee][Rr] return LEX_OUTPUT_FOLDER;

[Ss][Ii][Mm][Uu][Ll][Aa][Tt][Ii][Oo][Nn] return LEX_SIMUL;

[Ii][Nn][Ii][Tt][Ii][Aa][Ll][Ii][Zz][Aa][Tt][Ii][Oo][Nn] return LEX_INIT;
[Ii][Nn][Ii][Tt]"_"[Zz]"_"[Ff][Ii][Ll][Ee] return LEX_INIT_Z;
[Ii][Nn][Ii][Tt]"_"[Qq]"_"[Ff][Ii][Ll][Ee] return LEX_INIT_Q;

[Tt][Ii][Mm][Ee] return LEX_CHRONOS;
[Dd][Tt] return LEX_TIMESTEP;
[Tt]"_"[Ii][Nn][Ii] {yylval.integer = BEGINNING; return LEX_TIME;}
[Tt]"_"[Ee][Nn][Dd] {yylval.integer = END; return LEX_TIME;}
[Bb][Ee][Gg][Ii][Nn][Nn][Ii][Nn][Gg] return LEX_BEGIN;
[Jj][Aa][Nn][Uu][Aa][Rr][Yy] {yylval.integer = JANUARY; return LEX_MONTH;}
[Ff][Ee][Bb][Rr][Uu][Aa][Rr][Yy] {yylval.integer = FEBRUARY; return LEX_MONTH;}
[Mm][Aa][Rr][Cc][Hh] {yylval.integer = MARCH; return LEX_MONTH;}
[Aa][Pp][Rr][Ii][Ll] {yylval.integer = APRIL; return LEX_MONTH;}
[Mm][Aa][Yy] {yylval.integer = MAY; return LEX_MONTH;}
[Jj][Uu][Nn][Ee] {yylval.integer = JUNE; return LEX_MONTH;}
[Jj][Uu][Ll][Yy] {yylval.integer = JULY; return LEX_MONTH;}
[Aa][Uu][Gg][Uu][Ss][Tt] {yylval.integer = AUGUST; return LEX_MONTH;}
[Ss][Ee][Pp][Tt][Ee][Mm][Bb][Ee][Rr] {yylval.integer = SEPTEMBER; return LEX_MONTH;}
[Oo][Cc][Tt][Oo][Bb][Ee][Rr] {yylval.integer = OCTOBER; return LEX_MONTH;}
[Nn][Oo][Vv][Ee][Mm][Bb][Ee][Rr] {yylval.integer = NOVEMBER; return LEX_MONTH;}
[Dd][Ee][Cc][Ee][Mm][Bb][Ee][Rr] {yylval.integer = DECEMBER; return LEX_MONTH;}

[Ss][Ee][Tt][Tt][Ii][Nn][Gg][Ss] return LEX_SET;
[Nn][Dd][Ii][Mm] return LEX_DIM;
[Tt][Yy][Pp][Ee] return LEX_TYPE;
[Tt][Rr][Aa][Nn][Ss][Ii][Ee][Nn][Tt] {yylval.integer = TRANSIENT; return LEX_CALC_STATE;}
[Ss][Tt][Ee][Aa][Dd][Yy] {yylval.integer = STEADY; return LEX_CALC_STATE;}
[Cc][Aa][Ll][Cc][Uu][Ll][Aa][Tt][Ee]"_"[Cc][Uu][Rr][Vv][Aa][Tt][Uu][Rr][Ee] return LEX_CALC_CURVATURE;
[Uu][Pp][Ss][Tt][Rr][Ee][Aa][Mm]"_"[Hh][Mm][Ii][Nn] {yylval.integer = UP_HMIN;return LEX_GENERALP;}
[Dd][Oo][Ww][Nn][Ss][Tt][Rr][Ee][Aa][Mm]"_"[Hh][Mm][Aa][Xx] {yylval.integer = DOWN_HMAX;return LEX_GENERALP;}
[Dd][Xx] {yylval.integer = DX;return LEX_GENERALP;}
[Dd][Yy] {yylval.integer = DY;return LEX_GENERALP;}
[Dd][Zz] {yylval.integer = DZ;return LEX_GENERALP;}
[Cc][Uu][Rr][Vv][Aa][Tt][Uu][Rr][Ee] {yylval.integer = CURVATURE;return LEX_GENERALP;}
[Gg][Ll][Oo][Bb][Aa][Ll]"_"[Ss][Tt][Rr][Ii][Cc][Kk][Ll][Ee][Rr] {yylval.integer = STRICKLER;return LEX_GENERALP;}
[Tt][Hh][Ee][Tt][Aa] {yylval.integer = THETA;return LEX_GENERALP;}
[Ee][Pp][Ss]"_"[Qq] {yylval.integer = EPS_Q;return LEX_GENERALP;}
[Ee][Pp][Ss]"_"[Zz] {yylval.integer = EPS_Z;return LEX_GENERALP;}


[Nn][Ee][Tt][Ww][Oo][Rr][Kk]"_"[Dd][Ee][Ss][Cc][Rr][Ii][Pp][Tt][Ii][Oo][Nn] return LEX_NETWORK;

[Ss][Ii][Nn][Gg][Uu][Ll][Aa][Rr][Ii][Tt][Ii][Ee][Ss] return LEX_SING;
[Oo][Pp][Ee][Nn] {yylval.integer = OPEN; return LEX_BC_TYPE;}
[Cc][Oo][Nn][Tt][Ii][Nn][Uu] {yylval.integer = CONTINU; return LEX_BC_TYPE;}
[Dd][Ii][Ss][Cc][Hh][Aa][Rr][Gg][Ee] {yylval.integer = DISCHARGE; return LEX_BC_TYPE;}
[Ww][Aa][Tt][Ee][Rr]"_"[Ll][Ee][Vv][Ee][Ll] {yylval.integer = WATER_LEVEL; return LEX_BC_TYPE;}
[Hh][Yy][Dd][Ww][Oo][Rr][Kk] {yylval.integer = HYDWORK; return LEX_BC_TYPE;}
[Rr][Aa][Tt][Ii][Nn][Gg]"_"[Cc][Uu][Rr][Vv][Ee] {yylval.integer = RATING_CURVE; return LEX_BC_TYPE;}
[Pp][Kk] return LEX_PK;
[Pp][Oo][Ss][Ii][Tt][Ii][Oo][Nn] return LEX_POSITION;
[Uu][Pp][Ss][Tt][Rr][Ee][Aa][Mm] {yylval.integer = UPSTREAM; return LEX_DIR;}
[Dd][Oo][Ww][Nn][Ss][Tt][Rr][Ee][Aa][Mm] {yylval.integer = DOWNSTREAM; return LEX_DIR;}
[Hh][Yy][Dd]"_"[Ss][Tt][Rr][Uu][Cc][Tt][Uu][Rr][Ee][Ss] return LEX_HYDWORKS;
[Ww][Ee][Ii][Rr] {yylval.integer = WEIR; return LEX_WORK;}
[Gg][Aa][Tt][Ee] {yylval.integer = GATE; return LEX_WORK;}
[Zz]"("[Tt]")" {yylval.integer = ZT;return LEX_FION;}
[Zz][Ww]"("[Tt]")" {yylval.integer = ZWT;return LEX_FION;}
[Hh]"("[Tt]")" {yylval.integer = HT;return LEX_FION;}
[Mm][Uu] {yylval.integer = MU;return LEX_WORK_PARAM;}
[Ww][Ii][Dd][Tt][Hh] {yylval.integer = WIDTH;return LEX_WORK_PARAM;}

[Rr][Ee][Aa][Cc][Hh][Ee][Ss] return LEX_REACH;
[Rr][Ee][Aa][Cc][Hh][Ee][Ss]"_"[Mm][Uu][Ss][Kk] return LEX_REACH_MUSK;
[Ss][Tt][Rr][Ii][Cc][Kk][Ll][Ee][Rr] return LEX_STRICKLER;

[Pp][Rr][Oo][Ff][Ii][Ll][Ee][Ss] return LEX_PROFILES;
[Xx] return LEX_X;
[Yy] return LEX_Y;
[Aa][Bb][Ss][Cc] return LEX_ABSC;
[Tt][Rr][Aa][Jj][Ee][Cc][Tt][Oo][Rr][Yy] return LEX_TRAJECTORY;
[Ss][Hh][Aa][Pp][Ee] return LEX_SHAPE;
[Rr][Ee][Cc][Tt][Aa][Nn][Gg][Uu][Ll][Aa][Rr] {yylval.integer = RECTANGULAR; return LEX_CROSS_SECTION_TYPE;}
[Cc][Oo][Mm][Pp][Ll][Ee][Xx] {yylval.integer = COMPLEX; return LEX_CROSS_SECTION_TYPE;}

[Zz] return LEX_HYD_VAR;
[Qq] return LEX_HYD_VAR;

[Ii][Nn][Ff][Ll][Oo][Ww][Ss] return LEX_INFLOWS;
[Uu][Pp][Ss][Tt][Rr][Ee][Aa][Mm]_[Ii][Nn][Ff][Ll][Oo][Ww] {yylval.integer = UPSTREAM_INFLOW; return LEX_INFLOW_TYPE;}
[Ii][Nn][Ff][Ll][Uu][Ee][Nn][Tt] {yylval.integer = INFLUENT; return LEX_INFLOW_TYPE;}
[Ee][Ff][Ff][Ll][Uu][Ee][Nn][Tt] {yylval.integer = EFFLUENT; return LEX_INFLOW_TYPE;}
[Dd][Ii][Ff][Ff][Uu][Ss][Ee]_[Ii][Nn][Ff][Ll][Oo][Ww] {yylval.integer = DIFFUSE_INFLOW; return LEX_INFLOW_TYPE;}

[Oo][Uu][Tt][Pp][Uu][Tt][Ss] return LEX_OUTPUTS;
[Tt][Ii][Mm][Ee]"_"[Uu][Nn][Ii][Tt] return LEX_TUNIT;
[Ii][Tt][Ee][rr][Aa][Tt][Ii][Oo][Nn][Ss] {yylval.integer = ITERATIONS; return LEX_OUTPUT_TYPE;}
[Pp][Rr][Ii][Nn][Tt]"_"[Pp][Kk] {yylval.integer = PRINT_PK; return LEX_OUTPUT_TYPE;}
[Ff][Ii][Nn][Aa][Ll]"_"[Ss][Tt][Aa][Tt][Ee] {yylval.integer = FINAL_STATE; return LEX_OUTPUT_TYPE;}
[Mm][Bb]"_"[Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss] {yylval.integer = MB_ELE; return LEX_OUTPUT_TYPE;}
[Ff][Ii][Ll][Ee]"_"[Nn][Aa][Mm][Ee] return LEX_FILE_NAME;
[Tt][Ii][Mm][Ee]"_"[Ss][Ee][Rr][Ii][Ee][Ss] {yylval.integer = TRANSV_PROFILE; return LEX_OUTPUT_TYPE;}
[Ll][Oo][Nn][Gg][Ii][Tt][Uu][Dd][Ii][Nn][Aa][Ll]"_"[Pp][Rr][Oo][Ff][Ii][Ll][Ee][Ss] {yylval.integer = LONG_PROFILE; return LEX_OUTPUT_TYPE;}
[Mm][Aa][Ss][Ss]"_"[Bb][Aa][Ll][Aa][Nn][Cc][Ee][Ss] {yylval.integer = MASS_BALANCE; return LEX_OUTPUT_TYPE;}
[Vv][Aa][Rr] return LEX_VAR;
[Pp][Oo][Ii][Nn][Tt][Ss] return LEX_POINTS;
[Ee][Xx][Tt][Ee][Nn][Tt] return LEX_EXTENT;
[Gg][Rr][Aa][Pp][Hh][Ii][Cc][Ss] return LEX_GRAPHICS;
[Gg][Nn][Uu][Pp][Ll][Oo][Tt] {yylval.integer = GNUPLOT; return LEX_GRAPH_TYPE;}

[Zz][Ff][Ss] {yylval.integer = ZFS_IO; return LEX_ONE_VAR;}
[Hh] {yylval.integer = H_IO; return LEX_ONE_VAR;}
[Ss][Uu][Rr][Ff] {yylval.integer = SURF_IO; return LEX_ONE_VAR;}
[Pp][Ee][Rr][Ii] {yylval.integer = PERI_IO; return LEX_ONE_VAR;}
[Rr][Hh] {yylval.integer = RH_IO; return LEX_ONE_VAR;}
[Vv][Ee][Ll] {yylval.integer = VEL_IO; return LEX_ONE_VAR;}
[Ww][Ff][Ss] {yylval.integer = WIDTH; return LEX_ONE_VAR;}
[Dd][Ii][Ss][Cc][Hh] {yylval.integer = Q_IO; return LEX_ONE_VAR;}

[Kk]"_"[Mm][Uu][Ss][Kk] {yylval.integer = K; return LEX_MUSK_PAR;}
[Xx]"_"[Mm][Uu][Ss][Kk] {yylval.integer = ALPHA; return LEX_MUSK_PAR;}
[Aa][Rr][Ee][Aa] {yylval.integer = AREA; return LEX_CALC_MUSK_PAR;}
[Ss][Ll][Oo][Pp][Ee] {yylval.integer = SLOPE; return LEX_CALC_MUSK_PAR;}
[Mm][Uu][Ss][Kk][Ii][Nn][Gg][Uu][Mm] {yylval.integer = MUSKINGUM; return LEX_DEF_SCHEM_TYPE;}
[Ss][Tt]"_"[Vv][Ee][Nn][Aa][Nn][Tt] {yylval.integer = ST_VENANT; return LEX_DEF_SCHEM_TYPE;}
[Ss][Cc][Hh][Ee][Mm]"_"[Tt][Yy][Pp][Ee] return LEX_SCHEM_TYPE;
[Nn][Ee][Tt][Ww][Oo][Rr][Kk]"_"[Mm][Uu][Ss][Kk] return LEX_NETWORK_MUSK;
[Ee][Ll][Ee][Mm][Ee][Nn][Tt][Ss]"_"[Mm][Uu][Ss][Kk] return LEX_ELE_MUSK;
{number}+ 	|
"-"{number}+ {yylval.integer  = atoi(yytext); return LEX_INT;}
{number}+"."{number}*({to_power})?	| 
"-"{number}+"."{number}*({to_power})?	| 
{number}*"."{number}+({to_power})?	 |
"-"{number}*"."{number}+({to_power})?	{ yylval.real = atof(yytext); return LEX_DOUBLE;}
<incl>[ \t]*    /* skip spaces and tabulations */
<incl>[ \n]*    /* skip newlines */
<incl>\$ {BEGIN(incl_variable);} 
<incl>[^$ \t\n:;,\"]+ {
               char *name;
               name = strdup(yytext);
               include_function(name);
               BEGIN(INITIAL);
 }
\" {/* Allows to read string chains with spaces */ BEGIN(str);} 
<incl>\" {BEGIN(incl_str); /* Beginning of a string chain */ } 
<incl_str>\n { /* A string chain ends with \n */
         LP_error(Simul->poutputs,"HyCu %4.2f -> String chain not ended\n",NVERSION_HYCU);}
<incl_str>[^\\\n\"]+\" {
             int i,k;
             i = strlen(yytext);
             pname = (char *)malloc((i - 1) * sizeof(char));
             for (k = 0; k < i - 1; k++)
                   pname[k] = yytext[k];
             include_function(pname);
             BEGIN(INITIAL);
         }
<str>\" {BEGIN(INITIAL);} 
<str>\n { /* A string chain ends with \n */
         LP_error(Simul->poutputs,"HyCu %4.2f -> String chain not ended\n",NVERSION_HYCU);}
<str>[^\\\n\"]+ {
             yylval.string = strdup(yytext);
             return LEX_NAME;
}

<unit>"/" {return LEX_INV;}
<unit>"^" {return LEX_POW;}

<unit>"�C" {yylval.real = 1.0; return LEX_A_UNIT;}

<unit>"min" {yylval.real = 60.0; return LEX_A_UNIT;}
<unit>"s" {yylval.real = 1.0; return LEX_A_UNIT;}
<unit>"h" {yylval.real = 3600.0; return LEX_A_UNIT;}
<unit>"d" {yylval.real = 86400.0; return LEX_A_UNIT;}
<unit>"yr" {yylval.real = 31536000; return LEX_A_UNIT;} /* years on the bases of 365 days per year */


<unit>"m" {yylval.real = 1.0; return LEX_A_UNIT;}
<unit>"km" {yylval.real = 1000.0; return LEX_A_UNIT;}
<unit>"cm" {yylval.real = 0.01; return LEX_A_UNIT;}
<unit>"ha" {yylval.real = 10000; return LEX_A_UNIT;}
<unit>"mm" {yylval.real = 0.001; return LEX_A_UNIT;}

<unit>"l" {yylval.real = 0.001; return LEX_A_UNIT;}

<unit>"ug" {yylval.real = 0.000001; return LEX_A_UNIT;}
<unit>"mg" {yylval.real = 0.001; return LEX_A_UNIT;}
<unit>"g" {yylval.real = 1.; return LEX_A_UNIT;}
<unit>"kg" {yylval.real = 1000.0; return LEX_A_UNIT;}
<unit>"t" {yylval.real = 1000000.0; return LEX_A_UNIT;}

<unit>"%" {yylval.real = 1.0; return LEX_A_UNIT;}

<unit>{number}+ 	|
<unit>"-"{number}+ {yylval.integer  = atoi(yytext); return LEX_INT;}
<unit>{number}+"."{number}*({to_power})?	| 
<unit>"-"{number}+"."{number}*({to_power})?	| 
<unit>{number}*"."{number}+({to_power})?	 |
<unit>"-"{number}*"."{number}+({to_power})?	{ yylval.real = atof(yytext); return LEX_DOUBLE;}
<unit>\] {
  BEGIN(INITIAL);
  return LEX_CLOSING_BRACKET;
}
<variable>[[:alnum:]]+ {
  /* Inserts the contents of the given variable */
  char *valeur = getenv(yytext);
  if (valeur == NULL) {
    LP_error(Simul->poutputs,"HyCu %4.2f -> File \"%s\", variable \"%s\" undefined, line %d\n",NVERSION_HYCU,current_read_files[pile].name,yytext,line_nb);
  }
  {
    int i;
    for (i = strlen(valeur) - 1; i >= 0; i--)
      unput(valeur[i]);
  }
  BEGIN(INITIAL);
}
<incl_variable>[[:alnum:]]+ {
  /* Inserts the contents of the given variable */
  char *valeur = getenv(yytext);
  if (valeur == NULL) {
    LP_error(Simul->poutputs,"HYCU %4.2f --> File \"%s\", variable \"%s\" undefined, line %d\n",NVERSION_HYCU,current_read_files[pile].name,yytext,line_nb);
  }
  {
    int i;
    for (i = strlen(valeur) - 1; i >= 0; i--)
      unput(valeur[i]);
  }
  BEGIN(incl);
}
[^$ \t\n:;,\"\[\]=]+ {
yylval.string = strdup(yytext);
return LEX_NAME;
}

%%

/* Procedure used to find a file which name is given
* The folders defined by the user in the command file are searched in
* yyin points towards the file read by the parser
*/

void find_file(char *name)
{
   int i;
   char *new_name;

   yyin = fopen(name,"r");
   if (yyin == NULL) {
     for (i = 0; i <folder_nb; i++) {
       new_name = (char *)calloc(strlen(name_out_folder[i]) + strlen(name) + 2,sizeof(char));
       sprintf(new_name,"%s/%s",name_out_folder[i],name);
       /*printf("%s\n",new_name);*/
       yyin = fopen(new_name,"r");
       free(new_name);
       if (yyin != NULL) 
         break;
     }
   }
   if (yyin == NULL)
      LP_error(Simul->poutputs,"HyCu %4.2f -> No file %s\n",NVERSION_HYCU,name);
   
}

/* Procedure used to include a file
*
* - opening of the file which name is in argument
* - storage of the variables related to the file which was read (line_nb, file name, buffer's content)
* - incrementation of the variables of the file pile
*/

void include_file(char *name)
{
int i;
   char *new_name;

   current_read_files[pile].line_nb = line_nb;
   current_read_files[pile].buffer = YY_CURRENT_BUFFER;
   line_nb = 0;
   pile++;
   if (pile >= NPILE)
       LP_error(Simul->poutputs,"HyCu %4.2f -> File %s : Too many files included the ones in the others, maximum = %d\n",NVERSION_HYCU,name,NPILE);
   yyin = fopen(name,"r");
   if (yyin != NULL)
       current_read_files[pile].name = strdup(name);
   else {
     for (i = 0; i <folder_nb; i++) {
       new_name = (char *)calloc(strlen(name_out_folder[i])+strlen(name)+2,sizeof(char));
       sprintf(new_name,"%s/%s",name_out_folder[i],name);
       yyin = fopen(new_name,"r");
       current_read_files[pile].name = strdup(new_name);
       free(new_name);
       if (yyin != NULL) 
         break;
     }
   }
   if (yyin == NULL)
      LP_error(Simul->poutputs,"HyCu %4.2f -> Not possible to find the file %s\n",NVERSION_HYCU,name);
   
   current_read_files[pile].address = yyin;
   yy_switch_to_buffer(yy_create_buffer(yyin,YY_BUF_SIZE));
   LP_printf(Simul->poutputs,"File %s opened\n",current_read_files[pile].name);
   free(name);

}

void include_function(char *name)
{
int i;
   char *new_name;

   current_read_files[pile].line_nb = line_nb;
   current_read_files[pile].buffer = YY_CURRENT_BUFFER;
   line_nb = 0;
   pile++;
   if (pile >= NPILE)
       LP_error(Simul->poutputs,"HyCu %4.2f -> File %s : Too many files included the ones in the others, maximum = %d\n",NVERSION_HYCU,name,NPILE);
   yyin = fopen(name,"r");
   if (yyin != NULL)
       current_read_files[pile].name = strdup(name);
   else {
     for (i = 0; i <folder_nb; i++) {
       new_name = (char *)calloc(strlen(name_out_folder[i])+strlen(name)+2,sizeof(char));
       sprintf(new_name,"%s/%s",name_out_folder[i],name);
       yyin = fopen(new_name,"r");
       current_read_files[pile].name = strdup(new_name);

       if (yyin != NULL) 
         break;
     }
   }
   if (yyin == NULL)
      LP_error(Simul->poutputs,"HyCu %4.2f -> Not possible to find the file %s\n",NVERSION_HYCU,name);
   
   current_read_files[pile].address = yyin;
   yy_switch_to_buffer(yy_create_buffer(yyin,YY_BUF_SIZE));
   LP_printf(Simul->poutputs,"File %s opened\n",current_read_files[pile].name);
   free(name);

}

/* Procedure used to close the files read by the parser
* Called when the parser identifies the end of a file
*
* - closure of the file
* - decrementation of the file pile (pile--)
* - reset of the variables (name, line_nb, buffer) to the values of the last file (if it exists)
* - no more file to read if pile<0
*/

int yywrap()
{
   fclose(current_read_files[pile].address);
   yy_delete_buffer(YY_CURRENT_BUFFER);
   /*if (line_nb == 0)
      printf("\nWARNING : Empty file %s\n",current_read_files[pile].name);*/
   pile--;
   //num = 0;
    if (pile < 0)
      return 1;
   else {
      yyin = current_read_files[pile].address;

      #if defined GCC482 ||defined GCC472 ||  defined GCC471
      yy_switch_to_buffer((YY_BUFFER_STATE) current_read_files[pile].buffer);//gcc4.7
      #elif defined GCC441  || defined GCC442 ||  defined GCC443  || defined GCC444
// || defined GCC445
      yy_switch_to_buffer((yy_buffer_state*)current_read_files[pile].buffer);//gcc4.4
      #else 
// defined GCC445
      yy_switch_to_buffer(current_read_files[pile].buffer);//gcc3.x
      #endif /*test on GCC*/

      line_nb = current_read_files[pile].line_nb;
      return 0;
   }
}
