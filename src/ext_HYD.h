/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: ext_HYD.h
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/* Variables used when reading input data */
extern FILE *yyin;
extern int yydebug;
/* Structure containing the simulation caracteristics */
extern s_simul *Simul;
/* Table of the addresess of the read files */
extern s_file_io current_read_files[NPILE];
/* Number of folders defined by the ugloser */
extern int folder_nb;
/* File number */
extern int pile;
/* Position within the file being processed */
extern int line_nb;
/* Name of file folders */
extern char *name_out_folder[NPILE];

/* Functions of input.y */
void lecture(char *, FILE *);
int yylex();

#if defined GCC481 || defined GCC473 || defined GCC472 || defined GCC471
void yyerror(char const *);
#else
void yyerror(char *);
#endif /*test on GCC*/
