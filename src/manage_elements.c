/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_elements.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"

/* Structure containing the simulation caracteristics */
// extern s_simul *Simul;

/* Initializes all the elements in the network */
void HYD_create_elements(s_chyd *pchyd, FILE *fpout) {
  /* Reach, face, elemnt nb */
  int r, f, e;
  /* Element id number */
  int ele_id = 0;
  int ele_intern_id;
  /* Current reach, element and element description */
  s_reach_hyd *preach;
  s_element_hyd *pele;
  s_description_hyd *pdesc;

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    preach = pchyd->p_reach[r];

    /* Each reach must have at least two faces, otherwise there are no calculation elements */
    if (preach->nfaces < 2) {
      LP_printf(fpout, "Warning : less than two faces in the reach %s %s %d\n", preach->limits[ONE]->name, preach->limits[TWO]->name, preach->branch_nb);
      LP_printf(fpout, "          No element an be created\n");

    }

    else {
      preach->nele = preach->nfaces - 1;
      preach->p_ele = (s_element_hyd **)calloc(preach->nele, sizeof(s_element_hyd *));

      /* An element is created between each couple of transversal faces */
      ele_intern_id = 0;
      for (f = 0; f < preach->nfaces - 1; f++) {

        // printf("creation element n° %d\n",f);
        preach->p_ele[f] = HYD_create_element(pchyd);
        preach->p_ele[f]->reach = preach;
        preach->p_ele[f]->id[ABS_HYD] = ele_id++;
        preach->p_ele[f]->id[INTERN_HYD] = ele_intern_id++;
        preach->p_ele[f]->face[X_HYD][ONE] = preach->p_faces[f];
        preach->p_ele[f]->id[GIS_HYD] = preach->p_faces[f]->id[GIS_HYD];
        preach->p_ele[f]->face[X_HYD][ONE]->element[TWO] = preach->p_ele[f];
        preach->p_ele[f]->face[X_HYD][TWO] = preach->p_faces[f + 1];
        preach->p_ele[f]->face[X_HYD][TWO]->element[ONE] = preach->p_ele[f];
        preach->p_ele[f]->face[Y_HYD][ONE] = HYD_create_face(Y_HYD, pchyd);
        // printf("face Y ONE created\n");
        preach->p_ele[f]->face[Y_HYD][ONE]->element[TWO] = preach->p_ele[f];
        preach->p_ele[f]->face[Y_HYD][TWO] = HYD_create_face(Y_HYD, pchyd);
        // printf("face Y TWO created\n");
        preach->p_ele[f]->face[Y_HYD][TWO]->element[ONE] = preach->p_ele[f];
        preach->p_ele[f]->length = preach->p_faces[f]->geometry->dx; // NF 12/3/2018 Warning new_dx corresponds to an elevation. Here the length is dx, not new_dx !
        // LP_printf(fpout,"id_abs = %d length = %f\n",preach->p_ele[f]->id[ABS_HYD],preach->p_ele[f]->length);
      }
    }

    /* Description of the elements' geometry */
    for (e = 1; e < preach->nele - 1; e++) {

      pele = preach->p_ele[e];
      pdesc = pele->center->description;

      pdesc->Abscbottom = (pele->face[X_HYD][ONE]->description->Abscbottom + pele->face[X_HYD][TWO]->description->Abscbottom) / 2.;
      pdesc->Zbottom = (pele->face[X_HYD][ONE]->description->Zbottom + pele->face[X_HYD][TWO]->description->Zbottom) / 2.;
      pdesc->AbscRB = (pele->face[X_HYD][ONE]->description->AbscRB + pele->face[X_HYD][TWO]->description->AbscRB) / 2.;
      pdesc->AbscLB = (pele->face[X_HYD][ONE]->description->AbscLB + pele->face[X_HYD][TWO]->description->AbscLB) / 2.;
      pdesc->xc = (pele->face[X_HYD][ONE]->description->xc + pele->face[X_HYD][TWO]->description->xc) / 2.;
      pdesc->yc = (pele->face[X_HYD][ONE]->description->yc + pele->face[X_HYD][TWO]->description->yc) / 2.;
      pdesc->l = (pele->face[X_HYD][ONE]->description->l + pele->face[X_HYD][TWO]->description->l) / 2.; // LV 27/09/2012
      pdesc->curvature = (pele->face[X_HYD][ONE]->description->curvature + pele->face[X_HYD][TWO]->description->curvature) / 2.;
    }

    pele = preach->p_ele[0];
    pdesc = pele->center->description;

    pdesc->Abscbottom = pele->face[X_HYD][ONE]->description->Abscbottom;
    pdesc->Zbottom = pele->face[X_HYD][ONE]->description->Zbottom;
    pdesc->AbscRB = pele->face[X_HYD][ONE]->description->AbscRB;
    pdesc->AbscLB = pele->face[X_HYD][ONE]->description->AbscLB;
    pdesc->xc = pele->face[X_HYD][ONE]->description->xc;
    pdesc->yc = pele->face[X_HYD][ONE]->description->yc;
    pdesc->l = pele->face[X_HYD][ONE]->description->l; // LV 27/09/2012
    pdesc->curvature = pele->face[X_HYD][ONE]->description->curvature;

    pele = preach->p_ele[pchyd->p_reach[r]->nele - 1];
    pdesc = pele->center->description;

    pdesc->Abscbottom = pele->face[X_HYD][TWO]->description->Abscbottom;
    pdesc->Zbottom = pele->face[X_HYD][TWO]->description->Zbottom;
    pdesc->AbscRB = pele->face[X_HYD][TWO]->description->AbscRB;
    pdesc->AbscLB = pele->face[X_HYD][TWO]->description->AbscLB;
    pdesc->xc = pele->face[X_HYD][TWO]->description->xc;
    pdesc->yc = pele->face[X_HYD][TWO]->description->yc;
    pdesc->l = pele->face[X_HYD][TWO]->description->l; // LV 27/09/2012
    pdesc->curvature = pele->face[X_HYD][TWO]->description->curvature;

    pchyd->counter->nele_tot += preach->nele;
  }
}

/* Creates one element */
s_element_hyd *HYD_create_element(s_chyd *pchyd) {
  s_element_hyd *pele;

  pele = new_element();
  bzero((char *)pele, sizeof(s_element_hyd));

  pele->center = new_center();
  bzero((char *)pele->center, sizeof(s_center_hyd));
  pele->center->ele = pele;

  pele->center->description = HYD_create_description(pchyd);

  pele->mb = HYD_init_mass_balance();
  pele->modif = NO;

  return pele;
}

// NG : 06/03/2020 : Check if a river element (identified by its GIS_ID) is a river network outlet
int HYD_check_is_element_outlet(s_chyd *pchyd, int id_gis, FILE *fp) {

  int found = NO;
  int is_outlet = YES;
  int nb_reach = pchyd->counter->nreaches;
  int i, j, nele, tnode_reach;
  s_reach_hyd *preach;
  s_element_hyd *pele, *pele_down;

  for (i = 0; i < nb_reach; i++) {
    preach = pchyd->p_reach[i];
    nele = preach->nele;
    for (j = 0; j < nele; j++) {
      pele = preach->p_ele[j];
      if (pele->id[GIS_HYD] == id_gis) {
        found = YES;
        tnode_reach = pele->face[X_HYD][ONE]->tnode;
        break;
      }
    }
  }
  if (found == NO)
    LP_error(fp, "River element GIS ID %d is not part of the hydraulic network. Check either ELE_MUSK file or HDERM-AQ link file. Error in file %s, function %s at line %d.\n", id_gis, __FILE__, __func__, __LINE__);

  for (i = 0; i < nb_reach; i++) {
    preach = pchyd->p_reach[i];
    nele = preach->nele;
    for (j = 0; j < nele; j++) {
      pele = preach->p_ele[j];
      if (pele->face[X_HYD][ONE]->fnode == tnode_reach) {
        is_outlet = NO;
        break;
      }
    }
  }
  return is_outlet;
}

/* NG : 06/03/2020 : Extracts a discharge value of a given element.
 Used to redirect hyperdermic outlets discharges towards the aquifer in CAW_cAQ_add_recharge_from_HDERM() */
double HYD_extract_q_element(s_chyd *pchyd, int id_gis, FILE *fpout) {
  double q = 0;
  int nb_reach = pchyd->counter->nreaches;
  int i, j, nele;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  int found = NO;

  for (i = 0; i < nb_reach; i++) {
    preach = pchyd->p_reach[i];
    nele = preach->nele;
    for (j = 0; j < nele; j++) {
      pele = preach->p_ele[j];
      if (pele->id[GIS_HYD] == id_gis) {
        found = YES;
        q = pele->face[X_HYD][TWO]->hydro->Q[T_HYD];
        break;
      }
    }
  }
  if (found == NO)
    LP_error(fpout, "Couldn't find river element GIS ID %d in river network. Check your HDERM-AQ link file. Error in file %s, function %s at line %d.\n", id_gis, __FILE__, __func__, __LINE__);
  return q;
}

/**\fn s_element_hyd* HYD_check_is_ele_defined(s_chyd*, int, int, FILE*)
 *\brief Looks for a river element according to its ID.
 *\return If found, returns the pointer of the element.
 */
s_element_hyd *HYD_check_is_ele_defined(s_chyd *pchyd, int id, int type_id, FILE *fpout) {
  int r, ne;
  s_reach_hyd *preach;
  s_element_hyd *pele = NULL;
  int found = NO;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];
    for (ne = 0; ne < preach->nele; ne++) {
      if (preach->p_ele[ne]->id[type_id] == id) {
        pele = preach->p_ele[ne];
        found = YES;
        break;
      }
    }
  }
  if (found == NO)
    LP_error(fpout, "In libhyd%4.2f : File %s in %s line %d : River element %s ID %d does not exist. Check the consistency between transport_inflows and ele_musk input files.\n", NVERSION_HYD, __FILE__, __func__, __LINE__, HYD_id_type(type_id), id);

  return pele;
}

/**\fn void HYD_print_ele_transport_variable(s_chyd*, int, FILE*)
 *\brief Debug function : Prints transport variable associated with one given specie
 *\return -
 */
void HYD_print_ele_transport_variable(s_chyd *pchyd, int id_species, FILE *flog) {
  int r, ne;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  s_hydro_hyd *phydro;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];
    for (ne = 0; ne < preach->nele; ne++) {
      pele = preach->p_ele[ne];
      phydro = pele->center->hydro;
      LP_printf(flog, "Reach GIS id %d ABS id %d Element GIS id %d ABS id %d Variable (Specie %d) = %f\n", preach->id[GIS_HYD], preach->id[ABS_HYD], pele->id[GIS_HYD], pele->id[ABS_HYD], id_species, phydro->transp_var[id_species]);
    }
  }
}

/**
 * \fn s_element_hyd *HYD_find_element_by_id_io (int, int, s_chyd*, FILE*)
 * \brief Utilitarian function : Finds a river element based on its io* (id_reach,id_intern) coupled IDs
 * \return Pointer towards corresponding river element if found.
 */
s_element_hyd *HYD_find_element_by_id_io(int id_reach, int id_intern, s_chyd *pchyd, FILE *fp) {

  int i, j, found = NO;
  s_element_hyd *pele;
  s_reach_hyd *preach;

  for (i = 0; i < pchyd->counter->nreaches; i++) {
    preach = pchyd->p_reach[i];
    for (j = 0; j < preach->nele; j++) {
      if ((i == id_reach) && (id_intern == preach->p_ele[j]->id[INTERN_HYD])) {
        pele = preach->p_ele[j];
        found = YES;
        break;
      }
    }
  }
  if (found == NO) {
    LP_error(fp, "In libhyd%4.2f : In %s, function %s at line %d : Unable to find element INT_ID %d in reach INT_ID %d.\n", NVERSION_HYD, __FILE__, __func__, __LINE__, id_intern, id_reach);
  }
  return pele;
}