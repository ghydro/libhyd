/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_outputs.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"

/* Structure containing the simulation characteristics */
// extern s_simul *Simul;
s_inout_set_io *HYD_init_inout_set() {
  s_inout_set_io *pinout;
  int i;
  pinout = IO_create_inout_set(NOUTPUTS);
  pinout->init_from_file[IQ_IO] = NO;
  pinout->init_from_file[IZ_IO] = NO;
  for (i = 0; i < NOUTPUTS; i++)
    pinout->calc[i] = NO;
  return pinout;
}
s_output_hyd ***HYD_init_output_hyd() {
  s_output_hyd ***pout_hyd;
  int i;
  // pout_hyd=(s_output_hyd ***)malloc(NOUTPUTS*sizeof(s_lec_tmp_hyd **));//NF 24/4/2017 Bug le sizeof() ne contient pas la bonne structure
  pout_hyd = (s_output_hyd ***)malloc(NOUTPUTS * sizeof(s_output_hyd **)); // NF 24/4/2017 Bug le sizeof() ne contienait pas la bonne structure
  for (i = 0; i < NOUTPUTS; i++) {
    pout_hyd[i] = (s_output_hyd **)calloc(1, sizeof(s_output_hyd *));
  }
  return pout_hyd;
}
s_output_hyd *HYD_initialize_output(s_chronos_CHR *chronos) {
  s_output_hyd *pout_hyd;
  int i;

  pout_hyd = new_output();
  bzero((char *)pout_hyd, sizeof(s_output_hyd));
  pout_hyd->pout = IO_create_output();
  pout_hyd->pout->t_out[INI_IO] = chronos->t[BEGINNING];
  pout_hyd->pout->t_out[FINAL_IO] = chronos->t[END];
  pout_hyd->pout->deltat = chronos->dt;

  pout_hyd->graphics = NO;

  pout_hyd->pout->time_unit = 1.0;

  for (i = 0; i < NVAR_IO; i++) {
    pout_hyd->pout->hydvar[i] = NO;
    pout_hyd->pout->hydvar_unit[i] = 1.;
  }

  return pout_hyd;
}

void HYD_create_output_files(s_inout_set_io *pinout, s_output_hyd ***outputs, s_chyd *pchyd, FILE *fp) {

  pinout->fmb = HYD_create_header_mass_balance_file(fp);

  if (pinout->calc[PRINT_PK] == YES)
    HYD_print_pk(outputs, pinout, pchyd, fp);

  if (pinout->calc[MB_ELE] == YES) // LV 15/06/2012
    HYD_create_mb_at_elements_file(outputs, pinout, fp);

  HYD_print_outputs_formats(outputs, pinout, pchyd, fp);
}
s_output_hyd *HYD_chain_outputs(s_output_hyd *pout1, s_output_hyd *pout2) {
  pout1->next = pout2;
  pout2->prev = pout1;

  return pout1;
}

/* Initialisation of one lp extent */
s_lp_pk_hyd *HYD_init_lp_pk(char *river, double pk_up, int bnb_up, double pk_down, int bnb_down, s_chyd *pchyd) {
  s_lp_pk_hyd *ppk;
  int i;

  ppk = new_lp_pk();
  bzero((s_lp_pk_hyd *)ppk, sizeof(s_lp_pk_hyd)); // SW 25/01/2018 il faut initialiser a NULL
  ppk->reach_nb = (int *)calloc(pchyd->counter->nreaches, sizeof(int));
  for (i = 0; i < pchyd->counter->nreaches; i++)
    ppk->reach_nb[i] = -1;
  // fprintf(stdout,"reach_nb = %d done!\n",ppk->reach_nb[0]);
  ppk->pk_up = pk_up;
  ppk->branch_nb_up = bnb_up;
  ppk->pk_down = pk_down;
  ppk->branch_nb_down = bnb_down;
  ppk->river = river;

  return ppk;
}

/* Chains two time series' points */
s_ts_pk_hyd *HYD_chain_ts_pk(s_ts_pk_hyd *pk1, s_ts_pk_hyd *pk2) {
  pk1->next = pk2;
  pk2->prev = pk1;

  return pk1;
}

/* Chains two longitudinal profiles' extents */
s_lp_pk_hyd *HYD_chain_lp_pk(s_lp_pk_hyd *pk1, s_lp_pk_hyd *pk2) {
  pk1->next = pk2;
  pk2->prev = pk1;

  return pk1;
}

void HYD_create_time_series(s_output_hyd *pout_tmp, s_output_hyd ***p_outputs, int nts) {
  int i = 0;
  s_output_hyd *pout;
  pout = pout_tmp;
  p_outputs[TRANSV_PROFILE] = (s_output_hyd **)calloc(nts, sizeof(s_output_hyd *));

  while (pout != NULL) {
    p_outputs[TRANSV_PROFILE][i++] = pout;
    pout = pout->next;
  }
}

void HYD_create_long_profiles(s_output_hyd *pout, s_output_hyd ***p_outputs, int nlp) {
  int i = 0;

  p_outputs[LONG_PROFILE] = (s_output_hyd **)calloc(nlp, sizeof(s_output_hyd *));

  while (pout != NULL) {
    p_outputs[LONG_PROFILE][i++] = pout;
    pout = pout->next;
  }
}

void HYD_create_mass_balances(s_output_hyd *pout, s_output_hyd ***p_outputs, int nmb) {
  int i = 0;

  p_outputs[MASS_BALANCE] = (s_output_hyd **)calloc(nmb, sizeof(s_output_hyd *));

  while (pout != NULL) {
    p_outputs[MASS_BALANCE][i++] = pout;
    pout = pout->next;
  }
}

int HYD_find_river(char *name, int output_type, s_chyd *pchyd, FILE *fp) {
  int i = 0;

  while ((i < pchyd->counter->nupstr_limits) && (strcmp(pchyd->upstr_reach[i++]->p_faces[0]->pt_inflows[0]->name, name) != 0))
    ;

  if ((i >= pchyd->counter->nupstr_limits) && (strcmp(pchyd->upstr_reach[i - 1]->p_faces[0]->pt_inflows[0]->name, name) != 0)) {
    LP_error(fp, "Error on river's name %s for %s\n", name, HYD_name_output(output_type));
  }

  return i - 1;
}

void HYD_create_output_extents(s_output_hyd *pout, s_lp_pk_hyd *ppk) {
  int i;

  // ppk = rewind_lp_pk(ppk);
  ppk = HYD_browse_lp_pk(ppk, BEGINNING);

  pout->lp_pk = (s_lp_pk_hyd **)calloc(pout->npk, sizeof(s_lp_pk_hyd *));
  for (i = 0; i < pout->npk; i++) {
    pout->lp_pk[i] = ppk;
    ppk = ppk->next;
  }
}

s_lp_pk_hyd *HYD_browse_lp_pk(s_lp_pk_hyd *ppk, int beg_end) {

  s_lp_pk_hyd *ppk_tmp;
  ppk_tmp = ppk;
  if (ppk_tmp != NULL) {

    if (beg_end == BEGINNING) {
      while (ppk_tmp->prev != NULL)
        ppk_tmp = ppk_tmp->prev;
    }

    if (beg_end == END) {
      while (ppk_tmp->next != NULL)
        ppk_tmp = ppk_tmp->next;
    }
  }

  return ppk_tmp;
}

void HYD_create_output_points(s_output_hyd *pout, s_ts_pk_hyd *ppk) {
  int i;
  int npk;
  npk = pout->npk;
  ppk = HYD_browse_ts_pk(ppk, BEGINNING);

  pout->ts_pk = (s_ts_pk_hyd **)calloc(npk, sizeof(s_ts_pk_hyd *));
  for (i = 0; i < npk; i++) {
    pout->ts_pk[i] = ppk;
    ppk = ppk->next;
  }
}

s_ts_pk_hyd *HYD_browse_ts_pk(s_ts_pk_hyd *ppk_tmp, int beg_end) {

  s_ts_pk_hyd *ppk;
  ppk = ppk_tmp;
  if (ppk != NULL) {

    if (beg_end == BEGINNING) {
      while (ppk->prev != NULL)
        ppk = ppk->prev;
    }

    if (beg_end == END) {
      while (ppk->next != NULL)
        ppk = ppk->next;
    }
  }

  return ppk;
}

void HYD_calculate_output_domain(s_lp_pk_hyd *ppk, int output_type, s_chyd *pchyd, FILE *fp) {
  int n, i;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  double pk0, pkf, pkinit;
  int end_of_domain_reached;

  n = HYD_find_river(ppk->river, output_type, pchyd, fp);
  preach = pchyd->upstr_reach[n];

  /* Check if the two given pks are in the studied area */
  pk0 = ppk->pk_up;
  pkf = ppk->pk_down;

  if (pkf < pk0) {
    ppk->pk_down = pk0;
    ppk->pk_up = pkf;
    pk0 = ppk->pk_up;
    pkf = ppk->pk_down;
  }

  if (pkf < preach->limits[UPSTREAM]->pk) {
    LP_printf(fp, "output PK  %f out of the studied network, replaced by PK %f\n", pkf / 1000.0, (preach->limits[UPSTREAM]->pk) / 1000.0);
    pkf = preach->limits[UPSTREAM]->pk + 10;
  }

  if (pk0 < preach->limits[UPSTREAM]->pk)
    ppk->pk_up = preach->limits[UPSTREAM]->pk;

  /* Find first reach and first element */
  for (i = 0; i < pchyd->counter->nreaches; i++)
    pchyd->p_reach[i]->passage = NO;

  i = 0;
  end_of_domain_reached = NO;

  HYD_propage_output(preach, &i, pk0, ppk->branch_nb_up, pkf, ppk->branch_nb_down, ppk->reach_nb, &end_of_domain_reached, pchyd);

  if (ppk->reach_nb[0] < 0) {

    if (pk0 == pkf)
      LP_printf(fp, "No path for the pk %f, %s\n", pkf / 1000.0, HYD_name_output(output_type));
    else
      LP_printf(fp, "No domain found between the pk %f and %f, %s\n", pk0 / 1000.0, pkf / 1000.0, HYD_name_output(output_type));

    while ((preach != NULL) && (preach->limits[DOWNSTREAM]->pk < pk0)) {

      if (preach->limits[DOWNSTREAM]->ndownstr_reaches > 0)
        preach = preach->limits[DOWNSTREAM]->p_reach[DOWNSTREAM][0];
      else
        break;
    }

    if (preach != NULL)
      ppk->reach_nb[0] = preach->id[ABS_HYD];
    else
      ppk->reach_nb[0] = pchyd->counter->nreaches;
  }

  preach = pchyd->p_reach[ppk->reach_nb[0]];
  pkinit = preach->limits[UPSTREAM]->pk;
  pele = preach->p_ele[0];

  while ((pele != NULL) && (pkinit + pele->length < pk0)) {
    pkinit += pele->length;
    if (pele->id[ABS_HYD] < preach->p_ele[preach->nele - 1]->id[ABS_HYD])
      pele = pele->face[X_HYD][TWO]->element[TWO];
  }

  if (pele != NULL)
    ppk->pele = pele;
  else
    ppk->pele = preach->p_ele[preach->nele - 1];

  if (end_of_domain_reached == NO)
    LP_printf(fp, "PK %f could not be reached, %s\n", pkf / 1000.0, HYD_name_output(output_type));
}

void HYD_calculate_ts_domain(s_ts_pk_hyd *ppk, s_chyd *pchyd, FILE *fp) {
  int r = 0;
  int ne;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  double pkinit;
  int found = NO;

  while ((found == NO) && (r < pchyd->counter->nreaches)) {

    preach = pchyd->p_reach[r];
    // LP_printf(fp,"river in preach : %s, river in ppk : %s\n",preach->river,ppk->river); // BL to debug
    //  LP_printf(fp,"in river : %s pk at uptsream : %f,pk at downstream : %f \n",preach->river,preach->limits[UPSTREAM]->pk,preach->limits[DOWNSTREAM]->pk); // BL to debug
    //  LP_printf(fp,"pk at output %f \n",ppk->pk); // BL to debug
    if ((strcmp(preach->river, ppk->river) == 0) && (preach->branch_nb == ppk->branch_nb)) {
      if (ppk->pk_type == LENGTH_HYD) {

        ppk->pk = preach->limits[UPSTREAM]->pk + ppk->pk;
        // LP_printf(fp,"pk_new at output %f \n",ppk->pk); //BL to debug
      }
      if ((preach->limits[UPSTREAM]->pk <= ppk->pk) && (preach->limits[DOWNSTREAM]->pk >= ppk->pk)) {

        found = YES;
        ppk->reach_nb = preach->id[ABS_HYD];
        pkinit = preach->limits[UPSTREAM]->pk;

        ne = 0;
        pele = preach->p_ele[ne];

        while ((ne < preach->nele - 1) && (pkinit + 0.5 * pele->length < ppk->pk)) {
          pkinit = pele->face[X_HYD][TWO]->element[TWO]->center->pk;
          pele = pele->face[X_HYD][TWO]->element[TWO];
          ne++;
        }
        ppk->pele = pele;
        LP_printf(fp, "output pk %f is linked with ele %d in reach %s\n", ppk->pk, ppk->pele->id[ABS_HYD], ppk->pele->reach->river);
      }
    }
    r++;
  }

  if (found == NO)
    LP_error(fp, "Cannot find pk %f in branch %d of %s in given river network\n", ppk->pk / 1000., ppk->branch_nb, ppk->river);
}

void HYD_propage_output(s_reach_hyd *preach, int *k, double pk0, int bras0, double pkf, int brasf, int *reach_nb, int *end_of_domain_reached, s_chyd *pchyd) {
  int i, nbr;
  double pkinit;
  s_singularity_hyd *pcl;
  s_reach_hyd *preach1;
  int sortir;

  pkinit = preach->limits[UPSTREAM]->pk;

  pcl = preach->limits[DOWNSTREAM];
  preach->passage = YES;

  if (pcl->pk >= pkf)
    *end_of_domain_reached = YES;

  if ((pcl->pk >= pk0) && (pkinit <= pkf)) {
    if (((pkinit >= pk0) && (pcl->pk <= pkf)) || ((pkinit < pk0) && ((bras0 < 0) || ((bras0 > 0) && (preach->branch_nb == bras0)))) || ((pcl->pk > pkf) && ((brasf < 0) || ((brasf > 0) && (preach->branch_nb == brasf)))))
      reach_nb[(*k)++] = preach->id[ABS_HYD];
  }

  nbr = 0;

  if (pcl->ndownstr_reaches > 0)
    preach1 = pcl->p_reach[DOWNSTREAM][nbr];
  else
    preach1 = NULL;

  while (preach1 != NULL) {

    sortir = YES;

    if (pcl->pk < pkf) {

      if (pcl->type == CONF_DIFF) {

        for (i = 0; i < pchyd->counter->nsing; i++)
          pchyd->p_sing[i]->passage = NO;
        HYD_test_path(brasf, pkf, preach1, &sortir);
      }

      else
        sortir = NO;
    }

    if ((preach1->passage == NO) && (sortir == NO))
      HYD_propage_output(preach1, k, pk0, bras0, pkf, brasf, reach_nb, end_of_domain_reached, pchyd);

    if (pcl->ndownstr_reaches > nbr)
      preach1 = pcl->p_reach[DOWNSTREAM][++nbr];
    else
      preach1 = NULL;
  }
}

void HYD_test_path(int bras, double pkf, s_reach_hyd *preach, int *sortir) {
  s_reach_hyd *preach1;
  s_singularity_hyd *pcl;
  int nbr = 0;

  pcl = preach->limits[DOWNSTREAM];
  pcl->passage = YES;

  if ((pcl->pk >= pkf) && (((bras > 0) && (preach->branch_nb == bras)) || (bras < 0)))
    *sortir = NO;

  if (pcl->ndownstr_reaches > 0) {

    preach1 = pcl->p_reach[DOWNSTREAM][nbr];

    while (preach1 != NULL) {

      if ((preach1->limits[DOWNSTREAM]->passage == NO) && (*sortir == YES))
        HYD_test_path(bras, pkf, preach1, sortir);

      preach1 = pcl->p_reach[DOWNSTREAM][++nbr];
    }
  }
}
