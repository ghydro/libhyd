/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libhyd
* FILE NAME: input.y
* 
* CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
* 
* LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with 
* Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using 
* a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as 
* long as geometries are defined, 1D approach handles islands, tributaries, 
* and hydraulics works such as dams. 
*
* Library developed at the Geosciences Center, joint research center 
* of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
*
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/


%{



#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <libprint.h>
#include <time_series.h>
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD_full.h"
#include "global_HYD.h"
#include "ext_HYD.h"


  
  /* Previous time read in a time series */
  double told = 0.;
  /* Time */
  double tnew = 0.;
  /* Unit of the time or of ft->t in a s_ft structure */
  double unit_t = 1.;
  /* Unit of ft->f in a s_ft structure */
  double unit_f = 1.;
  double unit_x,unit_y,unit_z,unit_l,unit_kappa;
  /* Pointer towards the currently read reach and the total chain of reaches */
  s_reach_hyd *preach, *preachtot;
  /* Reach in which the bathymetry is currently being defined */
  s_reach_hyd *preach_bat;
  /* Reach in which the inflow arrives */
  s_reach_hyd *preach_infl;
  /* Pointer towards a transversal face */
  s_face_hyd *pface;
  /* Pointer towards a calculation element */
  s_element_hyd *pele;
  /* Pointer towards an inflow */
  s_inflow_hyd *pinflow;
  /* Pointer towards the currently read singularity and the total chain of singularities */
  s_singularity_hyd *psing, *psingtot;
  /* Current BC characteristics */
  s_BC_char_hyd *pwork, *pworktot;
  /* Nb of hydraulic works constituting the BC */
  int nworks;
  /* Type of hydraulic work*/
  int worktype;
  /* Geometry of the face described in Lambert coordinates */
  s_pointAbscZ_hyd *pptAbscZ;
  /* Geometry of the face described in XYZ coordinates */
  s_pointXYZ_hyd *pptXYZ;
  /* Number of steps in a time series s_ft */
  int nbsteps;
  /* Loop indexes */
  int s,r,i,p,f,e;
  /* Outputs */
  s_output_hyd *pout_hyd;
  s_output_hyd *pts=NULL,*plp=NULL,*pmb=NULL;
  s_ts_pk_hyd *pts_pk,*pts_pktot;
  s_lp_pk_hyd *plp_pk,*plp_pktot,*pmb_pk,*pmb_pktot;
  int output_type;
  char *river;
  char**FnodeTnode;
  int nb_outlet;
  int nb_reach=0;
  int dim_tmp_lec = NB_LEC_MAX;
  int nb_realloc = 1;
%}


%union {
  double real;
  int integer;
  char *string;
  s_ft *function;
  s_singularity_hyd *sing;
  s_reach_hyd *reach;
  s_pointXYZ_hyd *pointXYZ;
  s_pointAbscZ_hyd *pointAbscZ;
  s_face_hyd *face;
  s_inflow_hyd *inflow;
  s_BC_char_hyd *boundary;
  s_ts_pk_hyd *ts_pk;
  s_lp_pk_hyd *lp_pk;
}

%token <real> LEX_DOUBLE LEX_A_UNIT LEX_VAR_UNIT
%token <integer> LEX_INT
%token LEX_INIT LEX_INIT_Z LEX_INIT_Q
%token LEX_OPENING_BRACE LEX_CLOSING_BRACE LEX_OPENING_BRACKET LEX_CLOSING_BRACKET LEX_REVERSE_ARROW
%token LEX_INV LEX_POW LEX_EQUAL LEX_ARROW LEX_COLON LEX_SEMI_COLON
%token <string> LEX_NAME
%token LEX_INPUT_FOLDER LEX_OUTPUT_FOLDER 
%token LEX_SIMUL LEX_TIMESTEP LEX_CHRONOS LEX_BEGIN
%token <integer> LEX_MONTH
%token <integer> LEX_ANSWER LEX_TIME 
%token LEX_SET 
%token LEX_DIM LEX_TYPE LEX_CALC_CURVATURE 
%token <integer> LEX_CALC_STATE LEX_GENERALP
%token LEX_NETWORK LEX_PK LEX_STRICKLER LEX_HYDWORKS LEX_POSITION
%token <integer> LEX_FION LEX_DIR
%token LEX_PROFILES LEX_INFLOWS LEX_SING LEX_REACH LEX_TRAJECTORY
%token <integer> LEX_INFLOW_TYPE
%token LEX_ABSC LEX_X LEX_Y LEX_HYD_VAR LEX_SHAPE
%token <integer> LEX_BC_TYPE LEX_CROSS_SECTION_TYPE
%token <integer> LEX_WORK LEX_WORK_PARAM
%token LEX_FILE_NAME LEX_POINTS LEX_EXTENT 
%token LEX_OUTPUTS LEX_VAR LEX_TUNIT LEX_GRAPHICS
%token <integer> LEX_ONE_VAR LEX_OUTPUT_TYPE LEX_GRAPH_TYPE
%token <integer> LEX_MUSK_PAR LEX_CALC_MUSK_PAR LEX_DEF_SCHEM_TYPE
%token LEX_AREA LEX_SLOPE LEX_SCHEM_TYPE LEX_NETWORK_MUSK LEX_REACH_MUSK LEX_ELE_MUSK


%type <real> flottant mesure 
%type <real> a_unit units one_unit a_unit_value all_units read_units
%type <function> f_ts f_t 
%type <boundary> fion hydworks hydwork hydstructures
%type <sing> singularity singularities
 // sings_musk sing_musk
%type <reach> reach reaches 
 //reaches_musk reach_musk
%type <pointAbscZ> sectionAbscZ sectionAbscZs 
%type <pointXYZ> sectionXYZ sectionXYZs
%type <face> cross_sections cross_section bathymetry trajectory traj_point traj_points 
%type <face> faces_musk face_musk def_elements_musk
%type <inflow> inflow inflows
%type <lp_pk> extents
%type <ts_pk> points one_pk pk_list

%start beginning
%%


beginning : begin
            datas
            outputs;


begin : paths model_settings
| model_settings 
;

/* This part enables to define specific input and output folders */
paths : path paths
| path
;

path : input_folders
| output_folder
;

input_folders : LEX_INPUT_FOLDER folders
;

/* List of input folders' names */
folders : folder folders
| folder 
;

folder : LEX_EQUAL LEX_NAME
{
  if (folder_nb >= NPILE) {
    fprintf(Simul->poutputs,"Only %d folder names are available as input folders\n",NPILE);
    printf("Only %d folder names are available as input folders\n",NPILE);
  }
  name_out_folder[folder_nb++] = strdup($2);
  fprintf(Simul->poutputs,"path : %s\n",name_out_folder[folder_nb-1]);
  printf("path : %s\n",name_out_folder[folder_nb-1]);
  free($2);
} 
;

/* Definition of the folder where outputs are stored */
output_folder : LEX_OUTPUT_FOLDER LEX_EQUAL LEX_NAME
{
  int noname,str_len;
  char *new_name;
  char cmd[MAXCHAR];
  FILE *fp;

  fp=Simul->poutputs;

  Simul->pinout->name_outputs = $3;
  str_len=strlen($3)+8;
  new_name = (char *)calloc(str_len,sizeof(char));
  sprintf(new_name,"RESULT=%s",$3);
  fprintf(fp,"%s\n",new_name);
  printf("%s\n",new_name);
  noname = putenv(new_name); 
  if (noname == -1)
    LP_error(fp,"File %s, line %d : undefined variable RESULT\n",
		current_read_files[pile].name,line_nb); 
  sprintf(cmd,"mkdir %s",getenv("RESULT")); 
  system(cmd);
  //sprintf(cmd,"mkdir %s/time_series",getenv("RESULT")); 
  //system(cmd);
  //sprintf(cmd,"mkdir %s/longitudinal_profiles",getenv("RESULT")); 
  //system(cmd);
  //free(cmd);
  //free($3);
} 
;

/* This part defines global parameters of the model and stores them in the simulation structure */
model_settings : LEX_SIMUL LEX_EQUAL LEX_OPENING_BRACE simul_name atts_simul LEX_CLOSING_BRACE
{
  LP_printf(Simul->poutputs,"Global parameters of the simulation have been defined\n\n");
  
}
;

simul_name : LEX_NAME
{
  Simul->name = $1;
  LP_printf(Simul->poutputs,"\nSimulation name = %s\n",Simul->name);
  
}
;

atts_simul : att_simul atts_simul
| att_simul
; 

att_simul : def_initialization
| def_chronos
| def_settings
;

def_initialization : LEX_INIT LEX_EQUAL brace def_inits brace

def_inits : def_init def_inits
| def_init
;

def_init : init_Z
| init_Q
;

init_Z : LEX_INIT_Z LEX_EQUAL f_ts
{
  Simul->pchyd->settings->init_Z = $3;
  Simul->pinout->init_from_file[0] = YES;
}
;

init_Q : LEX_INIT_Q LEX_EQUAL f_ts
{
  Simul->pchyd->settings->init_Q = $3;
  Simul->pinout->init_from_file[1] = YES;
}
;

def_chronos : LEX_CHRONOS LEX_EQUAL LEX_OPENING_BRACE def_times LEX_CLOSING_BRACE

def_times : def_time def_times
| def_time
;

def_time : LEX_TIMESTEP LEX_EQUAL mesure
{
  Simul->chronos->dt = $3;
  
LP_printf(Simul->poutputs,"time step = %f s\n",Simul->chronos->dt);
  
} 
| LEX_TIME LEX_EQUAL mesure
{
  Simul->chronos->t[$1] = $3;
  LP_printf(Simul->poutputs,"t %s = %f s\n",HYD_name_extremum($1),Simul->chronos->t[$1]);
  
}
| LEX_BEGIN LEX_EQUAL LEX_INT LEX_MONTH LEX_INT flottant flottant
{
  Simul->chronos->day_d = $3;
  Simul->chronos->month = $4;
  Simul->chronos->year[BEGINNING] = $5;
  Simul->chronos->hour_h = $6 + $7 / 60;
  LP_printf(Simul->poutputs,"simulation starts on %d %s %d at %f h\n",
	    Simul->chronos->day_d,TS_name_month(Simul->chronos->month,Simul->poutputs),
	  Simul->chronos->year,Simul->chronos->hour_h);
  
}
;

def_settings : LEX_SET LEX_EQUAL LEX_OPENING_BRACE settings LEX_CLOSING_BRACE
;

settings : setting settings
| setting
;

setting : LEX_DIM LEX_EQUAL LEX_INT
{
  Simul->pchyd->settings->ndim = $3; // BL ici !!!
  LP_printf(Simul->poutputs,"ndim = %d\n",Simul->pchyd->settings->ndim);
 
}
| LEX_TYPE LEX_EQUAL LEX_CALC_STATE
{
  Simul->pchyd->settings->calc_state = $3;
  LP_printf(Simul->poutputs,"calculation in %s state\n",HYD_name_calc_state(Simul->pchyd->settings->calc_state));
  
}
| LEX_CALC_CURVATURE LEX_EQUAL LEX_ANSWER
{
  Simul->pchyd->settings->calc_curvature = $3;
  LP_printf(Simul->poutputs,"calculation of the curvature radius = %s\n",
	  HYD_name_answer(Simul->pchyd->settings->calc_curvature));
  
}
| LEX_GENERALP LEX_EQUAL mesure
{
  Simul->pchyd->settings->general_param[$1] = $3;
  LP_printf(Simul->poutputs,"%s = %f\n",HYD_name_general_param($1),Simul->pchyd->settings->general_param[$1]);
  
}
| LEX_SCHEM_TYPE LEX_EQUAL LEX_DEF_SCHEM_TYPE
{
  Simul->pchyd->settings->schem_type=$3;
}
;


/* Data giving information on the river network : singularities, reaches, inflows, cross_sections... */
datas : data datas
| data 
;

data : LEX_NETWORK LEX_EQUAL brace network_attributes brace
{
  
  HYD_BC_faces(Simul->pchyd,Simul->poutputs);
  //HYD_print_sing(Simul->pchyd,Simul->poutputs);
  HYD_verif_singularities(Simul->pchyd,Simul->poutputs);
  HYD_calculate_pk_sing(Simul->pchyd,Simul->poutputs);
  HYD_calculate_pk_faces_centers(Simul->pchyd,Simul->poutputs);
  if (Simul->pchyd->settings->calc_curvature == YES)
    HYD_calculate_curvature_radius(Simul->pchyd);
  else if (Simul->pchyd->settings->calc_curvature == NO)
    HYD_zero_curvature(Simul->pchyd,Simul->poutputs);
  //assign_river();//LV nov2014 déplacé après initialisation hydro
}
;

network_attributes : network_att network_attributes
| network_att
;

network_att : network_StVenant
| network_Musk;



network_StVenant : def_singularities
| def_reaches
| bathymetry //type face
{
  Simul->clock->time_spent[LEC_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
  HYD_table_hydro_faces($1,Simul->pchyd,Simul->chronos,Simul->poutputs);
  HYD_create_faces($1,Simul->pchyd,Simul->chronos,Simul->poutputs);
  HYD_create_elements(Simul->pchyd,Simul->poutputs);
  HYD_table_hydro_elements(Simul->pchyd,Simul->chronos,Simul->poutputs);
  Simul->clock->time_spent[TAB_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
}
| def_inflows
;

network_Musk : def_network_musk
	       //| def_reaches_musk 
| def_elements_musk
{
  Simul->clock->time_spent[LEC_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
  
  HYD_table_hydro_faces($1,Simul->pchyd,Simul->chronos,Simul->poutputs);
  
  HYD_create_faces($1,Simul->pchyd,Simul->chronos,Simul->poutputs);
 
  HYD_create_elements(Simul->pchyd,Simul->poutputs);
  
  HYD_table_hydro_elements(Simul->pchyd,Simul->chronos,Simul->poutputs);
  Simul->clock->time_spent[TAB_CHR] += CHR_end_timer();//LV 3/09/2012
  CHR_begin_timer();//LV 3/09/2012
}
| def_inflows
;

/* network muskingum attributes
 * - name
 * - id 
 * - FromNode ToNode
 *
 * Syntax :  tab of :
 *           name id FromNode ToNode
 *
 *
 *
 *
 *
 */
def_network_musk : intro_network_musk  network_musks brace
{
   int i;
  int max_FnodeTnode;
  s_singularity_hyd *psingtot;
  s_reach_hyd *preach;
  char **FnodeTnode;
 
  nb_outlet=0;
  psingtot=HYD_create_musk_sing(Simul->plec,Simul->pchyd,dim_tmp_lec);
  max_FnodeTnode=HYD_get_max_node(Simul->plec,dim_tmp_lec,Simul->poutputs);
  FnodeTnode=HYD_finalyze_sing(psingtot,Simul->pchyd,Simul->chronos,max_FnodeTnode,Simul->poutputs);
  LP_printf(Simul->poutputs,"max_Fnode_Tnode %d\n",max_FnodeTnode);
  HYD_create_singularities(psingtot,Simul->pchyd,Simul->poutputs);
  preach=HYD_create_reaches_musk_coupled(Simul->plec,Simul->pchyd,dim_tmp_lec,FnodeTnode,Simul->pcarac_fp,Simul->poutputs);
  HYD_create_reaches(preach,Simul->pchyd);
  
  for(i=0;i<2*max_FnodeTnode;i++)
    {
      if(FnodeTnode[i]!=NULL)
	{
	  free(FnodeTnode[i]);
	  FnodeTnode[i]=NULL;
	}
    }
  free(FnodeTnode);
  FnodeTnode=NULL;
  Simul->plec=HYD_free_plec(Simul->plec,dim_tmp_lec,Simul->poutputs);
};
/*
def_network_musk : intro_network_musk  sings_musk brace
{
  psingtot = $2;
  nb_outlet=0;
  FnodeTnode=HYD_finalyze_sing(psingtot,Simul->pchyd,Simul->chronos,&nb_outlet,Simul->poutputs);
  HYD_create_singularities(psingtot,Simul->pchyd,Simul->poutputs);
  LP_printf(Simul->poutputs,"nb_outlet %d\n",nb_outlet);
};
*/
intro_network_musk : LEX_NETWORK_MUSK LEX_EQUAL brace
{
  Simul->pchyd->settings->general_param[DX]=CODE; //BL si muskingum pas de discretisation longitudinale !!
}
network_musks : network_musk network_musks
| network_musk
;

//network_musk : LEX_NAME LEX_INT LEX_INT LEX_INT flottant flottant flottant flottant flottant flottant flottant flottant flottant flottant //BL avec pk
network_musk : LEX_NAME LEX_INT LEX_INT LEX_INT flottant flottant flottant flottant flottant flottant flottant flottant flottant 
{

  if($3 >= dim_tmp_lec)
    {
      ++ nb_realloc;
      dim_tmp_lec=HYD_resize_tmp_lec($3,Simul->plec,nb_realloc);
      
    }
  //Simul->tmp_lec[$3]=HYD_create_tmp_lec($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14); // BL avec pk
  Simul->plec[$3]=HYD_create_tmp_lec($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13);
  nb_reach++;
  //free($1);
}
;

/* Muskingum Reaches' characteristics
 * - name of the upsteam limit
 * - name of the downstream limit
 * - Strickler coefficients depending on the discharge
 * 
 * Syntax : upstream_name -> downstream_name 1 {
 *                                               min_radius = [m] 1000 
 *                                               strickler = 40.
 *                                             };	  
 */
		     /*
def_reaches_musk : LEX_REACH_MUSK LEX_EQUAL brace reaches_musk brace
{
  preachtot = $4;
  HYD_create_reaches(preachtot,Simul->pchyd);
  for(i=0;i<2*(Simul->pchyd->counter->nsing-nb_outlet);i++)
    {
      if(FnodeTnode[i]!=NULL)
	{
	  free(FnodeTnode[i]);
	  FnodeTnode[i]=NULL;
	}
    }
  free(FnodeTnode);
  FnodeTnode=NULL;
}
;

reaches_musk : reach_musk reaches_musk
{
  $$ = HYD_chain_reaches($1,$2);
}
| reach_musk
{
  $$ =$1;
}
;

reach_musk : LEX_NAME LEX_INT LEX_INT LEX_INT flottant flottant flottant flottant flottant flottant flottant flottant flottant
{
  int i;
  //  LP_printf(Simul->poutputs,"looking for upstream singularity %s\n",FnodeTnode[$3]);
  //LP_printf(Simul->poutputs,"looking for downstream singularity %s\n",FnodeTnode[$4]);
  preach = HYD_create_reach(FnodeTnode[$3],FnodeTnode[$4],1,Simul->pchyd,Simul->poutputs);
  HYD_set_reach_param(preach,$5,$6,$7,$8,$9,$10,$11,$12,$13,Simul->poutputs);
  $$ = preach;
  LP_printf(Simul->poutputs,"The reach %s %s %d has been read\n",
	  preach->limits[ONE]->name,preach->limits[TWO]->name,preach->branch_nb);
  free($1);
};
		     */
/* Muskingum element
 * A Muskingum element is constructed by intersecting reach geometrie and groundwater mesh
 * A Muskingum element is defined by tabular : 
 *  river_name id_river fnode tnode id_gw ele_length Z_bottom h_ini
 * - river_name is the name of the river in wich the element is defined
 * - id_river is the river id
 * - fnode is the From node of the element in the river permiting to locate the element in the river reach
 * - tnode is the To node of the element in the river permiting to locate the element in the river reach
 * - id_gw is the identifient of the groundwater mesh doit pouvoir creer list_riv in gw_mesh ici !!
 * - ele_length is the length of the element
 * - Z_bottom is the elevation of the river bottom in element (optional) 
 * - h_ini is the river deepness in element (optional)
 * - !!!!! Z_bottom OR h_ini HAVE TO BE DEFINED !!!!!
 */
def_elements_musk: LEX_ELE_MUSK LEX_EQUAL brace faces_musk  brace
{
  s_face_hyd *pface_tot,*pface_cpy;
  pface_tot = $4;
  //LP_printf(Simul->poutputs,"Entering in def_elements_musk in input.y \n");
  pface_cpy=HYD_reorder_musk_faces(pface_tot,Simul->pchyd,Simul->poutputs);
  //LP_printf(Simul->poutputs," pface_old \n");
  // HYD_print_faces(pface_tot,Simul->poutputs);
  pface_tot=HYD_free_chained_faces(pface_tot,Simul->poutputs);
  //LP_printf(Simul->poutputs," pface_NEW \n");
  //HYD_print_faces(pface_cpy,Simul->poutputs);
  HYD_create_all_musk_geometry(pface_cpy,Simul->poutputs);
  //HYD_print_geometry(pface_cpy,Simul->poutputs);
  $$=pface_cpy;
}
;


faces_musk : face_musk faces_musk
{
  $$ = HYD_chain_faces($1,$2);
}
| face_musk
{
  $$ = $1;
}
;

face_musk : LEX_NAME LEX_INT LEX_INT LEX_INT LEX_INT flottant flottant flottant
{
  char *new_name;
  s_face_hyd *pftmp=NULL;
  int str_len;
  str_len=strlen($1) + 8;
  new_name = (char *)calloc(str_len,sizeof(char));
  sprintf(new_name,"%s_%d",$1,$2);
  preach_bat = HYD_find_reach(new_name,1,Simul->pchyd);
  preach_bat->nfaces++;
  free(new_name);
  str_len=strlen($1) + 14;
  new_name = (char *)calloc(str_len,sizeof(char));
  sprintf(new_name,"%s_%d",$1,$2*100000+$3);
  pftmp=HYD_create_musk_face(new_name,$2,$3,$4,$5,$6,$7,$8,preach_bat,Simul->pchyd,Simul->poutputs);
  if(pftmp!=NULL)
    $$=pftmp;
  else
    LP_error(Simul->poutputs,"the face is null in face_musk, input.y\n");

  free($1);
}
; 

/* Singularities attributes
 * - name
 * - pk 
 * - weir characteristics for dams
 *
 * Syntax : upstream_point;
 *          downstream_dam {
 * 	                     pk = [km] 100. 
 *	                     z(t) = { [d]  [m] 
 *		                      0.0 17.40
 *	                            }
 *                         };
 */
def_singularities : LEX_SING LEX_EQUAL brace singularities brace
{
  psingtot = $4;
  HYD_create_singularities(psingtot,Simul->pchyd,Simul->poutputs);
}
;

singularities : singularity singularities
{
  $$ = HYD_chain_singularities($1,$2);
}
| singularity
{
  $$ = $1;
}
;

singularity : read_sing_name sing_attributes 
{
  $$ = new_singularity(); //curieux non ?
  $$ = psing;
  LP_printf(Simul->poutputs,"The singularity %s has been read\n",psing->name);
  
}
;

read_sing_name : LEX_NAME 
{
  psing = HYD_create_singularity($1,Simul->pchyd);
  nworks = 0;
  free($1);
}
;

sing_attributes : brace sequence_sing_attributes brace LEX_SEMI_COLON 
| LEX_SEMI_COLON 
;

sequence_sing_attributes : sing_attribute sequence_sing_attributes
| sing_attribute 
;

sing_attribute : LEX_PK LEX_EQUAL mesure
{
  psing->pk = $3;
  psing->passage = YES;
}
| LEX_TYPE LEX_EQUAL LEX_BC_TYPE
{
  psing->type = $3;
}
| LEX_POSITION LEX_EQUAL LEX_DIR LEX_INT
{
  if (psing->BC_char == NULL)
    psing->BC_char = ((s_BC_char_hyd **) calloc(1,sizeof(s_BC_char_hyd *)));
  psing->BC_char[0]->position[0] = $3;
  psing->BC_char[0]->position[1] = $4 - 1;
}
| fion
{
  if (psing->BC_char == NULL)
    psing->BC_char = ((s_BC_char_hyd **) calloc(1,sizeof(s_BC_char_hyd *)));
  psing->BC_char[0] = $1;
  psing->nworks = 1;
}
| hydstructures
{
  pworktot = $1;
  psing->nworks = nworks;
  psing->BC_char = ((s_BC_char_hyd **) calloc(nworks,sizeof(s_BC_char_hyd *)));
  psing->BC_char[0] = pworktot;

  for (i = 1; i < nworks; i++) {
    pworktot = pworktot->next;
    psing->BC_char[i] = pworktot;
  }
}
;

hydstructures : LEX_HYDWORKS LEX_EQUAL brace hydworks brace
{
  $$ = $4;
}
;

hydworks : hydwork hydworks
{
  $$ = HYD_chain_hydworks($1,$2);
}
| hydwork
{
  $$ = $1;
}
;

hydwork : intro_work work_atts brace
{
  $$ = pwork;
}
;


intro_work : LEX_WORK LEX_EQUAL brace
{
  worktype = $1;
  pwork = HYD_create_hydwork(worktype);
  nworks++;
}
;

work_atts : work_att work_atts
| work_att
;

work_att : LEX_WORK_PARAM LEX_EQUAL mesure
{
  pwork->hydwork_param[$1] = $3;
}
| LEX_POSITION LEX_EQUAL LEX_DIR LEX_INT
{
  pwork->position[0] = $3;
  pwork->position[1] = $4 - 1;
}
| fion
{
  LP_printf(Simul->poutputs,"Dans le fion !! \n");
  pwork->fion_type = $1->fion_type;
  pwork->fion = $1->fion;
  pwork->fion_t = TS_function_value_t(Simul->chronos->t[INI],pwork->fion,Simul->poutputs);//LV nov2014
}
;

fion : LEX_FION LEX_EQUAL brace read_units f_ts brace
{
  $$ = HYD_create_hydwork(NONE);
  $$->fion_type = $1;
  $$->fion = $5;
}
;


/* Reaches' characteristics
 * - name of the upsteam limit
 * - name of the downstream limit
 * - Strickler coefficients depending on the discharge
 * 
 * Syntax : upstream_name -> downstream_name 1 {
 *                                               min_radius = [m] 1000 
 *                                               strickler = 40.
 *                                             };	  
 */
def_reaches : LEX_REACH LEX_EQUAL brace reaches brace
{
  preachtot = $4;
  HYD_create_reaches(preachtot,Simul->pchyd);
}
;

reaches : reach reaches
{
  $$ = HYD_chain_reaches($1,$2);
}
| reach
{
  $$ =$1;
}
;

reach : read_reach_name reach_attributes
{
  $$ = new_reach();
  $$ = preach;
  LP_printf(Simul->poutputs,"The reach %s %s %d has been read\n",
	  preach->limits[ONE]->name,preach->limits[TWO]->name,preach->branch_nb);
  
}
;

read_reach_name : LEX_NAME LEX_ARROW LEX_NAME LEX_INT 
{
  preach = HYD_create_reach($1,$3,$4,Simul->pchyd,Simul->poutputs); // doit envoyer pschd car cherche singularity à reach
  free($1);
  free($3);
}
;

reach_attributes : brace sequence_reach_attributes brace LEX_SEMI_COLON 
| LEX_SEMI_COLON 
;

sequence_reach_attributes : reach_attribute sequence_reach_attributes
| reach_attribute 
;

reach_attribute : strickler
|params_musk
;

/*strickler : LEX_STRICKLER LEX_EQUAL mesure 
{
  preach->strickler = $3;
}
;*/

//LV 05/06/2012 : pour que ce soit décrit comme dans ProSe
strickler : intro_strickler f_ts brace
{
  preach->strickler = $2;
  //preach->strickler->prev = NULL;//LV 05/07/2012
}
;

intro_strickler : LEX_STRICKLER LEX_EQUAL brace
{
  nbsteps = 0;//Il n'y a pas d'unités à lire...
}
;


params_musk : param_musk params_musk
|param_musk;

param_musk : k_musk
{
  preach->pmusk->calc_type=CST_MUSK;
}
| var_musk
{
  preach->pmusk->calc_type=VAR_MUSK;
}
;

k_musk :  LEX_MUSK_PAR LEX_EQUAL flottant
{
  if($1==K)
    preach->pmusk->param[$1]=$3;
}
;

var_musk : LEX_CALC_MUSK_PAR LEX_EQUAL flottant 
{
  preach->pmusk->param[$1]=$3;
}
;

/* Bathymetry
 * An element is created for each defined cross_section.
 * Cross_Sections' characteristics :
 * - name
 * - reach (upstream limit and branch nb)
 * - type of description : Absc Z or X Y Z
 *   - X Y Z description (X Y in Lambert coordinates)
 *     calculation of the transversal cross_section with the values of X and Y
 *     the first point is taken as the referance
 *     (function transversal_abscisse()).
 *   - ABSC Z description 
 *     ABSC = curvilinear distance from a reference point
 * - Length of the cross_section. A length of zero is only used for the 
 *   interpolation of the sections that are not used as calculation 
 *   elements (usually at the end of a reach)
 *  
 * Synthaxe : cross_section_name <- limit_name i {
 *                         dx = 700 [m]
 *                         type  = ABSC	 Z
 *  		                   0.0	 20.97
 *  		                   10.7	 15.64
 *  		                   60.8	 15.64
 *  		                   71.5	 20.97
 *                         } ;
 */
bathymetry : LEX_PROFILES LEX_EQUAL brace cross_sections brace
{
  $$ = $4;
}
| LEX_PROFILES LEX_EQUAL brace cross_sections shape brace
{
  $$ = $4;
}
| LEX_TRAJECTORY LEX_EQUAL brace trajectory brace
{
  FILE *fp;
  fp=Simul->poutputs;
  $$ = $4;
  if (Simul->pchyd->counter->nreaches > 1) {
    LP_error(fp,"There cannot be more than one reach if a trajectory shape is given\n");
  }
  if (Simul->pchyd->p_reach[0]->nfaces > 1) {
    HYD_calculate_faces_traj($4,Simul->pchyd);
  }
  else {
    LP_error(fp,"The trajectory shape must be defined by more than one point\n");
  }
}
;

cross_sections : cross_section cross_sections
{
  $$ = HYD_chain_faces($1,$2);
}
| cross_section
{
  $$ = $1;
}
;

cross_section : read_geometry_name brace cross_section_options brace  LEX_SEMI_COLON
{
  $$ = new_face();
  $$ = pface;
  pface = NULL;
}
;

read_geometry_name : LEX_NAME LEX_REVERSE_ARROW LEX_NAME LEX_INT 
{
  preach_bat = HYD_find_reach($3,$4,Simul->pchyd);
  preach_bat->nfaces++;
  free($3);
  pface = HYD_create_face(X_HYD,Simul->pchyd);
  pface->name = $1;
  pface->def = RAW_SECTION;
  pface->reach = preach_bat;
  pface->geometry = HYD_create_geometry();
  LP_printf(Simul->poutputs,"The cross-section %s in reach %s %s %d has been read\n",
	  $1,preach_bat->limits[ONE]->name,preach_bat->limits[TWO]->name,preach_bat->branch_nb);
  
}
; 

cross_section_options : cross_section_option cross_section_options
| cross_section_option 
;

cross_section_option : LEX_GENERALP LEX_EQUAL mesure 
{  
  if ($1 == DX) {
    pface->geometry->dx = $3;
    preach_bat->length += $3;
  }

  else if ($1 == CURVATURE) {
    pface->description->curvature = $3;
  }
}
| intro_sectionAbscZ sectionAbscZs 
{
  pptAbscZ = $2;
  HYD_create_ptsAbscZ(pface,pptAbscZ);
  // !!!!! il faut libérer pptAbscZ car recrée tableau dans HYD_create_ptsAbscZ(pface,pptAbscZ); --> fuite memoire !!!
}
| intro_sectionXYZ sectionXYZs 
{
  pptXYZ = $2;
  HYD_create_ptsXYZ(pface,pptXYZ);
  HYD_calculate_AbscZ_pts(pface->geometry);
// !!!!! J'imagine idem plus haut il faut libérer pptAbscZ car recrée tableau dans HYD_create_ptsAbscZ(pface,pptAbscZ); --> fuite memoire !!!
}
;

intro_sectionAbscZ : LEX_TYPE LEX_EQUAL LEX_ABSC LEX_HYD_VAR
{
  pface->geometry->type = ABSC_Z;
}
;

intro_sectionXYZ :LEX_TYPE LEX_EQUAL LEX_X LEX_Y LEX_HYD_VAR
{
  pface->geometry->type = X_Y_Z;
}
;

sectionAbscZs : sectionAbscZ sectionAbscZs 
{
  $$ = HYD_chain_ptsAbscZ($1,$2);
}
| sectionAbscZ 
{
  $$ = $1; 
} 
;
		

sectionXYZs : sectionXYZ sectionXYZs 
{
  $$ = HYD_chain_ptsXYZ($1,$2);
}
| sectionXYZ
{
  $$ = $1; 
} 
;
		
sectionAbscZ : flottant flottant 
{ 
   $$ = HYD_create_ptAbscZ($1,$2);
   pface->geometry->npts++;
} 
;

sectionXYZ : flottant flottant flottant 
{ 
  $$ = HYD_create_ptXYZ($1,$2,$3);
  pface->geometry->npts++;
} 
;

shape : LEX_SHAPE LEX_EQUAL LEX_CROSS_SECTION_TYPE
{
  Simul->pchyd->settings->cs_shape = $3;
}
;

trajectory : traj_units traj_points
{
  $$ = $2;
}
;

traj_units : a_unit a_unit a_unit a_unit a_unit
{
  unit_x = $1;
  unit_y = $2;
  unit_z = $3;
  unit_l = $4;
  unit_kappa = $5;
}
;

traj_points : traj_point traj_points
{
  $$ = HYD_chain_faces($1,$2);
}
| traj_point
{
  $$ = $1;
}
;

traj_point : flottant flottant flottant flottant flottant
{
  preach_bat = Simul->pchyd->p_reach[0];
  preach_bat->nfaces++;

  pface = HYD_create_face(X_HYD,Simul->pchyd);
  pface->def = RAW_SECTION;
  pface->reach = preach_bat;
  pface->geometry = HYD_create_geometry();
  pface->description->xc = $1 * unit_x;
  pface->description->yc = $2 * unit_y;
  pface->description->Zbottom = $3 * unit_z;
  pface->description->l = $4 * unit_l;
  pface->description->curvature = $5 * unit_kappa;
  $$ = pface;
}
;


/* Inflows' characteristics : 
 * - TYPE : point or diffuse
 * - name
 * - upstream reach limit
 * - number of the river branche i
 * - distance x from upstream limit
 * - length dx on which the inflow occurs (diffuse inflow)
 * - transversal localization y (0=left bank to 1=right bank)
 * - discharge
 *
 * Syntax : name_inf1 <- name_singularity1 i1 {
 *                           type = UPSTREAM_INFLOW
 *                           q = { [d] [m^3/s] 0. 60.0 }
 *                   };
 *          name_inf2 <- name_singularity2 i2 {
 *                           type = PT_INFLOW
 *                           x = [km] 10.
 *                           q = { [d] [m^3/s] 0. 60.0 }
 *                   };                    
 *          name_inf3 <- name_singularity3 i3 {
 *                           type = DIFFUSE_INFLOW
 *                           x = [km] 10.
 *                           dx = [km] 30.
 *                           q = { [d] [m^3/s] 0. 60.0 }
 *                   };                    
 */
def_inflows : LEX_INFLOWS LEX_EQUAL brace  inflows brace
{
  HYD_create_inflows($4,Simul->pchyd,Simul->poutputs);
};

inflows : inflow inflows
{
  $$ = HYD_chain_inflows($1,$2);
}
| inflow
{
  $$ = $1;
}
;

inflow : read_name_inflow brace options_inflow brace LEX_SEMI_COLON
{
  $$ = new_inflow();
  $$ = pinflow;
  pinflow = NULL;
}
;

read_name_inflow : LEX_NAME LEX_COLON LEX_NAME LEX_INT
{
  preach_infl = HYD_find_reach($3,$4,Simul->pchyd);
  preach_infl->ninflows++;
  free($3);
  pinflow = HYD_create_inflow($1,Simul->pchyd);
  pinflow->reach = preach_infl;
  
  LP_printf(Simul->poutputs,"The inflow %s in reach %s %s %d has been read\n",
	  $1,preach_infl->limits[ONE]->name,preach_infl->limits[TWO]->name,preach_infl->branch_nb);
  
 free($1);
} 
;

options_inflow : option_inflow options_inflow
| option_inflow 
;

option_inflow : LEX_TYPE LEX_EQUAL LEX_INFLOW_TYPE 
{
  pinflow->type = $3;
  if (pinflow->type == DIFFUSE_INFLOW)
    pinflow->diff_inflow = new_diffuse_inflow();
  else {
    pinflow->pt_inflow = new_pt_inflow();
    pinflow->pt_inflow->name = pinflow->name;
  }
}
| LEX_X LEX_EQUAL mesure 
{
  pinflow->x = $3;
}
| LEX_GENERALP LEX_EQUAL mesure
{
  pinflow->diff_inflow->dx = $3;
}
| LEX_Y LEX_EQUAL flottant 
{
  pinflow->transversal_position = $3;
}
| LEX_HYD_VAR LEX_EQUAL brace read_units f_ts brace 
{
  pinflow->discharge = $5;
  if (pinflow->type != DIFFUSE_INFLOW)
    pinflow->pt_inflow->discharge = $5;
}
;


/* Definition of the outputs */
outputs : intro_outputs def_outputs LEX_CLOSING_BRACE
{
  char cmd[MAXCHAR];
  
  if (pts != NULL) {
    HYD_create_time_series(pts,Simul->outputs,Simul->pchyd->counter->nts);
    sprintf(cmd,"mkdir %s/time_series",getenv("RESULT")); 
    system(cmd);
  }
  if (plp != NULL) {
    HYD_create_long_profiles(plp,Simul->outputs,Simul->pchyd->counter->nlp);
    sprintf(cmd,"mkdir %s/longitudinal_profiles",getenv("RESULT")); 
    system(cmd);
  }
  if (pmb != NULL) {
    HYD_create_mass_balances(pmb,Simul->outputs,Simul->pchyd->counter->nmb);
    sprintf(cmd,"mkdir %s/mass_balances",getenv("RESULT")); 
    system(cmd);
  }
  LP_printf(Simul->poutputs,"End of outputs formats reading\n"); 
}
;

intro_outputs : LEX_OUTPUTS LEX_EQUAL LEX_OPENING_BRACE
{LP_printf(Simul->poutputs,"Begin outputs formats reading\n"); }
;

def_outputs : def_output def_outputs
| def_output
;

def_output : intro_output output_options brace
{
  if (output_type == TRANSV_PROFILE) {
    //Simul->outputs[TRANSV_PROFILE] = pout;
    if (pts == NULL)
      pts = pout_hyd;
    else
      pts = HYD_chain_outputs(pout_hyd,pts);
    HYD_create_output_points(pts,pts_pktot);
    Simul->pchyd->counter->nts++;
  }

  else if (output_type == LONG_PROFILE) {
    //Simul->outputs[LONG_PROFILE] = pout;
    if (plp == NULL)
      plp = pout_hyd;
    else
      plp = HYD_chain_outputs(pout_hyd,plp);
    HYD_create_output_extents(plp,plp_pktot);
    Simul->pchyd->counter->nlp++;
  }

  else if (output_type == MASS_BALANCE) {
    if (pmb == NULL)
      pmb = pout_hyd;
    else
      pmb = HYD_chain_outputs(pout_hyd,pmb);
    HYD_create_output_extents(pmb,pmb_pktot);
    Simul->pchyd->counter->nmb++;
  }

  else Simul->outputs[output_type][0] = pout_hyd;

  plp_pktot = NULL;
  pmb_pktot = NULL;
  pts_pktot = NULL;
}
;

intro_output : LEX_OUTPUT_TYPE LEX_EQUAL brace
{
  Simul->pinout->calc[$1] = YES;
  pout_hyd = HYD_initialize_output(Simul->chronos);
  output_type = $1;
}
;

output_options : output_option output_options
| output_option
;

output_option : LEX_ANSWER
{
  Simul->pinout->calc[output_type] = $1;
}
| LEX_FILE_NAME LEX_EQUAL LEX_NAME
{
  /*Simul->outputs[output_type] = new_output();
  Simul->outputs[output_type]->fic = new_file();
  Simul->outputs[output_type]->fic->name = $3;*/
  pout_hyd->fic = new_file_io();
  pout_hyd->fic->name = $3;
}
| time_unit
| out_chronos
| variables
| points
{
  if (pts_pktot != NULL)
    pts_pktot = HYD_chain_ts_pk(HYD_browse_ts_pk(pts_pktot,END),$1);
  else 
    pts_pktot = $1;
}
| extents
{
  if (output_type == LONG_PROFILE) {
    if (plp_pktot != NULL)
      plp_pktot = HYD_chain_lp_pk(HYD_browse_lp_pk(plp_pktot,END),$1);
    else
      plp_pktot = $1;
  }

  else if (output_type == MASS_BALANCE) {
    if (pmb_pktot != NULL)
      pmb_pktot = HYD_chain_lp_pk(HYD_browse_lp_pk(pmb_pktot,END),$1);
    else
      pmb_pktot = $1;
  }
}
| graphics
;

time_unit : LEX_TUNIT LEX_EQUAL a_unit
{
  pout_hyd->pout->time_unit = 1 / $3;
}
;

out_chronos : LEX_CHRONOS LEX_EQUAL brace out_times brace
{
  pout_hyd->pout->t_out[INI_IO] = Simul->chronos->t[BEGINNING];
}
;

out_times : out_time out_times
| out_time
;

out_time : LEX_TIME LEX_EQUAL mesure
{
  pout_hyd->pout->t_out[$1] = $3;
}
| LEX_TIMESTEP LEX_EQUAL mesure
{
  pout_hyd->pout->deltat = $3 < Simul->chronos->dt ? Simul->chronos->dt : $3;
}
;

variables : LEX_VAR LEX_EQUAL brace var_list brace
;

var_list : one_var var_list
| one_var
;

one_var : LEX_ONE_VAR a_unit
{
  pout_hyd->pout->hydvar[$1] = YES;
  pout_hyd->pout->hydvar_unit[$1] = $2;
}
;

points : LEX_POINTS LEX_EQUAL brace river pk_list brace
{
  $$ = $5;
}
;

river : LEX_NAME
{
  river = $1;
  LP_lowercase(river);
}
;

pk_list : one_pk pk_list
{
  $$ = HYD_chain_ts_pk($1,$2);
}
| one_pk
{
  $$ = $1;
}
;

one_pk : mesure LEX_INT
{
  pts_pk = new_ts_pk();
  pts_pk->river = river;
  pts_pk->pk = $1;
  pts_pk->branch_nb = $2;
  //find_element_reach(ts_pk);
  $$ = pts_pk;
  pout_hyd->npk++;
}
;

extents : LEX_EXTENT LEX_EQUAL brace river two_pk brace
{
  if (output_type == LONG_PROFILE) {
    $$ = plp_pk;
  }

  else if (output_type == MASS_BALANCE) {
    $$ = pmb_pk;
  }
}
;

two_pk : mesure LEX_INT mesure LEX_INT
{
  if (output_type == LONG_PROFILE) {
    plp_pk = HYD_init_lp_pk(river,$1,$2,$3,$4,Simul->pchyd);
  }

  else if (output_type == MASS_BALANCE) {
    pmb_pk = HYD_init_lp_pk(river,$1,$2,$3,$4,Simul->pchyd);
  }

  pout_hyd->npk++;
}
;

graphics : LEX_GRAPHICS LEX_EQUAL LEX_ANSWER
{
  pout_hyd->graphics = NO;
}
| LEX_GRAPHICS LEX_EQUAL LEX_GRAPH_TYPE
{
  pout_hyd->graphics = $3;

  if ($3 == GNUPLOT) {
    char cmd[MAXCHAR];
    sprintf(cmd,"mkdir %s/gnuplot",getenv("RESULT")); 
    system(cmd);
  }
}
;

/* This part describes how to deal with numbers and units*/
read_units : a_unit a_unit 
{
  unit_t = $1;
  unit_f = $2;
  nbsteps = 0;
}
;

a_unit : LEX_OPENING_BRACKET units LEX_CLOSING_BRACKET {$$ = $2;}
;


/* This part allows a_unit conversion in the programm units (SI), understanding power, division... */
units : one_unit units 
{
  $$ = $1 * $2;
}
| one_unit 
{
  $$ = $1;
}
;

one_unit : a_unit_value 
{
  $$ = $1;
}
| LEX_INV a_unit_value 
{
  $$ = 1.0/$2;
}
; /* inversion */


a_unit_value : all_units 
{
  $$ = $1;
} /* simple a_unit */
| all_units LEX_POW flottant 
{
  $$ = pow($1,$3);
}
; /* power a_unit */

all_units : LEX_A_UNIT  
{
  $$ = $1;
}
| LEX_A_UNIT LEX_VAR_UNIT 
{
  $$ = $1*$2;
}
;

mesure : a_unit flottant 
{
  $$ = $1 * $2;
} 
| flottant a_unit//LV 06/06/2012 pour ne pas a avoir à modifier tous les fichiers de profils ProSe !
{
  $$ = $1 * $2;
}
|flottant 
{
  $$ = $1;
}
;

flottant : LEX_DOUBLE
{
  $$ = $1;
}
| LEX_INT
{
  $$ = (double)$1;
}
;


/* Examples of chaining function of t s_ft */
f_ts : f_t f_ts 
{
  $$ = TS_chain_bck_ts($1,$2);
}
|  f_t 
{ 
  $$ = $1;
};

f_t : flottant flottant 
{
  nbsteps++;
  told = tnew;
  tnew = $1 * unit_t;
  if ((nbsteps >= 2) && (tnew < told)) 
  if (tnew < told) 
    LP_warning(Simul->poutputs,"HyCu %4.2f -> Inversion of the time steps, line %d\n",NVERSION_HYCU,nbsteps);
  $$ = TS_create_function($1*unit_t,$2*unit_f);
}
;


brace : LEX_OPENING_BRACE
| LEX_CLOSING_BRACE
;
%%


/* Procedure used to display errors
 * automatically called in the case of synthax errors
 * Specifies the file name and the line where the error was found
 */
	
#if defined GCC482 || defined GCC472 ||  defined GCC471
void yyerror(char const *s)
#else 
void yyerror(char *s)
#endif
{
  if (pile >= 0) {
    //LP_printf(Simul->poutputs,"File %s, line %d : %s\n",current_read_files[pile].name,line_nb+1,s);
    //exit -1;
    LP_error(Simul->poutputs,"File %s, line %d : %s\n",current_read_files[pile].name,line_nb+1,s);
  }
}


void lecture(char *name,FILE *fp) 
{
  pile = 0;
  current_read_files[pile].name = strdup(name);
  if ((yyin = fopen(name,"r")) == NULL)
    LP_error(fp,"File %s doesn't exist\n",name);
  current_read_files[pile].address = yyin;
  current_read_files[pile].line_nb = line_nb;

   LP_printf(fp,"\n****Reading input data****\n");
  yyparse();
  LP_printf(fp,"\n****Input data read****\n");
}

