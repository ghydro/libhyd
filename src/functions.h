/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: functions.h
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/********************************************************/
/*               Functions used in HYDRO                */
/********************************************************/

/* Functions of input.y */
void lecture(char *);
int yylex();
#if defined GCC482 || defined GCC472 || defined GCC471
void yyerror(char const *);
#else
void yyerror(char *);
#endif /*test on GCC*/

/* Useful function to debug by putting a breakpoint in gdb
 * or printing a number
 */
void test(int);

void testerreur(char *, int, ...);
void begin_timer();
double end_timer();
void init_timer();

/* Usual functions used in particular to print in debug file */
void *free_p(void *);

/* Functions returning the name or the unit of an integer parameter */
char *name_inflow(int);
char *name_output(int);
char *name_calc_state(int);
char *name_general_param(int);
char *name_month(int);
char *name_hyd_var(int);
char *name_extremum(int);
char *name_answer(int);
char *name_direction(int);
char *name_sing_type(int);
char *unit_general_param(int);
char *name_calc(int);
char *HYD_name_scheme(int);

/* Functions of manage_*.c used to allocate/create the adequate structures */
s_simul *init_simulation(void);

s_settings *init_settings(void);

void create_singularities(s_singularity *);
s_singularity *create_singularity(char *);
s_singularity *chain_singularities(s_singularity *, s_singularity *);
s_singularity *find_singularity(char *);
s_singularity *rewind_sing(s_singularity *);
s_BC_char *create_hydwork(int);
s_BC_char *chain_hydworks(s_BC_char *, s_BC_char *);
void verif_singularities();
void BC_faces();

void create_reaches(s_reach *);
s_reach *create_reach(char *, char *, int);
s_reach *chain_reaches(s_reach *, s_reach *);
s_reach *find_reach(char *, int);
s_reach *rewind_reach(s_reach *);
void assign_river();

void create_inflows(s_inflow *);
s_inflow *create_inflow(char *);
s_inflow *chain_inflows(s_inflow *, s_inflow *);
s_inflow *rewind_inflows(s_inflow *);

s_hydro *create_hydro();
void surf_peri_width(s_face *, double, int, int, int);
void calculate_hydro_table(s_face *);
void table_hydro_faces(s_face *);
void table_hydro_interpolated_face(s_face *, s_face *, s_face *);
void table_hydro_elements();
void print_function(s_ft *);
void print_hydro_table(s_hydro *);
void rewind_hydro_tables();

void add_faces();
void create_faces(s_face *);
void check_nb_faces();
s_face *create_face(int);
s_face *chain_faces(s_face *, s_face *);
s_face *rewind_faces(s_face *);

void calculate_AbscZ_pts(s_geometry *);

void create_elements();
s_element *create_element();

s_geometry *create_geometry();
void create_ptsAbscZ(s_face *, s_pointAbscZ *);
void create_ptsXYZ(s_face *, s_pointXYZ *);
s_pointAbscZ *create_ptAbscZ(double, double);
s_pointXYZ *create_ptXYZ(double, double, double);
s_pointAbscZ *chain_ptsAbscZ(s_pointAbscZ *, s_pointAbscZ *);
s_pointXYZ *chain_ptsXYZ(s_pointXYZ *, s_pointXYZ *);
s_pointAbscZ *rewind_ptAbscZ(s_pointAbscZ *, int);
s_pointXYZ *rewind_ptXYZ(s_pointXYZ *);
s_description *create_description();
void description_interpolated_face(s_face *, s_face *);
void calculate_faces_traj(s_face *);

s_output *initialize_output();
s_output *chain_outputs(s_output *, s_output *);
s_lp_pk *init_lp_pk(char *, double, int, double, int);
s_ts_pk *chain_ts_pk(s_ts_pk *, s_ts_pk *);
s_lp_pk *chain_lp_pk(s_lp_pk *, s_lp_pk *);
void create_output_points(s_output *, s_ts_pk *);
void create_output_extents(s_output *, s_lp_pk *);
s_ts_pk *browse_ts_pk(s_ts_pk *, int);
s_lp_pk *browse_lp_pk(s_lp_pk *, int);
int find_river(char *, int);
void calculate_output_domain(s_lp_pk *, int);
void calculate_ts_domain(s_ts_pk *);
void propage_output(s_reach *, int *, double, int, double, int, int *, int *);
void test_path(int, double, s_reach *, int *);
void create_time_series(s_output *);
void create_long_profiles(s_output *);
void create_mass_balances(s_output *);

/* Calculation of the radius of curvature at the elements' centers and faces */
void calculate_curvature_radius();
void calculate_curvature();
void zero_curvature();

/* Calculation of pk */
void calculate_pk_sing();
void propage_pk(s_singularity *, double, int);
// void propage_pk(s_singularity *,double,int,int);
void module_calculation_pk(s_singularity *, double, int);
// void module_calculation_pk(s_singularity *,double,int,int);
void calculate_pk_faces_centers();

/* Management of the clock and simulation date */
void calculate_calculation_length(s_clock *);
void calculate_simulation_date(s_chronos *, double);

/* Functions to calculate hydraulics */
void reallocate_openBC();

/*****************************************************************/
/*            Coupling with PETSc + hydraulic calculations       */
/*****************************************************************/

/* Creation of the hydraulic matrixes, solving */
// void create_PETSc_object();
// void destroy_PETSc_object();
void solve_Q();
// void write_values_from_matrix();
void interpolate_Q_Z(double);
void steady_state(double, s_mb *, s_mb *, FILE *);
void initialize_hydro();
// void zero_hydro();
s_mb *solve_hydro(double, double, FILE *, s_mb *);                   // NF 27/3/2012
void transient_hydraulics(double *, double, s_mb *, s_mb *, FILE *); // NF 27/3/2012
void strickler_coefficients(double);
// void calculate_gammas();
// void calculate_gammas_any_section();
// void fill_matrixes_Q(double);
// void fill_matrixes_Z(double);
void calc_Qapp(double);
// void calculate_Q(double);
// void calculate_Z(double);
s_hydro *update_hydro_table(s_hydro *, double);
s_hydro *calc_hydro_carac_rectangle(s_hydro *, double); // LV 27/09/2012
// void calculate_BC(double);
// void calculate_downstream_Z_BC(double,s_element *);
// void calculate_upstream_Q_BC(double,s_face *);
// void calculate_downstream_Q_BC(double,s_face *);
// void momentum_conservation_Q(PetscInt,s_face *,s_element *,s_element *);
// void mass_conservation_Z(PetscInt,s_element *);
// void mass_conservation_Q(PetscInt,s_element *,double);
// void equal_elevations_Q(PetscInt,s_element *,s_element *);
// void discharge_continuity_conf_diff(PetscInt,s_face *,s_face *,s_face *);
// void discharge_at_hydworks(PetscInt,int,s_face *,s_singularity *,double);
// void calc_mass_conservation_coefs(s_element *,double *);

int test_sing_face(s_face *, int);
void find_id_coefs_ele();
void find_id_coefs_faces();
void calc_coefs_ele(double);
void calc_coefs_faces(double);
void calc_coefs_ele_qup(s_element *, double);
void calc_coefs_ele_centraux(s_element *);
void calc_coefs_ele_wl(s_element *, double);
void calc_coefs_eq_Z(s_element *);
void calc_coefs_ele_weir_gate(s_element *, s_BC_char **, int);
void calc_coefs_face_qup(s_face *, double);
void calc_coefs_faces_centrales(s_face *);
// void calc_coefs_face_wl(s_face *,double);
void calc_coefs_face_mass_conservation_down(s_face *);
void calc_coefs_face_mass_conservation_up(s_face *);
void calc_coefs_eq_Q(s_face *);
void calc_coefs_eq_Q_conf_diff(s_face *);
void calc_coefs_eq_Q_confdiff(s_face *);
void calc_coefs_face_openBC(s_face *);

/* calc_hyd_gc.c */
void calc_prop_gc();
void fill_mat6_b(s_gc *);
void calc_sol_hydro();
void extract_solgc();
int HYD_define_ndl(int, FILE *);
void HYD_fill_mat5_mat4(s_gc *, int, FILE *);
void HYD_fill_mat6_musk(s_gc *);
void HYD_fill_b_musk(s_gc *);
void HYD_print_mats(s_gc *, FILE *);
void HYD_print_sol(s_gc *, FILE *);
void HYD_calc_sol_hydro(s_gc *, int, FILE *);
void HYD_extract_solgc(s_gc *, int);
void HYD_calc_sol_hydro(s_gc *, int, FILE *);

/* Initialization of the hydraulic variables in the whole river network */
void initialize_Z_from_file();
void initialize_Q_from_file();
void calculation_Z_init();
void calculate_Z_sing();
void propage_Z_init(s_singularity *, double, int, int);
void module_calculation_Z_init(s_singularity *, double, int, int);
void calculate_Q_reach();
void calculation_Q_init();

/* Functions of mass_balance.c*/
void initialize_mass_balance_t(s_mb *);
s_mb *init_mass_balance();
s_mb *chain_mb(s_mb *, s_mb *);
s_mb *set_state_for_mass_balance(s_mb *, int);
s_mb *calculate_single_mb(s_element *, int, s_mb *);
s_mb *print_mass_balance(s_mb *, double, FILE *);
FILE *create_header_mass_balance_file();
void create_mb_at_elements_file(); // LV 15/06/2012
void print_mb_at_elements(double); // LV 15/06/2012

/* Functions of Print.c*/
void print_hydraulic_state(FILE *);

/* Outputs */
FILE *open_iterations_file();
void print_final_state();
void print_pk();
void print_outputs_formats();
void long_profile_format();
void mass_balance_format();
void transv_profile_format();
void print_outputs(double);
void print_variables(FILE *, s_output *);
void long_profile(double, s_output *);
void mass_balance(double, s_output *);
void transv_profile(double, s_output *);
void print_average_result(s_element *, double, s_output *, FILE *);
void create_GNUPLOT_ts(int);
void create_GNUPLOT_lp(int);
void create_GNUPLOT_mb(int);
void print_GNUPLOT_lp(s_output *, s_lp_pk *, int, double);
void print_times(FILE *);

// Couplage Flumy
s_simul *init_Simul_Flumy();
void calculate_faces_Flumy(int, double **);
void init_hydro_structures_Flumy();
double h_manning(int, double, double);
void init_geom_Flumy(int, double **, double, double);
void fill_outputs_Flumy(double **);

// hydraulics MUSKINGUM
void HYD_find_id_coefs_muskingum();
void HYD_calc_LHS_Musk();
void HYD_calc_LHS_coef_Musk(s_face *, double, double);
void HYD_calc_RHS_Musk(double);
void HYD_calc_RHS_coef_upstream_Musk(s_face *, s_face *, double, double, double, double, FILE *);
void HYD_calc_RHS_coef_common_Musk(s_face *, double, double, double);
void HYD_calc_RHS_coef_central_Musk(s_face *, double);
void HYD_initialize_GC(int, FILE *);
void HYD_transient_hydraulics(double *, double, s_mb *, s_mb *, int, FILE *);
s_mb *HYD_solve_hydro(double, double, FILE *, s_mb *, int);

/*manage_musk_set*/
s_musk_set *HYD_create_musk_set();
double HYD_calc_k(s_face *);
