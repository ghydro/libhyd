/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: hydraulics.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include "spmatrix.h"
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"
#ifdef OMP
#include "omp.h"
#endif
/* Structure containing the simulation characteristics */
// extern s_simul *Simul;

// Pour memoire lorsque l'on calculera sur fond sec : rajouter fonction assess_infilt de HySeGeoS

/* Calculation of the convergence error on Q and Z at each time step */
void HYD_calc_cv_criteria(double *err1, double *err2, int *nume, int *numf, s_chyd *pchyd) {
  /* Reach, face, element nb */
  int r, f, e;
  /* Convergence error on Q and Z */
  double dq, dz;
  /* Current face or element */
  s_face_hyd *pface;
  s_element_hyd *pele;

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    /* Loop on the faces and elements of the reach */
    for (f = 0; f < pchyd->p_reach[r]->nfaces; f++) {

      pface = pchyd->p_reach[r]->p_faces[f];
      /* Error on Q */
      dq = pface->hydro->dq;
      *err1 = *err1 > fabs(pface->hydro->dq) ? *err1 : fabs(pface->hydro->dq);
      if (*err1 == fabs(dq))
        *numf = pface->id[ABS_HYD];
    }

    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {

      pele = pchyd->p_reach[r]->p_ele[e];
      /* Error on Z */
      dz = pele->center->hydro->dz;
      *err2 = *err2 > fabs(pele->center->hydro->dz) ? *err2 : fabs(pele->center->hydro->dz);
      if (*err2 == fabs(dz))
        *nume = pele->id[ABS_HYD];
    }
  }
}

/* Steady-state calculation
 * Iterative transient calculation (transient()) until convergence
 */
void HYD_steady_state(double dt, s_mb_hyd *pmb, s_mb_hyd *pmb2, FILE *fpmb, s_inout_set_io *pinout, s_output_hyd ***p_outputs, s_chyd *pchyd, s_clock_CHR *clock, s_chronos_CHR *chronos, FILE *pout) {
  /* Nb of the iteration, id of the element */
  int n, numf, nume, f;
  /* Calculation errors */
  double err1, err2;
  /* Iteration file */
  FILE *f_iter;
  /* Convergence criteria defined by the user */
  double eps_q, eps_z;
  s_face_hyd *pface_hyd;

  eps_q = pchyd->settings->general_param[EPS_Q];
  eps_z = pchyd->settings->general_param[EPS_Z];

  /* Opening of the iteration file */
  if (pinout->calc[ITERATIONS] == YES)
    f_iter = HYD_open_iterations_file(p_outputs, pinout, pout);

  /* Initialization of the hydraulic characteristics in the network */
  // begin_timer();//LV 3/09/2012
  // initialize_hydro();
  // assign_river();//LV nov2014
  // Simul->clock->time_spent[INITIALISATION] += end_timer();//LV 3/09/2012

  // calculate_Qapp(Simul->chronos->t[BEGINNING]);
  HYD_calc_Qapp(chronos->t[BEGINNING], pchyd, pout); // LV nov2014

  CHR_begin_timer(); // LV 3/09/2012
  if (pinout->calc[ITERATIONS] == YES)
    fprintf(f_iter, "0\n"), HYD_print_hydraulic_state(f_iter, pchyd);
  clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012

  n = 0;
  err1 = err2 = 1.;

  /* Loop until convergence isn't reached */
  while (((err1 > eps_q) || (err2 > eps_z)) && (n < MAX_CONV)) {

    if (pmb != NULL) {                 // LV 27/09/2012
      pmb = HYD_init_mass_balance();   // NF 27/3/2012
      if (pmb2 != NULL)                // LV 27/09/2012
        pmb = HYD_chain_mb(pmb, pmb2); // NF 27/3/2012
    }

    if (pmb != NULL) // LV 27/09/2012
      // pmb = HYD_solve_hydro(chronos->t[BEGINNING],fpmb,pmb,pchyd,clock,chronos,p_outputs,pinout,pout);//NF 27/3/2012
      pmb = HYD_solve_hydro(chronos->t[BEGINNING], fpmb, pmb, pchyd, clock, chronos, p_outputs, pinout, n, pout); // SW 11/07/2018
    else
      // HYD_solve_hydro(chronos->t[BEGINNING],fpmb,pmb,pchyd,clock,chronos,p_outputs,pinout,pout);//NF 27/3/2012
      HYD_solve_hydro(chronos->t[BEGINNING], fpmb, pmb, pchyd, clock, chronos, p_outputs, pinout, n, pout); // SW 11/07/2018
    // Chain t-1 and t mass balance
    if (pmb != NULL) // LV 27/09/2012
      pmb2 = pmb;
    err1 = err2 = 0.0;

    HYD_calc_cv_criteria(&err1, &err2, &nume, &numf, pchyd);

    n++;

    if (pinout->calc[ITERATIONS] == YES)
      fprintf(f_iter, "\n%d\n", n), HYD_print_hydraulic_state(f_iter, pchyd);

    // LP_printf(pout,"\n%d : error on Q = %f at face nb %d\n\n",n,err1,numf);//NF 25/3/2019 Useless
    // LP_printf(pout,"%d : error on Z = %f at element center nb %d\n\n",n,err2,nume);//NF 25/3/2019 Useless
  }

  pchyd->counter->niter = n;

  if ((err1 <= eps_q) && (err2 <= eps_z)) {

    LP_printf(pout, "Steady-state calculated with %d iterations.\n", n);
  } else {

    LP_printf(pout, "No convergence of the steady-state.\n");
    exit(2);
  }

  if (pinout->calc[ITERATIONS] == YES)
    fclose(f_iter);
}

/* Calculation of the transient state during one time step
 * Calls main subroutine transient() after initialization of the matrixes
 */
// s_mb *transient_hydraulics(double *t,
//			  double dt,s_mb *pmb,s_mb *pmb2,FILE *fpmb)
void HYD_transient_hydraulics(double *t, double dt, s_mb_hyd *pmb, s_mb_hyd *pmb2, FILE *fpmb, s_chyd *pchyd, s_clock_CHR *clock, s_chronos_CHR *chronos, s_output_hyd ***p_outputs, s_inout_set_io *pinout, FILE *fpout) {
  int n; // SW 11/07/2018
  // s_gc *pgc;
  // pgc=pchyd->calcul_hydro;
  // zero_hydro();
  *t += dt;
  if (*t > (chronos->t[BEGINNING] + dt)) // SW 11/07/2018
    n = 2;                               // SW 11/07/2018
  else                                   // SW 11/07/2018
    n = 0;                               // SW 11/07/2018
  pmb = HYD_init_mass_balance();         // LV 22/05/2012
  pmb = HYD_chain_mb(pmb, pmb2);         // LV 22/05/2012

  pmb = HYD_solve_hydro(*t, fpmb, pmb, pchyd, clock, chronos, p_outputs, pinout, n, fpout); // NF 27/3/2012
  // return pmb;//NF 27/3/2012 soit cela, soit un chainage avec pmb2
  pmb2 = pmb; // LV 22/05/2012
}

/* Transient hydraulics
 * Fills the matrix for resolution of St-Venant equations
 * and calls the solving functions
 */
s_mb_hyd *HYD_solve_hydro(double t, FILE *fpmb, s_mb_hyd *pmb, s_chyd *pchyd, s_clock_CHR *clock, s_chronos_CHR *chronos, s_output_hyd ***p_outputs, s_inout_set_io *pinout, int n, FILE *fp) {

  int schem_type;
  double dt;
  int *ts_count;

  ts_count = &n; // SW 14/06/2021 pour prose, compte le nombre de pas de temps
  dt = chronos->dt;
  schem_type = pchyd->settings->schem_type;
  if (pmb != NULL) {                                       // LV 27/09/2012
    pmb->time = t;                                         // NF 27/3/2012
    pmb = HYD_set_state_for_mass_balance(pmb, INI, pchyd); // NF 27/3/2012
  }

  // print_outputs(t);//LV test 26/07/2012

  CHR_begin_timer(); // LV 3/09/2012

  // if(t == 126299700.000000) // SW 07/11/2018 debug
  // printf("in hydraulic\n");

  /* Calculation of all the matrixes in the system */
  HYD_calc_Qapp(t, pchyd, fp);
  if (schem_type == MUSKINGUM) {
    HYD_calc_RHS_Musk(t, dt, pchyd, fp);
  } else {
    HYD_calc_coefs_ele(t, dt, pchyd, fp);
    HYD_calc_coefs_faces(t, dt, pchyd, fp);
  }

  clock->time_spent[MATRIX_FILL_CHR] += CHR_end_timer(); // LV 3/09/2012
  CHR_begin_timer();                                     // LV 3/09/2012

  /* Solving of the equations */
  if (pchyd->settings->solver_type != SP_SOL) // SW 11/07/2018
    HYD_calc_sol_hydro(pchyd, ts_count, fp);  // LV nov2014 seul appel dans hydraulics_v2.c aux fonctions de libraire libgc (cf. calc_hyd_gc.c)
  else
    HYD_calc_sol_hydro_sparse(pchyd, n, fp);              // SW 11/07/2018
  clock->time_spent[MATRIX_SOLVE_CHR] += CHR_end_timer(); // LV 3/09/2012

  CHR_begin_timer(); // LV 3/09/2012

  /* Calculation of the new hydraulic characteristics at each face and element center */
  HYD_extract_solgc(pchyd, fp);

  HYD_interpolate_Q_Z(t, pchyd, T_HYD, fp);

  clock->time_spent[MATRIX_GET_CHR] += CHR_end_timer(); // LV 3/09/2012

  if (pmb != NULL) // LV 27/09/2012
  {

    pmb = HYD_set_state_for_mass_balance(pmb, END, pchyd);
  }

  CHR_begin_timer(); // LV 3/09/2012
  if (pmb != NULL)   // LV 27/09/2012
  {

    pmb = HYD_print_mass_balance(pmb, t, fpmb, chronos, fp); // NF 5/15/06
  }
  if (pinout->calc[MB_ELE] == YES) // LV 15/06/2012
    HYD_print_mb_at_elements(t, p_outputs, pchyd, chronos, fp);

  clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012

  return pmb;
}

/* Calculation of the linear lateral discharge that enters each element */
void HYD_calc_Qapp(double t, s_chyd *pchyd, FILE *fp) {
  /* Reach, element, inflow nb */
  int r, e, i;
  /* Current element */
  s_element_hyd *pele;
  s_hydro_hyd *phyd;
  /* Total linear lateral discharge in one element */
  double qapp;

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {

      pele = pchyd->p_reach[r]->p_ele[e];
      phyd = pele->center->hydro;
      for (i = 0; i < NQAPP; i++)
        phyd->qapp[i][ITER_HYD] = phyd->qapp[i][T_HYD];

      qapp = 0.;

      for (i = 0; i < pele->face[Y_HYD][ONE]->ninflows; i++) {
        qapp += TS_function_value_t(t, pele->face[Y_HYD][ONE]->pt_inflows[i]->discharge, fp) / pele->length;
      }

      for (i = 0; i < pele->face[Y_HYD][TWO]->ninflows; i++) {
        qapp += TS_function_value_t(t, pele->face[Y_HYD][TWO]->pt_inflows[i]->discharge, fp) / pele->length;
      }

      // SW 03/05/2022 when the outflow (qapp < 0) is greater than the discharge in the cell, display a warning
      if ((qapp < 0.) && (fabs(qapp * pele->length) > pele->face[X_HYD][ONE]->hydro->Q[T_HYD])) {
        LP_warning(fp, "outflow qapp between singularity %s and singularity %s, at element id = %d pk = %f is too big: qapp = %f, Qface upstream = %f. This outflow is ignored in this time step\n", pele->reach->limits[UPSTREAM]->name, pele->reach->limits[DOWNSTREAM]->name, pele->id[ABS_HYD], pele->center->pk / 1000., qapp * pele->length, pele->face[X_HYD][ONE]->hydro->Q[T_HYD]);
        qapp = 0.;
      }

      phyd->qapp[RUNOFF_HYD][T_HYD] = qapp;
    }
  }
}

/* Allocate the memory for the size of the matrix */
void HYD_allocate_coefshydro(s_hydro_hyd *phyd, int ncoefs) {
  phyd->ncoefs = ncoefs;
  phyd->id = ((int *)malloc(phyd->ncoefs * sizeof(int)));
  phyd->coefs = ((double *)malloc(phyd->ncoefs * sizeof(double)));
}

// Identification des éléments à partir desquels sont calculés Z en un élément / Q en une face
void HYD_find_id_coefs_ele(s_chyd *pchyd) {
  int r, e;
  s_element_hyd *pele;
  s_hydro_hyd *phyd;

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    pele = pchyd->p_reach[r]->p_ele[0];
    phyd = pele->center->hydro;

    switch (HYD_test_sing_face(pele->face[X_HYD][ONE], UPSTREAM)) {
    case DISCHARGE:
    case CONTINU:
    case CONFLUENCE:
    case WATER_LEVEL:
    case HYDWORK: {
      HYD_allocate_coefshydro(phyd, 3);
      phyd->id[0] = pchyd->counter->nele_tot + pele->face[X_HYD][ONE]->id[ABS_HYD]; // test
      phyd->id[1] = pchyd->counter->nele_tot + pele->face[X_HYD][TWO]->id[ABS_HYD]; // test
      phyd->id[2] = pele->id[ABS_HYD];                                              // Le dernier terme est celui de la diagonale
      break;
    }
    case DIFFLUENCE:
    case CONF_DIFF: {
      HYD_allocate_coefshydro(phyd, 2);
      phyd->id[0] = pele->face[X_HYD][ONE]->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->id[ABS_HYD]; // meme
      phyd->id[1] = pele->id[ABS_HYD];
      break;
    } //
    default: {
      printf("Unknown sing type in function find_id_coefs_ele! \n");
      break;
    }
    }
    pchyd->counter->ncoefs_tot += phyd->ncoefs;

    for (e = 1; e < pchyd->p_reach[r]->nele - 1; e++) { // Identification des id pour les éléments au centre du bief
      // for (e = 0; e < Simul->p_reach[r]->nele - 1; e++) {//Identification des id pour les éléments au centre du bief

      pele = pchyd->p_reach[r]->p_ele[e];
      phyd = pele->center->hydro;

      HYD_allocate_coefshydro(phyd, 3);
      phyd->id[0] = pchyd->counter->nele_tot + pele->face[X_HYD][ONE]->id[ABS_HYD]; // test
      phyd->id[1] = pchyd->counter->nele_tot + pele->face[X_HYD][TWO]->id[ABS_HYD]; // test
      phyd->id[2] = pele->id[ABS_HYD];                                              // Le dernier terme est celui de la diagonale

      pchyd->counter->ncoefs_tot += phyd->ncoefs;
    }

    // Calcul des coefs pour élément aval du bief
    pele = pchyd->p_reach[r]->p_ele[e];
    phyd = pele->center->hydro;

    switch (HYD_test_sing_face(pele->face[X_HYD][TWO], DOWNSTREAM)) {
    case CONTINU:
    case CONFLUENCE: {
      HYD_allocate_coefshydro(phyd, 2);
      phyd->id[0] = pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->id[ABS_HYD];
      phyd->id[1] = pele->id[ABS_HYD];
      break;
    } //
    case WATER_LEVEL: {
      HYD_allocate_coefshydro(phyd, 1);
      phyd->id[0] = pele->id[ABS_HYD];
      break;
    }
    case HYDWORK: {
      if (pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->BC_char[0]->fion_type == ZT) {
        HYD_allocate_coefshydro(phyd, 1);
        phyd->id[0] = pele->id[ABS_HYD];
      } else {
        HYD_allocate_coefshydro(phyd, 3);
        phyd->id[0] = pchyd->counter->nele_tot + pele->face[X_HYD][TWO]->id[ABS_HYD];                       // face aval (confondue avec pele)
        phyd->id[1] = pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->id[ABS_HYD]; //
        phyd->id[2] = pele->id[ABS_HYD];                                                                    // Le dernier terme est celui de la diagonale
      }
      break;
    }
    case DIFFLUENCE:
    case OPEN: {
      HYD_allocate_coefshydro(phyd, 3);
      phyd->id[0] = pchyd->counter->nele_tot + pele->face[X_HYD][ONE]->id[ABS_HYD]; // test
      phyd->id[1] = pchyd->counter->nele_tot + pele->face[X_HYD][TWO]->id[ABS_HYD]; // test
      phyd->id[2] = pele->id[ABS_HYD];                                              // Le dernier terme est celui de la diagonale
      break;
    }
    case CONF_DIFF: {
      if (pele->reach->id[ABS_HYD] == pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->p_reach[UPSTREAM][0]->id[ABS_HYD]) { // 1er bief amont
        HYD_allocate_coefshydro(phyd, 3);
        phyd->id[0] = pchyd->counter->nele_tot + pele->face[X_HYD][ONE]->id[ABS_HYD];
        phyd->id[1] = pchyd->counter->nele_tot + pele->face[X_HYD][TWO]->id[ABS_HYD];
        phyd->id[2] = pele->id[ABS_HYD];
      } else { // 2eme bief amont
        HYD_allocate_coefshydro(phyd, 2);
        phyd->id[0] = pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO]->id[ABS_HYD];
        phyd->id[1] = pele->id[ABS_HYD];
      }
      break;
    } //
    default: {
      printf("Unknown sing type in function find_id_coefs_ele! \n");
      break;
    }
    }

    pchyd->counter->ncoefs_tot += phyd->ncoefs;
  }
}

void HYD_find_id_coefs_face(s_chyd *pchyd) {
  int r, f;
  s_face_hyd *pface;
  s_hydro_hyd *phyd;

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    pface = pchyd->p_reach[r]->p_faces[0];
    phyd = pface->hydro;

    switch (HYD_test_sing_face(pface, UPSTREAM)) {
    case DISCHARGE: {
      HYD_allocate_coefshydro(phyd, 1);
      phyd->id[0] = pchyd->counter->nele_tot + pface->id[ABS_HYD];
      break;
    } // amont
    case CONTINU:
    case WATER_LEVEL:
    case HYDWORK: {
      HYD_allocate_coefshydro(phyd, 2);
      phyd->id[0] = pchyd->counter->nele_tot + pface->limits[UPSTREAM]->faces[ONE][0]->id[ABS_HYD];
      phyd->id[1] = pchyd->counter->nele_tot + pface->id[ABS_HYD];
      break;
    }
    case CONFLUENCE: {
      HYD_allocate_coefshydro(phyd, 3);
      phyd->id[0] = pchyd->counter->nele_tot + pface->limits[UPSTREAM]->faces[ONE][0]->id[ABS_HYD];
      phyd->id[1] = pchyd->counter->nele_tot + pface->limits[UPSTREAM]->faces[ONE][1]->id[ABS_HYD];
      phyd->id[2] = pchyd->counter->nele_tot + pface->id[ABS_HYD];
      break;
    } //
    case DIFFLUENCE:
    case CONF_DIFF: {
      HYD_allocate_coefshydro(phyd, 3);
      phyd->id[0] = pface->element[TWO]->id[ABS_HYD];
      phyd->id[1] = pchyd->counter->nele_tot + pface->element[TWO]->face[X_HYD][TWO]->id[ABS_HYD];
      phyd->id[2] = pchyd->counter->nele_tot + pface->id[ABS_HYD];
      break;
    }
    default: {
      printf("Unknown sing type in function find_id_coefs_face! \n");
      break;
    }
    }

    pchyd->counter->ncoefs_tot += phyd->ncoefs;

    for (f = 1; f < pchyd->p_reach[r]->nfaces - 1; f++) { // Identification des id pour les faces au centre du bief

      pface = pchyd->p_reach[r]->p_faces[f];
      phyd = pface->hydro;

      HYD_allocate_coefshydro(phyd, 3);
      phyd->id[0] = pface->element[ONE]->id[ABS_HYD];
      phyd->id[1] = pface->element[TWO]->id[ABS_HYD];
      phyd->id[2] = pchyd->counter->nele_tot + pface->id[ABS_HYD];

      pchyd->counter->ncoefs_tot += phyd->ncoefs;
    }

    // Calcul des coefs pour face aval du bief
    pface = pchyd->p_reach[r]->p_faces[f];
    phyd = pface->hydro;

    switch (HYD_test_sing_face(pface, DOWNSTREAM)) {
    case CONTINU:
    case WATER_LEVEL:
    case CONFLUENCE:
    case HYDWORK: {
      HYD_allocate_coefshydro(phyd, 3);
      phyd->id[0] = pface->element[ONE]->id[ABS_HYD];
      phyd->id[1] = pchyd->counter->nele_tot + pface->element[ONE]->face[X_HYD][ONE]->id[ABS_HYD];
      phyd->id[2] = pchyd->counter->nele_tot + pface->id[ABS_HYD];
      break;
    }
    case DIFFLUENCE: {
      HYD_allocate_coefshydro(phyd, 3);
      phyd->id[0] = pchyd->counter->nele_tot + pface->limits[DOWNSTREAM]->faces[TWO][0]->id[ABS_HYD];
      phyd->id[1] = pchyd->counter->nele_tot + pface->limits[DOWNSTREAM]->faces[TWO][1]->id[ABS_HYD];
      phyd->id[2] = pchyd->counter->nele_tot + pface->id[ABS_HYD];
      break;
    } //
    case CONF_DIFF: {
      if (pface->id[ABS_HYD] == pface->limits[DOWNSTREAM]->faces[ONE][0]->id[ABS_HYD]) { // 1er bief amont
        HYD_allocate_coefshydro(phyd, 4);
        phyd->id[0] = pchyd->counter->nele_tot + pface->limits[DOWNSTREAM]->faces[ONE][1]->id[ABS_HYD];
        phyd->id[1] = pchyd->counter->nele_tot + pface->limits[DOWNSTREAM]->faces[TWO][0]->id[ABS_HYD];
        phyd->id[2] = pchyd->counter->nele_tot + pface->limits[DOWNSTREAM]->faces[TWO][1]->id[ABS_HYD];
        phyd->id[3] = pchyd->counter->nele_tot + pface->id[ABS_HYD];
      } else { // 2eme bief amont
        HYD_allocate_coefshydro(phyd, 3);
        phyd->id[0] = pface->element[ONE]->id[ABS_HYD];
        phyd->id[1] = pchyd->counter->nele_tot + pface->element[ONE]->face[X_HYD][ONE]->id[ABS_HYD];
        phyd->id[2] = pchyd->counter->nele_tot + pface->id[ABS_HYD];
      }
      break;
    } //
    case OPEN: {
      HYD_allocate_coefshydro(phyd, 2);
      phyd->id[0] = pchyd->counter->nele_tot + pface->element[ONE]->face[X_HYD][ONE]->id[ABS_HYD];
      phyd->id[1] = pchyd->counter->nele_tot + pface->id[ABS_HYD];
      break;
    } //
    default: {
      printf("Unknown sing type in function find_id_coefs_face! \n");
      break;
    }
    }

    pchyd->counter->ncoefs_tot += phyd->ncoefs;
  }
}

// defini la table de voisinage des faces dans un schema de muskingum.
void HYD_find_id_coefs_muskingum(s_chyd *pchyd, FILE *fp) {
  int r, e;
  s_face_hyd *pface, *pface_ups;
  s_element_hyd *pele;
  s_hydro_hyd *phyd;
  // dans le schema de muskingum on definit directement le debit de sortie de l'element on ne definit que les faces[X][TWO].
  // de ce fait la dimention du systeme à resoudre est nele_tot.
  // on ne traite donc pas la premiere face de chaques reachs. il faut ensuite definir la face "voisine" de la face traitee.
  // afin de ne pas avoir à redéfinir les id faces (in [0,nele_tot+n_reach], le referencement ce fait par rapport à l'id ele
  // attention pour la face[1] qui est la face downstream du premier element du reach doit etre liee avec la face DOWNSTREAM de la singularité
  for (r = 0; r < pchyd->counter->nreaches; r++) {

    pele = pchyd->p_reach[r]->p_ele[0];
    pface = pele->face[X_HYD][TWO];
    pface_ups = pele->face[X_HYD][ONE];
    phyd = pface->hydro;
    // LP_printf(fp," BC ele %d in reach %s is %s\n",pele->id[INTERN_HYD],pchyd->p_reach[r]->river,HYD_name_sing_type(HYD_test_sing_face(pele->face[X_HYD][ONE],UPSTREAM))); // BL to debug
    switch (HYD_test_sing_face(pele->face[X_HYD][ONE], UPSTREAM)) {
    case DISCHARGE: {
      HYD_allocate_coefshydro(phyd, 1);
      phyd->id[0] = pele->id[ABS_HYD];
      break;
    } // on va chercher l'id de l'element
    case CONTINU: {
      HYD_allocate_coefshydro(phyd, 2);
      phyd->id[0] = pface_ups->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->id[ABS_HYD];
      phyd->id[1] = pele->id[ABS_HYD];
      break;
    }

    case CONFLUENCE: {
      HYD_allocate_coefshydro(phyd, 3);
      phyd->id[0] = pface_ups->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->id[ABS_HYD];
      phyd->id[1] = pface_ups->limits[UPSTREAM]->faces[ONE][1]->element[ONE]->id[ABS_HYD];
      phyd->id[2] = pele->id[ABS_HYD];
      break;
    }                     //
    case TRICONFLUENCE: { // SW 20/11/2019 add TRICONFLUENCE
      HYD_allocate_coefshydro(phyd, 4);
      phyd->id[0] = pface_ups->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->id[ABS_HYD];
      phyd->id[1] = pface_ups->limits[UPSTREAM]->faces[ONE][1]->element[ONE]->id[ABS_HYD];
      phyd->id[2] = pface_ups->limits[UPSTREAM]->faces[ONE][2]->element[ONE]->id[ABS_HYD];
      phyd->id[3] = pele->id[ABS_HYD];
      break;
    } //
    case DIFFLUENCE: {
      HYD_allocate_coefshydro(phyd, 2);
      phyd->id[0] = pface_ups->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->id[ABS_HYD];
      phyd->id[1] = pele->id[ABS_HYD];
      break;
    }
    case TRIDIFFLUENCE: { // SW 20/11/2019 add TRIDIFFLUENCE
      HYD_allocate_coefshydro(phyd, 2);
      phyd->id[0] = pface_ups->limits[UPSTREAM]->faces[ONE][0]->element[ONE]->id[ABS_HYD];
      phyd->id[1] = pele->id[ABS_HYD];
      break;
    }
    default: {
      printf("Unknown sing type in function find_id_coefs_face! \n");
      break;
    }
    }

    pchyd->counter->ncoefs_tot += phyd->ncoefs;

    // test des faces amonts !!

    for (e = 1; e < pchyd->p_reach[r]->nele; e++) { // Identification des id pour les faces au centre du bief

      pele = pchyd->p_reach[r]->p_ele[e];
      pface = pele->face[X_HYD][TWO];
      phyd = pface->hydro;

      HYD_allocate_coefshydro(phyd, 2);
      phyd->id[0] = pele->face[X_HYD][ONE]->element[ONE]->id[ABS_HYD]; // la face voisine de la face f est la face amont de l'element à l'amont de la face traitee
      phyd->id[1] = pele->id[ABS_HYD];                                 // id de l'element diagonal cf notice libgc !!

      pchyd->counter->ncoefs_tot += phyd->ncoefs;
    }
  }
}

void HYD_calc_LHS_Musk_reach(s_reach_hyd *preach, double theta, double dt, FILE *fp) {
  int f;
  s_face_hyd *pface, *pface_upstream;
  s_hydro_hyd *phyd;
  double div;

  pface_upstream = preach->p_faces[0];
  pface = pface_upstream->element[TWO]->face[X_HYD][TWO];

  // if(HYD_test_sing_face(pface_upstream,UPSTREAM)!=DIFFLUENCE) // SW 20/11/2019 add TRIDIFFLUENCE
  if ((HYD_test_sing_face(pface_upstream, UPSTREAM) != DIFFLUENCE) && (HYD_test_sing_face(pface_upstream, UPSTREAM) != TRIDIFFLUENCE)) {
    div = 1.0;

  } else if (HYD_test_sing_face(pface_upstream, UPSTREAM) == TRIDIFFLUENCE) // SW 20/11/2019 add TRIDIFFLUENCE
  {
    div = 1. / 3.;
  } else if (HYD_test_sing_face(pface_upstream, UPSTREAM) == DIFFLUENCE) // SW 20/11/2019 add if DIFFLUENCE
  {
    div = 0.5;
  }
  HYD_calc_LHS_coef_Musk(pface, div, dt, theta, fp);
  div = 1.0;
  for (f = 2; f < preach->nfaces; f++) {

    pface = preach->p_faces[f];
    HYD_calc_LHS_coef_Musk(pface, div, dt, theta, fp);
  }
}

// calcul de la Left Hand side matrix pour le schema de muskingum
void HYD_calc_LHS_Musk(s_chyd *pchyd, s_chronos_CHR *chronos, FILE *fp) {
  int r, nreaches;
  double dt, theta;
  s_reach_hyd **p_reach;
  s_reach_hyd *preach;
#ifdef OMP
  s_smp *psmp;
  psmp = pchyd->settings->psmp;
#endif
  nreaches = pchyd->counter->nreaches;
  dt = chronos->dt;
  theta = pchyd->settings->general_param[THETA];
  p_reach = pchyd->p_reach;
#ifdef OMP
  int taille;
  int nthreads;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);

  psmp->chunk = PC_set_chunk_size_silent(fp, pchyd->counter->nreaches, nthreads);
  taille = psmp->chunk;

#pragma omp parallel shared(p_reach, theta, dt, nthreads, taille) private(r)
  {
#pragma omp for schedule(dynamic, taille)
#endif
    for (r = 0; r < pchyd->counter->nreaches; r++) {

      // preach=p_reach[r];
      HYD_calc_LHS_Musk_reach(p_reach[r], theta, dt, fp);
    }
#ifdef OMP
  } // end of parallel section
#endif
}
// calcul des termes de la Left hand side matrix
void HYD_calc_LHS_coef_Musk(s_face_hyd *pface, double div, double dt, double theta, FILE *fp) {

  double alpha, k, A_num, A_denum, A;
  int coef;
  s_hydro_hyd *phyd;

  phyd = pface->hydro;
  alpha = phyd->pmusk->param[ALPHA];
  k = phyd->pmusk->param[K];

  A_num = theta * dt - alpha * k;
  A_denum = theta * dt + (1 - alpha) * k;
  A = A_num / A_denum;
  // LP_printf(fp,"K %f Alpha %f A %f\n",k,alpha,A); //BL to debug
  for (coef = 0; coef < phyd->ncoefs - 1; coef++) {
    phyd->coefs[coef] = -A * div;
  }
  phyd->coefs[phyd->ncoefs - 1] = 1;
}

void HYD_calc_RHS_Musk_reach(s_reach_hyd *preach, double t, double dt, double theta, FILE *fp) {
  int f;
  s_face_hyd *pface, *pface_upstream;
  s_hydro_hyd *phyd;
  double alpha, k;
  double A_num, denum, B_num, C_num;
  double A, B, C;

  pface_upstream = preach->p_faces[0];
  pface = pface_upstream->element[TWO]->face[X_HYD][TWO];
  // LP_printf(fp,"in RHS Musk !!\n");
  phyd = pface->hydro;
  alpha = phyd->pmusk->param[ALPHA];
  k = phyd->pmusk->param[K];
  // LP_printf(fp,"K %f Alpha %f \n",k,alpha); //BL to debug
  A_num = theta * dt - alpha * k;
  denum = theta * dt + (1 - alpha) * k;
  A = A_num / denum;

  B_num = dt * (1 - theta) + alpha * k;
  B = B_num / denum;

  C_num = k * (1 - alpha) - (1 - theta) * dt;
  C = C_num / denum;
  /*
  if(pface->element[UPSTREAM]->id[ABS_HYD]==2138)
    {
      LP_printf(fp,"debug\n");
    }
  */
  HYD_calc_RHS_coef_upstream_Musk(pface, pface_upstream, A, B, t, dt, fp);
  HYD_calc_RHS_coef_common_Musk(pface, A, B, C, fp);

  // if(pface->hydro->b>0)
  //  LP_printf(fp,"%d %d %e  \n",pface->element[ONE]->id[ABS_HYD],pface->element[ONE]->id[GIS_HYD],pface->hydro->b); //BL to debug
  for (f = 2; f < preach->nfaces; f++) {

    pface = preach->p_faces[f];
    phyd = pface->hydro;
    alpha = phyd->pmusk->param[ALPHA];
    k = phyd->pmusk->param[K];
    // LP_printf(fp,"K %f Alpha %f \n",k,alpha); //BL to debug
    A_num = theta * dt - alpha * k;
    denum = theta * dt + (1 - alpha) * k;
    A = A_num / denum;

    B_num = dt * (1 - theta) + alpha * k;
    B = B_num / denum;

    C_num = k * (1 - alpha) - (1 - theta) * dt;
    C = C_num / denum;

    HYD_calc_RHS_coef_central_Musk(pface, B, fp);
    HYD_calc_RHS_coef_common_Musk(pface, A, B, C, fp);
    // if(pface->hydro->b>0)
    // LP_printf(fp,"%d %d %e  \n",pface->element[ONE]->id[ABS_HYD],pface->element[ONE]->id[GIS_HYD],pface->hydro->b);
  }
}

void HYD_calc_RHS_Musk(double t, double dt, s_chyd *pchyd, FILE *fp) {

  int r, nreaches;
  s_reach_hyd **p_reach;
  s_reach_hyd *preach;
  double theta;
#ifdef OMP
  s_smp *psmp;
  psmp = pchyd->settings->psmp;
#endif
  // LP_printf(fp,"ABS GIS b\n");
  nreaches = pchyd->counter->nreaches;
  theta = pchyd->settings->general_param[THETA];
  p_reach = pchyd->p_reach;
#ifdef OMP
  int taille;
  int nthreads;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);

  psmp->chunk = PC_set_chunk_size_silent(fp, pchyd->counter->nreaches, nthreads);
  taille = psmp->chunk;

#pragma omp parallel shared(p_reach, theta, nthreads, taille) private(r)
  {
#pragma omp for schedule(dynamic, taille)
#endif

    for (r = 0; r < pchyd->counter->nreaches; r++) {
      // preach=p_reach[r];
      HYD_calc_RHS_Musk_reach(p_reach[r], t, dt, theta, fp);
    }
#ifdef OMP
  } // end of parallel section
#endif
}

void HYD_calc_RHS_coef_upstream_Musk(s_face_hyd *pface, s_face_hyd *pface_upstream, double A, double B, double t, double dt, FILE *fp) {

  double q, q_old, div;

  switch (HYD_test_sing_face(pface_upstream, UPSTREAM)) {
    // cas upstream discharge recupere le debit en CL.
  case DISCHARGE: {
    q = TS_function_value_t(t, pface_upstream->pt_inflows[0]->discharge, fp);
    if (t > dt)
      q_old = TS_function_value_t(t - dt, pface_upstream->pt_inflows[0]->discharge, fp);
    else
      q_old = q;

    pface->hydro->b = A * q + B * q_old;
    // LP_printf(fp,"case DISCHARGE in calculate b : q %f q_old %f b %f face %d\n",q,q_old,pface->hydro->b,pface->id); // BL to debug
    pface_upstream->hydro->Q[T_HYD] = q;
    break;
  } // amont
    // cas diffluence meme chose que continu mis à part que par defaut 50% du debit est fourni à chacune des branches
  case DIFFLUENCE: {
    q = pface_upstream->limits[UPSTREAM]->faces[ONE][0]->hydro->Q[T_HYD];
    div = 0.5;
    pface->hydro->b = B * q * div;
    break;
  }
  case TRIDIFFLUENCE: // AR SW NG test pour 3 branches 20/11/2019
  {
    q = pface_upstream->limits[UPSTREAM]->faces[ONE][0]->hydro->Q[T_HYD];
    div = 1.0 / 3.0;
    pface->hydro->b = B * q * div;
    break;
  }
    // cas continu recupère le debit à l'aval du reach precedent.
  case CONTINU: {
    q = pface_upstream->limits[UPSTREAM]->faces[ONE][0]->hydro->Q[T_HYD];
    div = 1.0;
    pface->hydro->b = B * q * div;
    // LP_printf(fp,"case CONTINU in calculate b : river %s q %f b %e face %d\n",pface->reach->river,q,pface->hydro->b,pface->id); // BL to debug
    break;
  }
    // cas confluence recupere les debit des deux bras precedents.
  case CONFLUENCE: {
    q = pface_upstream->limits[UPSTREAM]->faces[ONE][0]->hydro->Q[T_HYD];
    pface->hydro->b = B * q;
    q = pface_upstream->limits[UPSTREAM]->faces[ONE][1]->hydro->Q[T_HYD];
    pface->hydro->b += B * q;
    break;
  } //
    // cas triconfluence recupere les debit des trois bras precedents.
  case TRICONFLUENCE: // AR SW NG test pour 3 branches 20/11/2019
  {
    q = pface_upstream->limits[UPSTREAM]->faces[ONE][0]->hydro->Q[T_HYD];
    pface->hydro->b = B * q;
    q = pface_upstream->limits[UPSTREAM]->faces[ONE][1]->hydro->Q[T_HYD];
    pface->hydro->b += B * q;
    q = pface_upstream->limits[UPSTREAM]->faces[ONE][2]->hydro->Q[T_HYD];
    pface->hydro->b += B * q;
    break;
  } //
  }
}

// calcul de la partie commune aux upstreams et aux mailles "centrales" du bief du RHS
void HYD_calc_RHS_coef_common_Musk(s_face_hyd *pface, double A, double B, double C, FILE *fp) {
  s_hydro_hyd *phyd;
  s_element_hyd *pelep;
  int i;
  double qapp = 0, qapp_old = 0;
  // double b=0;
  pelep = pface->element[ONE];
  phyd = pface->hydro;
  // phyd->b+=pelep->center->hydro->qapp*A+pelep->center->hydro->qapp_old*B+phyd->Q*C; //BL init
  for (i = 0; i < NQAPP; i++) {
    qapp_old += pelep->center->hydro->qapp[i][ITER_HYD];
    qapp += pelep->center->hydro->qapp[i][T_HYD];
  }
  phyd->b += A * pelep->length * qapp + qapp_old * pelep->length * B + phyd->Q[T_HYD] * C;
  /*
  if(pelep->id[GIS_HYD]==1046)
    LP_printf(fp,"qapp inr %f qapp_all %f length %f\n",pelep->center->hydro->qapp[INR_HYD][T_HYD]*pelep->length,pelep->length*qapp,pelep->length);
  //LP_printf(fp,"in RHS common river %s ele_length %f qapp %e qapp_old %e Q %e A %f B %f C %f b %e id face %d\n",pelep->reach->river,pelep->length,pelep->center->hydro->qapp[T_HYD],pelep->center->hydro->qapp[ITER_HYD],phyd->Q[T_HYD],A,B,C,phyd->b,pface->id[ABS_HYD]);// BL to debug
  */
}

// calcul de RHS pour les element centraux
void HYD_calc_RHS_coef_central_Musk(s_face_hyd *pface, double B, FILE *fp) {

  double q;
  s_hydro_hyd *phyd;
  phyd = pface->hydro;
  phyd->b = B * pface->element[ONE]->face[X_HYD][ONE]->hydro->Q[T_HYD];
  // LP_printf(fp,"in RHS central river %s q %f B %f  b %e id face %d\n",pface->reach->river,pface->element[ONE]->face[X_HYD][ONE]->hydro->Q[T_HYD],B,phyd->b,pface->id[ABS_HYD]);// BL to debug
}
/* Calcul des coefficients de la matrice à inverser (équation en Z)
 */
void HYD_calc_coefs_ele(double t, double dt, s_chyd *pchyd, FILE *fp) {
  int r, e, i;
  s_element_hyd *pele;
  double wl, q;

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    // Calcul des coefs pour élément amont du bief
    pele = pchyd->p_reach[r]->p_ele[0];
    // if(pele->id[ABS_HYD] == 95) // SW debug
    // printf("yes\n");
    switch (HYD_test_sing_face(pele->face[X_HYD][ONE], UPSTREAM)) {
    case DISCHARGE:
    case CONTINU:
    case CONFLUENCE:
    case WATER_LEVEL:
    case HYDWORK: {
      HYD_calc_coefs_ele_centraux(pele, dt, pchyd->settings->general_param[THETA]);
      break;
    } //
      /*case HYDWORK : {
      if (pele->face[X][ONE]->limits[UPSTREAM]->BC_char[0]->fion_type == ZT) {
      calc_coefs_ele_centraux(pele);
      }
      else {

      }
      break;}//*/
    case DIFFLUENCE:
    case CONF_DIFF: {
      HYD_calc_coefs_eq_Z(pele);
      break;
    } //
    default: {
      printf("Unknown sing type in function calc_coefs_ele! \n");
      break;
    }
    }

    for (e = 1; e < pchyd->p_reach[r]->nele - 1; e++) { // Calcul des coefs pour les éléments au centre du bief
      // for (e = 0; e < Simul->p_reach[r]->nele - 1; e++) {//Calcul des coefs pour les éléments au centre du bief

      pele = pchyd->p_reach[r]->p_ele[e];
      // if(pele->id[ABS_HYD] == 95) // SW debug
      // printf("yes\n");
      HYD_calc_coefs_ele_centraux(pele, dt, pchyd->settings->general_param[THETA]);
    }

    // Calcul des coefs pour élément aval du bief
    pele = pchyd->p_reach[r]->p_ele[e];
    // if(pele->id[ABS_HYD] == 95) // SW debug
    // printf("yes\n");
    switch (HYD_test_sing_face(pele->face[X_HYD][TWO], DOWNSTREAM)) {
    case CONTINU:
    case CONFLUENCE: {
      HYD_calc_coefs_eq_Z(pele);
      break;
    } //
    case WATER_LEVEL: {
      wl = TS_function_value_t(t, pele->face[X_HYD][TWO]->BC_char[0]->fion, fp);
      HYD_calc_coefs_ele_wl(pele, wl);
      break;
    } //
    case HYDWORK: {
      if (pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->BC_char[0]->fion_type == ZT) {
        wl = TS_function_value_t(t, pele->face[X_HYD][TWO]->BC_char[0]->fion, fp);
        HYD_calc_coefs_ele_wl(pele, wl);
      } else {
        for (i = 0; i < pele->face[X_HYD][TWO]->nworks; i++) {
          pele->face[X_HYD][TWO]->BC_char[i]->fion_t_old = pele->face[X_HYD][TWO]->BC_char[i]->fion_t;
          pele->face[X_HYD][TWO]->BC_char[i]->fion_t = TS_function_value_t(t, pele->face[X_HYD][TWO]->BC_char[i]->fion, fp);
        }
        HYD_calc_coefs_ele_weir_gate(pele, pele->face[X_HYD][TWO]->BC_char, pele->face[X_HYD][TWO]->nworks, dt, pchyd->settings->general_param[THETA]);
      }
      break;
    } //
      // case CONFLUENCE : {break;}//
    case DIFFLUENCE:
    case OPEN: {
      HYD_calc_coefs_ele_centraux(pele, dt, pchyd->settings->general_param[THETA]);
      break;
    } //
    case CONF_DIFF: {
      if (pele->reach->id[ABS_HYD] == pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->p_reach[UPSTREAM][0]->id[ABS_HYD]) { // 1er bief amont
        HYD_calc_coefs_ele_centraux(pele, dt, pchyd->settings->general_param[THETA]);
      } else { // 2eme bief amont
        HYD_calc_coefs_eq_Z(pele);
      }
      break;
    } //
    default: {
      printf("Unknown sing type in function calc_coefs_ele! \n");
      break;
    }
    }
  }
}

void HYD_calc_coefs_ele_centraux(s_element_hyd *pele, double dt, double theta) {
  s_hydro_hyd *phyd;
  s_face_hyd *pface, *pfacep, *pfacen;

  double alpha, a[NELEMENT_HYD], b[NELEMENT_HYD], c[NELEMENT_HYD], d[NELEMENT_HYD], e[NELEMENT_HYD], f[NELEMENT_HYD], g[NELEMENT_HYD];
  double cf;
  double ze[NELEMENT_HYD], qf[NELEMENT_HYD];

  int i;

  phyd = pele->center->hydro;

  alpha = dt / (pele->length * (phyd->H[T_HYD] * phyd->dL_dZ + phyd->Width));
  // if(pele->id[ABS_HYD]==506)
  // fprintf(stdout,"id = %d, alpha = %f, h = %f, dl_dz = %f, width = %f\n",pele->id[ABS_HYD],alpha,phyd->H[T_HYD],phyd->dL_dZ,phyd->Width);

  // element au milieu d'un bief
  // fprintf(stdout,"alpha = %f,length = %f,h = %f,dl_dz = %f width = %f\n",alpha,pele->length,phyd->H[T_HYD],phyd->dL_dZ,phyd->Width);
  phyd->coefs[0] = -alpha * theta;
  phyd->coefs[1] = alpha * theta;
  phyd->coefs[2] = 1.;

  phyd->b = phyd->Z[T_HYD];
  phyd->b += -alpha * (1 - theta) * (pele->face[X_HYD][TWO]->hydro->Q[T_HYD] - pele->face[X_HYD][ONE]->hydro->Q[T_HYD]);
  for (i = 0; i < NQAPP; i++)
    phyd->b += pele->length * alpha * (theta * phyd->qapp[i][T_HYD] + (1 - theta) * phyd->qapp[i][ITER_HYD]);
}

void HYD_calc_coefs_ele_qup(s_element_hyd *pele, double q, double dt, double theta) {
  s_hydro_hyd *phyd;
  s_face_hyd *pface, *pfacep, *pfacen;

  double alpha, a, b, c, d, e, f, g;
  double cf;
  // double ze, qf;

  int i;

  phyd = pele->center->hydro;

  alpha = dt / (pele->length * (phyd->H[T_HYD] * phyd->dL_dZ + phyd->Width));

  // element au milieu d'un bief
  phyd->coefs[0] = alpha * theta;
  phyd->coefs[1] = 1;

  phyd->b = alpha * theta * q;
  phyd->b += phyd->Z[T_HYD];
  phyd->b += -alpha * (1 - theta) * (pele->face[X_HYD][TWO]->hydro->Q[T_HYD] - pele->face[X_HYD][ONE]->hydro->Q[T_HYD]);
  for (i = 0; i < NQAPP; i++)
    phyd->b += pele->length * alpha * (theta * phyd->qapp[i][T_HYD] + (1 - theta) * phyd->qapp[i][ITER_HYD]);
}

void HYD_calc_coefs_ele_wl(s_element_hyd *pele, double wl) {
  pele->center->hydro->coefs[0] = 1.;
  pele->center->hydro->b = wl;
}

void HYD_calc_coefs_eq_Z(s_element_hyd *pele) {
  pele->center->hydro->coefs[0] = -1;
  pele->center->hydro->coefs[1] = 1;
  pele->center->hydro->b = 0;
}

void HYD_calc_coefs_ele_weir_gate(s_element_hyd *pele, s_BC_char_hyd **BC_char, int nworks, double dt, double theta) {
  int i;
  s_hydro_hyd *phyd, *phydn;
  s_element_hyd *pelen; // élément aval

  phyd = pele->center->hydro;

  pelen = pele->face[X_HYD][TWO]->limits[DOWNSTREAM]->faces[TWO][0]->element[TWO];
  phydn = pelen->center->hydro;
  theta = 1.;
  phyd->coefs[0] = theta;
  phyd->coefs[1] = 0.;
  phyd->coefs[2] = 0.;
  phyd->b = -(1 - theta) * pele->face[X_HYD][TWO]->hydro->Q[T_HYD];

  for (i = 0; i < nworks; i++) {

    if (BC_char[i]->fion_type == ZWT) {

      if (((phyd->Z[T_HYD] - BC_char[i]->fion_t_old) >= 1.5 * (phydn->Z[T_HYD] - BC_char[i]->fion_t_old)) && (phyd->Z[T_HYD] - BC_char[i]->fion_t_old) > EPS_HYD) { // seuil dénoyé

        phyd->coefs[2] += -2. / 3 * (BC_char[i]->hydwork_param[MU]) * BC_char[i]->hydwork_param[WIDTH] * sqrt(2 * GR) * sqrt(phyd->Z[T_HYD] - BC_char[i]->fion_t_old) * theta; // SW 07/03/2019 ajout 2/3 pour que mu soit le même valeur pour weir et gate mu= 0.6, coefficient du debit, avant c'etait 0.4
        phyd->b += -2. / 3 * (BC_char[i]->hydwork_param[MU]) * BC_char[i]->hydwork_param[WIDTH] * sqrt(2 * GR) * sqrt(phyd->Z[T_HYD] - BC_char[i]->fion_t_old) * theta * BC_char[i]->fion_t;
        phyd->b += 2. / 3 * (BC_char[i]->hydwork_param[MU]) * BC_char[i]->hydwork_param[WIDTH] * sqrt(2 * GR) * (1 - theta) * pow(phyd->Z[T_HYD] - BC_char[i]->fion_t_old, 1.5);
      }

      else if (((phyd->Z[T_HYD] - BC_char[i]->fion_t_old) < 1.5 * (phydn->Z[T_HYD] - BC_char[i]->fion_t_old)) && (phyd->Z[T_HYD] - BC_char[i]->fion_t_old) > EPS_HYD) { // seuil noyé

        phyd->coefs[1] += (sqrt(3.0) * BC_char[i]->hydwork_param[MU]) * BC_char[i]->hydwork_param[WIDTH] * sqrt(2 * GR) * (phydn->Z[T_HYD] - BC_char[i]->fion_t_old) * theta / sqrt(phyd->Z[T_HYD] - phydn->Z[T_HYD]); // SW 07/03/2019 remove 1.5, pour que mu soit le même valeur pour weir et gate 0.6, coefficient du debit, avant c'etait 0.4
        phyd->coefs[2] += -(sqrt(3.0) * BC_char[i]->hydwork_param[MU]) * BC_char[i]->hydwork_param[WIDTH] * sqrt(2 * GR) * (phydn->Z[T_HYD] - BC_char[i]->fion_t_old) * theta / sqrt(phyd->Z[T_HYD] - phydn->Z[T_HYD]);
        phyd->b += (sqrt(3.0) * BC_char[i]->hydwork_param[MU]) * BC_char[i]->hydwork_param[WIDTH] * sqrt(2 * GR) * (phydn->Z[T_HYD] - BC_char[i]->fion_t_old) * (1 - theta) * sqrt(phyd->Z[T_HYD] - phydn->Z[T_HYD]);
      }
    }

    else if (BC_char[i]->fion_type == HT) { // Vanne considérée comme toujours noyée mu = 0.6

      phyd->coefs[1] += BC_char[i]->hydwork_param[MU] * BC_char[i]->hydwork_param[WIDTH] * sqrt(2 * GR) * theta * BC_char[i]->fion_t / sqrt(phyd->Z[T_HYD] - phydn->Z[T_HYD]);
      phyd->coefs[2] += -BC_char[i]->hydwork_param[MU] * BC_char[i]->hydwork_param[WIDTH] * sqrt(2 * GR) * theta * BC_char[i]->fion_t / sqrt(phyd->Z[T_HYD] - phydn->Z[T_HYD]);
      phyd->b += BC_char[i]->hydwork_param[MU] * BC_char[i]->hydwork_param[WIDTH] * sqrt(2 * GR) * (1 - theta) * BC_char[i]->fion_t_old * sqrt(phyd->Z[T_HYD] - phydn->Z[T_HYD]);
    }
  }
}

void HYD_calc_coefs_faces(double t, double dt, s_chyd *pchyd, FILE *fp) {
  int r, f;
  s_face_hyd *pface;
  double q, wl;

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    pface = pchyd->p_reach[r]->p_faces[0];
    // if(pface->id[ABS_HYD] == 1064) // SW debug
    // printf("yes\n");
    // Calcul des coefs pour face amont du bief
    switch (HYD_test_sing_face(pface, UPSTREAM)) {
    case DISCHARGE: {
      q = TS_function_value_t(t, pface->pt_inflows[0]->discharge, fp);
      HYD_calc_coefs_face_qup(pface, q);
      ;
      break;
    } // amont
    case CONTINU:
    case WATER_LEVEL:
    case HYDWORK: {
      HYD_calc_coefs_eq_Q(pface);
      break;
    } //
      /*case HYDWORK : {
      if (pface->limits[UPSTREAM]->BC_char[0]->fion_type == ZT) {
      calc_coefs_eq_Q(pface);
      }
      else {

      }
      break;}*///
    case CONFLUENCE: {
      HYD_calc_coefs_eq_Q_conf_diff(pface);
      break;
    } //
    case DIFFLUENCE:
    case CONF_DIFF: {
      HYD_calc_coefs_face_mass_conservation_up(pface, dt, pchyd->settings->general_param[THETA]);
      break;
    } //
    default: {
      printf("Unknown sing type in function calc_coefs_faces! \n");
      break;
    }
    }
    // if((pchyd->counter->nele_tot + pface->id[ABS_HYD]) == 8656) // SW 30/01/2018 debug
    // LP_printf(fp,"id = %d name = %s\n",pface->id[ABS_HYD],pface->name);

    for (f = 1; f < pchyd->p_reach[r]->nfaces - 1; f++) { // Calcul des coefs pour les faces au centre du bief
      // if(pface->id[ABS_HYD] == 1064) // SW debug
      // printf("yes\n");
      pface = pchyd->p_reach[r]->p_faces[f];
      HYD_calc_coefs_faces_centrales(pface, dt, pchyd->settings->general_param[THETA]);
      // if((pchyd->counter->nele_tot + pface->id[ABS_HYD]) == 8656) // SW 30/01/2018 debug
      // LP_printf(fp,"id = %d name = %s\n",pface->id[ABS_HYD],pface->name);
    }

    // Calcul des coefs pour face aval du bief
    pface = pchyd->p_reach[r]->p_faces[f];
    // if(pface->id[ABS_HYD] == 1064) // SW debug
    // printf("yes\n");
    switch (HYD_test_sing_face(pface, DOWNSTREAM)) {
    case CONTINU:
    case WATER_LEVEL:
    case HYDWORK:
    case CONFLUENCE: {
      HYD_calc_coefs_face_mass_conservation_down(pface, dt, pchyd->settings->general_param[THETA]);
      break;
    }
      /*case WATER_LEVEL : {
      //wl=TS_function_value_t(t,pface->BC_char[0]->fion,fp);
      //calc_coefs_face_wl(pface,wl);
      calc_coefs_face_mass_conservation(pface);
      break;}//*/
      /*case HYDWORK : {
       if (pface->limits[DOWNSTREAM]->BC_char[0]->fion_type == ZT) {
         calc_coefs_face_mass_conservation_down(pface);
       }
       else {

       }
       break;}//*/
      // case CONFLUENCE : {break;}//
    case DIFFLUENCE: {
      HYD_calc_coefs_eq_Q_conf_diff(pface);
      break;
    } //
    case CONF_DIFF: {
      if (pface->id[ABS_HYD] == pface->limits[DOWNSTREAM]->faces[ONE][0]->id[ABS_HYD]) { // 1er bief amont
        HYD_calc_coefs_eq_Q_confdiff(pface);
      } else { // 2eme bief amont
        HYD_calc_coefs_face_mass_conservation_down(pface, dt, pchyd->settings->general_param[THETA]);
      }
      break;
    } //
    case OPEN: {
      HYD_calc_coefs_face_openBC(pface, dt, pchyd->settings->general_param[THETA]);
      break;
    }
    default: {
      printf("Unknown sing type in function calc_coefs_ele! \n");
      break;
    }
    }
    // if((pchyd->counter->nele_tot + pface->id[ABS_HYD]) == 8656) // SW 30/01/2018 debug
    // LP_printf(fp,"id = %d name = %s\n",pface->id[ABS_HYD],pface->name);
  }
}

void HYD_calc_coefs_faces_centrales(s_face_hyd *pface, double dt, double theta) {
  s_hydro_hyd *phyd;
  s_face_hyd *pfacep, *pfacen;
  s_element_hyd *pelep;

  /* Time step and degree of impliciteness */

  double qf, cf;

  double a, b, c, d, e, f, g;

  int i;

  phyd = pface->hydro;

  pelep = pface->element[ONE];
  pfacep = pelep->face[X_HYD][ONE];
  pfacen = pface->element[TWO]->face[X_HYD][TWO];

  // qf = pface->hydro->Q;
  cf = 2 * GR * pelep->center->hydro->H[T_HYD] / (pelep->center->hydro->ks * pelep->center->hydro->ks * pelep->center->hydro->rhyd43); // coef de frottement de Manning-Strickler

  a = (pfacen->hydro->Q[T_HYD] - pfacep->hydro->Q[T_HYD]) / (pface->ds * pface->hydro->Surf);
  b = -4 * phyd->Q[T_HYD] * (pface->element[TWO]->center->hydro->Surf - pface->element[ONE]->center->hydro->Surf);
  b /= pface->ds * (pface->element[TWO]->center->hydro->Surf + pface->element[ONE]->center->hydro->Surf) * (pface->element[TWO]->center->hydro->Surf + pface->element[ONE]->center->hydro->Surf);
  c = -GR * pface->hydro->Surf / pface->ds;
  d = -cf * phyd->Q[T_HYD] / (2 * pface->hydro->H[T_HYD] * pface->hydro->Surf);
  e = a + b - d;
  f = (1 - dt * (1 - theta) * e) / (1 + dt * theta * e);
  g = (dt * c) / (1 + dt * theta * e);

  // face au milieu d'un bief
  phyd->coefs[0] = theta * g;
  phyd->coefs[1] = -theta * g;
  phyd->coefs[2] = 1; // Terme diagonale

  phyd->b = f * phyd->Q[T_HYD];
  phyd->b += (1 - theta) * g * (pface->element[TWO]->center->hydro->Z[T_HYD] - pface->element[ONE]->center->hydro->Z[T_HYD]);
}

void HYD_calc_coefs_face_qup(s_face_hyd *pface, double q) {
  pface->hydro->coefs[0] = 1.;
  pface->hydro->b = q;
}

/*void calc_coefs_face_wl(s_face *pface, double wl)
{
  s_hydro *phyd;
  s_face *pfacep;
  s_element *pelep;
  double alpha, dt, theta;
  int i;
  pelep = pface->element[ONE];
  pfacep = pelep->face[X][ONE];
  phyd = pface->hydro;

  dt = Simul->chronos->dt;
  theta = Simul->settings->general_param[THETA];

  alpha = dt / (pelep->length * (pelep->center->hydro->H * pelep->center->hydro->dL_dZ + pelep->center->hydro->Width));

  phyd->coefs[0] = -1;
  phyd->coefs[1] = 1;
  phyd->b = (pelep->center->hydro->Z - wl) / (alpha * theta);
  phyd->b += - (1 - theta) * (phyd->Q - pfacep->hydro->Q) / theta;
  phyd->b += pelep->length * (theta * pelep->center->hydro->qapp + (1 - theta) * pelep->center->hydro->qapp_old) / theta;
  }*/

// Eq conservation de la masse pour CL aval
void HYD_calc_coefs_face_mass_conservation_down(s_face_hyd *pface, double dt, double theta) {
  s_hydro_hyd *phyd;
  s_face_hyd *pfacep;
  s_element_hyd *pelep;
  double alpha;
  int i;
  pelep = pface->element[ONE];
  pfacep = pelep->face[X_HYD][ONE];
  phyd = pface->hydro;

  alpha = dt / (pelep->length * (pelep->center->hydro->H[T_HYD] * pelep->center->hydro->dL_dZ + pelep->center->hydro->Width));

  phyd->coefs[0] = 1;
  phyd->coefs[1] = -alpha * theta;
  phyd->coefs[2] = alpha * theta;
  phyd->b = pelep->center->hydro->Z[T_HYD];
  phyd->b += -alpha * (1 - theta) * (phyd->Q[T_HYD] - pfacep->hydro->Q[T_HYD]);
  for (i = 0; i < NQAPP; i++)
    phyd->b += pelep->length * alpha * (theta * pelep->center->hydro->qapp[i][T_HYD] + (1 - theta) * pelep->center->hydro->qapp[i][ITER_HYD]);
}

// Eq conservation de la masse pour CL amont
void HYD_calc_coefs_face_mass_conservation_up(s_face_hyd *pface, double dt, double theta) {
  s_hydro_hyd *phyd;
  s_face_hyd *pfacen;
  s_element_hyd *pelen;
  double alpha;
  int i;
  pelen = pface->element[TWO];
  pfacen = pelen->face[X_HYD][TWO];
  phyd = pface->hydro;

  alpha = dt / (pelen->length * (pelen->center->hydro->H[T_HYD] * pelen->center->hydro->dL_dZ + pelen->center->hydro->Width));

  phyd->coefs[0] = 1;
  phyd->coefs[1] = alpha * theta;
  phyd->coefs[2] = -alpha * theta;
  phyd->b = pelen->center->hydro->Z[T_HYD];
  phyd->b += -alpha * (1 - theta) * (pfacen->hydro->Q[T_HYD] - phyd->Q[T_HYD]);
  for (i = 0; i < NQAPP; i++)
    phyd->b += pelen->length * alpha * (theta * pelen->center->hydro->qapp[i][T_HYD] + (1 - theta) * pelen->center->hydro->qapp[i][ITER_HYD]);
}

void HYD_calc_coefs_eq_Q(s_face_hyd *pface) {
  pface->hydro->coefs[0] = -1;
  pface->hydro->coefs[1] = 1;
  pface->hydro->b = 0;
}

void HYD_calc_coefs_eq_Q_conf_diff(s_face_hyd *pface) {
  pface->hydro->coefs[0] = -1;
  pface->hydro->coefs[1] = -1;
  pface->hydro->coefs[2] = 1;
  pface->hydro->b = 0;
}

void HYD_calc_coefs_eq_Q_confdiff(s_face_hyd *pface) {
  pface->hydro->coefs[0] = 1;
  pface->hydro->coefs[1] = -1;
  pface->hydro->coefs[2] = -1;
  pface->hydro->coefs[3] = 1;
  pface->hydro->b = 0;
}

void HYD_calc_coefs_face_openBC(s_face_hyd *pface, double dt, double theta) {
  s_face_hyd *pfacep;
  s_element_hyd *pelep;

  pelep = pface->element[ONE];
  pfacep = pelep->face[X_HYD][ONE];

  pface->hydro->coefs[0] = -theta * pfacep->hydro->Vel / pelep->length / pfacep->hydro->Surf;
  pface->hydro->coefs[1] = 1 / dt / pface->hydro->Surf;
  pface->hydro->coefs[1] += theta * pfacep->hydro->Vel / pelep->length / pface->hydro->Surf;
  pface->hydro->b = pface->hydro->Vel / dt;
  pface->hydro->b += -(1 - theta) * pfacep->hydro->Vel * (pface->hydro->Vel - pfacep->hydro->Vel) / pelep->length;
}

// Renvoie le type de condition limite en amont/aval d'un élément
// LV nov2014 : rajouter message si on teste un élément/face pas en amont/an aval...
int HYD_test_sing_face(s_face_hyd *pface, int up_down) {
  s_singularity_hyd *limit;

  limit = pface->limits[up_down];

  return limit->type;
}

// NF 7/3/2023 hCalcWay Way of calculating water height either with Manning Strickler or Muskingum mass balance (bugged)
void HYD_interpolate_Q_Z_reach(s_reach_hyd *preach, double t, int type_time, double dz, int schem_type, int hCalcWay, FILE *fp) {
  int e, f, nele, nfaces;
  /* Current face */
  s_face_hyd *pface, **p_faces;
  /* Current element */
  s_element_hyd *pele, **p_ele;
  double hold; // LV 3/09/2012
  nele = preach->nele;
  p_ele = preach->p_ele;

  for (e = 0; e < nele; e++) {

    pele = p_ele[e];
    pele->center->hydro->Q[type_time] = TS_interpol(pele->face[X_HYD][ONE]->hydro->Q[type_time], pele->face[X_HYD][ONE]->pk, pele->face[X_HYD][TWO]->hydro->Q[type_time], pele->face[X_HYD][TWO]->pk, pele->center->pk);
    // LV nov2014
    if (schem_type == MUSKINGUM) {
      if (type_time != PIC_HYD) {
        pele->center->hydro->H[type_time + 1] = pele->center->hydro->H[type_time];
      }
      if (hCalcWay == MANNING_STRICKLER)
        HYD_calc_Q2Z_ele(pele, type_time, fp);     // NF 7/3/2023 back to normal
      else                                         // NF 7/3/2023 MUSKINGUM_MB
        HYD_calc_Q2ZMusk_ele(pele, type_time, fp); // NF 7/3/2023 SW version Here we should have an option in libhyd but I don't want to break ProSe, nor implemented yet
    } else {
      if (type_time != PIC_HYD)
        pele->center->hydro->H[type_time + 1] = pele->center->hydro->H[type_time];
      pele->center->hydro->H[type_time] = pele->center->hydro->Z[type_time] - pele->center->description->Zbottom;
    }
    if ((pele->center->hydro->H[type_time] < EPS_HYD) && (schem_type != MUSKINGUM)) // SW 07/11/2018
      LP_warning(fp, "fond sec in element %d pk = %f H = %f, Q = %f\n", pele->id[ABS_HYD], pele->center->pk / 1000, pele->center->hydro->H[type_time], pele->center->hydro->Q[type_time]);
    if (type_time != PIC_HYD)
      pele->center->hydro = HYD_update_hydro_table(pele->center->hydro, pele->center->hydro->H[ITER_HYD], dz, fp);

    pele->center->hydro->Vel = pele->center->hydro->Q[type_time] / pele->center->hydro->Surf;
    //
  }

  nfaces = preach->nfaces;
  p_faces = preach->p_faces;

  for (f = 0; f < nfaces; f++) {

    pface = p_faces[f];

    if (f == 0)
      pface->hydro->Z[type_time] = pface->element[TWO]->center->hydro->Z[type_time];
    else if (f == nfaces - 1)
      pface->hydro->Z[type_time] = pface->element[ONE]->center->hydro->Z[type_time];
    else if (f == 1)
      pface->hydro->Z[type_time] = (2. / 3.) * pface->element[TWO]->center->hydro->Z[type_time] + (1. / 3.) * pface->element[ONE]->center->hydro->Z[type_time];
    else if (f == nfaces - 2)
      pface->hydro->Z[type_time] = (2. / 3.) * pface->element[ONE]->center->hydro->Z[type_time] + (1. / 3.) * pface->element[TWO]->center->hydro->Z[type_time];
    else
      pface->hydro->Z[type_time] = 0.5 * (pface->element[ONE]->center->hydro->Z[type_time] + pface->element[TWO]->center->hydro->Z[type_time]);

    if (type_time != PIC_HYD)
      pface->hydro->H[type_time + 1] = pface->hydro->H[type_time]; // SW 07/11/2018 i think it is pface
    // pele->center->hydro->H[type_time+1] = pele->center->hydro->H[type_time];
    pface->hydro->H[type_time] = pface->hydro->Z[type_time] - pface->description->Zbottom;
    if (type_time != PIC_HYD)
      pface->hydro = HYD_update_hydro_table(pface->hydro, pface->hydro->H[ITER_HYD], dz, fp);
    if ((pface->hydro->H[type_time] < EPS_HYD) && (schem_type != MUSKINGUM)) // SW 07/11/2018
      LP_warning(fp, "fond sec in face %s upstream singularity %s downsream singularity %s, pk = %f, H = %f, Q = %f\n", pface->name, pface->element[UPSTREAM]->reach->limits[UPSTREAM]->name, pface->element[DOWNSTREAM]->reach->limits[DOWNSTREAM]->name, pface->pk / 1000, pface->hydro->H[type_time], pface->hydro->Q[type_time]);
    pface->hydro->Vel = pface->hydro->Q[type_time] / pface->hydro->Surf;
  }
}

/* Calculation of Z at the faces and U at the elements' centers by interpolation */
void HYD_interpolate_Q_Z(double t, s_chyd *pchyd, int type_time, FILE *fp) {
  /* Loop indexes : reach, element, face */
  int r, nreaches, schem_type;
  s_reach_hyd **p_reach;
  s_reach_hyd *preach;
  /* Vertical discretization */
  double dz;
  int hCalcWay; // NF 7/3/2023 hCalcWay Way of calculating water height either with Manning Strickler or Muskingum mass balance (bugged)
#ifdef OMP
  s_smp *psmp;
  psmp = pchyd->settings->psmp;
#endif

  dz = pchyd->settings->general_param[DZ];
  hCalcWay = pchyd->settings->hCalcWay;
  schem_type = pchyd->settings->schem_type;
  nreaches = pchyd->counter->nreaches;
  p_reach = pchyd->p_reach;

#ifdef OMP
  int taille;
  int nthreads;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);

  psmp->chunk = PC_set_chunk_size_silent(fp, pchyd->counter->nreaches, nthreads);
  taille = psmp->chunk;

#pragma omp parallel shared(p_reach, dz, schem_type, nthreads, nreaches, t, type_time, taille) private(r)
  {
#pragma omp for schedule(dynamic, taille)
#endif

    for (r = 0; r < pchyd->counter->nreaches; r++) {
      // preach=p_reach[r];
      HYD_interpolate_Q_Z_reach(p_reach[r], t, type_time, dz, schem_type, hCalcWay, fp);
    }
#ifdef OMP
  } // end of parallel section
#endif
}

void HYD_initialize_GC(s_chyd *pchyd, s_chronos_CHR *chronos, FILE *fpout) {

  int schem_type;
  schem_type = pchyd->settings->schem_type;

  if (schem_type == ST_VENANT) {
    HYD_find_id_coefs_ele(pchyd);
    HYD_find_id_coefs_face(pchyd);
    HYD_calc_prop_gc(pchyd, fpout);
  } else if (schem_type == MUSKINGUM) {
    HYD_find_id_coefs_muskingum(pchyd, fpout);
    HYD_calc_LHS_Musk(pchyd, chronos, fpout);
    HYD_calc_prop_gc(pchyd, fpout);
    HYD_fill_mat6_musk(pchyd);
  } else
    LP_error(fpout, "no schem_type %d \n Check input files\n", schem_type);
}

void HYD_calc_Q2Z_ele(s_element_hyd *pele, int type_time, FILE *fp) {
  s_reach_hyd *preach;
  s_face_hyd *face_up, *face_down;
  double pente, width, h, dh = -CODE;
  int n = 0;
  preach = pele->reach;
  pente = pele->reach->pmusk->param[SLOPE];
  width = preach->width;

  if (pele->center->hydro->Q[type_time] > 0) {
    h = pele->center->hydro->Q[type_time] / (TS_function_value_t(pele->center->hydro->Q[type_time], preach->strickler, fp) * width * sqrt(pente));

    h = pow(h, 3. / 5.);
    pele->center->hydro->Z[type_time] = h + pele->center->description->Zbottom;
  } else {
    h = 0;
    pele->center->hydro->Z[type_time] = pele->center->description->Zbottom;
  }

  pele->center->hydro->H[type_time] = h;

  if (((pele->center->hydro->H[type_time] < 0) || (isnan(pele->center->hydro->H[type_time]) != 0)) || pele->center->hydro->Z[type_time] < -100) {

    LP_printf(fp, "Z : %f H : %f Q : %f zbot %f stricker %f width %f slope %f\n", pele->center->hydro->Z[type_time], pele->center->hydro->H[type_time], pele->center->hydro->Q[type_time], pele->center->description->Zbottom, TS_function_value_t(pele->center->hydro->Q[type_time], preach->strickler, fp), width, pente);
    LP_error(fp, "river heads are negatives or nan produced for ele ABS %d GIS %d in reach named %s \n", pele->id[ABS_HYD], pele->id[GIS_HYD], preach->river);

  }
#ifdef DEBUG
  else {
    LP_printf(fp, "Z : %f H : %f Q : %f zbot %f stricker %f width %f slope %f\n", pele->center->hydro->Z[type_time], pele->center->hydro->H[type_time], pele->center->hydro->Q[type_time], pele->center->description->Zbottom, TS_function_value_t(pele->center->hydro->Q[type_time], preach->strickler, fp), width, pente);
  }
#endif
  //}
  //    LP_printf(fp,"PIC_HYD pele=%d Z : %f H : %f Q : %f zbot %f  width %f slope %f\n",pele->id[INTERN_HYD],pele->center->hydro->Z[type_time],pele->center->hydro->H[type_time],pele->center->hydro->Q[type_time],pele->center->description->Zbottom,width,pente);
}

void HYD_calc_Q2ZMusk_ele(s_element_hyd *pele, int type_time, FILE *fp) {
  s_reach_hyd *preach;
  s_face_hyd *face_up, *face_down;
  double pente, width, h, dh = -CODE;
  double theta = 1., Vnew, alpha, k;
  double Qiter_in, Qiter_out, Qt_in, Qt_out, qappiter, qappt;
  int n = 0, i;
  preach = pele->reach;
  pente = pele->reach->pmusk->param[SLOPE];
  width = preach->width;
  // SW 07/10/2021
  k = pele->face[X_HYD][TWO]->hydro->pmusk->param[K];
  alpha = pele->face[X_HYD][TWO]->hydro->pmusk->param[ALPHA];
  if (pele->center->hydro->Q[type_time] > 0) {
    // SW 07/10/2021 on Muskingum bilan de masse
    Qt_in = pele->face[X_HYD][ONE]->hydro->Q[type_time];
    Qt_out = pele->face[X_HYD][TWO]->hydro->Q[type_time];
    qappt = 0.;
    for (i = 0; i < NQAPP; i++) {
      qappt += pele->center->hydro->qapp[i][INTERPOL_HYD] * pele->length;
    }

    if (preach->limits[ONE]->nupstr_reaches == 0 && pele->id[INTERN_HYD] == 0) // amont
    {
      Vnew = k * (alpha * (Qt_in + qappt) + (1 - alpha) * Qt_out);
    } else {
      Vnew = k * (alpha * (Qt_in + qappt) + (1 - alpha) * Qt_out);
    }
    h = Vnew / (pele->length * width);
    pele->center->hydro->Z[type_time] = h + pele->center->description->Zbottom;

  } else {
    h = 0;
    pele->center->hydro->Z[type_time] = pele->center->description->Zbottom;
  }
  pele->center->hydro->H[type_time] = h;

  if (((pele->center->hydro->H[type_time] < 0) || (isnan(pele->center->hydro->H[type_time]) != 0)) || pele->center->hydro->Z[type_time] < -100) {
    LP_printf(fp, "k = %f qappt = %f qt_in = %f qt_out = %f alpha = %f Vnew = %f Z : %f H : %f Q : %e zbot %f stricker %f width %f slope %f\n", k, qappt, Qt_in, Qt_out, alpha, Vnew, pele->center->hydro->Z[type_time], pele->center->hydro->H[type_time], pele->center->hydro->Q[type_time], pele->center->description->Zbottom,
              TS_function_value_t(pele->center->hydro->Q[type_time], preach->strickler, fp), width, pente);
    LP_warning(fp, "river heads are negatives or nan produced for ele ABS %d GIS %d in reach named %s \n", pele->id[ABS_HYD], pele->id[GIS_HYD], preach->river);

  }
#ifdef DEBUG
  else {
    LP_printf(fp, "Z : %f H : %f Q : %f zbot %f stricker %f width %f slope %f\n", pele->center->hydro->Z[type_time], pele->center->hydro->H[type_time], pele->center->hydro->Q[type_time], pele->center->description->Zbottom, TS_function_value_t(pele->center->hydro->Q[type_time], preach->strickler, fp), width, pente);
  }
#endif
}

/* solve system and fill element for pic time the T_HYD doesn't change.
 */
// Note : NG : 11/06/2021 : Le prototype de cette fonction n'est référencé nulle part dans libhyd...
void HYD_solve_hydro_pic(double t, s_chyd *pchyd, s_clock_CHR *clock, s_chronos_CHR *chronos, int *ts_count, FILE *fp) {

  int schem_type;
  double dt;

  dt = chronos->dt;
  schem_type = pchyd->settings->schem_type;

  CHR_begin_timer(); // LV 3/09/2012

  /* Calculation of all the matrixes in the system */
  HYD_calc_Qapp(t, pchyd, fp);
  if (schem_type == MUSKINGUM) {
    HYD_calc_RHS_Musk(t, dt, pchyd, fp);
  } else {
    HYD_calc_coefs_ele(t, dt, pchyd, fp);
    HYD_calc_coefs_faces(t, dt, pchyd, fp);
  }

  clock->time_spent[MATRIX_FILL_CHR] += CHR_end_timer(); // LV 3/09/2012
  CHR_begin_timer();                                     // LV 3/09/2012

  /* Solving of the equations */

  HYD_calc_sol_hydro(pchyd, ts_count, fp); // LV nov2014 seul appel dans hydraulics_v2.c aux fonctions de libraire libgc (cf. calc_hyd_gc.c)

  clock->time_spent[MATRIX_SOLVE_CHR] += CHR_end_timer(); // LV 3/09/2012

  CHR_begin_timer(); // LV 3/09/2012

  /* Calculation of the new hydraulic characteristics at each face and element center */
  HYD_extract_solgc_pic(pchyd, fp);

  HYD_interpolate_Q_Z(t, pchyd, PIC_HYD, fp);

  clock->time_spent[MATRIX_GET_CHR] += CHR_end_timer(); // LV 3/09/2012
}
