/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_input.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"

double **HYD_get_init_Q(FILE *fpdon, s_chyd *pchyd, FILE *flog) {
  int nele_tot, nreach, nele, r, e, output, i;
  s_element_hyd *pele;
  s_reach_hyd *preach;
  int nb_data, id_abs;
  int errno, position, position_init;
  double **val, Q_val;
  nele_tot = pchyd->counter->nele_tot;
  nreach = pchyd->counter->nreaches;

  output = NB_Q_FIN_HYD;
  val = (double **)malloc(output * sizeof(double *));
  for (i = 0; i < output; i++)
    val[i] = (double *)malloc(nele_tot * sizeof(double));
  position = 0;
  errno = fseek(fpdon, position, SEEK_SET);
  if (errno < 0)
    LP_error(flog, "Cannot reach position %d in file\n", position);
  fread(&nb_data, sizeof(int), 1, fpdon);

  if (nb_data != nele_tot)
    LP_error(flog, "number of value %d not equal to the number of element %d\n", nb_data, nele_tot);
  for (i = 0; i < output; i++) {
    position_init = (i + 1) * sizeof(int) + i * (nele_tot * sizeof(double) + sizeof(int));
    for (r = 0; r < nreach; r++) {

      preach = pchyd->p_reach[r];
      nele = preach->nele;
      for (e = 0; e < nele; e++) {
        pele = preach->p_ele[e];
        id_abs = pele->id[ABS_HYD];
        position = (id_abs) * sizeof(double);
        if (position < 0)
          LP_error(flog, "position %d\n", position);
        position += position_init;
        errno = fseek(fpdon, position, SEEK_SET);
        if (errno < 0)
          LP_error(flog, "Cannot reach position %d in file\n", position);
        fread(&Q_val, sizeof(double), 1, fpdon);
        val[i][id_abs] = Q_val;
      }
    }
  }

  return val;
}
