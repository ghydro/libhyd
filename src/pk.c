/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: pk.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"

/* Structure containing the simulation caracteristics */
// extern s_simul *Simul;

/* Initializes the calculation of the pk */
void HYD_calculate_pk_sing(s_chyd *pchyd, FILE *fp) {
  /* Loop indexes */
  int s, r;
  s_singularity_hyd *psing;
  int propage;

  if (pchyd->settings->pk_defined == -1) {
    LP_printf(fp, "No PK defined. Value 0. attributed to the first defined singularity\n");
    pchyd->settings->pk_defined = 0;
    pchyd->p_sing[0]->pk = 0.; // BL faire attention au positionnement de p_sing[0]
  }

  for (s = 0; s < pchyd->counter->nsing; s++)
    pchyd->p_sing[s]->passage = NO;

  for (r = 0; r < pchyd->counter->nreaches; r++)
    pchyd->p_reach[r]->passage = NO;
  psing = pchyd->p_sing[pchyd->settings->pk_defined];
  // BL test de definition des pk de plusieurs reseaux sur le bassin !!
  while (psing != NULL) {

    HYD_propage_pk(psing, psing->pk, UPSTREAM, fp);
    HYD_propage_pk(psing, psing->pk, DOWNSTREAM, fp);

    for (s = 0; s < pchyd->counter->nsing; s++) {

      psing = pchyd->p_sing[s];

      if ((psing->passage == YES) && (psing->ndownstr_reaches > 0)) {

        propage = NO;

        for (r = 0; r < psing->ndownstr_reaches; r++) {
          // LP_printf(fp,"psing id %d name %s nb_down %d \n",psing->id[ABS_HYD],psing->name,psing->ndownstr_reaches); // BL to debug
          if (psing->p_reach[DOWNSTREAM][r]->limits[DOWNSTREAM]->passage == NO)
            propage = YES;
        }
        if (propage == YES)
          HYD_propage_pk(psing, psing->pk, DOWNSTREAM, fp);
      }

      if ((psing->passage == YES) && (psing->nupstr_reaches > 0)) {

        propage = NO;

        for (r = 0; r < psing->nupstr_reaches; r++) {
          if (psing->p_reach[UPSTREAM][r]->limits[UPSTREAM]->passage == NO)
            propage = YES;
        }
        if (propage == YES)
          HYD_propage_pk(psing, psing->pk, UPSTREAM, fp);
      }
    }
    psing = HYD_get_undefined_pk(pchyd, fp);
  }
  // LP_printf(fp,"Out of calculate_pk_sing\n"); // BL to debug
}

/* From each singularity, launches the propagation of the pk value in one given direction
 * function's arguments :
 *  - psing : address of the singularity which PK is known
 *  - pk : value of the pk at psing
 */
void HYD_propage_pk(s_singularity_hyd *psing, double pk, int direction, FILE *fp) {
  psing->pk = pk;
  psing->passage = YES;
  // LP_printf(fp,"psing name in HYD_propage_pk %s\n",psing->name); // BL to debug
  HYD_module_calculation_pk(psing, pk, direction, fp);
}

s_singularity_hyd *HYD_get_undefined_pk(s_chyd *pchyd, FILE *fp) {
  s_singularity_hyd *psing;
  int i;
  int nb_undef = 0;

  for (i = 0; i < pchyd->counter->nsing; i++) {
    psing = pchyd->p_sing[i];
    if (psing->passage == NO) {
      if (fabs(psing->pk - CODE) < EPS_HYD)
        psing->pk = 0.;
      nb_undef++;
      break;
    }
  }
  if (nb_undef == 0) {
    psing = NULL;
  }
  return psing;
}

/* Calculation of the pk of an upstream /downstream limit of a reach
 * The pk is known at one limit and we calculate its value of the pk of the singularities one reach away in the upstream or downstream direction
 * We continue going upstream or downstream while
 * - the new singularity's pk was not calculated (passage = NO)
 * - the new pk is
 *   - smaller if we are going upstream
 *   - bigger if we are going downstream
 *   (in the case of several possible paths to reach a singularity (confluences), the longest one is used to calculatede its pk
 */
void HYD_module_calculation_pk(s_singularity_hyd *psing, double pk, int direction, FILE *fp) {
  /* Loop index, reach in direction */
  int r;
  s_reach_hyd *preach;
  /* Nb of reaches in direction */
  int nreach;
  /* next sing in direction */
  s_singularity_hyd *psing1;
  /* Pk of next sing in direction */
  double pk_bis;

  if (direction == UPSTREAM)
    nreach = psing->nupstr_reaches;
  else
    nreach = psing->ndownstr_reaches;

  for (r = 0; r < nreach; r++) {

    preach = psing->p_reach[direction][r];
    preach->passage = YES;
    psing1 = preach->limits[direction];
    if (psing1 != NULL) {
      // LP_printf(fp,"psing1 in module_calculation_pk %s\n",psing1->name);// BL to debug
      if (direction == DOWNSTREAM)
        pk_bis = psing->pk + preach->length;
      else
        pk_bis = psing->pk - preach->length;
      if ((psing1->passage == NO) || ((psing1->passage == YES) && (fabs(pk_bis - psing1->pk) > EPS_HYD) && (((direction == UPSTREAM) && (pk_bis < psing1->pk)) || ((direction == DOWNSTREAM) && (pk_bis > psing1->pk))))) {
        HYD_propage_pk(psing1, pk_bis, direction, fp);
      }
    }
  }
}

/* Calculation of the pk at the faces and element centers */
void HYD_calculate_pk_faces_centers(s_chyd *pchyd, FILE *fp) {
  /* Loop indexes */
  int r, f, e;
  /* Intermediary calculation of distances */
  double length_temp;
  /* Current face or element */
  s_face_hyd *pface;
  s_element_hyd *pele;
  // LP_printf(fp,"Enter in HYD_calculate_pk_faces_centers \n"); // BL to debug
  for (r = 0; r < pchyd->counter->nreaches; r++) {

    length_temp = 0.;

    for (f = 0; f < pchyd->p_reach[r]->nfaces; f++) {

      pface = pchyd->p_reach[r]->p_faces[f];
      pface->pk = pchyd->p_reach[r]->limits[UPSTREAM]->pk + length_temp;
      //  LP_printf(fp,"new_dx = %f \n",pface->new_dx); // BL to debug
      length_temp += pface->new_dx;
    }

    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {

      pele = pchyd->p_reach[r]->p_ele[e];
      if (pchyd->settings->schem_type == MUSKINGUM) {
        pele->center->pk = 0.5 * (pele->face[X_HYD][ONE]->pk + pele->face[X_HYD][TWO]->pk);
      } else {
        if (e == 0)
          pele->center->pk = pele->face[X_HYD][ONE]->pk;
        else if (e == pchyd->p_reach[r]->nele - 1)
          pele->center->pk = pele->face[X_HYD][TWO]->pk;
        else {
          pele->center->pk = 0.5 * (pele->face[X_HYD][ONE]->pk + pele->face[X_HYD][TWO]->pk);
        }
      }
      pele->face[Y_HYD][ONE]->pk = pele->center->pk;
      pele->face[Y_HYD][TWO]->pk = pele->center->pk;
    }
  }
  // LP_printf(fp,"Out of HYD_calculate_pk_faces_centers \n");// BL to debug
}
