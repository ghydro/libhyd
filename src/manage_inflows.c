/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_inflows.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"

/**\fn HYD_attribute_diffuse_inflows_time_series(s_inflow_hyd*, int, int, double, int, FILE*)
 *\brief For all inputs variables types, defines forcing value series at the element scale (= Temperature and concentrations time series
         remain untouched. Discharge is dispatched over river elements according to the position and length of diffuse inflow.
 *\return -
 */
void HYD_attribute_diffuse_inflows_time_series(s_inflow_hyd *pinflow, int npt_inflows, int ivar, double coef, int nb_species, FILE *fp) {
  int ispe;

  switch (ivar) {
  case DISCHARGE_INFLOW_HYD: {
    pinflow->diff_inflow->pt_inflows[npt_inflows]->discharge = new_function();
    pinflow->diff_inflow->pt_inflows[npt_inflows]->discharge = TS_multiply_ft_double(pinflow->discharge, coef);
    break;
  }
  case CONCENTRATION_INFLOW_HYD: {
    if (pinflow->concentration != NULL) {
      for (ispe = 0; ispe < nb_species; ispe++) {
        if (pinflow->concentration[ispe] != NULL) {
          pinflow->diff_inflow->pt_inflows[npt_inflows]->concentration[ispe] = new_function();
          pinflow->diff_inflow->pt_inflows[npt_inflows]->concentration[ispe] = pinflow->concentration[ispe];
        }
      }
      break;
    }
  case TEMPERATURE_INFLOW_HYD: {
    if (pinflow->temperature != NULL) {
      pinflow->diff_inflow->pt_inflows[npt_inflows]->temperature = new_function();
      pinflow->diff_inflow->pt_inflows[npt_inflows]->temperature = pinflow->temperature;
    }
    break;
  }
  }
  }
}

/**\fn HYD_create_inflows(s_inflow_hyd*, s_chyd*, int, FILE*)
 *\brief General function which defines all inflows (diffuse, point, etc.) arriving
         in the river elements' faces.
 *\return -
 */
void HYD_create_inflows(s_inflow_hyd *pinfl, s_chyd *pchyd, int nb_species, FILE *fp) {
  // Loop indexes
  int r, i, e, ivar, ispe, idir, iface;
  // Number of inflows initialized in each reach
  int *ninflows_reach;
  // Number of inflows initialized at each face
  int ****ninflows_face;
  // Number of point inflows constituting a diffuse inflow
  int npt_inflows;
  // Abscisses from the upstream limit
  double x = 0., x_old = 0., x_new = 0.;
  // Side on which the inflow arrives
  int side;
  // Current reach
  s_reach_hyd *preach;
  // Current point inflow
  s_inflow_hyd *pinflow;
  // Multiplication factor to distribute discharge inflow of several river element in case of diffuse inflow
  double coef_ts;
  // Verification counter to make sure the diffuse discharge has been correcty distributed.
  double verif_coef = 0.;
  // Diffuse inflow's ending abscisse (ie. x+dx)
  double xend_diff_inflow = 0.;

  ninflows_reach = (int *)calloc(pchyd->counter->nreaches, sizeof(int));
  ninflows_face = (int ****)calloc(pchyd->counter->nreaches, sizeof(int ***));

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];
    preach->p_inflows = (s_inflow_hyd **)calloc(preach->ninflows, sizeof(s_inflow_hyd *));
    ninflows_reach[r] = 0;
  }

  pinfl = HYD_browse_inflows(pinfl, BEGINNING_HYD, fp);

  // Attribution of the inflows to the corresponding river reach
  if (pinfl != NULL) {
    if (pinfl->discharge == NULL)
      LP_error(fp, "libhyd%4.2f : Error in file %s, function %s at line %d : Incoherent input : Inflow %s does not contain any discharge information !\n", NVERSION_HYD, __FILE__, __func__, __LINE__, pinfl->name);

    pinfl->reach->p_inflows[0] = pinfl;
    ninflows_reach[pinfl->reach->id[ABS_HYD]]++;

    while (pinfl->next != NULL) {
      pinfl = pinfl->next;
      pinfl->reach->p_inflows[ninflows_reach[pinfl->reach->id[ABS_HYD]]] = pinfl;
      ninflows_reach[pinfl->reach->id[ABS_HYD]]++;
    }
  }

  // Attribution of the inflows to the corresponding face
  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];
    verif_coef = 0.;

    ninflows_face[r] = (int ***)calloc(preach->nele, sizeof(int **));

    for (e = 0; e < preach->nele; e++) {
      ninflows_face[r][e] = (int **)calloc(2, sizeof(int *));
      ninflows_face[r][e][X_HYD] = (int *)calloc(2, sizeof(int));
      ninflows_face[r][e][Y_HYD] = (int *)calloc(2, sizeof(int));
      ninflows_face[r][e][X_HYD][ONE] = 0;
      ninflows_face[r][e][X_HYD][TWO] = 0;
      ninflows_face[r][e][Y_HYD][ONE] = 0;
      ninflows_face[r][e][Y_HYD][TWO] = 0;
    }

    // Counting of the number of inflows arriving through each face
    for (i = 0; i < preach->ninflows; i++) {
      side = HYD_riverside_inflow(preach->p_inflows[i]->transversal_position); // Returns side (ONE [left bank], TWO [right bank]) on which arrives the inflow

      // If DIFFUSE_INFLOW : count of the number of point inflows that will constitute the total inflow
      if (preach->p_inflows[i]->type == DIFFUSE_INFLOW) {
        xend_diff_inflow = preach->p_inflows[i]->x + preach->p_inflows[i]->diff_inflow->dx;

        x_old = 0.;
        e = 0;
        x_new = preach->p_ele[e]->length;

        if (x_new > xend_diff_inflow) // In case a reach is made of one unique element.
        {
          e++;
          x_old = x_new;
        } else {
          // Detecting the element the inflows will start on (ie. position of the first point inflow).
          while (x_old < preach->p_inflows[i]->x) {
            x_old = x_new;
            e++;
            x_new += preach->p_ele[e]->length;
          }
        }

        // = First point inflow is at element e-1
        preach->p_inflows[i]->diff_inflow->npt_inflows = 1;
        preach->p_ele[e - 1]->face[Y_HYD][side]->ninflows++;

        // Detecting the ending river element of the diffuse inflow
        while (x_old < xend_diff_inflow) {
          x_old = x_new;
          e++;

          if (e < preach->nele)
            x_new += preach->p_ele[e]->length;

          preach->p_inflows[i]->diff_inflow->npt_inflows++;
          preach->p_ele[e - 1]->face[Y_HYD][side]->ninflows++;
        }
        LP_printf(fp, "Number of point inflows constituting the diffuse inflow named %s = %d\n", preach->p_inflows[i]->name, preach->p_inflows[i]->diff_inflow->npt_inflows);
      } else if (preach->p_inflows[i]->type == UPSTREAM_INFLOW) {
        preach->p_faces[0]->ninflows++;
      } else // ie. either INFLUENT or EFFLUENT (point inflow only)
      {
        x = 0.;
        e = 0;

        while ((x < preach->p_inflows[i]->x) && (e < preach->nele)) {
          x += preach->p_ele[e]->length;
          e++;
        }

        e = e < 1 ? 1 : e;
        preach->p_ele[e - 1]->face[Y_HYD][side]->ninflows++;
      }
    }

    // Memory allocation for point inflows pointer at element's faces
    for (e = 0; e < preach->nele; e++) {
      for (idir = 0; idir < Z_HYD; idir++) {
        for (iface = 0; iface < NELEMENT_HYD; iface++) {
          if (preach->p_ele[e]->face[idir][iface]->ninflows > 0)
            preach->p_ele[e]->face[idir][iface]->pt_inflows = (s_pt_inflow_hyd **)calloc(preach->p_ele[e]->face[idir][iface]->ninflows, sizeof(s_pt_inflow_hyd *));
        }
      }
    }

    // Attribution of the inflows (ie. time series) to the corresponding faces
    for (i = 0; i < preach->ninflows; i++) {
      pinflow = preach->p_inflows[i];
      side = HYD_riverside_inflow(pinflow->transversal_position);

      // If DIFFUSE_INFLOW : Creation of value time serie of the point inflows which are part of the diffuse inflow.
      if (pinflow->type == DIFFUSE_INFLOW) {
        pinflow->diff_inflow->pt_inflows = (s_pt_inflow_hyd **)calloc(pinflow->diff_inflow->npt_inflows, sizeof(s_pt_inflow_hyd *));

        npt_inflows = 0;
        x_old = 0.;
        e = 0;
        x_new = preach->p_ele[e]->length;

        while (x_old < pinflow->x) {
          x_old = x_new;
          e++;
          if (preach->nele > 1)
            x_new += preach->p_ele[e]->length;
        }

        // Case of the first point inflow constituting the diffuse inflow
        pinflow->diff_inflow->pt_inflows[npt_inflows] = HYD_create_point_inflow(pinflow->name);

        for (ivar = 0; ivar < NINFLOW_VARS_HYD; ivar++) {
          coef_ts = 0.;
          coef_ts = (x_old - pinflow->x) / (pinflow->diff_inflow->dx);

          if (x_old > xend_diff_inflow)
            coef_ts = 1.0; // Beware ! Case if x and x+dx both belong to the same river element => pseudo point inflow !

          // LP_printf(fp,"First element : %d %f\n",e-1,coef_ts);  // NG check

          HYD_attribute_diffuse_inflows_time_series(pinflow, npt_inflows, ivar, coef_ts, nb_species, fp);
          verif_coef += coef_ts;
        }
        // LP_printf(fp,"Current reading position : element = %d, x_old = %f x_new = %f.\n",e, x_old, x_new);

        pinflow->diff_inflow->pt_inflows[npt_inflows]->face = preach->p_ele[e - 1]->face[Y_HYD][side];
        preach->p_ele[e - 1]->face[Y_HYD][side]->pt_inflows[ninflows_face[r][e - 1][Y_HYD][side]] = pinflow->diff_inflow->pt_inflows[npt_inflows];
        ninflows_face[r][e - 1][side]++;
        npt_inflows++;

        // Checking for others elements to distribute the diffuse inflow over
        while (x_old < xend_diff_inflow) {
          pinflow->diff_inflow->pt_inflows[npt_inflows] = HYD_create_point_inflow(pinflow->name);

          if (x_new < xend_diff_inflow) {
            // Time series attributions
            for (ivar = 0; ivar < NINFLOW_VARS_HYD; ivar++) {
              coef_ts = 0.;
              coef_ts = (preach->p_ele[e]->length) / (pinflow->diff_inflow->dx); // Case if river element is entirely covered
              // LP_printf(fp,"In-between element %d %f\n",e,coef_ts);   // NG check
              HYD_attribute_diffuse_inflows_time_series(pinflow, npt_inflows, ivar, coef_ts, nb_species, fp);
              verif_coef += coef_ts;
            }
          } else {
            // Time series attributions
            for (ivar = 0; ivar < NINFLOW_VARS_HYD; ivar++) {
              coef_ts = 0.;
              coef_ts = (xend_diff_inflow - x_old) / (pinflow->diff_inflow->dx); // Case if element is partially covered (ie. end of the diffuse inflow)
              // LP_printf(fp,"End element %d %f\n",e,coef_ts);   // NG check
              HYD_attribute_diffuse_inflows_time_series(pinflow, npt_inflows, ivar, coef_ts, nb_species, fp);
              verif_coef += coef_ts;
            }
          }

          // LP_printf(fp,"Current reading position : element = %d, x_old = %f x_new = %f.\n",e, x_old, x_new);

          pinflow->diff_inflow->pt_inflows[npt_inflows]->face = preach->p_ele[e]->face[Y_HYD][side];
          preach->p_ele[e]->face[Y_HYD][side]->pt_inflows[ninflows_face[r][e][Y_HYD][side]] = pinflow->diff_inflow->pt_inflows[npt_inflows];
          ninflows_face[r][e][side]++;
          npt_inflows++;

          x_old = x_new;
          e++;
          if (e < preach->nele)
            x_new += preach->p_ele[e]->length;
        }

        // Verification step : Making sure the sum of the distribution coefficents of the diffuse inflow over river elements is OK
        verif_coef /= (double)NINFLOW_VARS_HYD;

        LP_printf(fp, "Sum of distribution coefficients : %f.\n", verif_coef); // NG check

        if (fabs(verif_coef - 1.0) > EPS_HYD)
          LP_error(fp, "libhyd%4.2f : Error in file %s, function %s at line %d : Diffuse inflow named %s linked to river reach GIS id %d has not been properly distributed.\n", NVERSION_HYD, __FILE__, __func__, __LINE__, pinflow->name, pinflow->reach->id[GIS_HYD]);

      }

      else if (pinflow->type == UPSTREAM_INFLOW) {
        preach->p_faces[0]->pt_inflows[ninflows_face[r][0][X_HYD][ONE]] = pinflow->pt_inflow;
        preach->p_faces[0]->pt_inflows[ninflows_face[r][0][X_HYD][ONE]]->face = preach->p_faces[0];
        ninflows_face[r][0][X_HYD][side]++;
      } else {
        x = 0.;
        e = 0;

        while ((x < pinflow->x) && (e < preach->nele)) {
          x += preach->p_ele[e]->length;
          e++;
        }

        e = e < 1 ? 1 : e;

        preach->p_ele[e - 1]->face[Y_HYD][side]->pt_inflows[ninflows_face[r][e - 1][Y_HYD][side]] = pinflow->pt_inflow;
        preach->p_ele[e - 1]->face[Y_HYD][side]->pt_inflows[ninflows_face[r][e - 1][Y_HYD][side]]->face = preach->p_ele[e - 1]->face[Y_HYD][side];
        ninflows_face[r][e - 1][Y_HYD][side]++;
      }
    }
  }

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];
    for (e = 0; e < preach->nele; e++) {
      // free(ninflows_face[r][e][X_HYD]);
      // free(ninflows_face[r][e][Y_HYD]);
      free(ninflows_face[r][e]);
    }
    free(ninflows_face[r]);
  }

  free(ninflows_face);
}

/**\fn HYD_create_inflow(char*, s_chyd*)
 *\brief Allocates and initializes by default a inflow structure
 *\return newly created s_inflow_hyd pointer
 */
s_inflow_hyd *HYD_create_inflow(char *name, s_chyd *pchyd) {
  s_inflow_hyd *pinfl;
  int i;

  LP_lowercase(name);
  pinfl = new_inflow();
  bzero((char *)pinfl, sizeof(s_inflow_hyd));
  pchyd->counter->ninflows++;

  pinfl->name = strdup(name);
  pinfl->transversal_position = DEFAULT_INFLOW_YPOS; // Assigned to left bank by default
  pinfl->x = 0.;                                     // Inflow starting at reach upstream limit by default
  pinfl->discharge = NULL;
  pinfl->temperature = NULL;

  pinfl->concentration = (s_ft **)malloc(NB_SPECIE_INFLOW_MAX_HYD * sizeof(s_ft *)); // Moche ! Complexe à gérer dynamiquement.
  bzero((char *)pinfl->concentration, NB_SPECIE_INFLOW_MAX_HYD * sizeof(s_ft *));
  for (i = 0; i < NB_SPECIE_INFLOW_MAX_HYD; i++)
    pinfl->concentration[i] = NULL;

  return pinfl;
}

/**\fn HYD_create_point_inflow(char *name)
 *\brief Allocates and initializes by default a new point inflow structure
 *\return Pointer towards newly created point inflow structure
 */
s_pt_inflow_hyd *HYD_create_point_inflow(char *name) {
  s_pt_inflow_hyd *pt_inflow;
  int i;

  LP_lowercase(name);
  pt_inflow = new_pt_inflow();
  bzero((char *)pt_inflow, sizeof(s_pt_inflow_hyd));

  pt_inflow->name = strdup(name);
  pt_inflow->discharge = NULL;
  pt_inflow->temperature = NULL;
  pt_inflow->concentration = NULL;

  pt_inflow->concentration = (s_ft **)malloc(NB_SPECIE_INFLOW_MAX_HYD * sizeof(s_ft *));
  bzero((char *)pt_inflow->concentration, NB_SPECIE_INFLOW_MAX_HYD * sizeof(s_ft *));
  for (i = 0; i < NB_SPECIE_INFLOW_MAX_HYD; i++)
    pt_inflow->concentration[i] = NULL;

  return pt_inflow;
}

/**\fn HYD_create_new_diffuse_inflow()
 *\brief Creates and initializes a new diffuse inflow
 *\return Pointer towards newly created diffuse inflow structure
 */
s_diffuse_inflow_hyd *HYD_create_new_diffuse_inflow(void) {
  s_diffuse_inflow_hyd *pdiff_inflow;
  int i;

  pdiff_inflow = new_diffuse_inflow();
  bzero((char *)pdiff_inflow, sizeof(s_diffuse_inflow_hyd));

  pdiff_inflow->discharge = NULL;
  pdiff_inflow->temperature = NULL;
  pdiff_inflow->concentration = NULL;

  pdiff_inflow->concentration = (s_ft **)malloc(NB_SPECIE_INFLOW_MAX_HYD * sizeof(s_ft *));
  bzero((char *)pdiff_inflow->concentration, NB_SPECIE_INFLOW_MAX_HYD * sizeof(s_ft *));
  for (i = 0; i < NB_SPECIE_INFLOW_MAX_HYD; i++)
    pdiff_inflow->concentration[i] = NULL;

  return pdiff_inflow;
}

/**\fn HYD_chain_inflows(s_inflow_hyd*, s_inflow_hyd*)
 *\brief Function which chains two inflows pointers
 */
s_inflow_hyd *HYD_chain_inflows(s_inflow_hyd *pinfl1, s_inflow_hyd *pinfl2) {
  if ((pinfl1 != NULL) && (pinfl2 != NULL)) {
    pinfl1->next = pinfl2;
    pinfl2->prev = pinfl1;
  }
  return pinfl1;
}

/**\fn HYD_browse_inflows(s_inflow_hyd*, int, FILE*)
 *\brief Rewinds or fast-forwards a inflow pointer chain
 *\return Pointer towards the first or last pointer of the chain, according to 'browse_dir'
 */
s_inflow_hyd *HYD_browse_inflows(s_inflow_hyd *pchain, int browse_dir, FILE *flog) {
  s_inflow_hyd *ptmp = pchain;

  switch (browse_dir) {
  case BEGINNING_HYD:
    while (ptmp->prev != NULL)
      ptmp = ptmp->prev;
    break;
  case END_HYD:
    while (ptmp->next != NULL)
      ptmp = ptmp->next;
    break;
  default:
    LP_error(flog, "libhyd%4.2f : Unknown browse type indicator in file %s, function %s at line %d.\n", NVERSION_HYD, __FILE__, __func__, __LINE__);
    break;
  }
  return ptmp;
}

/**\fn HYD_riverside_inflow(double)
 *\brief Select element face (ONE,TWO) according to the transversal_position
         value of an inflow
 *\return int 'ONE' 'TWO'
 */
int HYD_riverside_inflow(double transversal_position) {
  int side;

  if (transversal_position <= 0.5)
    side = ONE;
  else
    side = TWO;

  return side;
}

/**\fn HYD_display_inflows_time_series(s_inflow_hyd*, int, FILE*)
 *\brief Displays all input values series for all declared variables (Q, C[], T) for one inflow,
         at the reach scale.
 *\return -
 */
void HYD_display_inflows_time_series(s_inflow_hyd *pinf, int nb_species, FILE *flog) {
  int ivar, nspe;
  s_ft *ts_discharge, *ts_temperature;
  s_ft **ts_concentration;
  int inflow_type = pinf->type;

  switch (inflow_type) {
  case DIFFUSE_INFLOW: {
    ts_discharge = pinf->discharge;
    ts_concentration = pinf->concentration;
    ts_temperature = pinf->temperature;
    LP_printf(flog, "Length of diffuse inflow : %f [m]\n", pinf->diff_inflow->dx);
    break;
  }
  default: {
    s_pt_inflow_hyd *pt_inflow = pinf->pt_inflow;
    ts_discharge = pt_inflow->discharge;
    ts_concentration = pt_inflow->concentration;
    ts_temperature = pt_inflow->temperature;
  }
  }

  for (ivar = 0; ivar < NINFLOW_VARS_HYD; ivar++) {
    switch (ivar) {
    case DISCHARGE_INFLOW_HYD: {
      if (ts_discharge != NULL) {
        LP_printf(flog, "Diffuse inflow variable type : %s. Input time serie : \n", HYD_inflow_variable_name(ivar));
        TS_print_ts(ts_discharge, flog);
      }
      break;
    }
    case CONCENTRATION_INFLOW_HYD: {
      if (ts_concentration != NULL) {
        for (nspe = 0; nspe < nb_species; nspe++) {
          if (ts_concentration[nspe] != NULL) {
            LP_printf(flog, "Diffuse inflow variable type : %s. Specie intern ID : %d. Time values : \n", HYD_inflow_variable_name(ivar), nspe);
            TS_print_ts(ts_concentration[nspe], flog);
          }
        }
      }
      break;
    }
    case TEMPERATURE_INFLOW_HYD: {
      if (ts_temperature != NULL) {
        LP_printf(flog, "Diffuse inflow variable type : %s. Input time serie : \n", HYD_inflow_variable_name(ivar));
        TS_print_ts(ts_temperature, flog);
      }
      break;
    }
    }
  }
}

/**\fn HYD_display_inflows_properties(s_chyd*, int, FILE*)
 *\brief Browse all river reaches and elements and displays inflow properties when found
 to make sure they have been stored, located and attributed properly to the hydraulic network.
 *\return -
 */
void HYD_display_inflows_properties(s_chyd *pchyd, int nb_species, FILE *flog) {
  int r, ne, ninf, side, nspe;
  s_reach_hyd *preach;
  s_inflow_hyd *pinflow;
  s_pt_inflow_hyd *pt_inflow;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];
    if (preach->ninflows > 0) {
      for (ninf = 0; ninf < preach->ninflows; ninf++) {
        pinflow = preach->p_inflows[ninf];
        side = HYD_riverside_inflow(pinflow->transversal_position);
        LP_printf(flog, "Inflow name : %s, Type : %s, Reach GIS id : %d, Bank : %s, Distance from reach upper limit : %f [m].\n", pinflow->name, HYD_name_inflow(pinflow->type), pinflow->reach->id[GIS_HYD], HYD_riverbank_inflow(side), pinflow->x);

        HYD_display_inflows_time_series(pinflow, nb_species, flog); // Input time series for all variables
      }
    }
  }
}

/**\fn HYD_inflows_settings(s_chyd*, int, int, s_inflow_hyd*, FILE*)
 *\brief Linking procedure of the inflows to the river elements' faces.
 *\return -
 */
void HYD_inflows_settings(s_chyd *pchyd, int idebug, int nb_species, s_inflow_hyd *pinfl, FILE *flog) {
  LP_printf(flog, "Total number of inflows declared at the reach scale : %d.\n", pchyd->counter->ninflows);

  HYD_create_inflows(pinfl, pchyd, nb_species, flog); // Links to the elements faces

  if (idebug == YES) {
    LP_printf(flog, "Summary of inflows attributes at the reach scale : \n");
    HYD_display_inflows_properties(pchyd, nb_species, flog); // Verification function to make sure everything's been stored properly at the reach scale
    LP_printf(flog, "Summary of point inflows attributes at the element scale : \n");
    HYD_display_inflow_at_element_faces(pchyd, nb_species, flog); // Same at the element' face scale
  }
}

/**\fn HYD_display_inflow_at_element_faces(s_chyd*, int, FILE*)
 *\brief Display inflow attributes to make sure they've been properly attributed to the river elements' face
         For exemple, useful to check if an diffuse inflow input has been properly turned into several point inflow
 *\return -
 */
void HYD_display_inflow_at_element_faces(s_chyd *pchyd, int nb_species, FILE *flog) {
  int r, ne, idir, iface, j, ivar, nspe;
  s_element_hyd *pele;
  s_face_hyd *pface;
  s_reach_hyd *preach;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];
    for (ne = 0; ne < preach->nele; ne++) {
      pele = preach->p_ele[ne];

      // Browsing all faces for all elements
      for (iface = 0; iface < Z_HYD; iface++) {
        for (idir = 0; idir < NELEMENT_HYD; idir++) {
          if (pele->face[iface][idir]->ninflows > 0) {
            pface = pele->face[iface][idir];
            for (j = 0; j < pface->ninflows; j++) {
              LP_printf(flog, "Reach GIS ID : %d, Element GIS %d, Direction %s, Face %s :\n", preach->id[GIS_HYD], pele->id[GIS_HYD], HYD_coordinates_hyd_type(iface), HYD_riverbank_inflow(idir));

              for (ivar = 0; ivar < NINFLOW_VARS_HYD; ivar++) {
                switch (ivar) {
                case DISCHARGE_INFLOW_HYD: {
                  if (pface->pt_inflows[j]->discharge != NULL) {
                    LP_printf(flog, "Variable type : %s. Value serie :\n", HYD_inflow_variable_name(ivar));
                    TS_print_ts(pface->pt_inflows[j]->discharge, flog);
                  }
                  break;
                }
                case CONCENTRATION_INFLOW_HYD: {
                  for (nspe = 0; nspe < nb_species; nspe++) {
                    if (pface->pt_inflows[j]->concentration[nspe] != NULL) {
                      LP_printf(flog, "Variable type : %s. Specie intern id : %d. Value serie :\n", HYD_inflow_variable_name(ivar), nspe);
                      TS_print_ts(pface->pt_inflows[j]->concentration[nspe], flog);
                    }
                  }
                  break;
                }
                case TEMPERATURE_INFLOW_HYD: {
                  if (pface->pt_inflows[j]->temperature != NULL) {
                    LP_printf(flog, "Variable type : %s. Value serie :\n", HYD_inflow_variable_name(ivar));
                    TS_print_ts(pface->pt_inflows[j]->temperature, flog);
                  }
                  break;
                }
                default:
                  LP_error(flog, "libhyd%4.2f : Error in file %s, function %s at line %d : Unknown inflow variable type.\n", NVERSION_HYD, __FILE__, __func__, __LINE__);
                }
              }
            }
          }
        }
      }
    }
  }
}
