/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: usual_functions.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"
// #include "/home/lvilmin/Programmes/LIBS/lib_time_series/time_series.h"

/* Function used to fix buggs, especially in the input.y file
 * Prints the i number on the screen
 */
void HYD_test(int i) {
  if (i == CODE)
    printf("Active test %d\n", i);
  else
    printf("Passive test %d\n", i);
}

/*--------------------------------------------------------------------------*/
/*                                                                          */
/*                            void *free_p(void *p)                        */
/*                                                                          */
/* -------------------------------------------------------------------------*/
/*                                                                          */
/* OBJECT :  Desallocates the memory pointed by p and return the NULL       */
/*           pointer, that doesn't do the free() function alone             */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/* INPUT                                                                    */
/* void *p pointer to memory                                                */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/* OUTPUT                                                                   */
/* void *p NULL pointer                                                     */
/* -------------------------------------------------------------------------*/
/*                                                                          */
/*  DATE   |  AUTHOR  | CHANGES                                             */
/*--------------------------------------------------------------------------*/
/* 3/20/06 |   N.F    | Starting of HySeGeoS programmation                  */
/* 6/28/06 |   N.F    | Insertions of comments                              */
/*--------------------------------------------------------------------------*/
void *HYD_free_p(void *p) {
  if (p != NULL) {
    free(p);
    p = NULL;
  }
  return p;
}

double HYD_deg2rad(double deg) {
  double rad;

  rad = deg * PI / 180;
  return rad;
}

int HYD_get_order(double data) {
  int order = 0;
  double power;
  power = pow(10.0, order);
  while (data / power >= 10) {
    order++;
    power = pow(10.0, order);
  }
  return order;
}
