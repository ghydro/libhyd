/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: main_HYDRO.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD_full.h"
#include "global_HYD.h"
#include "ext_HYD.h"

int main(int argc, char **argv) {
  /* Date of the beginning of the simulation */
  char *date_of_simulation;
  /* Structure from time.h containing date, time, etc */
  static struct timeval current_time;
  /* Structure containing computer clock time */
  struct tm *clock_time;
  /* Current time in seconds when programm starts */
  time_t clock;
  /* Error report */
  int timi;
  /* Current time in simulation */
  double t, dt;
  /* Time of end of the simulation in s */
  double tend;

  s_mb_hyd *pmb;  // Mass balance structure containing the mass balance at t if asked for and chained with the previous mass balance pmb2
  s_mb_hyd *pmb2; // Mass balance structure containing the mass balance at t-1 if asked for
  pmb = NULL;
  pmb2 = NULL;

  /* Creates the simulation structure */
  Simul = HYD_init_simulation();

  /* Beginning of the simulation */
  Simul->clock->begin = time(NULL);

  /* Date */
  timi = gettimeofday(&current_time, NULL);
  clock = (time_t)current_time.tv_sec;
  ctime(&clock);
  clock_time = localtime(&clock);
  date_of_simulation = asctime(clock_time);

  /* Opening of command file */
  if (argc != 3)
    LP_error(stderr, "WRONG COMMAND LINE : hyd_alpha.0 CommandFileName DebugFileName\n");

  if ((Simul->poutputs = fopen(argv[2], "w")) == 0)
    LP_error(stderr, "Impossible to open the debug file %s\n", argv[2]);

  // LP_log_file_header(stderr,"HyCu",NVERSION_HYCU,date_of_simulation,argv[1]);
  // LP_log_file_header(Simul->poutputs,"HyCu",NVERSION_HYCU,date_of_simulation,argv[1]);

  // SW 03/05/2022
  LP_log_file_header(stderr, CODE_NAME_HYD, NVERSION_HYCU, date_of_simulation, argv[1]);
  LP_log_file_header(Simul->poutputs, CODE_NAME_HYD, NVERSION_HYCU, date_of_simulation, argv[1]);

  /********************************************************/
  /*    Now input files are read  --> see file input.y    */
  /********************************************************/

  CHR_begin_timer(); // LV 3/09/2012
  lecture(argv[1], Simul->poutputs);
  Simul->clock->time_spent[LEC_CHR] += CHR_end_timer(); // LV 3/09/2012

  /********************************************************/
  /*           Starting HYDRO calculations                */
  /********************************************************/

  t = Simul->chronos->t[BEGINNING];
  tend = Simul->chronos->t[END];
  dt = Simul->chronos->dt;
  CHR_calculate_simulation_date(Simul->chronos, t);

  /* Initialization of the hydraulic characteristics in the network */ // LV nov2014 déplacé ici
  CHR_begin_timer();                                                   // LV 3/09/2012
  // HYD_print_network(Simul->pchyd,Simul->poutputs); //BL to debug
  HYD_initialize_hydro(Simul->pchyd, Simul->pinout, Simul->chronos, Simul->poutputs);

  HYD_assign_river(Simul->pchyd);                        // LV nov2014
  Simul->clock->time_spent[INIT_CHR] += CHR_end_timer(); // LV 3/09/2012

  CHR_begin_timer(); // LV 3/09/2012

  Simul->pinout->fmb = HYD_create_header_mass_balance_file(Simul->poutputs);

  if (Simul->pinout->calc[PRINT_PK] == YES)
    HYD_print_pk(Simul->outputs, Simul->pinout, Simul->pchyd, Simul->poutputs);

  if (Simul->pinout->calc[MB_ELE] == YES) // LV 15/06/2012
    HYD_create_mb_at_elements_file(Simul->outputs, Simul->pinout, Simul->poutputs);

  HYD_print_outputs_formats(Simul->outputs, Simul->pinout, Simul->pchyd, Simul->poutputs);

  Simul->clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012

  // LV nov2014 : initialisation des coefs de la matrice de calcul de l'hydro
  HYD_initialize_GC(Simul->pchyd, Simul->chronos, Simul->poutputs);

  if (Simul->pchyd->settings->calc_state == STEADY) {
    if (Simul->pchyd->settings->schem_type != MUSKINGUM) {
      HYD_steady_state(dt, pmb, pmb2, Simul->pinout->fmb, Simul->pinout, Simul->outputs, Simul->pchyd, Simul->clock, Simul->chronos, Simul->poutputs); // hydraulique_permanent(dt_calc) in ProSe

      CHR_begin_timer(); // LV 3/09/2012
      HYD_print_outputs(t, Simul->outputs, Simul->pinout, Simul->pchyd, Simul->chronos, Simul->poutputs);

      Simul->clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012
    } else
      LP_error(Simul->poutputs, "No steady state for %s scheme \n check input files", HYD_name_scheme(Simul->pchyd->settings->schem_type));
  }

  if (Simul->pchyd->settings->calc_state == TRANSIENT) {
    // begin_timer();//LV 3/09/2012
    // initialize_hydro();//initialise_hydrau() in ProSe
    // assign_river();//LV nov2014
    // Simul->clock->time_spent[INITIALISATION] += end_timer();//LV 3/09/2012

    CHR_begin_timer();                                                                                  // LV 3/09/2012
    HYD_print_outputs(t, Simul->outputs, Simul->pinout, Simul->pchyd, Simul->chronos, Simul->poutputs); // LV test 26/07/2012
    Simul->clock->time_spent[OUTPUTS_CHR] += CHR_end_timer();                                           // LV 3/09/2012

    while (t <= tend) {

      LP_printf(Simul->poutputs, "t = %f days\n", t / 3600 / 24);

      HYD_transient_hydraulics(&t, dt, pmb, pmb2, Simul->pinout->fmb, Simul->pchyd, Simul->clock, Simul->chronos, Simul->outputs, Simul->pinout, Simul->poutputs); // hydrau_moy(&t,&k,dt_calc) in ProSe
      CHR_begin_timer();                                                                                                                                           // LV 3/09/2012
      HYD_print_outputs(t, Simul->outputs, Simul->pinout, Simul->pchyd, Simul->chronos, Simul->poutputs);                                                          // LV test 26/07/2012
      Simul->clock->time_spent[OUTPUTS_CHR] += CHR_end_timer();                                                                                                    // LV 3/09/2012

      CHR_calculate_simulation_date(Simul->chronos, dt);
    }
  }

  // destroy_PETSc_object();//LV nov2014 suppression PetsC
  CHR_begin_timer(); // LV 3/09/2012

  pmb = HYD_print_mass_balance(pmb, t, Simul->pinout->fmb, Simul->chronos, Simul->poutputs); // NF 11/9/06
  fclose(Simul->pinout->fmb);                                                                // NF 11/9/06

  if (Simul->pinout->calc[MB_ELE] == YES) // LV 15/06/2012
    HYD_print_mb_at_elements(t, Simul->outputs, Simul->pchyd, Simul->chronos, Simul->poutputs);

  if (Simul->pinout->calc[FINAL_STATE] == YES)
    HYD_print_final_state(Simul->outputs, Simul->pinout, Simul->pchyd, Simul->poutputs);

  Simul->clock->time_spent[OUTPUTS_CHR] += CHR_end_timer(); // LV 3/09/2012

  printf("date of end of simulation : %d %s %d %f h\n", Simul->chronos->day_d, TS_name_month(Simul->chronos->month, Simul->poutputs), Simul->chronos->year[BEGINNING], Simul->chronos->hour_h);

  /********************************************************/
  /*        End, summary of the computation length        */
  /********************************************************/

  CHR_calculate_calculation_length(Simul->clock, Simul->poutputs);
  printf("Time of calculation : %f s\n", Simul->clock->time_length);

  CHR_print_times(Simul->poutputs, Simul->clock);

  fclose(Simul->poutputs);

  return 0;
}
