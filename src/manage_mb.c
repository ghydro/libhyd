/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_mb.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"

/* Structure containing the simulation characteristics */
// extern s_simul *Simul;

s_mb_hyd *HYD_init_mass_balance(void) {
  s_mb_hyd *pmb;
  pmb = new_mb();
  bzero((char *)pmb, sizeof(s_mb_hyd));
  return pmb;
}

s_mb_hyd *HYD_chain_mb(s_mb_hyd *pmb1, s_mb_hyd *pmb2) {
  if (pmb2 != NULL) {
    pmb1->next = pmb2;
    pmb2->prev = pmb1;
  }
  return pmb1;
}

void HYD_initialize_mass_balance_t(s_mb_hyd *mass_balance) {
  bzero((char *)mass_balance->mass, NTIME_HYD * sizeof(double));
  bzero((char *)mass_balance->flux, NTIME_HYD * sizeof(double));
  bzero((char *)mass_balance->stock, NSTOCK_HYD * sizeof(double));
  bzero((char *)mass_balance->error, NSTOCK_HYD * sizeof(double));
}

/****************************************************************************/
/*                                                                          */
/*             Calculation of mass balance between t-1 and t                */
/*                                                                          */
/****************************************************************************/

/*--------------------------------------------------------------------------*/
/*                                                                          */
/*                 *set_state_for_mass_balance(s_mb *,int)                  */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/* OBJECT : wrapper of the mass balance calculation between                 */
/*              t-1 (INI) and t (END)                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/* INPUT                                                                    */
/* @param s_mb *pmb current mass_balance between t-1 and t                  */
/* @param int state INI (t-1) or END (t)                                    */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/* OUTPUT                                                                   */
/* @param s_mb *pmb current mass_balance between t-1 and t                  */
/*                                                                          */
/*--------------------------------------------------------------------------*/

s_mb_hyd *HYD_set_state_for_mass_balance(s_mb_hyd *pmb, int state, s_chyd *pchyd) {
  int i, j;
  s_element_hyd *pele;
  s_reach_hyd *preach;

  for (j = 0; j < pchyd->counter->nreaches; j++) {
    preach = pchyd->p_reach[j];
    for (i = 0; i < preach->nele; i++) { // NF 11/6/06
      pele = preach->p_ele[i];
      HYD_calculate_single_mb(pele, state, pmb, pchyd); // NF 7/20/06
      pmb->mass[state] += pele->mb->mass[state];        // NF 7/20/06
    }
  }

  return pmb;
}

s_mb_hyd *HYD_calculate_single_mb(s_element_hyd *pele, int state, s_mb_hyd *pmbtot, s_chyd *pchyd) // NF 5/23/06
{

  s_mb_hyd *pmb;
  double theta;
  int j, k;
  s_face_hyd *pface;
  s_element_hyd *pele2;
  double q, q_t;   // NF 7/20/06
  double qapp = 0; // LV 22/06/2012
  // PetscInt line;
  // int line;//LV nov2014
  int i;
  pmb = pele->mb;

  switch (state) {
  case INI:
    theta = 1 - pchyd->settings->general_param[THETA];
    break;
  case END:
    theta = pchyd->settings->general_param[THETA];
    break;
  }

  pmb->mass[state] = pele->center->hydro->H[T_HYD] * pele->length * pele->center->hydro->Width;

  for (j = 0; j < NELEMENT_HYD; j++) {

    pface = pele->face[X_HYD][j];

    q = pface->hydro->Q[T_HYD] * theta; // NF 7/20/06//NF 7/21/06
    q *= pchyd->settings->normal[j];
    // pmb->flux[j] += q;//NF 8/28/06
    if (state == INI)
      pmb->flux[j] = q; // LV 14/06/2012
    else
      pmb->flux[j] += q; // LV 14/06/2012
    pmbtot->fluxtot += q;

    for (k = 0; k < NELEMENT_HYD; k++) {

      pele2 = pface->element[k];

      if (pele2 == NULL) {

        if (k == ONE)
          pmbtot->flux[UPSTREAM] += q;
        else
          pmbtot->flux[DOWNSTREAM] += q;
      }
    }
  }

  // LV 22/06/2012 : pris en compte des apports latéraux dans les mass_balance
  // line = pele->id;
  // VecGetValues(Simul->calcul_hydro->Qappnn,1,&line,&qapp);
  for (i = 0; i < NQAPP; i++)
    qapp += pele->center->hydro->qapp[i][T_HYD]; // LV nov2014
  qapp *= theta * pele->length;
  pmb->flux[UPSTREAM] += qapp;
  pmbtot->flux[UPSTREAM] += qapp;
  pmbtot->fluxtot += qapp;

  return pmb;
}

/****************************************************************************/
/*                                                                          */
/*                  Mass balance printing functions until EOF               */
/*                                                                          */
/****************************************************************************/

s_mb_hyd *HYD_print_mass_balance(s_mb_hyd *pmb, double t, FILE *fpmb, s_chronos_CHR *chronos, FILE *fp) {

  s_mb_hyd *pmb2 = NULL;
  double deltaFt, deltaM, deltaF, error_p, min_val;
  double err_phi; // NF 7/20/06

  // Going thru the mass balance structure chain
  // LP_printf(fp," printing hydraulic mass balance \n");
  if (pmb == NULL)
    // LP_error(fp," hydraulic mass balance is null\n");
    LP_warning(fp, " hydraulic mass balance is null\n"); // SW 25/01/2018
  if (pmb != NULL) {

    while (pmb->next != NULL)
      pmb = pmb->next;

    while (pmb != NULL) {
      // LP_printf(fp," calculating pmb\n");
      // difference between mass balance at t-1 and t
      deltaM = pmb->mass[END] - pmb->mass[INI]; // NF 7/27/06
      // difference between upstream and downstream fluxes
      deltaF = pmb->flux[UPSTREAM] + pmb->flux[DOWNSTREAM]; // NF 7/27/06 Because flux have a sign now face one = in (ie + even if the velocity is <0) and TWO = OUT
      deltaF *= chronos->dt;
      // deltaFt = deltaF + pmb->infilt;//NF 11/19/06 Pour memoire lorque l'on codera le fond sec
      deltaFt = deltaF;

      pmb->error[MB_HYD] = deltaM - deltaFt; // NF 7/27/06 Because flux have a sign now //NF 11/19/06

      // The minimum value between initial and  final mass and upstream and downstream is calculated. The percentage of error at a time step is calculated with this minimum value; it is thus a strong criteria
      min_val = TS_min(deltaFt, deltaM); /// NF 11/6/06//NF 11/19/06

      error_p = TS_avoid_inf(pmb->error[MB_HYD], (min_val)); /// NF 11/6/06
      error_p *= 100;

      pmb->fluxtot *= chronos->dt;
      err_phi = deltaF - pmb->fluxtot;

      // printing
      // fprintf(fpmb,"%9.2f %9.2f %9.2f %9.5f %9.2f  %9.2f %9.5f %9.5f %9.2f %9.6f %9.6f %9.5f %d\n",
      // pmb->time,pmb->mass[INI],pmb->mass[END],deltaM,pmb->flux[INI],pmb->flux[END],pmb->infilt,deltaFt,
      // pmb->fluxtot,err_phi,pmb->error,error_p,Simul->counter->nele_tot);//NF 11/17/06//NF 11/19/06 Pour memoire quand on programmera sur fond sec
      if (fpmb == NULL)
        LP_error(fp, "the mass balance file haven't been opened yet !! \n");
      // LP_printf(fp," printing\n %9.2f %9.2f %9.2f %9.5f %9.2f  %9.2f %9.5f %9.2f %9.6f %9.6f %9.5f\n",
      // pmb->time,pmb->mass[INI],pmb->mass[END],deltaM,pmb->flux[INI],pmb->flux[END],deltaFt,pmb->fluxtot,err_phi,pmb->error[MB_HYD],error_p);
      fprintf(fpmb, "%9.2f %9.2f %9.2f %9.5f %9.2f  %9.2f %9.5f %9.2f %9.6f %9.6f %9.5f\n", pmb->time, pmb->mass[INI], pmb->mass[END], deltaM, pmb->flux[INI], pmb->flux[END], deltaFt, pmb->fluxtot, err_phi, pmb->error[MB_HYD], error_p);

      if (pmb->prev != NULL) {
        pmb2 = pmb;
        pmb = pmb->prev;
        pmb->next = (s_mb_hyd *)HYD_free_p(pmb2);
      }

      // To free the allocated memory
      else
        pmb = (s_mb_hyd *)HYD_free_p(pmb);
    }
  }

  return pmb;
}

FILE *HYD_create_header_mass_balance_file(FILE *fpout) {

  char name[MAXCHAR];
  FILE *fp;

  LP_printf(fpout, "Printing hydraulic mass balance header \n");
  sprintf(name, "%s/mass_balance.txt", getenv("RESULT"));
  fp = fopen(name, "w");
  if (fp == NULL)
    LP_error(fpout, "Not possible to find %s, verify that your Output_folders exist\n", name);

  // fprintf(fp,"     time        Mi        Mo    deltaM    Phi_in   Phi_out    infilt   deltaF delta_Phi Err_Phi  Error_tot pourc_tot Nelement\n");//NF 7/20/06 Pour memoire quand on programmera sur fond sec
  fprintf(fp, "deltaF:diff flux upstream - flux aval\n delta_Phi:somme sur toutes les mailles des bilans de flux\nErr_Phi: difference entre deltaF et delta_Phi\nError_tot:deltaM-Err_Phi\npercent_tot c'est complique\n"); // NF 7/20/06

  fprintf(fp, "     time        Mi        Mo     deltaM    Phi_in    Phi_out    deltaF delta_Phi   Err_Phi  Error_tot    pourc_tot_tot\n"); // NF 7/20/06

  return fp;
}

void HYD_calc_mb_face(s_chyd *pchyd, s_element_hyd *pele, double dt, FILE *fp) {
  s_mb_hyd *pmb;
  double theta;
  int j, i;
  s_face_hyd *pface;
  double q, alpha, k;                         // NF 7/20/06
  double qapp = 0, A, A_num, denum, B, B_num; // LV 22/06/2012
  // PetscInt line;
  // int line;//LV nov2014

  pmb = pele->mb;
  theta = pchyd->settings->general_param[THETA];
#ifdef DEBUG

  LP_printf(fp, " in calc_mb_face river %s \n", pele->reach->river);
#endif
  for (j = 0; j < NELEMENT_HYD; j++) {
    pface = pele->face[X_HYD][j];
    q = pface->hydro->Q[INTERPOL_HYD] * theta * dt + (1 - theta) * pface->hydro->Q[ITER_INTERPOL_HYD] * dt;

#ifdef DEBUG
    LP_printf(fp, "face %d pface->hydro->Q[INTERPOL_HYD] %f pface->hydro->Q[ITER_INTERPOL_HYD] %f, V_%d %f\n", j + 1, pface->hydro->Q[INTERPOL_HYD], pface->hydro->Q[ITER_INTERPOL_HYD], j + 1, q); // BL to debug
#endif

    q *= pchyd->settings->normal[j];
    pmb->flux[j] += q; // LV 14/06/2012
  }

  for (i = 0; i < NQAPP; i++) {
    qapp = pele->center->hydro->qapp[i][INTERPOL_HYD] * theta * pele->length * dt + pele->center->hydro->qapp[i][ITER_INTERPOL_HYD] * (1 - theta) * pele->length * dt;
    pmb->qapp[i] = qapp;
#ifdef DEBUG

    LP_printf(fp, "%d  qapp %e qapp_iter %e qapp_vol %e   ele_length %f\n", i, pele->center->hydro->qapp[i][INTERPOL_HYD], pele->center->hydro->qapp[i][ITER_INTERPOL_HYD], qapp, pele->length); // BL to debug
#endif
  }
}

void HYD_calc_mb(s_element_hyd *pele, double dt, double theta, FILE *fp) {
  s_mb_hyd *pmb;

  int j, i;
  s_face_hyd *pface;

  pmb = pele->mb;
  pmb->mass[INI] = pmb->mass[END];
  pmb->mass[END] = 0;
  for (j = 0; j < NELEMENT_HYD; j++) {

    pmb->mass[END] += pmb->flux[j];
  }
  for (i = 0; i < NQAPP; i++) {
    pmb->mass[END] += pmb->qapp[i];
  }
  pmb->stock[MB_HYD] = (pele->center->hydro->H[INTERPOL_HYD] - pele->center->hydro->H[ITER_INTERPOL_HYD]) * pele->length * pele->center->hydro->Width;
  pmb->stock[MUSK_HYD] = HYD_calc_stock_musk(pele, dt, theta, fp);
  pmb->error[MB_HYD] = pmb->stock[MB_HYD] - pmb->mass[END];
  pmb->error[MUSK_HYD] = pmb->stock[MUSK_HYD] - pmb->mass[END];
  if (pmb->flux[DOWNSTREAM] > EPS_TS && fabs((pmb->error[MUSK_HYD] / pmb->flux[DOWNSTREAM]) * 100) > EPS_MB_HYD) // SW 25/01/2019 remplace EPS by EPS_TS
  {
    LP_printf(fp, "ERROR while calculating the hydraulic mass balance\n");
    LP_printf(fp, "qapp[RUNOFF] %e qapp[RUNOFF_ITER] %e qapp[iNR] %e qapp[iNR_ITER] %e ele_length %f\n", pele->center->hydro->qapp[RUNOFF_HYD][INTERPOL_HYD], pele->center->hydro->qapp[RUNOFF_HYD][ITER_INTERPOL_HYD], pele->center->hydro->qapp[INR_HYD][INTERPOL_HYD], pele->center->hydro->qapp[INR_HYD][ITER_INTERPOL_HYD], pele->length);
    LP_error(fp, "Vqapp_runoff %f Vqapp_iNR %f Q_in %f Q_out %f stock %f Error %f at ele hyd GIS %d ABS %d INTERN %d at reach %s\n", pmb->qapp[RUNOFF_HYD], pmb->qapp[INR_HYD], pmb->flux[UPSTREAM], pmb->flux[DOWNSTREAM], pmb->stock[MUSK_HYD], pmb->error[MUSK_HYD], pele->id[GIS_HYD], pele->id[ABS_HYD], pele->id[INTERN_HYD], pele->reach->river);
  }
}

double HYD_calc_stock_musk(s_element_hyd *pele, double dt, double theta, FILE *fp) {
  double Viter, Vt, Qiter_in, Qiter_out, Qt_in, Qt_out, qappiter, qappt;
  double stock_musk;
  double k, alpha;
  int i;
  qappiter = 0;
  qappt = 0;
  k = pele->center->hydro->pmusk->param[K];
  alpha = pele->center->hydro->pmusk->param[ALPHA];

  Qiter_in = pele->face[X_HYD][ONE]->hydro->Q[ITER_INTERPOL_HYD];
  Qiter_out = pele->face[X_HYD][TWO]->hydro->Q[ITER_INTERPOL_HYD];

  Qt_in = pele->face[X_HYD][ONE]->hydro->Q[INTERPOL_HYD];
  Qt_out = pele->face[X_HYD][TWO]->hydro->Q[INTERPOL_HYD];

  for (i = 0; i < NQAPP; i++) {
    qappiter += pele->center->hydro->qapp[i][ITER_INTERPOL_HYD] * pele->length;
    qappt += pele->center->hydro->qapp[i][INTERPOL_HYD] * pele->length;
  }
  Viter = k * (alpha * (Qiter_in + qappiter) + (1 - alpha) * Qiter_out);
  Vt = k * (alpha * (Qt_in + qappt) + (1 - alpha) * Qt_out);

  stock_musk = Vt - Viter;
#ifdef DEBUG
  if (pele->id[ABS_HYD] == 2138)
    LP_printf(fp, "river %s ele INTERN %d  ABS %d parametres musk K %f alpha %f Q0_in %f Q0_out %f Q1_in %f Q1_out %f qapp0 %e qapp2 %e V0 %f V1 %f stock %f\n", pele->reach->river, pele->id[INTERN_HYD], pele->id[ABS_HYD], k, alpha, Qiter_in, Qiter_out, Qt_in, Qt_out, qappiter, qappt, Viter, Vt, stock_musk); // BL to debug
#endif
  return stock_musk;
}

void HYD_calc_flux(s_element_hyd *pele, double dt, FILE *fp) {
  s_mb_hyd *pmb;
  int j;
  s_face_hyd *pface;

  for (j = 0; j < NELEMENT_HYD; j++) {

    pmb->flux[j] /= dt;
  }
}

void HYD_calc_output(s_chronos_CHR *chronos, s_chyd *pchyd, s_out_io *pout, int out_type, FILE *fpout) {
  int nreach, nele, r, j;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  double t, dt, t_out, theta;
  s_id_io *id_list;
  dt = chronos->dt;
  t = chronos->t[CUR_CHR];
  t_out = pout->t_out[CUR_IO];
  nreach = pchyd->counter->nreaches;
  theta = pchyd->settings->general_param[THETA];
  if (pout->type_out == ALL_IO) {
    for (r = 0; r < nreach; r++) {
      preach = pchyd->p_reach[r];
      nele = preach->nele;
      for (j = 0; j < nele; j++) {
        pele = preach->p_ele[j];
        if (out_type == MBHYD_IO) {
          HYD_calc_mb(pele, dt, theta, fpout);
          // HYD_calc_flux(pele,pout->deltat,fpout);
        }
      }
    }
  } else {
    id_list = pout->id_output;
    while (id_list != NULL) {
      pele = pchyd->p_reach[id_list->id_lay]->p_ele[id_list->id];

      if (out_type == MBHYD_IO) {
        HYD_calc_mb(pele, dt, theta, fpout);
        // HYD_calc_flux(pele,pout->deltat,fpout); // BL pas vraiment necessaire déjà dans HYD_var_file.txt
      }
    }
  }
}

// NG : 23/02/2020
void HYD_calc_output_hyperdermic(s_chronos_CHR *chronos, s_chyd *pchyd, s_out_io *pout, int out_type, FILE *fpout) {
  int nreach, nele, r, j;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  double t, dt, t_out, theta;
  s_id_io *id_list;
  dt = chronos->dt;
  t = chronos->t[CUR_CHR];
  t_out = pout->t_out[CUR_IO];
  nreach = pchyd->counter->nreaches;
  theta = pchyd->settings->general_param[THETA];

  if (pout->type_out == ALL_IO) {
    for (r = 0; r < nreach; r++) {
      preach = pchyd->p_reach[r];
      nele = preach->nele;
      for (j = 0; j < nele; j++) {
        pele = preach->p_ele[j];
        if (out_type == HDERM_MB_IO) {
          HYD_calc_mb(pele, dt, theta, fpout);
          // HYD_calc_flux(pele,pout->deltat,fpout);
        }
      }
    }
  } else {
    id_list = pout->id_output;
    while (id_list != NULL) {
      pele = pchyd->p_reach[id_list->id_lay]->p_ele[id_list->id];

      if (out_type == HDERM_MB_IO) {
        HYD_calc_mb(pele, dt, theta, fpout);
        // HYD_calc_flux(pele,pout->deltat,fpout); // BL pas vraiment necessaire déjà dans HYD_var_file.txt
      }
    }
  }
}

/**\fn void AQ_reinit_mb_face_All(s_cmsh *pcmsh,FILE *fpout)
 *\brief function to reinitialize mb at all faces
 *\return void
 *
 */
void HYD_reinit_mb_All(s_chyd *pchyd, FILE *fpout) {
  int nreach, nele, k, j;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  s_mb_hyd *pmb;

  nreach = pchyd->counter->nreaches;
  for (k = 0; k < nreach; k++) {
    preach = pchyd->p_reach[k];
    nele = preach->nele;
    for (j = 0; j < nele; j++) {
      pele = preach->p_ele[j];
      pmb = pele->mb;
      HYD_reinit_mb(pmb, fpout);
    }
  }
}

/**\fn void AQ_reinit_mb_face(s_mb_aq *pmb,FILE *fpout)
 *\brief function to reinitialize mb at one faces
 *\return void
 *
 */
void HYD_reinit_mb(s_mb_hyd *pmb, FILE *fpout) {
  int idir;

  for (idir = 0; idir < NELEMENT_HYD; idir++) {
    pmb->flux[idir] = 0;
  }
  for (idir = 0; idir < NQAPP; idir++) {
    pmb->qapp[idir] = 0;
  }
  for (idir = 0; idir < NSTOCK_HYD; idir++) {
    pmb->error[idir] = 0;
    pmb->stock[idir] = 0;
  }
}

/**\fn void void AQ_calc_mb_face_All(s_cmsh *pcmsh,s_param_aq *param,FILE *fpout)
 *\brief function to calculte mass balance at all faces
 *\return void
 *
 */
void HYD_calc_mb_All(s_chyd *pchyd, s_chronos_CHR *chronos, s_out_io **pout_all, double theta, FILE *fpout) {
  int nreach, nele, k, j;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  s_mb_hyd *pmb;
  double deltat;
  s_out_io *pout;
  double t_out, dt, t;

  if (pout_all[MBHYD_IO] != NULL) {

    pout = pout_all[MBHYD_IO];

    t = chronos->t[CUR_CHR];
    dt = chronos->dt;
    t_out = pout->t_out[CUR_IO];
    nreach = pchyd->counter->nreaches;

    for (k = 0; k < nreach; k++) {
      preach = pchyd->p_reach[k];
      nele = preach->nele;
      if (preach->limits[UPSTREAM]->nupstr_reaches == 0) {

        preach->p_ele[0]->face[X_HYD][UPSTREAM]->hydro->Q[INTERPOL_HYD] = 0;
        preach->p_ele[0]->face[X_HYD][UPSTREAM]->hydro->Q[ITER_INTERPOL_HYD] = 0;
      }
      for (j = 0; j < nele; j++) {
        pele = preach->p_ele[j];
        if (t_out <= t) {
          // if(pout->t_out[H][INI]<=(param->t_calc[INI]-param->deltat[INI]))
          if (t_out < t && t_out > (t - dt)) {
            dt = t - t_out;
            HYD_calc_mb_face(pchyd, pele, dt, fpout);

          }

          else
            HYD_calc_mb_face(pchyd, pele, dt, fpout);

        } else // si t_out > t on calcule les mb pour addition
        {

          HYD_calc_mb_face(pchyd, pele, dt, fpout);
        }
      }
    }
  }
}

// NG : 23/02/2020 : Fonction qu'il faudra merger avec HYD_calc_mb_All en passant le param MB_HYD/MB_HDERM en argument
void HYD_calc_mb_All_hyperdermic(s_chyd *pchyd, s_chronos_CHR *chronos, s_out_io **pout_all, double theta, FILE *fpout) {
  int nreach, nele, k, j;
  s_reach_hyd *preach;
  s_element_hyd *pele;
  s_mb_hyd *pmb;
  double deltat;
  s_out_io *pout;
  double t_out, dt, t;

  if (pout_all[HDERM_MB_IO] != NULL) {

    pout = pout_all[HDERM_MB_IO];

    t = chronos->t[CUR_CHR];
    dt = chronos->dt;
    t_out = pout->t_out[CUR_IO];
    nreach = pchyd->counter->nreaches;

    for (k = 0; k < nreach; k++) {
      preach = pchyd->p_reach[k];
      nele = preach->nele;
      if (preach->limits[UPSTREAM]->nupstr_reaches == 0) {

        preach->p_ele[0]->face[X_HYD][UPSTREAM]->hydro->Q[INTERPOL_HYD] = 0;
        preach->p_ele[0]->face[X_HYD][UPSTREAM]->hydro->Q[ITER_INTERPOL_HYD] = 0;
      }
      for (j = 0; j < nele; j++) {
        pele = preach->p_ele[j];
        if (t_out <= t) {
          // if(pout->t_out[H][INI]<=(param->t_calc[INI]-param->deltat[INI]))
          if (t_out < t && t_out > (t - dt)) {
            dt = t - t_out;
            HYD_calc_mb_face(pchyd, pele, dt, fpout);

          }

          else
            HYD_calc_mb_face(pchyd, pele, dt, fpout);

        } else // si t_out > t on calcule les mb pour addition
        {

          HYD_calc_mb_face(pchyd, pele, dt, fpout);
        }
      }
    }
  }
}

double HYD_calc_stock_musk_pic(s_element_hyd *pele, double theta, double qinr, FILE *fp) {
  double Viter, Vt, Qiter_in, Qiter_out, Qt_in, Qt_out, qappiter, qappt;
  double stock_musk;
  double k, alpha;
  int i;
  qappiter = 0;
  qappt = 0;
  k = pele->center->hydro->pmusk->param[K];
  alpha = pele->center->hydro->pmusk->param[ALPHA];

  // Qiter_in=pele->face[X_HYD][ONE]->hydro->Q[ITER_HYD];
  // Qiter_out=pele->face[X_HYD][TWO]->hydro->Q[ITER_HYD];

  // Qt_in=pele->face[X_HYD][ONE]->hydro->Q[T_HYD];
  // Qt_out=pele->face[X_HYD][TWO]->hydro->Q[T_HYD];
  Qiter_in = pele->face[X_HYD][ONE]->hydro->Q[T_HYD];
  Qiter_out = pele->face[X_HYD][TWO]->hydro->Q[T_HYD];

  Qt_in = pele->face[X_HYD][ONE]->hydro->Q[PIC_HYD];
  Qt_out = pele->face[X_HYD][TWO]->hydro->Q[PIC_HYD];

  for (i = 0; i < NQAPP; i++) {
    qappiter += pele->center->hydro->qapp[i][ITER_HYD] * pele->length;
  }
  qappt = pele->center->hydro->qapp[RUNOFF_HYD][T_HYD] * pele->length + qinr;
  Viter = k * (alpha * (Qiter_in + qappiter) + (1 - alpha) * Qiter_out);
  Vt = k * (alpha * (Qt_in + qappt) + (1 - alpha) * Qt_out);

  stock_musk = Vt - Viter;

  return stock_musk;
}
