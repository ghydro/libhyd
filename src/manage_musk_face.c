/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_musk_face.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"
#ifdef OMP
#include "omp.h"
#endif
/* Function used to rewind a chain of faces */
s_face_hyd *HYD_free_chained_faces(s_face_hyd *pface, FILE *fp) {
  s_face_hyd *pface_temp;

  // pface=HYD_rewind_faces(pface);
  pface = HYD_rewind_faces(pface);
  if (pface != NULL) {
    while (pface->next != NULL) {
      pface_temp = pface->next;
      pface = HYD_free_a_face(pface, fp);
      pface = pface_temp;
    }
    pface = HYD_free_a_face(pface, fp);

    if (pface != NULL)
      LP_warning(fp, "libhyd faces not properly deallocated\n");
  }
  return pface;
}
/* free all the structurs of a face WARNING singularities and element not freed because they haven't been allocated in the face structur !!! */
s_face_hyd *HYD_free_a_face(s_face_hyd *pface, FILE *fp) {
  int p;

  if (pface->geometry != NULL) {
    if (pface->geometry->p_pointsAbscZ != NULL) {
      for (p = 0; p < pface->geometry->npts; p++) {
        pface->geometry->p_pointsAbscZ[p] = (s_pointAbscZ_hyd *)HYD_free_p(pface->geometry->p_pointsAbscZ[p]);
      }
      pface->geometry->p_pointsAbscZ = (s_pointAbscZ_hyd **)HYD_free_p(pface->geometry->p_pointsAbscZ);
    }
    if (pface->geometry->p_pointsXYZ != NULL) {
      for (p = 0; p < pface->geometry->npts; p++) {
        pface->geometry->p_pointsXYZ[p] = (s_pointXYZ_hyd *)HYD_free_p(pface->geometry->p_pointsXYZ[p]);
      }
      pface->geometry->p_pointsXYZ = (s_pointXYZ_hyd **)HYD_free_p(pface->geometry->p_pointsXYZ);
    }
    pface->geometry = (s_geometry_hyd *)HYD_free_p(pface->geometry);
  }
  if (pface->description != NULL) {
    pface->description = (s_description_hyd *)HYD_free_p(pface->description);
  }
  if (pface->pt_inflows != NULL) {
    for (p = 0; p < pface->ninflows; p++) {

      pface->pt_inflows[p]->discharge = TS_free_ts(pface->pt_inflows[p]->discharge, fp);
      pface->pt_inflows[p] = (s_pt_inflow_hyd *)HYD_free_p(pface->pt_inflows[p]);
    }
    pface->pt_inflows = (s_pt_inflow_hyd **)HYD_free_p(pface->pt_inflows);
  }
  if (pface->BC_char != NULL) {
    for (p = 0; p < pface->nworks; p++) {
      pface->BC_char[p]->fion = TS_free_ts(pface->BC_char[p]->fion, fp);
      pface->BC_char[p] = (s_BC_char_hyd *)HYD_free_p(pface->BC_char[p]);
    }
    pface->BC_char = (s_BC_char_hyd **)HYD_free_p(pface->BC_char);
  }
  if (pface->id_gw != NULL) {
    pface->id_gw = IO_free_id_serie(pface->id_gw, fp);
  }
  pface = (s_face_hyd *)HYD_free_p(pface);

  return pface;
}

s_face_hyd **HYD_free_tab_faces(s_face_hyd **p_face, int nfaces, FILE *fp) {
  int i;
  for (i = 0; i < nfaces; i++) {
    p_face[i] = HYD_free_a_face(p_face[i], fp);
  }
  p_face = (s_face_hyd **)HYD_free_p(p_face);

  if (p_face != NULL)
    LP_warning(fp, "libhyd faces not properly deallocated\n");
  return p_face;
}

/* creer une table de face selon les reach de pface_tot*/
s_face_hyd **HYD_create_tab_faces(s_face_hyd *pface_tot, int nreaches, s_chyd *pchyd, FILE *fp) {
  s_face_hyd **preach_faces;
  s_face_hyd *pf_cpy, *pftmp;
  int r;
  int id_reach;
  // LP_printf(fp,"NREACHES : %d\n",nreaches);
  preach_faces = (s_face_hyd **)malloc(nreaches * sizeof(s_face_hyd *));
  for (r = 0; r < nreaches; r++) {
    preach_faces[r] = NULL;
  }
  pftmp = HYD_rewind_faces(pface_tot);
  while (pftmp != NULL) {
    if (pftmp->id_gw != NULL)
      pf_cpy = HYD_create_musk_face(pftmp->name, pftmp->id[GIS_HYD], pftmp->fnode, pftmp->tnode, pftmp->id_gw->id, pftmp->id_gw->id_lay, pftmp->geometry->dx, pftmp->description->Zbottom, pftmp->new_dx, pftmp->reach, pftmp->hyporeic_sets, pchyd, fp); // NF 27/9/2017 C'est idiot, il faut faire une copie sans les pointeurs
    else                                                                                                                                                                                                                                                  // BL for repsur MODE
      pf_cpy = HYD_create_musk_face(pftmp->name, pftmp->id[GIS_HYD], pftmp->fnode, pftmp->tnode, CODE, CODE, pftmp->geometry->dx, pftmp->description->Zbottom, pftmp->new_dx, pftmp->reach, pftmp->hyporeic_sets, pchyd, fp);
    id_reach = pftmp->reach->id[INTERN_HYD];
    // LP_printf(fp,"Adding face of reach %s INTERN %d in preach_faces[%d]\n",pf_cpy->reach->limits[ONE]->name,pf_cpy->reach->id[INTERN_HYD],id_reach);
    preach_faces[id_reach] = HYD_chain_faces_fwd(preach_faces[id_reach], pf_cpy);
    pftmp = pftmp->next;
  }
  return preach_faces;
}

s_face_hyd *HYD_reorder_musk_faces(s_face_hyd *pface_tot, s_chyd *pchyd, FILE *fp) {
  // remplacer le pface_tot par le tableau de pointeur !!
  s_face_hyd *pf_cpy, *pftmp;
  s_face_hyd **preach_faces, **preach_faces_order;
  s_reach_hyd **p_reach;
  int r, id_reach;

#ifdef OMP
  s_smp *psmp;
  psmp = pchyd->settings->psmp;
#endif

  preach_faces = HYD_create_tab_faces(pface_tot, pchyd->counter->nreaches, pchyd, fp);
  /*
  for(r=0;r<pchyd->counter->nreaches;r++)
    {
      LP_printf(fp,"reach number %d \n",r);
      HYD_print_faces(preach_faces[r],fp);
      LP_printf(fp,"\n");
    }
  */ // BL to debug

  preach_faces_order = (s_face_hyd **)malloc(pchyd->counter->nreaches * sizeof(s_face_hyd *));
  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach_faces_order[r] = NULL;
  }

  p_reach = pchyd->p_reach;
//  LP_printf(fp,"entering reorder_face \n"); //BL to debug
#ifdef OMP
  int taille;
  int nthreads;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);

  psmp->chunk = PC_set_chunk_size_silent(fp, pchyd->counter->nreaches, nthreads);
  taille = psmp->chunk;
#pragma omp parallel shared(preach_faces, p_reach, pchyd, preach_faces_order, nthreads, taille) private(r)
  {
#pragma omp for schedule(dynamic, taille)
#endif
    for (r = 0; r < pchyd->counter->nreaches; r++) {
      preach_faces_order[r] = HYD_order_musk_face_in_tab(preach_faces[r], p_reach[r], pchyd, fp);
    }
#ifdef OMP
  } // end of parallel section
#endif

  pf_cpy = HYD_concatene_faces(preach_faces_order, pchyd, fp);
  for (r = 0; r < pchyd->counter->nreaches; r++) {
    //      LP_printf(fp,"reach number %d after ordering\n",r); //BL to debug
    // HYD_print_faces(preach_faces_order[r],fp); //BL to debug
    preach_faces[r] = HYD_free_chained_faces(preach_faces[r], fp);
    preach_faces_order[r] = HYD_free_chained_faces(preach_faces_order[r], fp);
    // LP_printf(fp,"\n"); // BL to debug
  }
  free(preach_faces);
  free(preach_faces_order);
  preach_faces = NULL;
  preach_faces_order = NULL;
  return pf_cpy;
}

s_face_hyd *HYD_order_musk_face_in_tab(s_face_hyd *pface_tot, s_reach_hyd *preach, s_chyd *pchyd, FILE *fp) {
  s_face_hyd *pf_cpytmp;
  s_face_hyd *pftmp, *pftmp2;
  int id_reach;
  id_reach = preach->id[ABS_HYD];
  // LP_printf(fp,"ordering faces in reach %d named %s\n",id_reach,preach->limits[ONE]->name);

  pftmp = HYD_find_first_musk_face(pface_tot, id_reach, fp);

  if (pftmp->id_gw != NULL)
    pf_cpytmp = HYD_create_musk_face(pftmp->name, pftmp->id[GIS_HYD], pftmp->fnode, pftmp->tnode, pftmp->id_gw->id, pftmp->id_gw->id_lay, pftmp->geometry->dx, pftmp->description->Zbottom, pftmp->new_dx, pftmp->reach, pftmp->hyporeic_sets, pchyd, fp);

  else // BL for repsur MODE
    pf_cpytmp = HYD_create_musk_face(pftmp->name, pftmp->id[GIS_HYD], pftmp->fnode, pftmp->tnode, CODE, CODE, pftmp->geometry->dx, pftmp->description->Zbottom, pftmp->new_dx, pftmp->reach, pftmp->hyporeic_sets, pchyd, fp);

  pf_cpytmp = HYD_order_in_reach(pface_tot, pf_cpytmp, id_reach, pchyd, fp);
  pf_cpytmp = HYD_rewind_faces(pf_cpytmp);
  // LP_printf(fp,"done\n");
  return pf_cpytmp;
}

s_face_hyd *HYD_copy_musk_faces(s_face_hyd *pface2cpy, s_chyd *pchyd, FILE *fp) {
  s_face_hyd *pftmp, *pface_cpy = NULL, *pf_cpytmp;

  pftmp = HYD_rewind_faces(pface2cpy);
  while (pftmp != NULL) {
    if (pftmp->id_gw != NULL)
      pf_cpytmp = HYD_create_musk_face(pftmp->name, pftmp->id[GIS_HYD], pftmp->fnode, pftmp->tnode, pftmp->id_gw->id, pftmp->id_gw->id_lay, pftmp->geometry->dx, pftmp->description->Zbottom, pftmp->new_dx, pftmp->reach, pftmp->hyporeic_sets, pchyd, fp);

    else // BL for repsur MODE
      pf_cpytmp = HYD_create_musk_face(pftmp->name, pftmp->id[GIS_HYD], pftmp->fnode, pftmp->tnode, CODE, CODE, pftmp->geometry->dx, pftmp->description->Zbottom, pftmp->new_dx, pftmp->reach, pftmp->hyporeic_sets, pchyd, fp);
    pface_cpy = HYD_chain_faces_fwd(pface_cpy, pf_cpytmp);
    pftmp = pftmp->next;
  }
  pface_cpy = HYD_rewind_faces(pface_cpy);
  return pface_cpy;
}

s_face_hyd *HYD_concatene_faces(s_face_hyd **preach_faces, s_chyd *pchyd, FILE *fp) {
  s_face_hyd *pf_cpy = NULL;
  s_face_hyd *pf_cpytmp, *ptmp;
  int r;
  FILE *fpout;
  char *file_name;
  int nreaches;
  nreaches = pchyd->counter->nreaches;
  file_name = (char *)malloc((strlen(getenv("RESULT")) + ALLOCSCD_LP) * sizeof(char));
  sprintf(file_name, "%s/HYD_id_gis_amont_aval.txt", getenv("RESULT"));
  fpout = fopen(file_name, "w+");
  if (fpout == NULL)
    LP_error(fp, "file %s can't be opened check if path exists", file_name);
  for (r = 0; r < nreaches; r++) {
    // LP_printf(fp,"reach nb %d nb_ele %d\n",r,HYD_count_ele_musk(preach_faces[r])); BL to debug
    // BL attention à voir si bien contraint aux deux bouts
    pf_cpytmp = HYD_copy_musk_faces(preach_faces[r], pchyd, fp);
    if (pf_cpytmp != NULL)
      fprintf(fpout, "%s %d %d ", pf_cpytmp->name, pf_cpytmp->reach->id[GIS_HYD], pf_cpytmp->id[GIS_HYD]);
    else
      LP_error(fp, "in copy chained faces, returned pointer is null for reach %d", r);
    /*
     LP_printf(fp,"\t pface_cpy TMP : \n");
     HYD_print_faces(pf_cpytmp,fp);
     */ // BL to debug
    pf_cpy = HYD_chain_faces_fwd(pf_cpy, pf_cpytmp);
    pf_cpy = HYD_browse_faces(pf_cpy, END);
    fprintf(fpout, "%d\n", pf_cpy->prev->id[GIS_HYD]);
  }
  fclose(fpout);
  free(file_name);
  return pf_cpy;
}

int HYD_count_ele_musk(s_face_hyd *pface) {
  int count = 0;
  s_face_hyd *pftmp;
  pftmp = HYD_rewind_faces(pface);
  while (pftmp != NULL) {
    count++;
    pftmp = pftmp->next;
  }
  return count;
}

s_face_hyd *HYD_create_musk_face(char *new_name, int id_gis, int fnode, int tnode, int id_gw, int id_lay, double length, double z_bot, double h_ini, s_reach_hyd *preach, double *hyporheic_set, s_chyd *pchyd, FILE *fp) {
  s_face_hyd *pface = NULL;

  s_reach_hyd *preach_bat;
  int str_len = 0;
  int i;

  str_len = STRING_LENGTH_LP;
  // LP_printf(fp,"new_name in create_musk %s str_len = %d\n",new_name,str_len); BL to debug
  pface = HYD_create_face(X_HYD, pchyd);
  pface->id[GIS_HYD] = id_gis;

  if (pface->name != NULL) {
    free(pface->name);
    LP_error(fp, "error while creating face %s\n", new_name);
  }

  pface->name = (char *)malloc(str_len * sizeof(char));

  if (pface->name == NULL)
    LP_error(fp, "bad memory allocation in create_musk_face\n");
  sprintf(pface->name, "%s", new_name);
  // LP_printf(fp,"face name %s \n",pface->name); //BL to debug
  free(new_name);
  // LP_printf(fp,"new_name freed\n"); // BL to debug

  pface->def = RAW_SECTION;
  pface->reach = preach;
  pface->fnode = fnode;
  pface->tnode = tnode;

  if (fabs(pface->fnode - pface->tnode) < EPS_HYD && fabs(pface->fnode - CODE) > EPS_HYD)
    LP_error(fp, "error at face %s fnode and tnode are equal : fnode %d tnode %d\n", pface->name, fnode, tnode);
  if (id_lay != CODE && id_gw != CODE)
    pface->id_gw = IO_create_id(id_lay, id_gw);

  if (fabs(z_bot - CODE_HYD) > EPS_HYD)
    pface->description->Zbottom = z_bot;
  else
    pface->description->Zbottom = CODE_HYD;
  if (h_ini > 0)
    pface->new_dx = h_ini; // BL range ici temporairement le h_ini pour calcul geometry
  else
    pface->new_dx = 0;
  pface->geometry = HYD_create_geometry();
  // LP_printf(fp,"\t --> The cross-section %s in reach %s %s %d has been read\n", pface->name,preach->limits[ONE]->name,preach->limits[TWO]->name,preach->branch_nb); //BL to debug

  pface->geometry->dx = length;
  if (hyporheic_set != NULL) {
    for (i = 0; i < NS_HYPO; i++)
      pface->hyporeic_sets[i] = hyporheic_set[i];
  }
  return pface;
}

s_face_hyd *HYD_find_first_musk_face(s_face_hyd *pface_tot, int id_reach, FILE *fp) {
  s_face_hyd *pftmp, *pftmp2;
  int fnode, nb_fnode = 0;

  pftmp = HYD_rewind_faces(pface_tot);
  while (pftmp != NULL) {
    if (pftmp->reach->id[ABS_HYD] == id_reach) {
      fnode = pftmp->fnode;
      // LP_printf(fp,"looking for fnode %d of face %s in reach %d\n",fnode,pftmp->name,id_reach);
      pftmp2 = HYD_rewind_faces(pface_tot);
      nb_fnode = 0;
      while (pftmp2 != NULL) {
        if (fnode == pftmp2->tnode && id_reach == pftmp2->reach->id[ABS_HYD]) {
          nb_fnode++;
          break;
        }
        pftmp2 = pftmp2->next;
      }

      if (nb_fnode == 0) {
        // LP_printf(fp,"upstream face %s reached for fnode : %d tnode : %d \n",pftmp->name,fnode,pftmp->tnode);
        break;
      }
    }
    pftmp = pftmp->next;
  }
  if (pftmp == NULL)
    LP_error(fp, "no upstream face found... quite strange isn't it ? check your input files.\n");
  return pftmp;
}

s_face_hyd *HYD_find_dwonstr_musk_face(s_face_hyd *pface_tot, int tnode, int id_reach, FILE *fp) {
  s_face_hyd *pftmp;
  int verif_fnode = CODE;
  pftmp = HYD_rewind_faces(pface_tot);
  // LP_printf(fp,"In downstr search fnode %d reach %d\n",tnode,id_reach);
  while (pftmp != NULL) {
    if (pftmp->reach->id[ABS_HYD] == id_reach) {
      verif_fnode = pftmp->fnode;
      // LP_printf(fp,"verif_fnode %d \n",verif_fnode);
    }
    if (verif_fnode == tnode) {
      break;
    }
    pftmp = pftmp->next;
  }
  // if(pftmp!=NULL)
  // pftmp=pftmp->prev;
  // LP_error(fp,"No face with fnode = %d has been found check your input files \n",tnode);
  return pftmp;
}

void HYD_print_faces(s_face_hyd *pface_tot, FILE *fp) {
  s_face_hyd *pface;

  pface = HYD_rewind_faces(pface_tot);

  while (pface != NULL) {
    LP_printf(fp, "face %d named %s fnode %d tnode %d length %f \n", pface->id[ABS_HYD], pface->name, pface->fnode, pface->tnode, pface->geometry->dx);
    pface = pface->next;
  }
}

s_face_hyd *HYD_order_in_reach(s_face_hyd *pface_tot, s_face_hyd *pf_cpy, int id_reach, s_chyd *pchyd, FILE *fp) {
  s_face_hyd *pftmp, *pftmp2, *pf_cpytmp;
  char *new_name;
  pftmp2 = HYD_rewind_faces(pface_tot);
  while (pftmp2 != NULL) {
    // LP_printf(fp,"Looking for fnode %d in reach %d\n",pf_cpy->tnode,id_reach); // BL to debug
    pftmp = HYD_find_dwonstr_musk_face(pface_tot, pf_cpy->tnode, id_reach, fp);
    if (pftmp != NULL) {
      if (pftmp->id_gw != NULL)
        pf_cpytmp = HYD_create_musk_face(pftmp->name, pftmp->id[GIS_HYD], pftmp->fnode, pftmp->tnode, pftmp->id_gw->id, pftmp->id_gw->id_lay, pftmp->geometry->dx, pftmp->description->Zbottom, pftmp->new_dx, pftmp->reach, pftmp->hyporeic_sets, pchyd, fp);
      else // BL pour repsur mode
        pf_cpytmp = HYD_create_musk_face(pftmp->name, pftmp->id[GIS_HYD], pftmp->fnode, pftmp->tnode, CODE, CODE, pftmp->geometry->dx, pftmp->description->Zbottom, pftmp->new_dx, pftmp->reach, pftmp->hyporeic_sets, pchyd, fp);
      pf_cpy = HYD_chain_faces_fwd(pf_cpy, pf_cpytmp);
    } else {
      // LP_printf(fp,"\t --> hey! pftmp = NULL\n"); // BL to debug
      new_name = (char *)calloc(strlen(pf_cpy->name) + 5, sizeof(char));
      sprintf(new_name, "%s_bis", pf_cpy->name);
      if (pf_cpy->id_gw != NULL)
        pf_cpytmp = HYD_create_musk_face(new_name, CODE, CODE, CODE, pf_cpy->id_gw->id, pf_cpy->id_gw->id_lay, 0, pf_cpy->description->Zbottom, pf_cpy->new_dx, pf_cpy->reach, pf_cpy->hyporeic_sets, pchyd, fp);
      else // BL pour repsur mode
        pf_cpytmp = HYD_create_musk_face(new_name, CODE, CODE, CODE, CODE, CODE, 0, pf_cpy->description->Zbottom, pf_cpy->new_dx, pf_cpy->reach, pf_cpy->hyporeic_sets, pchyd, fp);
      pf_cpy->reach->nfaces++;
      pf_cpy = HYD_chain_faces_fwd(pf_cpy, pf_cpytmp);

      //	  free(new_name);
      break;
    }
    pftmp2 = pftmp2->next;
  }
  return pf_cpy;
}
