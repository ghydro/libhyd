/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: HyCu_Flumy.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <malloc.h>
#include <sys/time.h>
#include "param_HYDRO.h"
#include "struct_HYDRO.h"
#include "global_var.h"
#include "functions.h"
#include "petscksp.h"

/* Parameters for the coupling with FLumy*/

// enum flumy_carac_pts {FLUMY_X,FLUMY_Y,FLUMY_Z,FLUMY_ALPHA,FLUMY_L,FLUMY_KAPPA,NFLUMY_CARACS_PTS};
enum flumy_carac_pts { FLUMY_X, FLUMY_Y, FLUMY_Z, FLUMY_L, NFLUMY_CARACS_PTS };
enum flumy_outputs { FLUMY_H, FLUMY_U, NFLUMY_OUTPUTS };
// enum flumy_singtypes {FLUMY_UP = NSINGULARITIES,FLUMY_DOWN,NFLUMY_SING};
// #define FLUMY_DT 300. //5 minutes
#define FLUMY_DT 600.       // 5 minutes
#define FLUMY_UNIT_X 1.     //[m]
#define FLUMY_UNIT_Y 1.     //[m]
#define FLUMY_UNIT_Z 1.     //[m]
#define FLUMY_UNIT_L 1.     //[m]
#define FLUMY_UNIT_KAPPA 1. //[1/m]

#define FLUMY_KAPPA 0.

extern s_simul *Simul;

// Initialisation d'une simulation HyCu-Flumy
s_simul *init_Simul_Flumy() {
  s_simul *psimul;

  psimul = new_simulation();
  bzero((char *)psimul, sizeof(s_simul));

  psimul->chronos = new_chronos();
  bzero((char *)psimul->chronos, sizeof(s_chronos));
  psimul->chronos->dt = FLUMY_DT;

  psimul->clock = new_clock();
  psimul->clock->begin = time(NULL);

  psimul->counter = new_counter();
  bzero((char *)psimul->counter, sizeof(s_counter));

  psimul->calc[ITERATIONS] = NO;
  psimul->calc[MB_ELE] = NO;

  // Paramètres de calcul
  psimul->settings = new_settings();
  bzero((char *)psimul->settings, sizeof(s_settings));
  psimul->settings->calc_curvature = GIVEN;
  psimul->settings->cs_shape = RECTANGULAR;
  psimul->settings->pk_defined = -1;
  psimul->settings->general_param[THETA] = 0.9;
  psimul->settings->general_param[DX] = CODE;
  psimul->settings->general_param[DY] = BASIC_DY;
  psimul->settings->general_param[DZ] = BASIC_DZ;
  psimul->settings->general_param[EPS_Q] = EPS_CONV;
  psimul->settings->general_param[EPS_Z] = EPS_CONV;
  psimul->settings->facttype = LU;
  psimul->settings->Flumy = YES;

  // Initialisation de la structure contenant matrices et vecteurs
  psimul->calcul_hydro = new_petsc_calc();

  return psimul;
}

// Création des faces à partir des caractéristiques des sections
void calculate_faces_Flumy(int nsections, double **carac_sections) {
  int f;
  s_face *pfaces, *pface;

  pfaces = NULL;

  for (f = 0; f < nsections; f++) {

    Simul->p_reach[0]->nfaces++;

    pface = create_face(X);
    pface->def = RAW_SECTION;
    pface->reach = Simul->p_reach[0];
    pface->geometry = create_geometry();
    pface->description->xc = carac_sections[f][FLUMY_X] * FLUMY_UNIT_X;
    pface->description->yc = carac_sections[f][FLUMY_Y] * FLUMY_UNIT_Y;
    pface->description->Zbottom = carac_sections[f][FLUMY_Z] * FLUMY_UNIT_Z;
    pface->description->l = carac_sections[f][FLUMY_L] * FLUMY_UNIT_L;

    // Nf Pb pour la définition de la courbure. Rajouter la fonction qui la calcule
    // pface->description->curvature = carac_sections[f][FLUMY_KAPPA] * FLUMY_UNIT_KAPPA;
    pface->description->curvature = FLUMY_KAPPA;

    pfaces = chain_faces(pfaces, pface);
    if (pfaces->next != NULL)
      pfaces = pfaces->next;
  }

  calculate_faces_traj(pfaces);
  create_faces(pfaces);
}

// Initialisation des structures hydro des faces et éléments
void init_hydro_structures_Flumy() {
  int f, e;

  for (f = 0; f < Simul->counter->nfaces_tot; f++) {

    Simul->p_reach[0]->p_faces[f]->hydro = new_hydraulics();
    bzero((char *)Simul->p_reach[0]->p_faces[f]->hydro, sizeof(s_hydro));
    Simul->p_reach[0]->p_faces[f]->hydro->ks = Simul->p_reach[0]->strickler->ft;
  }

  for (e = 0; e < Simul->counter->nele_tot; e++) {

    Simul->p_reach[0]->p_ele[e]->center->hydro = new_hydraulics();
    bzero((char *)Simul->p_reach[0]->p_ele[e]->center->hydro, sizeof(s_hydro));
    Simul->p_reach[0]->p_ele[e]->center->hydro->ks = Simul->p_reach[0]->strickler->ft;
  }
}

// Calcul de la hauteur d'eau avale avec la formule de Manning simplifiée
double h_manning(int nsections, double Q_up, double Ks) {
  double I, L, h;

  // Calcul de la pente à l'aval
  I = Simul->p_reach[0]->p_faces[nsections - 2]->description->Zbottom - Simul->p_reach[0]->p_faces[nsections - 1]->description->Zbottom;
  I /= Simul->p_reach[0]->p_faces[nsections - 1]->pk - Simul->p_reach[0]->p_faces[nsections - 2]->pk;

  // Largeur de la dernière section
  L = Simul->p_reach[0]->p_faces[nsections - 1]->description->l;

  h = pow(Q_up / (Ks * L * sqrt(I)), 3. / 5.);

  return h;
}

// Initialisation des structures décrivant le bief et sa géométrie
void init_geom_Flumy(int nsections, double **carac_sections, double Q_up, double Ks) {
  s_singularity *psing_up, *psing_down;
  char *name_up, *name_down;
  char *name_Q;
  s_reach *preach;
  s_inflow *pinflow;
  s_BC_char *fBC_char;

  // Création des singularités
  name_up = strdup("UP");
  name_down = strdup("DOWN");
  // Simul->p_sing = new_p_sing();
  psing_up = create_singularity(name_up);
  // psing_up->type = FLUMY_UP;
  psing_up->type = DISCHARGE;
  psing_up->pk = 0.;
  psing_down = create_singularity(name_down);
  // psing_down->type = FLUMY_DOWN;
  psing_down->type = WATER_LEVEL;
  psing_up->next = psing_down;
  psing_down->prev = psing_up;
  create_singularities(psing_up);

  // Création du  bief
  Simul->p_reach = new_p_reach();
  preach = create_reach(psing_up->name, psing_down->name, 1);
  preach->strickler = TS_create_function(0., Ks);
  preach->Q_init = Q_up;
  create_reaches(preach);

  // Création des faces à partir des cractéristiques données
  calculate_faces_Flumy(nsections, carac_sections);

  // Création des éléments
  create_elements();

  // Création des apports
  name_Q = strdup("Q_up");
  pinflow = create_inflow(name_Q);
  pinflow->reach = Simul->p_reach[0];
  pinflow->type = UPSTREAM_INFLOW;
  pinflow->pt_inflow = new_pt_inflow();
  pinflow->pt_inflow->name = pinflow->name;
  pinflow->x = 0.;
  pinflow->pt_inflow->discharge = TS_create_function(0., Q_up);
  preach->ninflows = 1;
  create_inflows(pinflow);

  // Initialisation structure hydro des faces et éléments
  init_hydro_structures_Flumy();

  // Calcul des pk
  calculate_pk_sing();
  calculate_pk_faces_centers();

  // Calcul de la condition limite aval : Manning simplifié
  fBC_char = new_BC_char();
  fBC_char->fion = TS_create_function(0., Simul->p_reach[0]->p_faces[nsections - 1]->description->Zbottom + h_manning(nsections, Q_up, Ks));
  Simul->p_reach[0]->limits[DOWNSTREAM]->BC_char = (s_BC_char **)calloc(1, sizeof(s_BC_char *));
  Simul->p_reach[0]->limits[DOWNSTREAM]->BC_char[0] = fBC_char;
  BC_faces();
}

// Transforme la table de CI donnée en entrée en structures f_t
void init_hydro_Flumy(double **inputs, double Q_up) {
  int i;

  Simul->init_from_file[IZ] = Simul->init_from_file[IQ] = YES;

  Simul->init_Z = new_function();
  Simul->init_Z->prev = NULL;
  for (i = 0; i < Simul->counter->nele_tot; i++) {

    Simul->init_Z->t = (double)i;

    if (i == 0)
      Simul->init_Z->ft = inputs[i][FLUMY_H] + Simul->p_reach[0]->p_ele[i]->face[X][UPSTREAM]->description->Zbottom;

    else if (i == Simul->counter->nele_tot - 1)
      Simul->init_Z->ft = inputs[i + 1][FLUMY_H] + Simul->p_reach[0]->p_ele[i]->face[X][DOWNSTREAM]->description->Zbottom;

    else {
      Simul->init_Z->t = (double)i;
      Simul->init_Z->ft = 0.5 * (Simul->p_reach[0]->p_ele[i]->face[X][UPSTREAM]->description->Zbottom + inputs[i][FLUMY_H] + Simul->p_reach[0]->p_ele[i]->face[X][DOWNSTREAM]->description->Zbottom + inputs[i + 1][FLUMY_H]);
    }

    if (i < Simul->counter->nele_tot - 1) {
      Simul->init_Z->next = new_function();
      Simul->init_Z->next->prev = Simul->init_Z;
      Simul->init_Z = Simul->init_Z->next;
    } else
      Simul->init_Z->next = NULL;
  }

  Simul->init_Q = new_function();
  Simul->init_Q->prev = NULL;
  for (i = 0; i < Simul->counter->nfaces_tot; i++) {
    Simul->init_Q->t = (double)i;
    Simul->init_Q->ft = Q_up;

    if (i < Simul->counter->nfaces_tot - 1) {
      Simul->init_Q->next = new_function();
      Simul->init_Q->next->prev = Simul->init_Q;
      Simul->init_Q = Simul->init_Q->next;
    } else
      Simul->init_Q->next = NULL;
  }
}

// Remplissage du tableau de sortie
void fill_outputs_Flumy(double **outputs) {
  int f;
  s_hydro *phyd;

  for (f = 0; f < Simul->counter->nfaces_tot; f++) {

    phyd = Simul->p_reach[0]->p_faces[f]->hydro;
    outputs[f][FLUMY_H] = phyd->H;
    outputs[f][FLUMY_U] = phyd->Vel;
  }
}

// Calcul de l'hydraulique permanent
void start_HyCu_Flumy(int nsections, double **carac_sections, double Q_up, double Ks, double **in_outputs) {
  int i, s;
  char name[1000];

  printf("Steady-state hydraulic calculation with HyCu...\n");
  printf("nsections: %d\nQ_up: %f\nStrickler: %f\n", nsections, Q_up, Ks);
  printf("sections' characteristics:\n");
  for (i = 0; i < nsections; i++)
    printf("%d %f %f %f %f\n", i, carac_sections[i][0], carac_sections[i][1], carac_sections[i][2], carac_sections[i][3]);

  Simul = init_Simul_Flumy();
  // sprintf(name,"apoub.log");
  // Simul->poutputs=fopen(name,"w");

  init_geom_Flumy(nsections, carac_sections, Q_up, Ks);
  init_hydro_Flumy(in_outputs, Q_up);

  printf("singularités: nom face_am face_av type\n");
  for (s = 0; s < Simul->counter->nsing; s++) {
    printf("%s ", Simul->p_sing[s]->name);
    if (Simul->p_sing[s]->faces[UPSTREAM][0] != NULL)
      printf("%d ", Simul->p_sing[s]->faces[UPSTREAM][0]->id);
    else
      printf("no_face ");
    if (Simul->p_sing[s]->faces[DOWNSTREAM][0] != NULL)
      printf("%d ", Simul->p_sing[s]->faces[DOWNSTREAM][0]->id);
    else
      printf("no_face ");
    printf("%s\n", name_sing_type(Simul->p_sing[s]->type));
  }

  printf("CL_amont debit: %f\n", Simul->p_sing[0]->faces[DOWNSTREAM][0]->pt_inflows[0]->discharge->ft);
  printf("CL_aval hauteur: %f\n", Simul->p_sing[1]->faces[UPSTREAM][0]->BC_char[0]->fion->ft);

  // exit(0);

  steady_state(FLUMY_DT, NULL, NULL, NULL);
  printf("End of steady state calculation\n");

  destroy_PETSc_object();
  fill_outputs_Flumy(in_outputs);

  calculate_calculation_length(Simul->clock);
  printf("Time of calculation : %f s\n", Simul->clock->time_length);

  // fclose(Simul->poutputs);
}
