/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: struct_simul_HYDRO.h
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

typedef struct simulation s_simul;

/* Simulation structure */
struct simulation {
  /* Name of the simulation */
  char *name;
  /* Time characteristics */
  s_chronos_CHR *chronos;
  /* Timer */
  s_clock_CHR *clock;

  /*structur of hydraulic characteristics*/
  s_chyd *pchyd;

  /* Debug file */
  FILE *poutputs;

  /*output structur for hydraulics */
  s_output_hyd **outputs[NOUTPUTS];
  /*settings for input and outputs */
  s_inout_set_io *pinout;
  s_lec_tmp_hyd **plec;

  /*Simulation output */ // SW 24/12/2018
  s_out_io *pout;
};
