/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: struct_HYDRO.h
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/********************************************************/
/*               Structures used in HYDRO               */
/********************************************************/

#include <time.h>
// #include <libprint.h>
// #include <time_series.h>
// #include "GC.h"
// #include "IO.h"
// #include <petscksp.h>
// #include "time_series.h"

typedef struct settings s_settings_hyd;
typedef struct counter s_counter_hyd;
typedef struct reach s_reach_hyd;
typedef struct element s_element_hyd;
typedef struct element_center s_center_hyd;
typedef struct face s_face_hyd;
typedef struct singularity s_singularity_hyd;
typedef struct BC_characteristics s_BC_char_hyd;
typedef struct hydraulics s_hydro_hyd;
typedef struct inflow s_inflow_hyd;
typedef struct pt_inflow s_pt_inflow_hyd;
typedef struct diffuse_inflow s_diffuse_inflow_hyd;
typedef struct description s_description_hyd;
typedef struct geometry s_geometry_hyd;
typedef struct pointAbscZ s_pointAbscZ_hyd;
typedef struct pointXYZ s_pointXYZ_hyd;
// typedef struct petsc_calc s_petsc_calc;
typedef struct output s_output_hyd;
typedef struct ts_pk s_ts_pk_hyd;
typedef struct lp_pk s_lp_pk_hyd;
typedef struct mass_balance s_mb_hyd;
typedef struct musk_set s_musk_set_hyd;
typedef struct charac_hydro s_chyd;
typedef struct temp_lec s_lec_tmp_hyd;

struct temp_lec {
  /*name of the reach*/
  char *name;
  /*GIS id of the reach*/
  int id_gis;
  /*From node of reach */
  int Fnode;
  /*To node of reach */
  int Tnode;
  /* Altitude of Fnode of reach */
  double Alt_Fnode;
  /* Altitude of Tnode of reach */
  double Alt_Tnode;
  /*strahler order of node */
  int strahler;
  /* lenght of reach */
  double length;
  /*Watershed Area */
  double Wed_Area;
  /* k parameter of muskingum*/
  double k_musk;
  /* alpha parameter of muskingum */
  double alpha;
  /*width of the reach */
  double width;
  /* strickler of the reach */
  double strickler;
  /* Qlim over the reach */
  double Qlim;
  /* Mean slope of the watershed */
  double slope;
};

struct charac_hydro {
  /* Counters */
  s_counter_hyd *counter;
  /* Simulation settings */
  s_settings_hyd *settings;
  /* Reaches of the hydrographic network */
  s_reach_hyd **p_reach;
  /* Upstream reaches */
  s_reach_hyd **upstr_reach;
  /* Downstream reaches */
  s_reach_hyd **downstr_reach;
  /* Singularities of the hydrographic network */
  s_singularity_hyd **p_sing;
  /* Global mass balance structur (for the whole simulated domain) */
  s_mb_hyd *mass_balance;

  /* PETSc structures for the calculation of hydraulics */
  // s_petsc_calc *calcul_hydro;//LV nov2014 utiliser structure lib_gc
  s_gc *calcul_hydro; // LV nov2014
  /*  */
  double *H_out;
};

/* Structure defining the general settings of the simulation */
struct settings {
  /* Number of dimensions (1, 2 or 3) */
  int ndim;
  /* Type of calculation (steady or transient state) */
  int calc_state;
  /*solver type libgc(0) or sparse(1) default is libgc SW 11/07/2018*/
  int solver_type;
  /* Calulation of the curvature radius (yes or no) */
  int calc_curvature;
  /* Global parameters (vertical discretization, basic strickler and curvature values...) */
  double general_param[NPARAM_HYD];

  /* Number of the singularity at which the pk was defined (-1 means no pk was defined) */
  int pk_defined;
  /* Number of vertical discretuzations for the calculation of the hydro tables
   * (width, peri, surf, hydrad), calculated in table_hydro_faces()
   */
  int ndz;
  /* Shape of the cross sections (rectangular or complex) */
  int cs_shape;

  /* Schem_type in ST_VENANT,MUSKINGUM*/
  int schem_type;

  /* Initialization files for Z and U */
  s_ft *init_Z;
  s_ft *init_Q;
  /* Normal to a face =1 for ONE and =-1 for TWO.
   * Used through all the code to check if a velocity is incoming or outgoing from an element
   *///NF 8/23/06
  int normal[NELEMENT_HYD];

  int coupled;
  //  double Tc[NTC]; /* Concentration times */  //NG : 29/02/2020 : Tc is now managed by libfp as it is not declared for an entire river system anymore but at the catchment scale.
  int Kdef;
  int hCalcWay; // NF 7/3/2023 Way of calculating water depth from Manning Strickler or Muskingum
#ifdef OMP
  s_smp *psmp;
#endif
};

/* Structure containing various counters useful for the simulation */
struct counter {
  /* Total number of reaches in the network */
  int nreaches;
  /* Number of upstream reaches */
  int nupstr_limits;
  /* Number of downstream reaches */
  int ndownstr_limits;
  /* Number of defined inflows */
  int ninflows;
  /* Number of upstream inflows */
  // int nupstr_inflows;
  /* Total number of singularities in the network */
  int nsing;
  /* Total number of faces in the river network */
  int nfaces_tot;
  /* Total number of elements in the river network */
  int nele_tot;
  /* Nb of output time series asked by the user */
  int nts;
  /* Nb of output longitudinal profiles asked by the user */
  int nlp;
  /* Nb of iterations to reach steady-state */
  int niter;
  /* Nb of mass balance outputs */
  int nmb;
  // LV nov2014 : nb total de coefficients non nuls dans la matrice à inverser
  int ncoefs_tot;
};

struct mass_balance {
  double time;
  double mass[NTIME_HYD];
  // double infilt;//NF 27/3/2012 Pour memoire lorsque l'on traitera les fonds secs
  double flux[NDIRECTIONS_HYD];
  double fluxtot;
  double error[NSTOCK_HYD];
  double qapp[NQAPP];
  double stock[NSTOCK_HYD];
  s_mb_hyd *next;
  s_mb_hyd *prev;
};

/* Structure describing a river reach (between two singularities) */
struct reach { // based on <<bief>> structure in ProSe
  /* Number of the reach */
  int id[NID_HYD];
  char *river;
  /* Branch nb in the case of diffluences */
  int branch_nb;
  /* Integer defining if the reach is WET or DRY */
  int state;
  /* Integer defining wether or not the pk were already calculated within the reach */
  int passage;
  /* Length of the reach */
  double length;
  /*width of the reach */
  double width;
  /* mean slope of river bed b, z = a + bx*/
  double mean_slope;
  /* Coef multiplicatif pour calcul des pk dans le bief dans le cas de plsr branches parallèles */
  double coef;
  /* Strickler coefficient within the reach */
  // double strickler;
  s_ft *strickler;
  /* Number of inflows in the reach */
  int ninflows;
  /* Inflows in the reach */
  s_inflow_hyd **p_inflows;
  /* Singularities upstream and downstream of the reach */
  s_singularity_hyd *limits[NDIRECTIONS_HYD];
  /* Number of calculation elements in the reach */
  int nele;
  /* Number of transversal faces in the reach = nele + 1 */
  int nfaces;
  /* Calculation elements in the reach */
  s_element_hyd **p_ele;
  /* Transversal faces in the reach */
  s_face_hyd **p_faces;
  /* Next reach */
  s_reach_hyd *next;
  /* Previous reach */
  s_reach_hyd *prev;
  /* Initial discharge within the reach */
  double Q_init;
  /* Muskingum settings */
  s_musk_set_hyd *pmusk;
  /*strahler order */
  int strahler;
#ifdef COUPLED
  /*link with fp */
  s_id_spa *link_fp;
/* discharge at upstream */
#endif
  double q_upstr;
  /* maximal discharge from river to aquifer*/
  double Qlim;
  /* number of stream tubes when option is activated */
  int n_tube;
};

struct musk_set {

  /*for Muskingum */
  int calc_type;

  // parameters of muskingum equation
  double param[NMUSK_PAR];
};

/* Structure defining an element of calculation */
struct element { // based on <<element>> structure in HySeGeoS
  /* Element nb */
  int id[NID_HYD];
  /* Length of the element */
  double length;
  /* Defines if the element is WET or DRY (used for open boundary) */
  int state;
  /* Center of the element */
  s_center_hyd *center;
  /* Pointer to faces : X and Y */
  s_face_hyd *face[Z_HYD][NELEMENT_HYD];
  /* River reach containing the element */
  s_reach_hyd *reach;
  /* Mass balance */
  s_mb_hyd *mb;
  /* Modified element in [YES,NO,QLIM] */
  int modif;

  /* Next element*/
  s_element_hyd *next;
  /* Previous element*/
  s_element_hyd *prev;
};

/* Structure defining the center of an element of calculation (on which is calculated the hydraulic head) */
struct element_center {
  /* pk */
  double pk;
  /* Hydraulic characteristics of the center */
  s_hydro_hyd *hydro;
  /* Description of the center of the element (curvature, abscisses of the banks...) */
  s_description_hyd *description;
  /* Element to which the center belongs */
  s_element_hyd *ele;
};

/* Structure defining the faces between elements of calculation (on which is calculated the velocity) */
struct face { // based on <<face>> in HySeGeoS
  /* Face nb */
  int id[NID_HYD];
  /* Cross-section name */
  char *name;
  /* fnode of the face */
  int fnode;
  /*tnode of the face */
  int tnode;
  /* id_gw */
  s_id_io *id_gw;
  /* pk of the face */
  double pk;
  /* Integer defining how the face was defined
   * user defined : RAW_SECTION
   * or interpolated : INTERPOLATED_FACE
   */
  int def;
  /* New length between current and next cross-sections after interpolation */
  double new_dx;
  /* Type (X or Y face) */
  int type;
  /* Defines if the face is at the front border in the case of an open boundary simulation (YES or NO) */
  int state;
  /* Geometry of the face's cross-section */
  s_geometry_hyd *geometry;
  /* Description of the face (curvature, abscisses of the banks...) */
  s_description_hyd *description;
  /* Elements on both sides of the face */
  s_element_hyd *element[NELEMENT_HYD];
  /* Point inflows arriving through the face */
  s_pt_inflow_hyd **pt_inflows;
  /* Number of inflows arriving through the face */
  int ninflows;
  /* Singularities upstream and downstream of the reach */
  s_singularity_hyd *limits[NDIRECTIONS_HYD];
  /*boundary condition at face */
  s_BC_char_hyd **BC_char;
  /* number of BC on face */
  int nworks;
  /* Hydrological component at the cross-section (face or element center) : velocities, head...*/
  s_hydro_hyd *hydro;
  /* Next face */
  s_face_hyd *next;
  /* Previous face */
  s_face_hyd *prev;
  /* Reach containing the face */
  s_reach_hyd *reach;

  // LV nov2014
  double ds; // distance entre centre d'élément amont et centre d'élément aval, CODE si à l'amont ou l'aval d'un bief
  /* hydrological parameters of the hyporeic zone needed if coupled*/
  double hyporeic_sets[NS_HYPO];
};

/* Singularities (between the last face of a reach and the first face of another one) */
struct singularity { // based on <<limite>> structure in ProSe
  /* Number of the singularity */
  int id[NID_HYD];
  /* Name of the singularity */
  char *name;
  /* Type of singularity */
  int type;
  /* Position of the singularity */
  double pk;
  double Z_init;
  /* Integer defining wether or not the pk was already calculated */
  int passage;
  /* Faces on both sides of the singularity */
  s_face_hyd **faces[NELEMENT_HYD];
  /* Number of upstream reaches */
  int nupstr_reaches;
  /* Number of downstream reaches */
  int ndownstr_reaches;
  /* Reaches on both sides of the singularity */
  s_reach_hyd **p_reach[NDIRECTIONS_HYD];
  /* Characteristics of the singularity, depending on its' type*/
  s_BC_char_hyd **BC_char;
  int nworks;
  /*FromNodeToNode of the singularity*/
  int Fnode;
  int Tnode;
  /* holler coefficient for reoxygeneration of hydraulic struct*/ // SW 23/10/2018
  double holler;
  /*formule reaeration */
  int formule_rea;
  ;
  /* Next singularity */
  s_singularity_hyd *next;
  /* Previous singularity */
  s_singularity_hyd *prev;
};

/* Characteristics of a singularity
 * One of the 5 structures is initialized depending on the type of singularity
 */
struct BC_characteristics {
  int hydwork_type;
  int fion_type;
  s_ft *fion;
  double hydwork_param[NHYDWORK_PARAM_HYD];
  s_BC_char_hyd *next;
  s_BC_char_hyd *prev;
  int position[2];
  double fion_t, fion_t_old; // LV nov2014
};

/* Inflow characteristics */
struct inflow { // based on <<apport>> in ProSe
  /* Name of the inflow */
  char *name;
  /* Type of inflow (UPSTREAM,INFLUENT,DIFFUSE or EFFLUENT) */
  int type;
  /* Reach in which the inflow arrives */
  s_reach_hyd *reach;
  /* Distance from the upstream singularity of the reach */
  double x;
  /* Transversal position of the inflox (from 0 to 1) */
  double transversal_position;
  /* Discharge */
  s_ft *discharge;
  /* Temperature */
  s_ft *temperature;
  /* Concentration (sized over several species) */
  s_ft **concentration;
  /* Point inflow characteristics in the case of a POINT_INFLOW */
  s_pt_inflow_hyd *pt_inflow;
  /* Diffuse inflow characteristics in the case of a DIFFUSE_INFLOW */
  s_diffuse_inflow_hyd *diff_inflow;
  /* Next inflow */
  s_inflow_hyd *next;
  /* Previous inflow */
  s_inflow_hyd *prev;
};

struct pt_inflow {
  /* Name of the inflow */
  char *name;
  /* Discharge */
  s_ft *discharge;
  /* Temperature */
  s_ft *temperature;
  /* Face through which the inflow arrives */
  s_face_hyd *face;
  s_ft **app_bio[NSPECIES_BIO_HYD]; // SW 05/05/2022 replace NSPECIES_RIVE by NSPECIES_BIO_HYD
  s_ft **flow_in_macrospecies;      // MH : 13/09/2021 time series to store TOC
  /* Concentration (Sized over several species) */
  s_ft **concentration;
};

struct diffuse_inflow {
  /* Reach in which the diffuse inflow arrives */
  s_reach_hyd *reach;
  /* Discharge */
  s_ft *discharge;
  /* Temperature */
  s_ft *temperature;
  /* Concentration (sized over several species) */
  s_ft **concentration;
  /* Point inflows constituting the diffuse inflow */
  s_pt_inflow_hyd **pt_inflows;
  /* Number of point inflows constituting the diffuse inflow */
  int npt_inflows;
  /* Length of the inflow (diffuse inflows) */
  double dx;
};

/* Hydraulic calculations at the center or the faces of a calculation element */
struct hydraulics { // based on <<hydraulics>> in HySeGeoS
  /* Width table */
  s_ft *width;
  /* Wet surface table */
  s_ft *surf;
  /* Wet perimeter table */
  s_ft *peri;
  /* Hydraulic radius table */
  s_ft *hydrad;
  /* Mean velocity */
  double Vel;
  /* Water height */
  double H[NSTEP_HYD];
  /* Altitude of the free surface */
  double Z[NSTEP_HYD];
  /* Discharge */
  double Q[NSTEP_HYD];
  /* Transport-variable (ie. either Concentration and/or Temperature). Array sized over several species)*/
  double *transp_var;

  /* \def Transport-parameter-riverbed analytical sol.
   * (ie. Thermal conductivity of riverbed layer (W/m/K) and riverbed thickness (m) ).
   * Array sized over riverbed parameters */
  double *param_riverbed;

  /* Wet surface */
  double Surf;
  /* Wet perimeter */
  double Peri;
  /* Free surface width */
  double Width;
  /* Difference in U and Z at the previous time step
   * useful for the steady-state calculation
   */
  double dq, dz;
  /* Strickler coefficient */
  double ks;
  /* Hydraulic radius and radius exponent 4/3 */
  double rhyd, rhyd43;
  /* Differential of the wet surface by the free surface elevation */
  double dL_dZ;

  double qapp[NQAPP][NSTEP_HYD]; // LV nov2014 apports dans un élément (somme apports aux 2 faces latérales à t-dt et t)

  // LV nov 2014 coefs de la matrice à inverser
  int ncoefs;    // LV nov 2014 nb de coefs non nuls dans le calcul matriciel
  int *id;       // LV nov 2014 identifiant des éléments/faces utilisées dans le calcul de Z/Q
  int *id_mat;   // LV nov 2014 position des coefs dans mat6 -> spécifique à libgc
  double *coefs; // LV nov 2014 valeur des coefs
  double b;      // LV nov 2014 membre de droite du système matriciel
  /*structure contenant les paramètres de muskingum*/
  s_musk_set_hyd *pmusk;
  /*Qlim value of the element*/
  double Qlim;
  // double az1, az2, az3, bz;
  // double gamma1, gamma2; //LV nov2014 pour calcul eq Benjamin
  // double gamma3, gamma4;
  // double gamma5;
};

/* Structure containing the elements of description that are necessary in the matrixes' coefficicents' calculation */
struct description {
  /* Radius of curvature */
  double radius;
  /* Curvature */
  double curvature;
  double dK_ds;
  /* Altitude of the bottom of the cross-section */
  double Zbottom;
  /* Abscisse of the left bank of the cross-section */
  double AbscLB;
  /* Abscisse of the right bank of the cross-section */
  double AbscRB;
  /* Abscisse of the bottom  of the cross-section */
  double Abscbottom;
  /* Lambert coordinates of the center of the cross-section */
  double xc, yc, l;
};

struct geometry {
  /* Type : ABSC_Z or X_Y_Z */
  int type;
  /* Description of the cross-section by abscisse-altitude points*/
  s_pointAbscZ_hyd **p_pointsAbscZ;
  /* Description of the cross-section by Lambert coordinate points*/
  s_pointXYZ_hyd **p_pointsXYZ;
  /* Number of points in the cross-section */
  int npts;
  /* Number of local minima */
  int nmin;
  /* Number of local maxima */
  int nmax;
  /* Id of the local minima in p_pointAbscZ */
  int *min;
  /* Id of the local maxima in p_pointAbscZ */
  int *max;
  /* Id of the bottom of the cross-section in p_pointAbscZ */
  int bottom;
  /* Length between current and next cross-sections */
  double dx;
};

/* Point of a cross-section defined by abscisse and altitude */
struct pointAbscZ {
  /* Abscisse */
  double absc;
  /* Altitude */
  double z;
  /* Type of point : NOTHING or MIN or MAX */
  int pt_type;
  /* Next point */
  s_pointAbscZ_hyd *next;
  /* Previous point */
  s_pointAbscZ_hyd *prev;
};

/* Point of a cross-section defined by its Lambert coordinates */
struct pointXYZ {
  /* Coordinates */
  double x;
  double y;
  double z;
  /* Next point */
  s_pointXYZ_hyd *next;
  /* Previous point */
  s_pointXYZ_hyd *prev;
};

struct output {
  /*output structure*/
  s_out_io *pout;
  /* Time series' & longitudinal profiles' description */
  int npk;
  s_ts_pk_hyd **ts_pk;
  s_lp_pk_hyd **lp_pk;
  /* Output file for final_state */
  s_file_io *fic;
  s_file_io *gnu_fic;
  /* Type of graphics */
  int graphics;
  /* Next output */
  s_output_hyd *next;
  /* Previous output */
  s_output_hyd *prev;
};

struct ts_pk {
  char *river;
  int reach_nb;
  int branch_nb;
  int pk_type;
  double pk;
  s_element_hyd *pele;
  s_ts_pk_hyd *next, *prev;
  // s_file *fic;
  FILE *pfic;
  char file_name[MAXCHAR];
};

struct lp_pk {
  char *river;
  double pk_up;
  int branch_nb_up;
  double pk_down;
  int branch_nb_down;
  int *reach_nb;
  s_element_hyd *pele;
  s_lp_pk_hyd *next, *prev;
  s_file_io *fic;
  s_file_io *gnu_fic[NVAR_HYD];
  double Volume[2]; // volumes d'eau ini et end pour mb !!!
  double Flux[2];   // volumes d'eau ini et end pour mb !!!
};

/* Functions to initialize each structure */
#define new_simulation() ((s_simul *)malloc(sizeof(s_simul)))
#define new_settings() ((s_settings_hyd *)malloc(sizeof(s_settings_hyd)))
#define new_counter() ((s_counter_hyd *)malloc(sizeof(s_counter_hyd)))
#define new_p_reach() ((s_reach_hyd **)malloc(sizeof(s_reach_hyd *)))
#define new_reach() ((s_reach_hyd *)malloc(sizeof(s_reach_hyd)))
#define new_element() ((s_element_hyd *)malloc(sizeof(s_element_hyd)))
#define new_center() ((s_center_hyd *)malloc(sizeof(s_center_hyd)))
#define new_face() ((s_face_hyd *)malloc(sizeof(s_face_hyd)))
#define new_p_sing() ((s_singularity_hyd **)malloc(sizeof(s_singularity_hyd *)))
#define new_singularity() ((s_singularity_hyd *)malloc(sizeof(s_singularity_hyd)))
#define new_BC_char() ((s_BC_char_hyd *)malloc(sizeof(s_BC_char_hyd)))
#define new_hydraulics() ((s_hydro_hyd *)malloc(sizeof(s_hydro_hyd)))
#define new_inflow() ((s_inflow_hyd *)malloc(sizeof(s_inflow_hyd)))
#define new_pt_inflow() ((s_pt_inflow_hyd *)malloc(sizeof(s_pt_inflow_hyd)))
#define new_diffuse_inflow() ((s_diffuse_inflow_hyd *)malloc(sizeof(s_diffuse_inflow_hyd)))
#define new_description() ((s_description_hyd *)malloc(sizeof(s_description_hyd)))
#define new_geometry() ((s_geometry_hyd *)malloc(sizeof(s_geometry_hyd)))
#define new_pointAbscZ() ((s_pointAbscZ_hyd *)malloc(sizeof(s_pointAbscZ_hyd)))
#define new_pointXYZ() ((s_pointXYZ_hyd *)malloc(sizeof(s_pointXYZ_hyd)))
// #define new_petsc_calc() ((s_petsc_calc *) malloc(sizeof(s_petsc_calc)))
#define new_output() ((s_output_hyd *)malloc(sizeof(s_output_hyd)))
#define new_ts_pk() ((s_ts_pk_hyd *)malloc(sizeof(s_ts_pk_hyd)))
#define new_lp_pk() ((s_lp_pk_hyd *)malloc(sizeof(s_lp_pk_hyd)))
#define new_mb() ((s_mb_hyd *)malloc(sizeof(s_mb_hyd)))
#define new_musk_set() ((s_musk_set_hyd *)malloc(sizeof(s_musk_set_hyd)))
#define new_charac_hydro() ((s_chyd *)malloc(sizeof(s_chyd)))
#define new_lec_tmp() ((s_lec_tmp_hyd *)malloc(sizeof(s_lec_tmp_hyd)))
