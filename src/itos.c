/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: itos.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/* Functions used to print the names of parameters
 * (transform an int into a string)
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"

char *HYD_name_inflow(int type) {
  char *name;

  switch (type) {
  case INFLUENT: {
    name = strdup("influent");
    break;
  }
  case EFFLUENT: {
    name = strdup("effluent");
    break;
  }
  case UPSTREAM_INFLOW: {
    name = strdup("upstream inflow");
    break;
  }
  case DIFFUSE_INFLOW: {
    name = strdup("diffuse inflow");
    break;
  }
  default: {
    name = strdup("DEFAULT, undefined inflow type in itos.c");
    break;
  }
  }
  return name;
}

char *HYD_riverbank_inflow(int bank) {
  char *name;

  switch (bank) {
  case ONE: {
    name = strdup("ONE (or LEFT BANK)");
    break;
  }
  case TWO: {
    name = strdup("TWO (or RIGHT BANK)");
    break;
  }
  default: {
    name = strdup("DEFAULT, undefined face type");
    break;
  }
  }
  return name;
}

char *HYD_inflow_variable_name(int variable) {
  char *name;

  switch (variable) {
  case DISCHARGE_INFLOW_HYD: {
    name = strdup("Discharge");
    break;
  }
  case CONCENTRATION_INFLOW_HYD: {
    name = strdup("Concentration");
    break;
  }
  case TEMPERATURE_INFLOW_HYD: {
    name = strdup("Temperature");
    break;
  }
  default: {
    name = strdup("DEFAULT, undefined inflow input variable");
    break;
  }
  }
  return name;
}

char *HYD_name_output(int type) {
  char *name;

  switch (type) {
  case TRANSV_PROFILE: {
    name = strdup("transversal profile");
    break;
  }
  case LONG_PROFILE: {
    name = strdup("longitudinal profile");
    break;
  }
  case FINAL_STATE: {
    name = strdup("final state");
    break;
  }
  default: {
    name = strdup("DEFAULT, undefined output type in itos.c");
    break;
  }
  }
  return name;
}

char *HYD_name_calc_state(int param) {
  char *name;

  switch (param) {
  case STEADY: {
    name = strdup("steady");
    break;
  }
  case TRANSIENT: {
    name = strdup("transient");
    break;
  }
  default: {
    name = strdup("DEFAULT, unknown general_param in itos.c");
    break;
  }
  }
  return name;
}

char *HYD_name_general_param(int param) {
  char *name;

  switch (param) {
  case UP_HMIN: {
    name = strdup("upstream_hmin");
    break;
  }
  case DOWN_HMAX: {
    name = strdup("downstream_hmax");
    break;
  }
  case DX: {
    name = strdup("dx");
    break;
  }
  case DY: {
    name = strdup("dy");
    break;
  }
  case DZ: {
    name = strdup("dz");
    break;
  }
  case CURVATURE: {
    name = strdup("curvature");
    break;
  }
  case STRICKLER: {
    name = strdup("global Strickler");
    break;
  }
  case THETA: {
    name = strdup("theta");
    break;
  }
  case EPS_Q: {
    name = strdup("eps_Q");
    break;
  }
  case EPS_Z: {
    name = strdup("eps_Z");
    break;
  }
  default: {
    name = strdup("DEFAULT, unknown general_param in itos.c");
    break;
  }
  }
  return name;
}

char *HYD_name_extremum(int extremum) {
  char *name;

  switch (extremum) {
  case BEGINNING: {
    name = strdup("beginning");
    break;
  }
  case END: {
    name = strdup("end");
    break;
  }
  default: {
    name = strdup("DEFAULT, unknown extremum in itos.c");
    break;
  }
  }
  return name;
}

char *HYD_name_answer(int answer) {
  char *name;

  switch (answer) {
  case YES: {
    name = strdup("YES");
    break;
  }
  case NO: {
    name = strdup("NO");
    break;
  }
  default: {
    name = strdup("DEFAULT, unknown type of answer in itos.c");
    break;
  }
  }
  return name;
}

char *HYD_name_direction(int dir) {
  char *name;

  switch (dir) {
  case UPSTREAM: {
    name = strdup("upstream");
    break;
  }
  case DOWNSTREAM: {
    name = strdup("downstream");
    break;
  }
  default: {
    name = strdup("DEFAULT, unknown direction in itos.c");
    break;
  }
  }
  return name;
}

char *HYD_name_sing_type(int sing) {
  char *name;

  switch (sing) {
  case OPEN: {
    name = strdup("open");
    break;
  }
  case CONTINU: {
    name = strdup("continu");
    break;
  }
  case DISCHARGE: {
    name = strdup("discharge");
    break;
  }
  case WATER_LEVEL: {
    name = strdup("water_level");
    break;
  }
  case HYDWORK: {
    name = strdup("hydraulic work");
    break;
  }
  case RATING_CURVE: {
    name = strdup("rating_curve");
    break;
  }
  case CONFLUENCE: {
    name = strdup("confluence");
    break;
  }
  case CONF_DIFF: {
    name = strdup("confluence-diffluence");
    break;
  }
  case TRICONFLUENCE: {
    name = strdup("triconfluence");
    break;
  }
  case DIFFLUENCE: {
    name = strdup("diffluence");
    break;
  }
  case TRIDIFFLUENCE: {
    name = strdup("tridiffluence");
    break;
  }
  default: {
    name = strdup("DEFAULT, unknown singularity type in itos.c");
    break;
  }
  }
  return name;
}

char *HYD_name_hyd_var(int var) {
  char *name;

  switch (var) {
  case ZFS_IO: {
    name = strdup("Zfs");
    break;
  }
  case H_IO: {
    name = strdup("H");
    break;
  }
  case SURF_IO: {
    name = strdup("Surf");
    break;
  }
  case PERI_IO: {
    name = strdup("Peri");
    break;
  }
  case WFS_IO: {
    name = strdup("Widthfs");
    break;
  }
  case RH_IO: {
    name = strdup("Rh");
    break;
  }
  case VEL_IO: {
    name = strdup("Vel");
    break;
  }
  case Q_IO: {
    name = strdup("Q");
    break;
  }
  default: {
    name = strdup("DEFAULT, unknown hydraulic variable in itos.c");
    break;
  }
  }
  return name;
}

char *HYD_unit_general_param(int param) {
  char *name;

  switch (param) {
  case UP_HMIN: {
    name = strdup("m");
    break;
  }
  case DOWN_HMAX: {
    name = strdup("m");
    break;
  }
  case DX: {
    name = strdup("m");
    break;
  }
  case DY: {
    name = strdup("m");
    break;
  }
  case DZ: {
    name = strdup("m");
    break;
  }
  case CURVATURE: {
    name = strdup("m");
    break;
  }
  case STRICKLER: {
    name = strdup("m^(1/3)/s");
    break;
  }
  case THETA: {
    name = strdup("");
    break;
  }
  case EPS_Q: {
    name = strdup("m^3/s");
    break;
  }
  case EPS_Z: {
    name = strdup("m");
    break;
  }
  default: {
    name = strdup("DEFAULT, unknown general_param in itos.c");
    break;
  }
  }
  return name;
}

char *HYD_name_scheme(int iname) {
  char *name;

  switch (iname) {
  case ST_VENANT: {
    name = strdup("SAINT VENANT");
    break;
  }
  case MUSKINGUM: {
    name = strdup("MUSKINGUM");
    break;
  }
  default: {
    name = strdup("DEFAULT, unknown scheme name in itos.c");
    break;
  }
  }
  return name;
}

char *HYD_id_type(int idtype) {
  char *name;

  switch (idtype) {
  case GIS_HYD: {
    name = strdup("GIS");
    break;
  }
  case ABS_HYD: {
    name = strdup("ABS");
    break;
  }
  case INTERN_HYD: {
    name = strdup("INTERN");
    break;
  }
  default: {
    name = strdup("DEFAULT, unknown id type in itos.c");
    break;
  }
  }
  return name;
}

char *HYD_coordinates_hyd_type(int idir) {
  char *name;

  switch (idir) {
  case X_HYD: {
    name = strdup("X");
    break;
  }
  case Y_HYD: {
    name = strdup("Y");
    break;
  }
  case Z_HYD: {
    name = strdup("Z");
    break;
  }
  default: {
    name = strdup("DEFAULT, coordinates type in itos.c");
    break;
  }
  }
  return name;
}

char *HYD_K_method_name(int itype, FILE *fp) {
  char *name;

  switch (itype) {
  case FORMULA: {
    name = strdup("FORMULA");
    break;
  }
  case TTRA: {
    name = strdup("TTRA");
    break;
  }
  default: {
    LP_error(fp, "In libhyd%f.2, file %s, in function %s : Unknown Kdef method.\n", NVERSION_HYD, __FILE__, __func__);
    break;
  }
  }
  return name;
}

char *HYD_river_height_method_name(int itype, FILE *fp) {
  char *name;

  switch (itype) {
  case MANNING_STRICKLER: {
    name = strdup("MANNING STRICKLER");
    break;
  }
  case MUSKINGUM_MB: {
    name = strdup("MUSKINGUM MASS BALANCE");
    break;
  }
  default: {
    LP_error(fp, "In libhyd%f.2, file %s, in function %s : Unknown river height calculation method.\n", NVERSION_HYD, __FILE__, __func__);
    break;
  }
  }
  return name;
}