/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_musk_set.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"

s_musk_set_hyd *HYD_create_musk_set() {
  s_musk_set_hyd *pmusk;

  pmusk = new_musk_set();
  bzero((char *)pmusk, sizeof(s_musk_set_hyd));
  return pmusk;
}

// fonction pour calculer le paramètre k d'après la biblio k est le temps mis par la crue pour "traverser" la maille. Attention L en Km Area en Km2 et slope en prct
double HYD_calc_k_face(s_face_hyd *pface, FILE *fp) {

  double k = 0;
  double Tc;

  double dx;
  if (pface->prev != NULL && pface->prev->reach->id[INTERN_HYD] == pface->reach->id[INTERN_HYD]) {

    Tc = pface->reach->pmusk->param[K];
    dx = pface->prev->geometry->dx;
    if (dx < EPS_HYD) {
      LP_error(fp, "dx is null for face named %s in reach %d", pface->name, pface->reach->id[GIS_HYD]);
    }

    k = (dx / pface->reach->length) * Tc;
    // LP_printf(fp," face named %s length %f k %f length reach %f \n",pface->name,dx,k,pface->reach->length); // BL to debug
  }
  return k;
}

double HYD_calc_TC(double Area, double slope, double length, FILE *fp) {
  double Tc1, Tc2, Tcmean;

  Tc1 = VENTURA_PAR * sqrt((Area / 1000000)) / sqrt(slope * 100);
  Tc2 = PASSINI_PAR * pow((length / 1000) * (Area / 1000000), 1 / 3) / sqrt(slope * 100);
  // LP_printf(fp," SLOPE %f AREA %f LENGTH %f ",Area,slope,length); // BL to debug
  Tcmean = (Tc1 + Tc2) / 2; // NG : 29/02/2020 : La notice mentionne la moyenne géométrique, pas arithmétique.
  return Tcmean;
}

void HYD_calc_TC_reach(s_chyd *pchyd, FILE *fp) {
  s_reach_hyd *preach;
  int r, nreaches;

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    preach = pchyd->p_reach[r];

    if (preach->pmusk->param[SLOPE] < EPS_HYD) {
      LP_error(fp, " the slope is null for reach  named %s in reach GIS %d INTERN %d  slope %e\n", preach->limits[UPSTREAM]->name, preach->id[GIS_HYD], preach->id[INTERN_HYD], preach->pmusk->param[SLOPE]);
    }

    if (preach->pmusk->param[AREA] < EPS_HYD) {
      LP_error(fp, " the Area BV is null for reach named %s in reach GIS %d INTERN %d  area %e\n", preach->limits[UPSTREAM]->name, preach->id[GIS_HYD], preach->id[INTERN_HYD], preach->pmusk->param[AREA]);
    }
    preach->pmusk->param[K] = HYD_calc_TC(preach->pmusk->param[AREA], preach->pmusk->param[SLOPE], preach->length, fp);
    // LP_printf(fp,"reach named %s in reach GIS %d INTERN %d TC %e \n",preach->limits[UPSTREAM]->name,preach->id[GIS_HYD],preach->id[INTERN_HYD],preach->pmusk->param[K]); //BL to debug
  }
}

/*double HYD_get_TC_Bassin(s_chyd *pchyd,FILE *fp)
{
  double TC=0,max_TC;
  int nreaches,r,ndown,i;
  s_reach_hyd *preach;
  s_singularity_hyd *psing;
  s_ft *TC_bassin,*pftmp;
  s_ft *pft;
  TC_bassin=NULL;
  nreaches=pchyd->counter->nreaches;

  for(r=0;r<nreaches;r++)
    {
      preach=pchyd->p_reach[r];
      psing=preach->limits[UPSTREAM];
      if(psing->nupstr_reaches==0)
        {
          //LP_printf(fp,"\nTete Bassin at reach %s id %d\n",psing->name,preach->id[GIS_HYD]);
          psing=preach->limits[DOWNSTREAM];
          ndown=psing->ndownstr_reaches;
          if(ndown==0)
            LP_warning(fp,"reach %s id %d is don't have downstream\n",psing->name,preach->id[GIS_HYD]);
          TC=preach->pmusk->param[K];
          i=1;
          while(ndown>0)
            {
              if(psing->ndownstr_reaches>1)
                LP_warning(fp,"More than one downstream at singularity %s id %d\n",psing->name,psing->id[GIS_HYD]);
              preach=psing->p_reach[DOWNSTREAM][0];
              // LP_printf(fp," To debug : id_reach %d\n",preach->id[GIS_HYD]);
              TC+=preach->pmusk->param[K];
              psing=preach->limits[DOWNSTREAM];
              ndown=psing->ndownstr_reaches;
              i++;
            }

          pftmp=TS_create_function(0.,TC);
          TC_bassin=TS_secured_chain_fwd_ts(TC_bassin,pftmp);
          if(psing->ndownstr_reaches==0)
            {
              // LP_printf(fp,"outlet reach for river %s id %d length %d\n",psing->name,preach->id[GIS_HYD],i);
              //LP_printf(fp,"TC over the bassin %f\n",TC);
            }
        }

    }
  pft=TS_max_ft(TC_bassin);
  max_TC=pft->ft;
  pft=TS_free_ft(pft);
  TS_free_ts(TC_bassin,fp);
  LP_printf(fp,"\t--> Maximum TC over the bassin is %f sec \n",max_TC);
  return max_TC;
}*/

// NG : 29/02/2020 : Fonction BL obsolète remplacée par HYD_set_TC_reach_Catchments() dans HYD_hydraulics_coupled.c
/*void HYD_set_TC_reach(s_chyd *pchyd,FILE *fp)
{
  s_reach_hyd *preach;
  int r, nreaches;
  double Tc_frac=pchyd->settings->Tc[TC_FRAC];
  for(r=0;r<pchyd->counter->nreaches;r++)
    {
      preach=pchyd->p_reach[r];

      if(preach->pmusk->param[SLOPE] <EPS_HYD)
        {
          LP_error(fp," the slope is null for reach  named %s in reach %d slope %e \n",preach->limits[UPSTREAM]->name,preach->id[GIS_HYD],preach->pmusk->param[SLOPE]);
        }

      if(preach->pmusk->param[AREA]<EPS_HYD)
        {
          LP_error(fp," the Area BV is null for reach named %s in reach %d area %e\n",preach->limits[UPSTREAM]->name,preach->id[GIS_HYD],preach->pmusk->param[AREA]);
        }
      preach->pmusk->param[K]*=Tc_frac;


    }

}*/

void HYD_calc_SDA(s_chyd *pchyd, double **Itr, FILE *fp) {
  s_reach_hyd *preach;
  s_singularity_hyd *psing;
  int SDA = 0, ITRI = 1;
  double Area_up;
  int r, i, ndown, nreaches, r_up;

  nreaches = pchyd->counter->nreaches;
  for (r = 0; r < nreaches; r++) {
    preach = pchyd->p_reach[r];
    psing = preach->limits[UPSTREAM];
    if (psing->nupstr_reaches == 0) {
      // LP_printf(fp,"\nTete Bassin at reach %s id %d\n",psing->name,preach->id[GIS_HYD]);
      psing = preach->limits[DOWNSTREAM];
      ndown = psing->ndownstr_reaches;
      if (ndown == 0)
        LP_warning(fp, "River reach %s GIS ID %d doesn't have a downstream reach.\n", psing->name, preach->id[GIS_HYD]);
      i = 1;
      while (ndown > 0) {

        if (psing->ndownstr_reaches > 1)
          LP_warning(fp, "More than one downstream at singularity %s id %d\n", psing->name, psing->id[GIS_HYD]);
        preach = psing->p_reach[DOWNSTREAM][0];
        Area_up = 0;
        for (i = 0; i < preach->limits[UPSTREAM]->nupstr_reaches; i++) {
          r_up = preach->limits[UPSTREAM]->p_reach[UPSTREAM][i]->id[INTERN_HYD];
          Area_up += Itr[r_up][SDA];
        }
        Itr[preach->id[INTERN_HYD]][SDA] = preach->pmusk->param[AREA] / 1000000 + Area_up;

        psing = preach->limits[DOWNSTREAM];
        ndown = psing->ndownstr_reaches;
        i++;
      }
      // LP_printf(fp,"Area at outlet %s id %d is %f km2\n",psing->name,preach->id[GIS_HYD],Itr[preach->id[INTERN_HYD]][SDA]);
    }
  }
}

// NG : 25/02/2020 : Fonction BL obsolète remplacée par HYD_calc_Itr_i_outlets() dans HYD_hydraulics_coupled.c
/*double HYD_calc_Itr_i(s_chyd *pchyd, double ** Itr,FILE *fp)
{
  s_reach_hyd *preach;
  s_singularity_hyd *psing;
  int SDA=0,ITRI=1;
  double Area_up,betha=0.25,Itr_max=0;
  int r,i,ndown,nreaches;
  s_ft *Itr_max_bv=NULL,*pftmp,*pft;
nreaches=pchyd->counter->nreaches;
for(r=0;r<nreaches;r++)
    {
      preach=pchyd->p_reach[r];
      Itr[r][ITRI]=(preach->length*0.001)/(sqrt(preach->pmusk->param[SLOPE])*pow(Itr[r][SDA],betha));
      psing=preach->limits[UPSTREAM];
      if(psing->nupstr_reaches==0)
        {

          psing=preach->limits[DOWNSTREAM];
          ndown=psing->ndownstr_reaches;

          i=1;
          Itr_max=Itr[r][ITRI];
          while(ndown>0)
            {
              preach=psing->p_reach[DOWNSTREAM][0];
              if(Itr[preach->id[INTERN_HYD]][ITRI]==0)
                Itr[preach->id[INTERN_HYD]][ITRI]=(preach->length*0.001)/(sqrt(preach->pmusk->param[SLOPE])*pow(Itr[preach->id[INTERN_HYD]][SDA],betha));
              Itr_max+=Itr[preach->id[INTERN_HYD]][ITRI];
              psing=preach->limits[DOWNSTREAM];
              ndown=psing->ndownstr_reaches;
              i++;
            }
          pftmp=TS_create_function(0.,Itr_max);
          Itr_max_bv=TS_secured_chain_fwd_ts(Itr_max_bv,pftmp);
          LP_printf(fp,"Itr at outlet %s id %d is %f [km]\n",psing->name,preach->id[GIS_HYD],Itr_max);
        }
    }

 pft=TS_max_ft(Itr_max_bv);
 Itr_max=pft->ft;
 pft=TS_free_ft(pft);
 TS_free_ts(Itr_max_bv,fp);
 LP_printf(fp,"\t--> Maximum Itr_out over the bassin is %f  \n",Itr_max);
 return Itr_max;
}*/

/*
double HYD_calc_Itr_max(s_chyd *pchyd, double ** Itr,FILE *fp)
{
  s_reach_hyd *preach;
  s_singularity_hyd *psing;
  int SDA=0,ITRI=1,ITR_OUT=2;
  double Area_up,bheta=0.25,Itr_max;
  int r,i,ndown;
  s_ft *Itr_max_bv;
 nreaches=pchyd->counter->nreaches;
  for(r=0;r<nreaches;r++)
    {
      preach=pchyd->p_reach[r];
      psing=preach->limits[UPSTREAM];
      if(psing->nupstr_reaches==0)
        {
          //LP_printf(fp,"\nTete Bassin at reach %s id %d\n",psing->name,preach->id[GIS_HYD]);
          psing=preach->limits[DOWNSTREAM];
          ndown=psing->ndownstr_reaches;
          if(ndown==0)
            LP_warning(fp,"reach %s id %d is don't have downstream\n",psing->name,preach->id[GIS_HYD]);
          i=1;
          Itr[r][ITR_OUT]=Itr[r][ITRI];
          while(ndown>0)
            {

              preach=psing->p_reach[DOWNSTREAM][0];
              Itr[r][ITR_OUT]+=Itr[preach->id[INTERN_HYD]][ITRI];
              psing=preach->limits[DOWNSTREAM];
              ndown=psing->ndownstr_reaches;
              i++;
            }
          pftmp=TS_create_function(0.,Itr[r][ITR_OUT]);
          Itr_max_bv=TS_secured_chain_fwd_ts(Itr_max_bv,pftmp);
        }

    }

  Itr_max=TS_max_ft(Itr_max_bv);
  TS_free_ts(TC_bassin,fp);
  LP_printf(fp,"\t--> Maximum Itr_out over the bassin is %f sec \n",Itr_max);

for(r=0;r<nreaches;r++)
    {
      preach=pchyd->p_reach[r];
      psing=preach->limits[UPSTREAM];
      if(psing->nupstr_reaches==0)
        {

          psing=preach->limits[DOWNSTREAM];
          ndown=psing->ndownstr_reaches;
          if(ndown==0)
            LP_warning(fp,"reach %s id %d is don't have downstream\n",psing->name,preach->id[GIS_HYD]);
          i=1;
          while(ndown>0)
            {
              r_up=preach->id[INTERN_HYD];
              preach=psing->p_reach[DOWNSTREAM][0];
              Itr[preach->id[INTERN_HYD]][ITR_OUT]=Itr[r_up][ITR_OUT]-Itr[r_up][ITRI];
              psing=preach->limits[DOWNSTREAM];
              ndown=psing->ndownstr_reaches;
              i++;
            }

        }

    }

  return Itr_max;
}
*/

// NG : 25/02/2020 : Fonction BL obsolète remplacée par HYD_set_K_basin_by_TC_catchments dans HYD_hydraulics_coupled.c
/*double HYD_set_K_TTRA_Bassin(s_chyd *pchyd,double Tc,FILE *fp)
{
  double Itri,Sda,slope,length,bheta=0.25,Area_up;
  double Area,Itr_max;
  int nreaches,r,ndown,i;
  s_reach_hyd *preach;
  s_singularity_hyd *psing;
  s_ft *TC_bassin,*pftmp;
  double **Itr;
  int SDA=0,ITRI=1;
  TC_bassin=NULL;
  nreaches=pchyd->counter->nreaches;
  Itr=(double**)malloc(nreaches*sizeof(double*));
  for(r=0;r<nreaches;r++)
    {
      Itr[r]=(double*)malloc(2*sizeof(double));
      preach=pchyd->p_reach[r];
      Itr[r][SDA]=preach->pmusk->param[AREA]/1000000; // Surface drainée par le reach en km2 (= surface de la cprod associée)
      Itr[r][ITRI]=0;

    }
  HYD_calc_SDA(pchyd,Itr,fp); // Calcul de la surface drainée amont pour chaque reach


  Itr_max=HYD_calc_Itr_i(pchyd,Itr,fp); //BL peut calculer Itr_max ici
  // Itr_max=HYD_calc_Itr_out(pchyd,Itr,fp);

for(r=0;r<nreaches;r++)
    {
      preach=pchyd->p_reach[r];
      preach->pmusk->param[K]=(Itr[r][ITRI]/Itr_max)*Tc;
                LP_printf(fp,"K = %f pour le reach %d.\n",preach->pmusk->param[K],preach->id[GIS_HYD]);
      free(Itr[r]);
      Itr[r]=NULL;
    }
 free(Itr);
 Itr=NULL;
}*/

// NG : 25/02/2020 : Fonction BL obsolète remplacée par HYD_set_K_basin_by_TC_catchments dans HYD_hydraulics_coupled.c
/* void HYD_set_K_basin(s_chyd *pchyd,FILE *fp)
{
  int Kdef;

  Kdef=pchyd->settings->Kdef;


  if(Kdef==FORMULA)
    {
      HYD_calc_TC_reach(pchyd,fp);
      pchyd->settings->Tc[TC_CALC]=HYD_get_TC_Bassin(pchyd,fp);
      if(pchyd->settings->Tc[TC_OBS]>0 && pchyd->settings->Tc[TC_CALC] > pchyd->settings->Tc[TC_OBS])
        {
          LP_warning(fp,"Calculated and observed Tc are different, The Basin Tc is set to %f\n",pchyd->settings->Tc[TC_OBS]);
          pchyd->settings->Tc[TC_FRAC]=pchyd->settings->Tc[TC_OBS]/pchyd->settings->Tc[TC_CALC];
          HYD_set_TC_reach(pchyd,fp);
        }
    }

  else
    {
      if(pchyd->settings->Tc[TC_OBS]==0)
        LP_error(fp,"The global concentration time of the basin (in sec) is not defined or is null\n");
      HYD_set_K_TTRA_Bassin(pchyd,pchyd->settings->Tc[TC_OBS],fp);
    }

}  */
