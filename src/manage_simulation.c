/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_simulation.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD_full.h"

/* Function to initialize a simulation structure */
s_simul *HYD_init_simulation() {
  /* Structure containing all global variables */
  s_simul *psimul;
  int i;

  psimul = new_simulation();
  bzero((char *)psimul, sizeof(s_simul));

  psimul->pchyd = HYD_create_chyd(); // SW 25/01/2018
  HYD_init_chyd(psimul->pchyd);      // SW 25/01/2018

  psimul->chronos = CHR_new_chronos();
  bzero((char *)psimul->chronos, sizeof(s_chronos_CHR));

  psimul->clock = CHR_new_clock();
  psimul->clock->begin = time(NULL);

  psimul->pinout = IO_create_inout_set(NOUTPUTS);
  psimul->pinout->init_from_file[IQ_IO] = NO;
  psimul->pinout->init_from_file[IZ_IO] = NO;

  /* SW 24/12/2018*/
  psimul->pout = new_out_io();
  psimul->pout->init_from_file = NO_IO;
  // psimul->final_state = YES;

  for (i = 0; i < NOUTPUTS; i++)
    psimul->pinout->calc[i] = NO;

  psimul->outputs[PRINT_PK] = (s_output_hyd **)calloc(1, sizeof(s_output_hyd *));
  psimul->outputs[FINAL_STATE] = (s_output_hyd **)calloc(1, sizeof(s_output_hyd *));
  psimul->outputs[ITERATIONS] = (s_output_hyd **)calloc(1, sizeof(s_output_hyd *));
  psimul->outputs[MB_ELE] = (s_output_hyd **)calloc(1, sizeof(s_output_hyd *)); // LV 15/06/2012
  psimul->plec = (s_lec_tmp_hyd **)calloc(NB_LEC_MAX, sizeof(s_lec_tmp_hyd *));
  return psimul;
}
