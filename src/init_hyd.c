/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: init_hyd.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"
/* Structure containing the simulation characteristics */
// extern s_simul *Simul;

/* Initialization of the hydraulic variables in the whole river network */
void HYD_initialize_hydro(s_chyd *pchyd, s_inout_set_io *pinout, s_out_io *pout, s_chronos_CHR *chronos, FILE *fp) {

  HYD_rewind_hydro_tables(pchyd);
  if (pinout != NULL) {
    if (pinout->init_from_file[IQ_IO] == YES) {
      HYD_initialize_Q_from_file(pchyd, fp);
    }
    /*SW 25/01/2018*/
    else {
      HYD_calculate_Q_reach(pchyd, fp);
      HYD_calculation_Q_init(pchyd, fp);
    }
  } else {
    if (pout->init_from_file == NO) {
      HYD_calculate_Q_reach(pchyd, fp);
      HYD_calculation_Q_init(pchyd, fp);
    }
  }
  if (pinout != NULL) {
    if (pinout->init_from_file[IZ_IO] == YES) {
      HYD_initialize_Z_from_file(pchyd, fp);
    }
    /*SW 25/01/2018*/
    else {

      HYD_calculate_Z_sing(pchyd, chronos, fp);
      HYD_calculation_Z_init(pchyd, fp);
    }
  } else {
    HYD_calculate_Z_sing(pchyd, chronos, fp);
    HYD_calculation_Z_init(pchyd, fp);
  }
}

/* Initialization of Z values at element centers from given file */
void HYD_initialize_Z_from_file(s_chyd *pchyd, FILE *fp) {
  /* Number of values in the initialization file */
  int nval = 1;
  /* Reach, element, face nb */
  int r, e, f;
  /* Current face or element */
  s_face_hyd *pface;
  s_element_hyd *pele;
  /* Vertical discretization */
  double dz;

  dz = pchyd->settings->general_param[DZ];

  /* Verification of the nb of elements described in the file */
  pchyd->settings->init_Z = TS_browse_ft(pchyd->settings->init_Z, BEGINNING);
  while (pchyd->settings->init_Z->next != NULL) {
    pchyd->settings->init_Z = pchyd->settings->init_Z->next;
    nval++;
  }

  if (nval != pchyd->counter->nele_tot)

    LP_error(fp, "Warning ! There isn't the right nb of elements in the Z initialization file !\n nval = %d nele_tot = %d\n", nval, pchyd->counter->nele_tot);

  pchyd->settings->init_Z = TS_browse_ft(pchyd->settings->init_Z, BEGINNING);

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    /* Initialization of Z at the centers */
    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {

      pele = pchyd->p_reach[r]->p_ele[e];
      pele->center->hydro->Z[T_HYD] = pchyd->settings->init_Z->ft;
      pchyd->settings->init_Z = pchyd->settings->init_Z->next;

      pele->center->hydro->H[T_HYD] = pele->center->hydro->Z[T_HYD] - pele->center->description->Zbottom;

      if (pele->center->hydro->H[T_HYD] <= 0.) {
        LP_warning(fp, "Warning : initial elevation given at element %dABS_HYD in reach %s branch %d < bottom elevation !!!\n", pele->id[ABS_HYD], pchyd->p_reach[r]->limits[UPSTREAM]->name, pchyd->p_reach[r]->branch_nb); // NF 27/3/2019 improve warning message
        LP_warning(fp, "          the water elevation is set to %f m\n", pchyd->settings->general_param[DZ]);
        pele->center->hydro->H[T_HYD] = pchyd->settings->general_param[DZ];
        pele->center->hydro->Z[T_HYD] = pele->center->hydro->H[T_HYD] + pele->center->description->Zbottom;
      }

      pele->center->hydro = HYD_update_hydro_table(pele->center->hydro, 0., dz, fp); // LV 3/09/2012

      pele->center->hydro->Vel = pele->center->hydro->Q[T_HYD] / pele->center->hydro->Surf;

      // VecSetValue(Simul->calcul_hydro->VZn,pele->id,pele->center->hydro->Z,INSERT_VALUES);//LV nov2014 suppression PetsC
    }

    /* Interpolation of Z at the faces */
    for (f = 0; f < pchyd->p_reach[r]->nfaces; f++) {

      pface = pchyd->p_reach[r]->p_faces[f];

      if (f == 0)
        pface->hydro->Z[T_HYD] = pface->element[TWO]->center->hydro->Z[T_HYD];
      else if (f == pchyd->p_reach[r]->nfaces - 1)
        pface->hydro->Z[T_HYD] = pface->element[ONE]->center->hydro->Z[T_HYD];
      else
        pface->hydro->Z[T_HYD] = TS_interpol(pface->element[ONE]->center->hydro->Z[T_HYD], pface->element[ONE]->center->pk, pface->element[TWO]->center->hydro->Z[T_HYD], pface->element[TWO]->center->pk, pface->pk);

      pface->hydro->H[T_HYD] = pface->hydro->Z[T_HYD] - pface->description->Zbottom;

      if (pface->hydro->H[T_HYD] <= 0.) {
        pface->hydro->H[T_HYD] = pchyd->settings->general_param[DZ];
        pface->hydro->Z[T_HYD] = pface->hydro->H[T_HYD] + pface->description->Zbottom;
      }

      pface->hydro = HYD_update_hydro_table(pface->hydro, 0., dz, fp); // LV 3/09/2012
      pface->hydro->Vel = pface->hydro->Q[T_HYD] / pface->hydro->Surf;
    }
  }
}

/* Initialization of Q values at element faces from given file */
void HYD_initialize_Q_from_file(s_chyd *pchyd, FILE *fp) {
  /* Number of values in the initialization file */
  int nval = 1;
  /* Reach, element, face nb */
  int r, e, f;
  /* Current face or element */
  s_face_hyd *pface;
  s_element_hyd *pele;

  /* Verification of the nb of faces described in the file */
  pchyd->settings->init_Q = TS_browse_ft(pchyd->settings->init_Q, BEGINNING);
  while (pchyd->settings->init_Q->next != NULL) {
    pchyd->settings->init_Q = pchyd->settings->init_Q->next;
    nval++;
  }

  if (nval != pchyd->counter->nfaces_tot)
    LP_error(fp, "Warning ! There isn't the right nb of faces in the U initialization file !");

  pchyd->settings->init_Q = TS_browse_ft(pchyd->settings->init_Q, BEGINNING);

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    /* Initialization of Q at the faces */
    for (f = 0; f < pchyd->p_reach[r]->nfaces; f++) {

      pface = pchyd->p_reach[r]->p_faces[f];
      pface->hydro->Q[T_HYD] = pchyd->settings->init_Q->ft;
      pchyd->settings->init_Q = pchyd->settings->init_Q->next;
    }

    /* Interpolation of Q at the centers */
    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {

      pele = pchyd->p_reach[r]->p_ele[e];
      pele->center->hydro->Q[T_HYD] = pele->face[X_HYD][ONE]->hydro->Q[T_HYD] + pele->face[X_HYD][TWO]->hydro->Q[T_HYD];
      pele->center->hydro->Q[T_HYD] /= 2.;
      // pele->center->hydro->Vel = pele->center->hydro->Q / pele->center->hydro->Surf;
    }
  }
}

/* Initialization of Q values at element faces from given file */
void HYD_initialize_Q_from_file_caw(FILE *fpdon, s_chyd *pchyd, FILE *fp) {
  /* Number of values in the initialization file */
  int nval = 1;
  /* Reach, element, face nb */
  int r, e, f, i, j, id_abs;
  /* Current face or element */
  s_face_hyd *pface;
  s_element_hyd *pele;
  double **val;
  val = HYD_get_init_Q(fpdon, pchyd, fp);

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    /* Interpolation of Q at the centers */
    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {

      pele = pchyd->p_reach[r]->p_ele[e];
      id_abs = pele->id[ABS_HYD];
      // LP_printf(fp,"element %d : \n",id_abs);
      for (i = 0; i < NSTEP_HYD; i++) {

        pele->center->hydro->Q[i] = val[1][id_abs];
        // LP_printf(fp,"Q[%d] : %f\n",i,pele->center->hydro->Q[i]);
        for (j = 0; j < NELEMENT_HYD; j++) {
          pele->face[X_HYD][j]->hydro->Q[i] = val[j][id_abs];
          // LP_printf(fp,"face[%d] Q[%d] : %f\n",j,i,pele->face[X_HYD][j]->hydro->Q[i]);
        }
      }
    }
  }
  for (j = 0; j < NELEMENT_HYD; j++)
    free(val[j]);
  free(val);
  val = NULL;
}
/* Calculation of the free surface elevation at the elements' centers and faces */
void HYD_calculation_Z_init(s_chyd *pchyd, FILE *fp) {
  /* Reach, element, face nb */
  int r, f, e;
  /* Current face */
  s_face_hyd *pface;
  s_face_hyd *pfup, *pfdown;
  /* Current element */
  s_element_hyd *pele;
  /* Vertical discretization */
  double dz;

  dz = pchyd->settings->general_param[DZ];

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    pfup = pchyd->p_reach[r]->p_faces[0];
    pfdown = pchyd->p_reach[r]->p_faces[pchyd->p_reach[r]->nfaces - 1];
    if (pchyd->settings->schem_type == MUSKINGUM) {
      pfup->hydro->Z[T_HYD] = pchyd->p_reach[r]->limits[ONE]->Z_init;
      pfdown->hydro->Z[T_HYD] = pchyd->p_reach[r]->limits[TWO]->Z_init;
    }
    /* Initialization of Z faces */
    for (f = 0; f < pchyd->p_reach[r]->nfaces; f++) {
      pface = pchyd->p_reach[r]->p_faces[f];
      pface->hydro->Z[T_HYD] = TS_interpol(pfup->hydro->Z[T_HYD], pfup->pk, pfdown->hydro->Z[T_HYD], pfdown->pk, pface->pk);

      pface->hydro->Z[PIC_HYD] = pface->hydro->Z[T_HYD]; // BL pour premier calcul de q !!
      pface->hydro->H[T_HYD] = pface->hydro->Z[T_HYD] - pface->description->Zbottom;

      if (pface->hydro->H[T_HYD] <= 0.) {
        if (pchyd->settings->schem_type == MUSKINGUM) {
          pface->hydro->H[T_HYD] = pchyd->p_reach[r]->strahler * STRA2H;
        } else
          pface->hydro->H[T_HYD] = pchyd->settings->general_param[DZ];
        pface->hydro->Z[T_HYD] = pface->hydro->H[T_HYD] + pface->description->Zbottom;
        pface->hydro->Z[PIC_HYD] = pface->hydro->Z[T_HYD]; // BL pour premier calcul de q INR!!
      }

      pface->hydro = HYD_update_hydro_table(pface->hydro, 0., dz, fp);

      pface->hydro->Vel = pface->hydro->Q[T_HYD] / pface->hydro->Surf;
    }

    /* Initialization at the elements */
    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {

      pele = pchyd->p_reach[r]->p_ele[e];
      // if(pele->id[ABS_HYD] == 17 || pele->id[ABS_HYD] == 18)
      // printf("in init_hyd id = %d",pele->id[ABS_HYD]);
      if (e == 0) {
        pele->center->hydro->Z[T_HYD] = pele->face[X_HYD][ONE]->hydro->Z[T_HYD];
        pele->center->hydro->Z[PIC_HYD] = pele->center->hydro->Z[T_HYD];
      } else if (e == pchyd->p_reach[r]->nele - 1) {
        pele->center->hydro->Z[T_HYD] = pele->face[X_HYD][TWO]->hydro->Z[T_HYD];
        pele->center->hydro->Z[PIC_HYD] = pele->center->hydro->Z[T_HYD];

      } else {
        pele->center->hydro->Z[T_HYD] = pele->face[X_HYD][ONE]->hydro->Z[T_HYD] + pele->face[X_HYD][TWO]->hydro->Z[T_HYD];
        pele->center->hydro->Z[T_HYD] /= 2.;
        pele->center->hydro->Z[PIC_HYD] = pele->center->hydro->Z[T_HYD];
      }

      pele->center->hydro->H[T_HYD] = pele->center->hydro->Z[T_HYD] - pele->center->description->Zbottom;

      if (pele->center->hydro->H[T_HYD] <= 0.) {
        pele->center->hydro->H[T_HYD] = pchyd->settings->general_param[DZ];
        pele->center->hydro->Z[T_HYD] = pele->center->hydro->H[T_HYD] + pele->center->description->Zbottom;
        pele->center->hydro->Z[PIC_HYD] = pele->center->hydro->Z[T_HYD];
      }

      // if(pele->id[ABS_HYD] == 506){ // SW 29/01/2018 debug
      //  LP_printf(fp,"width1 =%f ,h = %f dl_dz = %f\n",pele->center->hydro->Width,pele->center->hydro->H[T_HYD],pele->center->hydro->dL_dZ);
      //}
      pele->center->hydro = HYD_update_hydro_table(pele->center->hydro, 0., dz, fp);
      // if(pele->id[ABS_HYD] == 506){ // SW 29/01/2018 debug
      // LP_printf(fp,"width =%f ,h = %f dl_dz = %f\n",pele->center->hydro->Width,pele->center->hydro->H[T_HYD],pele->center->hydro->dL_dZ);
      //}
      pele->center->hydro->Vel = pele->center->hydro->Q[T_HYD] / pele->center->hydro->Surf;
    }
  }
}

/* Calculation of the free surface elevation at the singularities */
void HYD_calculate_Z_sing(s_chyd *pchyd, s_chronos_CHR *chronos, FILE *fp) {
  /* Defining if all the singularities' elevations were calculated */
  int found_all = NO;
  /* Singularity nb */
  int s, r, f;
  /* Initial Z at the singularity */
  double Z_init;
  double Q_sing;
  double Z_am, Z_av;

  double h, dh = CODE;
  s_face_hyd *pface;
  // double pente = 0.0002;
  double pente = 0.;
  int n = 0;
  double dz; // SW 30/01/2018

  dz = pchyd->settings->general_param[DZ]; // SW 30/01/2018
  pente = pchyd->upstr_reach[0]->p_faces[0]->description->Zbottom - pchyd->downstr_reach[0]->p_faces[pchyd->downstr_reach[0]->nfaces - 1]->description->Zbottom;
  pente /= pchyd->downstr_reach[0]->limits[DOWNSTREAM]->pk - pchyd->upstr_reach[0]->p_faces[0]->pk;

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    for (f = 0; f < pchyd->p_reach[r]->nfaces; f++) {

      pface = pchyd->p_reach[r]->p_faces[f];
      pface->hydro->H[T_HYD] = 0.;
      pface->hydro->H[ITER_HYD] = 0.;
      pface->hydro->Width = pface->description->AbscLB - pface->description->AbscRB;
    }
  }

  // while ((fabs(dh) > DZ) && (n < 3)) { // SW 30/01/2018 je comprend pas, DZ = 4 dans libhyd DZ = 7 dans hycu
  while ((fabs(dh) > dz) && (n < 3)) {
    n++;
    dh = 0.;

    for (r = 0; r < pchyd->counter->nreaches; r++) {
      // pente = pchyd->p_reach[r]->p_faces[0]->description->Zbottom - pchyd->p_reach[r]->p_faces[pchyd->p_reach[r]->nfaces-1]->description->Zbottom;
      // pente /= pchyd->p_reach[r]->p_faces[pchyd->p_reach[r]->nfaces-1]->pk - pchyd->p_reach[r]->p_faces[0]->pk;
      // pente = fabs(pente);
      for (f = 0; f < pchyd->p_reach[r]->nfaces; f++) {
        pface = pchyd->p_reach[r]->p_faces[f];
        // if((pface->id[ABS_HYD]==720))// SW 30/01/2018 debug
        // LP_printf(fp,"id = %d dz = %f\n",pface->id[ABS_HYD], dz);

        if (pchyd->settings->schem_type != ST_VENANT) {
          h = pchyd->p_reach[r]->Q_init / (40 * pface->hydro->Width * sqrt(pente));
        } else {
          h = pchyd->p_reach[r]->Q_init / (TS_function_value_t(pchyd->p_reach[r]->Q_init, pchyd->p_reach[r]->strickler, fp) * pface->hydro->Width * sqrt(pente));
        }
        h = pow(h, 3. / 5.);

        dh = dh > fabs(pface->hydro->H[T_HYD] - h) ? dh : fabs(pface->hydro->H[T_HYD] - h);
        // if((pface->name != NULL) && (strcmp(pface->name, "Profil167.114")==0))// SW 30/01/2018 debug
        // LP_printf(fp,"id = %d dz = %f\n",pface->id, dz);

        // printf("face nb %d dh = %f\n",pface->id,pface->hydro->H - h);

        pface->hydro->H[T_HYD] = h;
        pface->hydro->H[ITER_HYD] = h;
        pface->hydro->Width = TS_function_value_t(h, pface->hydro->width, fp);
      }
    }
  }

  for (s = 0; s < pchyd->counter->nsing; s++)
    pchyd->p_sing[s]->passage = NO;

  while (found_all == NO) {

    found_all = YES;

    for (s = pchyd->counter->nsing - 1; s >= 0; s--) {

      if (pchyd->p_sing[s]->passage == NO) {

        if ((pchyd->p_sing[s]->type == WATER_LEVEL) || ((pchyd->p_sing[s]->type == HYDWORK) && (pchyd->p_sing[s]->BC_char[0]->fion_type == ZT))) {
          // if(s == 1)
          // printf("in init_hyd s == 1\n");
          Z_am = TS_function_value_t(chronos->t[BEGINNING], pchyd->p_sing[s]->BC_char[0]->fion, fp);

          if (pchyd->p_sing[s]->ndownstr_reaches > 0) {

            if (pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->limits[DOWNSTREAM]->passage == YES) {

              Z_av = pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->H[T_HYD] + pchyd->p_sing[s]->faces[DOWNSTREAM][0]->description->Zbottom;
              Z_av = TS_max(Z_av, pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->p_faces[pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->nfaces - 1]->hydro->Z[T_HYD]);

              pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[T_HYD] = Z_am;
              pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->Z[T_HYD] = Z_av;
              pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[ITER_HYD] = Z_am;
              pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->Z[ITER_HYD] = Z_av;
              pchyd->p_sing[s]->passage = YES;
              pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[PIC_HYD] = Z_am;
              pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->Z[PIC_HYD] = Z_av;
            }

            else
              found_all = NO;
          }

          else {
            pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[T_HYD] = Z_am;
            pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[ITER_HYD] = Z_am;
            pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[PIC_HYD] = Z_am;
            pchyd->p_sing[s]->passage = YES;
          }
        }

        else if ((pchyd->p_sing[s]->type == HYDWORK) && (pchyd->p_sing[s]->BC_char[0]->fion_type == ZWT)) {

          Q_sing = 0.;
          for (r = 0; r < pchyd->p_sing[s]->nupstr_reaches; r++)
            Q_sing += pchyd->p_sing[s]->p_reach[UPSTREAM][r]->Q_init;

          Z_am = TS_function_value_t(chronos->t[BEGINNING], pchyd->p_sing[s]->BC_char[0]->fion, fp);
          Z_am += pow(Q_sing / (pchyd->p_sing[s]->BC_char[0]->hydwork_param[WIDTH] * pchyd->p_sing[s]->BC_char[0]->hydwork_param[MU] * sqrt(2 * GR)), 2. / 3.);

          if (pchyd->p_sing[s]->ndownstr_reaches > 0) {

            if (pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->limits[DOWNSTREAM]->passage == YES) {

              Z_av = pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->H[T_HYD] + pchyd->p_sing[s]->faces[DOWNSTREAM][0]->description->Zbottom;
              Z_av = TS_max(Z_av, pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->p_faces[pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->nfaces - 1]->hydro->Z[T_HYD]);

              pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[T_HYD] = Z_am;
              pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->Z[T_HYD] = Z_av;
              pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[ITER_HYD] = Z_am;
              pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->Z[ITER_HYD] = Z_av;
              pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[PIC_HYD] = Z_am;
              pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->Z[PIC_HYD] = Z_av;
              pchyd->p_sing[s]->passage = YES;
            }

            else
              found_all = NO;
          }

          else {
            pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[T_HYD] = Z_am;
            pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[ITER_HYD] = Z_am;
            pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[PIC_HYD] = Z_am;
            pchyd->p_sing[s]->passage = YES;
          }
        }

        else if ((pchyd->p_sing[s]->type == HYDWORK) && (pchyd->p_sing[s]->BC_char[0]->fion_type == HT)) {

          Q_sing = 0.;
          for (r = 0; r < pchyd->p_sing[s]->nupstr_reaches; r++)
            Q_sing += pchyd->p_sing[s]->p_reach[UPSTREAM][r]->Q_init;

          if (pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->limits[DOWNSTREAM]->passage == YES) {

            // Zav
            Z_av = pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->H[T_HYD] + pchyd->p_sing[s]->faces[DOWNSTREAM][0]->description->Zbottom;
            Z_av = TS_max(Z_av, pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->p_faces[pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->nfaces - 1]->hydro->Z[T_HYD]);

            Z_am = Z_av;
            Z_am += pow(Q_sing / (pchyd->p_sing[s]->BC_char[0]->hydwork_param[WIDTH] * pchyd->p_sing[s]->BC_char[0]->hydwork_param[MU] * TS_function_value_t(chronos->t[BEGINNING], pchyd->p_sing[s]->BC_char[0]->fion, fp)), 2.) / (2 * GR);

            pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[T_HYD] = Z_am;
            pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->Z[T_HYD] = Z_av;
            pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[ITER_HYD] = Z_am;
            pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->Z[ITER_HYD] = Z_av;
            pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[PIC_HYD] = Z_am;
            pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->Z[PIC_HYD] = Z_av;
            pchyd->p_sing[s]->passage = YES;
          }

          else
            found_all = NO;
        }

        // else if ((pchyd->p_sing[s]->type == CONTINU) ||
        //(pchyd->p_sing[s]->type == CONFLUENCE) ||
        //(pchyd->p_sing[s]->type == DISCHARGE)) {
        else if ((pchyd->p_sing[s]->type == CONTINU) || (pchyd->p_sing[s]->type == CONFLUENCE) || (pchyd->p_sing[s]->type == DISCHARGE) || (pchyd->p_sing[s]->type == TRICONFLUENCE)) { // SW 20/11/2019 add TRICONFLUENCE

          if (pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->limits[DOWNSTREAM]->passage == YES) {

            Z_init = pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->p_faces[pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->nfaces - 1]->hydro->Z[T_HYD];
            Z_init = TS_max(Z_init, pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->H[T_HYD] + pchyd->p_sing[s]->faces[DOWNSTREAM][0]->description->Zbottom);

            for (r = 0; r < pchyd->p_sing[s]->nupstr_reaches; r++) {
              Z_init = TS_max(Z_init, pchyd->p_sing[s]->faces[UPSTREAM][r]->hydro->H[T_HYD] + pchyd->p_sing[s]->faces[UPSTREAM][r]->description->Zbottom);
            }

            for (r = 0; r < pchyd->p_sing[s]->nupstr_reaches; r++) {
              pchyd->p_sing[s]->faces[UPSTREAM][r]->hydro->Z[T_HYD] = Z_init;
              pchyd->p_sing[s]->faces[UPSTREAM][r]->hydro->Z[ITER_HYD] = Z_init;
              pchyd->p_sing[s]->faces[UPSTREAM][r]->hydro->Z[PIC_HYD] = Z_init;
            }
            pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->Z[T_HYD] = Z_init;
            pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->Z[ITER_HYD] = Z_init;
            pchyd->p_sing[s]->faces[DOWNSTREAM][0]->hydro->Z[PIC_HYD] = Z_init;
            pchyd->p_sing[s]->passage = YES;
          } else
            found_all = NO;
        }

        else if ((pchyd->p_sing[s]->type == DIFFLUENCE) || (pchyd->p_sing[s]->type == CONF_DIFF)) {

          if ((pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->limits[DOWNSTREAM]->passage == YES) && (pchyd->p_sing[s]->p_reach[DOWNSTREAM][1]->limits[DOWNSTREAM]->passage == YES)) {

            Z_init = pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->p_faces[pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->nfaces - 1]->hydro->Z[T_HYD];
            Z_init = TS_max(Z_init, pchyd->p_sing[s]->p_reach[DOWNSTREAM][1]->p_faces[pchyd->p_sing[s]->p_reach[DOWNSTREAM][1]->nfaces - 1]->hydro->Z[T_HYD]);

            for (r = 0; r < pchyd->p_sing[s]->nupstr_reaches; r++) {
              Z_init = TS_max(Z_init, pchyd->p_sing[s]->faces[UPSTREAM][r]->hydro->H[T_HYD] + pchyd->p_sing[s]->faces[UPSTREAM][r]->description->Zbottom);
            }

            for (r = 0; r < pchyd->p_sing[s]->ndownstr_reaches; r++) {
              Z_init = TS_max(Z_init, pchyd->p_sing[s]->faces[DOWNSTREAM][r]->hydro->H[T_HYD] + pchyd->p_sing[s]->faces[DOWNSTREAM][r]->description->Zbottom);
            }

            for (r = 0; r < pchyd->p_sing[s]->nupstr_reaches; r++) {
              pchyd->p_sing[s]->faces[UPSTREAM][r]->hydro->Z[T_HYD] = Z_init;
              pchyd->p_sing[s]->faces[UPSTREAM][r]->hydro->Z[ITER_HYD] = Z_init;
              pchyd->p_sing[s]->faces[UPSTREAM][r]->hydro->Z[PIC_HYD] = Z_init;
            }
            for (r = 0; r < pchyd->p_sing[s]->ndownstr_reaches; r++) {
              pchyd->p_sing[s]->faces[DOWNSTREAM][r]->hydro->Z[T_HYD] = Z_init;
              pchyd->p_sing[s]->faces[DOWNSTREAM][r]->hydro->Z[ITER_HYD] = Z_init;
              pchyd->p_sing[s]->faces[DOWNSTREAM][r]->hydro->Z[PIC_HYD] = Z_init;
            }
            pchyd->p_sing[s]->passage = YES;
          } else
            found_all = NO;
        }
        /* SW 20/11/2019 add TRIDIFFLUENCE */
        else if (pchyd->p_sing[s]->type == TRIDIFFLUENCE) {

          if ((pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->limits[DOWNSTREAM]->passage == YES) && (pchyd->p_sing[s]->p_reach[DOWNSTREAM][1]->limits[DOWNSTREAM]->passage == YES) && (pchyd->p_sing[s]->p_reach[DOWNSTREAM][2]->limits[DOWNSTREAM]->passage == YES)) {

            Z_init = pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->p_faces[pchyd->p_sing[s]->p_reach[DOWNSTREAM][0]->nfaces - 1]->hydro->Z[T_HYD];
            Z_init = TS_max(Z_init, pchyd->p_sing[s]->p_reach[DOWNSTREAM][1]->p_faces[pchyd->p_sing[s]->p_reach[DOWNSTREAM][1]->nfaces - 1]->hydro->Z[T_HYD]);
            Z_init = TS_max(Z_init, pchyd->p_sing[s]->p_reach[DOWNSTREAM][2]->p_faces[pchyd->p_sing[s]->p_reach[DOWNSTREAM][2]->nfaces - 1]->hydro->Z[T_HYD]);

            for (r = 0; r < pchyd->p_sing[s]->nupstr_reaches; r++) {
              Z_init = TS_max(Z_init, pchyd->p_sing[s]->faces[UPSTREAM][r]->hydro->H[T_HYD] + pchyd->p_sing[s]->faces[UPSTREAM][r]->description->Zbottom);
              // SW 20/11/2019 for instance, only only one upstream reach
              if (pchyd->p_sing[s]->nupstr_reaches != 1)
                LP_error(fp, "Three downstream reaches connect to more than one upstream reach.\n");
            }

            for (r = 0; r < pchyd->p_sing[s]->ndownstr_reaches; r++) {
              Z_init = TS_max(Z_init, pchyd->p_sing[s]->faces[DOWNSTREAM][r]->hydro->H[T_HYD] + pchyd->p_sing[s]->faces[DOWNSTREAM][r]->description->Zbottom);
            }

            for (r = 0; r < pchyd->p_sing[s]->nupstr_reaches; r++) // SW 20/11/2019 for instance, only only one upstream reach considered. I didn't modify the loop.
            {
              pchyd->p_sing[s]->faces[UPSTREAM][r]->hydro->Z[T_HYD] = Z_init;
              pchyd->p_sing[s]->faces[UPSTREAM][r]->hydro->Z[ITER_HYD] = Z_init;
              pchyd->p_sing[s]->faces[UPSTREAM][r]->hydro->Z[PIC_HYD] = Z_init;
            }
            for (r = 0; r < pchyd->p_sing[s]->ndownstr_reaches; r++) {
              pchyd->p_sing[s]->faces[DOWNSTREAM][r]->hydro->Z[T_HYD] = Z_init;
              pchyd->p_sing[s]->faces[DOWNSTREAM][r]->hydro->Z[ITER_HYD] = Z_init;
              pchyd->p_sing[s]->faces[DOWNSTREAM][r]->hydro->Z[PIC_HYD] = Z_init;
            }
            pchyd->p_sing[s]->passage = YES;
          } else
            found_all = NO;
        } else if (pchyd->p_sing[s]->type == OPEN) {

          pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[T_HYD] = pchyd->p_sing[s]->faces[UPSTREAM][0]->description->Zbottom + pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->H[T_HYD];
          pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[ITER_HYD] = pchyd->p_sing[s]->faces[UPSTREAM][0]->description->Zbottom + pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->H[T_HYD];
          pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->Z[PIC_HYD] = pchyd->p_sing[s]->faces[UPSTREAM][0]->description->Zbottom + pchyd->p_sing[s]->faces[UPSTREAM][0]->hydro->H[T_HYD];
          pchyd->p_sing[s]->passage = YES;
        }
      }
    }
  }
}

/* Calculation of each reach's initial discharge value */
void HYD_calculate_Q_reach(s_chyd *pchyd, FILE *fp) {
  /* Defining if all the reaches' discharges were calculated */
  int found_all = NO;
  /* Reach nb */
  int r;
  /* Initial Q within the current reach */
  double Q_init;
  /* Upstream limit */
  s_singularity_hyd *limit;

  for (r = 0; r < pchyd->counter->nreaches; r++)
    pchyd->p_reach[r]->passage = NO;

  while (found_all == NO) {

    found_all = YES;

    for (r = 0; r < pchyd->counter->nreaches; r++) {

      if (pchyd->p_reach[r]->passage == NO) {

        limit = pchyd->p_reach[r]->limits[UPSTREAM];

        if (limit->type == DISCHARGE) {

          Q_init = TS_function_value_t(0., limit->faces[DOWNSTREAM][0]->pt_inflows[0]->discharge, fp); // LV 22/05/2012
          pchyd->p_reach[r]->Q_init = Q_init;
          pchyd->p_reach[r]->passage = YES;
        }

        else if ((limit->type == CONTINU) || (limit->type == RATING_CURVE) || (limit->type == HYDWORK) || (limit->type == WATER_LEVEL)) {
          if (limit->p_reach[UPSTREAM][0]->passage == YES) {
            Q_init = limit->p_reach[UPSTREAM][0]->Q_init;
            pchyd->p_reach[r]->Q_init = Q_init;
            pchyd->p_reach[r]->passage = YES;
          } else
            found_all = NO;
        }

        else if ((limit->type == CONFLUENCE) || (limit->type == WORK_CONF) || (limit->type == WORKS_CONF) || (limit->type == CONF_WORK)) {

          if ((limit->p_reach[UPSTREAM][0]->passage == YES) && (limit->p_reach[UPSTREAM][1]->passage == YES)) {
            Q_init = limit->p_reach[UPSTREAM][0]->Q_init;
            Q_init += limit->p_reach[UPSTREAM][1]->Q_init;
            pchyd->p_reach[r]->Q_init = Q_init;
            pchyd->p_reach[r]->passage = YES;
          } else
            found_all = NO;
        }
        /* SW 20/11/2019 add TRICONFLUENCE */
        else if (limit->type == TRICONFLUENCE) {

          if ((limit->p_reach[UPSTREAM][0]->passage == YES) && (limit->p_reach[UPSTREAM][1]->passage == YES) && (limit->p_reach[UPSTREAM][2]->passage == YES)) {
            Q_init = limit->p_reach[UPSTREAM][0]->Q_init;
            Q_init += limit->p_reach[UPSTREAM][1]->Q_init;
            Q_init += limit->p_reach[UPSTREAM][2]->Q_init;
            pchyd->p_reach[r]->Q_init = Q_init;
            pchyd->p_reach[r]->passage = YES;
          } else
            found_all = NO;
        } else if ((limit->type == DIFFLUENCE) || (limit->type == DIFF_WORK) || (limit->type == DIFF_WORKS) || (limit->type == WORK_DIFF)) {

          if (limit->p_reach[UPSTREAM][0]->passage == YES) {
            Q_init = limit->p_reach[UPSTREAM][0]->Q_init / 2.;
            pchyd->p_reach[r]->Q_init = Q_init;
            pchyd->p_reach[r]->passage = YES;
          } else
            found_all = NO;
        }
        /* SW 20/11/2019 add TRIDIFFLUENCE */
        else if (limit->type == TRICONFLUENCE) {

          if (limit->p_reach[UPSTREAM][0]->passage == YES) {
            Q_init = limit->p_reach[UPSTREAM][0]->Q_init / 3.;
            pchyd->p_reach[r]->Q_init = Q_init;
            pchyd->p_reach[r]->passage = YES;
          } else
            found_all = NO;
        } else if (limit->type == CONF_DIFF) {

          if ((limit->p_reach[UPSTREAM][0]->passage == YES) && (limit->p_reach[UPSTREAM][1]->passage == YES)) {
            Q_init = (limit->p_reach[UPSTREAM][0]->Q_init + limit->p_reach[UPSTREAM][1]->Q_init) / 2.;
            pchyd->p_reach[r]->Q_init = Q_init;
            pchyd->p_reach[r]->passage = YES;
          } else
            found_all = NO;
        }
      }
    }
  }
}

/* Initialization of the discharge at all the elements' centers and faces */
void HYD_calculation_Q_init(s_chyd *pchyd, FILE *fp) {
  /* Current face */
  s_face_hyd *pface;
  /* Current element */
  s_element_hyd *pele;
  /* Reach, face, element nb */
  int r, f, e;

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    /* Initialization at the faces */
    for (f = 0; f < pchyd->p_reach[r]->nfaces; f++) {

      pface = pchyd->p_reach[r]->p_faces[f];
      pface->hydro->Q[T_HYD] = pface->reach->Q_init;
      pface->hydro->Q[ITER_HYD] = pface->reach->Q_init;
    }

    /* Initialization at the elements */
    for (e = 0; e < pchyd->p_reach[r]->nele; e++) {

      pele = pchyd->p_reach[r]->p_ele[e];
      pele->center->hydro->Q[T_HYD] = pele->reach->Q_init;
      pele->center->hydro->Q[ITER_HYD] = pele->reach->Q_init;
    }
  }
}
