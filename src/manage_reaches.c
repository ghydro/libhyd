/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_reaches.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"
/* Structure containing the simulation caracteristics */
// extern s_simul *Simul;

/* Craetion of the reaches of the simulated network */
void HYD_create_reaches(s_reach_hyd *preachtot, s_chyd *pchyd) {
  /* Loop indexes */
  int s, r, i;
  /* Number of upstream reaches initialized */
  int num_upstr_reach = 0;
  /* Number of upstream reaches initialized */
  int num_downstr_reach = 0;
  int *nupstr_reaches, *ndownstr_reaches;
  /* Current upstream and downstream limits */
  s_singularity_hyd *psing, *ulim, *dlim;

  pchyd->p_reach = (s_reach_hyd **)calloc(pchyd->counter->nreaches, sizeof(s_reach_hyd *));
  preachtot = HYD_rewind_reach(preachtot);

  nupstr_reaches = (int *)calloc(pchyd->counter->nsing, sizeof(int));
  ndownstr_reaches = (int *)calloc(pchyd->counter->nsing, sizeof(int));

  /* Initialisation of the reaches of the Simul->p_reach structure */
  for (r = 0; r < pchyd->counter->nreaches; r++) {

    pchyd->p_reach[r] = preachtot;
    preachtot = preachtot->next;
  }

  /* Counting of the nb of upstream and downstream limits
   * and allocation of the memory space for the upstream and downstream reaches of each singularity
   */
  for (s = 0; s < pchyd->counter->nsing; s++) {

    psing = pchyd->p_sing[s];

    if (psing->nupstr_reaches == 0)
      pchyd->counter->nupstr_limits++;
    else
      psing->p_reach[UPSTREAM] = (s_reach_hyd **)calloc(psing->nupstr_reaches, sizeof(s_reach_hyd *));
    if (psing->ndownstr_reaches == 0)
      pchyd->counter->ndownstr_limits++;
    else
      psing->p_reach[DOWNSTREAM] = (s_reach_hyd **)calloc(psing->ndownstr_reaches, sizeof(s_reach_hyd *));

    nupstr_reaches[s] = ndownstr_reaches[s] = 0;
  }

  /* Attribution of the upstream and downstream reaches to the singularities */
  for (r = 0; r < pchyd->counter->nreaches; r++) {

    ulim = pchyd->p_reach[r]->limits[UPSTREAM];
    dlim = pchyd->p_reach[r]->limits[DOWNSTREAM];

    if (ulim->ndownstr_reaches > 1) {
      ulim->p_reach[TWO][ndownstr_reaches[ulim->id[ABS_HYD]]] = pchyd->p_reach[r];
      ndownstr_reaches[ulim->id[ABS_HYD]]++;
    } else
      ulim->p_reach[TWO][0] = pchyd->p_reach[r];
    if (dlim->nupstr_reaches > 1) {
      dlim->p_reach[ONE][nupstr_reaches[dlim->id[ABS_HYD]]] = pchyd->p_reach[r];
      nupstr_reaches[dlim->id[ABS_HYD]]++;
    } else
      dlim->p_reach[ONE][0] = pchyd->p_reach[r];
  }

  pchyd->upstr_reach = (s_reach_hyd **)calloc(pchyd->counter->nupstr_limits, sizeof(s_reach_hyd *));
  pchyd->downstr_reach = (s_reach_hyd **)calloc(pchyd->counter->ndownstr_limits, sizeof(s_reach_hyd *));

  /* Initialization of the upstream and downstream reaches of the total network */
  for (s = 0; s < pchyd->counter->nsing; s++) {

    psing = pchyd->p_sing[s];

    if (psing->nupstr_reaches == 0) {

      for (i = 0; i < psing->ndownstr_reaches; i++) {
        pchyd->upstr_reach[num_upstr_reach] = psing->p_reach[TWO][i];
        num_upstr_reach++;
      }
    }

    if (psing->ndownstr_reaches == 0) {

      for (i = 0; i < psing->nupstr_reaches; i++) {
        pchyd->downstr_reach[num_downstr_reach] = psing->p_reach[ONE][i];
        num_downstr_reach++;
      }
    }
  }
  free(nupstr_reaches);
  free(ndownstr_reaches);
}

/* Initializes a reach structure */
s_reach_hyd *HYD_create_reach(char *upstr_name, char *downstr_name, int branch_nb, s_chyd *pchyd, FILE *fp) {
  s_reach_hyd *preach;

  preach = new_reach();
  bzero((char *)preach, sizeof(s_reach_hyd));
  // LP_printf(fp,"pchyd->counter->nreaches %d \n",pchyd->counter->nreaches); BL to debug
  preach->id[INTERN_HYD] = pchyd->counter->nreaches;
  preach->id[ABS_HYD] = pchyd->counter->nreaches;
  preach->passage = NO;

  preach->Q_init = CODE;

  // preach->strickler = Simul->settings->general_param[STRICKLER];

  // preach->strickler = new_function();
  // preach->strickler->t = 0.;
  // preach->strickler->ft = Simul->settings->general_param[STRICKLER];

  preach->limits[ONE] = HYD_find_singularity(upstr_name, pchyd);
  preach->limits[ONE]->ndownstr_reaches++;
  preach->id[GIS_HYD] = preach->limits[ONE]->id[GIS_HYD];

  preach->limits[TWO] = HYD_find_singularity(downstr_name, pchyd);
  preach->limits[TWO]->nupstr_reaches++;

  preach->branch_nb = branch_nb;

  if (pchyd->settings->schem_type == MUSKINGUM) {
    preach->pmusk = HYD_create_musk_set();
  }
  preach->strickler = TS_create_function(0., pchyd->settings->general_param[STRICKLER]);
  pchyd->counter->nreaches++;

  return preach;
}

/* Chains two reaches */
s_reach_hyd *HYD_chain_reaches(s_reach_hyd *preach1, s_reach_hyd *preach2) {
  if ((preach1 != NULL) && (preach2 != NULL)) {
    preach1->next = preach2;
    preach2->prev = preach1;
  }

  return preach1;
}

/* Chains two reaches */
s_reach_hyd *HYD_chain_reaches_fwd(s_reach_hyd *preach1, s_reach_hyd *preach2) {
  if ((preach1 != NULL)) {
    preach1->next = preach2;
    preach2->prev = preach1;
  }

  return preach2;
}

s_reach_hyd *HYD_secured_chain_reaches_fwd(s_reach_hyd *preach1, s_reach_hyd *preach2) {
  if (preach1 != NULL)
    preach1 = HYD_chain_reaches(preach1, preach2);
  else {
    preach1 = preach2;
  }
  return preach1;
}

/* Finds a reach in Simul->p_reach with the name of its upper limit and its branch nb */
s_reach_hyd *HYD_find_reach(char *name_upstr_sing, int branch_nb, s_chyd *pchyd) {
  /* Integer defining wether or not the reach was already found */
  int found = NO;
  /* Loop index */
  int i = 0;

  LP_lowercase(name_upstr_sing);

  while ((found == NO) && (i < pchyd->counter->nreaches)) {
    if ((strcmp(pchyd->p_reach[i]->limits[ONE]->name, name_upstr_sing) == 0) && (pchyd->p_reach[i]->branch_nb == branch_nb))
      found = YES;
    else
      i++;
  }

  return pchyd->p_reach[i];
}

/* Finds a reach in Simul->p_reach with the id_gis of its upper limit and its branch nb */
s_reach_hyd *HYD_find_reach_by_id(int id_reach, int branch_nb, s_chyd *pchyd, FILE *fp) {
  /* Integer defining wether or not the reach was already found */
  int found = NO;
  /* Loop index */
  int i = 0;

  while ((found == NO) && (i < pchyd->counter->nreaches)) {
    if ((fabs(pchyd->p_reach[i]->id[GIS_HYD] - id_reach) < EPS_HYD) && (pchyd->p_reach[i]->branch_nb == branch_nb))
      found = YES;
    else
      i++;
  }
  if (found == NO)
    LP_error(fp, "the reach number %d  haven't been found\n", id_reach);
  return pchyd->p_reach[i];
}

/* Rewinds a chain of reaches */
s_reach_hyd *HYD_rewind_reach(s_reach_hyd *preach) {
  s_reach_hyd *preach_temp;

  preach_temp = preach;

  while (preach_temp->prev != NULL)
    preach_temp = preach_temp->prev;

  return preach_temp;
}

// NG : 25/02/2020 : Rewind or fast-forward a reaches chain.
s_reach_hyd *HYD_browse_reach_chain(s_reach_hyd *rchain, int browse_type, FILE *fp) {

  s_reach_hyd *ptmp;

  if (rchain == NULL)
    LP_error(fp, "Nothing to browse in river reach chain. Pointer is NULL. Error in file %s, function %s at line %d.\n", __FILE__, __func__, __LINE__);

  ptmp = rchain;

  switch (browse_type) {
  case BEGINNING_HYD:
    while (ptmp->prev != NULL)
      ptmp = ptmp->prev;
    break;
  case END_HYD:
    while (ptmp->next != NULL)
      ptmp = ptmp->next;
    break;
  default:
    LP_error(fp, "Unknown browse type indicator in file %s, function %s at line %d.\n", __FILE__, __func__, __LINE__);
    break;
  }
  return ptmp;
}

void HYD_assign_river(s_chyd *pchyd) {
  int r, r2;
  s_reach_hyd *preach;
  s_singularity_hyd *pupl;
  double Qmax;
  // int nr;
  int nr = 0; // SW 25/01/2018
  int passage_all = NO;
  if (pchyd->settings->schem_type == MUSKINGUM) {
    for (r = 0; r < pchyd->counter->nreaches; r++) {
      // pchyd->p_reach[r]->river = (char*)malloc(strlen(pchyd->p_reach[r]->limits[UPSTREAM]->name)+1);
      // sprintf(pchyd->p_reach[r]->river,"%s",pchyd->p_reach[r]->limits[UPSTREAM]->name);
      // LP_lowercase(pchyd->p_reach[r]->river);
      pchyd->p_reach[r]->river = pchyd->p_reach[r]->limits[UPSTREAM]->name;
    }

  } else {
    for (r = 0; r < pchyd->counter->nreaches; r++)
      pchyd->p_reach[r]->passage = NO;

    for (r = 0; r < pchyd->counter->nupstr_limits; r++) {
      preach = pchyd->upstr_reach[r];
      preach->passage = YES;
      preach->river = preach->p_faces[0]->pt_inflows[0]->name;
    }

    while (passage_all == NO) {

      passage_all = YES;

      for (r = 0; r < pchyd->counter->nreaches; r++) {
        preach = pchyd->p_reach[r];

        if (preach->passage == NO) {

          pupl = preach->limits[UPSTREAM];
          Qmax = 0.;

          for (r2 = 0; r2 < pupl->nupstr_reaches; r2++) {
            if (pupl->p_reach[UPSTREAM][r2]->Q_init >= Qmax) {
              nr = r2;
              Qmax = pupl->p_reach[UPSTREAM][r2]->Q_init;
            }
          }

          if (pupl->p_reach[UPSTREAM][nr]->passage == YES) {
            preach->passage = YES;
            preach->river = pupl->p_reach[UPSTREAM][nr]->river;
          }

          else
            passage_all = NO;
        }
      }
    }
  }
}

void HYD_set_reach_param(s_reach_hyd *preach, int id_gis, double altFnode, double altTnode, double slope, int strahler, double reach_length, double reach_BV_Area, double k_musk, double alpha_musk, double width, double strickler, double Qlim, FILE *fp) {
  preach->limits[UPSTREAM]->Z_init = altFnode;
  preach->limits[DOWNSTREAM]->Z_init = altTnode;
  preach->strahler = strahler;
  preach->length = reach_length;
  preach->id[GIS_HYD] = id_gis;
  if (reach_BV_Area > 0) {
    preach->pmusk->calc_type = VAR_MUSK;
    preach->pmusk->param[AREA] = reach_BV_Area;
    if (fabs(slope - CODE) < EPS_HYD) {
      preach->pmusk->param[SLOPE] = fabs(altFnode - altTnode) / reach_length;
    } else {
      preach->pmusk->param[SLOPE] = slope;
    }
    if (preach->pmusk->param[SLOPE] < EPS_HYD) {
      preach->pmusk->param[SLOPE] = 1 / reach_length;
      LP_warning(fp, " the SLOPE of reach %d is null it has been changed to %e", preach->id[GIS_HYD], preach->pmusk->param[SLOPE]);
    }

  } else
    LP_error(fp, " the area of reach %d is %e \n", preach->id[GIS_HYD], reach_BV_Area);
  if (k_musk > 0) {
    preach->pmusk->calc_type = CST_MUSK;
    preach->pmusk->param[K] = k_musk;
  }

  if (alpha_musk > 0) {
    preach->pmusk->param[ALPHA] = alpha_musk;
  } else {
    preach->pmusk->param[ALPHA] = DEFAULT_ALPHA;
  }
  if (width > 0) {
    preach->width = width;
  }
  if (strickler > 0) {
    if (preach->strickler != NULL)
      preach->strickler = TS_free_ts(preach->strickler, fp);
    preach->strickler = TS_create_function(0., strickler);
  }
  if (fabs(Qlim - CODE_HYD) < EPS_HYD) {
    preach->Qlim = QLIM_HYD_DEFAULT;

  } else {
    preach->Qlim = Qlim;
  }
}

s_reach_hyd *HYD_create_reaches_musk(s_lec_tmp_hyd **plec, s_chyd *pchyd, int dim_tmp_lec, char **FnodeTnode, FILE *fp) {
  int i;
  s_reach_hyd *preach, *preach_tmp;
  preach = NULL;
  for (i = 0; i < dim_tmp_lec; i++) {
    if (plec[i] != NULL) {

      preach_tmp = HYD_create_reach(FnodeTnode[plec[i]->Fnode], FnodeTnode[plec[i]->Tnode], 1, pchyd, fp);
      HYD_set_reach_param(preach_tmp, plec[i]->id_gis, plec[i]->Alt_Fnode, plec[i]->Alt_Tnode, plec[i]->slope, plec[i]->strahler, plec[i]->length, plec[i]->Wed_Area, plec[i]->k_musk, plec[i]->alpha, plec[i]->width, plec[i]->strickler, plec[i]->Qlim, fp);

      preach = HYD_chain_reaches_fwd(preach, preach_tmp);
    }
  }
  preach = HYD_rewind_reach(preach);
  return preach;
}
