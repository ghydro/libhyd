/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: param_HYDRO.h
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/* Parameter for error messages with LEX and YACC */
#define YYERROR_VERBOSE 1
/* Maximum number of lines that can be read in a file */
#define YYMAXDEPTH_HYD 15000
/* Maximum number of input files which can be read with include() */
#define NPILE 25
/* Maximum number of characters in a (char *) structure */
#define MAXCHAR STRING_LENGTH_LP
/* Maximal number of dimensions */
#define NDIM 2
/* Maximal nb of iteration loops for the steady-state calculation */
#define MAX_CONV 60000
/* Small nb, used to avoid zeros */
#define EPS_HYD EPS_TS
/* For convergence tests */
#define EPS_CONV 0.000001
#define EPS_MB_HYD 0.1
/* Basic longitudinal discretization */
#define BASIC_DX 100.
/* Basic vertical discretization */
#define BASIC_DZ 0.1
/* Basic transversal discretization */
#define BASIC_DY 0.1
/* Default transversal position (y) of inflow (if left undefined) = left_bank */
#define DEFAULT_INFLOW_YPOS 0.25
/* Maximum default number of species for inflows to hydraulic network (should be dynamic) */
#define NB_SPECIE_INFLOW_MAX_HYD 3

// enum  beginning_end {BEGINNING,END,NEXTREMA};
#define BEGINNING BEGINNING_TS
#define END END_TS
#define NEXTREMA NEXTREMA_TS
#define MIDDLE MIDDLE_TS // deja dans param_ts.h
#define DEFAULT_ALPHA 0.3
#define PASSINI_PAR 3888
#define VENTURA_PAR 4578
#define PI 3.141592654
#define NB_LEC_MAX 10000
#define CODE_HYD CODE_TS
#define STRA2H 0.25

enum timing_ref { INI, FIN, NTIME_HYD }; // NF 27/3/2012 from Hysegeos for mb

/* ONE = upstream or left bank
 * TWO = downstream or right bank
 */
enum sequence { ONE, TWO, NELEMENT_HYD };

// enum answers {NO,YES,NANSWERS};
#define NO NO_TS   // 1
#define YES YES_TS // 0
#define NANSWERS NANSWER_TS
#define GIVEN NANSWERS
#define NVAR_HYD NVAR_IO
#define HYD_LIN_QLIM 1000

/* Type of cross section description */
enum type_geometry { ABSC_Z, X_Y_Z, NGEOM };

enum coordinates_hyd { X_HYD, Y_HYD, Z_HYD, NCOORD_HYD };

enum direction { UPSTREAM, DOWNSTREAM, NDIRECTIONS_HYD };

enum calc_states { STEADY, TRANSIENT, NSTATES };

/* WET : the water height in the element is > 0.
 *       or the reach is totally or partially wet
 * DRY : the element/reach is totally dry
 * BORDER : the element is the border of a wet part of the network
 */
enum element_state { WET, DRY, BORDER, NSTATE };

/* Other singularity types */
enum singularity_type { CONTINU, OPEN, DISCHARGE, WATER_LEVEL, RATING_CURVE, HYDWORK, CONFLUENCE, DIFFLUENCE, CONF_DIFF, WORK_DIFF, DIFF_WORK, DIFF_WORKS, WORK_CONF, WORKS_CONF, CONF_WORK, TRIDIFFLUENCE, TRICONFLUENCE, NSINGULARITIES };

// singularités implémentées :
// CONTINU -> deux biefs l'un à la suite de l'autre (singularité possède 1 amont et 1 aval)
// OPEN ->
// DISCHARGE -> CL amont de débit NEUMANN
// WATER_LEVEL -> CL amont de hauteur d'eau DIRICHLET
// HYDWORK -> ouvrage hydraulique
// CONFLUENCE -> confluence de rivères (singularité possède 2 amonts et 1 aval)
// DIFFLUENCE -> diffluence de rivère (singularité possède 1 amont et 2 avals)
// CONF_DIFF -> pour gerer les iles : mix de confluence diffluence (singularité possède 2 amonts et 2 avals)
// TRIDIFFLUENCE -> diffluence de rivières (singularité possède 1 amont et 3 avals)
// TRICONFLUENCE -> confluence de rivières (singularité possède 3 amonts et 1 aval)

// singularités non encore implémentées :
// RATING_CURVE -> CL hauteur d'eau variable
// WORK_DIFF -> ouvrage hydrau avant diffluence
// DIFF_WORK -> ouvrage hydrau après diffluence
// DIFF_WORKS -> ouvrages hydrau après diffluence
// WORK_CONF -> ouvrage hydrau avant confluence
// WORKS_CONF -> ouvrages hydrau avant confluence
// CONF_WORK -> ouvrages hydrau après confluence

/* Time series describing the functionning of an hydraulic work
 * ZT = water elevation upstream of the weir crest
 * ZWT = elevation of the weir crest
 * HT = height of the gate
 * ZQ = rating curve
 */
// enum dam_fions {ZT,ZWT,HT,ZQ,N_FIONS};
enum dam_fions { ZT, ZWT, HT, N_FIONS };

/* General parameters of the simulation
 * THETA = degree of impliciteness of the equations' discretization
 * EPS_Q = convergence precision of Q
 * EPS_Z = convergence precision of Z
 * UP_HMIN = minimal water height upstream of the network
 * DOWN_HMAX = maximal water height downstream of the network
 * DX = longitudinal discretization
 * DY = transversal discretization
 * DZ = vertical discretization
 * CURVATURE = global value of the curvature
 * STRICKLER = gobal value of the Strickler friction coefficient
 */
enum general_param { THETA, EPS_Q, EPS_Z, UP_HMIN, DOWN_HMAX, DX, DY, DZ, CURVATURE, STRICKLER, NPARAM_HYD };

enum inflow_type { UPSTREAM_INFLOW, INFLUENT, EFFLUENT, DIFFUSE_INFLOW, NINFLOW_TYPES };
enum inflow_variable { DISCHARGE_INFLOW_HYD, CONCENTRATION_INFLOW_HYD, TEMPERATURE_INFLOW_HYD, NINFLOW_VARS_HYD }; // NG : 01/10/2020 : Possible variables for an inflow

/* Type of face
 * RAW_SECTION = cross-section defined by its geometry in the Command file
 * INTERPOLATED_FACE = face added after interpolation (no geometrical description)
 */
enum face_types { RAW_SECTION, INTERPOLATED_FACE, NFACE_TYPES };

/* Type of point in the description of cross-sections
 * NOTHING = no peculiar characteristics
 * MIN = local minimum
 * MAX = local maximum
 */
enum pt_types { NOTHING, MIN, MAX, NPT_TYPES };

#define GR 9.81      // m/s^-2
#define RHO_EAU 1000 // kg/m^3

enum outputs { TRANSV_PROFILE, LONG_PROFILE, FINAL_STATE, PRINT_PK, ITERATIONS, MASS_BALANCE, MB_ELE, NOUTPUTS };

// enum factorisation {JACOBI,LU,INCOMPLETE_LU,INCOMPLETE_CHOLESKY,NFACT};

enum hydwork_param { WIDTH, MU, NHYDWORK_PARAM_HYD };
enum hydwork_type { NONE, WEIR, GATE, NHYDWORK_TYPE };

enum graphics_type { GNUPLOT, NGRAPH_TYPE };

enum cs_type { RECTANGULAR, COMPLEX, NCS_TYPE };
enum mc_coefficients { MAZ, MQZ1, MQZ2, NMC_COEFS };
enum id_name_hyd { INTERN_HYD, GIS_HYD, ABS_HYD, NID_HYD };

enum solv_hyd { GC_SOL, SP_SOL, SOLTYPE }; // SW 11/07/2018

enum schem_type { ST_VENANT, MUSKINGUM, NSCHEMTYPE };
enum param_musk { ALPHA, K, AREA, SLOPE, NMUSK_PAR };
enum type_parmusk { CST_MUSK, VAR_MUSK, NTYPE_PAR_MUSK };
enum pk_type { LENGTH_HYD, PK_HYD, NPK_TYPE };
enum hyporeic { EP_HYD, TZ_HYD, NS_HYPO };
enum calc_step { T_HYD, ITER_HYD, INTERPOL_HYD, ITER_INTERPOL_HYD, PIC_HYD, NSTEP_HYD };
enum stock_error { MB_HYD, MUSK_HYD, NSTOCK_HYD };
enum tc_hyd { TC_OBS, TC_CALC, TC_FRAC, NTC };
enum n_qapp { RUNOFF_HYD, INR_HYD, NQAPP };
enum K_deff { FORMULA, TTRA, NKDEF };
enum Ways_Hcalc { MANNING_STRICKLER, MUSKINGUM_MB };

#define NB_MB_VAL_HYD 9
#define NB_Q_VAL_HYD 1
#define NB_Q_FIN_HYD 2
#define NB_H_VAL_HYD 2
#define QLIM_HYD_DEFAULT -0.25
#define NVAL_FICVID 7
#define NVERSION_HYD 0.42
#define NVERSION_HYCU NVERSION_HYD
#define BEGINNING_HYD BEGINNING_TS   // NG : 25/02/2020
#define END_HYD END_TS               // NG : 25/02/2020
#define CODE_NAME_HYD "libhyd"       // SW : 03/05/2022
#define NSPECIES_BIO_HYD NSPECIES_IO // SW : 05/05/2022
