/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_faces.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"

/* Structure containing the simulation caracteristics */
// extern s_simul *Simul;

/* Redifinition of the faces pointer
 * Addition of interpolated faces if the distance between two given faces is too long
 */
void HYD_add_faces_ancien(s_chyd *pchyd, s_chronos_CHR *chronos, FILE *fp) // SW 22/08/2018 maillage par sections
{
  /* Chain of faces in the network */
  s_face_hyd *pfaces;
  /* Loop indexes : reach, face */
  int r, f;
  /* Number of faces */
  int nfaces;
  /* Id of the current face */
  int id_face = 0;
  int id_intern;
  for (r = 0; r < pchyd->counter->nreaches; r++) {
    id_intern = 0;
    if (r > 0)
      id_face++;

    nfaces = pchyd->p_reach[r]->nfaces;
    pfaces = pchyd->p_reach[r]->p_faces[0];
    pfaces->id[ABS_HYD] = id_face;
    pfaces->id[INTERN_HYD] = id_intern++;
    pfaces->new_dx = pchyd->p_reach[r]->p_faces[0]->geometry->dx;
    pfaces->prev = NULL;

    for (f = 0; f < pchyd->p_reach[r]->nfaces - 1; f++) {

      if (pfaces->geometry->dx > pchyd->settings->general_param[DX]) {

        /* While the distance between the 2 current daces is > DX, faces are added by interpolation */
        while (pfaces->new_dx > pchyd->settings->general_param[DX]) {

          nfaces++;
          id_face++;

          pfaces->next = HYD_create_face(X_HYD, pchyd);
          pfaces->next->prev = pfaces;
          pfaces->geometry->dx = pchyd->settings->general_param[DX]; // SW 16/04/2016 pour les sections donnees
          pfaces = pfaces->next;

          pfaces->id[ABS_HYD] = id_face;
          pfaces->def = INTERPOLATED_FACE;
          pfaces->reach = pchyd->p_reach[r];

          pfaces->geometry = HYD_create_geometry(); // SW 16/04/2016 ajout de geometrie pour face interpole, new_dx est utile que pour ici

          pfaces->new_dx = pfaces->prev->new_dx - pchyd->settings->general_param[DX];
          pfaces->prev->new_dx = pchyd->settings->general_param[DX];
          if (pfaces->new_dx > pchyd->settings->general_param[DX])     // SW 16/04/2016 dx est length de l'element
            pfaces->geometry->dx = pchyd->settings->general_param[DX]; // SW 16/04/2016 dx est length de l'element
          else
            pfaces->geometry->dx = pfaces->new_dx; // SW 16/04/2016 dx est length de l'element
          /* Definition of the hydraulic characteristics of the new face */
          HYD_table_hydro_interpolated_face(pfaces, pchyd->p_reach[r]->p_faces[f], pchyd->p_reach[r]->p_faces[f + 1], pchyd->settings->schem_type, chronos, fp);

          /* Description of the new face */
          HYD_description_interpolated_face(pfaces, pchyd->p_reach[r]->p_faces[f + 1]);
        }
      }

      pfaces->next = pchyd->p_reach[r]->p_faces[f + 1];
      pfaces->next->prev = pfaces;
      pfaces = pfaces->next;
      pfaces->new_dx = pchyd->p_reach[r]->p_faces[f + 1]->geometry->dx;
      pfaces->id[ABS_HYD] = ++id_face;
      pfaces->id[INTERN_HYD] = id_intern++;
    }

    pfaces->next = NULL;

    if (nfaces > pchyd->p_reach[r]->nfaces) {
      pchyd->p_reach[r]->nfaces = nfaces;
      pchyd->p_reach[r]->p_faces = (s_face_hyd **)calloc(nfaces, sizeof(s_face_hyd *));

      pfaces = HYD_rewind_faces(pfaces);

      for (f = 0; f < nfaces; f++) {
        pchyd->p_reach[r]->p_faces[f] = pfaces;
        pfaces = pfaces->next;
      }
    }
  }
}

/*cette fonction permet d'effectuer un remaillage du model, dont dx est donne par utilisateur,
le remaillage est fait entre les bathymetries définies (donnees d'entrees), si la longuer de section < dx, on regroupe les sections
dans prose369, le remaillage est fait entre singularities*/

void HYD_add_faces(s_chyd *pchyd, s_chronos_CHR *chronos, FILE *fp) // SW 22/08/2018 maillage par sections, regrouper les sections, si new_dx < dx
{
  /* Chain of faces in the network */
  s_face_hyd *pfaces;
  /* Loop indexes : reach, face */
  int r, f, i;
  int f_1, num2;
  double num1, leng_app;
  /* Number of faces */
  int nfaces;
  /* Id of the current face */
  int id_face = 0;
  int id_intern;
  for (r = 0; r < pchyd->counter->nreaches; r++) {
    id_intern = 0;
    if (r > 0)
      id_face++;

    nfaces = pchyd->p_reach[r]->nfaces;
    pfaces = pchyd->p_reach[r]->p_faces[0];
    pfaces->id[ABS_HYD] = id_face;
    pfaces->id[INTERN_HYD] = id_intern++;
    pfaces->new_dx = pchyd->p_reach[r]->p_faces[0]->geometry->dx;
    pfaces->prev = NULL;
    for (f = 0; f < pchyd->p_reach[r]->nfaces - 1; f++) {
      if (pfaces->geometry->dx > pchyd->settings->general_param[DX]) {

        /* While the distance between the 2 current daces is > DX, faces are added by interpolation */
        while (pfaces->new_dx > pchyd->settings->general_param[DX]) {

          nfaces++;
          id_face++;

          pfaces->next = HYD_create_face(X_HYD, pchyd);
          pfaces->next->prev = pfaces;
          pfaces->geometry->dx = pchyd->settings->general_param[DX]; // SW 16/04/2016 pour les sections donnees
          pfaces = pfaces->next;

          pfaces->id[ABS_HYD] = id_face;
          pfaces->id[INTERN_HYD] = id_intern++;
          pfaces->def = INTERPOLATED_FACE;
          pfaces->reach = pchyd->p_reach[r];

          pfaces->geometry = HYD_create_geometry(); // SW 16/04/2016 ajout de geometrie pour face interpole, new_dx est utile que pour ici

          pfaces->new_dx = pfaces->prev->new_dx - pchyd->settings->general_param[DX];
          pfaces->prev->new_dx = pchyd->settings->general_param[DX];
          if (pfaces->new_dx > pchyd->settings->general_param[DX])     // SW 16/04/2016 dx est length de l'element
            pfaces->geometry->dx = pchyd->settings->general_param[DX]; // SW 16/04/2016 dx est length de l'element
          else
            pfaces->geometry->dx = pfaces->new_dx; // SW 16/04/2016 dx est length de l'element
          /* Definition of the hydraulic characteristics of the new face */
          HYD_table_hydro_interpolated_face(pfaces, pchyd->p_reach[r]->p_faces[f], pchyd->p_reach[r]->p_faces[f + 1], pchyd->settings->schem_type, chronos, fp);

          /* Description of the new face */
          HYD_description_interpolated_face(pfaces, pchyd->p_reach[r]->p_faces[f + 1]);
        }
        pfaces->next = pchyd->p_reach[r]->p_faces[f + 1];
        pfaces->next->prev = pfaces;
        pfaces = pfaces->next;
        pfaces->new_dx = pchyd->p_reach[r]->p_faces[f + 1]->geometry->dx;
        pfaces->id[ABS_HYD] = ++id_face;
        pfaces->id[INTERN_HYD] = id_intern++;
      } else {
        f_1 = f; // longuer de la section inferieure a dx
        // if(f==146 && r == 0)
        // printf("yes\n");
        while ((pfaces->new_dx < pchyd->settings->general_param[DX]) && (f < pchyd->p_reach[r]->nfaces - 2)) {
          f++;
          nfaces--;
          pfaces->new_dx += pchyd->p_reach[r]->p_faces[f]->geometry->dx;
        }

        if (pfaces->new_dx > pchyd->settings->general_param[DX]) {
          num1 = pfaces->new_dx / pchyd->settings->general_param[DX];
          num2 = (int)num1;
          leng_app = pfaces->new_dx / (num2 + 1);
          pfaces->geometry->dx = leng_app;
          // pfaces->new_dx = leng_app;
          for (i = 0; i < num2; i++) {
            nfaces++;
            id_face++;
            pfaces->next = HYD_create_face(X_HYD, pchyd);
            pfaces->next->prev = pfaces;
            pfaces = pfaces->next;
            // pfaces->new_dx = leng_app;
            // pchyd->p_reach[r]->p_faces[f_1]->new_dx =  leng_app * (i + 1);
            pfaces->prev->new_dx = leng_app; // * (i + 1);
            pfaces->new_dx = leng_app * (num2 - i);
            pfaces->id[ABS_HYD] = id_face;
            pfaces->id[INTERN_HYD] = id_intern++;
            pfaces->def = INTERPOLATED_FACE;
            pfaces->reach = pchyd->p_reach[r];
            pfaces->geometry = HYD_create_geometry(); // SW 16/04/2016 ajout de geometrie pour face interpole, new_dx est utile que pour ici
            pfaces->geometry->dx = leng_app;
            HYD_table_hydro_interpolated_face(pfaces, pchyd->p_reach[r]->p_faces[f_1], pchyd->p_reach[r]->p_faces[f + 1], pchyd->settings->schem_type, chronos, fp);
            // pfaces->prev->new_dx =  leng_app;
            /* Description of the new face */
            HYD_description_interpolated_face(pfaces, pchyd->p_reach[r]->p_faces[f + 1]);
          }

          pfaces->next = pchyd->p_reach[r]->p_faces[f + 1];
          pfaces->next->prev = pfaces;
          pfaces = pfaces->next;
          pfaces->new_dx = pchyd->p_reach[r]->p_faces[f + 1]->geometry->dx;
          pfaces->id[ABS_HYD] = ++id_face;
          pfaces->id[INTERN_HYD] = id_intern++;
        } else if (nfaces == 2) // il y a que 2 faces
        {
          leng_app = pfaces->new_dx / 2.0;
          pfaces->geometry->dx = leng_app;
          pfaces->new_dx = leng_app;
          nfaces++;
          id_face++;
          pfaces->next = HYD_create_face(X_HYD, pchyd);
          pfaces->next->prev = pfaces;
          pfaces = pfaces->next;
          pfaces->new_dx = leng_app;
          pfaces->id[ABS_HYD] = id_face;
          pfaces->id[INTERN_HYD] = id_intern++;
          pfaces->def = INTERPOLATED_FACE;
          pfaces->reach = pchyd->p_reach[r];
          pfaces->geometry = HYD_create_geometry(); // SW 16/04/2016 ajout de geometrie pour face interpole, new_dx est utile que pour ici
          pfaces->geometry->dx = leng_app;
          HYD_table_hydro_interpolated_face(pfaces, pchyd->p_reach[r]->p_faces[f_1], pchyd->p_reach[r]->p_faces[f + 1], pchyd->settings->schem_type, chronos, fp);

          /* Description of the new face */
          HYD_description_interpolated_face(pfaces, pchyd->p_reach[r]->p_faces[f + 1]);
          pfaces->next = pchyd->p_reach[r]->p_faces[f + 1];
          pfaces->next->prev = pfaces;
          pfaces = pfaces->next;
          pfaces->new_dx = pchyd->p_reach[r]->p_faces[f + 1]->geometry->dx;
          pfaces->id[ABS_HYD] = ++id_face;
          pfaces->id[INTERN_HYD] = id_intern++;
        } else {
          pfaces->geometry->dx = pfaces->new_dx;
          pfaces->next = pchyd->p_reach[r]->p_faces[f + 1];
          pfaces->next->prev = pfaces;
          pfaces = pfaces->next;
          pfaces->new_dx = pchyd->p_reach[r]->p_faces[f + 1]->geometry->dx;
          pfaces->id[ABS_HYD] = ++id_face;
          pfaces->id[INTERN_HYD] = id_intern++;
        }
      }
    }
    pfaces->next = NULL;

    // if (nfaces > pchyd->p_reach[r]->nfaces) {
    pchyd->p_reach[r]->nfaces = nfaces;
    pchyd->p_reach[r]->p_faces = (s_face_hyd **)calloc(nfaces, sizeof(s_face_hyd *));

    pfaces = HYD_rewind_faces(pfaces);

    for (f = 0; f < nfaces; f++) {
      pchyd->p_reach[r]->p_faces[f] = pfaces;
      pfaces = pfaces->next;
    }
    //}
  }
}

void HYD_check_nb_faces_ancien(s_chyd *pchyd, s_chronos_CHR *chronos, FILE *fpout) // LV 13/06/2012
{
  int r, f;
  s_face_hyd *pface, *pface2, *pfacep;
  // s_reach *preach2;
  s_face_hyd **p_faces2;
  double dx1, dx2;

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    if (pchyd->p_reach[r]->nfaces == 1)
      LP_error(fpout, "Only one face in reach %s -> %s %d\n", pchyd->p_reach[r]->limits[UPSTREAM]->name, pchyd->p_reach[r]->limits[DOWNSTREAM]->name, pchyd->p_reach[r]->branch_nb);
    // LP_printf(fpout," reach %s face %d\n",pchyd->p_reach[r]->limits[UPSTREAM]->name,pchyd->p_reach[r]->nfaces - 1); // BL to debug
    //  LP_printf(fpout,"  fnode %d tnode %d\n",pchyd->p_reach[r]->p_faces[pchyd->p_reach[r]->nfaces - 1]->fnode,pchyd->p_reach[r]->p_faces[pchyd->p_reach[r]->nfaces - 1]->tnode); // BL to debug
    pface = pchyd->p_reach[r]->p_faces[pchyd->p_reach[r]->nfaces - 1];
    if (pface == NULL)
      LP_error(fpout, " Inconsistancy between reach and faces, check your input files elements_musk this error might be due to a bad order of the faces in the reach : \n --> check your Fnode Tnode in reach %s\n", pchyd->p_reach[r]->limits[UPSTREAM]->name);

    if (pface->geometry->dx > EPS_HYD) {

      // preach2 = copy_reach(Simul->p_reach[r]);
      // preach2->nfaces++;
      pchyd->p_reach[r]->nfaces++;
      // preach2->p_faces = (s_face **)calloc(preach2->nfaces,sizeof(s_face *));
      p_faces2 = (s_face_hyd **)calloc(pchyd->p_reach[r]->nfaces, sizeof(s_face_hyd *));

      pface2 = HYD_create_face(X_HYD, pchyd);
      pface2->def = INTERPOLATED_FACE;
      pface2->reach = pchyd->p_reach[r];
      pface2->geometry = HYD_create_geometry();
      pface2->geometry->dx = 0.;

      /* Definition of the hydraulic characteristics of the new face */
      // pface2->hydro = pface->hydro;
      pface2->hydro = HYD_create_hydro(pchyd->settings->schem_type); // LV 03072012
      pface2->hydro->width = pface->hydro->width;                    // LV 03072012
      pface2->hydro->peri = pface->hydro->peri;                      // LV 03072012
      pface2->hydro->surf = pface->hydro->surf;                      // LV 03072012
      pface2->hydro->hydrad = pface->hydro->hydrad;                  // LV 03072012

      /* Description of the new face */
      // pfacep = pchyd->p_reach[r]->p_faces[pchyd->p_reach[r]->nfaces - 2];
      pfacep = pchyd->p_reach[r]->p_faces[pchyd->p_reach[r]->nfaces - 2]; // SW 05/06/2018 puisque nfaces++, nfaces - 2 est le meme que pface
      dx1 = pfacep->geometry->dx;
      dx2 = pface->geometry->dx;

      pface2->description->Abscbottom = pface->description->Abscbottom;
      pface2->description->Zbottom = TS_interpol(pfacep->description->Zbottom, 0., pface->description->Zbottom, dx1, dx1 + dx2);
      pface2->description->AbscLB = pface->description->AbscLB;
      pface2->description->AbscRB = pface->description->AbscRB;

      if (pchyd->p_reach[r]->limits[DOWNSTREAM]->ndownstr_reaches == 0) {
        pface2->description->xc = TS_interpol(pfacep->description->xc, 0., pface->description->xc, dx1, dx1 + dx2);
        pface2->description->yc = TS_interpol(pfacep->description->yc, 0., pface->description->yc, dx1, dx1 + dx2);
        pface2->description->curvature = pface->description->curvature;
      } else {
        pface2->description->xc = pchyd->p_reach[r]->limits[DOWNSTREAM]->p_reach[DOWNSTREAM][0]->p_faces[0]->description->xc;
        pface2->description->yc = pchyd->p_reach[r]->limits[DOWNSTREAM]->p_reach[DOWNSTREAM][0]->p_faces[0]->description->yc;
        pface2->description->curvature = pchyd->p_reach[r]->limits[DOWNSTREAM]->p_reach[DOWNSTREAM][0]->p_faces[0]->description->curvature;
      }

      for (f = 0; f < pchyd->p_reach[r]->nfaces - 1; f++) {
        // preach2->p_faces[f] = Simul->p_reach[r]->p_faces[f];
        p_faces2[f] = pchyd->p_reach[r]->p_faces[f];
        // if(strcmp(pchyd->p_reach[r]->p_faces[f]->name,"P237.19_1")==0) // SW 29/01/2019 pour debug
        // LP_printf(fpout,"xc = %f name = %s r = %d f = %d\n",p_faces2[f]->description->xc,p_faces2[f]->name,r,f);
      }
      // preach2->p_faces[preach2->nfaces - 1] = pface2;
      p_faces2[pchyd->p_reach[r]->nfaces - 1] = pface2;

      // Simul->p_reach[r] = preach2;
      // pchyd->p_reach[r]->p_faces=HYD_free_tab_faces(pchyd->p_reach[r]->p_faces,pchyd->p_reach[r]->nfaces,fpout);
      // pchyd->p_reach[r]->p_faces=HYD_free_tab_faces(pchyd->p_reach[r]->p_faces,pchyd->p_reach[r]->nfaces-1,fpout); // SW 29/01/2018 le nombre de faces dans p_faces est nfaces-1
      pchyd->p_reach[r]->p_faces = HYD_free_p(pchyd->p_reach[r]->p_faces); // SW 29/01/2018 il faut pas desallouer tout, c'est pas propre ici
                                                                           // if(r==2) // SW 29/01/2019 pour debug
                                                                           // LP_printf(fpout,"xc = %f name = %s r = %d f = %d\n",p_faces2[0]->description->xc,p_faces2[0]->name,r,0);

      // HYD_free_p(pchyd->p_reach[r]->p_faces); // BL ça va pas dans l'etat !!!
      // pchyd->p_reach[r]->p_faces = (s_face_hyd **)calloc(pchyd->p_reach[r]->nfaces,sizeof(s_face_hyd *)); //BL pas besoin déjà créé dans p_faces2 !!
      pchyd->p_reach[r]->p_faces = p_faces2;
    }

    if (pchyd->settings->schem_type != MUSKINGUM) {
      if (pchyd->p_reach[r]->nfaces == 2) {

        // preach2 = copy_reach(Simul->p_reach[r]);
        // preach2->nfaces++;
        pchyd->p_reach[r]->nfaces++;
        // preach2->p_faces = (s_face **)calloc(preach2->nfaces,sizeof(s_face *));
        p_faces2 = (s_face_hyd **)calloc(pchyd->p_reach[r]->nfaces, sizeof(s_face_hyd *));

        pface2 = HYD_create_face(X_HYD, pchyd);
        pface2->def = INTERPOLATED_FACE;
        pface2->reach = pchyd->p_reach[r];
        pface2->geometry = HYD_create_geometry();
        pface2->geometry->dx = 0.;
        pface2->geometry->dx = pchyd->p_reach[r]->p_faces[0]->geometry->dx /= 2.;

        /* Definition of the hydraulic characteristics of the new face */

        HYD_table_hydro_interpolated_face(pface2, pchyd->p_reach[r]->p_faces[0], pchyd->p_reach[r]->p_faces[1], pchyd->settings->schem_type, chronos, fpout);

        /* Description of the new face */
        pface2->description->Abscbottom = (pchyd->p_reach[r]->p_faces[0]->description->Abscbottom + pchyd->p_reach[r]->p_faces[1]->description->Abscbottom) / 2.;
        pface2->description->Zbottom = (pchyd->p_reach[r]->p_faces[0]->description->Zbottom + pchyd->p_reach[r]->p_faces[1]->description->Zbottom) / 2.;
        pface2->description->AbscLB = (pchyd->p_reach[r]->p_faces[0]->description->AbscLB + pchyd->p_reach[r]->p_faces[1]->description->AbscLB) / 2.;
        pface2->description->AbscRB = (pchyd->p_reach[r]->p_faces[0]->description->AbscRB + pchyd->p_reach[r]->p_faces[1]->description->AbscRB) / 2.;
        pface2->description->xc = (pchyd->p_reach[r]->p_faces[0]->description->xc + pchyd->p_reach[r]->p_faces[1]->description->xc) / 2.;
        pface2->description->yc = (pchyd->p_reach[r]->p_faces[0]->description->yc + pchyd->p_reach[r]->p_faces[1]->description->yc) / 2.;
        pface2->description->curvature = (pchyd->p_reach[r]->p_faces[0]->description->curvature + pchyd->p_reach[r]->p_faces[1]->description->curvature) / 2.;

        /*preach2->p_faces[0] = Simul->p_reach[r]->p_faces[0];
          preach2->p_faces[1] = pface2;
          preach2->p_faces[2] = Simul->p_reach[r]->p_faces[1];*/
        p_faces2[0] = pchyd->p_reach[r]->p_faces[0];
        p_faces2[1] = pface2;
        p_faces2[2] = pchyd->p_reach[r]->p_faces[1];

        // Simul->p_reach[r] = preach2;

        // pchyd->p_reach[r]->p_faces=HYD_free_tab_faces(pchyd->p_reach[r]->p_faces,pchyd->p_reach[r]->nfaces,fpout);
        pchyd->p_reach[r]->p_faces = HYD_free_tab_faces(pchyd->p_reach[r]->p_faces, pchyd->p_reach[r]->nfaces - 1, fpout); // SW 29/01/2018 le nombre de faces dans p_faces est nfaces-1
        // HYD_free_p(pchyd->p_reach[r]->p_faces);//BL Attention il doit falloir faire un while pour libérer l'ensemble des faces
        //  pchyd->p_reach[r]->p_faces = (s_face_hyd **)calloc(pchyd->p_reach[r]->nfaces,sizeof(s_face_hyd *)); // BL pas besoin déjà créé pour p_face2
        pchyd->p_reach[r]->p_faces = p_faces2;
      }
    }
  }
}

void HYD_check_nb_faces(s_chyd *pchyd, s_chronos_CHR *chronos, FILE *fpout) // LV 13/06/2012
{
  int r, f;
  s_face_hyd *pface, *pface2, *pfacep;
  // s_reach *preach2;
  s_face_hyd **p_faces2;
  double dx1, dx2;

  for (r = 0; r < pchyd->counter->nreaches; r++) {

    if (pchyd->p_reach[r]->nfaces == 1)
      LP_error(fpout, "Only one face in reach %s -> %s %d\n", pchyd->p_reach[r]->limits[UPSTREAM]->name, pchyd->p_reach[r]->limits[DOWNSTREAM]->name, pchyd->p_reach[r]->branch_nb);
    // LP_printf(fpout," reach %s face %d\n",pchyd->p_reach[r]->limits[UPSTREAM]->name,pchyd->p_reach[r]->nfaces - 1); // BL to debug
    //  LP_printf(fpout,"  fnode %d tnode %d\n",pchyd->p_reach[r]->p_faces[pchyd->p_reach[r]->nfaces - 1]->fnode,pchyd->p_reach[r]->p_faces[pchyd->p_reach[r]->nfaces - 1]->tnode); // BL to debug
    pface = pchyd->p_reach[r]->p_faces[pchyd->p_reach[r]->nfaces - 1];
    if (pface == NULL)
      LP_error(fpout, " Inconsistancy between reach and faces, check your input files elements_musk this error might be due to a bad order of the faces in the reach : \n --> check your Fnode Tnode in reach %s\n", pchyd->p_reach[r]->limits[UPSTREAM]->name);

    if (pface->geometry->dx > EPS_HYD) {

      // preach2 = copy_reach(Simul->p_reach[r]);
      // preach2->nfaces++;
      pchyd->p_reach[r]->nfaces++;
      HYD_calc_reach_mean_slope_bed(pchyd->p_reach[r], fpout); // SW 15/10/2018 calculate mean slope of river bed in reach
      // preach2->p_faces = (s_face **)calloc(preach2->nfaces,sizeof(s_face *));
      p_faces2 = (s_face_hyd **)calloc(pchyd->p_reach[r]->nfaces, sizeof(s_face_hyd *));

      pface2 = HYD_create_face(X_HYD, pchyd);
      pface2->def = INTERPOLATED_FACE;
      pface2->reach = pchyd->p_reach[r];
      pface2->geometry = HYD_create_geometry();
      pface2->geometry->dx = 0.;

      /* Definition of the hydraulic characteristics of the new face */
      // pface2->hydro = pface->hydro;
      pface2->hydro = HYD_create_hydro(pchyd->settings->schem_type); // LV 03072012
      pface2->hydro->width = pface->hydro->width;                    // LV 03072012
      pface2->hydro->peri = pface->hydro->peri;                      // LV 03072012
      pface2->hydro->surf = pface->hydro->surf;                      // LV 03072012
      pface2->hydro->hydrad = pface->hydro->hydrad;                  // LV 03072012

      /* Description of the new face */
      // pfacep = pchyd->p_reach[r]->p_faces[pchyd->p_reach[r]->nfaces - 2];
      pfacep = pchyd->p_reach[r]->p_faces[pchyd->p_reach[r]->nfaces - 2]; // SW 05/06/2018 puisque nfaces++, nfaces - 2 est le meme que pface
      dx1 = pfacep->geometry->dx;
      dx2 = pface->geometry->dx;

      pface2->description->Abscbottom = pface->description->Abscbottom;
      // pface2->description->Zbottom = TS_interpol(pfacep->description->Zbottom,0.,pface->description->Zbottom,dx1,dx1 + dx2);
      //  SW 15/10/2018 Zbottom is calculated using mean slope, see above.
      pface2->description->Zbottom = pchyd->p_reach[r]->p_faces[0]->description->Zbottom - pchyd->p_reach[r]->mean_slope * pchyd->p_reach[r]->length;
      pface2->description->AbscLB = pface->description->AbscLB;
      pface2->description->AbscRB = pface->description->AbscRB;

      if (pchyd->p_reach[r]->limits[DOWNSTREAM]->ndownstr_reaches == 0) {
        pface2->description->xc = TS_interpol(pfacep->description->xc, 0., pface->description->xc, dx1, dx1 + dx2);
        pface2->description->yc = TS_interpol(pfacep->description->yc, 0., pface->description->yc, dx1, dx1 + dx2);
        pface2->description->curvature = pface->description->curvature;
      } else {
        pface2->description->xc = pchyd->p_reach[r]->limits[DOWNSTREAM]->p_reach[DOWNSTREAM][0]->p_faces[0]->description->xc;
        pface2->description->yc = pchyd->p_reach[r]->limits[DOWNSTREAM]->p_reach[DOWNSTREAM][0]->p_faces[0]->description->yc;
        pface2->description->curvature = pchyd->p_reach[r]->limits[DOWNSTREAM]->p_reach[DOWNSTREAM][0]->p_faces[0]->description->curvature;
      }

      for (f = 0; f < pchyd->p_reach[r]->nfaces - 1; f++) {
        // preach2->p_faces[f] = Simul->p_reach[r]->p_faces[f];
        p_faces2[f] = pchyd->p_reach[r]->p_faces[f];
        // if(strcmp(pchyd->p_reach[r]->p_faces[f]->name,"P237.19_1")==0) // SW 29/01/2019 pour debug
        // LP_printf(fpout,"xc = %f name = %s r = %d f = %d\n",p_faces2[f]->description->xc,p_faces2[f]->name,r,f);
      }
      // preach2->p_faces[preach2->nfaces - 1] = pface2;
      p_faces2[pchyd->p_reach[r]->nfaces - 1] = pface2;

      // Simul->p_reach[r] = preach2;
      // pchyd->p_reach[r]->p_faces=HYD_free_tab_faces(pchyd->p_reach[r]->p_faces,pchyd->p_reach[r]->nfaces,fpout);
      // pchyd->p_reach[r]->p_faces=HYD_free_tab_faces(pchyd->p_reach[r]->p_faces,pchyd->p_reach[r]->nfaces-1,fpout); // SW 29/01/2018 le nombre de faces dans p_faces est nfaces-1
      pchyd->p_reach[r]->p_faces = HYD_free_p(pchyd->p_reach[r]->p_faces); // SW 29/01/2018 il faut pas desallouer tout, c'est pas propre ici
                                                                           // if(r==2) // SW 29/01/2019 pour debug
                                                                           // LP_printf(fpout,"xc = %f name = %s r = %d f = %d\n",p_faces2[0]->description->xc,p_faces2[0]->name,r,0);

      // HYD_free_p(pchyd->p_reach[r]->p_faces); // BL ça va pas dans l'etat !!!
      // pchyd->p_reach[r]->p_faces = (s_face_hyd **)calloc(pchyd->p_reach[r]->nfaces,sizeof(s_face_hyd *)); //BL pas besoin déjà créé dans p_faces2 !!
      pchyd->p_reach[r]->p_faces = p_faces2;
    }

    if (pchyd->settings->schem_type != MUSKINGUM) {
      if (pchyd->p_reach[r]->nfaces == 2) { // SW 06/11/2018 il faut 2 faces (section) minimum d'entrée

        // preach2 = copy_reach(Simul->p_reach[r]);
        // preach2->nfaces++;
        pchyd->p_reach[r]->nfaces++;
        // preach2->p_faces = (s_face **)calloc(preach2->nfaces,sizeof(s_face *));
        p_faces2 = (s_face_hyd **)calloc(pchyd->p_reach[r]->nfaces, sizeof(s_face_hyd *));

        pface2 = HYD_create_face(X_HYD, pchyd);
        pface2->def = INTERPOLATED_FACE;
        pface2->reach = pchyd->p_reach[r];
        pface2->geometry = HYD_create_geometry();
        pface2->geometry->dx = 0.;
        pface2->geometry->dx = pchyd->p_reach[r]->p_faces[0]->geometry->dx /= 2.;

        /* Definition of the hydraulic characteristics of the new face */
        // SW 15/10/2018 I thinke there is a bug, because pface2->prev which is used in interpolation is not defined.
        HYD_table_hydro_interpolated_face(pface2, pchyd->p_reach[r]->p_faces[0], pchyd->p_reach[r]->p_faces[1], pchyd->settings->schem_type, chronos, fpout);

        /* Description of the new face */
        pface2->description->Abscbottom = (pchyd->p_reach[r]->p_faces[0]->description->Abscbottom + pchyd->p_reach[r]->p_faces[1]->description->Abscbottom) / 2.;
        pface2->description->Zbottom = (pchyd->p_reach[r]->p_faces[0]->description->Zbottom + pchyd->p_reach[r]->p_faces[1]->description->Zbottom) / 2.;
        pface2->description->AbscLB = (pchyd->p_reach[r]->p_faces[0]->description->AbscLB + pchyd->p_reach[r]->p_faces[1]->description->AbscLB) / 2.;
        pface2->description->AbscRB = (pchyd->p_reach[r]->p_faces[0]->description->AbscRB + pchyd->p_reach[r]->p_faces[1]->description->AbscRB) / 2.;
        pface2->description->xc = (pchyd->p_reach[r]->p_faces[0]->description->xc + pchyd->p_reach[r]->p_faces[1]->description->xc) / 2.;
        pface2->description->yc = (pchyd->p_reach[r]->p_faces[0]->description->yc + pchyd->p_reach[r]->p_faces[1]->description->yc) / 2.;
        pface2->description->curvature = (pchyd->p_reach[r]->p_faces[0]->description->curvature + pchyd->p_reach[r]->p_faces[1]->description->curvature) / 2.;

        /*preach2->p_faces[0] = Simul->p_reach[r]->p_faces[0];
          preach2->p_faces[1] = pface2;
          preach2->p_faces[2] = Simul->p_reach[r]->p_faces[1];*/
        p_faces2[0] = pchyd->p_reach[r]->p_faces[0];
        p_faces2[1] = pface2;
        p_faces2[2] = pchyd->p_reach[r]->p_faces[1];

        // Simul->p_reach[r] = preach2;

        // pchyd->p_reach[r]->p_faces=HYD_free_tab_faces(pchyd->p_reach[r]->p_faces,pchyd->p_reach[r]->nfaces,fpout);
        pchyd->p_reach[r]->p_faces = HYD_free_tab_faces(pchyd->p_reach[r]->p_faces, pchyd->p_reach[r]->nfaces - 1, fpout); // SW 29/01/2018 le nombre de faces dans p_faces est nfaces-1
        // HYD_free_p(pchyd->p_reach[r]->p_faces);//BL Attention il doit falloir faire un while pour libérer l'ensemble des faces
        //  pchyd->p_reach[r]->p_faces = (s_face_hyd **)calloc(pchyd->p_reach[r]->nfaces,sizeof(s_face_hyd *)); // BL pas besoin déjà créé pour p_face2
        pchyd->p_reach[r]->p_faces = p_faces2;
      }
    }
  }
}

// SW 15/10/2018
// calculation of mean slope of river bed in a reach
void HYD_calc_reach_mean_slope_bed(s_reach_hyd *preach, FILE *fp) {
  s_ft *pftx;
  s_ft *pftz;
  s_ft *pftx_temp;
  s_ft *pftz_temp;
  double a, b, zbot;
  int f;
  double dx = 0.;
  s_face_hyd *pface;

  pftx = TS_create_function(0., 0.);
  pftz = TS_create_function(0., preach->p_faces[0]->description->Zbottom);
  for (f = 1; f < preach->nfaces - 1; f++) {
    pface = preach->p_faces[f];
    dx += preach->p_faces[f - 1]->geometry->dx;
    zbot = pface->description->Zbottom;
    pftx_temp = TS_create_function(f, dx);
    pftz_temp = TS_create_function(f, zbot);
    pftx = chain_fwd_ts(pftx, pftx_temp);
    pftz = chain_fwd_ts(pftz, pftz_temp);
  }
  TS_linear_regression(pftx, pftz, &a, &b, fp);
  preach->mean_slope = fabs(b);
}
/* Initialization of all the transversal faces in the network
 * pface is calculated in input.y
 */
void HYD_create_faces(s_face_hyd *pface, s_chyd *pchyd, s_chronos_CHR *chronos, FILE *fp) {
  /* Reach, singularity, one/two, face nb */
  int r, s, n, f;
  /* Number of faces already initialized in each reach */
  int *nfaces;
  /* Face id */
  int id_face;
  int id_intern;
  int verif_faces;
  s_singularity_hyd *psing;
  nfaces = (int *)calloc(pchyd->counter->nreaches, sizeof(int));

  for (r = 0; r < pchyd->counter->nreaches; r++) {
    // LP_printf(fp,"reach in pchyd reach %d nfaces %d \n",pchyd->p_reach[r]->id[ABS_HYD],pchyd->p_reach[r]->nfaces); // BL to debug
    pchyd->p_reach[r]->p_faces = (s_face_hyd **)malloc(pchyd->p_reach[r]->nfaces * sizeof(s_face_hyd *));
    nfaces[r] = 0;
  }

  pface = HYD_rewind_faces(pface);

  /* The faces of the pface chain are put in the <<reach>> structures */
  if (pface != NULL) {

    pface->reach->p_faces[0] = pface;
    nfaces[pface->reach->id[ABS_HYD]]++;

    while (pface->next != NULL) {

      pface = pface->next;
      pface->reach->p_faces[nfaces[pface->reach->id[ABS_HYD]]] = pface;
      nfaces[pface->reach->id[ABS_HYD]]++;

      // LP_printf(fp,"reach in pface reach %d nfaces %d \n",pface->reach->id[ABS_HYD],nfaces[pface->reach->id[ABS_HYD]]); //BL to debug
    }
  } else
    LP_error(fp, "the face pointer is null in create_faces !! \n");
  // LV 13/06/2012 : ajout d'une face en fin de bief et interpolation si seulement 2 faces

  HYD_check_nb_faces(pchyd, chronos, fp);

  /* The upstream and downstream faces at the singularities are initialized */
  for (s = 0; s < pchyd->counter->nsing; s++) {
    psing = pchyd->p_sing[s];
    psing->faces[ONE] = (s_face_hyd **)malloc(psing->nupstr_reaches * sizeof(s_face_hyd *));

    psing->faces[TWO] = (s_face_hyd **)malloc(psing->ndownstr_reaches * sizeof(s_face_hyd *));
    //    LP_printf(fp,"Dealing with singularity %d named %s\n",psing->id[ABS_HYD],psing->name); // BL to debug
    // LP_printf(fp,"Nb upstream %d Nb downstr %d \n",psing->nupstr_reaches,psing->ndownstr_reaches);// BL to debug
    for (n = 0; n < psing->nupstr_reaches; n++) {
      // LP_printf(fp,"Up ,%s \n",psing->name);// BL to debug
      psing->faces[ONE][n] = psing->p_reach[UPSTREAM][n]->p_faces[psing->p_reach[UPSTREAM][n]->nfaces - 1];
      psing->faces[ONE][n]->limits[DOWNSTREAM] = psing;
    }

    for (n = 0; n < psing->ndownstr_reaches; n++) {
      // LP_printf(fp,"Down ,%s \n",psing->name);// BL to debug
      psing->faces[TWO][n] = psing->p_reach[DOWNSTREAM][n]->p_faces[0];
      psing->faces[TWO][n]->limits[UPSTREAM] = psing;
    }
  }

  if (pchyd->settings->general_param[DX] != CODE) {

    HYD_add_faces(pchyd, chronos, fp);
  } else {
    for (r = 0; r < pchyd->counter->nreaches; r++) {

      for (f = 0; f < pchyd->p_reach[r]->nfaces; f++)
        pchyd->p_reach[r]->p_faces[f]->new_dx = pchyd->p_reach[r]->p_faces[f]->geometry->dx;
    }
  }

  /* Initialisation of faces' ids */
  // LV nov2014 : ajout du calcul de la distance entre les centres d'élément amont et aval

  id_face = 0;
  for (r = 0; r < pchyd->counter->nreaches; r++) {
    id_intern = 0; // plutot un id face par reach ?
    for (f = 0; f < pchyd->p_reach[r]->nfaces; f++) {
      pchyd->p_reach[r]->p_faces[f]->id[INTERN_HYD] = id_face;
      pchyd->p_reach[r]->p_faces[f]->id[ABS_HYD] = id_face;
      id_face++;
      id_intern++;

      if (f == 1) // LV nov2014
        pchyd->p_reach[r]->p_faces[f]->ds = pchyd->p_reach[r]->p_faces[f - 1]->new_dx + 0.5 * pchyd->p_reach[r]->p_faces[f]->new_dx;
      else if ((f > 1) && (f < pchyd->p_reach[r]->nfaces - 2)) // LV nov2014
        pchyd->p_reach[r]->p_faces[f]->ds = 0.5 * (pchyd->p_reach[r]->p_faces[f - 1]->new_dx + pchyd->p_reach[r]->p_faces[f]->new_dx);
      else if (f == pchyd->p_reach[r]->nfaces - 2 && f > 0) // LV nov2014
        pchyd->p_reach[r]->p_faces[f]->ds = 0.5 * pchyd->p_reach[r]->p_faces[f - 1]->new_dx + pchyd->p_reach[r]->p_faces[f]->new_dx;
    }

    pchyd->counter->nfaces_tot += pchyd->p_reach[r]->nfaces;
  }
  // LP_printf(fp,"Out of create_faces\n"); // BL to debug
  free(nfaces);
}

/* Creation of one face
 * type = X or Y (transversal or lateral face)
 */
s_face_hyd *HYD_create_face(int type, s_chyd *pchyd) {
  s_face_hyd *pface;

  pface = new_face();
  bzero((char *)pface, sizeof(s_face_hyd));
  pface->name = NULL;
  pface->type = type;

  pface->pk = CODE;
  pface->ds = CODE;

  pface->description = HYD_create_description(pchyd);
  pface->next = NULL;
  pface->prev = NULL;

  return pface;
}

/* Function used to chain two faces */
/*s_face *chain_faces(s_face *pface1,s_face *pface2)
{
  if ((pface1 != NULL) && (pface2 != NULL)) {
    pface1->next = pface2;
    pface2->prev = pface1;
  }

  return pface1;
}*/

// LV 27/09/2012
s_face_hyd *HYD_chain_faces(s_face_hyd *pface1, s_face_hyd *pface2) {
  if (pface2 != NULL) {
    if (pface1 != NULL) {
      pface1->next = pface2;
      pface2->prev = pface1;
    } else
      pface1 = pface2;
  }

  return pface1;
}
/* Chains two faces fwd */
s_face_hyd *HYD_chain_faces_fwd(s_face_hyd *pf1, s_face_hyd *pf2) {
  if ((pf1 != NULL)) {
    pf1->next = pf2;
    pf2->prev = pf1;
  }

  return pf2;
}

/* Function used to rewind a chain of faces */
s_face_hyd *HYD_rewind_faces(s_face_hyd *pface) {
  s_face_hyd *pface_temp;

  pface_temp = pface;

  if (pface_temp != NULL) {
    while (pface_temp->prev != NULL)
      pface_temp = pface_temp->prev;
  }

  return pface_temp;
}

/* Function used to rewind a chain of faces */
s_face_hyd *HYD_browse_faces(s_face_hyd *pface, int beg_end) {
  s_face_hyd *pface_temp;

  pface_temp = pface;
  if (beg_end == BEGINNING) {
    if (pface_temp != NULL) {
      while (pface_temp->prev != NULL)
        pface_temp = pface_temp->prev;
    }
  } else {
    if (pface_temp != NULL) {
      while (pface_temp->next != NULL)
        pface_temp = pface_temp->next;
    }
  }

  return pface_temp;
}
