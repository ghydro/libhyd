/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libhyd
 * FILE NAME: manage_tmp_lec.c
 *
 * CONTRIBUTORS: Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: 1D river network simulation solving in 0D with
 * Muskigum or 1D with Barre de Saint Venant equation, 1D finite volume using
 * a semi-implicit temporal scheme, 0D approach is tuned into a 0.5D one as
 * long as geometries are defined, 1D approach handles islands, tributaries,
 * and hydraulics works such as dams.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libhyd Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "libpc.h"
#include "CHR.h"
#include "IO.h"
#include "GC.h"
#ifdef COUPLED
#include "spa.h"
#endif
#include "HYD.h"

s_lec_tmp_hyd *HYD_create_tmp_lec(char *name, int id_gis, int Fnode, int Tnode, double Alt_Fnode, double Alt_Tnode, double slope, int strahler, double length, double Wed_Area, double k_musk, double alpha, double width, double strickler, double Qlim, FILE *fp) {
  s_lec_tmp_hyd *plec;
  // char *new_name;
  int id_gis_order;
  plec = new_lec_tmp();
  bzero((char *)plec, sizeof(s_lec_tmp_hyd));
  id_gis_order = HYD_get_order((double)id_gis);
  // LP_printf(fp," in create_tmp_lec id_gis %d order %d \n",id_gis,id_gis_order); //BL to debug
  if (plec->name != NULL)
    free(plec->name);
  plec->name = (char *)malloc((strlen(name) + ALLOCSCD_LP) * sizeof(char));
  sprintf(plec->name, "%s_%d", name, id_gis);

  // LP_printf(fp," new_name %s \n",plec->name); // BL to debug
  if (name != NULL)
    free(name);
  plec->id_gis = id_gis;
  plec->Fnode = Fnode;
  plec->Tnode = Tnode;
  plec->Alt_Fnode = Alt_Fnode;
  plec->Alt_Tnode = Alt_Tnode;
  plec->strahler = strahler;
  plec->length = length;
  if (Wed_Area <= 0 && fabs(Wed_Area - CODE) > EPS_HYD)
    LP_error(fp, "the area of reach %d is negative : %e could be due to integer out of range in function atoi in lexical.l turn the value into double (adding .0)\n", id_gis, Wed_Area);
  /*
else
    LP_printf(fp,"the area of reach %d is %e\n",id_gis,Wed_Area);
  //to debug */
  plec->Wed_Area = Wed_Area;
  plec->k_musk = k_musk;
  plec->alpha = alpha;
  plec->width = width;
  plec->strickler = strickler;
  plec->Qlim = Qlim;
  if (slope > 0)
    plec->slope = slope;
  else
    plec->slope = CODE;
  return plec;
}

s_lec_tmp_hyd **HYD_resize_tmp_lec(s_lec_tmp_hyd **plec, int new_size, int old_size, FILE *fp) {
  // int dim_lec_tmp=NB_LEC_MAX;
  // s_lec_tmp_hyd** plec_tmp;
  int i;

  plec = (s_lec_tmp_hyd **)realloc(plec, new_size * sizeof(s_lec_tmp_hyd *));
  if (plec == NULL) {
    free(plec);
    LP_error(fp, "error in resizing plec \n");
  } else {
    for (i = old_size; i < new_size; i++) {
      plec[i] = NULL;
    }
  }
  return plec;
}

s_lec_tmp_hyd **HYD_free_plec(s_lec_tmp_hyd **plec, int dim_tmp_lec, FILE *fp) {
  int i;
  // LP_printf(fp,"dim_tmp_lec in free %d\n",dim_tmp_lec); // BL to debug
  for (i = 0; i < dim_tmp_lec; i++) {
    if (plec[i] != NULL) {
      free(plec[i]->name);
      plec[i]->name = NULL;
    }
    plec[i] = (s_lec_tmp_hyd *)HYD_free_p(plec[i]);
  }
  plec = (s_lec_tmp_hyd **)HYD_free_p(plec);
  if (plec != NULL)
    LP_error(fp, "the pointeur hasn't been de allocated in HYD_free_plec \n");
  return plec;
}

int HYD_get_max_node(s_lec_tmp_hyd **plec, int dim_tmp_lec, FILE *fp) {

  int i;
  int max = CODE;
  double max_tmp;
  for (i = 0; i < dim_tmp_lec; i++) {

    if (plec[i] != NULL)
      max_tmp = TS_max((double)plec[i]->Fnode, (double)plec[i]->Tnode);

    max = (int)TS_max((double)max, (double)max_tmp);
  }
  return max;
}
/*
s_ft *HYD_define_pk(s_lec_tmp_hyd **plec,int dim_tmp_lec)
{

  int i,j;
  s_id_io *upstr,*uptr_tmp;
  int nb_upstr;

  for(i=0;i<dim_tmp_lec;i++)
    {
      if(plec[i]!=NULL)
        {
          Fnode=plec[i]->Fnode;
        }
      nb_upstr=0;
      for(j=0;j<dim_tmp_lec;j++)
        {
          if(plec[j]!=NULL)
            {
              if(plec[j]->Tnode == Fnode)
                {
                  nb_uptr++;
                  break;
                }
            }


        }
      if(nb_upstr > 0)
        {
          //create id

        }

    }


}
*/
