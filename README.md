<br />
<div align="left">
 
  <h2 align="left">libhyd</h2>
  <p align="left">
    1D river network simulation solving in 0D with Muskigum or 1D with Barre de Saint-Venant equation, 1D finite volume using a semi-implicit temporal scheme, 
    0D approach is tuned into a 0.5D one as long as geometries are defined, 1D approach handles islands, tributaries and hydraulics works such as dams. 
    <br />
    <br />
    Library developed at the Centre for geosciences and geoengineering, Mines Paris/ARMINES, PSL University, Fontainebleau, France.
    <br />
    <br />
    <strong>Contributors</strong>
    <br />
    Lauriane VILMIN, Shuaitao WANG, Nicolas FLIPO, Nicolas GALLOIS
    <br />
    <br />
    <strong>Contact</strong>
    <br />
    Nicolas FLIPO <a href="mailto:nicolas.flipo@minesparis.psl.eu">nicolas.flipo@minesparis.psl.eu</a>
    <br />
    Nicolas GALLOIS <a href="mailto:nicolas.gallois@minesparis.psl.eu">nicolas.gallois@minesparis.psl.eu</a>
  </p>
</div>

## Copyright

[![License](https://img.shields.io/badge/License-EPL_2.0-blue.svg)](https://opensource.org/licenses/EPL-2.0)

&copy; 2022 Contributors to the libhyd library.

*All rights reserved*. This software and the accompanying materials are made available under the terms of the Eclipse Public License (EPL) v2.0 
which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v20.html.
